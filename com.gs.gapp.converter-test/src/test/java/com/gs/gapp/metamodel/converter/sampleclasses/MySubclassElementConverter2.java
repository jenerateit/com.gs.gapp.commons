/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
//@ConversionMapping(source=MySubclassInputModelElement2.class, target=MySubclassModelElement2.class)
public class MySubclassElementConverter2 extends MyElementConverter2 {

	public MySubclassElementConverter2(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.GAppAbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected MySubclassModelElement2 onCreateModelElement(
			MyInputModelElement2 originalModelElement, ModelElementI previouslyExistingElement) {
		
		return new MySubclassModelElement2(originalModelElement.getName(),
						                   originalModelElement.getName() + " ... comment");
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.sampleclasses.MyElementConverter1#onConvert(com.gs.gapp.metamodel.converter.sampleclasses.MyInputModelElement1, com.gs.gapp.metamodel.converter.sampleclasses.MyModelElement1)
	 */
	protected void onConvert(MyInputModelElement2 originalModelElement,
			MySubclassModelElement2 resultingModelElement) {
		
		super.onConvert(originalModelElement, resultingModelElement);
		
		if (originalModelElement.getDate() != null) {
			resultingModelElement.setBody(originalModelElement.getDate().toString());
		}
	}
}
