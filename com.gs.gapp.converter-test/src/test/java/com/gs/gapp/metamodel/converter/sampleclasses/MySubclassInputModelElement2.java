/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;

import java.util.Date;

/**
 * @author mmt
 *
 */
public class MySubclassInputModelElement2 extends MyInputModelElement2 {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4028146293930177844L;

	/**
	 * @param name
	 */
	public MySubclassInputModelElement2(String name, Date date) {
		super(name, date);
	}
}
