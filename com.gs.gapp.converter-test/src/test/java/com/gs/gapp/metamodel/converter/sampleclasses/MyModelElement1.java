/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;


/**
 * @author mmt
 *
 */
public class MyModelElement1 extends MyModelElement {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7818183872832989782L;

	/**
	 * @param name
	 */
	public MyModelElement1(String name, String comment) {
		super(name, comment);
	}
}
