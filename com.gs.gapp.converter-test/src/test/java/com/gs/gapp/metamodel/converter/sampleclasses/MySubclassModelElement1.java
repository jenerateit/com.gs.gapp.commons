/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;


/**
 * @author mmt
 *
 */
public class MySubclassModelElement1 extends MyModelElement1 {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7818183872832989782L;

	/**
	 * @param name
	 */
	public MySubclassModelElement1(String name, String comment) {
		super(name, comment);
	}
}
