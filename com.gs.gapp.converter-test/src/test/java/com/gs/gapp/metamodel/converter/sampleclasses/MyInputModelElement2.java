/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;

import java.util.Date;

/**
 * @author mmt
 *
 */
public class MyInputModelElement2 extends MyInputModelElement {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8677581534100122491L;
	
	private final Date date;

	/**
	 * @param name
	 */
	public MyInputModelElement2(String name, Date date) {
		super(name);
		this.date = date;
	}

	/**
	 * @return the date
	 */
	protected final Date getDate() {
		return date;
	}
}
