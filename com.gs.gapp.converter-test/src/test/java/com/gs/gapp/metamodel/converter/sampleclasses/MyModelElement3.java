/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;


/**
 * @author mmt
 *
 */
public class MyModelElement3 extends MyModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7865289400397026648L;

	/**
	 * @param name
	 */
	public MyModelElement3(String name, String comment) {
		super(name, comment);
	}

}
