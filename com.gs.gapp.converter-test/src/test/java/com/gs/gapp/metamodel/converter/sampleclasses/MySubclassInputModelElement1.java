/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;

/**
 * @author mmt
 *
 */
public class MySubclassInputModelElement1 extends MyInputModelElement1 {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4028146293930177844L;

	/**
	 * @param name
	 */
	public MySubclassInputModelElement1(String name) {
		super(name);
	}
}
