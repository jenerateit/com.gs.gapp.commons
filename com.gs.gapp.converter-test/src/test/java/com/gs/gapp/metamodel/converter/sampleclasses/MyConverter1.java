/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
public class MyConverter1 extends MyConverter {
	
	/**
	 * 
	 */
	public MyConverter1() {
		super();
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.GAppAbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result =
			new ArrayList<>();
		
		result.add( new MyElementConverter1<>(this) );
		result.add( new MySubclassElementConverter1<>(this) );
		
		return result;
	}


	@Override
	protected void onPerformCompatibilityCheck(Set<?> elements) {
		// TODO Auto-generated method stub
		
	}

}
