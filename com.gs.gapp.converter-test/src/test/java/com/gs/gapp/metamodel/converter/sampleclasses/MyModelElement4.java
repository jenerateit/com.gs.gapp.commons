/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;


/**
 * @author mmt
 *
 */
public class MyModelElement4 extends MyModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085814752958619541L;

	/**
	 * @param name
	 */
	public MyModelElement4(String name, String comment) {
		super(name, comment);
	}

}
