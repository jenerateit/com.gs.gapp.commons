/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
//@ConversionMapping(source=MyInputModelElement2.class, target=MyModelElement2.class)
public class MyElementConverter2 extends AbstractModelElementConverter<MyInputModelElement2, MyModelElement2> {

	public MyElementConverter2(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.GAppAbstractModelElementConverter#doConversion(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(
			MyInputModelElement2 originalModelElement,
			MyModelElement2 resultingModelElement) {
		
		
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.GAppAbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected MyModelElement2 onCreateModelElement(
			MyInputModelElement2 originalModelElement, ModelElementI previouslyExistingElement) {
		
		return new MyModelElement2(originalModelElement.getName() + " ... name after conversion",
				                   originalModelElement.getName() + " ... comment after conversion");
	}

}
