/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
//@ConversionMapping(source=MyInputModelElement1.class, target=MyModelElement1.class)
public class MyElementConverter1<S extends MyInputModelElement1, T extends MyModelElement1> extends AbstractModelElementConverter<S, T> {

	public MyElementConverter1(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.GAppAbstractModelElementConverter#doConversion(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(
			S originalModelElement,
			T resultingModelElement) {
		
		if (originalModelElement.getRelatedInputModelelement() != null) {
			MyModelElement myModelElement =
				convertWithOtherConverter(MyModelElement.class, originalModelElement.getRelatedInputModelelement());
			if (myModelElement != null) {
				resultingModelElement.setRelatedElement(myModelElement);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.GAppAbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyExistingElement) {
		
		T result = (T) new MyModelElement1(originalModelElement.getName() + " ... name after conversion",
				                           originalModelElement.getName() + " ... comment after conversion");
		
		return result;
	}
	
	
}
