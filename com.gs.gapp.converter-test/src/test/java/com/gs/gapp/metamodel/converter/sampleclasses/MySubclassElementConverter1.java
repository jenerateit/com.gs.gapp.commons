/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
//@ConversionMapping(source=MySubclassInputModelElement1.class, target=MySubclassModelElement1.class)
public class MySubclassElementConverter1<S extends MyInputModelElement1, T extends MySubclassModelElement1> extends MyElementConverter1<S, T> {

	public MySubclassElementConverter1(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.GAppAbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(
			S originalModelElement, ModelElementI previouslyExistingElement) {
		
		return (T) new MySubclassModelElement1(originalModelElement.getName(),
						                       originalModelElement.getName() + " ... comment");
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.sampleclasses.MyElementConverter1#onConvert(com.gs.gapp.metamodel.converter.sampleclasses.MyInputModelElement1, com.gs.gapp.metamodel.converter.sampleclasses.MyModelElement1)
	 */
	@Override
	protected void onConvert(S originalModelElement,
			T resultingModelElement) {
		
		super.onConvert(originalModelElement, resultingModelElement);
		
		if (originalModelElement.getRelatedInputModelelement() != null) {
			MyModelElement myModelElement =
				convertWithOtherConverter(MyModelElement.class,
						                  originalModelElement.getRelatedInputModelelement());
			
			resultingModelElement.setRelatedElement(myModelElement);
		}
	}
}
