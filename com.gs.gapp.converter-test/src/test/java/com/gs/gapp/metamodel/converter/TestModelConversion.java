/**
 * 
 */
package com.gs.gapp.metamodel.converter;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterOptions;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.gs.gapp.metamodel.converter.sampleclasses.MyConverter1;
import com.gs.gapp.metamodel.converter.sampleclasses.MyConverter2;
import com.gs.gapp.metamodel.converter.sampleclasses.MyInputModelElement1;
import com.gs.gapp.metamodel.converter.sampleclasses.MyInputModelElement2;
import com.gs.gapp.metamodel.converter.sampleclasses.MySubclassInputModelElement1;
import com.gs.gapp.metamodel.converter.sampleclasses.MySubclassInputModelElement2;

/**
 * @author mmt
 *
 */
public class TestModelConversion {
	
	private MyConverter1 myConverter1;
	private ModelConverterOptions modelConverterOptions1;
	
	private MyConverter2 myConverter2;
	private ModelConverterOptions modelConverterOptions2;
	
	@BeforeClass
	public void setUp() {
		this.myConverter1 = new MyConverter1();
		this.myConverter2 = new MyConverter2();
	}

	@Test
	public void testConversion1() {
		Set<Serializable> inputElements = new LinkedHashSet<>();
		
		MyInputModelElement1 inputModelElementNumber1 = new MyInputModelElement1("test1: input model element #1");
		inputElements.add(inputModelElementNumber1);
		MyInputModelElement1 inputModelElementNumber2 = new MyInputModelElement1("test1: input model element #2");
		inputModelElementNumber2.setRelatedInputModelelement(inputModelElementNumber1);
		inputElements.add(inputModelElementNumber2);
		
		inputElements.add(new MySubclassInputModelElement1("test1: input model element #1"));
		inputElements.add(new MySubclassInputModelElement1("test1: input model element #2"));
		
		Set<Object> conversionResult =
			myConverter1.clientConvert(inputElements, modelConverterOptions1);
		Assert.assertNotNull(conversionResult);
		Assert.assertFalse(conversionResult.isEmpty());
	}
	
	@Test
	public void testConversion2() {
		Set<Serializable> inputElements = new LinkedHashSet<>();
		
		inputElements.add(new MyInputModelElement2("test2: input model element #1", new Date()));
		inputElements.add(new MyInputModelElement2("test2: input model element #2", new Date()));
		inputElements.add(new MySubclassInputModelElement2("test2: input model element #1", new Date()));
		inputElements.add(new MySubclassInputModelElement2("test2: input model element #2", new Date()));
		
		Set<Object> conversionResult =
		    myConverter2.clientConvert(inputElements, modelConverterOptions2);
		Assert.assertNotNull(conversionResult);
		Assert.assertFalse(conversionResult.isEmpty());
	}
}
