/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;

/**
 * @author mmt
 *
 */
public class MyInputModelElement1 extends MyInputModelElement {
	
	private MyInputModelElement relatedInputModelelement;

	/**
	 * 
	 */
	private static final long serialVersionUID = 4028146293930177844L;

	/**
	 * @param name
	 */
	public MyInputModelElement1(String name) {
		super(name);
	}

	/**
	 * @return the relatedInputModelelement
	 */
	public final MyInputModelElement getRelatedInputModelelement() {
		return relatedInputModelelement;
	}

	/**
	 * @param relatedInputModelelement the relatedInputModelelement to set
	 */
	public final void setRelatedInputModelelement(
			MyInputModelElement relatedInputModelelement) {
		this.relatedInputModelelement = relatedInputModelelement;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return super.toString() + " - " + (getRelatedInputModelelement() == null ? "no-related-input-model-element" : getRelatedInputModelelement().toString());
	}
}
