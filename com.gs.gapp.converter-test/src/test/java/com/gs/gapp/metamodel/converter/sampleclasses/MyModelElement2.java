/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;


/**
 * @author mmt
 *
 */
public class MyModelElement2 extends MyModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3861253279819981541L;

	/**
	 * @param name
	 */
	public MyModelElement2(String name, String comment) {
		super(name, comment);
	}

}
