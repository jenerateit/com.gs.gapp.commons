/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;

import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.converter.AbstractConverter;

/**
 * @author mmt
 *
 */
public abstract class MyConverter extends AbstractConverter {

	/**
	 * 
	 */
	public MyConverter() {
		super(new ModelElementCache());
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.GAppAbstractConverter#getInputMetaModelVersion()
	 */
	@Override
	public String getInputMetaModelVersion() {
        return "0.0.1";
	}

	
}
