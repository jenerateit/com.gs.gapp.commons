/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * @author mmt
 *
 */
public class MyModelElement extends ModelElement {

	private final String comment;
	
	private MyModelElement relatedElement;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7818183872832989782L;

	/**
	 * @param name
	 */
	public MyModelElement(String name, String comment) {
		super(name);
		this.comment = comment;
	}

	/**
	 * @return the comment
	 */
	public final String getComment() {
		return comment;
	}

	/**
	 * @return the relatedElement
	 */
	protected final MyModelElement getRelatedElement() {
		return relatedElement;
	}

	/**
	 * @param relatedElement the relatedElement to set
	 */
	protected final void setRelatedElement(MyModelElement relatedElement) {
		this.relatedElement = relatedElement;
	}
	
	
}
