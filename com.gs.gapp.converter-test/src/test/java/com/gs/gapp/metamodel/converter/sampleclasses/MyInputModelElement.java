/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;

import java.io.Serializable;

/**
 * @author mmt
 *
 */
public class MyInputModelElement implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1505707455031176104L;
	
	private final String name;

	/**
	 * @param name
	 */
	public MyInputModelElement(String name) {
		super();
		this.name = name;
	}

	/**
	 * @return the name
	 */
	protected final String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getName();
	}
}
