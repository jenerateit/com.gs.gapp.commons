/**
 * 
 */
package com.gs.gapp.metamodel.converter.sampleclasses;


/**
 * @author mmt
 *
 */
public class MySubclassModelElement2 extends MyModelElement2 {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7818183872832989782L;

	/**
	 * @param name
	 */
	public MySubclassModelElement2(String name, String comment) {
		super(name, comment);
	}
}
