/**
 * 
 */
package com.gs.gapp.generation.anyfile;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.anyfile.AnyFile;

/**
 * @author mmt
 *
 */
public class AnyFileWriter extends AbstractWriter {
	
	@ModelElement
	private AnyFile file;

	/**
	 * 
	 */
	public AnyFileWriter() {}

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
    public void transform(TargetSection ts) throws WriterException {
		if (file.getContent() == null || file.getContent().length == 0) throw new NullPointerException("text file object does not have any content (name:" + file.getUri().toString());
		
		write(file.getContent());
	}
}
