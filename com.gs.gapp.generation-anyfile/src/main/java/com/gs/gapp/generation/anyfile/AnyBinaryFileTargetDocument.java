package com.gs.gapp.generation.anyfile;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jenerateit.target.AbstractTargetDocument;
import org.jenerateit.target.TargetSection;

public class AnyBinaryFileTargetDocument extends AbstractTargetDocument {

	private static final long serialVersionUID = 1212115583695943479L;
	
	public static final TargetSection DOCUMENT = new TargetSection("document", 10);

	private static final SortedSet<TargetSection> SECTIONS = new TreeSet<>(
			Arrays.asList(new TargetSection[] {
					DOCUMENT
			}));
	
	@Override
	public SortedSet<TargetSection> getTargetSections() {
		return SECTIONS;
	}

}
