package com.gs.gapp.generation.anyfile;

import java.net.URI;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTarget;

import com.gs.gapp.metamodel.anyfile.AnyFile;

public class AnyBinaryFileTarget extends AbstractTarget<AnyBinaryFileTargetDocument> {

	@ModelElement
	private AnyFile binaryFile;
	
	@Override
	protected URI getTargetURI() {
		return binaryFile.getUri();
	}

}
