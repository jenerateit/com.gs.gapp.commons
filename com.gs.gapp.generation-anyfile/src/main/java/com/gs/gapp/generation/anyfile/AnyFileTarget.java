/**
 * 
 */
package com.gs.gapp.generation.anyfile;

import java.net.URI;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTextTarget;

import com.gs.gapp.metamodel.anyfile.AnyFile;

/**
 * @author mmt
 *
 */
public class AnyFileTarget extends AbstractTextTarget<AnyFileTargetDocument> {
	
	
	@ModelElement
	private AnyFile textFile;

	@Override
	protected URI getTargetURI() {
		return textFile.getUri();
	}
}
