/**
 * 
 */
package com.gs.gapp.generation.anyfile;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupProviderI;

/**
 * @author mmt
 *
 */
public class GenerationGroupAnyFileProvider implements GenerationGroupProviderI {

	/**
	 * 
	 */
	public GenerationGroupAnyFileProvider() {}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.GenerationGroupProviderI#getGenerationGroupConfig()
	 */
	@Override
	public GenerationGroupConfigI getGenerationGroupConfig() {
		return new GenerationGroupAnyFile();
	}
}
