package com.gs.gapp.generation.anyfile;

import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.WriterLocatorI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.basic.AbstractGenerationGroup;
import com.gs.gapp.metamodel.anyfile.AnyFile;
import com.gs.gapp.metamodel.basic.ModelElementCache;

public class GenerationGroupAnyFile extends AbstractGenerationGroup implements GenerationGroupConfigI {

	public GenerationGroupAnyFile() {
		super(new ModelElementCache());
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement,
			Class<? extends TargetI<?>> targetClass) {
		if (modelElement instanceof AnyFile) {
			
			AnyFile anyFile = (AnyFile) modelElement;
			if (anyFile.isBinary() && targetClass == AnyBinaryFileTarget.class) {
			    return AnyFileWriter.class;
			}
			else if (!anyFile.isBinary() && targetClass == AnyFileTarget.class) {
			    return AnyFileWriter.class;
			}
		}
		return null;
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement, TargetI<?> targetInstance) {
		return AnyFileWriter.class;
	}

	@Override
	public Set<Class<? extends TargetI<?>>> getAllTargets() {
		Set<Class<? extends TargetI<?>>> result = new LinkedHashSet<>();
		result.add(AnyFileTarget.class);
		result.add(AnyBinaryFileTarget.class);
		return result;
	}

	@Override
	public WriterLocatorI getWriterLocator() {
		return null;
	}
}
