package com.gs.gapp.generation.analytics.writer;

import com.gs.gapp.generation.analytics.GenerationGroupAnalyticsOptions;
import com.gs.gapp.generation.basic.writer.ModelElementWriter;

public abstract class AbstractAnalyticsWriter extends ModelElementWriter {

	private GenerationGroupAnalyticsOptions generationGroupOptions;

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.basic.writer.ModelElementWriter#getGenerationGroupOptions()
	 */
	@Override
	public GenerationGroupAnalyticsOptions getGenerationGroupOptions() {
		if (this.generationGroupOptions == null) {
			this.generationGroupOptions = new GenerationGroupAnalyticsOptions(this);
		}
		return generationGroupOptions;
	}
}
