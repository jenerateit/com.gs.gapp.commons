package com.gs.gapp.generation.analytics.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetException;

import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;

public class ElementConverterConfigurationTreeVisualizationTextTarget extends BasicTextTarget<TextTargetDocument> {

	@ModelElement
	private ElementConverterConfigurationTreeNode rootNode;

	@Override
	public URI getTargetURI() {
		StringBuilder sb = new StringBuilder(getTargetRoot()).append("/vd-reports").append("/").append(getTargetPrefix()).append("/").append(rootNode.getName().replace(".", "_").replace(" ", "_")).append(".txt");
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw createTargetException(e, this, rootNode);
		}
	}

}
