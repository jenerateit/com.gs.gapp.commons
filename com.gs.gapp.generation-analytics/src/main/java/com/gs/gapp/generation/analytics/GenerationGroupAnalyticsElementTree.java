package com.gs.gapp.generation.analytics;

import org.jenerateit.generationgroup.WriterLocatorI;

import com.gs.gapp.generation.anyfile.AnyFileTarget;
import com.gs.gapp.generation.basic.AbstractGenerationGroup;
import com.gs.gapp.metamodel.basic.ModelElementCache;

/**
 * This generation group creates files for the visualization of the
 * inner workings of generators. The visualization shows the so-called
 * "model element tree", where the vertexes represent instances of metatypes
 * and the edges represent model element converters.
 * 
 * @author mmt
 *
 */
public class GenerationGroupAnalyticsElementTree extends AbstractGenerationGroup {

	private WriterLocatorI writerLocator;

	public GenerationGroupAnalyticsElementTree() {
		super(new ModelElementCache());
		
		if (com.gs.gapp.metamodel.basic.ModelElement.isAnalyticsMode()) {
	//		addTargetClass(ModelElementTreeVisualizationXMLTarget.class);
	//		addTargetClass(ModelElementTreeVisualizationTextTarget.class);
	//		addTargetClass(ModelElementTreeVisualizationDOTTarget.class);
	//		addTargetClass(ModelElementTreeVisualizationDOTBatchTarget.class);
			
			addTargetClass(AnyFileTarget.class);
		}
		
		writerLocator = new WriterLocatorAnalyticsElementTree(getAllTargets());
	}

	@Override
	public String getName() {
		return getClass().getSimpleName();
	}

	@Override
	public String getDescription() {
		return "Generates files that contain analytical data related to the model element tree";
	}

	@Override
	public WriterLocatorI getWriterLocator() {
		return writerLocator;
	}
}
