package com.gs.gapp.generation.analytics.writer;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.DoNotWriteException;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.GenerationGroupTargetConfiguration;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;

public class ElementConverterConfigurationFlatTreeVisualizationDOTWriter extends AbstractElementConverterConfigurationVisualizationDOTWriter {

	@ModelElement
	private ElementConverterConfigurationTreeNode rootNode;

	private HashMap<ElementConverterConfigurationTreeNode, LinkedHashSet<GenerationGroupTargetConfiguration>> map  =
			new HashMap<>();
	
	private HashSet<String> mapWrittenConfig  = new HashSet<>();
	
	private boolean logVisualizationData = false;

	/**
	 * @param node
	 */
	protected void traverseNode(ElementConverterConfigurationTreeNode node) {
		
		if (logVisualizationData) {
			if (node.getElementConverterConfiguration() != null && node.getElementConverterConfiguration().getModelConverterConfiguration() != null) {
				TransformationStepConfiguration transformationStepConfiguration = node.getElementConverterConfiguration().getModelConverterConfiguration();
				for (TransformationStepConfiguration childTransStepConfig : transformationStepConfiguration.getChildConfigurations()) {
					if (childTransStepConfig.getTargetConfigurations().size() > 0) {
						// the transformationStepConfiguration relates to a generation group that has targets configured
						for (GenerationGroupTargetConfiguration genGroupTargetConf : childTransStepConfig.getTargetConfigurations()) {
							if (node.getElementConverterConfiguration().getTargetClass() == genGroupTargetConf.getMetaModelClass()) {
								// ---------------------------------------------------------------------------------------------------
								// --- found a target class that might cause a file generation for the given transformation step tree
								ElementConverterConfigurationTreeNode rootNodeForBranch = null;
								Class<?> contextSourceClass = getContextSourceClass();
								if (contextSourceClass != null) {
									rootNodeForBranch = node.getLeadingNode(getElementConverterVisualizationContext());
								}
							
								if (rootNodeForBranch != null) {
								    addTarget(rootNodeForBranch, genGroupTargetConf);
								}
							}
						}
					}
				}
			}
		}

		for (ElementConverterConfigurationTreeNode childTreeNode : node.getChildren()) {
			if (ignoreNode(childTreeNode)) {
				continue;
			}
			boolean enabledLoggingOfVisualizationData = false;
			if (logVisualizationData == false && childTreeNode.featuresSourceClass(getContextSourceClass())) {
				logVisualizationData = true;
				enabledLoggingOfVisualizationData = true;
			}
			traverseNode(childTreeNode);
			if (enabledLoggingOfVisualizationData) {
				logVisualizationData = false;
			}
		}
	}

	/**
	 * @param rootNodeForBranch
	 * @param genGroupTargetConf
	 */
	private void addTarget(
			ElementConverterConfigurationTreeNode rootNodeForBranch,
			GenerationGroupTargetConfiguration genGroupTargetConf) {
		
		if ((genGroupTargetConf.getTargetClass().getModifiers() & Modifier.ABSTRACT) != 0) {
			return;  // ignore abstract target classes
		}
		
		LinkedHashSet<GenerationGroupTargetConfiguration> targetConfigurations = null;
		
        if (map.containsKey(rootNodeForBranch)) {
        	targetConfigurations = map.get(rootNodeForBranch);
        } else {
        	targetConfigurations = new LinkedHashSet<>();
        	map.put(rootNodeForBranch, targetConfigurations);
        }
        
        targetConfigurations.add(genGroupTargetConf);
	}

	/**
	 * 
	 */
	private void writeTree() {
		for (ElementConverterConfigurationTreeNode elementConvConfTreeNode : map.keySet()) {
			LinkedHashSet<GenerationGroupTargetConfiguration> targetConfigurations = map.get(elementConvConfTreeNode);
			for (GenerationGroupTargetConfiguration genGroupTargetConf : targetConfigurations) {
				
				StringBuilder sb = new StringBuilder(elementConvConfTreeNode.getSourceId()).append(" -> ").append(genGroupTargetConf.getTargetId());
				if (mapWrittenConfig.contains(sb.toString()) == false) {
					wNL(sb);
					mapWrittenConfig.add(sb.toString());
					if (elementConvConfTreeNode.getSourceLabel() != null) {
					    wNL(elementConvConfTreeNode.getSourceId(), " [label = \"", elementConvConfTreeNode.getSourceLabel(), "\"]");
					}
					wNL(genGroupTargetConf.getTargetId(), " [shape = note, style = filled, fillcolor = \"#eff3ff\"]");
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		// there is only one target section, thus we do not need the if-else construct to differentiate the sections
		wNLI("digraph elementtree {");
		    wNL("rankdir = LR");
		    wNL("node[fontsize=10, fontname=\"helvetica\"]");
		    wNL("edge[fontsize=8, fontname=\"helvetica\"]");
		    wNL("graph[fontsize=10, fontname=\"helvetica bold\"]");
		    traverseNode(rootNode);
		    writeTree();
		wONL("}");
		
		if (mapWrittenConfig.isEmpty()) {
			throw new DoNotWriteException("converter configuration tree would be empty - nothing to be generated");
		}
	}
}
