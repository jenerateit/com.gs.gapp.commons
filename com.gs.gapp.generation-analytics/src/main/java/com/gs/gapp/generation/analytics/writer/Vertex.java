package com.gs.gapp.generation.analytics.writer;

public class Vertex {

	private final String id;
	private final String name;
	private String styles;

	public Vertex(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

	public String getStyles() {
		return styles;
	}

	public void setStyles(String styles) {
		this.styles = styles;
	}
}
