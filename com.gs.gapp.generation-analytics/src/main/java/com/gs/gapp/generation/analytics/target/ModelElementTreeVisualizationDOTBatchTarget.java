package com.gs.gapp.generation.analytics.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetI;

import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.metamodel.analytics.ModelElementTreeNode;

public class ModelElementTreeVisualizationDOTBatchTarget extends BasicTextTarget<TextTargetDocument> {

	@ModelElement
	private ModelElementTreeNode rootNode;

	@Override
	public URI getTargetURI() {
		StringBuilder sb = new StringBuilder(getTargetRoot()).append("/vd-reports").append("/").append("element-trees").append("/").append("execdot.bat");
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw createTargetException(e, this, rootNode);
		}
	}

}
