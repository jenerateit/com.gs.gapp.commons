package com.gs.gapp.generation.analytics.target;

import org.jenerateit.annotation.Context;

@Context(provider=ElementConverterSubNodesContextProvider.class)
public class ElementConverterConfigurationTreeVisualizationDOTTarget extends AbstractElementConverterConfigurationVisualizationDOTTarget {

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.analytics.target.AbstractElementConverterConfigurationVisualizationDOTTarget#getPathPrefix()
	 */
	@Override
	protected String getPathPrefix() {
		
		return "full-conversion-tree/";
	}
}
