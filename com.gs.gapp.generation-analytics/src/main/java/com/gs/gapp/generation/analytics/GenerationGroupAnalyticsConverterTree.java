package com.gs.gapp.generation.analytics;

import org.jenerateit.generationgroup.WriterLocatorI;

import com.gs.gapp.generation.analytics.target.ElementConverterConfigurationFlatTreeVisualizationMxGraphHtmlTarget;
import com.gs.gapp.generation.analytics.target.ElementConverterConfigurationFlatTreeVisualizationMxGraphJavaScriptTarget;
import com.gs.gapp.generation.analytics.target.ElementConverterConfigurationTreeVisualizationMxGraphHtmlTarget;
import com.gs.gapp.generation.analytics.target.ElementConverterConfigurationTreeVisualizationMxGraphJavaScriptTarget;
import com.gs.gapp.generation.basic.AbstractGenerationGroup;
import com.gs.gapp.metamodel.basic.ModelElementCache;

/**
 * This generation group creates files for the visualization of the
 * inner workings of generators. The output shows how the model
 * element converters work together, in a chain of element conversions.
 * 
 * @author mmt
 *
 */
public class GenerationGroupAnalyticsConverterTree extends AbstractGenerationGroup {

	private WriterLocatorI writerLocator;

	public GenerationGroupAnalyticsConverterTree() {
		super(new ModelElementCache());
		
		if (com.gs.gapp.metamodel.basic.ModelElement.isAnalyticsMode()) {
	//		addTargetClass(ElementConverterConfigurationTreeVisualizationTextTarget.class);
	//		
	//		addTargetClass(ElementConverterConfigurationFlatTreeVisualizationDOTTarget.class);
	//		addTargetClass(ElementConverterConfigurationFlatTreeVisualizationDOTBatchTarget.class);
	//		
	//		addTargetClass(ElementConverterConfigurationTreeVisualizationDOTTarget.class);
	//		addTargetClass(ElementConverterConfigurationTreeVisualizationDOTBatchTarget.class);
			
			addTargetClass(ElementConverterConfigurationTreeVisualizationMxGraphJavaScriptTarget.class);
			addTargetClass(ElementConverterConfigurationTreeVisualizationMxGraphHtmlTarget.class);
			
			addTargetClass(ElementConverterConfigurationFlatTreeVisualizationMxGraphJavaScriptTarget.class);
			addTargetClass(ElementConverterConfigurationFlatTreeVisualizationMxGraphHtmlTarget.class);
		}
		writerLocator = new WriterLocatorAnalyticsConverterTree(getAllTargets());
	}

	@Override
	public String getName() {
		return getClass().getSimpleName();
	}

	@Override
	public String getDescription() {
		return getClass().getName();
	}

	@Override
	public WriterLocatorI getWriterLocator() {
		return writerLocator;
	}
}
