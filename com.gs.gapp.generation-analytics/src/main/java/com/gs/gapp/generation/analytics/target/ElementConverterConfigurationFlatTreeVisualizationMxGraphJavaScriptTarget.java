package com.gs.gapp.generation.analytics.target;

import org.jenerateit.annotation.Context;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;

@Context(provider=ElementConverterSubNodesContextProvider.class)
public class ElementConverterConfigurationFlatTreeVisualizationMxGraphJavaScriptTarget extends AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptTarget {

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.analytics.target.AbstractElementConverterConfigurationVisualizationDOTTarget#getPathPrefix()
	 */
	@Override
	protected String getPathPrefix() {
		
		return "flat-conversion-tree/";
	}
	
	public static class ElementConverterConfigurationFlatTreeVisualizationMxGraphJavaScriptWriter extends AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptWriter {

		@ModelElement
		private ElementConverterConfigurationTreeNode rootNode;
		
		/* (non-Javadoc)
		 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
		 */
		@Override
		public void transform(TargetSection ts) {
			// there is only one target section, thus we do not need the if-else construct to differentiate the sections
			
			wNL("TODO");
		}
		
	}
}
