package com.gs.gapp.generation.analytics.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.analytics.ElementConverterConfiguration;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;

public class ElementConverterConfigurationTreeVisualizationTextWriter extends AbstractAnalyticsWriter {

	@ModelElement
	private ElementConverterConfigurationTreeNode rootNode;

	protected void writeNode(ElementConverterConfigurationTreeNode node) {

        wElementConverterConfigurationInfo(node.getElementConverterConfiguration());

		if (node.isRoot()) {
			wNL();
		}

		if (node.getChildren().size() > 0) {
			indent(6);
			for (ElementConverterConfigurationTreeNode childTreeNode : node.getChildren()) {
				if (node.isRoot()) {
					wNL("----------------------------------------------------------------------------");
				}
				writeNode(childTreeNode);
			}
			outdent(6);
		} else {
			indent(6);
			//writeTargets(node);
			outdent(6);
            wNL();
		}
	}

	private void wElementConverterConfigurationInfo(ElementConverterConfiguration elementConverterConfiguration) {
		String source = "-";
		String target = "-";
		String converter = "-";

		if (elementConverterConfiguration != null) {
			source = elementConverterConfiguration.getSimpleSourceClassName();
			target = elementConverterConfiguration.getSimpleTargetClassName();
			if (source.equals(target)) {
				source = elementConverterConfiguration.getQualifiedSourceClassName();
				target = elementConverterConfiguration.getQualifiedTargetClassName();
			}
			converter = elementConverterConfiguration.getElementConverterClassName();

			wNL(source, " --> ", target, " (", converter, (elementConverterConfiguration.isIsResponsibleOverwritten() ? " *" : ""), ")");
		}
	}

	protected void writeTargets(ElementConverterConfigurationTreeNode leafNode) {
		/*
		if (leafNode != null && leafNode.getModelElement() != null) {
			wNL("**************************************************");
	    	@SuppressWarnings("unchecked")
			Set<TargetI<? extends TargetDocumentI>> targets = findTargets(FindTargetScope.PROJECT, leafNode.getModelElement(), (Class<? extends TargetI<?>>) AbstractTarget.class, AbstractWriter.class);
	    	if (targets != null && targets.size() > 0) {
		    	for (TargetI<? extends TargetDocumentI> target : targets) {
		    		wNL("TARGET: ", target.getTargetURI().toString());
		    		wNL("TARGET-CLASS: ", target.getClass().getName());
		    	}
	    	} else {
	    		wNL("*** NO TARGETS FOUND");
	    	}
		}
		*/
	}

	@Override
	public void transform(TargetSection ts) {
		// there is only one target section, thus we do not need the if-else construct to differentiate the sections
		writeNode(rootNode);
	}
}
