package com.gs.gapp.generation.analytics;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.jenerateit.writer.AbstractWriter;

import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.metamodel.basic.options.GenerationGroupOptions;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionLong;

public class GenerationGroupAnalyticsOptions extends GenerationGroupOptions {
	
	public static final OptionDefinitionLong OPTION_DEF_ANALYTICS_ROOT_LEVEL =
			new OptionDefinitionLong("analytics-root-level", "the level in the tree where the analysis commences, 1 means the root level", 1L);

	public GenerationGroupAnalyticsOptions(AbstractWriter writer) {
		super(writer);
	}

	public GenerationGroupAnalyticsOptions(BasicTextTarget<?> target) {
		super(target);
	}
	
	/**
	 * TODO This option is not used at the moment. We have implemented a way for transformation analysis, that does not need an explicit setting of a root level. Check whether it is ok to delete this code here. (mmt 20-Dec-2013)
	 * @return
	 * 
	 */
	public int getAnalyticsRootLevel() {
		Serializable rawOption = getOptionValue(OptionDefinitionEnum.OPTION_ANALYTICS_ROOT_LEVEL.getName());
		validateBooleanOption(rawOption, OptionDefinitionEnum.OPTION_ANALYTICS_ROOT_LEVEL.getName());
        Long analyticsRootLevel = OPTION_DEF_ANALYTICS_ROOT_LEVEL.convertToTypedOptionValue(rawOption);
		return analyticsRootLevel.intValue();
	}
	
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.GenerationGroupOptions#getOptionDefinitions()
	 */
	@Override
	public Set<OptionDefinition<? extends Serializable>> getOptionDefinitions() {
		Set<OptionDefinition<? extends Serializable>> result = super.getOptionDefinitions();
		result.addAll(OptionDefinitionEnum.getDefinitions());
		return result;
	}




	/**
	 * @author mmt
	 *
	 */
	public static enum OptionDefinitionEnum {

		
		OPTION_ANALYTICS_ROOT_LEVEL( OPTION_DEF_ANALYTICS_ROOT_LEVEL ),
		;

		private static final Map<String, OptionDefinitionEnum> stringToEnum = new HashMap<>();

		static {
			for (OptionDefinitionEnum m : values()) {
				stringToEnum.put(m.getName(), m);
			}
		}
		
		/**
		 * @return
		 */
		public static Set<OptionDefinition<? extends Serializable>> getDefinitions() {
			Set<OptionDefinition<? extends Serializable>> result = new LinkedHashSet<>();
			for (OptionDefinitionEnum m : values()) {
				result.add(m.getDefinition());
			}
			return result;
		}

		/**
		 * @param datatypeName
		 * @return
		 */
		public static OptionDefinitionEnum fromString(String datatypeName) {
			return stringToEnum.get(datatypeName);
		}

		private final OptionDefinition<? extends Serializable> definition;
		
		private OptionDefinitionEnum(OptionDefinition<? extends Serializable> definition) {
			this.definition = definition;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return this.getDefinition().getName();
		}

		public OptionDefinition<? extends Serializable> getDefinition() {
			return definition;
		}
	}
}
