package com.gs.gapp.generation.analytics;

import java.util.Set;

import org.jenerateit.target.TargetI;

import com.gs.gapp.generation.anyfile.AnyFileTarget;
import com.gs.gapp.generation.anyfile.AnyFileWriter;
import com.gs.gapp.generation.basic.AbstractWriterLocator;
import com.gs.gapp.generation.basic.WriterMapper;
import com.gs.gapp.metamodel.analytics.MxGraphJavaScriptLib;

public class WriterLocatorAnalyticsElementTree extends AbstractWriterLocator {

	public WriterLocatorAnalyticsElementTree(Set<Class<? extends TargetI<?>>> targetClasses) {
		super(targetClasses);

		if (com.gs.gapp.metamodel.basic.ModelElement.isAnalyticsMode()) {
	//		addWriterMapperForGenerationDecision( new WriterMapper(ModelElementTreeNode.class,
	//				ModelElementTreeVisualizationTextTarget.class,
	//				ModelElementTreeVisualizationTextWriter.class) );
	
	//		addWriterMapperForGenerationDecision( new WriterMapper(ModelElementTreeNode.class,
	//				ModelElementTreeVisualizationDOTTarget.class,
	//				ModelElementTreeVisualizationDOTWriter.class) );
	//
	//		addWriterMapperForGenerationDecision( new WriterMapper(ModelElementTreeNode.class,
	//				ModelElementTreeVisualizationDOTBatchTarget.class,
	//				ModelElementTreeVisualizationDOTBatchWriter.class) );
			
			addWriterMapperForGenerationDecision( new WriterMapper(MxGraphJavaScriptLib.class,
					AnyFileTarget.class,
					AnyFileWriter.class) );
		}
	}
}
