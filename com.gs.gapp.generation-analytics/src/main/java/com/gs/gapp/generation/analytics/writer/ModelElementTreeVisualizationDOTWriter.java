package com.gs.gapp.generation.analytics.writer;

import java.util.HashMap;
import java.util.List;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.DoNotWriteException;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.analytics.target.ModelElementTreeGenerationContext;
import com.gs.gapp.generation.analytics.target.ModelElementTreeVisualizationDOTTarget;
import com.gs.gapp.metamodel.analytics.ModelElementTreeNode;
import com.gs.gapp.metamodel.basic.ModelElementWrapper;
import com.gs.gapp.metamodel.basic.TargetElement;

public class ModelElementTreeVisualizationDOTWriter extends AbstractAnalyticsWriter {

	public static final String CONTEXT_TARGETS_ONLY = "CONTEXT_TARGETS_ONLY";
	public static final String CONTEXT_ALL = "CONTEXT_ALL";

	@ModelElement
	private ModelElementTreeNode rootNode;

	private HashMap<String, ModelElementTreeNode> map = new HashMap<>();

	private ModelElementTreeGenerationContext getContext() {
		return ((ModelElementTreeVisualizationDOTTarget)getTransformationTarget()).getContext();
	}

	protected void writeNode(ModelElementTreeNode node) {


		for (ModelElementTreeNode childTreeNode : node.getChildren()) {

			if (node.isRoot() && getContext().getRootElement() != null && childTreeNode.getModelElement() != getContext().getRootElement()) {
				continue;  // in this case we are just generating the tree for a single root element
			}

			if (childTreeNode.getModelElement() != null && childTreeNode.getModelElement() instanceof com.gs.gapp.metamodel.basic.ModelElement && !(childTreeNode.getModelElement() instanceof TargetElement)) {
				com.gs.gapp.metamodel.basic.ModelElement modelElement = (com.gs.gapp.metamodel.basic.ModelElement) childTreeNode.getModelElement();
				List<TargetElement> allExtensionElementsWithTargets = modelElement.getAllExtensionElementDeeply(TargetElement.class, true);
				if (CONTEXT_TARGETS_ONLY.equals(getContext().getType()) && allExtensionElementsWithTargets.size() <= 0) {  // only show branches that lead to the generation/loading of a target
					continue;
				}
			}

			String converterName = "-";
			if (childTreeNode.getModelElement() != null && childTreeNode.getModelElement().getConversionDetails() != null) {
				String elementConverterClassname = childTreeNode.getModelElement().getConversionDetails().getModelElementConverterClassname();
				if (elementConverterClassname != null) {
				    elementConverterClassname = elementConverterClassname.substring(elementConverterClassname.lastIndexOf(".")+1);
				} else {
					elementConverterClassname = "-";
				}
				String modelConverterClassname = childTreeNode.getModelElement().getConversionDetails().getModelConverterClassName();
				modelConverterClassname = modelConverterClassname.substring(modelConverterClassname.lastIndexOf(".")+1);

				converterName = elementConverterClassname + "\n" + modelConverterClassname;
			}

			if (node.isRoot() == false) {
				map.put(node.getId(), node);
				map.put(childTreeNode.getId(), childTreeNode);
			    wNL(node.getId(), " -> ", childTreeNode.getId(), "[label = \"", converterName, "\"]");
			}
			writeNode(childTreeNode);
		}
	}

	@Override
	public void transform(TargetSection ts) {
		// there is only one target section, thus we do not need the if-else construct to differentiate the sections

		if (rootNode.getChildren() == null || rootNode.getChildren().size() == 0) {
			throw new DoNotWriteException("there is not more than a single root node '" + rootNode.getName() + "' for the model element transformation tree, so we are not generating a file");
		}

		try {
		wNLI("digraph elementtree {");
		    wNL("rankdir = LR");
		    writeNode(rootNode);
		    wNL();
		    writeNodeLabels(rootNode);
		wONL("}");
		} catch (Throwable th) {
			th.printStackTrace();
		}
	}

	private void writeNodeLabels(ModelElementTreeNode node) {

		String typeName = "-";
		if (node.getModelElement() != null) {
			if (node.getModelElement() instanceof ModelElementWrapper) {
				ModelElementWrapper modelElementWrapper = (ModelElementWrapper) node.getModelElement();
				typeName = modelElementWrapper.getType().replace("$", "_").replace(".", "_");
			} else {
				typeName = node.getModelElement().getClass().getSimpleName().replace("$", "_");
			}
		}

		if (node.isRoot() == false) {
			if (node.getModelElement() != null && map.containsKey(node.getId())) {
				String name = node.getModelElement().getName();
				if (node.getModelElement() instanceof TargetElement) {
					TargetElement targetElement = (TargetElement) node.getModelElement();

					String status = null;
					switch (targetElement.getTargetStatus()) {
					case FOUND:
						status = "found";
						break;
					case LOADED:
						status = "loaded";
						break;
					case TRANSFORMED:
						status = "transformed";
						break;
					default:
                        throw new JenerateITException("found unhandled target status '" + targetElement.getTargetStatus() + "'");
					}
					name = new StringBuilder(targetElement.getName().substring(targetElement.getName().lastIndexOf("/")+1)).append(" (").append(status).append(")").toString();
				}

				String fillColor = null;
				if (node.getModelElement() instanceof TargetElement) {
					@SuppressWarnings("unused")
					TargetElement targetElement = (TargetElement) node.getModelElement();
					fillColor = "style = filled, fillcolor = green";
				}

				wNL(node.getId(), "[label = \"", name, "\n (", typeName, ")", "\"" + (fillColor == null ? "" : ", "+fillColor) + "]");
			} else if (node.isRoot()) {
//				wNL(node.getId(), "[label = \"root\"]");
				wNL(node.getId(), "[label = \"", node.getModelElement().getName(), "\n (", typeName, ")", "\"]");
			}
		}


		for (ModelElementTreeNode childTreeNode : node.getChildren()) {
			writeNodeLabels(childTreeNode);
		}
	}
}
