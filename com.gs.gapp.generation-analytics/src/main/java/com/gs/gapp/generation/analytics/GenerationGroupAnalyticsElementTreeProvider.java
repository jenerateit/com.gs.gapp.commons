/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.generation.analytics;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupProviderI;
import org.osgi.service.component.annotations.Component;


/**
 * @author hrr
 *
 */
@Component
public class GenerationGroupAnalyticsElementTreeProvider implements GenerationGroupProviderI {

	/**
	 *
	 */
	public GenerationGroupAnalyticsElementTreeProvider() {
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.GenerationGroupProviderI#getGenerationGroupConfig()
	 */
	@Override
	public GenerationGroupConfigI getGenerationGroupConfig() {
		return new GenerationGroupAnalyticsElementTree();
	}

}
