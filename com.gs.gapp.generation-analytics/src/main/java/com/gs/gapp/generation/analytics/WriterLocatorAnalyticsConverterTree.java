package com.gs.gapp.generation.analytics;

import java.util.Set;

import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.analytics.target.ElementConverterConfigurationFlatTreeVisualizationMxGraphHtmlTarget;
import com.gs.gapp.generation.analytics.target.ElementConverterConfigurationFlatTreeVisualizationMxGraphHtmlTarget.ElementConverterConfigurationFlatTreeVisualizationMxGraphHtmlWriter;
import com.gs.gapp.generation.analytics.target.ElementConverterConfigurationFlatTreeVisualizationMxGraphJavaScriptTarget;
import com.gs.gapp.generation.analytics.target.ElementConverterConfigurationFlatTreeVisualizationMxGraphJavaScriptTarget.ElementConverterConfigurationFlatTreeVisualizationMxGraphJavaScriptWriter;
import com.gs.gapp.generation.analytics.target.ElementConverterConfigurationTreeVisualizationMxGraphHtmlTarget;
import com.gs.gapp.generation.analytics.target.ElementConverterConfigurationTreeVisualizationMxGraphHtmlTarget.ElementConverterConfigurationTreeVisualizationMxGraphHtmlWriter;
import com.gs.gapp.generation.analytics.target.ElementConverterConfigurationTreeVisualizationMxGraphJavaScriptTarget;
import com.gs.gapp.generation.analytics.target.ElementConverterConfigurationTreeVisualizationMxGraphJavaScriptTarget.ElementConverterConfigurationTreeVisualizationMxGraphJavaScriptWriter;
import com.gs.gapp.generation.basic.AbstractWriterLocator;
import com.gs.gapp.generation.basic.WriterMapper;
import com.gs.gapp.generation.basic.target.TransformationStepAnalysisTarget;
import com.gs.gapp.generation.basic.target.TransformationStepsAnalyticsTextTarget;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;

public class WriterLocatorAnalyticsConverterTree extends AbstractWriterLocator {

	public WriterLocatorAnalyticsConverterTree(Set<Class<? extends TargetI<?>>> targetClasses) {
		super(targetClasses);
		
		if (com.gs.gapp.metamodel.basic.ModelElement.isAnalyticsMode()) {

	//		addWriterMapperForGenerationDecision( new WriterMapper(ElementConverterConfigurationTreeNode.class,
	//				ElementConverterConfigurationTreeVisualizationTextTarget.class,
	//				ElementConverterConfigurationTreeVisualizationTextWriter.class) );
	//		
	//		addWriterMapperForGenerationDecision( new WriterMapper(ElementConverterConfigurationTreeNode.class,
	//				ElementConverterConfigurationFlatTreeVisualizationDOTTarget.class,
	//				ElementConverterConfigurationFlatTreeVisualizationDOTWriter.class) );
	//		
	//		addWriterMapperForGenerationDecision( new WriterMapper(ElementConverterConfigurationTreeNode.class,
	//				ElementConverterConfigurationFlatTreeVisualizationDOTBatchTarget.class,
	//				ElementConverterConfigurationFlatTreeVisualizationDOTBatchWriter.class) );
	//		
	//
	//		addWriterMapperForGenerationDecision( new WriterMapper(ElementConverterConfigurationTreeNode.class,
	//				ElementConverterConfigurationTreeVisualizationDOTTarget.class,
	//				ElementConverterConfigurationTreeVisualizationDOTWriter.class) );
	//		
	//		addWriterMapperForGenerationDecision( new WriterMapper(ElementConverterConfigurationTreeNode.class,
	//				ElementConverterConfigurationTreeVisualizationDOTBatchTarget.class,
	//				ElementConverterConfigurationTreeVisualizationDOTBatchWriter.class) );
			
			addWriterMapperForGenerationDecision( new WriterMapper(ElementConverterConfigurationTreeNode.class,
					ElementConverterConfigurationTreeVisualizationMxGraphJavaScriptTarget.class,
					ElementConverterConfigurationTreeVisualizationMxGraphJavaScriptWriter.class) );
			addWriterMapperForGenerationDecision( new WriterMapper(ElementConverterConfigurationTreeNode.class,
					ElementConverterConfigurationTreeVisualizationMxGraphHtmlTarget.class,
					ElementConverterConfigurationTreeVisualizationMxGraphHtmlWriter.class) );
			
			addWriterMapperForGenerationDecision( new WriterMapper(ElementConverterConfigurationTreeNode.class,
					ElementConverterConfigurationFlatTreeVisualizationMxGraphJavaScriptTarget.class,
					ElementConverterConfigurationFlatTreeVisualizationMxGraphJavaScriptWriter.class) );
			addWriterMapperForGenerationDecision( new WriterMapper(ElementConverterConfigurationTreeNode.class,
					ElementConverterConfigurationFlatTreeVisualizationMxGraphHtmlTarget.class,
					ElementConverterConfigurationFlatTreeVisualizationMxGraphHtmlWriter.class) );
			
		}
				
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object element, Class<? extends TargetI<?>> targetClass) {
		
		if (targetClass == TransformationStepAnalysisTarget.class || targetClass == TransformationStepsAnalyticsTextTarget.class) {
			return null;
		}
		
		return super.getWriterClass(element, targetClass);
	}
}
