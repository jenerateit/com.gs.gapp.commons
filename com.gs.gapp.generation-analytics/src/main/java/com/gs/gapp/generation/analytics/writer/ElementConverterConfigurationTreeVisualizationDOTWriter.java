package com.gs.gapp.generation.analytics.writer;

import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.DoNotWriteException;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.GenerationGroupTargetConfiguration;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;

public class ElementConverterConfigurationTreeVisualizationDOTWriter extends AbstractElementConverterConfigurationVisualizationDOTWriter {

	@ModelElement
	private ElementConverterConfigurationTreeNode rootNode;
	
	private boolean logVisualizationData = false;
	private boolean somethingGotWritten = false;
	
	private HashSet<String> mapWrittenTargetConfig = new HashSet<>();
	
	private LinkedHashMap<String, LinkedHashSet<String>> subgraphs = new LinkedHashMap<>();
	private LinkedHashMap<String, String> subgraphLabels = new LinkedHashMap<>();
	
	private LinkedHashSet<String> writtenSourceToTargetLines = new LinkedHashSet<>();
	
	/**
	 * @param node
	 */
	private void addNodeToSubgraph(ElementConverterConfigurationTreeNode node) {
		String subgraphId = node.getElementConverterConfiguration().getModelConverterConfiguration().getId();
		LinkedHashSet<String> elementIdsForSubgraph = null;
		if (subgraphs.containsKey(subgraphId)) {
			elementIdsForSubgraph = subgraphs.get(subgraphId);
		} else {
			elementIdsForSubgraph = new LinkedHashSet<>();
			subgraphs.put(subgraphId, elementIdsForSubgraph);
			subgraphLabels.put(subgraphId, node.getElementConverterConfiguration().getModelConverterConfiguration().getName());
		}
		
		elementIdsForSubgraph.add(node.getTargetId());
	}
	
	/**
	 * @param node
	 */
	protected void traverseNode(final ElementConverterConfigurationTreeNode node) {

		if (logVisualizationData) {
			
			if (node.getElementConverterConfiguration() != null && node.getElementConverterConfiguration().getModelConverterConfiguration() != null) {
				TransformationStepConfiguration transformationStepConfiguration = node.getElementConverterConfiguration().getModelConverterConfiguration();
				for (TransformationStepConfiguration childTransStepConfig : transformationStepConfiguration.getChildConfigurations()) {
					if (childTransStepConfig.getTargetConfigurations().size() > 0) {
						// the transformationStepConfiguration relates to a generation group that has targets configured
						for (GenerationGroupTargetConfiguration genGroupTargetConf : childTransStepConfig.getTargetConfigurations()) {
							if ((genGroupTargetConf.getTargetClass().getModifiers() & Modifier.ABSTRACT) != 0) {
								continue;  // ignore abstract target classes
							}
							if (node.getElementConverterConfiguration().getTargetClass() == genGroupTargetConf.getMetaModelClass()) {
								// ---------------------------------------------------------------------------------------------------
								// --- found a target class that might cause a file generation for the given transformation step tree
								ElementConverterConfigurationTreeNode rootNodeForBranch = null;
								Class<?> contextSourceClass = getContextSourceClass();
								if (contextSourceClass != null) {
//									node.getElementConverterConfiguration().getSourceClass()
//									System.out.println("node:" + node);
//									System.out.println("visualization context:" + getElementConverterVisualizationContext());
//									System.out.println("leading node:" + node.getLeadingNode(getElementConverterVisualizationContext()));
									rootNodeForBranch = node.getLeadingNode(getElementConverterVisualizationContext());
								}
								
								if (rootNodeForBranch != null) {
									StringBuilder sb = new StringBuilder(node.getTargetId()).append(" -> ").append(genGroupTargetConf.getTargetId());
									
									if (mapWrittenTargetConfig.contains(sb.toString()) == false) {
										wNL(sb);
										wNL(genGroupTargetConf.getTargetId(), " [shape = note, style = filled, fillcolor = \"#eff3ff\"]");
										mapWrittenTargetConfig.add(sb.toString());
										somethingGotWritten = true;
									}
								}
//										" [label = \"", genGroupTargetConf.getGenerationGroupClassName(), "\"]");
							}
						}
					}
				}
			}
			
			ElementConverterConfigurationTreeNode rootNodeForBranch = node.getLeadingNode(getElementConverterVisualizationContext());
			if (rootNodeForBranch != null) {
//				if (node.getParent().isRoot() || node.getSourceId().equals(node.getParent().getTargetId())) {
				if (node.getParent().isRoot()) {
					String fromSourceToTarget = new StringBuilder(node.getSourceId()).append(" -> ").append(node.getTargetId()).append(" [label = \"").append(node.getLabel()).append("\"]").toString();
					if (!writtenSourceToTargetLines.contains(fromSourceToTarget)) {
						writtenSourceToTargetLines.add(fromSourceToTarget);
					    wNL(fromSourceToTarget);
					    wNL(node.getSourceId(), " [label = \"", node.getSourceLabel(), "\"]");
					    wNL(node.getTargetId(), " [label = \"", node.getTargetLabel(), "\"]");
					    addNodeToSubgraph(node);
					}
				} else {
					if (node.getElementConverterConfiguration().getSourceClass() == node.getParent().getElementConverterConfiguration().getTargetClass()) {
						// a perfect match, target class of previous element converter == source class of current element converter
						String fromSourceToTarget = new StringBuilder(node.getSourceId()).append(" -> ").append(node.getTargetId()).append(" [label = \"").append(node.getLabel()).append("\"]").toString();
						if (!writtenSourceToTargetLines.contains(fromSourceToTarget)) {
							writtenSourceToTargetLines.add(fromSourceToTarget);
						    wNL(fromSourceToTarget);
						    wNL(node.getSourceId(), " [label = \"", node.getSourceLabel(), "\"]");
						    wNL(node.getTargetId(), " [label = \"", node.getTargetLabel(), "\"]");
						    addNodeToSubgraph(node);
						}
					} else if (node.getElementConverterConfiguration().getSourceClass().isAssignableFrom(node.getParent().getElementConverterConfiguration().getTargetClass())) {
						String fromSourceToTarget = new StringBuilder(node.getParent().getTargetId()).append(" -> ").append(node.getTargetId()).append(" [label = \"").append(node.getLabel()).append("\"]").toString();
						if (!writtenSourceToTargetLines.contains(fromSourceToTarget)) {
							writtenSourceToTargetLines.add(fromSourceToTarget);
						    wNL(fromSourceToTarget);
						    wNL(node.getParent().getTargetId(), " [label = \"", node.getParent().getTargetLabel(), "\"]");
						    wNL(node.getTargetId(), " [label = \"", node.getTargetLabel(), "\"]");
						    addNodeToSubgraph(node);
						}
					}
				}
						
//						|| node.getElementConverterConfiguration().getSourceClass().isAssignableFrom(node.getParent().getElementConverterConfiguration().getTargetClass())) {
//					String fromSourceToTarget = new StringBuilder(node.getSourceId()).append(" -> ").append(node.getTargetId()).append(" [label = \"").append(node.getLabel()).append("\"]").toString();
//					if (!writtenSourceToTargetLines.contains(fromSourceToTarget)) {
//						writtenSourceToTargetLines.add(fromSourceToTarget);
//					    wNL(fromSourceToTarget);
//					    wNL(node.getSourceId(), " [label = \"", node.getSourceLabel(), "\"]");
//					    wNL(node.getTargetId(), " [label = \"", node.getTargetLabel(), "\"]");
//					    addNodeToSubgraph(node);
//					}
//					
//					if (!node.getParent().isRoot() && node.getElementConverterConfiguration().getSourceClass() != node.getParent().getElementConverterConfiguration().getTargetClass()) {
//						// in this case we do not have an exact "prev target" -> "source" match for two element converters
//						
//						fromSourceToTarget = new StringBuilder(node.getParent().getTargetId()).append(" -> ").append(node.getTargetId()).append(" [label = \"").append(node.getLabel()).append("\"]").toString();
//						if (!writtenSourceToTargetLines.contains(fromSourceToTarget)) {
//							writtenSourceToTargetLines.add(fromSourceToTarget);
//						    wNL(fromSourceToTarget);
//						    wNL(node.getParent().getTargetId(), " [label = \"", node.getParent().getTargetLabel(), "\"]");
//						}
//					}
//				} else {
//					System.out.println("source id:" + node.getSourceId() + ", parent's target id:" + node.getParent().getTargetId());
//				}
//			} else {
//				System.out.println("no root node for branch for node '" + node + "' and visualization context '" + getElementConverterVisualizationContext() + "'");
			}
		}

		// --- continue traversal over children
		for (ElementConverterConfigurationTreeNode childTreeNode : node.getChildren()) {
			if (ignoreNode(childTreeNode)) {
				continue;
			}
			
			boolean enabledLoggingOfVisualizationData = false;
			if (logVisualizationData == false && childTreeNode.featuresSourceClass(getContextSourceClass())) {
				logVisualizationData = true;
				enabledLoggingOfVisualizationData = true;
			}
			traverseNode(childTreeNode);
			if (enabledLoggingOfVisualizationData) {
				logVisualizationData = false;
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) {
		// there is only one target section, thus we do not need the if-else construct to differentiate the sections
		wNLI("digraph elementtree {");
		    wNL("rankdir = TB");
		    wNL("node[fontsize=10, fontname=\"helvetica\"]");
		    wNL("edge[fontsize=8, fontname=\"helvetica\"]");
		    wNL("graph[fontsize=10, fontname=\"helvetica bold\"]");
		    traverseNode(rootNode);
		    if (somethingGotWritten == false) {
			    throw new DoNotWriteException("converter configuration tree would be empty - nothing to be generated");
		    }
		    wNL();
		    writeSubgraphs();
//		    writeNodeLabels(rootNode);
		wONL("}");
	}
	
	/**
	 * 
	 */
	private void writeSubgraphs() {
		for (String subgraphId : subgraphs.keySet()) {
			String label = subgraphLabels.get(subgraphId).substring(subgraphLabels.get(subgraphId).lastIndexOf(".")+1);
			wNLI("subgraph cluster_", subgraphId, " {");
			    wNL("label = \"", label, "\";");
				wNL("style = filled;");
		    	wNL("color = \"#edf8e9\";");
				for (String elementId : subgraphs.get(subgraphId)) {
					wNL(elementId, ";");
				}
			wONL("}");
		}
	}

	/**
	 * @param node
	 */
	@SuppressWarnings("unused")
	private void writeNodeLabels(ElementConverterConfigurationTreeNode node) {
		if (node.getSourceLabel() != null) {
		    wNL(node.getSourceId(), " [label = \"", node.getSourceLabel(), "\"]");
		}
		if (node.getTargetLabel() != null) {
		    wNL(node.getTargetId(), " [label = \"", node.getTargetLabel(), "\"]");
		}

		for (ElementConverterConfigurationTreeNode childTreeNode : node.getChildren()) {
			if (ignoreNode(childTreeNode)) {
				continue;
			}
			writeNodeLabels(childTreeNode);
		}
	}
}
