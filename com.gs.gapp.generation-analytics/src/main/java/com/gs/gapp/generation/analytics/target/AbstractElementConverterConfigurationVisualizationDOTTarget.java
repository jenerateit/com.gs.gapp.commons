package com.gs.gapp.generation.analytics.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ContextObject;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetException;

import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.ElementConverterVisualizationContext;

/**
 * @author mmt
 *
 */
public abstract class AbstractElementConverterConfigurationVisualizationDOTTarget extends BasicTextTarget<TextTargetDocument> {

	@ModelElement
	private ElementConverterConfigurationTreeNode rootNode;

	@ContextObject
	private ElementConverterVisualizationContext elementConverterVisualizationContext;

	@Override
	public URI getTargetURI() {
		
		StringBuilder sb =
				new StringBuilder(getTargetRoot()).append("/vd-reports/").append(getTargetPrefix()).append("/").append(getPathPrefix()).append(getFileName()).append(".dot");
		
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw createTargetException(e, this, rootNode);
		}
	}
	
	/**
	 * @return
	 */
	protected String getFileName() {
		return getElementConverterVisualizationContext().getSourceClass().getSimpleName() + "-" + getElementConverterVisualizationContext().getClassOrMetatypeName();
	}

	/**
	 * @return the elementConverterConfiguration
	 */
	public Class<?> getContextSourceClass() {
		return getElementConverterVisualizationContext().getSourceClass();
	}
	
	/**
	 * @return
	 */
	protected abstract String getPathPrefix();
	
	public ElementConverterVisualizationContext getElementConverterVisualizationContext() {
		return elementConverterVisualizationContext;
	}
}
