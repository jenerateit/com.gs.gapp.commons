package com.gs.gapp.generation.analytics.writer;

public class Edge {

	private final String id;
	private final String name;
	
	private Vertex source;
	private Vertex target;

	public Edge(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}

	public Vertex getSource() {
		return source;
	}

	public void setSource(Vertex source) {
		this.source = source;
	}

	public Vertex getTarget() {
		return target;
	}

	public void setTarget(Vertex target) {
		this.target = target;
	}
}
