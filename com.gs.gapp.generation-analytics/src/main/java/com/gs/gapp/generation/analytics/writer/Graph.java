package com.gs.gapp.generation.analytics.writer;

import java.util.Collection;
import java.util.LinkedHashSet;

public class Graph {

	private final String name;
	private final LinkedHashSet<Vertex> vertexes = new LinkedHashSet<>();
	private final LinkedHashSet<Edge> edges = new LinkedHashSet<Edge>();
	
	public Graph(String name) {
		this.name = name;
	}
	
	public Graph(String name, Collection<Vertex> vertexes, Collection<Edge> edges) {
		this.name = name;
		this.vertexes.addAll(vertexes);
		this.edges.addAll(edges);
	}

	public LinkedHashSet<Vertex> getVertexes() {
		return vertexes;
	}
	
	public boolean addVertex(Vertex vertex) {
		return this.vertexes.add(vertex);
	}

	public LinkedHashSet<Edge> getEdges() {
		return edges;
	}
	
	public boolean addEdges(Edge edge) {
		return this.edges.add(edge);
	}

	public String getName() {
		return name;
	}
}
