package com.gs.gapp.generation.analytics.target;

import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Set;

import org.jenerateit.annotation.Context;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.DoNotWriteException;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.analytics.writer.Edge;
import com.gs.gapp.generation.analytics.writer.ElementConverterConfigurationVisualizationMxGraphJavaScriptWriterSnippets;
import com.gs.gapp.generation.analytics.writer.Graph;
import com.gs.gapp.generation.analytics.writer.Vertex;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.GenerationGroupTargetConfiguration;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;

@Context(provider=ElementConverterSubNodesContextProvider.class)
public class ElementConverterConfigurationTreeVisualizationMxGraphJavaScriptTarget
    extends AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptTarget {

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.analytics.target.AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptTarget#getPathPrefix()
	 */
	@Override
	protected String getPathPrefix() {
		
		return "full-conversion-tree/";
	}
	
	/**
	 * @author mmt
	 *
	 */
	public static class ElementConverterConfigurationTreeVisualizationMxGraphJavaScriptWriter extends AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptWriter {

		@ModelElement
		private ElementConverterConfigurationTreeNode rootNode;
		
		private final LinkedHashMap<String, Vertex> vertexMap = new LinkedHashMap<>();
		private final LinkedHashMap<Vertex, Set<Vertex>> edgeMap = new LinkedHashMap<>();
		
		private boolean logVisualizationData = false;
		private boolean somethingGotWritten = false;
		
		/**
		 * @param node
		 */
		protected void traverseNode(final ElementConverterConfigurationTreeNode node, Graph graph) {

			if (logVisualizationData) {
				
				// --- first handly generation groups and their targets, to add leaf vertexes for target classes
				if (node.getElementConverterConfiguration() != null && node.getElementConverterConfiguration().getModelConverterConfiguration() != null) {
					TransformationStepConfiguration transformationStepConfiguration = node.getElementConverterConfiguration().getModelConverterConfiguration();
					for (TransformationStepConfiguration childTransStepConfig : transformationStepConfiguration.getChildConfigurations()) {
						addLeafVertexesForTargets(node, graph, childTransStepConfig);
					}
				}
				
				// --- here is the central logic to decide about vertexes and edges to be inserted for model element converters
				ElementConverterConfigurationTreeNode rootNodeForBranch = node.getLeadingNode(getElementConverterVisualizationContext());
				if (rootNodeForBranch != null) {
					if (node.getParent().isRoot()) {
						addEdge(node, graph);
					} else {
						if (node.getElementConverterConfiguration().getSourceClass() == node.getParent().getElementConverterConfiguration().getTargetClass()) {
							// a perfect match, target class of previous element converter == source class of current element converter
							addEdge(node, graph);
						} else if (node.getElementConverterConfiguration().getSourceClass().isAssignableFrom(node.getParent().getElementConverterConfiguration().getTargetClass())) {
							addEdge(node, graph);
						}
					}
				}
			}

			// --- recursively handle a node's children to further complete the graph that is based on the model elment converter tree
			for (ElementConverterConfigurationTreeNode childTreeNode : node.getChildren()) {
				if (ignoreNode(childTreeNode)) {
					continue;
				}
				
				boolean enabledLoggingOfVisualizationData = false;
				if (logVisualizationData == false && childTreeNode.featuresSourceClass(getContextSourceClass())) {
//						&&
//						childTreeNode.isInBranch(getElementConverterVisualizationContext())) {
					logVisualizationData = true;
					enabledLoggingOfVisualizationData = true;
				}
				// ----------------------------------------------------------
				// --- here the recursive call is being made
//				System.out.println("traversing child tree node '" + childTreeNode + "' (logVisualizationData:" + logVisualizationData + ", enabledLoggingOfVisualizationData:" + enabledLoggingOfVisualizationData + ")");
				traverseNode(childTreeNode, graph);
				if (enabledLoggingOfVisualizationData) {
					logVisualizationData = false;
				}
			}
		}

		/**
		 * @param node
		 * @param graph
		 * @param transformationStepConfiguration
		 */
		protected void addLeafVertexesForTargets(final ElementConverterConfigurationTreeNode node, Graph graph,
				TransformationStepConfiguration transformationStepConfiguration) {
			if (transformationStepConfiguration.getTargetConfigurations().size() > 0) {
				// the transformationStepConfiguration relates to a generation group that has targets configured
//							System.out.println("The TransformationStepConfiguration '" + childTransStepConfig + "' relates to a generation group that has targets configured");
				for (GenerationGroupTargetConfiguration genGroupTargetConf : transformationStepConfiguration.getTargetConfigurations()) {
//								System.out.println("Generation Group Target Conf:" + genGroupTargetConf);
					if ((genGroupTargetConf.getTargetClass().getModifiers() & Modifier.ABSTRACT) != 0) {
						continue;  // ignore abstract target classes
					}
					if (node.getElementConverterConfiguration().getTargetClass() == genGroupTargetConf.getMetaModelClass()) {
						// ---------------------------------------------------------------------------------------------------
						// --- found a target class that might cause a file generation for the given transformation step tree
						ElementConverterConfigurationTreeNode rootNodeForBranch = null;
						Class<?> contextSourceClass = getContextSourceClass();
						if (contextSourceClass != null) {
							rootNodeForBranch = node.getLeadingNode(getElementConverterVisualizationContext());
						}
						
						if (rootNodeForBranch != null) {
							
							somethingGotWritten = true;
							
							Vertex vSource = vertexMap.get(node.getTargetLabel());
							if (vSource == null) {
								vSource = new Vertex(null, node.getTargetLabel());
								vertexMap.put(node.getTargetLabel(), vSource);
							}
							Vertex vTarget = vertexMap.get(genGroupTargetConf.getSimpleTargetClassName());
							if (vTarget == null) {
								vTarget = new Vertex(null, genGroupTargetConf.getSimpleTargetClassName());
								vTarget.setStyles("defaultVertex;fillColor=lightGreen;");
								vertexMap.put(genGroupTargetConf.getSimpleTargetClassName(), vTarget);
							}
							
							// --- finally, add the edge, unless the graph alreay includes such an edge
							Set<Vertex> targetVertexes = edgeMap.get(vSource);
							if (targetVertexes == null) {
								targetVertexes = new HashSet<>();
								edgeMap.put(vSource, targetVertexes);
							}
							
							if (!targetVertexes.contains(vTarget)) {
								Edge edge = new Edge(null, genGroupTargetConf.getSimpleWriterClassName());
								edge.setSource(vSource);
								edge.setTarget(vTarget);
								graph.addVertex(vSource);
								graph.addVertex(vTarget);
								graph.addEdges(edge);
								targetVertexes.add(vTarget);
							}
						}
					}
				}
			}
		}

		/**
		 * @param node
		 * @param graph
		 */
		private void addEdge(final ElementConverterConfigurationTreeNode node, Graph graph) {
			Vertex vSource = vertexMap.get(node.getSourceLabel());
			if (vSource == null) {
//				if ("ComplexType".equalsIgnoreCase(node.getSourceLabel())) {
//					int a = 1;
//				}
				vSource = new Vertex(null, node.getSourceLabel());
				vertexMap.put(node.getSourceLabel(), vSource);
			}
			Vertex vTarget = vertexMap.get(node.getTargetLabel());
			if (vTarget == null) {
				vTarget = new Vertex(null, node.getTargetLabel());
				vertexMap.put(node.getTargetLabel(), vTarget);
			}

			// --- finally, add the edge, unless the graph alreay includes such an edge
			Set<Vertex> targetVertexes = edgeMap.get(vSource);
			if (targetVertexes == null) {
				targetVertexes = new HashSet<>();
				edgeMap.put(vSource, targetVertexes);
			}
			if (!targetVertexes.contains(vTarget) && node.getLabel().indexOf("PASSTHROUGH") < 0) {
				Edge edge = new Edge(null, node.getLabel());  // TODO do we need to have an edge id?
				edge.setSource(vSource);
				edge.setTarget(vTarget);
				graph.addVertex(vSource);
				graph.addVertex(vTarget);
				graph.addEdges(edge);
				targetVertexes.add(vTarget);
			}
		}

		/* (non-Javadoc)
		 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
		 */
		@Override
		public void transform(TargetSection ts) {
			// there is only one target section, thus we do not need the if-else construct to differentiate the sections
			
			String fileName = ((AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptTarget)getTextTransformationTarget()).getFileName();
			Graph graph = new Graph(fileName);
			
//			System.out.println("##### FILE: " + fileName);
			
			traverseNode(rootNode, graph);
			
			if (somethingGotWritten == false) {
			    throw new DoNotWriteException("converter configuration tree would be empty - nothing to be generated");
		    }
			
			String javaScriptFileContent = ElementConverterConfigurationVisualizationMxGraphJavaScriptWriterSnippets.
					getMxGraphJavaScript(graph);
			if (javaScriptFileContent != null && javaScriptFileContent.length() > 0) {
			    w(javaScriptFileContent);
			}
		}
	}
}
