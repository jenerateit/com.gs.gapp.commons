package com.gs.gapp.generation.analytics.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.Context;
import org.jenerateit.annotation.ContextObject;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetI;

import com.gs.gapp.generation.analytics.writer.ModelElementTreeVisualizationDOTWriter;
import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.metamodel.analytics.ModelElementTreeNode;

@Context(provider=ModelElementTreeVisualizationContextProvider.class)
public class ModelElementTreeVisualizationDOTTarget extends BasicTextTarget<TextTargetDocument> {

	@ModelElement
	private ModelElementTreeNode rootNode;

	@ContextObject
    private ModelElementTreeGenerationContext context;

	@Override
	public URI getTargetURI() {
		StringBuilder sb = new StringBuilder(getTargetRoot()).append("/vd-reports").append("/").append("element-trees").append("/").append(getRootElementId() == null ? "ALL" : getRootElementId()).append("-").append(getFilenamePostfix()).append(".dot");
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw createTargetException(e, this, rootNode);
		}
	}

	private String getRootElementId() {
		if (getContext().getRootElement() == null) {
			return null;
		} else {
			return getContext().getRootElement().getId();
		}
	}

	private String getFilenamePostfix() {
		if (ModelElementTreeVisualizationDOTWriter.CONTEXT_ALL.equals(getContext().getType())) {
			return "full";
		} else if (ModelElementTreeVisualizationDOTWriter.CONTEXT_TARGETS_ONLY.equals(getContext().getType())) {
			return "targets-only";
		} else {
			return "";
		}
	}

	/**
	 * @return the context
	 */
	public ModelElementTreeGenerationContext getContext() {
		return context;
	}
}
