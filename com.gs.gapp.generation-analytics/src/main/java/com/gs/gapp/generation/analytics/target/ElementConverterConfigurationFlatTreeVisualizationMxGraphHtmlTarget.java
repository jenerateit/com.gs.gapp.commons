package com.gs.gapp.generation.analytics.target;

import org.jenerateit.annotation.Context;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;

@Context(provider=ElementConverterSubNodesContextProvider.class)
public class ElementConverterConfigurationFlatTreeVisualizationMxGraphHtmlTarget extends AbstractElementConverterConfigurationVisualizationMxGraphHtmlTarget {

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.analytics.target.AbstractElementConverterConfigurationVisualizationDOTTarget#getPathPrefix()
	 */
	@Override
	protected String getPathPrefix() {
		
		return "flat-conversion-tree/";
	}
	
	public static class ElementConverterConfigurationFlatTreeVisualizationMxGraphHtmlWriter extends AbstractElementConverterConfigurationVisualizationMxGraphHtmlWriter {

		@ModelElement
		private ElementConverterConfigurationTreeNode rootNode;
		
		/* (non-Javadoc)
		 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
		 */
		@Override
		public void transform(TargetSection ts) {
			wNL("TODO");
//			MxGraphJavaScriptLib mxGraphJavaScriptLib = rootNode.getModel().getSingleExtensionElement(MxGraphJavaScriptLib.class);
//			AnyFileTarget javaScriptLibTarget = (AnyFileTarget) findTarget(FindTargetScope.PROJECT, mxGraphJavaScriptLib, AnyFileTarget.class, null);
//			MxGraphJavaScriptLib javaScriptLib = (MxGraphJavaScriptLib) javaScriptLibTarget.getBaseElement();
//			String fileName = ((AbstractElementConverterConfigurationVisualizationMxGraphHtmlTarget)getTextTransformationTarget()).getFileName();
//			String graphName = fileName;
//			String nameOfMxGraphJavaScriptFile = javaScriptLib.getName() + "." + javaScriptLib.getExtension();
//			String nameOfJavaScriptFile = fileName + ".js";
//			String htmlFileContent = ElementConverterConfigurationVisualizationMxGraphHtmlWriterSnippets.
//					getMxGraphHtml(graphName, nameOfMxGraphJavaScriptFile, nameOfJavaScriptFile);
//			if (htmlFileContent != null && htmlFileContent.length() > 0) {
//			    w(htmlFileContent);
//			}
		}
	}
}
