package com.gs.gapp.generation.analytics.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;

import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.metamodel.analytics.ModelElementTreeNode;

public class ModelElementTreeVisualizationTextTarget extends BasicTextTarget<TextTargetDocument> {

	@ModelElement
	private ModelElementTreeNode rootNode;

	@Override
	public URI getTargetURI() {
		StringBuilder sb = new StringBuilder(getTargetRoot()).append("/").append(rootNode.getName()).append(".txt");
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw createTargetException(e, this, rootNode);
		}
	}

}
