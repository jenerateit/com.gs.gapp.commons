package com.gs.gapp.generation.analytics.target;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.jenerateit.annotation.ContextProviderI;
import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.ElementNotSupportedException;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetI;

import com.gs.gapp.generation.analytics.writer.ModelElementTreeVisualizationDOTWriter;
import com.gs.gapp.metamodel.analytics.ModelElementTreeNode;

public class ModelElementTreeVisualizationContextProvider implements
		ContextProviderI {

	public ModelElementTreeVisualizationContextProvider() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Serializable[] getAllContext(
			Class<? extends TargetI<? extends TargetDocumentI>> targetClass,
			Object modelElement) throws ElementNotSupportedException,
			JenerateITException {
		Set<Serializable> result = new LinkedHashSet<>();
		List<ModelElementTreeGenerationContext> test = new ArrayList<>();  // TEST only
		if (modelElement instanceof ModelElementTreeNode) {
			ModelElementTreeNode modelElementTreeNode = (ModelElementTreeNode) modelElement;
			if (modelElementTreeNode.isRoot()) {
                result.add(new ModelElementTreeGenerationContext(null, ModelElementTreeVisualizationDOTWriter.CONTEXT_ALL));  // indicates the root node => full tree
                result.add(new ModelElementTreeGenerationContext(null, ModelElementTreeVisualizationDOTWriter.CONTEXT_TARGETS_ONLY));  // indicates the root node => full tree
                for (ModelElementTreeNode subnode : modelElementTreeNode.getChildren()) {
                	if (subnode.getChildren().size() > 0) {
                		boolean newAddition = false;
                		// we only want to generate a visualization for elements that actually _are_ transformed into other elements
                		ModelElementTreeGenerationContext context = new ModelElementTreeGenerationContext(subnode.getModelElement(), ModelElementTreeVisualizationDOTWriter.CONTEXT_ALL);
                		newAddition = result.add(context);
                		if (!newAddition) {
                			@SuppressWarnings("unused")
							int a = 1;
                		}
                		if (context.getRootElement().getName().equalsIgnoreCase("ContactsPersistence")) test.add(context);
	                    context = new ModelElementTreeGenerationContext(subnode.getModelElement(), ModelElementTreeVisualizationDOTWriter.CONTEXT_TARGETS_ONLY);
	                    newAddition = result.add(context);
	                    if (!newAddition) {
                			@SuppressWarnings("unused")
							int a = 1;
                		}
	                    if (context.getRootElement().getName().equalsIgnoreCase("ContactsPersistence")) test.add(context);
                	}
                }
			}
		}

		// --- TEST only
//		result.clear();
//		result.addAll(test);

		Serializable[] resultArray = result.toArray(new Serializable[0]);
//		System.out.println("*** returning " + resultArray.length + " contexts");
		return resultArray;


	}

}
