package com.gs.gapp.generation.analytics.target;

import org.jenerateit.annotation.Context;

@Context(provider=ElementConverterSubNodesContextProvider.class)
public class ElementConverterConfigurationFlatTreeVisualizationDOTTarget extends AbstractElementConverterConfigurationVisualizationDOTTarget {


	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.analytics.target.AbstractElementConverterConfigurationVisualizationDOTTarget#getPathPrefix()
	 */
	@Override
	protected String getPathPrefix() {
		
		return "flat-conversion-tree/";
	}

}
