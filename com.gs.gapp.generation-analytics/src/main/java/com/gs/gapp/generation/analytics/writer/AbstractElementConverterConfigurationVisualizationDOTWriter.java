package com.gs.gapp.generation.analytics.writer;

import com.gs.gapp.generation.analytics.target.AbstractElementConverterConfigurationVisualizationDOTTarget;
import com.gs.gapp.metamodel.analytics.ElementConverterConfiguration;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.ElementConverterVisualizationContext;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;

public abstract class AbstractElementConverterConfigurationVisualizationDOTWriter extends AbstractAnalyticsWriter {

	
	/**
	 * @param node
	 * @return
	 */
	protected final boolean ignoreNode(ElementConverterConfigurationTreeNode node) {
	
		// --- ignore visualization for meta-classes that related to the visualization itself
		if (node.getElementConverterConfiguration() != null) {
			ElementConverterConfiguration elementConverterConfiguration = node.getElementConverterConfiguration();
			if (TransformationStepConfiguration.class.isAssignableFrom(elementConverterConfiguration.getSourceClass()) ||
					TransformationStepConfiguration.class.isAssignableFrom(elementConverterConfiguration.getTargetClass())) {
				return true;
			}
		}
		
		return false;
	}

		
	/**
	 * @return
	 */
	protected final Class<?> getContextSourceClass() {
		if (getTransformationTarget() instanceof AbstractElementConverterConfigurationVisualizationDOTTarget) {
			AbstractElementConverterConfigurationVisualizationDOTTarget elementConverterConfigurationVisualizationDOTTarget =
					(AbstractElementConverterConfigurationVisualizationDOTTarget) getTransformationTarget();
			return elementConverterConfigurationVisualizationDOTTarget.getContextSourceClass();
		}
		return null;
	}
	
	/**
	 * @return
	 */
	protected final ElementConverterVisualizationContext getElementConverterVisualizationContext() {
		if (getTransformationTarget() instanceof AbstractElementConverterConfigurationVisualizationDOTTarget) {
			AbstractElementConverterConfigurationVisualizationDOTTarget elementConverterConfigurationVisualizationDOTTarget =
					(AbstractElementConverterConfigurationVisualizationDOTTarget) getTransformationTarget();
			return elementConverterConfigurationVisualizationDOTTarget.getElementConverterVisualizationContext();
		}
		return null;
	}
	
}
