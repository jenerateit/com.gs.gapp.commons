package com.gs.gapp.generation.analytics.writer;

import java.util.Set;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTarget;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.FindTargetScope;

import com.gs.gapp.metamodel.analytics.ModelElementTreeNode;
import com.gs.gapp.metamodel.basic.ConversionDetails;

public class ModelElementTreeVisualizationTextWriter extends AbstractAnalyticsWriter {

	@ModelElement
	private ModelElementTreeNode rootNode;

	protected void writeNode(ModelElementTreeNode node) {
		@SuppressWarnings("unused")
		int lengthOfChars = 0;
		if (node.getModelElement() != null) {

			if (node.getModelElement() instanceof com.gs.gapp.metamodel.basic.ModelElement) {

				com.gs.gapp.metamodel.basic.ModelElement modelElement = (com.gs.gapp.metamodel.basic.ModelElement) node.getModelElement();
				ConversionDetails conversionDetails = modelElement.getConversionDetails();
				lengthOfChars = wElementTransformationInfo(modelElement.getName(), modelElement.getClass().getName(),
			    		(conversionDetails == null ? "-" : conversionDetails.getModelElementConverterClassname()));
			} else {
				lengthOfChars = wElementTransformationInfo(node.getModelElement().toString(), node.getModelElement().getClass().getName(), "-");
			}

		} else {
			lengthOfChars = wElementTransformationInfo("empty-node", "-", "-");
		}

		if (node.isRoot()) {
			wNL();
		}

		if (node.getChildren().size() > 0) {
			indent(6);
			for (ModelElementTreeNode childTreeNode : node.getChildren()) {
				if (node.isRoot()) {
					wNL("----------------------------------------------------------------------------");
				}
				writeNode(childTreeNode);
			}
			outdent(6);
		} else {
			indent(6);
			//writeTargets(node);
			outdent(6);
            wNL();
		}
	}

	protected void writeTargets(ModelElementTreeNode leafNode) {
		if (leafNode != null && leafNode.getModelElement() != null) {
			wNL("**************************************************");
	    	@SuppressWarnings("unchecked")
			Set<TargetI<? extends TargetDocumentI>> targets = findTargets(FindTargetScope.PROJECT, leafNode.getModelElement(), (Class<? extends TargetI<?>>) AbstractTarget.class, AbstractWriter.class);
	    	if (targets != null && targets.size() > 0) {
		    	for (TargetI<? extends TargetDocumentI> target : targets) {
		    		wNL("TARGET: ", target.getTargetPath().toString());
		    		wNL("TARGET-CLASS: ", target.getClass().getName());
		    	}
	    	} else {
	    		wNL("*** NO TARGETS FOUND");
	    	}
		}
	}

	@Override
	public void transform(TargetSection ts) {
		// there is only one target section, thus we do not need the if-else construct to differentiate the sections

		/* test to simulate big files*/
//		for (int ii=0; ii<100000; ii++) {
//		    wNL("Hello World", "-", ""+ii);
//		}

		writeNode(rootNode);

/*
		// TODO this is just for testing
		com.gs.gapp.metamodel.basic.ModelElement startModelElement = null;
		Model mainModel = textDocument.getOriginatingElement(Model.class);
*/
		/*
		wNL("start of original model element quest");
		Serializable startElement = getTransformationTarget().getBaseElement();
		//Serializable startElement = getTransformationTarget().getBaseElement();
		if (startElement instanceof com.gs.gapp.metamodel.basic.ModelElement) {
			startModelElement = (com.gs.gapp.metamodel.basic.ModelElement) startElement;
			mainModelElement = startModelElement;
			while (startModelElement != null) {
				wNL("model element:" + startModelElement.getName() + ", metatype:" + startModelElement.getClass().getSimpleName());
				if (startModelElement.getOriginatingElement() instanceof com.gs.gapp.metamodel.basic.ModelElement) {
					startModelElement = startModelElement.getOriginatingElement(com.gs.gapp.metamodel.basic.ModelElement.class);
				} else {
					if (startModelElement.getOriginatingElement() instanceof ModelElementWrapper) {
						ModelElementWrapper wrapper = startModelElement.getOriginatingElement(ModelElementWrapper.class);
						wNL("model element:" + wrapper.getWrappedElement().toString() + ", metatype:" + wrapper.getWrappedElement().getClass().getSimpleName());
					} else if (startModelElement.getOriginatingElement() != null) {
						wNL("model element:" + startModelElement.getOriginatingElement().toString() + ", metatype:" + startModelElement.getOriginatingElement().getClass().getSimpleName());
					}
					startModelElement = null;
				}
			}
		}
		wNL("end of original model element quest");
		*/
/*
		if (mainModel != null) {
			wNL(mainModel.getElements().size() + " model elements:");

			for (com.gs.gapp.metamodel.basic.ModelElement modelElement : mainModel.getElements()) {
				wNL("model element:" + modelElement.getName() + ", metatype:" + modelElement.getClass().getSimpleName());

				indent();
				startModelElement = modelElement;
				while (startModelElement != null) {
					wElementTransformationInfo(startModelElement.getName(), startModelElement.getClass().getSimpleName(), startModelElement.getCreatorClassname());
					if (startModelElement.getOriginatingElement() instanceof com.gs.gapp.metamodel.basic.ModelElement) {
						startModelElement = startModelElement.getOriginatingElement(com.gs.gapp.metamodel.basic.ModelElement.class);
					} else {
						if (startModelElement.getOriginatingElement() instanceof ModelElementWrapper) {
							ModelElementWrapper wrapper = startModelElement.getOriginatingElement(ModelElementWrapper.class);
							wElementTransformationInfo(wrapper.getWrappedElement().toString(), wrapper.getWrappedElement().getClass().getSimpleName(), "-");
						} else if (startModelElement.getOriginatingElement() != null) {
							wElementTransformationInfo(startModelElement.getOriginatingElement().toString(), startModelElement.getOriginatingElement().getClass().getSimpleName(), "-");
						}
						startModelElement = null;
					}
				}
				outdent();
			}

            for (com.gs.gapp.metamodel.basic.ModelElement modelElement : mainModel.getElements()) {
            	wNL(modelElement.getName() + ", metatype:" + modelElement.getClass().getSimpleName());
            	@SuppressWarnings("unchecked")
				Set<TargetI<? extends TargetDocumentI>> targets = findTargets(FindTargetScope.PROJECT, modelElement, (Class<? extends TargetI<?>>) AbstractTarget.class, AbstractWriter.class);
            	indent();
            	for (TargetI<? extends TargetDocumentI> target : targets) {
            		wNL(target.getTargetURI() + "/" + target.getClass().getSimpleName());
            	}
            	outdent();
            }
		}
		*/
	}

	private int wElementTransformationInfo(String elementName, String elementTypeName, String elementTransformatorName) {
		//wNL(elementName + " (" + elementTypeName + "/" + elementTransformatorName + ")");
		//StringBuilder sb = new StringBuilder(elementName).append("/").append(elementTypeName);
		//w(sb);
		//return sb.length();
		wNL("NAME: ", elementName);
		wNL("TYPE: ", elementTypeName);
		wNL("CREATOR: ", elementTransformatorName);
		return 0;
	}
}
