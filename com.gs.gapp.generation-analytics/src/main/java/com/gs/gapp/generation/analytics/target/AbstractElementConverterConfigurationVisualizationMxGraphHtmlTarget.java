package com.gs.gapp.generation.analytics.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ContextObject;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.FindTargetScope;

import com.gs.gapp.generation.analytics.writer.AbstractAnalyticsWriter;
import com.gs.gapp.generation.analytics.writer.ElementConverterConfigurationVisualizationMxGraphHtmlWriterSnippets;
import com.gs.gapp.generation.anyfile.AnyFileTarget;
import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.metamodel.analytics.ElementConverterConfiguration;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.ElementConverterVisualizationContext;
import com.gs.gapp.metamodel.analytics.MxGraphJavaScriptLib;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;

/**
 * @author mmt
 *
 */
public abstract class AbstractElementConverterConfigurationVisualizationMxGraphHtmlTarget extends BasicTextTarget<TextTargetDocument> {

	@ModelElement
	private ElementConverterConfigurationTreeNode rootNode;

	@ContextObject
	private ElementConverterVisualizationContext elementConverterVisualizationContext;

	@Override
	public URI getTargetURI() {
		
		StringBuilder sb =
				new StringBuilder(getTargetRoot()).append("/vd-reports/").append(getTargetPrefix()).append("/").append(getPathPrefix()).append(getFileName()).append(".html");
		
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw createTargetException(e, this, rootNode);
		}
	}
	
	/**
	 * @return
	 */
	protected String getFileName() {
		return getElementConverterVisualizationContext().getSourceClass().getSimpleName() + "_" + getElementConverterVisualizationContext().getClassOrMetatypeName();
	}

	/**
	 * @return the elementConverterConfiguration
	 */
	public Class<?> getContextSourceClass() {
		return getElementConverterVisualizationContext().getSourceClass();
	}
	
	/**
	 * @return
	 */
	protected abstract String getPathPrefix();
	
	public ElementConverterVisualizationContext getElementConverterVisualizationContext() {
		return elementConverterVisualizationContext;
	}
	
    /**
     * @author mmt
     *
     */
    public static abstract class AbstractElementConverterConfigurationVisualizationMxGraphHtmlWriter extends AbstractAnalyticsWriter {
    	
    	@ModelElement
    	private ElementConverterConfigurationTreeNode rootNode;

    	/* (non-Javadoc)
		 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
		 */
		@Override
		public void transform(TargetSection ts) {
			
			MxGraphJavaScriptLib mxGraphJavaScriptLib = rootNode.getModel().getSingleExtensionElement(MxGraphJavaScriptLib.class);
			AnyFileTarget javaScriptLibTarget = (AnyFileTarget) findTarget(FindTargetScope.PROJECT, mxGraphJavaScriptLib, AnyFileTarget.class, null);
			MxGraphJavaScriptLib javaScriptLib = (MxGraphJavaScriptLib) javaScriptLibTarget.getBaseElement();
			String fileName = ((AbstractElementConverterConfigurationVisualizationMxGraphHtmlTarget)getTextTransformationTarget()).getFileName();
			String graphName = fileName;
			String nameOfMxGraphJavaScriptFile = javaScriptLib.getName() + "." + javaScriptLib.getExtension();
			String nameOfJavaScriptFile = fileName + ".js";
			String htmlFileContent = ElementConverterConfigurationVisualizationMxGraphHtmlWriterSnippets.
					getMxGraphHtml(graphName, nameOfMxGraphJavaScriptFile, nameOfJavaScriptFile);
			if (htmlFileContent != null && htmlFileContent.length() > 0) {
			    w(htmlFileContent);
			}
		}
		
		/**
		 * @param node
		 * @return
		 */
		protected final boolean ignoreNode(ElementConverterConfigurationTreeNode node) {
		
			// --- ignore visualization for meta-classes that related to the visualization itself
			if (node.getElementConverterConfiguration() != null) {
				ElementConverterConfiguration elementConverterConfiguration = node.getElementConverterConfiguration();
				if (TransformationStepConfiguration.class.isAssignableFrom(elementConverterConfiguration.getSourceClass()) ||
						TransformationStepConfiguration.class.isAssignableFrom(elementConverterConfiguration.getTargetClass())) {
					return true;
				}
			}
			
			return false;
		}

			
		/**
		 * @return
		 */
		protected final Class<?> getContextSourceClass() {
			if (getTransformationTarget() instanceof AbstractElementConverterConfigurationVisualizationDOTTarget) {
				AbstractElementConverterConfigurationVisualizationDOTTarget elementConverterConfigurationVisualizationDOTTarget =
						(AbstractElementConverterConfigurationVisualizationDOTTarget) getTransformationTarget();
				return elementConverterConfigurationVisualizationDOTTarget.getContextSourceClass();
			}
			return null;
		}
		
		/**
		 * @return
		 */
		protected final ElementConverterVisualizationContext getElementConverterVisualizationContext() {
			if (getTransformationTarget() instanceof AbstractElementConverterConfigurationVisualizationDOTTarget) {
				AbstractElementConverterConfigurationVisualizationDOTTarget elementConverterConfigurationVisualizationDOTTarget =
						(AbstractElementConverterConfigurationVisualizationDOTTarget) getTransformationTarget();
				return elementConverterConfigurationVisualizationDOTTarget.getElementConverterVisualizationContext();
			}
			return null;
		}
	}
}
