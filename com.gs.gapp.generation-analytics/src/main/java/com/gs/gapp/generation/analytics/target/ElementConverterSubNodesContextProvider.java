package com.gs.gapp.generation.analytics.target;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.annotation.ContextProviderI;
import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.ElementNotSupportedException;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetI;

import com.gs.gapp.metamodel.analytics.ElementConverterConfiguration;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.ElementConverterVisualizationContext;

/**
 * The purpose of this context provider is to collect meta information
 * about all element converters that are available within a root transformation
 * step of all of the transformation steps of a generator. This information then
 * is used by analytic targets/writers to create element converter tree visualizations.
 * 
 * @author mmt
 *
 */
public class ElementConverterSubNodesContextProvider implements ContextProviderI {

	/* (non-Javadoc)
	 * @see org.jenerateit.annotation.ContextProviderI#getAllContext(java.lang.Class, java.lang.Object)
	 */
	@Override
	public Serializable[] getAllContext(
			Class<? extends TargetI<? extends TargetDocumentI>> targetClass,
			Object modelElement) throws ElementNotSupportedException,
			JenerateITException {

		Set<ElementConverterVisualizationContext> result = new LinkedHashSet<>();
		
		if (modelElement instanceof ElementConverterConfigurationTreeNode) {
			ElementConverterConfigurationTreeNode elementConverterConfigurationTreeNode = (ElementConverterConfigurationTreeNode) modelElement;
			if (elementConverterConfigurationTreeNode.isRoot()) {
                for (ElementConverterConfigurationTreeNode subnode : elementConverterConfigurationTreeNode.getChildren()) {
                	if (subnode.getElementConverterConfiguration() == null) continue;
                	result.add(constructContext(subnode.getElementConverterConfiguration()));
                    for (ElementConverterConfigurationTreeNode subnode2 : subnode.getChildren()) {
                    	if (subnode2.getElementConverterConfiguration() == null) continue;
                    	result.add(constructContext(subnode2.getElementConverterConfiguration()));
                    	for (ElementConverterConfigurationTreeNode subnode3 : subnode2.getChildren()) {
                        	if (subnode3.getElementConverterConfiguration() == null) continue;
                        	result.add(constructContext(subnode3.getElementConverterConfiguration()));
                        }
                    }
                }
			}
		}

		return result.toArray(new Serializable[0]);
	}

	/**
	 * @param elementConverterConfiguration
	 * @return
	 */
	private ElementConverterVisualizationContext constructContext(ElementConverterConfiguration elementConverterConfiguration) {
		String classOrMetatypeName = null;
		if (elementConverterConfiguration.getMetatypedConverter() != null && elementConverterConfiguration.getMetatypedConverter().getMetatype() != null) {
			classOrMetatypeName = elementConverterConfiguration.getMetatypedConverter().getMetatype().getName();
		} else {
			classOrMetatypeName = elementConverterConfiguration.getSourceClass().getName();
		}
		
		
		return new ElementConverterVisualizationContext(classOrMetatypeName, elementConverterConfiguration.getSourceClass());
	}
}
