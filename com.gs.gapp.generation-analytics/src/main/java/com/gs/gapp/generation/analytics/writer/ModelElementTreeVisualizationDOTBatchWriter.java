package com.gs.gapp.generation.analytics.writer;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.metamodel.analytics.ModelElementTreeNode;

public class ModelElementTreeVisualizationDOTBatchWriter extends AbstractAnalyticsWriter {

	@ModelElement
	private ModelElementTreeNode rootNode;


	@Override
	public void transform(TargetSection ts) {
		// there is only one target section, thus we do not need the if-else construct to differentiate the sections
        wNL(ModelElementTreeVisualizationDOTBatchWriterSnippets.getExecDotBatch());
	}
}
