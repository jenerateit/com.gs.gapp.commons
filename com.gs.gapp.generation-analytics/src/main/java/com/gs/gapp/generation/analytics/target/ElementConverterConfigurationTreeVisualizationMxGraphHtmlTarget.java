package com.gs.gapp.generation.analytics.target;

import org.jenerateit.annotation.Context;
import org.jenerateit.annotation.ContextObject;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.DoNotWriteException;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.FindTargetScope;

import com.gs.gapp.generation.analytics.writer.ElementConverterConfigurationVisualizationMxGraphHtmlWriterSnippets;
import com.gs.gapp.generation.anyfile.AnyFileTarget;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.ElementConverterVisualizationContext;
import com.gs.gapp.metamodel.analytics.MxGraphJavaScriptLib;

@Context(provider=ElementConverterSubNodesContextProvider.class)
public class ElementConverterConfigurationTreeVisualizationMxGraphHtmlTarget extends AbstractElementConverterConfigurationVisualizationMxGraphHtmlTarget {

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.analytics.target.AbstractElementConverterConfigurationVisualizationDOTTarget#getPathPrefix()
	 */
	@Override
	protected String getPathPrefix() {
		
		return "full-conversion-tree/";
	}
	
	public static class ElementConverterConfigurationTreeVisualizationMxGraphHtmlWriter extends AbstractElementConverterConfigurationVisualizationMxGraphHtmlWriter {

		@ModelElement
		private ElementConverterConfigurationTreeNode rootNode;
		
		/* (non-Javadoc)
		 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
		 */
		@Override
		public void transform(TargetSection ts) {
			
			ElementConverterVisualizationContext context = ((AbstractElementConverterConfigurationVisualizationMxGraphHtmlTarget)getTextTransformationTarget()).getElementConverterVisualizationContext();
			if (!rootNode.hasATargetAsLeaf(context)) {
				throw new DoNotWriteException("no html file generated in case the converter tree for '" + rootNode + "' does not have at least one target typed leaf");
			}
			
			MxGraphJavaScriptLib mxGraphJavaScriptLib = rootNode.getModel().getSingleExtensionElement(MxGraphJavaScriptLib.class);
			AnyFileTarget javaScriptLibTarget = (AnyFileTarget) findTarget(FindTargetScope.PROJECT, mxGraphJavaScriptLib, AnyFileTarget.class, null);
			MxGraphJavaScriptLib javaScriptLib = (MxGraphJavaScriptLib) javaScriptLibTarget.getBaseElement();
			String fileName = ((AbstractElementConverterConfigurationVisualizationMxGraphHtmlTarget)getTextTransformationTarget()).getFileName();
			String graphName = fileName;
			String nameOfMxGraphJavaScriptFile = javaScriptLib.getName() + "." + javaScriptLib.getExtension();
			String nameOfJavaScriptFile = fileName + ".js";
			String htmlFileContent = ElementConverterConfigurationVisualizationMxGraphHtmlWriterSnippets.
					getMxGraphHtml(graphName, nameOfMxGraphJavaScriptFile, nameOfJavaScriptFile);
			if (htmlFileContent != null && htmlFileContent.length() > 0) {
			    w(htmlFileContent);
			}
		}
	}
}
