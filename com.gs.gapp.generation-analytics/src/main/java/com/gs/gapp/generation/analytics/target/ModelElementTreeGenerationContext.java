package com.gs.gapp.generation.analytics.target;

import java.io.Serializable;

import com.gs.gapp.metamodel.basic.ModelElementI;

public class ModelElementTreeGenerationContext implements Serializable {

	private static final long serialVersionUID = -5858580431416351260L;

	private final String type;
	private final ModelElementI rootElement;

	/**
	 * @param rootElement
	 * @param type
	 */
	public ModelElementTreeGenerationContext(ModelElementI rootElement,
			String type) {
		super();
		this.rootElement = rootElement;
		this.type = type;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the rootElement
	 */
	public ModelElementI getRootElement() {
		return rootElement;
	}


	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((rootElement == null) ? 0 : rootElement.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ModelElementTreeGenerationContext other = (ModelElementTreeGenerationContext) obj;
		if (rootElement == null) {
			if (other.rootElement != null) {
				return false;
			}
		} else if (!rootElement.equals(other.rootElement)) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ModelElementTreeGenerationContext [type=" + type
				+ ", rootElement=" + rootElement + "]";
	}



}
