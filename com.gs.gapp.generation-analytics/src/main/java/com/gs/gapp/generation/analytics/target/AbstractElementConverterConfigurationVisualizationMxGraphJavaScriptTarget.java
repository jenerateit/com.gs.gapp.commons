package com.gs.gapp.generation.analytics.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ContextObject;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetException;

import com.gs.gapp.generation.analytics.writer.AbstractAnalyticsWriter;
import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.metamodel.analytics.ElementConverterConfiguration;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.ElementConverterVisualizationContext;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;

/**
 * @author mmt
 *
 */
public abstract class AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptTarget extends BasicTextTarget<TextTargetDocument> {

	@ModelElement
	private ElementConverterConfigurationTreeNode rootNode;

	@ContextObject
	private ElementConverterVisualizationContext elementConverterVisualizationContext;

	@Override
	public URI getTargetURI() {
		
		StringBuilder sb =
				new StringBuilder(getTargetRoot()).
				    append("/vd-reports/").
				    append(getTargetPrefix()).
				    append("/").
				    append(getPathPrefix()).
				    append(getFileName()).
				    append(".js");
		
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw createTargetException(e, this, rootNode);
		}
	}
	
	/**
	 * @return
	 */
	protected String getFileName() {
		return getElementConverterVisualizationContext().getSourceClass().getSimpleName() + "_" + getElementConverterVisualizationContext().getClassOrMetatypeName();
	}

	/**
	 * @return the elementConverterConfiguration
	 */
	public Class<?> getContextSourceClass() {
		return getElementConverterVisualizationContext().getSourceClass();
	}
	
	/**
	 * @return
	 */
	protected abstract String getPathPrefix();
	
	public ElementConverterVisualizationContext getElementConverterVisualizationContext() {
		return elementConverterVisualizationContext;
	}
	
	
	/**
	 * @author mmt
	 *
	 */
	public static abstract class AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptWriter extends AbstractAnalyticsWriter {

		
		/**
		 * @param node
		 * @return
		 */
		protected final boolean ignoreNode(ElementConverterConfigurationTreeNode node) {
		
			// --- ignore visualization for meta-classes that related to the visualization itself
			if (node.getElementConverterConfiguration() != null) {
				ElementConverterConfiguration elementConverterConfiguration = node.getElementConverterConfiguration();
				if (TransformationStepConfiguration.class.isAssignableFrom(elementConverterConfiguration.getSourceClass()) ||
						TransformationStepConfiguration.class.isAssignableFrom(elementConverterConfiguration.getTargetClass())) {
					return true;
				}
			}
			
			return false;
		}

			
		/**
		 * @return
		 */
		protected final Class<?> getContextSourceClass() {
			if (getTransformationTarget() instanceof AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptTarget) {
				AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptTarget target =
						(AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptTarget) getTransformationTarget();
				return target.getContextSourceClass();
			}
			return null;
		}
		
		/**
		 * @return
		 */
		protected final ElementConverterVisualizationContext getElementConverterVisualizationContext() {
			if (getTransformationTarget() instanceof AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptTarget) {
				AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptTarget elementConverterConfigurationVisualizationMxGraphJavaScriptTarget =
						(AbstractElementConverterConfigurationVisualizationMxGraphJavaScriptTarget) getTransformationTarget();
				return elementConverterConfigurationVisualizationMxGraphJavaScriptTarget.getElementConverterVisualizationContext();
			}
			return null;
		}
		
	}
}
