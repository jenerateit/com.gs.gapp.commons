package com.gs.gapp.generation.analytics.writer

class ElementConverterConfigurationVisualizationMxGraphJavaScriptWriterSnippets {

	public static String getMxGraphJavaScript(Graph graph) {

		LinkedHashMap<Vertex, String> vertexVariableNames = new LinkedHashMap<>();
		StringBuilder indentation = new StringBuilder();
		for (int ii=0; ii<20; ii++) indentation.append(" ");
		StringBuilder vertexes = new StringBuilder();
		StringBuilder edges = new StringBuilder();

		int counter = 0;
		for (Vertex vertex : graph.getVertexes()) {
			String varName = "vertex" + counter;
			counter++;
			vertexVariableNames.put(vertex, varName);
			vertexes.append(indentation).append("var ").append(varName).append(" = ").
			    append("graph.insertVertex(parent, null, '");
			if (vertex.getStyles() == null) {
				vertexes.append(vertex.getName()).append("', 20, 20, 200, 40);").append(System.lineSeparator());
			} else {
				vertexes.append(vertex.getName()).append("', 20, 20, 200, 40, '").append(vertex.getStyles()).append("');").append(System.lineSeparator());
			}
		}

		counter = 0;
		for (Edge edge : graph.getEdges()) {
			String sourceVertexVarName = vertexVariableNames.get(edge.getSource());
			String targetVertexVarName = vertexVariableNames.get(edge.getTarget());
			String varName = "edge" + counter;
			counter++;
			edges.append(indentation).append("var ").append(varName).append(" = ").
				append("graph.insertEdge(parent, null, '").
				append(edge.getName()).append("', ").
				append(sourceVertexVarName).append(", ").append(targetVertexVarName).append(");").append(System.lineSeparator());
		}

		String result =
"""var ${graph.getName()} = {

		show : function(container) {
			// Checks if the browser is supported
			if (!mxClient.isBrowserSupported())
			{
				// Displays an error message if the browser is not supported.
				mxUtils.error('Browser is not supported!', 200, false);
			}
			else
			{
				var outline = document.getElementById('outlineContainer');

				// Creates the graph inside the given container
				var graph = new mxGraph(container);

				// Creates the outline (navigator, overview) for moving
				// around the graph in the top, right corner of the window.
				var outln = new mxOutline(graph, outline);

				// Disables basic selection and cell handling
				graph.setEnabled(false);

				// Disables the built-in context menu
				mxEvent.disableContextMenu(container);

				// Enables rubberband selection
				new mxRubberband(graph);

				// Gets the default parent for inserting new cells.
				var parent = graph.getDefaultParent();

				// Changes the default vertex style in-place
				var defaultVertexStyle = graph.getStylesheet().getDefaultVertexStyle();
				defaultVertexStyle[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
				defaultVertexStyle[mxConstants.STYLE_GRADIENTCOLOR] = 'white';
				defaultVertexStyle[mxConstants.STYLE_PERIMETER_SPACING] = 6;
				defaultVertexStyle[mxConstants.STYLE_ROUNDED] = true;
				defaultVertexStyle[mxConstants.STYLE_SHADOW] = true;

				var defaultEdgeStyle = graph.getStylesheet().getDefaultEdgeStyle();
				defaultEdgeStyle[mxConstants.STYLE_ROUNDED] = true;
				defaultEdgeStyle[mxConstants.STYLE_VERTICAL_ALIGN] = mxConstants.ALIGN_BOTTOM;
				defaultEdgeStyle[mxConstants.STYLE_VERTICAL_LABEL_POSITION] = mxConstants.ALIGN_BOTTOM;
				defaultEdgeStyle[mxConstants.STYLE_FONTSIZE] = 10;
                defaultEdgeStyle[mxConstants.STYLE_ENDARROW] = mxConstants.NONE;

				// Creates a layout algorithm to be used
				// with the graph
				var hierarchicalLayout = new mxHierarchicalLayout(graph);
				hierarchicalLayout.intraCellSpacing = 90;
				hierarchicalLayout.interRankCellSpacing = 300;
				hierarchicalLayout.interHierarchySpacing = 200;
				hierarchicalLayout.parallelEdgeSpacing = 200;
				hierarchicalLayout.orientation = mxConstants.DIRECTION_WEST;
				hierarchicalLayout.tightenToSource = true;

				var organicLayout = new mxFastOrganicLayout(graph);
				organicLayout.forceConstant = 120;

				var radialTreeLayout = new mxRadialTreeLayout(graph);

				var compactTreeLayout = new mxCompactTreeLayout(graph);
				compactTreeLayout.moveTree = true;
				compactTreeLayout.nodeDistance = 30;
				compactTreeLayout.levelDistance = 250;
//				compactTreeLayout.prefHozEdgeSep = 250;
//				compactTreeLayout.prefVertEdgeOff = 250;
				compactTreeLayout.minEdgeJetty = 20;
//				compactTreeLayout.maxRankHeight = [50, 50, 50];

				var partitionLayout = new mxPartitionLayout(graph);
				var parallelEdgeLayout = new mxParallelEdgeLayout(graph);
				var stackLayout = new mxStackLayout(graph);
				var circleLayout = new mxCircleLayout(graph);

				// Adds a button to execute the layout
				var button = document.createElement('button');
				mxUtils.write(button, 'Circle');
				mxEvent.addListener(button, 'click', function(evt)
				{
					circleLayout.execute(parent);
				});
				document.body.insertBefore(button, container);

				// Adds a button to execute the layout
				var button = document.createElement('button');
				mxUtils.write(button, 'Stack');
				mxEvent.addListener(button, 'click', function(evt)
				{
					stackLayout.execute(parent);
				});
				document.body.insertBefore(button, container);

				// Adds a button to execute the layout
				var button = document.createElement('button');
				mxUtils.write(button, 'Parallel Edge');
				mxEvent.addListener(button, 'click', function(evt)
				{
					parallelEdgeLayout.execute(parent);
				});
				document.body.insertBefore(button, container);

				// Adds a button to execute the layout
				var button = document.createElement('button');
				mxUtils.write(button, 'Partition');
				mxEvent.addListener(button, 'click', function(evt)
				{
					partitionLayout.execute(parent);
				});
				document.body.insertBefore(button, container);


				// Adds a button to execute the layout
				var button = document.createElement('button');
				mxUtils.write(button, 'Hierarchical');
				mxEvent.addListener(button, 'click', function(evt)
				{
					hierarchicalLayout.execute(parent);
				});
				document.body.insertBefore(button, container);

				// Adds a button to execute the layout
				var button = document.createElement('button');
				mxUtils.write(button, 'Organic');

				mxEvent.addListener(button, 'click', function(evt)
				{
					organicLayout.execute(parent);
				});

				document.body.insertBefore(button, container);

				// Adds a button to execute the layout
				var button = document.createElement('button');
				mxUtils.write(button, 'Radial Tree');

				mxEvent.addListener(button, 'click', function(evt)
				{
					radialTreeLayout.execute(parent);
				});

				document.body.insertBefore(button, container);

				// Adds a button to execute the layout
				var button = document.createElement('button');
				mxUtils.write(button, 'Compact Tree');

				mxEvent.addListener(button, 'click', function(evt)
				{
					compactTreeLayout.execute(parent);
				});

				document.body.insertBefore(button, container);

				// Adds cells to the model in a single step
				graph.getModel().beginUpdate();
				try
				{

${vertexes}
${edges}

//					var v0 = graph.insertVertex(parent, null, 'type', 20, 20, 150, 40);
//					var v1 = graph.insertVertex(parent, null, 'ComplexType', 20, 20, 150, 40);
//					var v2 = graph.insertVertex(parent, null, 'JavaClass', 200, 150, 150, 40);
//					var v3a = graph.insertVertex(parent, null, 'JavaClassTarget', 400, 250, 150, 40);
//					var v3b = graph.insertVertex(parent, null, 'JavaClassTarget2', 400, 250, 150, 40);
//					var v3c = graph.insertVertex(parent, null, 'JavaClassTarget3', 400, 250, 150, 40);
//
//					var v4 = graph.insertVertex(parent, null, 'AnotherJavaClass', 400, 250, 150, 40);
//					var v5 = graph.insertVertex(parent, null, 'YetAnotherJavaClass', 400, 250, 150, 40);
//
//					var e0 = graph.insertEdge(parent, null, 'ComplexTypeConverter', v0, v1, 'verticalLabelPosition=bottom;');
//					var e1a = graph.insertEdge(parent, null, 'ComplexTypeToJavaClassConverter', v1, v2, 'verticalLabelPosition=bottom;');
//					var e1b = graph.insertEdge(parent, null, 'ComplexTypeToJavaClassConverter', v1, v4, 'verticalLabelPosition=bottom;');
//					var e1c = graph.insertEdge(parent, null, 'ComplexTypeToJavaClassConverter', v1, v5, 'verticalLabelPosition=bottom;');
//					var e2a = graph.insertEdge(parent, null, 'JavaClassWriter', v2, v3a, 'verticalLabelPosition=bottom;');
//					var e2b = graph.insertEdge(parent, null, 'JavaClassWriter', v4, v3b, 'verticalLabelPosition=bottom;');
//					var e2c = graph.insertEdge(parent, null, 'JavaClassWriter', v5, v3c, 'verticalLabelPosition=bottom;');

//					var v1_v2 = graph.insertVertex(parent, null, 'ComplexTypeToJavaClassConverter', 20, 20, 180, 30, "shape=ellipse;fillColor=green;");
//					var v2_v3 = graph.insertVertex(parent, null, 'JavaClassWriter', 20, 20, 180, 30, "shape=ellipse;fillColor=green;");

//					var e1 = graph.insertEdge(parent, null, '', v1, v1_v2);
//					var e2 = graph.insertEdge(parent, null, '', v1_v2, v2);
//					var e3 = graph.insertEdge(parent, null, '', v2, v2_v3);
//					var e4 = graph.insertEdge(parent, null, '', v2_v3, v3);

//					var e1 = graph.insertEdge(parent, null, 'ComplexTypeToJavaClassConverter', v1, v2);
//					var e2 = graph.insertEdge(parent, null, 'Generation', v2, v3);

					// Executes the layout
					hierarchicalLayout.execute(parent);
				}
				finally
				{
					// Updates the display
					graph.getModel().endUpdate();
				}

				if (mxClient.IS_QUIRKS)
				{
					document.body.style.overflow = 'hidden';
					new mxDivResizer(container);
				}

//				var content = document.createElement('div');
//				content.style.padding = '4px';
//
//				var tb = new mxToolbar(content);
//
//				tb.addItem('Zoom In', 'images/zoom_in32.png',function(evt)
//				{
//					graph.zoomIn();
//				});
//
//				tb.addItem('Zoom Out', 'images/zoom_out32.png',function(evt)
//				{
//					graph.zoomOut();
//				});
//
//				tb.addItem('Actual Size', 'images/view_1_132.png',function(evt)
//				{
//					graph.zoomActual();
//				});
//
//				wnd = new mxWindow('Tools', content, 0, 0, 200, 66, false);
//				wnd.setMaximizable(false);
//				wnd.setScrollable(false);
//				wnd.setResizable(false);
//				wnd.setVisible(true);
			}
		}
}""";
        return result;
	}
}
