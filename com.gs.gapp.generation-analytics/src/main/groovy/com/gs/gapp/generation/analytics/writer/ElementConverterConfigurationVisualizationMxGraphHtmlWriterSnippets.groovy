package com.gs.gapp.generation.analytics.writer

class ElementConverterConfigurationVisualizationMxGraphHtmlWriterSnippets {

	public static String getMxGraphHtml(String graphName, String nameOfMxGraphJavaScriptFile, String nameOfJavaScriptFile) {
		String result =
"""<html>
<head>
	<title>Visualization of the element converter tree ${graphName}</title>

	<!-- Loads and initializes the libraries -->
	<script type="text/javascript" src="../../js/${nameOfMxGraphJavaScriptFile}"></script>
	<script type="text/javascript" src="./${nameOfJavaScriptFile}"></script>
</head>

<!-- Page passes the container for the graph to the program -->
<body onload="${graphName}.show(document.getElementById('graphContainer'))">
	<div id="outlineContainer"
		style="z-index:1;position:absolute;overflow:hidden;top:0px;right:0px;width:260px;height:120px;background:transparent;border-style:solid;border-color:lightgray;">
	</div>
	<div
	    id="graphContainer"
	    style="position:relative;overflow:hidden;width:100%;height:100%;top:120px;cursor:default;">
	</div>
</body>
</html>""";
        return result;
	}
}
