package com.gs.gapp.generation.analytics.writer

class ModelElementTreeVisualizationDOTBatchWriterSnippets {

	public static String getExecDotBatch() {
		String result =
"""
@echo on

set DOT=D:\\Software\\Graphviz2.30\\bin\\dot.exe

md images

for %%f in (.\\*.dot) do (
    echo %%f
    %DOT% -Tpng -o.\\images\\%%f.png %%f
)

pause
""";
        return result;
	}
}
