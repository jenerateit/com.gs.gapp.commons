package com.gs.gapp.metamodel.product;

import java.util.Collection;
import java.util.LinkedHashSet;

import com.gs.gapp.metamodel.basic.AbstractModelFilter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelFilterI;
import com.gs.gapp.metamodel.function.BusinessException;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionMetamodel;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.Service;
import com.gs.gapp.metamodel.function.ServiceClient;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.function.ServiceInterface;
import com.gs.gapp.metamodel.function.TechnicalException;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.Namespace;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.gs.gapp.metamodel.persistence.enums.PersistenceMetamodel;

/**
 * The consumed services filter removes all model elements that are types of the Persistence metamodel
 * or the Function metamodel in case those elements do not belong to {@link Capability#getConsumedServices()}.
 * 
 * Note that if the passed collection of model elements does not contain any capability, no
 * filtering takes place whatsoever.
 * 
 * @author marcu
 *
 */
public class ConsumedServicesFilter extends AbstractModelFilter implements ModelFilterI {
	
	public ConsumedServicesFilter() {}

	@Override
	public Collection<?> filter(Collection<?> modelElements) {
		Collection<Capability> capabilities = getElementsForMetatype(Capability.class, modelElements);
		if (capabilities.size() == 0) return modelElements;  // capabilities are the key factor for filtering, so, when there are none => don't filter
		
		Collection<Object> result = new LinkedHashSet<>();
		Collection<?> mandatoryElements = AbstractModelFilter.extractMandatoryElements(modelElements);
		LinkedHashSet<ServiceClient> serviceClients = new LinkedHashSet<>();
		for (Capability capability : capabilities) {
			serviceClients.addAll(capability.getConsumedServices());
		}
		
		for (Object element : modelElements) {
			if (element instanceof ModelElementI) {
				ModelElementI modelElement = (ModelElementI) element;
				
				if (PersistenceMetamodel.INSTANCE.isConversionChecked(modelElement.getClass())) {
					addPersistenceModelElement(result, serviceClients, modelElement);
				} else if (FunctionMetamodel.INSTANCE.isConversionChecked(modelElement.getClass())) {
					addFunctionModelElement(result, serviceClients, modelElement);
				} else {
					result.add(element);	
				}
				
			} else {
				result.add(element);
			}
		}
		
		result.addAll(mandatoryElements);
		return result;
	}
	
	/**
	 * @param result
	 * @param serviceClientElements
	 * @param modelElement
	 * @return true in case the element got added
	 */
	private boolean addPersistenceModelElement(Collection<Object> result, LinkedHashSet<ServiceClient> serviceClientElements, ModelElementI modelElement) {
		Entity entity = null;
		Namespace persistenceNamespace = null;
		PersistenceModule persistenceModule = null;
		if (modelElement instanceof Entity) {
			entity = (Entity) modelElement;
		} else if (modelElement instanceof Namespace) {
			persistenceNamespace = (Namespace) modelElement;
		} else if (modelElement instanceof PersistenceModule) {
			persistenceModule = (PersistenceModule) modelElement;
		}
		
		boolean included = false;
		for (ServiceClient serviceClient : serviceClientElements) {
			if (persistenceModule != null && serviceClient.getServiceInterface().getPersistenceModules().contains(persistenceModule)) {
				included = true;
				break;
			}
			if (entity != null && serviceClient.getServiceInterface().contains(entity)) {
				included = true;
				break;
			}
			if (persistenceNamespace != null) {
				for (PersistenceModule aPersistenceModule : serviceClient.getServiceInterface().getPersistenceModules()) {
					if (aPersistenceModule.getNamespace() == persistenceNamespace) {
						included = true;
						break;
					}
				}
				if (included) break;
			}
		}
		
		if (included) {
			result.add(modelElement);
		} else {
//			System.out.println("model element " + modelElement + " is filtered"); 
		}
		
		return included;
	}
	
	/**
	 * @param result
	 * @param serviceClientElements
	 * @param modelElement
	 * @return true in case the element got added
	 */
	private boolean addFunctionModelElement(Collection<Object> result, LinkedHashSet<ServiceClient> serviceClientElements, ModelElementI modelElement) {
		Function function = null;
		com.gs.gapp.metamodel.function.Namespace functionNamespace = null;
		FunctionModule functionModule = null;
		@SuppressWarnings("unused")
		Service service = null;
		@SuppressWarnings("unused")
		ServiceImplementation serviceImplementation = null;
		@SuppressWarnings("unused")
		ServiceInterface serviceInterface = null;
		ServiceClient serviceClientToCheck = null;
		@SuppressWarnings("unused")
		BusinessException businessException = null;
		@SuppressWarnings("unused")
		TechnicalException technicalException = null;
		
		if (modelElement instanceof Function) {
			function = (Function) modelElement;
		} else if (modelElement instanceof com.gs.gapp.metamodel.function.Namespace) {
			functionNamespace = (com.gs.gapp.metamodel.function.Namespace) modelElement;
		} else if (modelElement instanceof FunctionModule) {
			functionModule = (FunctionModule) modelElement;
		} else if (modelElement instanceof ServiceClient) {
			serviceClientToCheck = (ServiceClient) modelElement;
		}
		
		boolean included = false;
		for (ServiceClient serviceClient : serviceClientElements) {
			if (functionModule != null) {
				if (serviceClient.getServiceInterface().getFunctionModules().contains(functionModule)) {
				    included = true;
				    break;
				} else if (functionModule.getOriginatingElement(Entity.class) != null) {
					// the given function module got created on-the-fly, for a given entity
					boolean entityIncluded = addPersistenceModelElement(result, serviceClientElements, functionModule.getOriginatingElement(Entity.class));
					included = entityIncluded;  // The on-the-fly created function module needs to be included, too. We want to generate something for them.
					if (included) {
						break;
					}
				}
			}
			
			if (function != null && serviceClient.getServiceInterface().contains(function)) {
				included = true;
				break;
			}
			
			if (functionNamespace != null && serviceClient.getServiceInterface().contains(functionNamespace)) {
				included = true;
				break;
			}
			
			if (serviceClientToCheck != null && serviceClient == serviceClientToCheck) {
				included = true;
				break;
			}
		}
		
		if (included) {
			result.add(modelElement);
		} else {
//			if ("GenerationClient".equalsIgnoreCase(modelElement.getName())) {
//				int a = 1;
//			}
//			System.out.println("model element " + modelElement + " is filtered"); 
		}
		
		return included;
	}
}
