/**
 *
 */
package com.gs.gapp.metamodel.product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.Placement;
import com.gs.gapp.metamodel.ui.ContainerFlow;
import com.gs.gapp.metamodel.ui.UIModule;
import com.gs.gapp.metamodel.ui.component.UIActionComponent;
import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.component.UIEmbeddedContainer;
import com.gs.gapp.metamodel.ui.container.UIContainer;
import com.gs.gapp.metamodel.ui.container.UIMenuContainer;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;
import com.gs.gapp.metamodel.ui.databinding.FunctionUsage;

/**
 * A UI application has the purpose to provide an interface for human beings
 * to work with data. A UI application can be implemented with 2-/3- or 4-tier
 * architectures. It for instance can result in a web or desktop application.
 * 
 * @author mmt
 *
 */
public class UiApplication extends AbstractApplication {

	/**
	 *
	 */
	private static final long serialVersionUID = 939747746554605578L;
	
	private static UIModuleComparator uiModuleComparator = new UIModuleComparator();
	
	private UIContainer rootContainer;

	private final Set<UIMenuContainer> applicationMenus = new LinkedHashSet<>();

	private final Set<FunctionUsage> functionUsage = new LinkedHashSet<>();
	
	private final Set<LayoutAreaEnum> renderedLayoutAreas = new LinkedHashSet<>();
	
	private final Set<LayoutAreaEnum> customizedLayoutAreas = new LinkedHashSet<>();
	
	private final Set<UIModule> uiModules = new LinkedHashSet<>();
	
	private final Set<Placement> applicationMenuPlacements = new LinkedHashSet<>();
	
	private final Set<Placement> viewMenuPlacements = new LinkedHashSet<>();
	
	private boolean offlineCapable;

	/**
	 * @param name
	 */
	public UiApplication(String name) {
		super(name);
	}
	
	/**
	 * @return the rootContainer
	 */
	public UIContainer getRootContainer() {
		return rootContainer;
	}

	/**
	 * @param rootContainer the rootContainer to set
	 */
	public void setRootContainer(UIContainer rootContainer) {
		this.rootContainer = rootContainer;
	}

	/**
	 * @return the applicationMenu
	 */
	public Set<UIMenuContainer> getApplicationMenus() {
		return applicationMenus;
	}

	/**
	 * @param applicationMenu the applicationMenu to set
	 */
	public boolean addApplicationMenu(UIMenuContainer applicationMenu) {
		return this.applicationMenus.add(applicationMenu);
	}

	/**
	 * Collects all data containers that are directly managed by this application.
	 *
	 * @return
	 */
	public Set<UIDataContainer> getManagedDataContainers() {
		Set<UIDataContainer> result = new LinkedHashSet<>();

		if (rootContainer != null) {
			if (rootContainer instanceof UIDataContainer) {
				UIDataContainer dataContainer = (UIDataContainer) rootContainer;
				result.add(dataContainer);
				for (UIComponent component : dataContainer.getComponents()) {
					if (component instanceof UIEmbeddedContainer) {
						UIEmbeddedContainer embeddedContainer = (UIEmbeddedContainer) component;
						result.addAll(embeddedContainer.getAllDataContainers());
					}
				}
			} else if (rootContainer instanceof UIStructuralContainer) {
				result.addAll( ((UIStructuralContainer)rootContainer).getAllDataContainers() );
			}
		}

		return result;
	}

	/**
	 * @return
	 */
	public Set<UIContainer> getAllContainers() {
		Set<UIContainer> result = new LinkedHashSet<>();

		if (rootContainer != null) {
			result.add(rootContainer);
			if (rootContainer instanceof UIDataContainer) {
				result.addAll( ((UIDataContainer)rootContainer).getAllContainers() );
			} else if (rootContainer instanceof UIStructuralContainer) {
				result.addAll( ((UIStructuralContainer)rootContainer).getAllContainers() );
			}
		}

		return result;
	}

	/**
	 * @return
	 */
	public Set<ContainerFlow> getAllContainerFlows() {
		Set<ContainerFlow> result = new LinkedHashSet<>();
		for (UIContainer container : getAllContainers()) {
			result.addAll(container.getFlows());
		}
		return result;
	}

	/**
	 * @param actionComponent
	 * @return
	 */
	public Set<ContainerFlow> getAllContainerFlows(UIActionComponent actionComponent) {
		Set<ContainerFlow> result = new LinkedHashSet<>();
		for (ContainerFlow containerFlow : getAllContainerFlows()) {
			if (containerFlow.getActionComponents().contains(actionComponent)) {
				result.add(containerFlow);
			}
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<UIStructuralContainer> getAllStructuralContainers() {
		Set<UIStructuralContainer> result = new LinkedHashSet<>();
		for (UIModule uiModule : getUiModulesForGeneration()) {
			result.addAll(uiModule.getElements(UIStructuralContainer.class));
		}
		return result;
	}

	/**
	 * @return the functionUsage
	 */
	public Set<FunctionUsage> getFunctionUsage() {
		return Collections.unmodifiableSet(functionUsage);
	}

	/**
	 * @param functionUsage
	 */
	public void addFunctionUsage(FunctionUsage functionUsage) {
		this.functionUsage.add(functionUsage);
	}
	
	public Set<LayoutAreaEnum> getRenderedLayoutAreas() {
		return renderedLayoutAreas;
	}
	
	public boolean addRenderedLayoutArea(LayoutAreaEnum layoutArea) {
		return this.renderedLayoutAreas.add(layoutArea);
	}

	public Set<LayoutAreaEnum> getCustomizedLayoutAreas() {
		return customizedLayoutAreas;
	}
	
	public boolean addCustomizedLayoutArea(LayoutAreaEnum layoutArea) {
		return this.customizedLayoutAreas.add(layoutArea);
	}

	public Set<UIModule> getUiModules() {
		return uiModules;
	}
	
	public boolean addUiModule(UIModule uiModule) {
		return this.uiModules.add(uiModule);
	}
	
	protected Set<Placement> getApplicationMenuPlacements() {
		return applicationMenuPlacements;
	}
	
	public boolean addApplicationMenuPlacement(Placement placement) {
		return this.applicationMenuPlacements.add(placement);
	}

	protected Set<Placement> getViewMenuPlacements() {
		return viewMenuPlacements;
	}
	
	public boolean addViewMenuPlacement(Placement placement) {
		return this.viewMenuPlacements.add(placement);
	}

	public boolean isOfflineCapable() {
		return offlineCapable;
	}

	public void setOfflineCapable(boolean offlineCapable) {
		this.offlineCapable = offlineCapable;
	}

	/**
	 * 
	 * @return all UIModule instances that have to be generated, ordered by name (ascending)
	 */
	public List<UIModule> getUiModulesForGeneration() {
		
		// this method had been added to fix APPJSF-438
		
		HashSet<UIModule> resultAsSet = new HashSet<UIModule>();
		LinkedHashSet<Capability> capabilitiesWithIndividualModules = new LinkedHashSet<Capability>();
		
		if (uiModules.isEmpty()) {
			// --- no explicitly assigned ui modules => _all_ modules have to be used (this is kind of a default setting then)
			for (Capability capability : getCapabilities()) {
				for (Module module : capability.getInterfaceModules()) {
					if (module instanceof UIModule) {
				        resultAsSet.add((UIModule) module);
					}
				}
				resultAsSet.addAll(capability.getUiModules());
			}
		} else {
			for (UIModule uiModule : uiModules) {
				for (Capability capability : getCapabilities()) {
					if (capability.getUiModules().contains(uiModule) || capability.getInterfaceModules().contains(uiModule)) {
						capabilitiesWithIndividualModules.add(capability);
					}
				}
				resultAsSet.add(uiModule);
			}
			ArrayList<Capability> capabilitiesWithNoModuleAssigned = new ArrayList<>(getCapabilities());
			capabilitiesWithNoModuleAssigned.removeAll(capabilitiesWithIndividualModules);
			
			for (Capability capability : capabilitiesWithNoModuleAssigned) {
				for (Module module : capability.getInterfaceModules()) {
					if (module instanceof UIModule) {
				        resultAsSet.add((UIModule) module);
					}
				}
				resultAsSet.addAll(capability.getUiModules());
			}
		}
		
		ArrayList<UIModule> resultAsList = new ArrayList<>(resultAsSet);
		Collections.sort(resultAsList, uiModuleComparator);

		return resultAsList;
	}
	
	/**
	 * @author mmt
	 *
	 */
	private static class UIModuleComparator implements Comparator<UIModule> {

		@Override
		public int compare(UIModule module1, UIModule module2) {
			if (module1 == null && module2 != null) {
				return 1;
			} else if (module1 != null && module2 == null) {
				return -1;
			}
			return module1.getName().compareTo(module2.getName());
		}
	}


	public static enum LayoutAreaEnum {
		
		NORTH ("North"),
		EAST ("East"),
		SOUTH ("South"),
		WEST ("West"),
		CENTER ("Center"),
		CENTER_NORTH ("Center.North"),
		CENTER_CENTER ("Center.Center"),
		;
		
		
	    private static final Map<String, LayoutAreaEnum> stringToEnum = new HashMap<>();
	    private static final List<LayoutAreaEnum> defaultLayoutAreas = new ArrayList<>();
		
		static {
			for (LayoutAreaEnum m : values()) {
				stringToEnum.put(m.getName(), m);
			}
			
			defaultLayoutAreas.add(NORTH);
			defaultLayoutAreas.add(WEST);
			defaultLayoutAreas.add(CENTER_NORTH);
			defaultLayoutAreas.add(CENTER_CENTER);
		}
		
		/**
		 * @param name
		 * @return
		 */
		public static LayoutAreaEnum fromString(String name) {
			return stringToEnum.get(name);
		}
		
		private final String name;
		
		private LayoutAreaEnum(String name) {
			this.name=name;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}
		
		/**
		 * @return
		 */
		public static List<String> getNames() {
			List<String> result = new ArrayList<>();
			
			for (LayoutAreaEnum entry : values()) {
				result.add(entry.name());
			}
			
			return result;
		}
		
		public static List<LayoutAreaEnum> getDefaultLayoutAreas() {
			return defaultLayoutAreas;
		}
	}
}
