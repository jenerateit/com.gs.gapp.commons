package com.gs.gapp.metamodel.product;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.function.FunctionModule;

/**
 * This validator checks whether there are entities that
 * have multiple entity fields with the same name.
 * 
 * @author marcu
 *
 */
public class ValidatorUniqueResourcePaths implements ModelValidatorI {

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(assertNoDuplicateResourcePaths(modelElements));
		return result;
	}

	/**
	 * @param rawElements
	 * @return
	 */
	private Collection<Message> assertNoDuplicateResourcePaths(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();
		
		Set<String> paths = new HashSet<>();
		Map<String,Integer> resourcePathsMap = new LinkedHashMap<>();
		for (Object element : rawElements) {
			if (element instanceof FunctionModule) {
				FunctionModule functionModule = (FunctionModule) element;
				OptionDefinition<String>.OptionValue path = functionModule.getOption(RestOptionEnum.PATH.getName(), String.class);
				
				if (path != null && path.getOptionValue() != null && path.getOptionValue().length() > 0) {
					if (!paths.contains(path.getOptionValue())) {
						paths.add(path.getOptionValue());
					} else {
						if (!resourcePathsMap.containsKey(path.getOptionValue())) {
							resourcePathsMap.put(path.getOptionValue(), 2);
						} else {
							int counter = resourcePathsMap.get(path.getOptionValue());
							counter++;
							resourcePathsMap.put(path.getOptionValue(), counter);
						}
					}
				}
			}
		}

		// note that there are only entries in resource paths map where there is more than one occurrence of a path
		for (String path : resourcePathsMap.keySet()) {
			Integer counter = resourcePathsMap.get(path);
			Message message = ProductMessage.RESOURCE_PATH_NOT_UNIQUE.getMessage(path, counter);
			result.add(message);
		}

		return result;
	}
}
