package com.gs.gapp.metamodel.product;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;

/**
 * @author marcu
 *
 */
public class ValidatorTestPreludeViewNotPartOfApp implements ModelValidatorI {

	public ValidatorTestPreludeViewNotPartOfApp() {}

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		
		List<UiApplication> applications = modelElements.stream().filter(element -> element instanceof UiApplication)
		.map(element -> (UiApplication)element)
		.collect(Collectors.toList());
		
		applications.stream().forEach(application -> {
			final Set<UIStructuralContainer> structuralContainers = application.getAllStructuralContainers();
			
			structuralContainers.stream()
				.filter(structuralContainer -> structuralContainer.isMainContainer() &&
						structuralContainer.getTestPreludeViews() != null &&
						!structuralContainer.getTestPreludeViews().isEmpty())
				.forEach(structuralContainer -> {
					structuralContainer.getTestPreludeViews().forEach(preludeView -> {
						if (!structuralContainers.contains(preludeView)) {
							// There is a prelude view assignment for a view that actually is not part of the application.
							Message message = ProductMessage.TEST_PRELUDE_VIEW_NOT_PART_OF_APP.getMessage(
									structuralContainer.getName(),
									structuralContainer.getModule().getName(),
									preludeView.getName(),
									preludeView.getModule().getName(),
									application.getName());
							result.add(message);
						}
					});
				});
		});
		
		return result;
	}
}
