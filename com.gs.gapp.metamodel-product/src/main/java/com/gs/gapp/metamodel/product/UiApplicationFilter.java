package com.gs.gapp.metamodel.product;

import java.util.Collection;
import java.util.LinkedHashSet;

import com.gs.gapp.metamodel.basic.AbstractModelFilter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelFilterI;

/**
 * The ui application filter removes all model elements that are types of the Ui metamodel
 * in case those elements do not belong to {@link Capability#getUiApplications()}.
 * 
 * Note that if the passed collection of model elements does not contain any capability, no
 * filtering takes place whatsoever.
 * 
 * @author marcu
 *
 */
public class UiApplicationFilter extends AbstractModelFilter implements ModelFilterI {

	private final int uiApplicationIndex;
	
	public UiApplicationFilter() {
		this.uiApplicationIndex = -1;
	}
	
	public UiApplicationFilter(int uiApplicationIndex) {
		this.uiApplicationIndex = uiApplicationIndex;
	}

	@Override
	public Collection<?> filter(Collection<?> modelElements) {
		Collection<Capability> capabilities = getElementsForMetatype(Capability.class, modelElements);
		if (capabilities.size() == 0) return modelElements;  // capabilities are the key factor for filtering, so, when there are none => don't filter
		
		Collection<Object> result = new LinkedHashSet<>();
		Collection<?> mandatoryElements = AbstractModelFilter.extractMandatoryElements(modelElements);
		LinkedHashSet<UiApplication> omittedApplications = new LinkedHashSet<>();
		LinkedHashSet<UiApplication> uiApplications = new LinkedHashSet<>();
		for (Capability capability : capabilities) {
			int ii = 0;
			for (UiApplication uiApplication : capability.getUiApplications()) {
				if (getUiApplicationIndex() < 0 || getUiApplicationIndex() == ii) {
			        uiApplications.add(uiApplication);
				} else {
					omittedApplications.add(uiApplication);
				}
			    ii++;
			}
		}
		
		for (Object element : modelElements) {
			if (element instanceof ModelElementI) {
				ModelElementI modelElement = (ModelElementI) element;
				
				if (modelElement instanceof UiApplication) {
					UiApplication uiApplication = (UiApplication) modelElement;
					if (uiApplications.contains(uiApplication)) {
						result.add(uiApplication);
					}
				} else {
					result.add(element);	
				}
			} else {
				result.add(element);
			}
		}
		
		result.addAll(mandatoryElements);
		return result;
	}
	

	public int getUiApplicationIndex() {
		return uiApplicationIndex;
	}
}
