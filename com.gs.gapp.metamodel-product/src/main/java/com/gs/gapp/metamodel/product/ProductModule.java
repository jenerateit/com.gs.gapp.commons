/**
 * 
 */
package com.gs.gapp.metamodel.product;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.Module;


/**
 * @author mmt
 *
 */
public class ProductModule extends Module {

	private static final long serialVersionUID = 3472023271336071158L;

	/**
	 * @param name
	 */
	public ProductModule(String name) {
		super(name);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#getNamespace()
	 */
	@Override
	public Namespace getNamespace() {
		return (Namespace) super.getNamespace();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#setNamespace(com.gs.gapp.metamodel.basic.Namespace)
	 */
	@Override
	public void setNamespace(com.gs.gapp.metamodel.basic.Namespace namespace) {
		if (!(namespace instanceof Namespace)) {
			throw new IllegalArgumentException("the namespace of a ProductModule must be an instance of com.gs.gapp.metamodel.product.Namespace");
		}
		super.setNamespace(namespace);
	}
	
	/**
	 * @return
	 */
	public Set<AbstractApplication> getApplications() {
		Set<AbstractApplication> result = new LinkedHashSet<>();

		for (ModelElement modelElement : getElements()) {
			if (modelElement instanceof AbstractApplication) {
				result.add((AbstractApplication) modelElement);
			} else if (modelElement instanceof com.gs.gapp.metamodel.product.Namespace) {
				result.addAll(((com.gs.gapp.metamodel.product.Namespace)modelElement).getApplications());
			}
		}

		return result;
	}

}
