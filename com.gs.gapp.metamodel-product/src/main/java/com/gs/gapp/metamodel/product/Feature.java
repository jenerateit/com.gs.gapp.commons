/**
 * 
 */
package com.gs.gapp.metamodel.product;

/**
 * @author mmt
 *
 */
public class Feature extends AbstractProductModelElement {

	private static final long serialVersionUID = 9028097676882169471L;
	
	private boolean active = true;
	
	/**
	 * @param name
	 */
	public Feature(String name) {
		super(name);
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
}
