/**
 * 
 */
package com.gs.gapp.metamodel.product;

/**
 * @author mmt
 *
 */
public class Organization extends AbstractProductModelElement {

	private static final long serialVersionUID = 5130241448042594357L;

	/**
	 * @param name
	 */
	public Organization(String name) {
		super(name);
	}
}
