package com.gs.gapp.metamodel.product;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs.gapp.metamodel.basic.ModelValidatorI;

/**
 * @author marcu
 *
 */
public class ValidatorApplications implements ModelValidatorI {

	/**
	 *
	 */
	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		final Collection<Message> result = new LinkedHashSet<>();
		
        Set<Capability> capabilities = modelElements.stream()
            .filter(Capability.class::isInstance)
            .map(Capability.class::cast)
            .collect(Collectors.toCollection(LinkedHashSet::new));
		
		List<AbstractApplication> applications = modelElements.stream()
			.filter(AbstractApplication.class::isInstance)
			.map(AbstractApplication.class::cast)
			.filter(application -> application.getCapabilities().isEmpty())
			.collect(Collectors.toList());
		
		if (!capabilities.isEmpty()) {
			// Only check application assignment if there actually are capabilities in the incoming model.
			result.addAll(applications.stream()
			    .map(application -> ProductMessage.ERROR_APPLICATION_WITHOUT_CAPABILITY_ASSIGNMENT.getMessageBuilder()
			    		.modelElement(application)
			    		.parameters(application.getName())
			    		.build())
			    .collect(Collectors.toCollection(LinkedHashSet::new)));
		}
		
		return result;
	}
}
