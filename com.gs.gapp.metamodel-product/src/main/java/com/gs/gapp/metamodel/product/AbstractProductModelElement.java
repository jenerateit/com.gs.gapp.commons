/**
 * 
 */
package com.gs.gapp.metamodel.product;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * @author mmt
 *
 */
public abstract class AbstractProductModelElement extends ModelElement {

	private static final long serialVersionUID = -2973142159468241232L;
	
	/**
	 * @param name
	 */
	public AbstractProductModelElement(String name) {
		super(name);
	}
}
