/**
 * 
 */
package com.gs.gapp.metamodel.product;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author mmt
 *
 */
public class ProductVariant extends AbstractProductModelElement {

	private static final long serialVersionUID = 4736729569173111270L;
	
	private Product product;
	
	private final Set<Capability> capabilities = new LinkedHashSet<>();
	
	private final Set<Feature> features = new LinkedHashSet<>();
	
	private final Set<UiApplication> uiApplications = new LinkedHashSet<>();

	private final Set<ServiceApplication> serviceApplications = new LinkedHashSet<>();
	
	private final Set<IotGatewayApplication> iotGatewayApplications = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public ProductVariant(String name) {
		super(name);
	}

	public Product getProduct() {
		return product;
	}

	public Set<Capability> getCapabilities() {
		return capabilities;
	}
	
	public boolean addCapability(Capability capability) {
		return this.capabilities.add(capability);
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	public Set<Feature> getFeatures() {
		return features;
	}

	public boolean addFeature(Feature feature) {
		return this.features.add(feature);
	}
	
	public Set<UiApplication> getUiApplications() {
		return uiApplications;
	}
	
	public boolean addUiApplication(UiApplication application) {
		return this.uiApplications.add(application);
	}
	
	public Set<ServiceApplication> getServiceApplications() {
		return serviceApplications;
	}
	
	public boolean addServiceApplication(ServiceApplication application) {
		return this.serviceApplications.add(application);
	}
	
	public Set<IotGatewayApplication> getIotGatewayApplications() {
		return iotGatewayApplications;
	}
	
	public boolean addIotGatewayApplication(IotGatewayApplication application) {
		return this.iotGatewayApplications.add(application);
	}
}
