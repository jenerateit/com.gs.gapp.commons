/**
 * 
 */
package com.gs.gapp.metamodel.product;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author mmt
 *
 */
public class Product extends AbstractProductModelElement {

	private static final long serialVersionUID = 9028097676882169471L;
	
	private Organization organization;
	
	private final Set<Capability> capabilities = new LinkedHashSet<>();
	
	private final Set<Feature> features = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public Product(String name) {
		super(name);
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	
	public Set<Capability> getCapabilities() {
		return capabilities;
	}
	
	public boolean addCapability(Capability capability) {
		return this.capabilities.add(capability);
	}
	
	public Set<Feature> getFeatures() {
		return features;
	}
	
	public boolean addFeature(Feature feature) {
		return this.features.add(feature);
	}
}
