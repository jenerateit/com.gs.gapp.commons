package com.gs.gapp.metamodel.product;

import com.gs.gapp.metamodel.basic.MessageI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.MessageStatus;

public enum ProductMessage implements MessageI {

	RESOURCE_PATH_NOT_UNIQUE (MessageStatus.ERROR, "0001",
			"Ambiguous resource path '%s' found, %d occurrences.",
			"Modify the model to make sure that the resource paths are unique in the generated code."),
	
	TEST_PRELUDE_VIEW_NOT_PART_OF_APP (MessageStatus.WARNING, "0002",
			"View '%s' in module '%s' has the test prelude view '%s' in module '%s' assigned, but that view is not part of the application '%s' that is going to be generated.",
			"You don't need to change anything. This doesn't stop your application from working. But you don't get code generated that opens the test prelude view."),
	
	ERROR_APPLICATION_WITHOUT_CAPABILITY_ASSIGNMENT (MessageStatus.ERROR, "0003",
			"Found an application '%s' that is not used in any capability.",
			"Please complete the model by assigning an application to at least one capability."),
	;

	private final MessageStatus status;
	private final String organization = "GS";
	private final String section = "PRODUCT";
	private final String id;
	private final String problemDescription;
	private final String instruction;
	
	private ProductMessage(MessageStatus status, String id, String problemDescription, String instruction) {
		this.status = status;
		this.id = id;
		this.problemDescription = problemDescription;
		this.instruction = instruction;
	}
	
	@Override
	public MessageStatus getStatus() {
		return status;
	}

	@Override
	public String getOrganization() {
		return organization;
	}

	@Override
	public String getSection() {
		return section;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getProblemDescription() {
		return problemDescription;
	}
	
	@Override
	public String getInstruction() {
		return instruction;
	}
}
 