/**
 * 
 */
package com.gs.gapp.metamodel.product;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.Namespace;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.function.BusinessLogic;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.ServiceClient;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.function.Storage;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.gs.gapp.metamodel.ui.UIModule;
import com.gs.gapp.metamodel.ui.container.UIContainer;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * @author mmt
 *
 */
public class Capability extends AbstractProductModelElement {

	private static final long serialVersionUID = -5666385565611123192L;
	
	private final Set<Feature> features = new LinkedHashSet<>();
	
	private final Set<Module> interfaceModules = new LinkedHashSet<>();
	private final Set<Module> internalModules = new LinkedHashSet<>();

	private Enumeration configurationItems;
	
	private final Set<ServiceImplementation> services = new LinkedHashSet<>();
	
	private final Set<UIModule> uiModules = new LinkedHashSet<>();
	
	private final Set<Storage> storages = new LinkedHashSet<>();
	
	private Storage defaultStorage;
	
	private final Set<ServiceClient> consumedServices = new LinkedHashSet<>();
	
	private final Set<BusinessLogic> businessLogic = new LinkedHashSet<>();
	
	private BusinessLogic defaultBusinesslogic;
	
	private final Set<UiApplication> uiApplications = new LinkedHashSet<>();
	
	private final Set<ServiceApplication> serviceApplications = new LinkedHashSet<>();
	
	private final Set<IotGatewayApplication> iotGatewayApplications = new LinkedHashSet<>();
	
	private final Set<Capability> extendedCapabilities = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public Capability(String name) {
		super(name);
	}

	/**
	 * TODO remove this method and replace calls with getUiModules() (mmt 27-Nov-2018)
	 * 
	 * @deprecated calls to this method are going to be replaced by calls to getUiModules() 
	 * @return
	 * 
	 */
	@Deprecated
	public Set<Module> getInterfaceModules() {
		return interfaceModules;
	}
	
	public boolean addInterfaceModule(Module module) {
		return this.interfaceModules.add(module);
	}

	/**
	 * TODO remove this method and replace calls with calls to other getter of Capability (mmt 27-Nov-2018)
	 * 
	 * @deprecated calls to this method are going to be replaced by calls to getUiModules()
	 * @return
	 */
	@Deprecated
	public Set<Module> getInternalModules() {
		return internalModules;
	}
	
	public boolean addInternalModule(Module module) {
		return this.internalModules.add(module);
	}
	
	public Set<Feature> getFeatures() {
		return features;
	}
	
	public boolean addFeature(Feature feature) {
		return this.features.add(feature);
	}

	public Enumeration getConfigurationItems() {
		return configurationItems;
	}

	public void setConfigurationItems(Enumeration configurationItems) {
		this.configurationItems = configurationItems;
	}

	/**
	 * A service implementation of the INTERNAL category is a service that can only
	 * be called from other components within the same capability, running in the same
	 * process.
	 * 
	 * @return
	 */
	public Set<ServiceImplementation> getServices() {
		return services;
	}
	
	public boolean addServices(ServiceImplementation serviceImplementation) {
		return this.services.add(serviceImplementation);
	}

	public Set<UIModule> getUiModules() {
		return uiModules;
	}
	
	public boolean addUiModule(UIModule uiModule) {
		return this.uiModules.add(uiModule);
	}

	public Set<ServiceClient> getConsumedServices() {
		return consumedServices;
	}
	
	public boolean addStorage(Storage storage) {
		return this.storages.add(storage);
	}
	
	public Set<Storage> getStorage() {
		return storages;
	}
	
	public boolean addConsumedService(ServiceClient serviceClient) {
		return this.consumedServices.add(serviceClient);
	}

	public Set<BusinessLogic> getBusinessLogic() {
		return businessLogic;
	}
	
	public boolean addBusinessLogic(BusinessLogic businessLogic) {
		return this.businessLogic.add(businessLogic);
	}
	
	/**
	 * Ui applications that belong to a capability only uses views that
	 * belong to the same capability. This method returns such applications.
	 * 
	 * @return
	 */
	public Set<UiApplication> getUiApplications() {
		return uiApplications;
	}
	
	public boolean addUiApplication(UiApplication application) {
		return this.uiApplications.add(application);
	}
	
	public Set<ServiceApplication> getServiceApplications() {
		return serviceApplications;
	}
	
	public boolean addServiceApplication(ServiceApplication application) {
		return this.serviceApplications.add(application);
	}
	
	public Set<IotGatewayApplication> getIotGatewayApplications() {
		return iotGatewayApplications;
	}
	
	public boolean addIotGatewayApplication(IotGatewayApplication application) {
		return this.iotGatewayApplications.add(application);
	}
	
	public Set<Capability> getExtendedCapabilities() {
		return extendedCapabilities;
	}
	
	public boolean addExtendedCapability(Capability capability) {
		return this.extendedCapabilities.add(capability);
	}

	/**
	 * A default business logic element is automatically created and collects all function and persistence modules that have been directly linked
	 * to a capability, without explicitely model a business logic element. It is a convenience method for the modeling of simple
	 * capabilities that only use a few functions.
	 * @return
	 */
	public BusinessLogic getDefaultBusinesslogic() {
		return defaultBusinesslogic;
	}

	public void setDefaultBusinesslogic(BusinessLogic defaultBusinesslogic) {
		this.defaultBusinesslogic = defaultBusinesslogic;
	}

	/**
	 * A default storage is automatically created and collects all function and persistence modules that have been directly linked
	 * to a capability, without explicitely model a storage element. It is a convenience method for the modeling of simple
	 * capabilities that only use a few entities.
	 * 
	 * @return
	 */
	public Storage getDefaultStorage() {
		return defaultStorage;
	}

	public void setDefaultStorage(Storage defaultStorage) {
		this.defaultStorage = defaultStorage;
	}
	
	/**
	 * @param view
	 * @return
	 */
	public boolean isViewIncluded(UIStructuralContainer view) {
		if (view != null) {
			return this.getInterfaceModules().contains(view.getModule()) ||
		    		this.getInternalModules().contains(view.getModule()) ||
		    		this.getUiModules().contains(view.getModule());
		}
		
		return false;
	}
	
	public boolean isContainerIncluded(UIContainer container) {
		if (container != null) {
			return getUiModules().stream().flatMap(uiModule -> uiModule.getElements(UIStructuralContainer.class).stream())
						.filter(uiStructuralContainer -> uiStructuralContainer.getAllChildContainers().contains(container))
						.findAny().isPresent();
		}
		
		return false;
	}
	
	/**
	 * @param modelElement
	 * @return
	 */
	public boolean hasModuleWithSameNameAndNamespace(ModelElement modelElement) {
		Module module = null;
		ModelElement originatingElement = modelElement;
		while (originatingElement != null) {
			module = originatingElement.getModule();
			if (module != null) {
				break;
			}
			originatingElement = originatingElement.getOriginatingElement(ModelElement.class);
		}

		if (module != null && module.getNamespace() != null) {
			Namespace namespace = module.getNamespace();
			
			for (Module aModule : getAllModules()) {
				if (aModule.getNamespace() == null) continue;
				
				if (aModule.getName().equals(module.getName()) && namespace.getName().equals(aModule.getNamespace().getName())) {
					return true;
				}
			}
		} else {
			System.out.println("model element " + modelElement + " has no module");
		}
		
		return false;
	}
	
	/**
	 * @return
	 */
	public Set<Module> getAllModules() {
		Set<Module> modules = new LinkedHashSet<>();
		
		modules.add(this.getModule());
		for (UIModule uiModule : this.uiModules) {
			modules.add(uiModule);
		}
		
		for (BusinessLogic businessLogic : this.businessLogic) {
			if (businessLogic.getModule() != null) modules.add(businessLogic.getModule());
			for (PersistenceModule persistenceModule : businessLogic.getPersistenceModules()) {
				modules.add(persistenceModule);
			}
			for (FunctionModule functionModule : businessLogic.getFunctionModules()) {
				modules.add(functionModule);
			}
		}
		
		for (Module interfaceModule : this.interfaceModules) {
			modules.add(interfaceModule);
		}
		
		for (Module internalModule : this.internalModules) {
			modules.add(internalModule);
		}
		
		return modules;
	}
	
	/**
	 * @return
	 */
	public Set<PersistenceModule> getAllPersistenceModulesInStorages() {
		Set<PersistenceModule> result = new LinkedHashSet<>();
		
		if (defaultStorage != null) {
			result.addAll(defaultStorage.getPersistenceModules());
		}
		
		for (Storage storage : storages) {
			result.addAll(storage.getPersistenceModules());
		}
		
		return result;
	}
	
	/**
	 * Checks whether the given entity is part of this capability's
	 * storages.
	 * 
	 * @param entity
	 * @return
	 */
	public boolean isEntityPartOfStorage(Entity entity) {
		if (entity != null && entity.getPersistenceModule() != null && this.getAllPersistenceModulesInStorages().contains(entity.getPersistenceModule())) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * @param entity
	 * @return
	 */
	public boolean isEntityPartOfServiceImpl(Entity entity) {
		for (ServiceImplementation serviceImpl : services) {
			if (serviceImpl.getServiceInterface() != null) {
				if (serviceImpl.getServiceInterface().contains(entity)) {
					return true;
				}
			}
		}
		
		for (ServiceApplication serviceApplication : this.serviceApplications) {
			for (ServiceImplementation serviceImpl : serviceApplication.getServices()) {
				if (serviceImpl.getServiceInterface() != null) {
					if (serviceImpl.getServiceInterface().contains(entity)) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	/**
	 * @param entity
	 * @return
	 */
	public boolean isEntityPartOfBusinessLogic(Entity entity) {
		LinkedHashSet<BusinessLogic> allBussinesLogic = new LinkedHashSet<>(this.getBusinessLogic());
		if (getDefaultBusinesslogic() != null) allBussinesLogic.add(getDefaultBusinesslogic());
		
		for (BusinessLogic businessLogic : allBussinesLogic) {
			if (businessLogic.uses(entity)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * @param entity
	 * @return
	 */
	public boolean isEntityPartOfBusinessLogicOnly(Entity entity) {
		boolean partOfBusinessLogic = this.isEntityPartOfBusinessLogic(entity);
		boolean partOfServiceImpl = this.isEntityPartOfServiceImpl(entity);
		boolean partOfStorage = this.isEntityPartOfStorage(entity);
		
		return partOfBusinessLogic && !(partOfServiceImpl || partOfStorage);
	}
	
	/**
	 * @param entity
	 * @return
	 */
	public boolean isEntityPartOfStorageAndBusinessLogicOnly(Entity entity) {
		boolean partOfBusinessLogic = this.isEntityPartOfBusinessLogic(entity);
		boolean partOfServiceImpl = this.isEntityPartOfServiceImpl(entity);
		boolean partOfStorage = this.isEntityPartOfStorage(entity);
		
		return partOfBusinessLogic && partOfStorage && !partOfServiceImpl;
	}
	
	/**
	 * @return
	 */
	public Set<ComplexType> getAllUsedComplexTypes() {
		LinkedHashSet<ComplexType> result = new LinkedHashSet<>();
		for (Storage storage : getStorage()) {
			result.addAll(storage.getAllUsedComplexTypes());
		}
		if (getDefaultStorage() != null) {
		    result.addAll(getDefaultStorage().getAllUsedComplexTypes());
		}
		
		for (BusinessLogic businessLogic : getBusinessLogic()) {
			result.addAll(businessLogic.getAllUsedComplexTypes());
		}
		if (getDefaultBusinesslogic() != null) {
		    result.addAll(getDefaultBusinesslogic().getAllUsedComplexTypes());
		}
		
		for (UiApplication uiApplication : getUiApplications()) {
			for (UIStructuralContainer uiStructuralContainer : uiApplication.getAllStructuralContainers()) {
				for (UIDataContainer uiDataContainer : uiStructuralContainer.getAllDataContainers()) {
					result.addAll(uiDataContainer.getAllUsedComplexTypes());
					result.addAll(uiDataContainer.getAppliedComplexDataTypes());
				}
			}
		}
		
		for (ServiceImplementation serviceImplementation : getServices()) {
			serviceImplementation.getServiceInterface().getPersistenceModules().stream()
			    .forEach(module -> result.addAll(module.getAllUsedComplexTypes()));
			
			serviceImplementation.getServiceInterface().getFunctionModules().stream()
		        .forEach(module -> result.addAll(module.getAllUsedComplexTypes()));
		}
		
		getServiceApplications().stream().flatMap(serviceApp -> serviceApp.getServices().stream())
			.forEach(serviceImplementation -> {
				serviceImplementation.getServiceInterface().getPersistenceModules().stream()
			    .forEach(module -> result.addAll(module.getAllUsedComplexTypes()));
			
			serviceImplementation.getServiceInterface().getFunctionModules().stream()
		        .forEach(module -> result.addAll(module.getAllUsedComplexTypes()));
			});
		
		return result;
	}
}
