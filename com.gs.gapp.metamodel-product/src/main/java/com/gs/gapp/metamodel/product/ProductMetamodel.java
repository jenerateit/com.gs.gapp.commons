package com.gs.gapp.metamodel.product;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs.gapp.metamodel.basic.MetamodelI;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;

/**
 * @author marcu
 *
 */
public enum ProductMetamodel implements MetamodelI {

	INSTANCE,
	;
	
	private static final LinkedHashSet<Class<? extends ModelElementI>> metatypes = new LinkedHashSet<>();
	private static final Collection<Class<? extends ModelElementI>> collectionOfCheckedMetatypes = new LinkedHashSet<>();
	
	static {
		metatypes.add(UiApplication.class);
		metatypes.add(AbstractApplication.class);
		metatypes.add(AbstractProductModelElement.class);
		metatypes.add(Capability.class);
		metatypes.add(Feature.class);
		metatypes.add(IotGatewayApplication.class);
		metatypes.add(Namespace.class);
		metatypes.add(Organization.class);
		metatypes.add(Product.class);
		metatypes.add(ProductModule.class);
		metatypes.add(ProductVariant.class);
		metatypes.add(ServiceApplication.class);
		
		collectionOfCheckedMetatypes.add(ProductModule.class);
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypes() {
		return Collections.unmodifiableCollection(metatypes);
	}

	@Override
	public boolean isIncluded(Class<? extends ModelElementI> metatype) {
		return metatypes.contains(metatype);
	}

	@Override
	public boolean isExtendingOneOfTheMetatypes(Class<? extends ModelElementI> metatype) {
		for (Class<? extends ModelElementI> metatypeOfMetamodel : metatypes) {
			if (metatypeOfMetamodel.isAssignableFrom(metatype)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypesForConversionCheck() {
		return collectionOfCheckedMetatypes;
	}
	
	/**
	 * @param metatype
	 * @return
	 */
	public Class<? extends Module> getModuleType(Class<? extends ModelElementI> metatype) {
		if (isIncluded(metatype)) {
		    return ProductModule.class;
		}
		return null;
	}
	
	/**
	 * @param nameOfFilteredCapability
	 * @param capabilities
	 */
	public void completeCapabilityDependencies(String nameOfFilteredCapability, List<Capability> capabilities) {
		if (nameOfFilteredCapability != null && !nameOfFilteredCapability.isEmpty()) {
        	Optional<Capability> filteredCapability = capabilities.stream()
		        	.filter(capability -> capability.getName().equalsIgnoreCase(nameOfFilteredCapability))
		        	.findFirst();
        	if (filteredCapability.isPresent()) {
        		capabilities.stream().filter(capability -> capability != filteredCapability.get())
        		    .forEach(capability -> filteredCapability.get().addExtendedCapability(capability));
        	}
        }
	}
	
	/**
	 * @param modelElements
	 */
	public void autolinkSingleCapabilityWithSingleApplication(Set<Object> modelElements, List<Capability> capabilities) {
        if (capabilities.size() == 1) {
        	final Capability singleCapability = capabilities.iterator().next();
	        List<UiApplication> uiApplications = modelElements.stream()
	        		.filter(UiApplication.class::isInstance)
	        		.map(UiApplication.class::cast)
	        		.collect(Collectors.toList());
	        
	        List<ServiceApplication> serviceApplications = modelElements.stream()
	        		.filter(ServiceApplication.class::isInstance)
	        		.map(ServiceApplication.class::cast)
	        		.collect(Collectors.toList());
        
	        if (uiApplications.size() == 1) {
	        	capabilities.iterator().next().addUiApplication(uiApplications.iterator().next());
	        }
        
	        if (serviceApplications.size() == 1) {
	        	capabilities.iterator().next().addServiceApplication(serviceApplications.iterator().next());
	        }
	        
	        uiApplications.stream()
		        .forEach(uiApplication -> {
		        	if (!uiApplication.getCapabilities().contains(singleCapability)) {
		        		uiApplication.addCapability(singleCapability);
		        	}
		        });
	        
	        serviceApplications.stream()
		        .forEach(serviceApplication -> {
		        	if (!serviceApplication.getCapabilities().contains(singleCapability)) {
		        		serviceApplication.addCapability(singleCapability);
		        	}
		        });
        }
	}
}
