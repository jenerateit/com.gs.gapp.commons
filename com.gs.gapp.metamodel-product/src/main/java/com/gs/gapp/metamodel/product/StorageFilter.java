package com.gs.gapp.metamodel.product;

import java.util.Collection;
import java.util.LinkedHashSet;

import com.gs.gapp.metamodel.basic.AbstractModelFilter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelFilterI;
import com.gs.gapp.metamodel.function.BusinessException;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionMetamodel;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.Service;
import com.gs.gapp.metamodel.function.ServiceClient;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.function.ServiceInterface;
import com.gs.gapp.metamodel.function.Storage;
import com.gs.gapp.metamodel.function.TechnicalException;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.Namespace;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.gs.gapp.metamodel.persistence.enums.PersistenceMetamodel;

/**
 * The storage filter removes all model elements that are types of the Persistence metamodel
 * or the Function metamodel in case those elements do not belong to {@link Capability#getDefaultStorage()}
 * or {@link Capability#getStorage()}.
 * 
 * Note that if the passed collection of model elements does not contain any capability, no
 * filtering takes place whatsoever.
 * 
 * @author marcu
 *
 */
public class StorageFilter extends AbstractModelFilter implements ModelFilterI {

	public StorageFilter() {}

	@Override
	public Collection<?> filter(Collection<?> modelElements) {
		Collection<Capability> capabilities = getElementsForMetatype(Capability.class, modelElements);
		if (capabilities.size() == 0) return modelElements;  // capabilities are the key factor for filtering, so, when there are none => don't filter
		
		Collection<Object> result = new LinkedHashSet<>();
		Collection<?> mandatoryElements = AbstractModelFilter.extractMandatoryElements(modelElements);
		LinkedHashSet<Storage> storages = new LinkedHashSet<>();
		for (Capability capability : capabilities) {
			if (capability.getDefaultStorage() != null) storages.add(capability.getDefaultStorage());
			storages.addAll(capability.getStorage());
		}
		
		for (Object element : modelElements) {
			if (element instanceof ModelElementI) {
				ModelElementI modelElement = (ModelElementI) element;
				
				if (PersistenceMetamodel.INSTANCE.isConversionChecked(modelElement.getClass())) {
					addPersistenceModelElement(result, storages, modelElement);
				} else if (FunctionMetamodel.INSTANCE.isConversionChecked(modelElement.getClass())) {
					addFunctionModelElement(result, storages, modelElement);
				} else {
					result.add(element);	
				}
				
			} else {
				result.add(element);
			}
		}
		
		result.addAll(mandatoryElements);
		return result;
	}

	/**
	 * @param result
	 * @param storages
	 * @param modelElement
	 */
	private void addPersistenceModelElement(Collection<Object> result, LinkedHashSet<Storage> storages, ModelElementI modelElement) {
		Entity entity = null;
		Namespace persistenceNamespace = null;
		PersistenceModule persistenceModule = null;
		if (modelElement instanceof Entity) {
			entity = (Entity) modelElement;
		} else if (modelElement instanceof Namespace) {
			persistenceNamespace = (Namespace) modelElement;
		} else if (modelElement instanceof PersistenceModule) {
			persistenceModule = (PersistenceModule) modelElement;
		}
		
		boolean included = false;
		
		if (isEntityIncludedInStorages(storages, entity)) {
			included = true;
		} else {
			for (Storage storage : storages) {
				if (persistenceModule != null && storage.getPersistenceModules().contains(persistenceModule)) {
					included = true;
					break;
				}
				if (persistenceNamespace != null) {
					for (PersistenceModule aPersistenceModule : storage.getPersistenceModules()) {
						if (aPersistenceModule.getNamespace() == persistenceNamespace) {
							included = true;
							break;
						}
					}
					if (included) break;
				}
			}
		}
		
		if (included) {
			result.add(modelElement);
		}
	}
	
	/**
	 * @param storages
	 * @param entity
	 * @return
	 */
	private boolean isEntityIncludedInStorages(LinkedHashSet<Storage> storages, Entity entity) {
		if (entity != null) {
			for (Storage storage : storages) {
				if (storage.contains(entity)) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * @param result
	 * @param storages
	 * @param modelElement
	 */
	private void addFunctionModelElement(Collection<Object> result, LinkedHashSet<Storage> storages, ModelElementI modelElement) {
		Function function = null;
		com.gs.gapp.metamodel.function.Namespace functionNamespace = null;
		FunctionModule functionModule = null;
		@SuppressWarnings("unused")
		Service service = null;
		@SuppressWarnings("unused")
		ServiceClient serviceClient = null;
		@SuppressWarnings("unused")
		ServiceImplementation serviceImplementation = null;
		@SuppressWarnings("unused")
		ServiceInterface serviceInterface = null;
		@SuppressWarnings("unused")
		BusinessException businessException = null;
		@SuppressWarnings("unused")
		TechnicalException technicalException = null;
		
		if (modelElement instanceof Function) {
			function = (Function) modelElement;
		} else if (modelElement instanceof com.gs.gapp.metamodel.function.Namespace) {
			functionNamespace = (com.gs.gapp.metamodel.function.Namespace) modelElement;
		} else if (modelElement instanceof FunctionModule) {
			functionModule = (FunctionModule) modelElement;
		}
		
		boolean included = false;
		
		if (functionModule != null && functionModule.getOriginatingElement(Entity.class) != null) {
			Entity originatingEntity = functionModule.getOriginatingElement(Entity.class);
			if (isEntityIncludedInStorages(storages, originatingEntity)) {
				included = true;
			}
		}
		
		if (!included) {
			for (Storage storage : storages) {
				if (functionModule != null && storage.getFunctionModules().contains(functionModule)) {
					included = true;
					break;
				}
				if (function != null && storage.contains(function)) {
					included = true;
					break;
				}
				if (functionNamespace != null) {
					for (FunctionModule aFunctionModule : storage.getFunctionModules()) {
						if (aFunctionModule.getNamespace() == functionNamespace) {
							included = true;
							break;
						}
					}
					if (included) break;
				}
			}
		}
		
		if (included) {
			result.add(modelElement);
		}
	}
}
