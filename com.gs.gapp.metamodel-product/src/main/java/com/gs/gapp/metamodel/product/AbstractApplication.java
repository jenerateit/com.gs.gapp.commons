/**
 *
 */
package com.gs.gapp.metamodel.product;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.ui.UIModule;

/**
 * An Application element assembles a set of modules and other model elements.
 * The context of an application can be defined by the business capabilities it
 * uses and by the product variant it relates to. 
 * 
 * @author mmt
 *
 */
public abstract class AbstractApplication extends ModelElement {

	/**
	 *
	 */
	private static final long serialVersionUID = 939747746554605578L;
	
	private static final CapabilityComparator capabilityComparator = new CapabilityComparator();

	private String defaultLanguage;
	
	private ProductVariant productVariant;

	private final Set<Capability> capabilities = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public AbstractApplication(String name) {
		super(name);
	}

	/**
	 * @return the defaultLanguage
	 */
	public String getDefaultLanguage() {
		return defaultLanguage;
	}

	/**
	 * @param defaultLanguage the defaultLanguage to set
	 */
	public void setDefaultLanguage(String defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}
	
	public Set<Capability> getCapabilities() {
		return capabilities;
	}
	
	/**
	 * @return the application's capabilities, ordered by name (ascending)
	 */
	public List<Capability> getCapabilitiesOrdered() {
		ArrayList<Capability> result = new ArrayList<>(capabilities);
		Collections.sort(result, capabilityComparator);
		return result;
	}
	
	public boolean addCapability(Capability capability) {
		return this.capabilities.add(capability);
	}
	
	public Capability getCapability(Module module) {
		for (Capability capability : capabilities) {
			if (capability.getInterfaceModules().contains(module) ||
	        	capability.getInternalModules().contains(module) ||
	        	capability.getUiModules().contains(module)) {
				return capability;
			}
		}
		
		return null;
	}

	public ProductVariant getProductVariant() {
		return productVariant;
	}

	public void setProductVariant(ProductVariant productVariant) {
		this.productVariant = productVariant;
	}
	
	/**
	 * @author mmt
	 *
	 */
	private static class CapabilityComparator implements Comparator<Capability> {

		@Override
		public int compare(Capability capability1, Capability capability2) {
			if (capability1 == null && capability2 != null) {
				return 1;
			} else if (capability1 != null && capability2 == null) {
				return -1;
			}
			return capability1.getName().compareTo(capability2.getName());
		}
	}
}
