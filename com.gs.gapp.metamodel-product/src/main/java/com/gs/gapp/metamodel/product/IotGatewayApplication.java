/**
 *
 */
package com.gs.gapp.metamodel.product;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.iot.businesslogic.BusinessLogic;
import com.gs.gapp.metamodel.iot.device.Device;
import com.gs.gapp.metamodel.iot.topology.BrokerConnection;

/**
 * An IoT gateway application has the purpose to control some devices
 * and send sensor data to a server, often by means of a publish/subscribe
 * mechanism.
 * 
 * @author mmt
 *
 */
public class IotGatewayApplication extends AbstractApplication {

	/**
	 *
	 */
	private static final long serialVersionUID = 939747746554605578L;
	
	private final Set<Device> devices = new LinkedHashSet<>();
	private final Set<BrokerConnection> brokerConnections = new LinkedHashSet<>();
	private final Set<BusinessLogic> businessLogic = new LinkedHashSet<>();
	

	
	/**
	 * @param name
	 */
	public IotGatewayApplication(String name) {
		super(name);
	}

	public Set<Device> getDevices() {
		return devices;
	}

    public boolean addDevice(Device device) {
    	return this.devices.add(device);
    }

	public Set<BrokerConnection> getBrokerConnections() {
		return brokerConnections;
	}

	public boolean addBrokerConnection(BrokerConnection brokerConnection) {
    	return this.brokerConnections.add(brokerConnection);
    }

	public Set<BusinessLogic> getBusinessLogic() {
		return businessLogic;
	}
	
	public boolean addBusinessLogic(BusinessLogic businessLogic) {
    	return this.businessLogic.add(businessLogic);
    }
}
