package com.gs.gapp.metamodel.product;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

import com.gs.gapp.metamodel.basic.AbstractModelFilter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelFilterI;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionMetamodel;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.Namespace;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.gs.gapp.metamodel.persistence.enums.PersistenceMetamodel;

/**
 * The service implementation filter removes all model elements that are types of the Persistence metamodel
 * or the Function metamodel in case those elements do not belong to {@link Capability#getServices()}.
 * 
 * Note that if the passed collection of model elements does not contain any capability, no
 * filtering takes place whatsoever.
 * 
 * @author marcu
 *
 */
public class ServiceImplementationFilter extends AbstractModelFilter implements ModelFilterI {
	
	public ServiceImplementationFilter() {}

	@Override
	public Collection<?> filter(Collection<?> modelElements) {
		Collection<Capability> capabilities = getElementsForMetatype(Capability.class, modelElements);
		if (capabilities.size() == 0) {
			return modelElements;  // capabilities are the key factor for filtering, so, when there are none => don't filter
		}
		
		Collection<Object> result = new LinkedHashSet<>();
		Collection<?> mandatoryElements = AbstractModelFilter.extractMandatoryElements(modelElements);
		LinkedHashSet<ServiceImplementation> serviceImplementations = new LinkedHashSet<>();
		for (Capability capability : capabilities) {
			serviceImplementations.addAll(capability.getServices());
			
			serviceImplementations.addAll(capability.getServiceApplications().stream()
			    .flatMap(serviceApp -> serviceApp.getServices().stream())
			    .collect(Collectors.toCollection(LinkedHashSet::new)));
		}
		
		
		
		for (Object element : modelElements) {
			if (element instanceof ModelElementI) {
				ModelElementI modelElement = (ModelElementI) element;
				
				if (PersistenceMetamodel.INSTANCE.isConversionChecked(modelElement.getClass())) {
					addPersistenceModelElement(result, serviceImplementations, modelElement);
				} else if (FunctionMetamodel.INSTANCE.isConversionChecked(modelElement.getClass())) {
					addFunctionModelElement(result, serviceImplementations, modelElement);
				} else {
					result.add(element);	
				}
				
			} else {
				result.add(element);
			}
		}
		
		result.addAll(mandatoryElements);
		return result;
	}

	/**
	 * @param result
	 * @param serviceImplementationElements
	 * @param modelElement
	 * @return true in case the element got added
	 */
	private boolean addPersistenceModelElement(Collection<Object> result, LinkedHashSet<ServiceImplementation> serviceImplementationElements, ModelElementI modelElement) {
		Entity entity = null;
		Namespace persistenceNamespace = null;
		PersistenceModule persistenceModule = null;
		if (modelElement instanceof Entity) {
			entity = (Entity) modelElement;
		} else if (modelElement instanceof Namespace) {
			persistenceNamespace = (Namespace) modelElement;
		} else if (modelElement instanceof PersistenceModule) {
			persistenceModule = (PersistenceModule) modelElement;
		}
		
		boolean included = false;
		for (ServiceImplementation serviceImplementation : serviceImplementationElements) {
			if (persistenceModule != null && serviceImplementation.getServiceInterface().getPersistenceModules().contains(persistenceModule)) {
				included = true;
				break;
			}
			if (entity != null && serviceImplementation.getServiceInterface().contains(entity)) {
				included = true;
				break;
			}
			if (persistenceNamespace != null) {
				for (PersistenceModule aPersistenceModule : serviceImplementation.getServiceInterface().getPersistenceModules()) {
					if (aPersistenceModule.getNamespace() == persistenceNamespace) {
						included = true;
						break;
					}
				}
				if (included) break;
			}
		}
		
		if (included) {
			result.add(modelElement);
		}
		
		return included;
	}
	
	/**
	 * @param result
	 * @param serviceImplementationElements
	 * @param modelElement
	 * @return true in case the element got added
	 */
	private boolean addFunctionModelElement(Collection<Object> result, LinkedHashSet<ServiceImplementation> serviceImplementationElements, ModelElementI modelElement) {
		Function function = null;
		com.gs.gapp.metamodel.function.Namespace functionNamespace = null;
		FunctionModule functionModule = null;
		
		if (modelElement instanceof Function) {
			function = (Function) modelElement;
		} else if (modelElement instanceof com.gs.gapp.metamodel.function.Namespace) {
			functionNamespace = (com.gs.gapp.metamodel.function.Namespace) modelElement;
		} else if (modelElement instanceof FunctionModule) {
			functionModule = (FunctionModule) modelElement;
		}
		
		boolean included = false;
		for (ServiceImplementation serviceImplementation : serviceImplementationElements) {
			if (functionModule != null) {
				if (serviceImplementation.getServiceInterface().getFunctionModules().contains(functionModule)) {
				    included = true;
				    break;
				} else if (functionModule.getOriginatingElement(Entity.class) != null) {
					// the given function module got created on-the-fly, for a given entity
					boolean entityIncluded = addPersistenceModelElement(result, serviceImplementationElements, functionModule.getOriginatingElement(Entity.class));
					included = entityIncluded;  // The on-the-fly created function module needs to be included, too. We want to generate something for them.
				}
			}
			if (function != null && serviceImplementation.getServiceInterface().contains(function)) {
				included = true;
				break;
			}
			if (functionNamespace != null) {
				for (FunctionModule aFunctionModule : serviceImplementation.getServiceInterface().getFunctionModules()) {
					if (aFunctionModule.getNamespace() == functionNamespace) {
						included = true;
						break;
					}
				}
				if (included) break;
			}
		}
		
		if (included) {
			result.add(modelElement);
		}
		
		return included;
	}
}
