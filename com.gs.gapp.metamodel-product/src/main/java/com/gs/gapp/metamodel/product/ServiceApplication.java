/**
 *
 */
package com.gs.gapp.metamodel.product;

import java.net.URL;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.function.ServiceInterface.Contact;
import com.gs.gapp.metamodel.function.ServiceInterface.License;

/**
 * A service application has the purpose to give remote clients access to
 * one or more service implementations.
 * 
 * @author mmt
 *
 */
public class ServiceApplication extends AbstractApplication {

	/**
	 *
	 */
	private static final long serialVersionUID = 939747746554605578L;
	
    private URL termsOfService;
	
    private Contact contact;
	
	private License license;
	
	private final Set<ServiceImplementation> services = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public ServiceApplication(String name) {
		super(name);
	}
	
	public URL getTermsOfService() {
		return termsOfService;
	}

	public void setTermsOfService(URL termsOfService) {
		this.termsOfService = termsOfService;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public License getLicense() {
		return license;
	}

	public void setLicense(License license) {
		this.license = license;
	}

	
	public Set<ServiceImplementation> getServices() {
		return services;
	}
	
	public boolean addService(ServiceImplementation serviceImplementation) {
		return this.services.add(serviceImplementation);
	}
}
