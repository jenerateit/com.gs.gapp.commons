package com.gs.gapp.dsl;

import java.util.ArrayList;
import java.util.List;


public enum OptionEnum implements IOption {

	TYPE ("Type"),
	ABSTRACT ("Abstract"),
	TYPE_FILTER_GROUPS ("TypeFilterGroups"),
	TYPE_FILTERS ("TypeFilters"),
	SORT_VALUE ("SortValue"),
	;

	private final String name;

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private OptionEnum(String name) {
		this(name, null);
	}

    private OptionEnum(String name, IMetatype exactMetatype) {
		this(name, exactMetatype, false);
	}

    private OptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(name, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private OptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this.name = name;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	public static List<OptionEnum> getElementOptions(IElement element) {
		List<OptionEnum> result = new ArrayList<>();
		for (OptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<OptionEnum> getMemberOptions(IMember member) {
		List<OptionEnum> result = new ArrayList<>();
		for (OptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}
}
