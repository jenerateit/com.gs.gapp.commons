package com.gs.gapp.dsl.ui;

import com.gs.gapp.dsl.IElement;

public enum UiModuleTypeEnum implements IElement {

	UI ("UI"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private UiModuleTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
}
