package com.gs.gapp.dsl.ui;



public enum StorageTypeEnum  {

	LOCAL ("Local"),
	REMOTE ("Remote"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private StorageTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static StorageTypeEnum getByName(String name) {
		for (StorageTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
