package com.gs.gapp.dsl.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;
import com.gs.gapp.dsl.basic.BasicMemberEnum;
import com.gs.gapp.dsl.function.FunctionElementEnum;
import com.gs.gapp.dsl.function.FunctionMemberEnum;
import com.gs.gapp.dsl.function.FunctionModuleTypeEnum;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionBoolean;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionLong;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;


public enum RestOptionEnum implements IOption {

	PURPOSE        (new OptionDefinitionString("REST-Purpose"),       FunctionElementEnum.FUNCTION),
	IDEMPOTENT     (new OptionDefinitionBoolean("REST-Idempotent"),   FunctionElementEnum.FUNCTION),
	HATEOAS        (new OptionDefinitionBoolean("REST-HATEOAS"),      BasicMemberEnum.FIELD),
	CONSUMES       (new OptionDefinitionString("REST-Consumes"),      FunctionModuleTypeEnum.FUNCTION),
	PRODUCES       (new OptionDefinitionString("REST-Produces"),      FunctionModuleTypeEnum.FUNCTION),
	PARAM_TYPE     (new OptionDefinitionString("REST-ParamType"),     FunctionMemberEnum.IN),
	PATH           (new OptionDefinitionString("REST-Path"),          FunctionElementEnum.FUNCTION),
	STATUS_CODE    (new OptionDefinitionLong("REST-StatusCode"),      FunctionMemberEnum.EXCEPTION),
	OPERATION      (new OptionDefinitionString("REST-Operation"),     FunctionElementEnum.FUNCTION),
	TRANSPORT_TYPE (new OptionDefinitionString("REST-TransportType"), FunctionMemberEnum.OUT),
	PRODUCED_MEDIA_TYPES (new OptionDefinitionString("Produced_Media_Types"), FunctionElementEnum.FUNCTION),
	CONSUMED_MEDIA_TYPES (new OptionDefinitionString("Consumed_Media_Types"), FunctionElementEnum.FUNCTION),
	
	;
	
	public static final OptionDefinitionString OPTION_DEFINITION_CONSUMES =
			new OptionDefinitionString(RestOptionEnum.CONSUMES.getName(), "value for the @Consumes annotation on a REST resource or a resource's http operation", false, true);
	public static final OptionDefinitionString OPTION_DEFINITION_PRODUCES =
			new OptionDefinitionString(RestOptionEnum.PRODUCES.getName(), "value for the @Produces annotation on a REST resource or a resource's http operation", false, true);
	public static final OptionDefinitionString OPTION_DEFINITION_PARAM_TYPE =
			new OptionDefinitionString(RestOptionEnum.PARAM_TYPE.getName(), "value that determines, which kind of annotation should be used for a resource method parameter (Cookie, Form, Header, Path, Query)", null);
	public static final OptionDefinitionString OPTION_DEFINITION_PATH =
			new OptionDefinitionString(RestOptionEnum.PATH.getName(), "value for the @Path annotation on a REST resource or a resource's http operation", null);
	public static final OptionDefinitionString OPTION_DEFINITION_OPERATION =
			new OptionDefinitionString(RestOptionEnum.OPERATION.getName(), "value for the annotation for the HTTP verb", null);
	public static final OptionDefinitionLong OPTION_DEFINITION_STATUS_CODE =
			new OptionDefinitionLong(RestOptionEnum.STATUS_CODE.getName(), "value for the HTTP status code to return in exceptional situations", null);
	public static final OptionDefinitionString OPTION_DEFINITION_TRANSPORT_TYPE =
			new OptionDefinitionString(RestOptionEnum.TRANSPORT_TYPE.getName(), "value for the way an out parameter is handled in the context of REST-APIs", false, false);
	public static final OptionDefinitionString OPTION_DEFINITION_PRODUCED_MEDIA_TYPES =
			new OptionDefinitionString(RestOptionEnum.PRODUCED_MEDIA_TYPES.getName(), "media types for outgoing data", false, true);
	public static final OptionDefinitionString OPTION_DEFINITION_CONSUMED_MEDIA_TYPES =
			new OptionDefinitionString(RestOptionEnum.CONSUMED_MEDIA_TYPES.getName(), "media types for incoming data", false, true);
	
	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;
	
	private final OptionDefinition<?> definition;
	
	private RestOptionEnum(OptionDefinition<?> definition) {
		this(definition, null);
	}

    private RestOptionEnum(OptionDefinition<?> definition, IMetatype exactMetatype) {
		this(definition, exactMetatype, false);
	}
    
    private RestOptionEnum(OptionDefinition<?> definition, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(definition, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private RestOptionEnum(OptionDefinition<?> definition, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this.definition = definition;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return this.getDefinition(Serializable.class).getKey();
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}
	
	/**
	 * @return the definition
	 */
	@SuppressWarnings("unchecked")
	public <T extends Serializable> OptionDefinition<T> getDefinition(Class<T> clazz) {
		return (OptionDefinition<T>)definition;
	}

	public static List<RestOptionEnum> getElementOptions(IElement element) {
		List<RestOptionEnum> result = new ArrayList<>();
		for (RestOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<RestOptionEnum> getMemberOptions(IMember member) {
		List<RestOptionEnum> result = new ArrayList<>();
		for (RestOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}
	
	/**
	 * Possible values for RestOptionEnum entry PARAM_TYPE
	 * 
	 * @author mmt
	 *
	 */
	public static enum ParamTypeEnum  {

		/**
		 * JAX-RS doesn't have a corresponding annotation. Method parameters without annotation are meant to be part of the request body.
		 * 
		 * <p>Note that the MARS-Curiosity framework (Delphi) _does_ have a Body attribute.
		 */
		BODY ("Body"),
		
		/**
		 * JAX-RS: @FormParam
		 * Is equivalent to a form parameter
		 */
		FIELD ("Field"),
	
		/**
		 * JAX-RS: @HeaderParam
		 */
		HEADER ("Header"),
		
		/**
		 * <p>Retrofit has a @Part annotation. However, it is not required to model that.
		 * For Retrofit the effect is that the @Part annotation is generated for a method parameter.
		 * However, this is only being done when the methods creates a multipart request.
		 * For the generation of the service implementation, parameters that have this option set are simply ignored.
		 */
		PART ("Part"),
		
		/**
		 * JAX-RS: @PathParam
		 */
		PATH ("Path"),
		
		/**
		 * JAX-RS: @QueryParam
		 */
		QUERY ("Query"),
		
		/**
		 * <p>Retrofit has a @QueryName annotation. It indicates that a query parameter is part of the URL but doesn't have a value set.
		 * For Retrofit the effect is that the @QueryName annotation is generated for a method parameter.
		 * For the generation of the service implementation, parameters that have this option set are simply ignored.
		 */
		QUERY_NAME ("QueryName"),
		
		/**
		 * <p>Retrofit has a @Url annotation. However, it is not required to model that.
		 * For Retrofit the effect is that the @Url annotation is generated for a method parameter.
		 * For the generation of the service implementation, parameters that have this option set are simply ignored.
		 */
		URL ("Url"),
		
		/**
		 * JAX-RS: @CookieParam
		 */
		COOKIE ("Cookie"),
		
		/**
		 * JAX-RS: @MatrixParam
		 */
		MATRIX ("Matrix"),
		;

		private final String name;

		/**
		 * @param name
		 */
		private ParamTypeEnum(String name) {
			this.name = name;
		}

		/* (non-Javadoc)
		 * @see com.gs.gapp.dsl.IMetatype#getName()
		 */
		public String getName() {
			return name;
		}

		public static ParamTypeEnum getByName(String name) {
			for (ParamTypeEnum enumEntry : values()) {
				if (enumEntry.getName().equalsIgnoreCase(name)) {
					return enumEntry;
				}
			}
			return null;
		}
	}

}
