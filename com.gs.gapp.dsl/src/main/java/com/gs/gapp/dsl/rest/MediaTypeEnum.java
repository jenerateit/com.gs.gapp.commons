package com.gs.gapp.dsl.rest;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;

public enum MediaTypeEnum  {

	APPLICATION_JAVASCRIPT     ("application___javascript",   "application/javascript"),
	APPLICATION_PDF            ("application___pdf",          "application/pdf"),
	APPLICATION_VND_API_JSON   ("application___vnd.api-json", "application/vnd.api+json"),
	APPLICATION_VND_MS_EXCEL   ("application___vnd.ms-excel", "application/vnd.ms-excel"), // .xls
	APPLICATION_VND_MS_POWERPOINT               ("application___vnd.ms-powerpoint",           "application/vnd.ms-powerpoint"), // .ppt
	APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT     ("application___vnd.oasis.opendocument.text", "application/vnd.oasis.opendocument.text"), // .odt
	APPLICATION_VND_OPENXMLFORMATS_PRESENTATION ("application___vnd.openxmlformats-officedocument.presentationml.presentation", "vnd.openxmlformats-officedocument.presentationml.presentation"), // .pptx
	APPLICATION_VND_OPENXMLFORMATS_SPREADSHEET  ("application___vnd.openxmlformats-officedocument.spreadsheetml.sheet",         "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"), // .xlsx
	APPLICATION_VND_OPENXMLFORMATS_WORD         ("application___vnd.openxmlformats-officedocument.wordprocessingml.document",   "application/vnd.openxmlformats-officedocument.wordprocessingml.document"), // .docx
	APPLICATION_ZIP ("application___zip", "application/zip"),
	
	APPLICATION_JSON            ("application___json",                  MediaType.APPLICATION_JSON_TYPE),
	APPLICATION_XML             ("application___xml",                   MediaType.APPLICATION_XML_TYPE),
	APPLICATION_FORM_URLENCODED ("application___x-www-form-urlencoded", MediaType.APPLICATION_FORM_URLENCODED_TYPE),
	
	TEXT_CSS   ("text___css",   "text/css"),
	TEXT_CSV   ("text___csv",   "text/csv"),
	TEXT_PLAIN ("text___plain", MediaType.TEXT_PLAIN_TYPE),
	TEXT_XML   ("text___xml",   MediaType.TEXT_XML_TYPE),
	TEXT_HTML  ("text___html",  MediaType.TEXT_HTML_TYPE),
	
	APPLICATION_ATOM_XML     ("application___atom_xml",     MediaType.APPLICATION_ATOM_XML_TYPE),
	APPLICATION_OCTET_STREAM ("application___octet-stream", MediaType.APPLICATION_OCTET_STREAM_TYPE),
	APPLICATION_XHTML_XML    ("application___xhtml_xml",    MediaType.APPLICATION_XHTML_XML_TYPE),
	
	// for form-data, see here: https://www.rfc-editor.org/rfc/rfc7578.html
	MULTIPART_FORMDATA    ("multipart___form-data",   MediaType.MULTIPART_FORM_DATA),
	
	// for the following three entries see here: https://www.w3.org/Protocols/rfc1341/7_2_Multipart.html
	MULTIPART_MIXED       ("multipart___mixed",       "multipart/mixed"),
	MULTIPART_ALTERNATIVE ("multipart___alternative", "multipart/alternative"),
	MULTIPART_DIGEST      ("multipart___digest",      "multipart/digest"),
	MULTIPART_RELATED     ("multipart___related",     "multipart/related"),
	
	IMAGE_GIF     ("image___gif",     "image/gif"),
	IMAGE_JPEG    ("image___jpeg",    "image/jpeg"),
	IMAGE_PNG     ("image___png",     "image/png"),
	IMAGE_SVG_XML ("image___svg-xml", "image/svg+xml"),
	;
	
	private static final Map<String,MediaTypeEnum> map = new HashMap<>();
	
	private static final Map<String,MediaTypeEnum> inverseMap = new HashMap<>();
	
	static {
		for (MediaTypeEnum enumEntry : MediaTypeEnum.values()) {
			map.put(enumEntry.getName().toLowerCase(), enumEntry);
			inverseMap.put(enumEntry.getMediaTypeAsString().toLowerCase(), enumEntry);
		}
	}
	
	private final String name;
	
	private final MediaType mediaType;
	
	private final String mediaTypeString;
	

	/**
	 * @param name
	 * @param mediaType
	 */
	private MediaTypeEnum(String name, MediaType mediaType) {
		this.name = name;
		this.mediaType = mediaType;
		this.mediaTypeString = null;
	}
	
	/**
	 * @param name
	 * @param mediaTypeString
	 */
	private MediaTypeEnum(String name, String mediaTypeString) {
		this.name = name;
		this.mediaType = null;
		this.mediaTypeString = mediaTypeString;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * @return
	 */
	public static MediaTypeEnum get(String name) {
		if (name != null) {
			return map.get(name.toLowerCase());
		}
		return null;
	}
	
	/**
	 * @param mediaTypeName
	 * @return
	 */
	public static MediaTypeEnum getInverse(String mediaTypeName) {
		if (mediaTypeName != null) {
			return inverseMap.get(mediaTypeName.toLowerCase());
		}
		return null;
	}

	/**
	 * @return
	 */
	public MediaType getMediaType() {
		return mediaType;
	}
	
	/**
	 * @return
	 */
	public String getMediaTypeAsString() {
		if (mediaType != null) {
			StringBuilder mediaTypeAsStringBuilder = new StringBuilder(mediaType.getType());
			if (mediaType.getSubtype() != null) mediaTypeAsStringBuilder.append("/").append(mediaType.getSubtype());
			return mediaTypeAsStringBuilder.toString();
		}
		return this.mediaTypeString;
	}
}
