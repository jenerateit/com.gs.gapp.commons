package com.gs.gapp.dsl.ui;



public enum ChoiceTypeEnum  {

	BUTTON ("Button"),
	CHECK ("Check"),
	DROP_DOWN ("DropDown"),
	LIST ("List"),
	AUTO_COMPLETE ("AutoComplete"),
	PICK_LIST ("PickList"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private ChoiceTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static ChoiceTypeEnum getByName(String name) {
		for (ChoiceTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
