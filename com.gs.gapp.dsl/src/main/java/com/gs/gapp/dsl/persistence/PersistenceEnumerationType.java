package com.gs.gapp.dsl.persistence;



public enum PersistenceEnumerationType {

	// for entity
	ORDINAL ("Ordinal"),
	LITERAL ("Literal"),
    ;

	private final String name;

	/**
	 * @param name
	 */
	private PersistenceEnumerationType(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	public String getName() {
		return name;
	}
}
