package com.gs.gapp.dsl.ui;



public enum OrientationEnum  {

	LEFT_TO_RIGHT ("LeftToRight"),
	TOP_TO_BOTTOM ("TopToBottom"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private OrientationEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static OrientationEnum getByName(String name) {
		for (OrientationEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
