package com.gs.gapp.dsl;

import java.util.List;

public interface IMember extends IMetatype {


	/**
	 * @return
	 */
	List<IElement> getOwners();

	/**
	 * @return
	 */
	List<IElement> getTypes();
}
