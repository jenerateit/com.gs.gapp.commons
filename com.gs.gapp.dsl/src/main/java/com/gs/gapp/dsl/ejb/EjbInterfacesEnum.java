package com.gs.gapp.dsl.ejb;

import java.util.HashMap;
import java.util.Map;



public enum EjbInterfacesEnum  {

	LOCAL ("Local"),
	REMOTE ("Remote"),
	LOCAL_AND_REMOTE ("LocalAndRemote"),
	;

	private static Map<String, EjbInterfacesEnum> nameToEnumMap =
			new HashMap<>();

	static {
		for (EjbInterfacesEnum e : values()) {
			nameToEnumMap.put(e.getName(), e);
		}
	}

	private final String name;

	/**
	 * @param name
	 */
	private EjbInterfacesEnum(String name) {
		this.name = name;
	}
	
	public static EjbInterfacesEnum getFromName(String name) {
		return nameToEnumMap.get(name);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static EjbInterfacesEnum getByName(String name) {
		for (EjbInterfacesEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
