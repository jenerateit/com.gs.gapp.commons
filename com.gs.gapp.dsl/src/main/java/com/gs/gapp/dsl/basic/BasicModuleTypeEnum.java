package com.gs.gapp.dsl.basic;

import com.gs.gapp.dsl.IElement;

public enum BasicModuleTypeEnum implements IElement {

	BASIC ("Basic"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private BasicModuleTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
}
