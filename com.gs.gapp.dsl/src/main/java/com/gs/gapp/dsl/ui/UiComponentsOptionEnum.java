package com.gs.gapp.dsl.ui;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;
import com.gs.gapp.dsl.ITypeOfMember;


public enum UiComponentsOptionEnum implements IOption {

	LABEL ("Label", UiMemberEnum.COMPONENT, false, ComponentsEnum.COMPONENT, true),
	OPTIONAL ("Optional", UiMemberEnum.COMPONENT, false, ComponentsEnum.COMPONENT, true),
	DISABLABLE ("Disablable", UiMemberEnum.COMPONENT, false, ComponentsEnum.COMPONENT, true),
	HANDLE_SELECTION_EVENT ("HandleSelectionEvent", UiMemberEnum.COMPONENT, false, ComponentsEnum.COMPONENT, true),
	WITHOUT_LABEL ("WithoutLabel", UiMemberEnum.COMPONENT, false, ComponentsEnum.COMPONENT, true),
	DATA_ENTRY_REQUIRED ("DataEntryRequired", UiMemberEnum.COMPONENT, false, ComponentsEnum.COMPONENT, true),
	ENTITY_FIELD ("EntityField", UiMemberEnum.COMPONENT, false, ComponentsEnum.COMPONENT, true),
	ENTITY_FIELD_FOR_DISPLAY ("EntityFieldForDisplay", UiMemberEnum.COMPONENT, false, ComponentsEnum.COMPONENT, true),
	PASSWORD ("Password", UiMemberEnum.COMPONENT, false, ComponentsEnum.TEXT_FIELD, false),
	HAS_ICON ("HasIcon", UiMemberEnum.COMPONENT, false, ComponentsEnum.ACTION_COMPONENT, true),
	CONTAINER_FOR_CHOICE ("Container", UiMemberEnum.COMPONENT, false, ComponentsEnum.CHOICE, false),
	CONTAINER_FOR_EMBEDDED_CONTAINER ("Container", UiMemberEnum.COMPONENT, false, ComponentsEnum.EMBEDDED_CONTAINER, false),
	MULTIPLICITY ("Multiplicity", UiMemberEnum.COMPONENT, false, ComponentsEnum.CHOICE, false),
	EDITABLE ("Editable", UiMemberEnum.COMPONENT, false, ComponentsEnum.CHOICE, false),
	TYPE ("Type", UiMemberEnum.COMPONENT, false, ComponentsEnum.DATE_SELECTOR, false),
	LOWER ("Lower", UiMemberEnum.COMPONENT, false, ComponentsEnum.RANGE, false),
	UPPER ("Upper", UiMemberEnum.COMPONENT, false, ComponentsEnum.RANGE, false),
	STEPPING ("Stepping", UiMemberEnum.COMPONENT, false, ComponentsEnum.RANGE, false),
	PRESELECTED ("Preselected", UiMemberEnum.COMPONENT, false, ComponentsEnum.CHOICE, true),
	
	CHOICE_TYPE ("ChoiceType", UiMemberEnum.COMPONENT, false, ComponentsEnum.CHOICE, false),
	BOOLEAN_CHOICE_TYPE ("BooleanChoiceType", UiMemberEnum.COMPONENT, false, ComponentsEnum.BOOLEAN_CHOICE, false),
	INPUT_MASK ("InputMask", UiMemberEnum.COMPONENT, false, ComponentsEnum.TEXT_FIELD, false),
	
	DISPLAY ("Display", UiMemberEnum.COMPONENT, false, ComponentsEnum.EMBEDDED_COMPONENT_GROUP, false),
	
	DATA_TYPE ("DataType", UiMemberEnum.COMPONENT, false, ComponentsEnum.COMPONENT, true),
	
	LONG_LASTING ("LongLasting", UiMemberEnum.COMPONENT, false, ComponentsEnum.ACTION_COMPONENT, true),
	ACTION_PURPOSE ("ActionPurpose", UiMemberEnum.COMPONENT, false, ComponentsEnum.ACTION_COMPONENT, true),
	DEFAULT_ACTION ("DefaultAction", UiMemberEnum.COMPONENT, false, ComponentsEnum.ACTION_COMPONENT, true),
	KEYBOARD_CONTROL ("KeyboardControl", UiMemberEnum.COMPONENT, false, ComponentsEnum.ACTION_COMPONENT, true),
	
	CUSTOM_MESSAGE ("CustomMessage", UiMemberEnum.COMPONENT, false, ComponentsEnum.COMPONENT, true),
	READ_ONLY ("ReadOnly", UiMemberEnum.COMPONENT, false, ComponentsEnum.COMPONENT, true),
	NAVIGATION_TARGET ("NavigationTarget", UiMemberEnum.COMPONENT, false, ComponentsEnum.COMPONENT, true),
	
	CHART_TYPE ("ChartType", UiMemberEnum.COMPONENT, false, ComponentsEnum.CHART, false),
	
	LENGTH ("Length", UiMemberEnum.COMPONENT, false, ComponentsEnum.TEXT_FIELD, false),
	
	FORMAT ("Format", UiMemberEnum.COMPONENT, false, ComponentsEnum.LABEL, false),
	
	IMAGE_TYPE ("ImageType", UiMemberEnum.COMPONENT, false, ComponentsEnum.IMAGE, false),
	;

	private final String name;

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private final ITypeOfMember typeofMember;

	private final boolean usingDeepFilterForTypeofMember;

	/**
     * @param name
     * @param exactMetatype
     */
    private UiComponentsOptionEnum(String name, IMetatype exactMetatype) {
		this(name, exactMetatype, false);
	}

    /**
     * @param name
     * @param exactMetatype
     * @param usingDeepFilter
     */
    private UiComponentsOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(name, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private UiComponentsOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this(name, exactMetatype, usingDeepFilter, metatypePattern, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param typeofMember
	 * @param usingDeepFilterForTypeofMember
	 */
	private UiComponentsOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, ITypeOfMember typeofMember, boolean usingDeepFilterForTypeofMember) {
		this(name, exactMetatype, usingDeepFilter, null, typeofMember, usingDeepFilterForTypeofMember);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 * @param typeofMember
	 */
	private UiComponentsOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern, ITypeOfMember typeofMember) {
		this(name, exactMetatype, usingDeepFilter, metatypePattern, typeofMember, false);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 * @param typeofMember
	 * @param usingDeepFilterForTypeofMember
	 */
	private UiComponentsOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern, ITypeOfMember typeofMember, boolean usingDeepFilterForTypeofMember) {
		this.name = name;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
		this.typeofMember = typeofMember;
		this.usingDeepFilterForTypeofMember = usingDeepFilterForTypeofMember;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	public static List<UiComponentsOptionEnum> getElementOptions(IElement element) {
		List<UiComponentsOptionEnum> result = new ArrayList<>();
		for (UiComponentsOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<UiComponentsOptionEnum> getMemberOptions(IMember member) {
		List<UiComponentsOptionEnum> result = new ArrayList<>();
		for (UiComponentsOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}

	/**
	 * @return the typeofMember
	 */
	public ITypeOfMember getTypeofMember() {
		return typeofMember;
	}

	/**
	 * @return the usingDeepFilterForTypeofMember
	 */
	public boolean isUsingDeepFilterForTypeofMember() {
		return usingDeepFilterForTypeofMember;
	}
}
