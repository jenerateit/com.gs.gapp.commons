package com.gs.gapp.dsl.iot;

import com.gs.gapp.dsl.IElement;

public enum IotModuleTypeEnum implements IElement {

	DEVICE ("Device"),
	TOPOLOGY ("Topology"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private IotModuleTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
}
