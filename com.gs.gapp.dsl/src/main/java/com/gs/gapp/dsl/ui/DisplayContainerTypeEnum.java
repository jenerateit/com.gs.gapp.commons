package com.gs.gapp.dsl.ui;

public enum DisplayContainerTypeEnum  {

	FORM ("Form"),
	LIST ("List"),
	GRID ("Grid"),
	TREE_TABLE ("TreeTable"),
	CUSTOM ("Custom"),
	TREE ("Tree"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private DisplayContainerTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static DisplayContainerTypeEnum getByName(String name) {
		for (DisplayContainerTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
