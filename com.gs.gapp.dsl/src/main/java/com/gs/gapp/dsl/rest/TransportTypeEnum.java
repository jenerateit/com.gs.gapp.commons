package com.gs.gapp.dsl.rest;



/**
 * A Function "out" parameter can be annotated with the REST option
 * 'REST-TransportType', an option the can get one of the enum entry
 * names that are declared in this enumeration. 
 *  
 * @author mmt
 *
 */
public enum TransportTypeEnum  {

	HEADER ("Header"),
	COOKIE ("Cookie"),
	BODY ("Body"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private TransportTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static TransportTypeEnum getByName(String name) {
		for (TransportTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
