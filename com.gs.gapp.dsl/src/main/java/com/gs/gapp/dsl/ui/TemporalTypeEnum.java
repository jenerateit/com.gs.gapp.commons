package com.gs.gapp.dsl.ui;



public enum TemporalTypeEnum  {

	DATE ("Date"),
	TIME ("Time"),
	DATETIME ("DateTime"),
	TIMESTAMP ("Timestamp"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private TemporalTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static TemporalTypeEnum getByName(String name) {
		for (TemporalTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
