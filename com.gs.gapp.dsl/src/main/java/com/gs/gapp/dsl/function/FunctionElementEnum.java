package com.gs.gapp.dsl.function;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;

public enum FunctionElementEnum implements IElement {

	FUNCTION ("function"),
	INTERFACE ("interface"),
	IMPLEMENTATION ("implementation"),
	CLIENT ("client"),
	STORAGE ("storage"),
	BUSINESSLOGIC ("businesslogic"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private FunctionElementEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	public static List<String> getElementTypeNames() {
		List<String> result = new ArrayList<>();
		for (IElement element : FunctionElementEnum.values()) {
			result.add(element.getName());
		}

		return result;
	}
}
