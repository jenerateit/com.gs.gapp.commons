package com.gs.gapp.dsl.delphi;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;
import com.gs.gapp.dsl.basic.BasicMemberEnum;
import com.gs.gapp.dsl.function.FunctionMemberEnum;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;


public enum DelphiOptionEnum implements IOption {

	PARAMETER_KEYWORDS ("Delphi-Parameter-Keywords", FunctionMemberEnum.IN),
	DB_FIELD_TYPE ("Delphi-DbFieldType", BasicMemberEnum.FIELD),
	;

	public final static OptionDefinitionString OPTION_DEFINITION_PARAMETER_KEYWORDS
        = new OptionDefinitionString(PARAMETER_KEYWORDS.getName(), "", false, true);
	
	public final static OptionDefinitionString OPTION_DEFINITION_DB_FIELD_TYPE
        = new OptionDefinitionString(DB_FIELD_TYPE.getName(), "", false, false);
	
	private final String name;

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private DelphiOptionEnum(String name) {
		this(name, null);
	}

    private DelphiOptionEnum(String name, IMetatype exactMetatype) {
		this(name, exactMetatype, false);
	}

    private DelphiOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(name, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private DelphiOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this.name = name;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	public static List<DelphiOptionEnum> getElementOptions(IElement element) {
		List<DelphiOptionEnum> result = new ArrayList<>();
		for (DelphiOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<DelphiOptionEnum> getMemberOptions(IMember member) {
		List<DelphiOptionEnum> result = new ArrayList<>();
		for (DelphiOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}
}
