package com.gs.gapp.dsl.ejb;

import java.util.HashMap;

public enum EjbImplementationEnum  {

	SKELETON ("Skeleton"),
	PERSISTENCE_JPA ("PersistenceJpa"),
	REST_RETROFIT ("RestRetrofit"),
	REST_JACKSON ("RestJackson"),
	REST_JSON_API ("RestJsonApi"),
	REST_UNIREST ("RestUnirest"),
	SOAP_JAX_WS ("SoapJaxWs"),
	;

	private static HashMap<String, EjbImplementationEnum> entryMap =
			new HashMap<>();
	
	static {
		for (EjbImplementationEnum enumEntry : values()) {
            entryMap.put(enumEntry.getName().toLowerCase(), enumEntry);
		}
	}
	
	private final String name;

	/**
	 * @param name
	 */
	private EjbImplementationEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * @return
	 */
	public static EjbImplementationEnum forName(String name) {
		if (name != null) {
            return entryMap.get(name.toLowerCase());
		}
		return null;
	}
}
