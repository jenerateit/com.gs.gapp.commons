package com.gs.gapp.dsl.product;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;

public enum ProductElementEnum implements IElement {

	ORGANIZATION ("organization"),
	PRODUCT ("product"),
	VARIANT ("variant"),
	CAPABILITY ("capability"),
	FEATURE ("feature"),
	APPLICATION ("application"),
	APPLICATION_UI ("application-ui"),
	APPLICATION_SERVICE ("application-service"),
	APPLICATION_IOT ("application-iot"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private ProductElementEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	public static List<String> getElementTypeNames() {
		List<String> result = new ArrayList<>();
		for (IElement element : ProductElementEnum.values()) {
			result.add(element.getName());
		}

		return result;
	}
}
