package com.gs.gapp.dsl.ui;



public enum FormatEnum  {

	DATE ("Date"),
	TIME ("Time"),
	DATE_TIME ("DateTime"),
	TIMESTAMP ("Timestamp"),
	NUMBER ("Number"),
	DECIMAL_NUMBER ("DecimalNumber"),
	CURRENCY ("Currency"),
	PERCENT ("Percent"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private FormatEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static FormatEnum getByName(String name) {
		for (FormatEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
