package com.gs.gapp.dsl.android;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;
import com.gs.gapp.dsl.ui.UiElementEnum;


public enum AndroidOptionEnum implements IOption {

	// for application element
	EXTEND_APP_CLASS ("Android-ExtendAppClass", UiElementEnum.APPLICATION),
	NAVIGATION_WITHIN_TABS ("Android-NavigationWithinTabs", UiElementEnum.APPLICATION),

	// for display element
	LIST_ADAPTER_TYPE ("Android-ListAdapterType", UiElementEnum.DISPLAY),

	// for component element
	CHOICE_COMPONENT_TYPE ("Android-ChoiceComponentType", UiElementEnum.COMPONENT),
	BOOLEAN_CHOICE_COMPONENT_TYPE ("Android-BooleanChoiceComponentType", UiElementEnum.COMPONENT),
	RADIO_BUTTON_POSTFIXES ("Android-RadioButtonPostfixes", UiElementEnum.COMPONENT),
	CONTEXTUAL ("Android-Contextual", UiElementEnum.TOOLBAR),

	;

	private final String name;

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private AndroidOptionEnum(String name) {
		this(name, null);
	}

    private AndroidOptionEnum(String name, IMetatype exactMetatype) {
		this(name, exactMetatype, false);
	}

    private AndroidOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(name, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private AndroidOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this.name = name;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	/**
	 * @param element
	 * @return
	 */
	public static List<AndroidOptionEnum> getElementOptions(IElement element) {
		List<AndroidOptionEnum> result = new ArrayList<>();
		for (AndroidOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	/**
	 * @param member
	 * @return
	 */
	public static List<AndroidOptionEnum> getMemberOptions(IMember member) {
		List<AndroidOptionEnum> result = new ArrayList<>();
		for (AndroidOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}
}
