package com.gs.gapp.dsl.basic;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;

public enum BasicElementEnum implements IElement {

	TYPE ("type"),
	ENUMERATION ("enumeration"),
	EXCEPTION ("exception"),
	MODULE ("module"),
	VERSION ("version"),
	;
	

	private final String name;

	/**
	 * @param name
	 */
	private BasicElementEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	public static List<String> getElementTypeNames() {
		List<String> result = new ArrayList<>();
		for (IElement element : BasicElementEnum.values()) {
			result.add(element.getName());
		}

		return result;
	}
}
