package com.gs.gapp.dsl.jpa;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;
import com.gs.gapp.dsl.basic.BasicMemberEnum;
import com.gs.gapp.dsl.persistence.PersistenceElementEnum;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;


public enum JpaOptionEnum implements IOption {

	JPA_TABLE_NAME ("JPA-TableName", PersistenceElementEnum.ENTITY),
	JPA_COLUMN_NAME ("JPA-ColumnName", BasicMemberEnum.FIELD),
	JPA_JOIN_TABLE ("JPA-JoinTable", BasicMemberEnum.FIELD),
	JPA_INHERITANCE_STRATEGY ("JPA-InheritanceStrategy", PersistenceElementEnum.ENTITY),
	;
	
	public final static OptionDefinitionString OPTION_DEFINITION_JPA_INHERITANCE_STRATEGY
	    = new OptionDefinitionString(JPA_INHERITANCE_STRATEGY.getName());

	private final String name;

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;
	

	private JpaOptionEnum(String name) {
		this(name, null);
	}

    private JpaOptionEnum(String name, IMetatype exactMetatype) {
		this(name, exactMetatype, false);
	}
    
    private JpaOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(name, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private JpaOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this.name = name;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	/**
	 * @param element
	 * @return
	 */
	public static List<JpaOptionEnum> getElementOptions(IElement element) {
		List<JpaOptionEnum> result = new ArrayList<>();
		for (JpaOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	/**
	 * @param member
	 * @return
	 */
	public static List<JpaOptionEnum> getMemberOptions(IMember member) {
		List<JpaOptionEnum> result = new ArrayList<>();
		for (JpaOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}

}