package com.gs.gapp.dsl.ui;

public enum LayoutContainerTypeEnum  {

	TAB ("Tab"),
	SPLIT ("Split"),
	WIZARD ("Wizard"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private LayoutContainerTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static LayoutContainerTypeEnum getByName(String name) {
		for (LayoutContainerTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
