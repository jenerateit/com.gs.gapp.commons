package com.gs.gapp.dsl.java;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;
import com.gs.gapp.dsl.basic.BasicMemberEnum;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionBoolean;


public enum JavaOptionEnum implements IOption {

	USE_PRIMITIVE_TYPE_WRAPPER ("Java-UsePrimitiveWrapper", BasicMemberEnum.FIELD),
	;

	public final static OptionDefinitionBoolean OPTION_DEFINITION_USE_PRIMITIVE_TYPE_WRAPPER
        = new OptionDefinitionBoolean(USE_PRIMITIVE_TYPE_WRAPPER.getName());
	
	private final String name;

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private JavaOptionEnum(String name) {
		this(name, null);
	}

    private JavaOptionEnum(String name, IMetatype exactMetatype) {
		this(name, exactMetatype, false);
	}

    private JavaOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(name, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private JavaOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this.name = name;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	public static List<JavaOptionEnum> getElementOptions(IElement element) {
		List<JavaOptionEnum> result = new ArrayList<>();
		for (JavaOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<JavaOptionEnum> getMemberOptions(IMember member) {
		List<JavaOptionEnum> result = new ArrayList<>();
		for (JavaOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}
}
