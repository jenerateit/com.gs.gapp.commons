package com.gs.gapp.dsl;

public interface IOption {

	/**
	 * @return
	 */
	String getName();

	IMetatype getExactMetatype();

	boolean isUsingDeepFilter();

	String getMetatypePattern();
}
