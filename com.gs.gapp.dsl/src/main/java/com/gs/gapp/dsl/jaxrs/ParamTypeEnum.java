package com.gs.gapp.dsl.jaxrs;



/**
 * @deprecated use RestOptionEnum.ParamTypeEnum instead 
 * @author mmt
 *
 */
@Deprecated
public enum ParamTypeEnum  {

	QUERY_PARAM ("QueryParam"),
	PATH_PARAM ("PathParam"),
	FORM_PARAM ("FormParam"),
	HEADER_PARAM ("HeaderParam"),
	COOKIE_PARAM ("CookieParam"),
	MATRIX_PARAM ("MatrixParam"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private ParamTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static ParamTypeEnum getByName(String name) {
		for (ParamTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
