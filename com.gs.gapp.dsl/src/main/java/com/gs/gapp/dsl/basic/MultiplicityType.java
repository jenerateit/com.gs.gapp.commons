package com.gs.gapp.dsl.basic;

import java.util.HashMap;
import java.util.Map;




/**
 * @author mmt
 *
 */
public enum MultiplicityType {

	// for entity
	ONE_TO_ONE ("OneToOne"),
	ONE_TO_N ("OneToN"),
	N_TO_ONE ("NToOne"),
	M_TO_N ("MToN"),
    ;

	private static Map<String, MultiplicityType> nameToEnumMap =
			new HashMap<>();

	static {
		for (MultiplicityType e : values()) {
			nameToEnumMap.put(e.getName(), e);
		}
	}
	
	public static MultiplicityType getFromName(String name) {
		return nameToEnumMap.get(name);
	}
	
	private final String name;

	/**
	 * @param name
	 */
	private MultiplicityType(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	public String getName() {
		return name;
	}
}
