package com.gs.gapp.dsl.persistence;

import com.gs.gapp.dsl.IElement;

public enum PersistenceModuleTypeEnum implements IElement {

	PERSISTENCE ("Persistence"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private PersistenceModuleTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
}
