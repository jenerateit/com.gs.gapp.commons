package com.gs.gapp.dsl.ui;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;

public enum UiElementEnum implements IElement {

	COMPONENT ("component"),
	DISPLAY ("display"),
	DATABINDING ("databinding"),
	CONTAINER ("container"),
	LAYOUT ("layout"),
	VIEW ("view"),
	TOOLBAR ("toolbar"),
	MENU ("menu"),
	FLOW ("flow"),
	/**
	 * @deprecated application modeling has been moved to the Product DSL
	 */
	APPLICATION ("application"),
	STORAGE ("storage"),
	INTERFACES ("interfaces"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private UiElementEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	public static List<String> getElementTypeNames() {
		List<String> result = new ArrayList<>();
		for (IElement element : UiElementEnum.values()) {
			result.add(element.getName());
		}

		return result;
	}
}
