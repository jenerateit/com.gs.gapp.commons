package com.gs.gapp.dsl.iot;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;


/**
 * @author mmt
 *
 */
public enum IotOptionEnum implements IOption {

	PRODUCER ("Producer", IotElementEnum.NODE),
	VERSION ("Version", IotElementEnum.NODE),
	DEFAULT_PRODUCER ("DefaultProducer"),  // TODO not yet used
	DEVICE_IMPLEMENTATION ("DeviceImplementation", IotElementEnum.DEVICE),
	SENSOR_IMPLEMENTATION ("SensorImplementation", IotMemberEnum.SENSOR_USAGE),
	DEVICES ("Devices", IotElementEnum.APP),
	ACTUATOR_IMPLEMENTATION ("ActuatorImplementation", IotMemberEnum.ACTUATOR_USAGE),
	TOPIC_LEVELS ("TopicLevels"),
	FUNCTIONBLOCK_IMPLEMENTATION ("FunctionblockImplementation", IotMemberEnum.BLOCK),
	MEASURES ("Measures", IotElementEnum.SENSOR),
	UNITS ("Units", IotElementEnum.MEASURE),
	BROKER_IMPLEMENTATION ("BrokerImplementation", IotElementEnum.BROKER),
	APPLICATION_IMPLEMENTATION ("ApplicationImplementation", IotElementEnum.APP),
	SUBSCRIPTIONS ("Subscriptions", IotMemberEnum.CONNECTION),
	PUBLICATIONS ("Publications", IotMemberEnum.CONNECTION),
	QUALIFIER ("Qualifier", IotElementEnum.TOPIC),
	UNGRACEFUL_DISCONNECT_MESSAGE ("UngracefulDisconnectMessage", IotMemberEnum.CONNECTION),  // TODO not yet used
	PERSIST_SESSION ("PersistSession", IotMemberEnum.CONNECTION),  // TODO not yet used
	KEEP_ALIVE_TIME_INTERVAL ("KeepAliveTimeInterval", IotMemberEnum.CONNECTION),  // TODO not yet used
	AUTHENTICATION ("Authentication", IotMemberEnum.CONNECTION),  // TODO not yet used
	ORGANISATION_NAME ("OrganisationName", IotElementEnum.PRODUCER),
	HOMEPAGE ("Homepage", IotElementEnum.PRODUCER),
	APP_TYPE ("AppType", IotElementEnum.APP),
	URL ("URL", IotMemberEnum.BRIDGE),
	TRIGGERS ("Triggers", IotMemberEnum.SENSOR_USAGE),
	
	STATUS ("Status", IotElementEnum.HARDWARE),
	OPERATIONS ("Operations", IotElementEnum.HARDWARE),
	CONFIGURATION ("Configuration", IotElementEnum.HARDWARE),
	FAULT ("Fault", IotElementEnum.HARDWARE),
	CONNECTION ("Connection", IotElementEnum.HARDWARE),
	
	SAMPLING_INTERVAL ("SamplingInterval", IotMemberEnum.SENSOR_USAGE),
	HISTORY_LENGTH ("HistoryLength", IotMemberEnum.SENSOR_USAGE),
	;
	
	private final String name;

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private IotOptionEnum(String name) {
		this(name, null);
	}

    private IotOptionEnum(String name, IMetatype exactMetatype) {
		this(name, exactMetatype, false);
	}

    private IotOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(name, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private IotOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this.name = name;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	public static List<IotOptionEnum> getElementOptions(IElement element) {
		List<IotOptionEnum> result = new ArrayList<>();
		for (IotOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<IotOptionEnum> getMemberOptions(IMember member) {
		List<IotOptionEnum> result = new ArrayList<>();
		for (IotOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}
}
