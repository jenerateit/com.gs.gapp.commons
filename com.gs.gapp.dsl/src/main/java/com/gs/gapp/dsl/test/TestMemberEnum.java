package com.gs.gapp.dsl.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.ui.UiElementEnum;

public enum TestMemberEnum implements IMember {

	STEP ("step", new IElement[] {TestElementEnum.TEST}, new IElement[] {UiElementEnum.LAYOUT}),
	;

	private final String name;
	private final List<IElement> owners = new ArrayList<>();
	private final List<IElement> types = new ArrayList<>();

	/**
	 * @param name
	 */
	private TestMemberEnum(String name, IElement[] owners, IElement[] types) {
		this.name = name;
		if (owners != null) {
			for (IElement element : owners) {
				this.owners.add(element);
			}
		}

		if (types != null) {
			for (IElement element : types) {
				this.types.add(element);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMember#getName()
	 */
	@Override
	public String getName() {
		return name;
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMember#getOwners()
	 */
	@Override
	public List<IElement> getOwners() {
		return Collections.unmodifiableList(owners);
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMember#getTypes()
	 */
	@Override
	public List<IElement> getTypes() {
		return Collections.unmodifiableList(types);
	}
}
