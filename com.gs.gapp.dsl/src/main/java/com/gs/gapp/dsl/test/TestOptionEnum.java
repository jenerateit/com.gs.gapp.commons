package com.gs.gapp.dsl.test;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;


/**
 * @author mmt
 *
 */
public enum TestOptionEnum implements IOption {

	ACTIONS ("Actions", TestMemberEnum.STEP),
	PRELUDE_PARAMETERS ("PreludeParameters", TestElementEnum.TEST),
	PRELUDE_VIEWS ("PreludeViews", TestElementEnum.TEST),
	OPEN_THE_STEP_VIEW ("OpenTheStepView", TestMemberEnum.STEP),
	;
	
	private final String name;

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private TestOptionEnum(String name) {
		this(name, null);
	}

    private TestOptionEnum(String name, IMetatype exactMetatype) {
		this(name, exactMetatype, false);
	}

    private TestOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(name, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private TestOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this.name = name;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	public static List<TestOptionEnum> getElementOptions(IElement element) {
		List<TestOptionEnum> result = new ArrayList<>();
		for (TestOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<TestOptionEnum> getMemberOptions(IMember member) {
		List<TestOptionEnum> result = new ArrayList<>();
		for (TestOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}
}
