package com.gs.gapp.dsl.ui;

import com.gs.gapp.dsl.ITypeOfMember;


public enum ComponentsEnum implements ITypeOfMember {

	COMPONENT ("Component"),
	ACTION_COMPONENT ("ActionComponent"),
	TEXT_FIELD ("TextField"),
	TEXT_AREA ("TextArea"),
	BOOLEAN_CHOICE ("BooleanChoice"),
	DATE_SELECTOR ("DateSelector"),
	CHOICE ("Choice"),
	RANGE ("Range"),
	LABEL ("Label"),
	EMBEDDED_CONTAINER ("EmbeddedContainer"),
	ACTION ("Action"),
	BUTTON ("Button"),
	LINK ("Link"),
	MAP ("Map"),
	HTML ("HTML"),
	IMAGE ("Image"),
	VIDEO ("Video"),
	EMBEDDED_COMPONENT_GROUP ("EmbeddedComponentGroup"),
	FILE_UPLOAD ("FileUpload"),
	FILE_DOWNLOAD ("FileDownload"),
	TIMELINE ("Timeline"),
	TREE ("Tree"),
	CHART ("Chart"),
	;
	

	private final String name;

	/**
	 * @param name
	 */
	private ComponentsEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
}
