package com.gs.gapp.dsl.jaxrs;


public enum ContextParamTypeEnum  {

	URI_INFO ("UriInfo"),
	HTTP_HEADERS ("HttpHeaders"),
	SECURITY_CONTEXT ("SecurityContext"),
	REQUEST ("Request"),
	PROVIDERS ("Providers"),
	HTTP_SERVLET_REQUEST ("HttpServletRequest"),
	HTTP_SERVLET_RESPONSE ("HttpServletResponse"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private ContextParamTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static ContextParamTypeEnum getByName(String name) {
		for (ContextParamTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
