package com.gs.gapp.dsl.function;

import com.gs.gapp.dsl.IElement;

public enum FunctionModuleTypeEnum implements IElement {

	FUNCTION ("Function"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private FunctionModuleTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
}
