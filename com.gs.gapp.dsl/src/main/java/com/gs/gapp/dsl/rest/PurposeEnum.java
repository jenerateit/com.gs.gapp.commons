package com.gs.gapp.dsl.rest;



public enum PurposeEnum  {

	CREATE ("Create"),
	READ ("Read"),
	UPDATE ("Update"),
	DELETE ("Delete"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private PurposeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static PurposeEnum getByName(String name) {
		for (PurposeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
