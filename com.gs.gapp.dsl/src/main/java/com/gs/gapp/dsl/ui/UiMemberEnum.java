package com.gs.gapp.dsl.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;

public enum UiMemberEnum implements IMember {

	COMPONENT ("component", new IElement[] {UiElementEnum.DISPLAY, UiElementEnum.TOOLBAR}, new IElement[] {UiElementEnum.COMPONENT}),
	;

	private final String name;
	private final List<IElement> owners = new ArrayList<>();
	private final List<IElement> types = new ArrayList<>();

	/**
	 * @param name
	 */
	private UiMemberEnum(String name, IElement[] owners, IElement[] types) {
		this.name = name;
		if (owners != null) {
			for (IElement element : owners) {
				this.owners.add(element);
			}
		}

		if (types != null) {
			for (IElement element : types) {
				this.types.add(element);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMember#getName()
	 */
	@Override
	public String getName() {
		return name;
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMember#getOwners()
	 */
	@Override
	public List<IElement> getOwners() {
		return Collections.unmodifiableList(owners);
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMember#getTypes()
	 */
	@Override
	public List<IElement> getTypes() {
		return Collections.unmodifiableList(types);
	}
}
