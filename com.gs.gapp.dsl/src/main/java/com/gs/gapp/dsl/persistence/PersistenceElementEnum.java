package com.gs.gapp.dsl.persistence;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;

public enum PersistenceElementEnum implements IElement {

	ENTITY ("entity"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private PersistenceElementEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	public static List<String> getElementTypeNames() {
		List<String> result = new ArrayList<>();
		for (IElement element : PersistenceElementEnum.values()) {
			result.add(element.getName());
		}

		return result;
	}
}
