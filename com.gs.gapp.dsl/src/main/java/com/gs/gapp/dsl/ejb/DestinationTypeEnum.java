package com.gs.gapp.dsl.ejb;



public enum DestinationTypeEnum  {

	QUEUE ("Queue"),
	TOPIC ("Topic"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private DestinationTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static DestinationTypeEnum getByName(String name) {
		for (DestinationTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
