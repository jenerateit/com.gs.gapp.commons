package com.gs.gapp.dsl.test;

import com.gs.gapp.dsl.IElement;

public enum TestModuleTypeEnum implements IElement {

	TEST ("Test"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private TestModuleTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
}
