package com.gs.gapp.dsl.ui;

public enum FilterModeEnum  {

	STARTS_WITH ("StartsWith"),
	CONTAINS ("Contains"),
	EXACT ("Exact"),
	ENDS_WITH ("EndsWith"),
	LESS_THAN ("LessThan"),
	LESS_THAN_OR_EQUAL ("LessThanOrEqual"),
	GREATER_THAN ("GreaterThan"),
	GREATER_THAN_OR_EQUAL ("GreaterThanOrEqual"),
	EQUAL ("Equal"),
	IN ("In"),
	;
	
	private final String name;

	/**
	 * @param name
	 */
	private FilterModeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static FilterModeEnum getByName(String name) {
		for (FilterModeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
