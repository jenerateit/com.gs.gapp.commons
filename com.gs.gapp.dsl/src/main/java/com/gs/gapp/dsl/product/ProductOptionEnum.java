package com.gs.gapp.dsl.product;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;
import com.gs.gapp.dsl.ITypeOfMember;


public enum ProductOptionEnum implements IOption {

	PRODUCT ("Product", ProductElementEnum.VARIANT),
	CAPABILITIES ("Capabilities"),
	/**
	 * @deprecated this is going to be replaced by {@link ProductOptionEnum#UI_MODULES}, {@link ProductOptionEnum#PROVIDED_SERVICES}, ...
	 */
	MODULES ("Modules"),
	ORGANIZATION ("Organization", ProductElementEnum.PRODUCT),
	FEATURES ("Features"),
	ACTIVE ("Active", ProductElementEnum.FEATURE),
	CONFIGURATION_ITEMS ("ConfigurationItems", ProductElementEnum.CAPABILITY),
	
	/**
	 * 
	 */
	SERVICES ("Services", ProductElementEnum.APPLICATION_SERVICE),
	
	/**
	 * This option nominates one or more function or persistence modules that contain 'entity', 'function' and 'implementation' elements.
	 * A provided service can be a webservice endpoint or a message subscriber. In short: PROVIDED = YOU GET CALLED
	 * 
	 * TODO user results in converters
	 */
	PROVIDED_SERVICES ("ProvidedServices", ProductElementEnum.CAPABILITY),
	
	
	/**
	 * This option nominates one or more function or persistence modules that contain 'entity', 'function' and 'implementation' elements.
	 * A consumed service can be a webservice client or a message publisher. In short: CONSUMED = YOU CALL
	 * 
	 * TODO user results in converters
	 */
	CONSUMED_SERVICES ("ConsumedServices", ProductElementEnum.CAPABILITY),
	
	/**
	 * User interface modules nominate the user interface pages that are part of the capability.
	 * 
	 * TODO use results in converters
	 */
	USER_INTERFACES ("UserInterfaces", ProductElementEnum.CAPABILITY),
	
	/**
	 * This option nominates one or more function or persistence modules.
	 * Those modules represent the capability's storage functionality (e.g. JPA for Java generation).
	 * 
	 * TODO use results in converters
	 */
	STORAGE ("storage", ProductElementEnum.CAPABILITY),
	
	/**
	 * This option nominates one or more function modules.
	 * Those modules represent the capability's business logic.
	 * For Java generation there are typically EJBs generated for those modules.
	 * 
	 * TODO use results in converters
	 */
	BUSINESS_LOGIC ("BusinessLogic", ProductElementEnum.CAPABILITY),
	
	/**
	 * This option nominates one or more service applications
	 * that a capability provides.
	 * 
	 */
	SERVICE_APPLICATIONS ("ServiceApplications", ProductElementEnum.CAPABILITY),
	
	/**
	 * This option nominates one or more ui applications
	 * that a capability provides.
	 * 
	 */
	UI_APPLICATIONS ("UiApplications", ProductElementEnum.CAPABILITY),
	
	/**
	 * Nominates some function and persistence modules that alltogether
	 * represent the default storage for the capability. 
	 */
	DEFAULT_STORAGE ("DefaultStorage", ProductElementEnum.CAPABILITY),
	
	ROOT ("Root", ProductElementEnum.APPLICATION_UI),
	STORAGE_APPLICATION ("Storage", ProductElementEnum.APPLICATION_UI),
	MENUS ("Menus", ProductElementEnum.APPLICATION_UI),
	DEFAULT_LANGUAGE ("DefaultLanguage", ProductElementEnum.APPLICATION),
	
	/**
	 * This option indirectly links function modules to an application.
	 * As if 28-Nov-2018 this information is not yet used for anything.
	 * 
	 * @deprecated since this option is not yet used, we are going to remove it at some point (mmt 28-Nov-2018)
	 */
	@Deprecated
	INTERFACES ("Interfaces", ProductElementEnum.APPLICATION_UI),
	
	OFFLINE_CAPABILITY ("OfflineCapability", ProductElementEnum.APPLICATION),
	
	RENDERED_LAYOUT_AREAS ("RenderedLayoutAreas", ProductElementEnum.APPLICATION_UI),
	CUSTOMIZED_LAYOUT_AREAS ("CustomizedLayoutAreas", ProductElementEnum.APPLICATION_UI),
	UI_MODULES ("UiModules", ProductElementEnum.APPLICATION_UI),
	
	EXTENDS ("Extends", ProductElementEnum.CAPABILITY),
	;

	private final String name;

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private final ITypeOfMember typeofMember;

	private final boolean usingDeepFilterForTypeofMember;

	private ProductOptionEnum(String name) {
		this(name, null);
	}

    /**
     * @param name
     * @param exactMetatype
     */
    private ProductOptionEnum(String name, IMetatype exactMetatype) {
		this(name, exactMetatype, false);
	}

    /**
     * @param name
     * @param exactMetatype
     * @param usingDeepFilter
     */
    private ProductOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(name, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private ProductOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this(name, exactMetatype, usingDeepFilter, metatypePattern, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param typeofMember
	 * @param usingDeepFilterForTypeofMember
	 */
	private ProductOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, ITypeOfMember typeofMember, boolean usingDeepFilterForTypeofMember) {
		this(name, exactMetatype, usingDeepFilter, null, typeofMember, usingDeepFilterForTypeofMember);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 * @param typeofMember
	 */
	private ProductOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern, ITypeOfMember typeofMember) {
		this(name, exactMetatype, usingDeepFilter, metatypePattern, typeofMember, false);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 * @param typeofMember
	 * @param usingDeepFilterForTypeofMember
	 */
	private ProductOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern, ITypeOfMember typeofMember, boolean usingDeepFilterForTypeofMember) {
		this.name = name;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
		this.typeofMember = typeofMember;
		this.usingDeepFilterForTypeofMember = usingDeepFilterForTypeofMember;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	public static List<ProductOptionEnum> getElementOptions(IElement element) {
		List<ProductOptionEnum> result = new ArrayList<>();
		for (ProductOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<ProductOptionEnum> getMemberOptions(IMember member) {
		List<ProductOptionEnum> result = new ArrayList<>();
		for (ProductOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}

	/**
	 * @return the typeofMember
	 */
	public ITypeOfMember getTypeofMember() {
		return typeofMember;
	}

	/**
	 * @return the usingDeepFilterForTypeofMember
	 */
	public boolean isUsingDeepFilterForTypeofMember() {
		return usingDeepFilterForTypeofMember;
	}
}
