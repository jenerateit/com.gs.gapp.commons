package com.gs.gapp.dsl.ui;



public enum ChartTypeEnum  {

	BAR ("Bar"),
	POLAR_AREA ("PolarArea"),
	BUBBLE ("Bubble"),
	RADAR ("Radar"),
	DONUT ("Donut"),
	LINE ("Line"),
	PIE ("Pie"),
	SCATTER ("Scatter"),
    ;
	
	private final String name;

	/**
	 * @param name
	 */
	private ChartTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static ChartTypeEnum getByName(String name) {
		for (ChartTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
