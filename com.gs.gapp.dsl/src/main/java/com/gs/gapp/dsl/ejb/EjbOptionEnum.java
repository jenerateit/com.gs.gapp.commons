package com.gs.gapp.dsl.ejb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;
import com.gs.gapp.dsl.basic.BasicElementEnum;
import com.gs.gapp.dsl.function.FunctionMemberEnum;
import com.gs.gapp.dsl.persistence.PersistenceElementEnum;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionBoolean;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;


public enum EjbOptionEnum implements IOption {

	// for module
	EJB_TYPE (new OptionDefinitionString("EJB-Type"), BasicElementEnum.MODULE),
	EJB_WEB_SERVICE (new OptionDefinitionBoolean("EJB-WebService"), BasicElementEnum.MODULE),
	EJB_INTERFACES (new OptionDefinitionString("EJB-Interfaces"), BasicElementEnum.MODULE),
	
	EJB_EXCEPTION_ROLLBACK_TRANSACTION (new OptionDefinitionString("EJB-RollbackTransaction"), FunctionMemberEnum.EXCEPTION),
	
	EJB_IMPLEMENTATION_FOR_ENTITY (new OptionDefinitionString("EJB-Implementation"), PersistenceElementEnum.ENTITY),
	EJB_IMPLEMENTATION_FOR_MODULE (new OptionDefinitionString("EJB-Implementation"), BasicElementEnum.MODULE),
	
	;
	
	public final static OptionDefinitionBoolean OPTION_DEFINITION_EXCEPTION_ROLLBACK_TRANSACTION
        = new OptionDefinitionBoolean(EJB_EXCEPTION_ROLLBACK_TRANSACTION.getName());

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private final OptionDefinition<?> definition;

	private EjbOptionEnum(OptionDefinition<?> definition) {
		this(definition, null);
	}

    private EjbOptionEnum(OptionDefinition<?> definition, IMetatype exactMetatype) {
		this(definition, exactMetatype, false);
	}

    private EjbOptionEnum(OptionDefinition<?> definition, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(definition, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private EjbOptionEnum(OptionDefinition<?> definition, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this.definition = definition;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return definition.getKey();
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	public static List<EjbOptionEnum> getElementOptions(IElement element) {
		List<EjbOptionEnum> result = new ArrayList<>();
		for (EjbOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<EjbOptionEnum> getMemberOptions(IMember member) {
		List<EjbOptionEnum> result = new ArrayList<>();
		for (EjbOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}

	/**
	 * @return the definition
	 */
	@SuppressWarnings("unchecked")
	public <T extends Serializable> OptionDefinition<T> getDefinition(Class<T> clazz) {
		return (OptionDefinition<T>)definition;
	}
}
