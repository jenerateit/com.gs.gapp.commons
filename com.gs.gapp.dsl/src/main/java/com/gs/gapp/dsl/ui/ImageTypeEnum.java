package com.gs.gapp.dsl.ui;

/**
 * @author marcu
 *
 */
public enum ImageTypeEnum  {

	SVG ("SVG"),
	DATA_URI ("DATA_URI"),
	IMAGE ("IMAGE"),
	ANY ("ANY"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private ImageTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static ImageTypeEnum getByName(String name) {
		for (ImageTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
