package com.gs.gapp.dsl.iot;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;

public enum IotElementEnum implements IElement {
	
	APP ("app"),
	BROKER ("broker"),
	TOPIC ("topic"),
	PRODUCER ("producer"),
	MEASURE ("measure"),
	UNIT ("unit"),
	SENSOR ("sensor"),
	ACTUATOR ("actuator"),
	FUNCTIONBLOCK ("functionblock"),
	HARDWARE ("hardware"),
	DEVICE ("device"),
	NODE ("node"),  // a node is only represented by an abstract parent class
	;

	private final String name;

	/**
	 * @param name
	 */
	private IotElementEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	public static List<String> getElementTypeNames() {
		List<String> result = new ArrayList<>();
		for (IElement element : IotElementEnum.values()) {
			result.add(element.getName());
		}

		return result;
	}
}
