package com.gs.gapp.dsl.basic;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;


public enum BasicOptionEnum implements IOption {

	COLLECTION_TYPE ("CollectionType", BasicMemberEnum.FIELD),
	EQUALITY_RELEVANCE ("EqualityRelevance", BasicMemberEnum.FIELD),
	READ_ONLY ("ReadOnly", BasicMemberEnum.FIELD),
	DEFAULT_VALUE ("DefaultValue", BasicMemberEnum.FIELD),
	MULTIPLICITY_TYPE ("MultiplicityType", BasicMemberEnum.FIELD),
	CARDINALITY ("Cardinality", BasicMemberEnum.FIELD),
	LENGTH ("Length", BasicMemberEnum.FIELD),
	MINIMUM_LENGTH ("MinimumLength", BasicMemberEnum.FIELD),
	MANDATORY ("Mandatory", BasicMemberEnum.FIELD),
	MAXIMUM_VALUE ("MaximumValue", BasicMemberEnum.FIELD),
	MINIMUM_VALUE ("MinimumValue", BasicMemberEnum.FIELD),
	MAXIMUM_DECIMAL_VALUE ("MaximumDecimalValue", BasicMemberEnum.FIELD),
	MINIMUM_DECIMAL_VALUE ("MinimumDecimalValue", BasicMemberEnum.FIELD),
	
	VERSION_NUMBER ("VersionNumber", BasicElementEnum.VERSION),
	
	VERSION ("Version"),
	SINCE_VERSION ("SinceVersion"),
	DEPRECATED_SINCE_VERSION ("DeprecatedSinceVersion"),
	
	EXPANDABLE ("Expandable", BasicMemberEnum.FIELD),
	KEYTYPE ("KeyType", BasicMemberEnum.FIELD),
	DIMENSION ("Dimension", BasicMemberEnum.FIELD),
	
	CONSTRAINED_TYPE ("ConstrainedType", BasicElementEnum.TYPE),
	
	EXTERNALIZABLE ("Externalizable", BasicElementEnum.TYPE),
	
	BINARY_CODING ("BinaryCoding", BasicElementEnum.TYPE),
	BINARY_CODING_ENDIANNESS ("BinaryCodingEndianness", BasicElementEnum.TYPE),
	NUMBER_OF_BYTES ("NumberOfBytes", BasicMemberEnum.FIELD),
	;

	private final String name;

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private BasicOptionEnum(String name) {
		this(name, null);
	}

    private BasicOptionEnum(String name, IMetatype exactMetatype) {
		this(name, exactMetatype, false);
	}

    private BasicOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(name, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private BasicOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this.name = name;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	public static List<BasicOptionEnum> getElementOptions(IElement element) {
		List<BasicOptionEnum> result = new ArrayList<>();
		for (BasicOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<BasicOptionEnum> getMemberOptions(IMember member) {
		List<BasicOptionEnum> result = new ArrayList<>();
		for (BasicOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}
}
