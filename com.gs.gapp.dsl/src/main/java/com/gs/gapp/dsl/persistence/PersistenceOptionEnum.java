package com.gs.gapp.dsl.persistence;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;
import com.gs.gapp.dsl.basic.BasicMemberEnum;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionBoolean;


public enum PersistenceOptionEnum implements IOption {

	// for entity
	STORABLE ("Storable", PersistenceElementEnum.ENTITY),
	UTILITY_FIELDS ("UtilityFields", PersistenceElementEnum.ENTITY),
	NATURE ("Nature", PersistenceElementEnum.ENTITY),
	EXTERNALIZE ("Externalize", PersistenceElementEnum.ENTITY),
	EXCEPTIONS ("Exceptions", PersistenceElementEnum.ENTITY),
	FUNCTIONS ("Functions", PersistenceElementEnum.ENTITY),
	CONSUMED_ENTITIES ("ConsumedEntities", PersistenceElementEnum.ENTITY),
	NUMBER_OF_TEST_ENTRIES ("NumberOfTestEntries", PersistenceElementEnum.ENTITY),

	// for entity fields
	TRANSIENT ("Transient", BasicMemberEnum.FIELD),
	ID ("Id", BasicMemberEnum.FIELD),
	BUSINESS_ID ("BusinessId", BasicMemberEnum.FIELD),
	UNIQUE ("Unique", BasicMemberEnum.FIELD),
	PRIVATELY_OWNED ("PrivatelyOwned", BasicMemberEnum.FIELD),
	ENUMERATION_TYPE ("EnumerationType", BasicMemberEnum.FIELD),
	BINARY ("Binary", BasicMemberEnum.FIELD),
	LAZY_LOADING ("LazyLoading", BasicMemberEnum.FIELD),
	CASCADE_TYPES ("CascadeTypes", BasicMemberEnum.FIELD),
	ORDER_BY ("OrderBy", BasicMemberEnum.FIELD),
	RELATES_TO ("RelatesTo", BasicMemberEnum.FIELD),
	KEEP_ORDERING ("KeepOrdering", BasicMemberEnum.FIELD),
	SEARCHABLE ("Searchable", BasicMemberEnum.FIELD),
	CACHEABLE_FIELD ("Cacheable", BasicMemberEnum.FIELD),
	CACHEABLE_ENTITY ("Cacheable", PersistenceElementEnum.ENTITY),
	
	TENANT_ENTITY ("TenantEntity", PersistenceElementEnum.ENTITY),
	USER_ACCOUNT_ENTITY ("UserAccountEntity", PersistenceElementEnum.ENTITY),
	;

	public final static OptionDefinitionBoolean OPTION_DEFINITION_EXTERNALIZE
        = new OptionDefinitionBoolean(EXTERNALIZE.getName());
	
	private final String name;

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private PersistenceOptionEnum(String name) {
		this(name, null);
	}

    private PersistenceOptionEnum(String name, IMetatype exactMetatype) {
		this(name, exactMetatype, false);
	}

    private PersistenceOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(name, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private PersistenceOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this.name = name;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	public static List<PersistenceOptionEnum> getElementOptions(IElement element) {
		List<PersistenceOptionEnum> result = new ArrayList<>();
		for (PersistenceOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<PersistenceOptionEnum> getMemberOptions(IMember member) {
		List<PersistenceOptionEnum> result = new ArrayList<>();
		for (PersistenceOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}
}
