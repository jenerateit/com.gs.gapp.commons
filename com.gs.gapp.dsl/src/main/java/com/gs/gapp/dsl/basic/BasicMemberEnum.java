package com.gs.gapp.dsl.basic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;

public enum BasicMemberEnum implements IMember {

	FIELD ("field", new IElement[] {BasicElementEnum.TYPE}, new IElement[] {BasicElementEnum.TYPE}),
	ENTRY ("entry", new IElement[] {BasicElementEnum.ENUMERATION}, new IElement[] {}),
	;

	private final String name;
	private final List<IElement> owners = new ArrayList<>();
	private final List<IElement> types = new ArrayList<>();

	/**
	 * @param name
	 */
	private BasicMemberEnum(String name, IElement[] owners, IElement[] types) {
		this.name = name;
		if (owners != null) {
			for (IElement element : owners) {
				this.owners.add(element);
			}
		}

		if (types != null) {
			for (IElement element : types) {
				this.types.add(element);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMember#getName()
	 */
	@Override
	public String getName() {
		return name;
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMember#getOwners()
	 */
	@Override
	public List<IElement> getOwners() {
		return Collections.unmodifiableList(owners);
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMember#getTypes()
	 */
	@Override
	public List<IElement> getTypes() {
		return Collections.unmodifiableList(types);
	}
}
