package com.gs.gapp.dsl.ui;



public enum FlowTypeEnum  {

	OPEN ("Open"),
	REPLACE ("Replace"),
	PUSH ("Push"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private FlowTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static FlowTypeEnum getByName(String name) {
		for (FlowTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
