package com.gs.gapp.dsl.basic;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import com.gs.gapp.dsl.IElement;

public enum BinaryCodingEndiannessEnum implements IElement {

	BIG_ENDIAN ("BigEndian"),
	LITTLE_ENDIAN ("LittleEndian"),
	;
	
	private static final Map<String,BinaryCodingEndiannessEnum> nameMap = new HashMap<>();
	
	public static BinaryCodingEndiannessEnum forName(String name) {
		if (name != null) {
		    return nameMap.get(name.toLowerCase());
		}
		return null;
	}
	
	static {
		Stream.of(BinaryCodingEndiannessEnum.values()).forEach(endiannessEnum -> nameMap.put(endiannessEnum.getName().toLowerCase(), endiannessEnum));
	}

	private final String name;

	/**
	 * @param name
	 */
	private BinaryCodingEndiannessEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
}
