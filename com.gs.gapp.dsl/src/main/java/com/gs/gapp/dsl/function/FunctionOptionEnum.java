package com.gs.gapp.dsl.function;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;
import com.gs.gapp.dsl.basic.BasicElementEnum;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionSerializable;


public enum FunctionOptionEnum implements IOption {

	COLLECTION_TYPE ("CollectionType"),
	MODULE_DATA ("Data", BasicElementEnum.MODULE),
	IN_PARAM_DATA ("Data", FunctionMemberEnum.IN),
	OUT_PARAM_DATA ("Data", FunctionMemberEnum.OUT),
	DATA_IN ("DataIn", FunctionElementEnum.FUNCTION),
	DATA_OUT ("DataOut", FunctionElementEnum.FUNCTION),
	FUNCTION ("Function", FunctionElementEnum.FUNCTION),
	
	@Deprecated
	PROVIDED_SERVICES ("ProvidedServices", BasicElementEnum.MODULE),
	@Deprecated
	CONSUMED_SERVICES ("ConsumedServices", BasicElementEnum.MODULE),
	
	CONSUMED_ENTITIES ("ConsumedEntities", BasicElementEnum.MODULE),
	INTERFACE_IMPLEMENTATION ("Interface", FunctionElementEnum.IMPLEMENTATION),
	INTERFACE_CLIENT ("Interface", FunctionElementEnum.CLIENT),
	CONSUMED_SERVICE_IMPLEMENTATIONS ("ConsumedServiceImplementations", FunctionElementEnum.CLIENT),
	USED_SERVICE_CLIENTS ("UsedServiceClients", FunctionElementEnum.IMPLEMENTATION),
	USED_SERVICE_INTERFACES ("UsedServiceInterfaces", FunctionElementEnum.IMPLEMENTATION),
	FUNCTION_MODULES ("FunctionModules", FunctionElementEnum.INTERFACE),
	EXCEPTIONS ("Exceptions", FunctionElementEnum.FUNCTION),
	
	PERSISTENCE_MODULES_FOR_STORAGE ("PersistenceModules", FunctionElementEnum.STORAGE),
	FUNCTION_MODULES_FOR_STORAGE ("FunctionModules", FunctionElementEnum.STORAGE),
	
	PERSISTENCE_MODULES_FOR_BUSINESSLOGIC ("PersistenceModules", FunctionElementEnum.BUSINESSLOGIC),
	FUNCTION_MODULES_FOR_BUSINESSLOGIC ("FunctionModules", FunctionElementEnum.BUSINESSLOGIC),
	
	/**
	 * This option is a means to relate custom functions to a function module that gets implicitly created for a modeled entity.
	 */
	ENTITY ("Entity", FunctionElementEnum.FUNCTION),
	
	/**
	 * This option links one or more persistence modules to a service 'interface'.
	 * The entities in those modules are converted to function modules that then are
	 * the input for code generators to generate service implementations and clients.
	 * 
	 * TODO use the result in converters
	 */
	PERSISTENCE_MODULES ("PersistenceModules", FunctionElementEnum.INTERFACE),
	
	/**
	 * This option links one or more entities to a service 'interface'.
	 * The entities are converted to function modules that then are
	 * the input for code generators to generate service implementations and clients.
	 * 
	 * TODO use the result in converters
	 */
	ENTITIES ("Entities", FunctionElementEnum.INTERFACE),
	
	/**
	 * By setting the service pattern you can control the implementation that is getting generated.
	 * In case of Java generation, the PERSISTENCE pattern for instance generates code that uses JPA entity classes.
	 * 
	 * TODO use the result in converters
	 */
	SERVICE_PATTERN ("ServicePattern", FunctionElementEnum.IMPLEMENTATION),
	
	PARAM_MANDATORY ("Mandatory", FunctionMemberEnum.IN),
	;
	
	public final static OptionDefinitionSerializable OPTION_DEFINITION_MODULE_DATA
                            = new OptionDefinitionSerializable(MODULE_DATA.getName());
	
	public final static OptionDefinitionSerializable OPTION_DEFINITION_IN_DATA
                            = new OptionDefinitionSerializable(IN_PARAM_DATA.getName());

	public final static OptionDefinitionSerializable OPTION_DEFINITION_OUT_DATA
                            = new OptionDefinitionSerializable(OUT_PARAM_DATA.getName());
	
	private final String name;

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private FunctionOptionEnum(String name) {
		this(name, null);
	}

    private FunctionOptionEnum(String name, IMetatype exactMetatype) {
		this(name, exactMetatype, false);
	}

    private FunctionOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(name, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private FunctionOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this.name = name;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	public static List<FunctionOptionEnum> getElementOptions(IElement element) {
		List<FunctionOptionEnum> result = new ArrayList<>();
		for (FunctionOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<FunctionOptionEnum> getMemberOptions(IMember member) {
		List<FunctionOptionEnum> result = new ArrayList<>();
		for (FunctionOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}
}
