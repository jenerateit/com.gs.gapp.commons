package com.gs.gapp.dsl.jaxrs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;
import com.gs.gapp.dsl.basic.BasicElementEnum;
import com.gs.gapp.dsl.function.FunctionElementEnum;
import com.gs.gapp.dsl.function.FunctionMemberEnum;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionBoolean;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;


public enum JaxrsOptionEnum implements IOption {

	// for function
	CONTEXT_PARAM_TYPES (new OptionDefinitionString("JAXRS-ContextParamTypes"), FunctionElementEnum.FUNCTION),
	
	// for function module
	ANNOTATIONS_IN_INTERFACE (new OptionDefinitionBoolean("JAXRS-AnnotationsInInterface"), BasicElementEnum.MODULE),
	AS_SESSION_BEAN (new OptionDefinitionBoolean("JAXRS-AsSessionBean"), BasicElementEnum.MODULE),

	// for in parameter
	/**
	 * @deprecated see RestOptionEnum.PARAM_TYPE
	 */
	@Deprecated
	PARAM_TYPE (new OptionDefinitionString("JAXRS-ParamType"), FunctionMemberEnum.IN),
	;


	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private final OptionDefinition<?> definition;

	private JaxrsOptionEnum(OptionDefinition<?> definition) {
		this(definition, null);
	}

    private JaxrsOptionEnum(OptionDefinition<?> definition, IMetatype exactMetatype) {
		this(definition, exactMetatype, false);
	}

    private JaxrsOptionEnum(OptionDefinition<?> definition, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(definition, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private JaxrsOptionEnum(OptionDefinition<?> definition, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this.definition = definition;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;

	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return this.getDefinition(Serializable.class).getKey();
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	public static List<JaxrsOptionEnum> getElementOptions(IElement element) {
		List<JaxrsOptionEnum> result = new ArrayList<>();
		for (JaxrsOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<JaxrsOptionEnum> getMemberOptions(IMember member) {
		List<JaxrsOptionEnum> result = new ArrayList<>();
		for (JaxrsOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}

	/**
	 * @return the definition
	 */
	@SuppressWarnings("unchecked")
	public <T extends Serializable> OptionDefinition<T> getDefinition(Class<T> clazz) {
		return (OptionDefinition<T>)definition;
	}
}
