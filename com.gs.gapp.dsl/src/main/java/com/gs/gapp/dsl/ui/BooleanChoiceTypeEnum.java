package com.gs.gapp.dsl.ui;



public enum BooleanChoiceTypeEnum  {

	BUTTON ("Button"),
	CHECK ("Check"),
	SWITCH ("Switch"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private BooleanChoiceTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static BooleanChoiceTypeEnum getByName(String name) {
		for (BooleanChoiceTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
