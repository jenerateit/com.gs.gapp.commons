package com.gs.gapp.dsl.iot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;

public enum IotMemberEnum implements IMember {

	SENSOR_USAGE ("sensorUsage", new IElement[] {IotElementEnum.FUNCTIONBLOCK}, new IElement[] {IotElementEnum.SENSOR}),
	ACTUATOR_USAGE ("actuatorUsage", new IElement[] {IotElementEnum.FUNCTIONBLOCK}, new IElement[] {IotElementEnum.ACTUATOR}),
	BLOCK ("block", new IElement[] {IotElementEnum.FUNCTIONBLOCK}, new IElement[] {IotElementEnum.FUNCTIONBLOCK}),
	CONNECTION ("connection", new IElement[] {IotElementEnum.APP}, new IElement[] {IotElementEnum.BROKER}),
	BRIDGE ("bridge", new IElement[] {IotElementEnum.BROKER}, new IElement[] {IotElementEnum.BROKER}),
	BUSINESS_LOGIC ("businessLogic", new IElement[] {IotElementEnum.BROKER}, new IElement[] {IotElementEnum.BROKER}),
	;

	private final String name;
	private final List<IElement> owners = new ArrayList<>();
	private final List<IElement> types = new ArrayList<>();

	/**
	 * @param name
	 */
	private IotMemberEnum(String name, IElement[] owners, IElement[] types) {
		this.name = name;
		if (owners != null) {
			for (IElement element : owners) {
				this.owners.add(element);
			}
		}

		if (types != null) {
			for (IElement element : types) {
				this.types.add(element);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMember#getName()
	 */
	@Override
	public String getName() {
		return name;
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMember#getOwners()
	 */
	@Override
	public List<IElement> getOwners() {
		return Collections.unmodifiableList(owners);
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMember#getTypes()
	 */
	@Override
	public List<IElement> getTypes() {
		return Collections.unmodifiableList(types);
	}
}
