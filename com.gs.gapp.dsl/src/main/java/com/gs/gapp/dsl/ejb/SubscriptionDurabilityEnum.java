package com.gs.gapp.dsl.ejb;



public enum SubscriptionDurabilityEnum  {

	DURABLE ("Durable"),
	NON_DURABLE ("NonDurable"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private SubscriptionDurabilityEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static SubscriptionDurabilityEnum getByName(String name) {
		for (SubscriptionDurabilityEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
