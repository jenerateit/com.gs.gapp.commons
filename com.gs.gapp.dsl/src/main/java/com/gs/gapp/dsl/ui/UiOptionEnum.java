package com.gs.gapp.dsl.ui;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;
import com.gs.gapp.dsl.IMember;
import com.gs.gapp.dsl.IMetatype;
import com.gs.gapp.dsl.IOption;
import com.gs.gapp.dsl.ITypeOfMember;


public enum UiOptionEnum implements IOption {

	ABSTRACT ("Abstract", UiMemberEnum.COMPONENT),
	CONTAINERS ("Containers", UiElementEnum.DATABINDING),
	ENTITY_FOR_DATABINDING ("Entity", UiElementEnum.DATABINDING),
	ENTITY_FOR_DISPLAY ("Entity", UiElementEnum.DISPLAY),
	
	/**
	 * This option lets you assign a Function to a group of displays (through the databinding element).
	 * The purpose of the function is to retrieve data for ui data binding.
	 * 
	 * TODO use the results in converters
	 */
	FUNCTION_FOR_DATABINDING ("Function", UiElementEnum.DATABINDING),
	
	/**
	 * With the 'Function' option for a display you can model direct databinding without having to model additional 'databinding' elements.
	 * 
	 * TODO use the results in converters
	 */
	FUNCTION_FOR_DISPLAY ("Function", UiElementEnum.DISPLAY),
	
	/**
	 * This option lets you define other databindings that relate to this databinding.
	 * With this it becomes possible to generate code that retrieves additional data when a data record gets selected in a data container.
	 * 
	 * TODO use the results in converters
	 */
	DEPENDENT_DATABINDINGS ("DependentDatabindings", UiElementEnum.DATABINDING),
	
	OPTIONAL ("Optional", UiElementEnum.CONTAINER),
	DISABLABLE ("Disablable", UiElementEnum.CONTAINER),
	
	TITLE ("Title", UiElementEnum.CONTAINER),
	CHILDREN ("Children", UiElementEnum.CONTAINER),
	TOOLBARS ("Toolbars", UiElementEnum.CONTAINER),
	
	CONSUMED_ENTITIES ("ConsumedEntities", UiElementEnum.CONTAINER),
	
	TYPE_FOR_DISPLAY ("Type", UiElementEnum.DISPLAY),
	TYPE_FOR_LAYOUT ("Type", UiElementEnum.LAYOUT),
	TYPE_FOR_FLOW ("Type", UiElementEnum.FLOW),
	TYPE_FOR_STORAGE ("Type", UiElementEnum.STORAGE),
	ORIENTATION ("Orientation", UiElementEnum.LAYOUT),
	NUMBER_OF_ROWS_PER_PAGE ("NumberOfRowsPerPage", UiElementEnum.DISPLAY),
	SELECTION_MODE ("SelectionMode", UiElementEnum.DISPLAY),
	SORT_BY ("SortBy", UiElementEnum.DISPLAY),
	FILTER_BY ("FilterBy", UiElementEnum.DISPLAY),
	DYNAMIC_STRUCTURE ("DynamicStructure", UiElementEnum.DISPLAY),
	COLLAPSIBLE ("Collapsible", UiElementEnum.DISPLAY),
	
	FROM ("From", UiElementEnum.FLOW),
	TO ("To", UiElementEnum.FLOW),
	COMPONENTS ("Components", UiElementEnum.FLOW),
	
	MODULES_FOR_STORAGE ("Modules", UiElementEnum.STORAGE),
	MODULES_FOR_INTERFACES ("Modules", UiElementEnum.INTERFACES),
	ENTITIES ("Entities", UiElementEnum.STORAGE),
	ENTITY_FIELDS ("EntityFields", UiElementEnum.STORAGE),
	FUNCTIONS ("Functions", UiElementEnum.INTERFACES),
	
	CONFIGURATION_ITEMS ("ConfigurationItems", UiElementEnum.CONTAINER),
	INIT_PARAMETERS ("InitParameters", UiElementEnum.CONTAINER),
	LAYOUT_VARIANTS ("LayoutVariants", UiElementEnum.CONTAINER),
	MESSAGES ("Messages", UiElementEnum.CONTAINER),
	WARNING_MESSAGES ("WarningMessages", UiElementEnum.CONTAINER),
	ERROR_MESSAGES ("ErrorMessages", UiElementEnum.CONTAINER),
	
	SERVICES ("Services", UiElementEnum.LAYOUT),
	MENUS ("Menus", UiElementEnum.VIEW),
	
	/**
	 * Nominates a type that provides parameters that are needed for the execution of some logic before the actual ui page can be tested.
	 * This could be for instance logic to do a login before being able to access the page under test.
	 * 
	 * Note that values for this option normally are only being used for the implicit creation of test for each modeled page.
	 */
	TEST_PRELUDE_PARAMETERS ("TestPreludeParameters", UiElementEnum.CONTAINER),
	
	TEST_PRELUDE_VIEWS ("TestPreludeViews", UiElementEnum.CONTAINER),
	
	CUSTOM_TITLE ("CustomTitle", UiElementEnum.LAYOUT),
	;

	private final String name;

	private final IMetatype exactMetatype;

	private final boolean usingDeepFilter;

	private final String metatypePattern;

	private final ITypeOfMember typeofMember;

	private final boolean usingDeepFilterForTypeofMember;

	private UiOptionEnum(String name) {
		this(name, null);
	}

    /**
     * @param name
     * @param exactMetatype
     */
    private UiOptionEnum(String name, IMetatype exactMetatype) {
		this(name, exactMetatype, false);
	}

    /**
     * @param name
     * @param exactMetatype
     * @param usingDeepFilter
     */
    private UiOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter) {
		this(name, exactMetatype, usingDeepFilter, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 */
	private UiOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern) {
		this(name, exactMetatype, usingDeepFilter, metatypePattern, null);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param typeofMember
	 * @param usingDeepFilterForTypeofMember
	 */
	private UiOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, ITypeOfMember typeofMember, boolean usingDeepFilterForTypeofMember) {
		this(name, exactMetatype, usingDeepFilter, null, typeofMember, usingDeepFilterForTypeofMember);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 * @param typeofMember
	 */
	private UiOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern, ITypeOfMember typeofMember) {
		this(name, exactMetatype, usingDeepFilter, metatypePattern, typeofMember, false);
	}

	/**
	 * @param name
	 * @param exactMetatype
	 * @param usingDeepFilter
	 * @param metatypePattern
	 * @param typeofMember
	 * @param usingDeepFilterForTypeofMember
	 */
	private UiOptionEnum(String name, IMetatype exactMetatype, boolean usingDeepFilter, String metatypePattern, ITypeOfMember typeofMember, boolean usingDeepFilterForTypeofMember) {
		this.name = name;
		this.exactMetatype = exactMetatype;
		this.usingDeepFilter = usingDeepFilter;
		this.metatypePattern = metatypePattern;
		this.typeofMember = typeofMember;
		this.usingDeepFilterForTypeofMember = usingDeepFilterForTypeofMember;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IOption#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	@Override
	public IMetatype getExactMetatype() {
		return exactMetatype;
	}

	@Override
	public boolean isUsingDeepFilter() {
		return usingDeepFilter;
	}

	@Override
	public String getMetatypePattern() {
		return metatypePattern;
	}

	public static List<UiOptionEnum> getElementOptions(IElement element) {
		List<UiOptionEnum> result = new ArrayList<>();
		for (UiOptionEnum entry : values()) {
			if (entry.getExactMetatype() == element ) {
				result.add(entry);
			}
		}
		return result;
	}

	public static List<UiOptionEnum> getMemberOptions(IMember member) {
		List<UiOptionEnum> result = new ArrayList<>();
		for (UiOptionEnum entry : values()) {
			if (entry.getExactMetatype() == member) {
				result.add(entry);
			}
		}
		return result;
	}

	/**
	 * @return the typeofMember
	 */
	public ITypeOfMember getTypeofMember() {
		return typeofMember;
	}

	/**
	 * @return the usingDeepFilterForTypeofMember
	 */
	public boolean isUsingDeepFilterForTypeofMember() {
		return usingDeepFilterForTypeofMember;
	}
}
