package com.gs.gapp.dsl.ui;



public enum MultiplicityEnum  {

	SINGLE_VALUED ("SingleValued"),
	MULTI_VALUED ("MultiValued"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private MultiplicityEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static MultiplicityEnum getByName(String name) {
		for (MultiplicityEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
