package com.gs.gapp.dsl.product;

import com.gs.gapp.dsl.IElement;

public enum ProductModuleTypeEnum implements IElement {

	PRODUCT ("Product"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private ProductModuleTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}
}
