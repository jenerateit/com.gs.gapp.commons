package com.gs.gapp.dsl.ejb;



public enum EjbTypeEnum  {

	STATEFUL ("Stateful"),
	STATELESS ("Stateless"),
	ASYNCHRONOUS ("Asynchronous"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private EjbTypeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static EjbTypeEnum getByName(String name) {
		for (EjbTypeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
