package com.gs.gapp.dsl.ejb;



public enum AcknowledgeModeEnum  {

	AUTO ("Auto"),
	DUPS_OK ("DupsOk"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private AcknowledgeModeEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IMetatype#getName()
	 */
	public String getName() {
		return name;
	}

	public static AcknowledgeModeEnum getByName(String name) {
		for (AcknowledgeModeEnum enumEntry : values()) {
			if (enumEntry.getName().equalsIgnoreCase(name)) {
				return enumEntry;
			}
		}
		return null;
	}
}
