/**
 *
 */
package com.gs.gapp.dsl.function;

import java.util.HashMap;
import java.util.Map;

/**
 * @author swr
 *
 */
public enum DataCollectionTypeEnum {

	LIST ("List"),
	SET ("Set"),
	ARRAY ("Array"),
	;

	
	private static Map<String, DataCollectionTypeEnum> nameToEnumMap =
			new HashMap<>();

	static {
		for (DataCollectionTypeEnum e : values()) {
			nameToEnumMap.put(e.getName(), e);
		}
	}

	private String name;

	/**
	 * @param name
	 */
	private DataCollectionTypeEnum(String name) {
		this.name = name;
	}

	/**
	 * @param name
	 * @return
	 */
	public static DataCollectionTypeEnum getFromName(String name) {
		return nameToEnumMap.get(name);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
