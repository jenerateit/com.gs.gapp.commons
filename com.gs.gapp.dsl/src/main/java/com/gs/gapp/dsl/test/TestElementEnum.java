package com.gs.gapp.dsl.test;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.dsl.IElement;

public enum TestElementEnum implements IElement {
	
	TEST ("test"),
	;

	private final String name;

	/**
	 * @param name
	 */
	private TestElementEnum(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.dsl.IElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	public static List<String> getElementTypeNames() {
		List<String> result = new ArrayList<>();
		for (IElement element : TestElementEnum.values()) {
			result.add(element.getName());
		}

		return result;
	}
}
