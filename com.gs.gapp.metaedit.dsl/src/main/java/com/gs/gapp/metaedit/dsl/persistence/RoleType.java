package com.gs.gapp.metaedit.dsl.persistence;

import com.gs.gapp.dsl.IMetatype;

public enum RoleType implements IMetatype {

	RELATION_END ("RelationEnd"),
	;
	
	private final String name;

	private RoleType(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}
}
