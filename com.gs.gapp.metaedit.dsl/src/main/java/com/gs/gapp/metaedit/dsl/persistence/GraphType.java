package com.gs.gapp.metaedit.dsl.persistence;

import com.gs.gapp.dsl.IMetatype;

public enum GraphType implements IMetatype {

	PERSISTENCE_MODULE ("Persistence Module"),
	;
	
	private final String name;

	private GraphType(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}
}
