package com.gs.gapp.metaedit.dsl.basic;

import com.gs.gapp.dsl.IMetatype;

public enum GraphType implements IMetatype {

	BASIC_MODULE ("Basic Module"),
	;
	
	private final String name;

	private GraphType(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}
}
