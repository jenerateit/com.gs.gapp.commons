package com.gs.gapp.metaedit.dsl.basic;

import com.gs.gapp.dsl.IMetatype;

public enum ObjectType implements IMetatype {

	ENUMERATION ("Enumeration"),
	BASIC_TYPE ("BasicType"),
	BASIC_FIELD ("BasicField"),
	;
	
	private final String name;

	private ObjectType(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}
}
