package com.gs.gapp.metaedit.dsl.persistence;

import com.gs.gapp.dsl.IMetatype;

public enum ObjectType implements IMetatype {

	ENTITY ("Entity"),
	ENTITY_FIELD ("EntityField"),
	;
	
	private final String name;

	private ObjectType(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}
}
