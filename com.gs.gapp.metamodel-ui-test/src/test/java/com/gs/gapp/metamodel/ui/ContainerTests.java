package com.gs.gapp.metamodel.ui;

import org.junit.BeforeClass;
import org.junit.Test;

import com.gs.gapp.metamodel.ui.container.UIContainer;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

public class ContainerTests {
	
	private UIModule module;
	
    @Test
    public void testReuseOfUIContainers() {
    	for (UIContainer container : module.getNamespace().getContainers()) {
    		if (container instanceof UIDataContainer && container.getTechnicalName().equalsIgnoreCase("ReusedDataContainer")) {
    			@SuppressWarnings("unused")
				UIDataContainer dataContainer = (UIDataContainer) container;
    		}
    	}
    	
    }
  
    @BeforeClass
    public void beforeClass() {
	    module = new UIModule("myModule");
	  
	    Namespace uiNamespace = new Namespace("ui.namespace");
	    module.setNamespace(uiNamespace);
	    
        UIStructuralContainer tabbedContainer1 = new UIStructuralContainer("TabbedContainer1");
        uiNamespace.addContainer(tabbedContainer1);
	 
        UIStructuralContainer tabbedContainer2 = new UIStructuralContainer("TabbedContainer2");
        uiNamespace.addContainer(tabbedContainer2);
        
        UIDataContainer reusedDataContainer = new UIDataContainer("ReusedDataContainer");
        uiNamespace.addContainer(reusedDataContainer);
        tabbedContainer1.addChildContainer(reusedDataContainer);
        tabbedContainer2.addChildContainer(reusedDataContainer);
        
    }
}
