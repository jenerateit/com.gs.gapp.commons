package com.gs.gapp.metamodel.analytics;

import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;


/**
 *
 * @author mmt
 *
 */
public class GenerationGroupTargetConfiguration {
	private TransformationStepConfiguration generationGroupConfiguration;

	private final String generationGroupClassName;

	private final Class<?> metaModelClass;
	private final String qualifiedMetaModelClassName;
	private final String simpleMetaModelClassName;

	private final Class<? extends TargetI<? extends TargetDocumentI>> targetClass;
	private final String qualifiedTargetClassName;
	private final String simpleTargetClassName;
	
	private final Class<? extends WriterI> writerClass;
	private final String qualifiedWriterClassName ;
	private final String simpleWriterClassName ;


	/**
	 * @param metaModelClass
	 * @param targetClass
	 * @param writerClass 
	 * @param generationGroupClassName
	 */
	public GenerationGroupTargetConfiguration(Class<?> metaModelClass, Class<? extends TargetI<? extends TargetDocumentI>> targetClass,
			Class<? extends WriterI> writerClass, String generationGroupClassName) {
		super();
		this.generationGroupClassName = generationGroupClassName;
		this.metaModelClass = metaModelClass;
		this.qualifiedMetaModelClassName = metaModelClass == null ? null : metaModelClass.getName();
		this.simpleMetaModelClassName = metaModelClass == null ? null : metaModelClass.getSimpleName();

		this.targetClass = targetClass;
		this.qualifiedTargetClassName = targetClass.getName();
		this.simpleTargetClassName = targetClass.getSimpleName();
		
		this.writerClass = writerClass;
		this.qualifiedWriterClassName = writerClass.getName();
		this.simpleWriterClassName = writerClass.getSimpleName();
	}

	/**
	 * @return the qualifiedMetaModelClassName
	 */
	public String getQualifiedMetaModelClassName() {
		return qualifiedMetaModelClassName;
	}

	/**
	 * @return the simpleMetaModelClassName
	 */
	public String getSimpleMetaModelClassName() {
		return simpleMetaModelClassName;
	}

	/**
	 * @return the qualifiedTargetClassName
	 */
	public String getQualifiedTargetClassName() {
		return qualifiedTargetClassName;
	}

	/**
	 * @return the simpleTargetClassName
	 */
	public String getSimpleTargetClassName() {
		return simpleTargetClassName;
	}

	/**
	 * @return the metaModelClass
	 */
	public Class<?> getMetaModelClass() {
		return metaModelClass;
	}

	/**
	 * @return the targetClass
	 */
	public Class<? extends TargetI<? extends TargetDocumentI>> getTargetClass() {
		return targetClass;
	}
	
	
	
	public Class<? extends WriterI> getWriterClass() {
		return writerClass;
	}

	public String getQualifiedWriterClassName() {
		return qualifiedWriterClassName;
	}

	public String getSimpleWriterClassName() {
		return simpleWriterClassName;
	}

	/**
	 * @return
	 */
	public String getTargetId() {
//		return targetClass == null ? "" : targetClass.getSimpleName().replace("-", "_").replace("/", "_").replace(".", "_").replace("$", "_");
		return metaModelClass == null ? "" : metaModelClass.getSimpleName().replace("-", "_").replace("/", "_").replace(".", "_").replace("$", "_");
	}

	/**
	 * @return the generationGroupClassName
	 */
	public String getGenerationGroupClassName() {
		return generationGroupClassName;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return generationGroupClassName + ":" + simpleMetaModelClassName + " --> " + simpleTargetClassName;
	}

	public TransformationStepConfiguration getGenerationGroupConfiguration() {
		return generationGroupConfiguration;
	}

	public void setGenerationGroupConfiguration(
			TransformationStepConfiguration generationGroupConfiguration) {
		this.generationGroupConfiguration = generationGroupConfiguration;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((generationGroupClassName == null) ? 0
						: generationGroupClassName.hashCode());
		result = prime
				* result
				+ ((qualifiedMetaModelClassName == null) ? 0
						: qualifiedMetaModelClassName.hashCode());
		result = prime
				* result
				+ ((qualifiedTargetClassName == null) ? 0
						: qualifiedTargetClassName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GenerationGroupTargetConfiguration other = (GenerationGroupTargetConfiguration) obj;
		if (generationGroupClassName == null) {
			if (other.generationGroupClassName != null)
				return false;
		} else if (!generationGroupClassName
				.equals(other.generationGroupClassName))
			return false;
		if (qualifiedMetaModelClassName == null) {
			if (other.qualifiedMetaModelClassName != null)
				return false;
		} else if (!qualifiedMetaModelClassName
				.equals(other.qualifiedMetaModelClassName))
			return false;
		if (qualifiedTargetClassName == null) {
			if (other.qualifiedTargetClassName != null)
				return false;
		} else if (!qualifiedTargetClassName
				.equals(other.qualifiedTargetClassName))
			return false;
		return true;
	}
	


}
