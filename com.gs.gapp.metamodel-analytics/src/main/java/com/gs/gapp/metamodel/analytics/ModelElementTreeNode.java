/**
 *
 */
package com.gs.gapp.metamodel.analytics;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * The purpose of this class is to create a model element tree that
 * can be navigated in both directions: up and down. Such a model element
 * tree serves as the input for the creation of reports that show details
 * of the whole transformation-chain getting executed for a target project.
 *
 * @author mmt
 *
 */
public class ModelElementTreeNode extends ModelElement {

	private static final long serialVersionUID = -2615512899221834828L;

	private final ModelElementI modelElement;
	private ModelElementTreeNode parent;
	private final Set<ModelElementTreeNode> children = new LinkedHashSet<>();


	/**
	 *
	 */
	public ModelElementTreeNode(ModelElementI modelElement) {
		super(modelElement.getName());
		this.modelElement = modelElement;
	}

	/**
	 * @return the parent
	 */
	public final ModelElementTreeNode getParent() {
		return parent;
	}


	/**
	 * @return the children
	 */
	public final Set<ModelElementTreeNode> getChildren() {
		return children;
	}

	/**
	 *
	 * @return true if this tree node does not have a parent
	 */
	public final boolean isRoot() {
		return getParent() == null;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(ModelElementTreeNode parent) {
		if (parent == null) throw new NullPointerException("parameter 'parent' must not be null");
		if (parent == this || children.contains(parent)) throw new IllegalArgumentException("param 'parent' must point to one of its children");
		this.parent = parent;
		parent.children.add(this);
	}

	/**
	 * @return the modelElement
	 */
	public ModelElementI getModelElement() {
		return modelElement;
	}

	@Override
	public String getId() {
		if (getModelElement() != null) {
			return getModelElement().getId();
//			return getModelElement().getName().replace("-", "_").replace("/", "_").replace(".", "_") + "_" + getModelElement().getClass().getName().replace(".", "_").replace("$", "_");
		} else {
			return ""+this.hashCode();
		}
	}
}
