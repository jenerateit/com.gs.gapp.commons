package com.gs.gapp.metamodel.analytics;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.metamodel.basic.options.ConverterOptions;

public class AnyToAnalyticsConverterOptions extends ConverterOptions {
	
	public AnyToAnalyticsConverterOptions(ModelConverterOptions options) {
		super(options);
	}

}
