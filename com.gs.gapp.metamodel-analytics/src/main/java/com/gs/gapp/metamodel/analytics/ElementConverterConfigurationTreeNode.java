/**
 *
 */
package com.gs.gapp.metamodel.analytics;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * The purpose of this class is to create a a tree of model element converters.
 * This tree can be visualized in order to support the developer of model converters
 * in his daily work.
 *
 * @author mmt
 *
 */
public class ElementConverterConfigurationTreeNode extends ModelElement {

	private static final long serialVersionUID = -8204945488390597755L;

	private final TransformationStepConfiguration modelConverterConfiguration;
	private final ElementConverterConfiguration elementConverterConfiguration;
	private ElementConverterConfigurationTreeNode parent;
	private final Set<ElementConverterConfigurationTreeNode> children = new LinkedHashSet<>();

	/**
	 *
	 */
	public ElementConverterConfigurationTreeNode(ElementConverterConfiguration elementConverterConfiguration, TransformationStepConfiguration modelElementConfiguration) {
		this(elementConverterConfiguration == null ? null : elementConverterConfiguration.getElementConverterClassName(), elementConverterConfiguration, modelElementConfiguration);

	}

	public ElementConverterConfigurationTreeNode(String name, ElementConverterConfiguration elementConverterConfiguration, TransformationStepConfiguration transformationStepConfiguration) {
		super(name == null ? transformationStepConfiguration.getName() : name);
		this.elementConverterConfiguration = elementConverterConfiguration;
		this.modelConverterConfiguration = transformationStepConfiguration;
	}

	public boolean branchContainsElementConverterConfiguration(ElementConverterConfiguration otherElementConverterConfiguration) {
        boolean result = false;

        ElementConverterConfigurationTreeNode currentTreeNode = this;
        while (currentTreeNode != null) {
        	if (currentTreeNode.getElementConverterConfiguration() != null &&
        		currentTreeNode.getElementConverterConfiguration().equals(otherElementConverterConfiguration)) {
        		result = true;
        		break;
        	}
        	currentTreeNode = currentTreeNode.getParent();
        }

        return result;
	}

	/**
	 * @return the parent
	 */
	public final ElementConverterConfigurationTreeNode getParent() {
		return parent;
	}


	/**
	 * @return the children
	 */
	public final Set<ElementConverterConfigurationTreeNode> getChildren() {
		return Collections.unmodifiableSet(children);
	}

	/**
	 *
	 * @return true if this tree node does not have a parent
	 */
	public final boolean isRoot() {
		return getParent() == null;
	}
	
	public final boolean hasATargetAsLeaf(ElementConverterVisualizationContext context) {
		boolean result = traverseNode(this, context);
		
		return result;
	}
	
	/**
	 * @param node
	 * @param context
	 * 
	 */
	private boolean traverseNode(final ElementConverterConfigurationTreeNode node, ElementConverterVisualizationContext context) {
		if (context == null) {
			return false;
		}
		boolean foundATargetLeaf = false;
		if (node.getElementConverterConfiguration() != null && node.getElementConverterConfiguration().getModelConverterConfiguration() != null) {
			TransformationStepConfiguration transformationStepConfiguration = node.getElementConverterConfiguration().getModelConverterConfiguration();
			for (TransformationStepConfiguration childTransStepConfig : transformationStepConfiguration.getChildConfigurations()) {
				if (childTransStepConfig.getTargetConfigurations().size() > 0) {
					// the transformationStepConfiguration relates to a generation group that has targets configured
					for (GenerationGroupTargetConfiguration genGroupTargetConf : childTransStepConfig.getTargetConfigurations()) {
						if ((genGroupTargetConf.getTargetClass().getModifiers() & Modifier.ABSTRACT) != 0) {
							continue;  // ignore abstract target classes
						}
						if (node.getElementConverterConfiguration().getTargetClass() == genGroupTargetConf.getMetaModelClass()) {
							// ---------------------------------------------------------------------------------------------------
							// --- found a target class that might cause a file generation for the given transformation step tree
							ElementConverterConfigurationTreeNode rootNodeForBranch = null;
							Class<?> contextSourceClass = context.getSourceClass();
							if (contextSourceClass != null) {
								rootNodeForBranch = node.getLeadingNode(context);
							}
							
							if (rootNodeForBranch != null) {
								foundATargetLeaf = true;
								break;
							}
						}
					}
				}
				if (foundATargetLeaf) {
					break;
				}
			}
		}

		if (!foundATargetLeaf) {
			// --- continue traversal over children
			for (ElementConverterConfigurationTreeNode childTreeNode : node.getChildren()) {
				if (ignoreNode(childTreeNode)) {
					continue;
				}
				// ----------------------------------------------------------
				// --- here the recursive call is being made
				foundATargetLeaf = traverseNode(childTreeNode, context);
				if (foundATargetLeaf) {
					break;
				}
			}
		}
		
		return foundATargetLeaf;
	}
		
	/**
	 * @param node
	 * @return
	 */
	private boolean ignoreNode(ElementConverterConfigurationTreeNode node) {
	
		// --- ignore visualization for meta-classes that related to the visualization itself
		if (node.getElementConverterConfiguration() != null) {
			ElementConverterConfiguration elementConverterConfiguration = node.getElementConverterConfiguration();
			if (TransformationStepConfiguration.class.isAssignableFrom(elementConverterConfiguration.getSourceClass()) ||
					TransformationStepConfiguration.class.isAssignableFrom(elementConverterConfiguration.getTargetClass())) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(ElementConverterConfigurationTreeNode parent) {
		if (parent == null) throw new NullPointerException("parameter 'parent' must not be null");
		if (parent == this) throw new IllegalArgumentException("param 'parent' must not be its own parent");
		if (children.contains(parent)) throw new IllegalArgumentException("param 'parent' must not be a child of this element");

		if (parent.branchContainsElementConverterConfiguration(this.elementConverterConfiguration)) {
			throw new IllegalArgumentException("must not set a tree node as parent that has a element converter configuration in its branch that is the same as the on of this tree node");
		}

		this.parent = parent;
		boolean isNewAddition = this.parent.children.add(this);
		if (!isNewAddition) {}  // this might be used for testing
	}

	/**
	 * @return the elementConverterConfiguration
	 */
	public ElementConverterConfiguration getElementConverterConfiguration() {
		return elementConverterConfiguration;
	}

	/**
	 * Collect and return all ElementConverterConfigurationTreeNode children that belong to the same ModelConverterConfiguration
	 *
	 * @return
	 */
	public Set<ElementConverterConfigurationTreeNode> getAllScopedChildren() {
		Set<ElementConverterConfigurationTreeNode> result = new LinkedHashSet<>();
		collectAllScopedChildren(result, this.getModelConverterConfiguration());
		return result;
	}

	private void collectAllScopedChildren(Set<ElementConverterConfigurationTreeNode> result, TransformationStepConfiguration scope) {
		for (ElementConverterConfigurationTreeNode child : this.children) {
			if (child != this && scope.equals(child.getModelConverterConfiguration())) {
			    result.add(child);
				child.collectAllScopedChildren(result, scope);
			}
		}
	}

	/**
	 * @return
	 */
	public Set<ElementConverterConfigurationTreeNode> getLeaves() {
		return getLeaves(false);
	}

	/**
	 * @param scopedCollection
	 * @return
	 */
	public Set<ElementConverterConfigurationTreeNode> getLeaves(boolean scopedCollection) {
	    Set<ElementConverterConfigurationTreeNode> leaves = new LinkedHashSet<>();
	    Set<ElementConverterConfigurationTreeNode> processedNodes = new LinkedHashSet<>();
	    collectLeaves(leaves, processedNodes, 1, scopedCollection ? this.getModelConverterConfiguration() : null);
	    return leaves;
	}

	private void collectLeaves(Set<ElementConverterConfigurationTreeNode> leaves, Set<ElementConverterConfigurationTreeNode> processedNodes, int counter, TransformationStepConfiguration scope) {

		if (this.children.size() == 0) {
			leaves.add(this);
		} else {
			if (processedNodes.contains(this) == false) {  // in order to avoid infinite loop
				processedNodes.add(this);
				for (ElementConverterConfigurationTreeNode child : this.children) {
					if (child != this) {
						if (scope == null || scope.equals(child.getModelConverterConfiguration())) {
					        child.collectLeaves(leaves, processedNodes, counter++, scope);
						} else if (scope != null && scope.equals(child.getModelConverterConfiguration()) == false) {
							// in a scoped collection of leaves, a leaf element might have children
							leaves.add(this);
						}
					} else {
						// TODO this must not happen, but I added the if-else in order to avoid an infinite loop (mmt 06-Jun-2013)
						throw new RuntimeException("ERROR");
					}
				}
			}
		}
	}

	/**
	 * @return the modelConverterConfiguration
	 */
	public TransformationStepConfiguration getModelConverterConfiguration() {
		if (modelConverterConfiguration != null) {
		    return modelConverterConfiguration;
		} else if (elementConverterConfiguration != null) {
			return elementConverterConfiguration.getModelConverterConfiguration();
		} else {
			return null;
		}
	}

	public String getSourceId() {
		String sourceId = "no_source_id";
		if (getElementConverterConfiguration() != null) {
			sourceId = getElementConverterConfiguration().getQualifiedSourceClassName();
			if (getElementConverterConfiguration().getMetatypedConverter() != null && getElementConverterConfiguration().getMetatypedConverter().getMetatype() != null) {
				sourceId = sourceId + "_" + getElementConverterConfiguration().getMetatypedConverter().getMetatype().getName();
			}
		}
		return sourceId.replace("-", "_").replace("/", "_").replace(".", "_").replace("$", "_");
	}

	public String getSourceLabel() {
		String sourceLabel = null;
		if (getElementConverterConfiguration() != null) {
			if (getElementConverterConfiguration().getMetatypedConverter() != null && getElementConverterConfiguration().getMetatypedConverter().getMetatype() != null) {
				sourceLabel = getElementConverterConfiguration().getMetatypedConverter().getMetatype().getName();
			} else {
			    sourceLabel = getElementConverterConfiguration().getSimpleSourceClassName();
			}
		}
		return sourceLabel;
	}

	public String getTargetId() {
		String targetId = "no_target_id";
		if (getElementConverterConfiguration() != null) {
			targetId = getElementConverterConfiguration().getQualifiedTargetClassName();
		}
		return targetId.replace("-", "_").replace("/", "_").replace(".", "_").replace("$", "_");
	}

	public String getTargetLabel() {
		String targetLabel = null;
		if (getElementConverterConfiguration() != null) {
			targetLabel = getElementConverterConfiguration().getSimpleTargetClassName();
		}
		return targetLabel;
	}


	@Override
	public String getId() {
		StringBuilder id = new StringBuilder();

		if (getElementConverterConfiguration() != null) {
			id.append(getElementConverterConfiguration().getElementConverterClassName()).append("_");
		}

		id.append(getModelConverterConfiguration().getName());

        return id.toString().replace("-", "_").replace("/", "_").replace(".", "_").replace("$", "_");
	}

	public String getLabel() {
		StringBuilder label = new StringBuilder();

		if (getElementConverterConfiguration() != null) {
			label.append( getElementConverterConfiguration().getElementConverterClassName().substring(getElementConverterConfiguration().getElementConverterClassName().lastIndexOf(".")+1) );
			label.append( getElementConverterConfiguration().isIsResponsibleOverwritten() ? " *" : "" );
		} else {
			label.append("n.a.");
		}

//		{  // --- additionally show name of converter
//			label.append("\n");
//			label.append(getModelConverterConfiguration().getName().substring(getModelConverterConfiguration().getName().lastIndexOf(".")+1));
//		}

		return label.toString();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#toString()
	 */
	@Override
	public String toString() {
		return this.elementConverterConfiguration == null ? this.getName() : this.elementConverterConfiguration.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((elementConverterConfiguration == null) ? 0
						: elementConverterConfiguration.hashCode());
		result = prime
				* result
				+ ((modelConverterConfiguration == null) ? 0
						: modelConverterConfiguration.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ElementConverterConfigurationTreeNode other = (ElementConverterConfigurationTreeNode) obj;
		if (elementConverterConfiguration == null) {
			if (other.elementConverterConfiguration != null)
				return false;
		} else if (!elementConverterConfiguration
				.equals(other.elementConverterConfiguration))
			return false;
		if (modelConverterConfiguration == null) {
			if (other.modelConverterConfiguration != null)
				return false;
		} else if (!modelConverterConfiguration
				.equals(other.modelConverterConfiguration))
			return false;
		return true;
	}

	/**
	 * @return
	 */
	public boolean isLeaf() {
		return this.children.size() == 0;
	}


	/**
	 * Returns a list of nodes that makes up the branch from the root node up to this node.
	 * 
	 * @return
	 */
	public List<ElementConverterConfigurationTreeNode> getBranch() {
		List<ElementConverterConfigurationTreeNode> result = new ArrayList<>();
		
		ElementConverterConfigurationTreeNode node = this;
		while (node != null) {
			result.add(0, node);
			node = node.getParent();
		}
		
		return result;
	}
	
	/**
	 * @param node
	 * @return
	 */
	public boolean isInBranch(ElementConverterConfigurationTreeNode node) {
		for (ElementConverterConfigurationTreeNode aNodeOfTheBranch : this.getBranch()) {
			if (aNodeOfTheBranch == node) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * @param context
	 * @return
	 */
	public boolean isInBranch(ElementConverterVisualizationContext context) {
		for (ElementConverterConfigurationTreeNode aNodeOfTheBranch : this.getBranch()) {
			Class<?> sourceClass = context.getSourceClass();
			String classOrMetatypeName = context.getClassOrMetatypeName();
			if (aNodeOfTheBranch.getElementConverterConfiguration() != null && aNodeOfTheBranch.getElementConverterConfiguration().getSourceClass() == sourceClass) {
				if (aNodeOfTheBranch.getElementConverterConfiguration().getMetatypedConverter() == null) {
					return true;
				} else if (aNodeOfTheBranch.getElementConverterConfiguration().getMetatypedConverter().getMetatype() != null &&
						   aNodeOfTheBranch.getElementConverterConfiguration().getMetatypedConverter().getMetatype().getName().equals(classOrMetatypeName)) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * @param elementConverterVisualizationContext
	 * @return
	 */
	public ElementConverterConfigurationTreeNode getLeadingNode(ElementConverterVisualizationContext elementConverterVisualizationContext) {
		ElementConverterConfigurationTreeNode result = null;
		ElementConverterConfigurationTreeNode node = this;
		Class<?> sourceClass = elementConverterVisualizationContext.getSourceClass();
		while (node != null && node.getElementConverterConfiguration() != null) {
//			if (node.getElementConverterConfiguration().getSourceClass() == sourceClass) {
			if (node.getElementConverterConfiguration().getSourceClass().isAssignableFrom(sourceClass)) {
				if (node.getElementConverterConfiguration().getMetatypedConverter() != null && node.getElementConverterConfiguration().getMetatypedConverter().getMetatype() != null) {
					if (node.getElementConverterConfiguration().getMetatypedConverter().getMetatype().getName().equalsIgnoreCase(elementConverterVisualizationContext.getClassOrMetatypeName())) {
						result = node;
						break;
					}
				} else {
					result = node;
					break;
				}
			}
			node = node.getParent();
		}
		
		if (result != null && result.getModelConverterConfiguration().getParentConfiguration() != null && result.getElementConverterConfiguration().getSourceClass() != Model.class) {
            // reseting the result to null since we do not want to visualize element converters that start in the middle of nowhere (exception: source class Model, since this is intrinsic) 
			result = null;
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public ElementConverterConfigurationTreeNode getRootNode() {
		ElementConverterConfigurationTreeNode result = null;
		
		result = this;
		while (result.getParent() != null) {
			result = result.getParent();
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public int getLevel() {
		int level = 0;
		
		ElementConverterConfigurationTreeNode node = this;
		while (node != null) {
			level++;
			node = node.getParent();
		}
		
		return level;
	}
	
	/**
	 * @param sourceClass
	 * @return
	 */
	public boolean featuresSourceClass(Class<?> sourceClass) {
		return this.elementConverterConfiguration != null &&
				this.elementConverterConfiguration.getSourceClass() != null &&
				this.elementConverterConfiguration.getSourceClass().isAssignableFrom(sourceClass);
//		this.elementConverterConfiguration.getSourceClass() == sourceClass;
	}
}
