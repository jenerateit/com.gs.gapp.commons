package com.gs.gapp.metamodel.analytics;

import java.io.Serializable;

import com.gs.gapp.dsl.IMetatyped;

/**
 * 
 *
 * @author mmt
 *
 */
public class ElementConverterConfiguration implements Serializable {

	private static final long serialVersionUID = -7404323434867638639L;

	private TransformationStepConfiguration modelConverterConfiguration;

	private final String elementConverterClassName;
	private final String qualifiedElementConverterClassName;

	private final Class<?> sourceClass;
	private final String qualifiedSourceClassName;
	private final String simpleSourceClassName;

	private final Class<?> targetClass;
	private final String qualifiedTargetClassName;
	private final String simpleTargetClassName;

	private final boolean isIsResponsibleOverwritten;

    private final IMetatyped metatypedConverter;

    /**
     * @param sourceClass
     * @param targetClass
     * @param elementConverterClassName
     * @param qualifiedElementConverterClassName
     * @param isIsResponsibleOverwritten
     */
    public ElementConverterConfiguration(Class<?> sourceClass, Class<?> targetClass, String elementConverterClassName, String qualifiedElementConverterClassName, boolean isIsResponsibleOverwritten) {
    	this(sourceClass, targetClass, elementConverterClassName, qualifiedElementConverterClassName, isIsResponsibleOverwritten, null);
    }
    
	/**
	 * @param sourceClass
	 * @param targetClass
	 * @param elementConverterClassName
	 * @param qualifiedElementConverterClassName
	 * @param isIsResponsibleOverwritten
	 * @param metatypedConverter
	 */
	public ElementConverterConfiguration(Class<?> sourceClass, Class<?> targetClass, String elementConverterClassName, String qualifiedElementConverterClassName, boolean isIsResponsibleOverwritten, IMetatyped metatypedConverter) {
		super();
		this.elementConverterClassName = elementConverterClassName;
		this.qualifiedElementConverterClassName = qualifiedElementConverterClassName;
		this.sourceClass = sourceClass;
		this.qualifiedSourceClassName = sourceClass == null ? null : sourceClass.getName();
		this.simpleSourceClassName = sourceClass == null ? null : sourceClass.getSimpleName();

		this.targetClass = targetClass;
		this.qualifiedTargetClassName = targetClass.getName();
		this.simpleTargetClassName = targetClass.getSimpleName();

		this.isIsResponsibleOverwritten = isIsResponsibleOverwritten;
		
		this.metatypedConverter = metatypedConverter;
	}

	/**
	 * @return the qualifiedSourceClassName
	 */
	public String getQualifiedSourceClassName() {
		return qualifiedSourceClassName;
	}

	/**
	 * @return the simpleSourceClassName
	 */
	public String getSimpleSourceClassName() {
		return simpleSourceClassName;
	}

	/**
	 * @return the qualifiedTargetClassName
	 */
	public String getQualifiedTargetClassName() {
		return qualifiedTargetClassName;
	}

	/**
	 * @return the simpleTargetClassName
	 */
	public String getSimpleTargetClassName() {
		return simpleTargetClassName;
	}


	/**
	 * @return the isIsResponsibleOverwritten
	 */
	public boolean isIsResponsibleOverwritten() {
		return isIsResponsibleOverwritten;
	}

	/**
	 * @return the sourceClass
	 */
	public Class<?> getSourceClass() {
		return sourceClass;
	}

	/**
	 * @return the targetClass
	 */
	public Class<?> getTargetClass() {
		return targetClass;
	}

	/**
	 * @return the modelConverterConfiguration
	 */
	public TransformationStepConfiguration getModelConverterConfiguration() {
		return modelConverterConfiguration;
	}

	/**
	 * @param modelConverter the modelConverterConfiguration to set
	 */
	protected void setModelConverterConfiguration(TransformationStepConfiguration modelConverter) {
		this.modelConverterConfiguration = modelConverter;
	}

	/**
	 * @return the elementConverterClassName
	 */
	public String getElementConverterClassName() {
		return elementConverterClassName;
	}

	/**
	 * @param otherElementConverterConfiguration
	 * @return
	 */
	public boolean canBeFollowedBy(ElementConverterConfiguration otherElementConverterConfiguration) {
		boolean result = false;
		Class<?> firstClass = this.getTargetClass();
		Class<?> secondClass = otherElementConverterConfiguration.getSourceClass();


		if (secondClass.isAssignableFrom(firstClass)) {
	    // --- if isAssignableFrom() is not being used, too few vertexes would be shown in the generation analytics diagram (mmt 14-Nov-2018)
//	    if (secondClass == firstClass) {
			result = true;
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return simpleSourceClassName + " --> " + elementConverterClassName + " --> " + simpleTargetClassName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((elementConverterClassName == null) ? 0
						: elementConverterClassName.hashCode());
		result = prime * result
				+ ((modelConverterConfiguration == null) ? 0 : modelConverterConfiguration.hashCode());
		result = prime
				* result
				+ ((qualifiedSourceClassName == null) ? 0
						: qualifiedSourceClassName.hashCode());
		result = prime
				* result
				+ ((qualifiedTargetClassName == null) ? 0
						: qualifiedTargetClassName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ElementConverterConfiguration other = (ElementConverterConfiguration) obj;
		if (elementConverterClassName == null) {
			if (other.elementConverterClassName != null)
				return false;
		} else if (!elementConverterClassName
				.equals(other.elementConverterClassName))
			return false;
		if (modelConverterConfiguration == null) {
			if (other.modelConverterConfiguration != null)
				return false;
		} else if (!modelConverterConfiguration.equals(other.modelConverterConfiguration))
			return false;
		if (qualifiedSourceClassName == null) {
			if (other.qualifiedSourceClassName != null)
				return false;
		} else if (!qualifiedSourceClassName
				.equals(other.qualifiedSourceClassName))
			return false;
		if (qualifiedTargetClassName == null) {
			if (other.qualifiedTargetClassName != null)
				return false;
		} else if (!qualifiedTargetClassName
				.equals(other.qualifiedTargetClassName))
			return false;
		return true;
	}

	public IMetatyped getMetatypedConverter() {
		return metatypedConverter;
	}

	public String getQualifiedElementConverterClassName() {
		return qualifiedElementConverterClassName;
	}


}
