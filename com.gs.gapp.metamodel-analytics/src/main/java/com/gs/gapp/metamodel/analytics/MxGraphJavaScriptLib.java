package com.gs.gapp.metamodel.analytics;

import com.gs.gapp.metamodel.anyfile.AnyFile;

public class MxGraphJavaScriptLib extends AnyFile {

	private static final long serialVersionUID = -434502556627342011L;

	public MxGraphJavaScriptLib(String name, String path, String extension) {
		super(name, path, extension);
	}
}
