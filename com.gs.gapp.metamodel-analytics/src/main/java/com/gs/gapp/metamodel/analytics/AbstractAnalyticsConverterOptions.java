package com.gs.gapp.metamodel.analytics;

import java.io.Serializable;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.metamodel.basic.options.ConverterOptions;

public class AbstractAnalyticsConverterOptions extends ConverterOptions {
	
	public static final String OPTION_ANALYTICS = "provide-analytics";
	
	public AbstractAnalyticsConverterOptions(ModelConverterOptions options) {
		super(options);
	}

	/**
	 * @return
	 */
	public boolean generateAnalytics() {
		Serializable provideAnalytics =	getOptions().get(OPTION_ANALYTICS);
		
		if (provideAnalytics != null) {
			validateBooleanOption(provideAnalytics.toString(), OPTION_ANALYTICS);
			return Boolean.parseBoolean(provideAnalytics.toString());
		}
		
		return false;
	}
}
