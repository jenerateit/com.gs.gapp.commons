package com.gs.gapp.metamodel.analytics;

import java.io.Serializable;

public class ElementConverterVisualizationContext implements Serializable {

	private static final long serialVersionUID = 898359719113162551L;
	
	private final String classOrMetatypeName;
	private final Class<?> sourceClass;
	
	public ElementConverterVisualizationContext(String classOrMetatypeName, Class<?> sourceClass) {
		super();
		this.classOrMetatypeName = classOrMetatypeName;
		this.sourceClass = sourceClass;
	}

	public String getClassOrMetatypeName() {
		return classOrMetatypeName;
	}

	public Class<?> getSourceClass() {
		return sourceClass;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classOrMetatypeName == null) ? 0 : classOrMetatypeName.hashCode());
		result = prime * result + ((sourceClass == null) ? 0 : sourceClass.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ElementConverterVisualizationContext other = (ElementConverterVisualizationContext) obj;
		if (classOrMetatypeName == null) {
			if (other.classOrMetatypeName != null)
				return false;
		} else if (!classOrMetatypeName.equals(other.classOrMetatypeName))
			return false;
		if (sourceClass == null) {
			if (other.sourceClass != null)
				return false;
		} else if (!sourceClass.equals(other.sourceClass))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ElementConverterVisualizationContext [classOrMetatypeName=" + classOrMetatypeName + ", sourceClass="
				+ sourceClass + "]";
	}
}
