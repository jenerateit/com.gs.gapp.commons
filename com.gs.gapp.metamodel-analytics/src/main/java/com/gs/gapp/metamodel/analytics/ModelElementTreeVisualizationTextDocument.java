package com.gs.gapp.metamodel.analytics;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * @author mmt
 *
 */
public class ModelElementTreeVisualizationTextDocument extends ModelElement {

	private static final long serialVersionUID = -7687202121605617706L;

	public ModelElementTreeVisualizationTextDocument(String name) {
		super(name);
	}
}
