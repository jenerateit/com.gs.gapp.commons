package com.gs.gapp.metamodel.analytics;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.options.ConverterOptions;
import com.gs.gapp.metamodel.basic.options.GenerationGroupOptions;

/**
 * The purpose of this meta-type is to transfer internal converter configuration
 * from one converter to the next and finally to the generation groups. There
 * a special target class is being used to generate the converter configuration as a whole into a file.
 *
 * @author mmt
 *
 */
public class TransformationStepConfiguration extends ModelElement {

	private static final long serialVersionUID = 2565858336497224714L;

	private final Set<ElementConverterConfiguration> elementConverterConfigurations =
			new LinkedHashSet<>();

	private TransformationStepConfiguration parentConfiguration;
	
	private final GenerationGroupOptions generationGroupOptions;
	
	private final ConverterOptions converterOptions;

	private final ModelConverterOptions modelConverterOptions;

	private final Set<TransformationStepConfiguration> childConfigurations = new LinkedHashSet<>();

	private final Set<ElementConverterConfigurationTreeNode> treeNodes = new LinkedHashSet<>();
	
	private final Set<GenerationGroupTargetConfiguration> targetConfigurations =
			new LinkedHashSet<>();
	
	/**
	 * @param name
	 * @param modelConverterOptions
	 */
	public TransformationStepConfiguration(String name, ModelConverterOptions modelConverterOptions) {
		super(name);
		this.modelConverterOptions = modelConverterOptions;
		this.generationGroupOptions = null;
		this.converterOptions = null;
	}
	
	/**
	 * @param name
	 * @param generationGroupOptions
	 */
	public TransformationStepConfiguration(String name, GenerationGroupOptions generationGroupOptions) {
		super(name);
		this.modelConverterOptions = null;
		this.generationGroupOptions = generationGroupOptions;
		this.converterOptions = null;
	}

	/**
	 * @param name
	 * @param converterOptions
	 */
	public TransformationStepConfiguration(String name, ConverterOptions converterOptions) {
		super(name);
		this.modelConverterOptions = null;
		this.generationGroupOptions = null;
		this.converterOptions = converterOptions;
	}
	
	/**
	 * @return the elementConverterConfigurations
	 */
	public Set<ElementConverterConfiguration> getElementConverterConfigurations() {
		return Collections.unmodifiableSet(elementConverterConfigurations);
	}

	/**
	 * @param elementConverterConfiguration
	 * @return
	 */
	public boolean addElementConverterConfiguration(ElementConverterConfiguration elementConverterConfiguration) {
		boolean result = this.elementConverterConfigurations.add(elementConverterConfiguration);
		elementConverterConfiguration.setModelConverterConfiguration(this);
		return result;
	}

	/**
	 * @return the parentConfiguration
	 */
	public TransformationStepConfiguration getParentConfiguration() {
		return parentConfiguration;
	}

	/**
	 * @param parentConfiguration the parentConfiguration to set
	 */
	public void setParentConfiguration(TransformationStepConfiguration parentConfiguration) {
		this.parentConfiguration = parentConfiguration;
		parentConfiguration.childConfigurations.add(this);
	}

	/**
	 * @return the childConfigurations
	 */
	public Set<TransformationStepConfiguration> getChildConfigurations() {
		return Collections.unmodifiableSet(childConfigurations);
	}

	/**
	 * @return
	 */
	public TransformationStepConfiguration getRootModelConverterConfiguration() {
		TransformationStepConfiguration root = this;

		while (root.getParentConfiguration() != null) {
			root = root.getParentConfiguration();
		}

		return root;
	}
	
	/**
	 * @return the treeNodes
	 */
	public Set<ElementConverterConfigurationTreeNode> getTreeNodes() {
		return Collections.unmodifiableSet(treeNodes);
	}

	/**
	 * @param treeNode
	 */
	public boolean addTreeNode(ElementConverterConfigurationTreeNode treeNode) {
		return treeNodes.add(treeNode);
	}

	/**
	 * @param treeNodes
	 */
	public void addTreeNodes(Collection<ElementConverterConfigurationTreeNode> treeNodes) {
		this.treeNodes.addAll(treeNodes);
	}

	public GenerationGroupOptions getGenerationGroupOptions() {
		return generationGroupOptions;
	}

	public ConverterOptions getConverterOptions() {
		return converterOptions;
	}

	public Set<GenerationGroupTargetConfiguration> getTargetConfigurations() {
		return targetConfigurations;
	}
	
	/**
	 * @param targetConfiguration
	 * @return
	 */
	public boolean addTargetConfiguration(GenerationGroupTargetConfiguration targetConfiguration) {
		return this.targetConfigurations.add(targetConfiguration);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime
				* result
				+ ((modelConverterOptions == null) ? 0 : modelConverterOptions
						.hashCode());
		result = prime
				* result
				+ ((parentConfiguration == null) ? 0 : parentConfiguration
						.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TransformationStepConfiguration other = (TransformationStepConfiguration) obj;
		if (modelConverterOptions == null) {
			if (other.modelConverterOptions != null)
				return false;
		} else if (!modelConverterOptions.equals(other.modelConverterOptions))
			return false;
		if (parentConfiguration == null) {
			if (other.parentConfiguration != null)
				return false;
		} else if (!parentConfiguration.equals(other.parentConfiguration))
			return false;
		return true;
	}

	
}
