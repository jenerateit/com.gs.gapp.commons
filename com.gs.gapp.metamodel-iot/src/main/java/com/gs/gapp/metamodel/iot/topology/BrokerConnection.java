/**
 * 
 */
package com.gs.gapp.metamodel.iot.topology;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;

import com.gs.gapp.metamodel.iot.AbstractIotModelElement;
import com.gs.gapp.metamodel.iot.Application;


/**
 * @author mmt
 *
 */
public class BrokerConnection extends AbstractIotModelElement {

	private static final long serialVersionUID = 8916876091288039170L;

	private final Broker broker;
	
	private final Application application;
	
	private final Set<Publication> publications = new LinkedHashSet<>();
	private final Set<Subscription> subscriptions = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public BrokerConnection(String name, Application application, Broker broker) {
		super(name);
		this.application = application;
		this.broker = broker;
	}

	@NotNull
	public Broker getBroker() {
		return broker;
	}

	public Set<Publication> getPublications() {
		return publications;
	}
	
	public boolean addPublication(Publication publication) {
		return this.publications.add(publication);
	}

	public Set<Subscription> getSubscriptions() {
		return subscriptions;
	}
	
	public boolean addSubscription(Subscription subscription) {
		return this.subscriptions.add(subscription);
	}

	public Application getApplication() {
		return application;
	}
}
