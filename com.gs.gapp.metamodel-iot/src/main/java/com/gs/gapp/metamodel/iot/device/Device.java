/**
 * 
 */
package com.gs.gapp.metamodel.iot.device;

import com.gs.gapp.metamodel.iot.Application;

/**
 * @author mmt
 *
 */
public class Device extends Functionblock {

	private static final long serialVersionUID = 4258446698042977767L;

	private Device implementation;
	
	private Application application;
	
	/**
	 * @param name
	 */
	public Device(String name, Functionblock owner) {
		super(name, owner);
	}

	/**
	 * @param name
	 */
	public Device(String name) {
		super(name);
	}

	@Override
	public Device getImplementation() {
		return implementation;
	}

	public void setImplementation(Device implementation) {
		this.implementation = implementation;
	}

	@Override
	public Application getApplication() {
		return application;
	}

	public void setApplication(Application application) {
		this.application = application;
	}
}
