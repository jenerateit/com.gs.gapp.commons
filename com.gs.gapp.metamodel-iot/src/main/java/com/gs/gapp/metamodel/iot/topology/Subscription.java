/**
 * 
 */
package com.gs.gapp.metamodel.iot.topology;



/**
 * @author mmt
 *
 */
public class Subscription extends AbstractPubSub {

	private static final long serialVersionUID = -7989281600599302669L;
	
	/**
	 * @param name
	 */
	public Subscription(String name) {
		super(name);
	}
}
