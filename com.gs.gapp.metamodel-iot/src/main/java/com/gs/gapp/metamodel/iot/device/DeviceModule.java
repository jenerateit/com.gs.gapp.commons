/**
 *
 */
package com.gs.gapp.metamodel.iot.device;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.iot.Namespace;

/**
 * @author mmt
 *
 */
public class DeviceModule extends Module {

	private static final long serialVersionUID = 2898208776049989583L;

	private final Set<Device> devices = new LinkedHashSet<>();
	
	private final Set<Functionblock> functionblocks = new LinkedHashSet<>();
	
	private final Set<Sensor> sensors = new LinkedHashSet<>();
	
	private final Set<Actuator> actuators = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public DeviceModule(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#getNamespace()
	 */
	@Override
	public Namespace getNamespace() {
		return (Namespace) super.getNamespace();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#setNamespace(com.gs.gapp.metamodel.basic.Namespace)
	 */
	@Override
	public void setNamespace(com.gs.gapp.metamodel.basic.Namespace namespace) {
		if (!(namespace instanceof Namespace)) {
			throw new IllegalArgumentException("the namespace of a DeviceModule must be an instance of com.gs.gapp.metamodel.iot.Namespace");
		}
		super.setNamespace(namespace);
	}

	public Set<Device> getDevices() {
		return devices;
	}
	
	public boolean addDevice(Device device) {
		boolean result = this.devices.add(device);
		this.addElement(device);
		device.setModule(this);
		return result;
	}

	public Set<Sensor> getSensors() {
		return sensors;
	}
	
	public boolean addSensor(Sensor sensor) {
		boolean result = this.sensors.add(sensor);
		this.addElement(sensor);
		sensor.setModule(this);
		return result;
	}

	public Set<Actuator> getActuators() {
		return actuators;
	}
	
	public boolean addActuator(Actuator actuator) {
		boolean result = this.actuators.add(actuator);
		this.addElement(actuator);
		actuator.setModule(this);
		return result;
	}

	public Set<Functionblock> getFunctionblocks() {
		return functionblocks;
	}
	
	public boolean addFunctionblock(Functionblock functionblock) {
		boolean result = this.functionblocks.add(functionblock);
		this.addElement(functionblock);
		functionblock.setModule(this);
		return result;
	}
}
