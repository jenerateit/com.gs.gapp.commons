/**
 * 
 */
package com.gs.gapp.metamodel.iot;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * @author mmt
 *
 */
public abstract class AbstractIotModelElement extends ModelElement {

	private static final long serialVersionUID = -2973142159468241232L;
	
	/**
	 * @param name
	 */
	public AbstractIotModelElement(String name) {
		super(name);
	}
}
