/**
 * 
 */
package com.gs.gapp.metamodel.iot;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author mmt
 *
 */
public class Measure extends AbstractIotModelElement {

	private static final long serialVersionUID = 1656275280213516820L;
	
	private final Set<Unit> units = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public Measure(String name) {
		super(name);
	}

	public Set<Unit> getUnits() {
		return units;
	}

	public boolean addUnit(Unit unit) {
		return this.units.add(unit);
	}
}
