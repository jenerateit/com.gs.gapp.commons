/**
 *
 */
package com.gs.gapp.metamodel.iot.businesslogic;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.iot.Application;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

/**
 * TODO implement the storage functionality (storing data on a gateway)
 * 
 * @author mmt
 *
 */
public class Storage extends ModelElement {

	private static final long serialVersionUID = 1533762616203653476L;

	private Set<PersistenceModule> persistenceModules = new LinkedHashSet<>();

	private final Set<Application> applications = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public Storage(String name) {
		super(name);
	}

	/**
	 * @return the persistenceModules
	 */
	public Set<PersistenceModule> getPersistenceModules() {
		return persistenceModules;
	}

	/**
	 * @param persistenceModule
	 * @return
	 */
	public boolean addPersistenceModule(PersistenceModule persistenceModule) {
		return this.persistenceModules.add(persistenceModule);
	}

	/**
	 * @return the applications
	 */
	public Set<Application> getApplications() {
		return Collections.unmodifiableSet(applications);
	}

	/**
	 * @param application
	 * @return
	 */
	public boolean addApplication(Application application) {
		return this.applications.add(application);
	}
}
