/**
 * 
 */
package com.gs.gapp.metamodel.iot;


/**
 * @author mmt
 *
 */
public class Producer extends AbstractIotModelElement {

	private static final long serialVersionUID = -6196479524177864193L;
	
	private String organisationName;
	private String homepage;

	/**
	 * @param name
	 */
	public Producer(String name) {
		super(name);
	}

	public String getHomepage() {
		return homepage;
	}

	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}

	public String getOrganisationName() {
		return organisationName;
	}

	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

}
