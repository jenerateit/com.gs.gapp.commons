/**
 * 
 */
package com.gs.gapp.metamodel.iot.topology;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.iot.AbstractIotModelElement;
import com.gs.gapp.metamodel.iot.Topic;
import com.gs.gapp.metamodel.iot.device.ActuatorUsage;
import com.gs.gapp.metamodel.iot.device.SensorUsage;

/**
 * Each publication/subscription can handle sensors and actuators. Those sensors and
 * actuators are then used to derive appropriate topics. Moreover, additional, explicit
 * topics can be added in order to have a means to express calculated or derived topics/messages
 * in a model.
 * 
 * @author mmt
 *
 */
public abstract class AbstractPubSub extends AbstractIotModelElement {

	private static final long serialVersionUID = -7989281600599302669L;
	
	private final Set<SensorUsage> sensorUsages = new LinkedHashSet<>();
	
	private final Set<ActuatorUsage> actuatorUsages = new LinkedHashSet<>();
	
	private final Set<Topic> topics = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public AbstractPubSub(String name) {
		super(name);
	}

	public Set<SensorUsage> getSensorUsages() {
		return sensorUsages;
	}
	
	public boolean addSensorUsage(SensorUsage sensorUsage) {
		return this.sensorUsages.add(sensorUsage);
	}

	public Set<ActuatorUsage> getActuatorUsages() {
		return actuatorUsages;
	}
	
	public boolean addActuatorUsage(ActuatorUsage actuatorUsage) {
		return this.actuatorUsages.add(actuatorUsage);
	}

	public Set<Topic> getTopics() {
		return topics;
	}
	
	public boolean addTopic(Topic topic) {
		return this.topics.add(topic);
	}
}
