/**
 * 
 */
package com.gs.gapp.metamodel.iot.topology;



/**
 * @author mmt
 *
 */
public class Publication extends AbstractPubSub {

	private static final long serialVersionUID = -7989281600599302669L;
	
	/**
	 * @param name
	 */
	public Publication(String name) {
		super(name);
	}
}
