/**
 *
 */
package com.gs.gapp.metamodel.iot.businesslogic;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.iot.AbstractIotModelElement;
import com.gs.gapp.metamodel.iot.Application;
import com.gs.gapp.metamodel.iot.Topic;
import com.gs.gapp.metamodel.iot.device.ActuatorUsage;
import com.gs.gapp.metamodel.iot.device.SensorUsage;

/**
 * @author mmt
 *
 */
public class BusinessLogic extends AbstractIotModelElement {

	/**
	 *
	 */
	private static final long serialVersionUID = 4813008250796758824L;

	private Function function;
	
	private final Application owner;
	
	private final Set<Topic> triggerTopics = new LinkedHashSet<>();
	private final Set<SensorUsage> triggerSensorUsages = new LinkedHashSet<>();
	private final Set<ActuatorUsage> triggerActuatorUsages = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public BusinessLogic(String name, Application owner) {
		super(name);
		this.owner = owner;
		this.owner.addBusinessLogic(this);
	}

	public Function getFunction() {
		return function;
	}

	public Set<Topic> getTriggerTopics() {
		return triggerTopics;
	}
	
	public boolean addTriggerTopic(Topic topic) {
		return this.triggerTopics.add(topic);
	}
	
	public Set<SensorUsage> getTriggerSensorUsages() {
		return triggerSensorUsages;
	}
	
	public boolean isTriggeredBy(SensorUsage sensorUsage) {
		boolean result = triggerSensorUsages.contains(sensorUsage);
		return result ? result : triggerActuatorUsages.contains(sensorUsage);
	}
	
	public boolean isTriggeredBy(Topic topic) {
		return triggerTopics.contains(topic);
	}
	
	public boolean addTriggerSensorUsage(SensorUsage sensorUsage) {
		return this.triggerSensorUsages.add(sensorUsage);
	}

	public Set<ActuatorUsage> getTriggerActuatorUsages() {
		return triggerActuatorUsages;
	}
	
	public boolean addTriggerActuatorUsage(ActuatorUsage actuatorUsage) {
		return this.triggerActuatorUsages.add(actuatorUsage);
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public Application getOwner() {
		return owner;
	}
}
