/**
 * 
 */
package com.gs.gapp.metamodel.iot;


/**
 * @author mmt
 *
 */
public abstract class Node extends AbstractIotModelElement {

	private static final long serialVersionUID = -2973142159468241232L;
	
	private Producer producer;

	/**
	 * @param name
	 */
	public Node(String name) {
		super(name);
	}

	public Producer getProducer() {
		return producer;
	}

	public void setProducer(Producer producer) {
		this.producer = producer;
	}
}
