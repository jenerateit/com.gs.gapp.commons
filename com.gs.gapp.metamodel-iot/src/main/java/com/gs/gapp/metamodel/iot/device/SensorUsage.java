/**
 * 
 */
package com.gs.gapp.metamodel.iot.device;

/**
 * A sensor can point to the effective sensor being used. In this case, the original sensor name is not the
 * technical name but a name that takes semantics into account, e.g.:
 * "Temperature Sensor ABC123" vs. "Sensor to measure Soil Temperature in a Greenhouse"
 * 
 * @author mmt
 *
 */
public class SensorUsage extends HardwareUsage {

	private static final long serialVersionUID = -2656643219652976097L;

	private Sensor implementation;
	
	private Long samplingInterval;
	private Integer historyLength;
	
	/**
	 * @param name
	 */
	public SensorUsage(String name, Functionblock owner) {
		super(name, owner);
		owner.addSensorUsage(this);
	}

	/**
	 * @return
	 */
	@Override
	public Sensor getImplementation() {
		return implementation;
	}

	public void setImplementation(Sensor implementation) {
		this.implementation = implementation;
		this.implementation.addSensorUsage(this);
        super.setImplementation(implementation);
	}

	public Long getSamplingInterval() {
		return samplingInterval;
	}

	public void setSamplingInterval(Long samplingInterval) {
		this.samplingInterval = samplingInterval;
	}

	public Integer getHistoryLength() {
		return historyLength;
	}

	public void setHistoryLength(Integer historyLength) {
		this.historyLength = historyLength;
	}
}
