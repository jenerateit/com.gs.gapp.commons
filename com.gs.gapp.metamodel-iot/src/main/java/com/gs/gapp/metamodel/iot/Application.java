/**
 * 
 */
package com.gs.gapp.metamodel.iot;

import java.util.LinkedHashSet;
import java.util.Set;

import org.osgi.framework.Version;
import org.osgi.framework.VersionRange;

import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.iot.businesslogic.BusinessLogic;
import com.gs.gapp.metamodel.iot.device.ActuatorUsage;
import com.gs.gapp.metamodel.iot.device.Device;
import com.gs.gapp.metamodel.iot.device.SensorUsage;
import com.gs.gapp.metamodel.iot.topology.BrokerConnection;
import com.gs.gapp.metamodel.iot.topology.Publication;
import com.gs.gapp.metamodel.iot.topology.Subscription;


/**
 * An application can be a web application, an android app, a gateway application or any other
 * application, even a simulation program could be represented by a modeled application.
 * As far as the model is concerned, any application can control devices and their sensors and actuators.
 * However, most of the time it will be only gateway applications that make use of this.
 * 
 * @author mmt
 *
 */
public class Application extends Node {

	private static final long serialVersionUID = -2973142159468241232L;
	
	private final Set<Device> devices = new LinkedHashSet<>();
	private final Set<BrokerConnection> brokerConnections = new LinkedHashSet<>();
	
	private Application implementation;
	
	private ApplicationTypeEnum applicationType;
	
	private final Set<FunctionModule> functionModules = new LinkedHashSet<>();
	
	private final Set<BusinessLogic> businessLogic = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public Application(String name) {
		super(name);
	}
	
	public Set<BrokerConnection> getBrokerConnections() {
		return brokerConnections;
	}
	
	public boolean addBrokerConnection(BrokerConnection brokerConnection) {
		return this.brokerConnections.add(brokerConnection);
	}

	public Set<Device> getDevices() {
		return devices;
	}
	
	public boolean addDevice(Device device) {
		return this.devices.add(device);
	}

	public Application getImplementation() {
		return implementation;
	}

	public void setImplementation(Application implementation) {
		this.implementation = implementation;
	}

	public ApplicationTypeEnum getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(ApplicationTypeEnum applicationType) {
		this.applicationType = applicationType;
	}
	
	/**
	 * @return
	 */
	public Set<SensorUsage> getAllPublishedSensorUsages() {
		Set<SensorUsage> result = new LinkedHashSet<>();
		
		for (BrokerConnection brokerConnection : brokerConnections) {
			for (Publication pub : brokerConnection.getPublications()) {
				for (SensorUsage sensorUsage : pub.getSensorUsages()) {
					result.add(sensorUsage);
				}
			}
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<SensorUsage> getAllSubscribedSensorUsages() {
		Set<SensorUsage> result = new LinkedHashSet<>();
		
		for (BrokerConnection brokerConnection : brokerConnections) {
			for (Subscription sub : brokerConnection.getSubscriptions()) {
				for (SensorUsage sensorUsage : sub.getSensorUsages()) {
					result.add(sensorUsage);
				}
			}
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<ActuatorUsage> getAllPublishedActuatorUsages() {
		Set<ActuatorUsage> result = new LinkedHashSet<>();
		
		for (BrokerConnection brokerConnection : brokerConnections) {
			for (Publication pub : brokerConnection.getPublications()) {
				for (ActuatorUsage actuatorUsage : pub.getActuatorUsages()) {
					result.add(actuatorUsage);
				}
			}
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<ActuatorUsage> getAllSubscribedActuatorUsages() {
		Set<ActuatorUsage> result = new LinkedHashSet<>();
		
		for (BrokerConnection brokerConnection : brokerConnections) {
			for (Subscription sub : brokerConnection.getSubscriptions()) {
				for (ActuatorUsage actuatorUsage : sub.getActuatorUsages()) {
					result.add(actuatorUsage);
				}
			}
		}
		
		return result;
	}

	public Set<FunctionModule> getFunctionModules() {
		return functionModules;
	}
	
	public boolean addFunctionModule(FunctionModule functionModule) {
		return this.functionModules.add(functionModule);
	}

	public Set<BusinessLogic> getBusinessLogic() {
		return businessLogic;
	}
	
	/**
	 * @return
	 */
	public Set<Topic> getTopicsWithBusinessLogic() {
		Set<Topic> result = new LinkedHashSet<>();
		for (BusinessLogic businessLogic : this.businessLogic) {
			result.addAll(businessLogic.getTriggerTopics());
		}
		return result;
	}
	
	public boolean addBusinessLogic(BusinessLogic businessLogic) {
		return this.businessLogic.add(businessLogic);
	}

	public Version getBundleVersion() {
		Version result = Version.emptyVersion;
		if (getVersion() != null && getVersion().getSemanticVersion() != null) {
			result = getVersion().getSemanticVersion();
		}
			
		return result;
	}
	
	public VersionRange getBundleVersionRange() {
		return new VersionRange(VersionRange.LEFT_CLOSED, getBundleVersion(), null, VersionRange.RIGHT_OPEN);
	}
	
	/**
	 * @return
	 */
	public Set<SensorUsage> getAllSensorUsages() {
		Set<SensorUsage> result = new LinkedHashSet<>();
		for (Device device : devices) {
			result.addAll(device.getAllSensorUsages());
		}
		return result;
	}
	
	public SensorUsage getSensorUsage(String name) {
		for (SensorUsage sensorUsage : getAllSensorUsages()) {
			if (sensorUsage.getName().equalsIgnoreCase(name)) {
				return sensorUsage;
			}
		}
		
		return null;
	}
	
	/**
	 * @return
	 */
	public Set<ActuatorUsage> getAllActuatorUsages() {
		Set<ActuatorUsage> result = new LinkedHashSet<>();
		for (Device device : devices) {
			result.addAll(device.getAllActuatorUsages());
		}
		return result;
	}
	
	public ActuatorUsage getActuatorUsage(String name) {
		for (ActuatorUsage actuatorUsage : getAllActuatorUsages()) {
			if (actuatorUsage.getName().equalsIgnoreCase(name)) {
				return actuatorUsage;
			}
		}
		
		return null;
	}
}