/**
 * 
 */
package com.gs.gapp.metamodel.iot.device;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.iot.Measure;

/**
 * A sensor can point to the effective sensor being used. In this case, the original sensor name is not the
 * technical name but a name that takes semantics into account, e.g.:
 * "Temperature Sensor ABC123" vs. "Sensor to measure Soil Temperature in a Greenhouse"
 * 
 * @author mmt
 *
 */
public class Sensor extends Hardware {

	private static final long serialVersionUID = -2656643219652976097L;

	private final Set<Measure> measures = new LinkedHashSet<>();
	
	private Sensor implementation;
	
	private final Set<SensorUsage> sensorUsages = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public Sensor(String name) {
		super(name);
	}

	/**
	 * @return
	 */
	@Override
	public Sensor getImplementation() {
		return implementation;
	}

	public void setImplementation(Sensor implementation) {
		this.implementation = implementation;
	}

	public Set<Measure> getMeasures() {
		return measures;
	}
	
	public boolean addMeasure(Measure measure) {
		return this.measures.add(measure);
	}

	public Set<SensorUsage> getSensorUsages() {
		return sensorUsages;
	}
	
	public boolean addSensorUsage(SensorUsage sensorUsage) {
		addHardwareUsage(sensorUsage);
		return this.sensorUsages.add(sensorUsage);
	}
}
