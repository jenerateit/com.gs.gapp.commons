/**
 * 
 */
package com.gs.gapp.metamodel.iot;


/**
 * @author mmt
 *
 */
public class Topic extends AbstractIotModelElement {

	private static final long serialVersionUID = -2024997291469266947L;
	
	private final String qualifier;

	/**
	 * @param name
	 */
	public Topic(String name, String qualifier) {
		super(name);
		this.qualifier = qualifier;
	}

	public String getQualifier() {
		return qualifier;
	}
}
