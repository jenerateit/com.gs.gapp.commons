/**
 *
 */
package com.gs.gapp.metamodel.iot.topology;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.iot.Application;
import com.gs.gapp.metamodel.iot.Namespace;
import com.gs.gapp.metamodel.iot.device.DeviceModule;

/**
 * @author mmt
 *
 */
public class TopologyModule extends Module {

	/**
	 *
	 */
	private static final long serialVersionUID = 2898208776049989583L;

	private final Set<Broker> brokers = new LinkedHashSet<>();
	
	private final Set<DeviceModule> deviceModules = new LinkedHashSet<>();
	
	private final Set<Application> applications = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public TopologyModule(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#getNamespace()
	 */
	@Override
	public Namespace getNamespace() {
		return (Namespace) super.getNamespace();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#setNamespace(com.gs.gapp.metamodel.basic.Namespace)
	 */
	@Override
	public void setNamespace(com.gs.gapp.metamodel.basic.Namespace namespace) {
		if (!(namespace instanceof Namespace)) {
			throw new IllegalArgumentException("the namespace of a TopologyModule must be an instance of com.gs.gapp.metamodel.iot.Namespace");
		}
		super.setNamespace(namespace);
	}

	public Set<Broker> getBrokers() {
		return brokers;
	}
	
	public boolean addBroker(Broker broker) {
		boolean result = this.brokers.add(broker);
		this.addElement(broker);
		broker.setModule(this);
		return result;
	}

	public Set<DeviceModule> getDeviceModules() {
		return deviceModules;
	}
	
	public boolean addDeviceModule(DeviceModule module) {
		boolean result = this.deviceModules.add(module);
		this.addElement(module);
		return result;
	}

	public Set<Application> getApplications() {
		return applications;
	}
	
	public boolean addApplication(Application application) {
		boolean result = applications.add(application);
		this.addElement(application);
		application.setModule(this);
		return result;
	}
}
