package com.gs.gapp.metamodel.iot.device;

import java.util.LinkedHashSet;
import java.util.Set;


public class Functionblock extends Hardware {

	private static final long serialVersionUID = -2371998216896634184L;

	private final Set<SensorUsage> sensorUsages = new LinkedHashSet<>();
	private final Set<ActuatorUsage> actuatorUsages = new LinkedHashSet<>();
	private final Set<Functionblock> nestedFunctionblocks = new LinkedHashSet<>();
	private Functionblock implementation;
	private final Functionblock owner;
	
	public Functionblock(String name, Functionblock owner) {
		super(name);
		this.owner = owner;
		owner.addNestedFunctionblock(this);
	}
	
	/**
	 * @param name
	 */
	public Functionblock(String name) {
		super(name);
		this.owner = null;
	}

	public Set<SensorUsage> getSensorUsages() {
		return sensorUsages;
	}

	protected boolean addSensorUsage(SensorUsage sensorUsage) {
		boolean result = this.sensorUsages.add(sensorUsage);
		this.addHardwareUsage(sensorUsage);
		return result;
	}
	
	public Set<ActuatorUsage> getActuatorUsages() {
		return actuatorUsages;
	}

	protected boolean addActuatorUsage(ActuatorUsage actuatorUsage) {
		boolean result = this.actuatorUsages.add(actuatorUsage);
		this.addHardwareUsage(actuatorUsage);
		return result;
	}

	public Set<Functionblock> getNestedFunctionblocks() {
		return nestedFunctionblocks;
	}
	
	protected boolean addNestedFunctionblock(Functionblock functionblock) {
		return this.nestedFunctionblocks.add(functionblock);
	}

    @Override
	public Functionblock getImplementation() {
		return implementation;
	}

	public void setImplementation(Functionblock implementation) {
		this.implementation = implementation;
	}

	public Functionblock getOwner() {
		return owner;
	}
	
	/**
	 * @return
	 */
	public Set<SensorUsage> getAllSensorUsages() {
		Set<SensorUsage> result = new LinkedHashSet<>();
		collectSensorUsages(this, result);
		return result;
	}
	
	private void collectSensorUsages(Functionblock functionblock, Set<SensorUsage> sensors) {
		for (SensorUsage sensorUsage : functionblock.getSensorUsages()) {
			sensors.add(sensorUsage);
		}
		for (Functionblock nestedFunctionblock : functionblock.getNestedFunctionblocks()) {
			collectSensorUsages(nestedFunctionblock, sensors);
		}
	}
	
	/**
	 * @return
	 */
	public Set<ActuatorUsage> getAllActuatorUsages() {
		Set<ActuatorUsage> result = new LinkedHashSet<>();
		collectActuatorUsages(this, result);
		return result;
	}
	
	private void collectActuatorUsages(Functionblock functionblock, Set<ActuatorUsage> actuatorUsages) {
		for (ActuatorUsage actuatorUsage : functionblock.getActuatorUsages()) {
			actuatorUsages.add(actuatorUsage);
		}
		for (Functionblock nestedFunctionblock : functionblock.getNestedFunctionblocks()) {
			collectActuatorUsages(nestedFunctionblock, actuatorUsages);
		}
	}
	
	/**
	 * @return
	 */
	public Set<HardwareUsage> getAllHardwareUsages() {
		Set<HardwareUsage> result = new LinkedHashSet<>();
		
		result.addAll(getAllSensorUsages());
		result.addAll(getAllActuatorUsages());
		return result;
	}
}
