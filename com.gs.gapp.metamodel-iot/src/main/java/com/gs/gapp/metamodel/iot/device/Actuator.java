/**
 * 
 */
package com.gs.gapp.metamodel.iot.device;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author mmt
 *
 */
public class Actuator extends Sensor {

	private static final long serialVersionUID = -2656643219652976097L;
	
	private Actuator implementation;
	
	private final Set<ActuatorUsage> actuatorUsages = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public Actuator(String name) {
		super(name);
	}

	@Override
	public Actuator getImplementation() {
		return implementation;
	}

	public void setImplementation(Actuator implementation) {
		this.implementation = implementation;
	}

	public Set<ActuatorUsage> getActuatorUsages() {
		return actuatorUsages;
	}
	
	public boolean addActuatorUsage(ActuatorUsage actuatorUsage) {
		addHardwareUsage(actuatorUsage);
		return this.actuatorUsages.add(actuatorUsage);
	}
}
