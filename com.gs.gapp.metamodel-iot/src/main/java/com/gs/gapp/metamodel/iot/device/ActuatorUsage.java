/**
 * 
 */
package com.gs.gapp.metamodel.iot.device;

/**
 * A sensor can point to the effective sensor being used. In this case, the sensor is
 * meant to be so-called conceptual sensor.
 * 
 * @author mmt
 *
 */
public class ActuatorUsage extends SensorUsage {

	private static final long serialVersionUID = -2656643219652976097L;

	private Actuator implementation;
	
	/**
	 * @param name
	 */
	public ActuatorUsage(String name, Functionblock owner) {
		super(name, owner);
		owner.addActuatorUsage(this);
	}

	/**
	 * @return
	 */
	@Override
	public Actuator getImplementation() {
		return implementation;
	}

	public void setImplementation(Actuator implementation) {
		this.implementation = implementation;
		this.implementation.addActuatorUsage(this);
		super.setImplementation(implementation);
	}
}
