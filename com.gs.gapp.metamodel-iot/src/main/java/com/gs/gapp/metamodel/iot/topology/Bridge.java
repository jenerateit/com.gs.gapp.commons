/**
 * 
 */
package com.gs.gapp.metamodel.iot.topology;

import java.net.URL;

import com.gs.gapp.metamodel.iot.AbstractIotModelElement;

/**
 * If the bridge has a remote broker set and that remote broker has a url set, that url represents
 * this bridge's address. Otherwise, this bridge needs to have explicitely set the address of the remote broker.
 * The latter is normally the case when the remote broker does not exist in the model.
 * 
 * @author mmt
 *
 */
public class Bridge extends AbstractIotModelElement {

	private static final long serialVersionUID = 9208893470008056726L;
	
	private final Broker broker;
	
	private final Broker remoteBroker;
	
	private URL address;

	/**
	 * @param name
	 */
	public Bridge(String name, Broker broker, Broker remoteBroker) {
		super(name);
		this.broker = broker;
		this.remoteBroker = remoteBroker;
	}

	public Broker getRemoteBroker() {
		return remoteBroker;
	}

	/**
	 * @return
	 */
	public URL getAddress() {
		return address;
	}

	public void setAddress(URL address) {
		this.address = address;
	}

	public Broker getBroker() {
		return broker;
	}
}
