/**
 * 
 */
package com.gs.gapp.metamodel.iot.device;

import com.gs.gapp.metamodel.iot.AbstractIotModelElement;
import com.gs.gapp.metamodel.iot.Topic;

/**
 * A sensor can point to the effective sensor being used. In this case, the sensor is
 * meant to be so-called conceptual sensor.
 * 
 * @author mmt
 *
 */
public class HardwareUsage extends AbstractIotModelElement {

	private static final long serialVersionUID = -2656643219652976097L;

	private final Functionblock owner;
	
	private Topic topic;
	
	private Hardware implementation;
	
	/**
	 * @param name
	 */
	public HardwareUsage(String name, Functionblock owner) {
		super(name);
		this.owner = owner;
	}

	public Functionblock getOwner() {
		return owner;
	}

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}

	public Hardware getImplementation() {
		return implementation;
	}

	protected void setImplementation(Hardware implementation) {
		this.implementation = implementation;
	}
}
