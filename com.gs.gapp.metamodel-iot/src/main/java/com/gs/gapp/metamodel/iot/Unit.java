/**
 * 
 */
package com.gs.gapp.metamodel.iot;


/**
 * @author mmt
 *
 */
public class Unit extends AbstractIotModelElement {

	private static final long serialVersionUID = 1656275280213516820L;

	/**
	 * @param name
	 */
	public Unit(String name) {
		super(name);
	}

}
