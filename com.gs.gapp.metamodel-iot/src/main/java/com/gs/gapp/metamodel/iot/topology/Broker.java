/**
 * 
 */
package com.gs.gapp.metamodel.iot.topology;

import java.net.URL;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.iot.Node;

/**
 * @author mmt
 *
 */
public class Broker extends Node {

	private static final long serialVersionUID = 5658974233609990115L;

	private Broker implementation;
	
	private URL url;
	
	private final Set<Bridge> bridges = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public Broker(String name) {
		super(name);
	}

	public Set<Bridge> getBridges() {
		return bridges;
	}

	public boolean addBridge(Bridge bridge) {
		return this.bridges.add(bridge);
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public Broker getImplementation() {
		return implementation;
	}

	public void setImplementation(Broker implementation) {
		this.implementation = implementation;
	}
}
