/**
 * 
 */
package com.gs.gapp.metamodel.iot.device;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.iot.Application;
import com.gs.gapp.metamodel.iot.Node;
import com.gs.gapp.metamodel.iot.Topic;

/**
 * @author mmt
 *
 */
public abstract class Hardware extends Node {

	private static final long serialVersionUID = -1934899574808348076L;
	
	private Topic topic;
	
	private ComplexType statusDataStructure;
	
	private ComplexType configurationDataStructure;
	
	private ComplexType connectionDataStructure;
	
	private ExceptionType faultDataStructure;

	private FunctionModule operations;
	
	private final Set<HardwareUsage> hardwareUsages = new LinkedHashSet<>();
	
	public Hardware(String name) {
		super(name);
	}
	
	abstract Hardware getImplementation();

	public Topic getTopic() {
		return topic;
	}

	public void setTopic(Topic topic) {
		this.topic = topic;
	}
	
	/**
	 * @return the application that uses this hardware
	 */
	public Application getApplication() {
        Application result = null;
        
        for (HardwareUsage hardwareUsage : hardwareUsages) {
        	if (hardwareUsage.getOwner() != null && hardwareUsage.getOwner() != this && hardwareUsage.getOwner().getApplication() != null) {
        		Application potentialResult = hardwareUsage.getOwner().getApplication();
        		if (result == null || result == potentialResult) {
        			result = potentialResult;
        		} else {
        			throw new RuntimeException("found more than one application that uses one and the same sensor: app1=" + result + ", app2=" + potentialResult);
        		}
        	}
        }
		
		return result;
	}
	
	/**
	 * Any hardware that has no producer set is considered to be a conceptual hardware.
	 * Conceptual hardware means that there is no real hardware existing that relates to this.
	 * Conceptual hardware is going to be used to simulate an IoT system without the need to have any hardware.
	 *  
	 * @return
	 */
	public boolean isConceptualOnly() {
		return this.getProducer() == null;
	}

	public Set<HardwareUsage> getHardwareUsages() {
		return hardwareUsages;
	}
	
	protected boolean addHardwareUsage(HardwareUsage hardwareUsage) {
		return this.hardwareUsages.add(hardwareUsage);
	}

	/**
	 * This data structure represents the status of the hardware.
	 * Example: in case of a switch this could be a boolean indicating whether the switch is on or off. 
	 * 
	 * @return
	 */
	public ComplexType getStatusDataStructure() {
		return statusDataStructure;
	}

	public void setStatusDataStructure(ComplexType statusDataStructure) {
		this.statusDataStructure = statusDataStructure;
	}

	/**
	 * Configuration data is meant to be used to initialize the hardware.
	 * Example: in case of light bulb the brightness level could be configured.
	 * 
	 * @return
	 */
	public ComplexType getConfigurationDataStructure() {
		return configurationDataStructure;
	}

	public void setConfigurationDataStructure(ComplexType configurationDataStructure) {
		this.configurationDataStructure = configurationDataStructure;
	}

	public ExceptionType getFaultDataStructure() {
		return faultDataStructure;
	}

	public void setFaultDataStructure(ExceptionType faultDataStructure) {
		this.faultDataStructure = faultDataStructure;
	}

	/**
	 * This function module holds functions that are supposed to provide
	 * higher level driver logic.
	 * Example: a gyroscope has x, y and z values. Higher level functions
	 *          isHorizontal(), isVertical() could make it easier to use
	 *          a device driver since this logic doesn't have to be implemented
	 *          by the driver's caller.
	 * 
	 * @return
	 */
	public FunctionModule getOperations() {
		return operations;
	}

	public void setOperations(FunctionModule operations) {
		this.operations = operations;
	}

	/**
	 * Connection data is meant to be used to connect the hardware.
	 * Example: a hardware pin number could be set on an instance of the connection data structure.
	 * 
	 * @return
	 */
	public ComplexType getConnectionDataStructure() {
		return connectionDataStructure;
	}

	public void setConnectionDataStructure(ComplexType connectionDataStructure) {
		this.connectionDataStructure = connectionDataStructure;
	}
}
