/**
 *
 */
package com.gs.gapp.metamodel.iot;

import java.util.HashMap;
import java.util.Map;

/**
 * @author swr
 *
 */
public enum ApplicationTypeEnum {

	GATEWAY ("Gateway"),
	WEB ("Web"),
	MOBILE ("Mobile"),
	;

	private static Map<String, ApplicationTypeEnum> nameToEnumMap =
			new HashMap<>();

	static {
		for (ApplicationTypeEnum e : values()) {
			nameToEnumMap.put(e.getName().toLowerCase(), e);
		}
	}

	private final String name;
	
	/**
	 * @param name
	 */
	private ApplicationTypeEnum(String name) {
		this.name = name;
	}


	public static ApplicationTypeEnum getFromName(String name) {
		return nameToEnumMap.get(name.toLowerCase());
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
