/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.converter.ui.function;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;

import com.gs.gapp.metamodel.basic.ModelElementCache;

/**
 * @author hrr
 *
 */
public class UiToFunctionConverterProvider implements ModelConverterProviderI{

	/**
	 *
	 */
	public UiToFunctionConverterProvider() {
		super();
	}

	@Override
	public ModelConverterI getModelConverter() {
		return new UiToFunctionConverter(new ModelElementCache());
	}
}
