package com.gs.gapp.converter.ui.function;

import java.io.Serializable;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.metamodel.analytics.AbstractAnalyticsConverterOptions;

public class UiToFunctionConverterOptions extends AbstractAnalyticsConverterOptions {
	
	public static final String OPTION_USE_INTERFACES = "use-interfaces";
	public static final String OPTION_NAMESPACE_POSTFIX = "namespace-postfix";
	
	public UiToFunctionConverterOptions(ModelConverterOptions options) {
		super(options);
	}
	
	public boolean areInterfacesUsed() {
		String useInterfaces = (String) getOptions().get(OPTION_USE_INTERFACES);
		validateBooleanOption(useInterfaces, OPTION_USE_INTERFACES);
		return useInterfaces != null && useInterfaces.length() > 0 ?
			Boolean.valueOf(useInterfaces) : false;
	}

	/**
	 * @return
	 */
	public String getNamespacePostfix() {
		Serializable namespacePostfix = getOptions().get(OPTION_NAMESPACE_POSTFIX);

		String result = namespacePostfix == null || namespacePostfix.toString().length() == 0 ? "function" : namespacePostfix.toString();
		if (result != null && result.startsWith(".") == false) result = "." + result;
		return result;
	}
}
