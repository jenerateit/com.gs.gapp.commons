package com.gs.gapp.converter.ui.function;

import java.util.Set;

import com.gs.gapp.dsl.ejb.EjbInterfacesEnum;
import com.gs.gapp.dsl.ejb.EjbOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.Namespace;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.product.Capability;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.UIViewContainer;

/**
 * @author marcu
 *
 * @param <S>
 * @param <T>
 */
public class UIStructuralContainerToFunctionModuleConverter<S extends UIStructuralContainer, T extends FunctionModule> extends
    AbstractM2MModelElementConverter<S, T> {


	public UIStructuralContainerToFunctionModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S uiStructuralContainer, T functionModule) {
		super.onConvert(uiStructuralContainer, functionModule);

		if (getConverterOptions().areInterfacesUsed()) {
			// --- ensure that there is a local interface generated for the EJB
			OptionDefinitionString optionDefinitionEjbInterfaces = new OptionDefinitionString(EjbOptionEnum.EJB_INTERFACES.getName());
			OptionDefinition<String>.OptionValue useLocalInterface = optionDefinitionEjbInterfaces.new OptionValue(EjbInterfacesEnum.LOCAL.getName());
			functionModule.addOptions(useLocalInterface);
		}
	}
	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		
		boolean isResponsible = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		if (isResponsible) {
			UIStructuralContainer uiStructuralContainer = (UIStructuralContainer) originalModelElement;
			isResponsible = uiStructuralContainer.isMainContainer();
		}
		
		return isResponsible;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S uiStructuralContainer, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T functionModule = (T) new FunctionModule(uiStructuralContainer.getName());
		
		functionModule.setNamespace( new Namespace(uiStructuralContainer.getModule().getNamespace().getName()) );
		
		// --- assign persistence modules
		Set<Capability> capabilities = uiStructuralContainer.getModel().getElements(Capability.class);
		for (Entity entity : uiStructuralContainer.getEntities()) {
			if (entity.getPersistenceModule() != null) {
				for (Capability capability : capabilities) {
					if (capability.getAllPersistenceModulesInStorages().contains(entity.getPersistenceModule())) {
						// there is at least one capability that uses the entity's persistence module as storage => we can presume that the created function module has to handle the persistence module (e.g. a persitence module in JPA for an EJB) 
						functionModule.addPersistenceModule(entity.getPersistenceModule());
					}
				}
			}
		}
		
		return functionModule;
	}

	@Override
	protected UiToFunctionConverterOptions getConverterOptions() {
		return getModelConverter().getConverterOptions();
	}

	@Override
	protected UiToFunctionConverter getModelConverter() {
		if (super.getModelConverter() instanceof UiToFunctionConverter) {
		    return (UiToFunctionConverter) super.getModelConverter();
		}
		
		throw new RuntimeException("model element converter " + this.getClass() + " has been incorrectly used (wrong model converter " + super.getModelConverter().getClass() + ")");
	}

	/**
	 * @author marcu
	 *
	 * @param <S>
	 * @param <T>
	 */
	public static class UIViewContainerToFunctionModuleConverter<S extends UIViewContainer, T extends FunctionModule> extends UIStructuralContainerToFunctionModuleConverter<S,T> {

		public UIViewContainerToFunctionModuleConverter(AbstractConverter modelConverter) {
			super(modelConverter);
		}

		/* (non-Javadoc)
		 * @see com.gs.gapp.converter.ui.function.UIStructuralContainerToFunctionModuleConverter#onCreateModelElement(com.gs.gapp.metamodel.ui.container.UIStructuralContainer, com.gs.gapp.metamodel.basic.ModelElementI)
		 */
		@Override
		protected T onCreateModelElement(S uiViewContainer, ModelElementI previousResultingModelElement) {
			return super.onCreateModelElement(uiViewContainer, previousResultingModelElement);
		}
	}
}
