package com.gs.gapp.converter.ui.function;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.function.Namespace;

public class NamespaceToFunctionNamespaceConverter<S extends com.gs.gapp.metamodel.ui.Namespace, T extends Namespace> extends
    AbstractM2MModelElementConverter<S, T> {

	public NamespaceToFunctionNamespaceConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#getModelConverter()
	 */
	@Override
	protected UiToFunctionConverter getModelConverter() {
		return (UiToFunctionConverter) super.getModelConverter();
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#getConverterOptions()
	 */
	@Override
	protected UiToFunctionConverterOptions getConverterOptions() {
		return getModelConverter().getConverterOptions();
	}

	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		
		String postfix = getConverterOptions().getNamespacePostfix() == null ? "" : getConverterOptions().getNamespacePostfix();
		
		@SuppressWarnings("unchecked")
		T result = (T) new Namespace(originalModelElement.getName() + postfix);
		result.setOriginatingElement(originalModelElement);
		return result;
	}
}
