package com.gs.gapp.converter.ui.function;

import java.util.List;
import java.util.Set;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.converter.ui.function.UIStructuralContainerToFunctionModuleConverter.UIViewContainerToFunctionModuleConverter;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.product.Organization;
import com.gs.gapp.metamodel.product.ProductModule;
import com.gs.gapp.metamodel.ui.UIModule;
import com.gs.gapp.metamodel.ui.container.UIContainer;


/**
 * @author mmt
 *
 */
public class UiToFunctionConverter extends AbstractAnalyticsConverter {

	public UiToFunctionConverter(ModelElementCache modelElementCache) {
		super(modelElementCache);
	}


	private UiToFunctionConverterOptions converterOptions;
	

	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.BasicConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result =
				super.onGetAllModelElementConverters();

		result.add(new UIStructuralContainerToFunctionModuleConverter<>(this));
		result.add(new UIViewContainerToFunctionModuleConverter<>(this));
		result.add(new NamespaceToFunctionNamespaceConverter<>(this));

		return result;
	}
	
	

    @Override
	protected Set<Object> onPerformModelConsolidation(Set<?> normalizedElements) {
		Set<Object> result = super.onPerformModelConsolidation(normalizedElements);
		
		// --- re-add all elements that serve as input for this model converter (pass-through functionality), exception: model element of type 'Model'
		for (Object normalizedElement : normalizedElements) {
			if (normalizedElement instanceof Model) continue;
			
			if (normalizedElement instanceof UIModule) {
			    UIModule uiModule = (UIModule) normalizedElement;
				result.add(uiModule);
			    getModel().addElement(uiModule);
			} else if (normalizedElement instanceof ProductModule) {
			    ProductModule productModule = (ProductModule) normalizedElement;
				result.add(productModule);
			    getModel().addElement(productModule);
			} else if (normalizedElement instanceof UIContainer) {
				result.add(normalizedElement);
			} else if (normalizedElement instanceof Organization) {
				result.add(normalizedElement);
				getModel().addElement((Organization) normalizedElement);
			}
		}
		
		return result;
	}
    

	/* (non-Javadoc)
     * @see com.gs.gapp.converter.analytics.AbstractAnalyticsConverter#getConverterOptions()
     */
    @Override
	public UiToFunctionConverterOptions getConverterOptions() {
    	if (this.converterOptions == null) {
    		this.converterOptions = new UiToFunctionConverterOptions(getOptions());
    	}
		return converterOptions;
	}
}
