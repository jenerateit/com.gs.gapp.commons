package com.gs.gapp.metamodel.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs.gapp.metamodel.basic.ModelElementWrapper;
import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.container.UIActionContainer;
import com.gs.gapp.metamodel.ui.container.UIContainer;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * @author marcu
 *
 */
public class ValidatorDuplicateContainersInView implements ModelValidatorI {

	public ValidatorDuplicateContainersInView() {}

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(assertNoDuplicateContainersPerView(modelElements));
		result.addAll(assertNoDuplicateComponentNamesPerContainer(modelElements));
		return result;
	}

	/**
	 * @param rawElements
	 */
	private Collection<Message> assertNoDuplicateContainersPerView(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();
		
		Map<UIStructuralContainer, Map<String, List<UIContainer>>> containerMap = new LinkedHashMap<>();
		
		// --- collect all duplicate containers
		for (Object element : rawElements) {
			if (element instanceof UIStructuralContainer) {
				UIStructuralContainer potentialMainContainer = (UIStructuralContainer) element;
				if (potentialMainContainer.isMainContainer()) {
					LinkedHashMap<String, List<UIContainer>> containerMapForName = new LinkedHashMap<>();
					containerMap.put(potentialMainContainer, containerMapForName);
					for (UIContainer container : potentialMainContainer.getAllChildContainers()) {
						if (!containerMapForName.containsKey(container.getName())) {
							List<UIContainer> containersWithSameName = potentialMainContainer.getAllContainersWithSameName(container);
							if (containersWithSameName.size() > 1) {
								// Bang!!! We have found at least two containers that have the same name.
								containerMapForName.put(container.getName(), containersWithSameName);
							}
						}
					}
				}
			}
		}
		
		// --- create validation messages for duplicate usage of containers
		for (UIStructuralContainer mainContainer : containerMap.keySet()) {
			Map<String, List<UIContainer>> duplicateContainerMap = containerMap.get(mainContainer);
			for (String containerName : duplicateContainerMap.keySet()) {
				List<UIContainer> containersWithDuplicateName = duplicateContainerMap.get(containerName);
				
				StringBuilder messageText = new StringBuilder();
				Set<String> handledContainerNames = new HashSet<>();
				for (UIContainer containerWithDuplicateName : new LinkedHashSet<>(containersWithDuplicateName)) {
					if (handledContainerNames.contains(containerWithDuplicateName.getName())) continue;
					
					String typeOfContainer = null;
					int frequency = Collections.frequency(containersWithDuplicateName, containerWithDuplicateName);
					Object originatingElement = containerWithDuplicateName.getOriginatingElement();
					if (originatingElement instanceof ModelElementWrapper) {
						ModelElementWrapper modelElementWrapper = (ModelElementWrapper) originatingElement;
						typeOfContainer = modelElementWrapper.getType();
					} else if (originatingElement != null) {
						typeOfContainer = originatingElement.getClass().getSimpleName();
					} else {
						typeOfContainer = containerWithDuplicateName.getClass().getSimpleName();
					}
					
					if (typeOfContainer != null) {
					    messageText.append(System.lineSeparator()).append("   ").append(typeOfContainer).append(":").append(frequency);
					}
					handledContainerNames.add(containerWithDuplicateName.getName());
				}
				
				Message errorMessage =
						UiMessage.DUPLICATE_CONTAINERS_IN_VIEW.getMessage(mainContainer.getName(),
								mainContainer.getModule().getName(),
								containersWithDuplicateName.size(),
								containerName,
								messageText.toString());
				
				result.add(errorMessage);
			}
		}

		return result;
	}
	
	
	/**
	 * @param rawElements
	 */
	private Collection<Message> assertNoDuplicateComponentNamesPerContainer(Collection<?> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();
		
		LinkedHashMap<UIContainer, Map<String, List<UIComponent>>> containersWithDuplicateComponents = new LinkedHashMap<>();
		
		// --- collection data-/action-containers that contain components with duplicate names
		for (Object element : rawElements) {
			if (element instanceof UIDataContainer) {
				UIDataContainer uiDataContainer = (UIDataContainer) element;
				if (!containersWithDuplicateComponents.containsKey(uiDataContainer)) {
					Map<String, List<UIComponent>> componentsByName = uiDataContainer.getComponents().stream().collect(Collectors.groupingBy(UIComponent::getName));
					containersWithDuplicateComponents.put(uiDataContainer, componentsByName);
					Collection<String> keysToBeRemoved = new ArrayList<>();
					for (String name : componentsByName.keySet()) {
						List<UIComponent> components = componentsByName.get(name);
						if (components.size() <= 1) {
							keysToBeRemoved.add(name);
						}
					}
					for (String keyToBeRemoved : keysToBeRemoved) {
					    componentsByName.remove(keyToBeRemoved);
					}
					
				}
				
			} else if (element instanceof UIActionContainer) {
				UIActionContainer uiActionContainer = (UIActionContainer) element;
				if (!containersWithDuplicateComponents.containsKey(uiActionContainer)) {
					Map<String, List<UIComponent>> actionComponentsByName = uiActionContainer.getAllActionComponents().stream().collect(Collectors.groupingBy(UIComponent::getName));
					containersWithDuplicateComponents.put(uiActionContainer, actionComponentsByName);
					Collection<String> keysToBeRemoved = new ArrayList<>();
					for (String name : actionComponentsByName.keySet()) {
						List<UIComponent> components = actionComponentsByName.get(name);
						if (components.size() <= 1) {
							keysToBeRemoved.add(name);
						}
					}
					for (String keyToBeRemoved : keysToBeRemoved) {
						actionComponentsByName.remove(keyToBeRemoved);
					}
				}
			}
		}
		
		// --- create validation messages for data-/action-containers with duplicate usage of components
		containersWithDuplicateComponents.forEach((container, compMap) -> {
			if (compMap.size() > 0) {
			    StringBuilder messageText = new StringBuilder("action/data container '").append(container.getName()).append("' has components with duplicate names:");
			    compMap.forEach((name, compList) -> {
			    	messageText.append(System.lineSeparator()).append("   ").append(compList.size()).append("x '").append(name).append("'");
			    });
			    messageText.append(System.lineSeparator()).append("   ==> Change the component names in the model or remove some of the components that have duplicate names.");
			    result.add(new Message(MessageStatus.ERROR, messageText.toString()));
			}
		});
		
		return result;
	}
}
