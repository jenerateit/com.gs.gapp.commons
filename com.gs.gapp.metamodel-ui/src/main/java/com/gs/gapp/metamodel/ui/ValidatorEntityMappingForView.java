package com.gs.gapp.metamodel.ui;

import java.util.Collection;
import java.util.LinkedHashSet;

import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * This validator checks, whether the modeled entity mapping for displays is correct
 * such that subsequent converter and writer steps can process the ui model.
 * 
 * @author marcu
 *
 */
public class ValidatorEntityMappingForView implements ModelValidatorI {

	public ValidatorEntityMappingForView() {}

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(assertNoEntityFieldMappingWithoutEntityMapping(modelElements));
		result.addAll(assertEntityMapping(modelElements));
		return result;
	}

	/**
	 * @param rawElements
	 */
	private Collection<Message> assertNoEntityFieldMappingWithoutEntityMapping(Collection<Object> rawElements) {
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		
		// --- collect all data containers with incorrect modeling
		for (Object element : rawElements) {
			if (element instanceof UIStructuralContainer) {
				UIStructuralContainer potentialMainContainer = (UIStructuralContainer) element;
				if (potentialMainContainer.isMainContainer()) {
					LinkedHashSet<Message> resultPerView = new LinkedHashSet<>();
					for (UIDataContainer dataContainer : potentialMainContainer.getAllChildDataContainers()) {
						if ((!dataContainer.getMappedEntityTypedFields().isEmpty()) && dataContainer.getEntity() == null) {
							if (resultPerView.isEmpty()) {
								Message mainMessage = UiMessage.MISSING_ENTITY_MAPPING_FOR_DISPLAY_SUMMARY.getMessage(potentialMainContainer.getName(), potentialMainContainer.getModule().getName());
								resultPerView.add(mainMessage);
							}
							Message messagePerDisplay = UiMessage.MISSING_ENTITY_MAPPING_FOR_DISPLAY.getMessage(dataContainer.getName(),
									dataContainer.getModule().getName(),
									dataContainer.getMappedEntityTypedFields().size());
							resultPerView.add(messagePerDisplay);
						}
					}
					result.addAll(resultPerView);
				}
			}
		}
		
		return result;
	}
	
	/**
	 * @param rawElements
	 */
	private Collection<Message> assertEntityMapping(Collection<Object> rawElements) {
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		
		// --- collect all data containers with incorrect modeling
		for (Object element : rawElements) {
			if (element instanceof UIStructuralContainer) {
				UIStructuralContainer potentialMainContainer = (UIStructuralContainer) element;
				if (potentialMainContainer.isMainContainer()) {
					LinkedHashSet<Message> resultPerView = new LinkedHashSet<>();
					for (UIDataContainer dataContainer : potentialMainContainer.getAllChildDataContainers()) {
						if (dataContainer.getEntity() != null && (dataContainer.getEntity().isAbstractType() || dataContainer.getEntity().isMappedSuperclass())) {	
							if (resultPerView.isEmpty()) {
								Message mainMessage = new Message(MessageStatus.ERROR, "view/page '" + potentialMainContainer.getName() + "' in module '" + potentialMainContainer.getModule().getName() + "' has display sections that have an incorrect entity mapped.");
								resultPerView.add(mainMessage);
							}
							
							StringBuilder commentOnError = new StringBuilder();
							if (dataContainer.getEntity().isAbstractType()) {
								if (commentOnError.length() > 0) commentOnError.append(" and ");
								commentOnError.append("abstract"); 
							}
							if (dataContainer.getEntity().isMappedSuperclass()) {
								if (commentOnError.length() > 0) commentOnError.append(" and ");
							    commentOnError.append("mapped supperclass");
							}
							
							Message messagePerDisplay = new Message(MessageStatus.ERROR, "display '" + dataContainer.getName() + "' in module '" + dataContainer.getModule().getName() +
									"' has the entity '" + dataContainer.getEntity().getName() + "' mapped that is " + commentOnError +". That's not a valid entity for this mapping.");
							resultPerView.add(messagePerDisplay);
						}
					}
					result.addAll(resultPerView);
				}
			}
		}
		
		return result;
	}
}
