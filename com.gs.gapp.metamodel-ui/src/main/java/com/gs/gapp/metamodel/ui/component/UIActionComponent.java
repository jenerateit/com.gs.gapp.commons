/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.ui.ActionPurpose;
import com.gs.gapp.metamodel.ui.ContainerFlow;
import com.gs.gapp.metamodel.ui.container.UIActionContainer;
import com.gs.gapp.metamodel.ui.container.UIContainer;

/**
 * @author mmt
 *
 */
public class UIActionComponent extends UIComponent {

    /**
	 *
	 */
	private static final long serialVersionUID = -4937262789957517074L;

	private boolean hasLabel = true;
    private boolean hasIcon = true;
    
    private UIActionContainer owningActionContainer;
    
    private final Set<ContainerFlow> flows = new LinkedHashSet<>();

    private final Set<UIActionGroup> groups = new LinkedHashSet<>();
    
    private boolean longLasting = false;
    
    private ActionPurpose actionPurpose = ActionPurpose.UNDEFINED;
    
    private boolean defaultAction = false;
    
    private String keyboardControl;

	/**
	 * @param name
	 */
	public UIActionComponent(String name) {
		super(name);
		setWithoutLabel(true);
		setReadOnly(true);
	}

	/**
	 * @return the hasIcon
	 */
	public boolean isHasIcon() {
		return hasIcon;
	}

	/**
	 * @param hasIcon the hasIcon to set
	 */
	public void setHasIcon(boolean hasIcon) {
		this.hasIcon = hasIcon;
	}

	/**
	 * @return the hasLabel
	 */
	public boolean isHasLabel() {
		return hasLabel;
	}

	/**
	 * @param hasLabel the hasLabel to set
	 */
	public void setHasLabel(boolean hasLabel) {
		this.hasLabel = hasLabel;
	}

	/**
	 * @return the flows
	 */
	public Set<ContainerFlow> getFlows() {
		return flows;
	}

	/**
	 * @param flow
	 * @return
	 */
	public boolean addFlow(ContainerFlow flow) {
		if (flow == null) throw new NullPointerException("param 'flow' must not be null");
		return this.flows.add(flow);
	}

	/**
	 * @return the groups
	 */
	public Set<UIActionGroup> getGroups() {
		return groups;
	}

	/**
	 * @param group
	 * @return
	 */
	public boolean addGroup(UIActionGroup group) {
		if (group == null) throw new NullPointerException("param 'group' must not be null");
		return this.groups.add(group);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.ui.component.UIComponent#hasSeparateLabel()
	 */
	@Override
	public boolean hasSeparateLabel() {
		return false;
	}

	public UIActionContainer getOwningActionContainer() {
		return owningActionContainer;
	}

	public void setOwningActionContainer(UIActionContainer owningActionContainer) {
		this.owningActionContainer = owningActionContainer;
	}

	public boolean isLongLasting() {
		return longLasting;
	}

	public void setLongLasting(boolean longLasting) {
		this.longLasting = longLasting;
	}

	public ActionPurpose getActionPurpose() {
		return actionPurpose;
	}

	public void setActionPurpose(ActionPurpose actionPurpose) {
		this.actionPurpose = actionPurpose;
	}
	
	/**
	 * @return the owner
	 */
	@Override
	public UIContainer getOwner() {
		UIContainer componentOwner = super.getOwner();
		if (componentOwner == null) {
			return getOwningActionContainer();
		}
		
		return componentOwner;
	}

	public boolean isDefaultAction() {
		return defaultAction;
	}

	public void setDefaultAction(boolean defaultAction) {
		this.defaultAction = defaultAction;
	}

	/**
	 * @return the keyboardControl
	 */
	public String getKeyboardControl() {
		return keyboardControl;
	}

	/**
	 * @param keyboardControl the keyboardControl to set
	 */
	public void setKeyboardControl(String keyboardControl) {
		this.keyboardControl = keyboardControl;
	}
}
