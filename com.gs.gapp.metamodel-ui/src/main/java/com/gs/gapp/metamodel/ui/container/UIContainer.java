/**
 *
 */
package com.gs.gapp.metamodel.ui.container;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.gs.gapp.metamodel.basic.Placement;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.ServiceInterface;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.ui.ContainerFlow;
import com.gs.gapp.metamodel.ui.ContainerFlowType;
import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.component.UIEmbeddedComponentGroup;
import com.gs.gapp.metamodel.ui.component.UIEmbeddedContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;
import com.gs.gapp.metamodel.ui.databinding.ContainerGroup;

/**
 * @author mmt
 *
 */
public class UIContainer extends ComplexType {

	/**
	 *
	 */
	private static final long serialVersionUID = 6099635173926421071L;

	private final Set<UIContainer> parentContainers = new LinkedHashSet<>();

	private final Set<UIActionContainer> actionContainers = new LinkedHashSet<>();
	
	private final Map<UIActionContainer,Placement> actionContainerPlacement = new LinkedHashMap<>();

	private String title;

	private final Set<ContainerFlow> flows = new LinkedHashSet<>();

	private final Set<ContainerGroup> groups = new LinkedHashSet<>();

	private final Set<UIEmbeddedContainer> embeddingComponents = new LinkedHashSet<>();

	private boolean scrollable = true;
	
    private boolean optional = false;
	
	private boolean disablable = false;
	
	private final Set<Entity> consumedEntities = new LinkedHashSet<>();
	
	private final Set<FunctionModule> consumedFunctionModules = new LinkedHashSet<>();
	
	private ComplexType configurationItems;
	
	private ComplexType initParameters;
	
	private Enumeration layoutVariants;
	
	private ComplexType testPreludeParameters;
	
	private final Set<UIStructuralContainer> testPreludeViews = new LinkedHashSet<>();
	
	private final Set<ServiceInterface> consumedServiceInterfaces = new LinkedHashSet<>();
	
	private boolean dynamicStructure = false;
	
	private final Set<String> messageKeys = new LinkedHashSet<>();  // APPJSF-610
	
    private final Set<String> messageKeysForWarnings = new LinkedHashSet<>();  // APPJSF-610
	
	private final Set<String> messageKeysForErrors = new LinkedHashSet<>();  // APPJSF-610
	
	/**
	 * @param name
	 */
	public UIContainer(String name) {
		super(name);
	}

	/**
	 * @return the actionContainers
	 */
	public Set<UIActionContainer> getActionContainers() {
		return actionContainers;
	}
	
	/**
	 * @param pattern
	 * @return
	 */
	public Set<UIActionContainer> getActionContainers(String pattern) {
		Set<UIActionContainer> result = new LinkedHashSet<>();
		
		for (UIActionContainer actionContainer : this.actionContainers) {
			if (actionContainer.getTechnicalName().toLowerCase().contains(pattern.toLowerCase())) result.add(actionContainer);
		}
		
		return result;
	} 

	/**
	 * @param container
	 * @return
	 */
	public boolean addActionContainer(UIActionContainer container) {
		container.addParentContainer(this);
		return this.actionContainers.add(container);
	}
	
	/**
	 * @param container
	 * @param placement
	 * @return
	 */
	public boolean addActionContainer(UIActionContainer container, Placement placement) {
		boolean result = addActionContainer(container);
		if (placement != null) {
		    this.actionContainerPlacement.put(container, placement);
		}
		return result;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the flows
	 */
	public Set<ContainerFlow> getFlows() {
		return flows;
	}

	/**
	 * @return
	 */
	public Set<ContainerFlow> getOutoingFlows() {
		Set<ContainerFlow> result = new LinkedHashSet<>();
		for (ContainerFlow flow : getFlows()) {
			if (flow.getSource() == this) {
				result.add(flow);
			}
		}
		return result;
	}

	/**
	 * @return
	 */
	public Set<ContainerFlow> getIncomingFlows() {
		Set<ContainerFlow> result = new LinkedHashSet<>();
		for (ContainerFlow flow : getFlows()) {
			if (flow.getTarget() == this) {
				result.add(flow);
			}
		}
		return result;
	}

	/**
	 * @param flow
	 * @return
	 */
	public boolean addFlow(ContainerFlow flow) {
	    return this.flows.add(flow);
	}

	/**
	 * @param flow
	 * @return
	 */
	public boolean removeFlow(ContainerFlow flow) {
		if (flow.getTarget() == this) flow.setTarget(null);
		return this.flows.remove(flow);
	}

	/**
	 * @return true if this data container serves as the target container in at least one container flow
	 */
	public boolean isTargetOfAtLeastOneFlow() {
		return isTargetOfAtLeastOneFlow(false);
	}

	/**
	 * @param ignoreReplaceType if this is set to true, flows of type "REPLACE" are not going to be taken into account.
	 * @return true if this data container serves as the target container in at least one container flow
	 */
	public boolean isTargetOfAtLeastOneFlow(boolean ignoreReplaceType) {
		boolean result = false;

		for (ContainerFlow flow : getFlows()) {
			if (ignoreReplaceType && flow.getType() == ContainerFlowType.REPLACE) {
				continue;
			}
			if (flow.getTarget() == this) {
				result = true;
				break;
			}
		}

		return result;
	}

	/**
	 * @return the groups
	 */
	public Set<ContainerGroup> getGroups() {
		return groups;
	}

	/**
	 * @param group
	 * @return
	 */
	public boolean addGroup(ContainerGroup group) {
	    return this.groups.add(group);
	}

	/**
	 * @return the parentContainers
	 */
	public Set<UIContainer> getParentContainers() {
		Set<UIContainer> parentAndFlowSourceContainers = new LinkedHashSet<>();

		parentAndFlowSourceContainers.addAll(parentContainers);
		for (ContainerFlow flow : this.getFlows()) {
			if (flow.getTarget() == this && flow.getType() == ContainerFlowType.REPLACE) {
				parentAndFlowSourceContainers.add(flow.getSource());
			}
		}

		for (UIEmbeddedContainer embeddingComponent : getEmbeddingComponents()) {
			parentAndFlowSourceContainers.add(embeddingComponent.getOwner());
		}

		return parentAndFlowSourceContainers;
	}

	
	/**
	 * @return
	 */
	public UIContainer getRootContainer() {
		UIContainer result = this;
		
		if (this.parentContainers.size() > 1) throw new RuntimeException("more than one parent container found for container '" + this + "':" + this.parentContainers);
		
		if (this.parentContainers.size() == 1) {
			result = this.parentContainers.iterator().next().getRootContainer();
		}
	
			
		return result;
	}
	
	public boolean hasNoParents() {
		return this.parentContainers.size() == 0;
	}

	/**
	 * @return
	 */
	public Set<UIContainer> getAllParentContainers() {
		Set<UIContainer> result = new LinkedHashSet<>();
		collectAllParentContainers(result);
		return result;
	}

	/**
	 * @param containers
	 * @return
	 */
	protected Set<UIContainer> collectAllParentContainers(Set<UIContainer> containers) {
		for (UIContainer parentContainer : getParentContainers()) {
			containers.add(parentContainer);
			parentContainer.collectAllParentContainers(containers);
		}
		return containers;
	}

	/**
	 * @param parentContainer
	 * @return
	 */
	public boolean addParentContainer(UIContainer parentContainer) {
		return this.parentContainers.add(parentContainer);
	}

	/**
	 * @return the embeddingComponents
	 */
	public Set<UIEmbeddedContainer> getEmbeddingComponents() {
		return embeddingComponents;
	}

	/**
	 * @param embeddedContainer
	 * @return
	 */
	public boolean addEmbeddingComponent(UIEmbeddedContainer embeddedContainer) {
		return embeddingComponents.add(embeddedContainer);
	}

	/**
	 * @return
	 */
	public Set<UIStructuralContainer> getParentTabContainers() {
		Set<UIStructuralContainer> result = new LinkedHashSet<>();
        for (UIContainer parentContainer : getParentContainers()) {
			if (parentContainer instanceof UIStructuralContainer && ((UIStructuralContainer)parentContainer).getType() == StructuralContainerType.TAB) {
				result.add((UIStructuralContainer) parentContainer);
			}
		}
		return result;
	}

	/**
	 * @param mainContainer
	 * @return
	 */
	public Set<UIStructuralContainer> getAllParentTabContainers(UIStructuralContainer mainContainer) {
		if (mainContainer == null) throw new NullPointerException("parameter 'mainContainer' must not be null");

		Set<UIStructuralContainer> result = new LinkedHashSet<>();

		result.addAll(collectAllParentTabContainers(mainContainer));
		return result;
	}

	/**
	 * @param mainContainer
	 * @return
	 */
	protected Set<UIStructuralContainer> collectAllParentTabContainers(UIStructuralContainer mainContainer) {
		if (mainContainer == null) throw new NullPointerException("parameter 'mainContainer' must not be null");

		Set<UIStructuralContainer> intermediateResult = null;

		for (UIContainer parentContainer : getParentContainers()) {
			if (parentContainer instanceof UIStructuralContainer && ((UIStructuralContainer)parentContainer).isMainContainer()) {
				// reached the root of a container tree
				if (parentContainer == mainContainer) {
					// reached the start of the container tree
					intermediateResult = new LinkedHashSet<>();
				}
			} else {
				// need to go further up
				intermediateResult = parentContainer.collectAllParentTabContainers(mainContainer);
				if (intermediateResult != null) {
					if (parentContainer instanceof UIStructuralContainer && ((UIStructuralContainer)parentContainer).getType() == StructuralContainerType.TAB) {
					    intermediateResult.add((UIStructuralContainer) parentContainer);
					}
					break;
				}
			}
		}

		return intermediateResult;
	}

	/**
	 * @return the scrollable
	 */
	public boolean isScrollable() {
		return scrollable;
	}

	/**
	 * @param scrollable the scrollable to set
	 */
	public void setScrollable(boolean scrollable) {
		this.scrollable = scrollable;
	}
	
	/**
	 * @return all action containers, traversing all action containers' action containers (excluding this container if this container is an action container)
	 */
	public Set<UIActionContainer> getAllActionContainers() {
		Set<UIActionContainer> result = new LinkedHashSet<>();
		return collectAllActionContainers(result);
	}
	
	/**
	 * @param collectedActionContainers
	 * @return
	 */
	protected Set<UIActionContainer> collectAllActionContainers(Set<UIActionContainer> collectedActionContainers) {
		Set<UIActionContainer> actionContainers = new LinkedHashSet<>();

		actionContainers.addAll(this.actionContainers);


		for (UIActionContainer childActionContainer : actionContainers) {
			collectedActionContainers.add(childActionContainer);
			childActionContainer.collectAllActionContainers(collectedActionContainers);
		}
		
		return collectedActionContainers;
	}
	
	public List<UIContainer> getContainerBranch(UIContainer aContainer) {
		List<UIContainer> result = new ArrayList<>();
		result.add(this);
		if (!findMatchingBranch(result, this, aContainer)) {
			result.remove(this);
		}
		return result;
	}
	
	private boolean findMatchingBranch(List<UIContainer> branch, UIContainer currentContainer, UIContainer containerToSearchFor) {
		boolean found = false;
		
		if (currentContainer == containerToSearchFor) return true;  // found the matching branch
		
		if (currentContainer instanceof UIStructuralContainer) {
			UIStructuralContainer structuralContainer = (UIStructuralContainer) currentContainer;
			for (UIContainer childContainer : structuralContainer.getChildContainers()) {
				branch.add(childContainer);
				if (findMatchingBranch(branch, childContainer, containerToSearchFor)) {
					found = true;
				} else {
					branch.remove(childContainer);
				}
			}
		} else if (currentContainer instanceof UIActionContainer) {
			UIActionContainer actionContainer = (UIActionContainer) currentContainer;
			for (UIContainer childActionContainer : actionContainer.getActionContainers()) {
				branch.add(childActionContainer);
				if (findMatchingBranch(branch, childActionContainer, containerToSearchFor)) {
					found = true;
				} else {
					branch.remove(childActionContainer);
				}
			}
		} else if (currentContainer instanceof UIDataContainer) {
			UIDataContainer dataContainer = (UIDataContainer) currentContainer;
			// --- search for components that refer to an embedded container or an embedded container group
			for (UIComponent component : dataContainer.getComponents()) {
				if (component instanceof UIEmbeddedComponentGroup) {
					UIEmbeddedComponentGroup embeddedComponentGroup = (UIEmbeddedComponentGroup) component;
					branch.add(embeddedComponentGroup.getDataContainer());					
					if (findMatchingBranch(branch, embeddedComponentGroup.getDataContainer(), containerToSearchFor)) {
						found = true;
					} else {
						branch.remove(embeddedComponentGroup.getDataContainer());
					}
				} else if (component instanceof UIEmbeddedContainer) {
					UIEmbeddedContainer embeddedContainer = (UIEmbeddedContainer) component;
					branch.add(embeddedContainer.getContainer());					
					if (findMatchingBranch(branch, embeddedContainer.getContainer(), containerToSearchFor)) {
						found = true;
					} else {
						branch.remove(embeddedContainer.getContainer());
					}
				}
			}
		}
		
		return found;
	}

	public boolean isOptional() {
		return optional;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

	public boolean isDisablable() {
		return disablable;
	}

	public void setDisablable(boolean disablable) {
		this.disablable = disablable;
	}

	public Set<FunctionModule> getConsumedFunctionModules() {
		return consumedFunctionModules;
	}
	
	public boolean addConsumedFunctionModule(FunctionModule functionModule) {
		return this.consumedFunctionModules.add(functionModule);
	}

	public Set<Entity> getConsumedEntities() {
		return consumedEntities;
	}
	
	public boolean addConsumedEntity(Entity entity) {
		return this.consumedEntities.add(entity);
	}

	/**
	 * 
	 * @return a type that holds configuration values that are used to parameterize a container
	 */
	public ComplexType getConfigurationItems() {
		return configurationItems;
	}

	public void setConfigurationItems(ComplexType configurationItems) {
		this.configurationItems = configurationItems;
	}

	/**
	 * @return a type that holds init parameters for this container, passed from a different part of an application  
	 */
	public ComplexType getInitParameters() {
		return initParameters;
	}

	public void setInitParameters(ComplexType initParameters) {
		this.initParameters = initParameters;
	}

	public Enumeration getLayoutVariants() {
		return layoutVariants;
	}

	public void setLayoutVariants(Enumeration layoutVariants) {
		this.layoutVariants = layoutVariants;
	}
	
	/**
	 * A ui container can use one or more service interfaces.
	 * At runtime of a generated application a decision is taken
	 * on which service implementations to use that matches the given
	 * service interfaces.
	 * 
	 * @return
	 */
	public Set<ServiceInterface> getConsumedServiceInterfaces() {
		return Collections.unmodifiableSet(consumedServiceInterfaces);
	}
	
	public boolean addConsumedServiceInterface(ServiceInterface serviceInterface) {
		return this.consumedServiceInterfaces.add(serviceInterface);
	}

	public boolean isDynamicStructure() {
		return dynamicStructure;
	}

	public void setDynamicStructure(boolean dynamicStructure) {
		this.dynamicStructure = dynamicStructure;
	}
	
	public ComplexType getTestPreludeParameters() {
		return testPreludeParameters;
	}

	public void setTestPreludeParameters(ComplexType testPreludeParameters) {
		this.testPreludeParameters = testPreludeParameters;
	}

	public Set<UIStructuralContainer> getTestPreludeViews() {
		return testPreludeViews;
	}
	
	public boolean addTestPreludeView(UIStructuralContainer uiStructuralContainer) {
		return this.testPreludeViews.add(uiStructuralContainer);
	}

	public Map<UIActionContainer,Placement> getActionContainerPlacement() {
		return actionContainerPlacement;
	}

	/**
	 * @return the messageKeys
	 */
	public Set<String> getMessageKeys() {
		return messageKeys;
	}

	/**
	 * @return the messageKeysForWarnings
	 */
	public Set<String> getMessageKeysForWarnings() {
		return messageKeysForWarnings;
	}

	/**
	 * @return the messageKeysForErrors
	 */
	public Set<String> getMessageKeysForErrors() {
		return messageKeysForErrors;
	}
	
	/**
	 * This method tells the generator code, whether the container requires a lot of space to be displayed.
	 * 
	 * <p>When web pages are generated, charts normally require a large width to be displayed.
	 * In such a case, the generated code should not pose any limits to the available width
	 * for the component to be displayed.
	 * 
	 * @return
	 */
	public boolean isLargeWidthRequired() {
		return false;
	}
}
