/**
 *
 */
package com.gs.gapp.metamodel.ui.container;

/**
 * @author mmt
 *
 */
public class UIMenuContainer extends UIActionContainer {
	
	private static final long serialVersionUID = 3723342647495329980L;

	
	/**
	 * @param name
	 */
	public UIMenuContainer(String name) {
		super(name);
	}
}
