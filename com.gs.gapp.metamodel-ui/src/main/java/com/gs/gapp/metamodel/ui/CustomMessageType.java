package com.gs.gapp.metamodel.ui;

/**
 * types of messages that optionally are not handled by a framework's/technology's default message handling mechanism
 * 
 * @author marcu
 *
 */
public enum CustomMessageType {
	
	REQUIRED,
	CONVERSION,
	VALIDATION,
	;
}
