/**
 *
 */
package com.gs.gapp.metamodel.ui.container;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author mmt
 *
 */
public class UIDockingContainer extends UIStructuralContainer {



	/**
	 *
	 */
	private static final long serialVersionUID = 5286452091268667781L;

	private final Set<UIDockableContainer> dockableContainers = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public UIDockingContainer(String name) {
		super(name);
	}

	/**
	 * @return the actionComponents
	 */
	public Set<UIDockableContainer> getDockableContainers() {
		return dockableContainers;
	}

	/**
	 * @param container
	 */
	public void addDockableContainer(UIDockableContainer container) {
		this.dockableContainers.add(container);
	}

}
