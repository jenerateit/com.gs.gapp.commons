/**
 *
 */
package com.gs.gapp.metamodel.ui.component;


/**
 * @author mmt
 *
 */
public class UIFileUpload extends UIComponent {

	private static final long serialVersionUID = 3244254353826241729L;

	/**
	 * @param name
	 */
	public UIFileUpload(String name) {
		super(name);
	}
}
