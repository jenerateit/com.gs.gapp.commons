/**
 *
 */
package com.gs.gapp.metamodel.ui.container;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs.gapp.metamodel.basic.Orientation;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.ui.ContainerFlow;
import com.gs.gapp.metamodel.ui.ContainerFlowType;
import com.gs.gapp.metamodel.ui.component.UIActionComponent;
import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * @author mmt
 *
 */
public class UIStructuralContainer extends UIContainer {

	/**
	 *
	 */
	private static final long serialVersionUID = -3885958620186980955L;

	private final Set<UIContainer> childContainers = new LinkedHashSet<>();

	private StructuralContainerType type = StructuralContainerType.SPLIT;

	private Orientation orientation = Orientation.TOP_TO_BOTTOM;
	
	private Boolean customTitle;
	
	/**
	 * @param name
	 */
	public UIStructuralContainer(String name) {
		super(name);
	}

	/**
	 * @return the childcontainers
	 */
	public Set<UIContainer> getChildContainers() {
		return childContainers;
	}

	/**
	 * @return
	 */
	public Set<UIDataContainer> getChildDataContainers() {
		Set<UIDataContainer> result = new LinkedHashSet<>();
		
		// TODO add embedded containers?
		
		for (UIContainer childContainer : getChildContainers()) {
			if (childContainer instanceof UIDataContainer) {
				UIDataContainer dataContainer = (UIDataContainer) childContainer;
				result.add(dataContainer);
			}
		}

		return result;
	}
	
	public UIStructuralContainer getStructuralRootContainer() {
		UIContainer rootContainer = getRootContainer();
		if (rootContainer instanceof UIStructuralContainer) {
			return (UIStructuralContainer) rootContainer;
		}
			
		return this;
	}

	/**
	 * @param container
	 * @return
	 */
	public boolean addChildContainer(UIContainer container) {
		container.addParentContainer(this);
		return this.childContainers.add(container);
	}

	/**
	 * @return the type
	 */
	public StructuralContainerType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(StructuralContainerType type) {
		this.type = type;
	}

	/**
	 * @return the orientation
	 */
	public Orientation getOrientation() {
		return orientation;
	}

	/**
	 * @param orientation the orientation to set
	 */
	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}

	/**
	 * @return all child containers, including containers that are the target of a flow and including this container
	 */
	public Set<UIContainer> getAllContainers() {
		Set<UIContainer> result = new LinkedHashSet<>();

		result.add(this);
		for (UIContainer childContainer : getAllChildContainers()) {
			result.add(childContainer);
			for (ContainerFlow flow : childContainer.getFlows()) {
				if (childContainer != flow.getTarget()) {
					if (flow.getTarget() instanceof UIStructuralContainer) {
						UIStructuralContainer structuralContainer = (UIStructuralContainer) flow.getTarget();
						result.addAll(structuralContainer.getAllContainers());
					} else if (flow.getTarget() instanceof UIDataContainer) {
						UIDataContainer dataContainer = (UIDataContainer) flow.getTarget();
						result.addAll(dataContainer.getAllContainers());
					}
				}
			}
		}
		return result;
	}

	/**
	 * @return
	 */
	public Set<UIContainer> getAllChildContainers() {
		return getAllChildContainers(true);
	}
	
	/**
	 * @return all child containers (excluding this container), including containers that are the target of a flow of type REPLACE
	 */
	public Set<UIContainer> getAllChildContainers(boolean includeEmbeddedContainers) {
		Set<UIContainer> result = new LinkedHashSet<>();
		return collectAllChildContainers(result, includeEmbeddedContainers);
	}
	
	public List<UIContainer> getAllChildContainersWithDuplicates() {
		return getAllChildContainersWithDuplicates(true);
	}
	
	/**
	 * @return all child containers (excluding this container), including containers that are the target of a flow of type REPLACE, including duplicates
	 */
	public List<UIContainer> getAllChildContainersWithDuplicates(boolean includeEmbeddedContainers) {
		List<UIContainer> result = new ArrayList<>();
		return collectAllChildContainersWithDuplicates(result, includeEmbeddedContainers);
	}
	
	/**
	 * @return
	 */
	public Set<UIActionComponent> getAllActionComponentsOfAllChildContainers() {
		Set<UIActionComponent> result = new LinkedHashSet<>();
		
		Set<UIActionContainer> actionContainers = new LinkedHashSet<>();
		
		for (UIContainer aChildContainer : getAllChildContainers()) {
			if (aChildContainer instanceof UIStructuralContainer) {
				UIStructuralContainer uiStructuralContainer = (UIStructuralContainer) aChildContainer;
				actionContainers.addAll(uiStructuralContainer.getActionContainers());
			} else if (aChildContainer instanceof UIActionContainer) {
				UIActionContainer uiActionContainer = (UIActionContainer) aChildContainer;
				actionContainers.add(uiActionContainer);
				actionContainers.addAll(uiActionContainer.getActionContainers());
			} else if (aChildContainer instanceof UIDataContainer) {
				UIDataContainer uiDataContainer = (UIDataContainer) aChildContainer;
				result.addAll(uiDataContainer.getActionComponents());
				actionContainers.addAll(uiDataContainer.getActionContainers());
			}
		}
		
		for (UIActionContainer actionContainer : actionContainers) {
		    result.addAll(actionContainer.getAllActionComponents());  // TODO handle further action containers that are child containers of action containers
		}
		
		return result;
	}
	
	
	/**
	 * @param containers
	 * @return
	 */
	protected Set<UIContainer> collectAllChildContainers(Set<UIContainer> containers) {
		return collectAllChildContainers(containers, true);
	}
	
	/**
	 * @param containers
	 * @return
	 */
	protected Set<UIContainer> collectAllChildContainers(Set<UIContainer> containers, boolean includeEmbeddedContainers) {
		Set<UIContainer> childContainersAndReplaceFlowContainers = new LinkedHashSet<>();

		childContainersAndReplaceFlowContainers.addAll(childContainers);
		for (ContainerFlow flow : getFlows()) {
			if (flow.getType() == ContainerFlowType.REPLACE && flow.getTarget() != this) {
				childContainersAndReplaceFlowContainers.add(flow.getTarget());
			}
		}

		for (UIContainer childContainer : childContainersAndReplaceFlowContainers) {
			containers.add(childContainer);
			if (childContainer instanceof UIDataContainer) {
				UIDataContainer dataContainer = (UIDataContainer) childContainer;
				containers.addAll(dataContainer.getAllChildContainers(includeEmbeddedContainers));
			} else if (childContainer instanceof UIStructuralContainer) {
				((UIStructuralContainer)childContainer).collectAllChildContainers(containers, includeEmbeddedContainers);
			}
		}
		return containers;
	}
	
	/**
	 * @param containers
	 * @return
	 */
	protected List<UIContainer> collectAllChildContainersWithDuplicates(List<UIContainer> containers) {
		return collectAllChildContainersWithDuplicates(containers, true);
	}
	
	/**
	 * Same logic as {@link #collectAllChildContainers(Set, boolean)} with the exception that a List is used.
	 * Due to this we can identify multiple occurencies of containers.
	 * 
	 * @param containers
	 * @return
	 */
	protected List<UIContainer> collectAllChildContainersWithDuplicates(List<UIContainer> containers, boolean includeEmbeddedContainers) {
		List<UIContainer> childContainersAndReplaceFlowContainers = new ArrayList<>();

		childContainersAndReplaceFlowContainers.addAll(childContainers);
		for (ContainerFlow flow : getFlows()) {
			if (flow.getType() == ContainerFlowType.REPLACE && flow.getTarget() != this) {
				childContainersAndReplaceFlowContainers.add(flow.getTarget());
			}
		}

		for (UIContainer childContainer : childContainersAndReplaceFlowContainers) {
			containers.add(childContainer);
			if (childContainer instanceof UIDataContainer) {
				UIDataContainer dataContainer = (UIDataContainer) childContainer;
				containers.addAll(dataContainer.getAllChildContainers(includeEmbeddedContainers));
			} else if (childContainer instanceof UIStructuralContainer) {
				((UIStructuralContainer)childContainer).collectAllChildContainersWithDuplicates(containers, includeEmbeddedContainers);
			}
		}
		return containers;
	}

	/**
	 * @return
	 */
	public Set<UIDataContainer> getAllChildEmbeddedDataContainers() {
		Set<UIDataContainer> allChildDataContainers = getAllChildDataContainers(true);
		Set<UIDataContainer> allChildDataContainersExcludingEmbeddedContainers= getAllChildDataContainers(false);
		
		allChildDataContainers.removeAll(allChildDataContainersExcludingEmbeddedContainers);
		return allChildDataContainers;
	}
	
	/**
	 * @param includeEmbeddedContainers
	 * @return
	 */
	public Set<UIDataContainer> getAllChildDataContainers(boolean includeEmbeddedContainers) {
		Set<UIDataContainer> result = new LinkedHashSet<>();
		for (UIContainer childContainer : getAllChildContainers(includeEmbeddedContainers)) {
			if (childContainer instanceof UIDataContainer) {
				result.add((UIDataContainer) childContainer);
			}
		}

		return result;
	}
	
	/**
	 * Collects all data containers that are part of the container tree
	 * that starts with this structural container.
	 *
	 * @return
	 */
	public Set<UIDataContainer> getAllChildDataContainers() {
		return getAllChildDataContainers(true);
	}
	
	/**
	 * @return
	 */
	public Set<UIStructuralContainer> getAllChildStructuralContainers(boolean inclusiveThis) {
		Set<UIStructuralContainer> result = new LinkedHashSet<>();
		if (inclusiveThis) result.add(this);
		result.addAll( getAllChildStructuralContainers() );

		return result;
	}
	
	/**
	 * @return
	 */
	public Set<UIStructuralContainer> getChildStructuralContainers() {
		Set<UIStructuralContainer> result = new LinkedHashSet<>();
		for (UIContainer childContainer : getChildContainers()) {
			if (childContainer instanceof UIStructuralContainer) {
				result.add((UIStructuralContainer) childContainer);
			}
		}

		return result;
	}
	
	/**
	 * @return
	 */
	public Set<UIStructuralContainer> getAllChildStructuralContainers() {
		Set<UIStructuralContainer> result = new LinkedHashSet<>();
		for (UIContainer childContainer : getAllChildContainers()) {
			if (childContainer instanceof UIStructuralContainer) {
				result.add((UIStructuralContainer) childContainer);
			}
		}

		return result;
	}

	/**
	 * @return
	 */
	public Set<UIDataContainer> getAllDataContainers() {
		Set<UIDataContainer> result = new LinkedHashSet<>();
		for (UIContainer container : getAllContainers()) {
			if (container instanceof UIDataContainer) {
				result.add((UIDataContainer) container);
			}
		}

		return result;
	}
	
	/**
	 * Collect all containers with the same name in order to find out duplicates.
	 * Duplicate container names affect the uniqueness of field and method names
	 * in code generation. With this method you can find out such potential ambiguous cases.
	 * 
	 * Note that the resulting list might contain one and the same container several times.
	 * If this is the case, it means that this container is used more than once in the page.
	 * 
	 * @param container
	 * @return
	 */
	public List<UIContainer> getAllContainersWithSameName(UIContainer container) {
		List<UIContainer> result = new ArrayList<>();
		for (UIContainer aContainer : getAllChildContainersWithDuplicates()) {
			if (container.getTechnicalName().equals(aContainer.getTechnicalName())) {
				result.add(aContainer);
			}
			for (UIContainer anActionContainer : aContainer.getAllActionContainers()) {
				if (container.getTechnicalName().equals(anActionContainer.getTechnicalName())) {
					result.add(anActionContainer);
				}
			}
		}
		
		return result;
	}

	/**
	 * @return
	 */
	public boolean isMainContainer() {
		boolean result = getParentContainers().size() == 0;

		return result;
	}

	/**
	 * @param childContainer
	 * @return
	 */
	public int getTabIndex(UIContainer childContainer) {
		int index = -1;
		int ii=0;

		for (UIContainer aChildContainer : childContainers) {
			if (aChildContainer == childContainer) {
				index = ii;
				break;
			}

			if (aChildContainer instanceof UIStructuralContainer) {
				UIStructuralContainer structuralContainer = (UIStructuralContainer) aChildContainer;
				if (structuralContainer.getAllChildContainers().contains(childContainer)) {
					index = ii;
					break;
				}
			}
			ii++;
		}

		return index;
	}

	/**
	 * @return
	 */
	public Set<ContainerFlow> getAllChildContainerFlows() {
		Set<ContainerFlow> result = new LinkedHashSet<>();
		for (UIContainer container : getAllChildContainers()) {
			result.addAll(container.getFlows());
		}
		return result;
	}

	/**
	 * @return
	 */
	public Set<ContainerFlow> getAllContainerFlows() {
		Set<ContainerFlow> result = new LinkedHashSet<>();
		for (UIContainer container : getAllContainers()) {
			result.addAll(container.getFlows());
		}
		return result;
	}

	/**
	 * @param actionComponent
	 * @return
	 */
	public Set<ContainerFlow> getAllChildContainerFlows(UIActionComponent actionComponent) {
		Set<ContainerFlow> result = new LinkedHashSet<>();
		for (ContainerFlow containerFlow : getAllChildContainerFlows()) {
			if (containerFlow.getActionComponents().contains(actionComponent)) {
				result.add(containerFlow);
			}
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<UIComponent> getComponentsWithCustomMessages() {
		LinkedHashSet<UIComponent> componentsWithCustomMessages = new LinkedHashSet<>();
		for (UIDataContainer dataContainer : getAllChildDataContainers()) {
			componentsWithCustomMessages.addAll(dataContainer.getComponentsWithCustomMessages());
		}
		
		return componentsWithCustomMessages;
	}
	
	/**
	 * Collect all entities that are used in all data containers
	 * of this structural container.
	 * 
	 * @return
	 */
	public Set<Entity> getEntities() {
		Set<Entity> result = new LinkedHashSet<>();
	
		for (UIDataContainer dataContainer : getAllDataContainers()) {
			result.addAll(dataContainer.getEntities());
		}
		
		return result;
	}

	public Boolean getCustomTitle() {
		return customTitle;
	}

	public void setCustomTitle(Boolean customTitle) {
		this.customTitle = customTitle;
	}
	
	@Override
	public boolean isLargeWidthRequired() {
		if (this.getType() == StructuralContainerType.DASHBOARD ||
				getType() == StructuralContainerType.TAB ||
				getType() == StructuralContainerType.WIZARD) {
			return true;
		}
		
		return super.isLargeWidthRequired();
	}
	
	/**
	 * @return
	 */
	public List<UIContainer> getChildContainersThatRequireLargeWidth() {
		Set<UIContainer> containers = getAllChildContainers(true);
		containers.add(this);
		List<UIContainer> result = containers.stream()
			.filter(container -> container.isLargeWidthRequired())
			.collect(Collectors.toList());
    	return result;
    }
}
