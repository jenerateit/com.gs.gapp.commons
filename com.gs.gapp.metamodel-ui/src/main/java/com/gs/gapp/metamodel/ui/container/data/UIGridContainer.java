/**
 *
 */
package com.gs.gapp.metamodel.ui.container.data;

import com.gs.gapp.metamodel.ui.component.UIComponent;

/**
 * @author mmt
 *
 */
public class UIGridContainer extends UIDataContainer implements PageableContainer {

	private static final long serialVersionUID = -2670019358815986974L;
	
	private Integer numberOfRowsPerPage;
	
	/**
	 * @param name
	 */
	public UIGridContainer(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.ui.container.UIContainer#isScrollable()
	 */
	@Override
	public boolean isScrollable() {
		return false;
	}

	/**
	 * @return true if at least one of the list's components is _not_ readOnly 
	 */
	public boolean isEditable() {
		for (UIComponent component : getComponents()) {
			if (component.isReadOnly() == false) {
				return true;
			}
		}
		
		return false;
	}

	public Integer getNumberOfRowsPerPage() {
		return numberOfRowsPerPage;
	}

	public void setNumberOfRowsPerPage(Integer numberOfRowsPerPage) {
		this.numberOfRowsPerPage = numberOfRowsPerPage;
	}
}
