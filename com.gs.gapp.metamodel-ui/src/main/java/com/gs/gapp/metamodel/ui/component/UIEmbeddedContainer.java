/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.gs.gapp.metamodel.ui.ContainerFlow;
import com.gs.gapp.metamodel.ui.container.UIContainer;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * @author mmt
 *
 */
public class UIEmbeddedContainer extends UIComponent {

	/**
	 *
	 */
	private static final long serialVersionUID = -4628620746279275422L;

	private UIContainer container;

	/**
	 * @param name
	 */
	public UIEmbeddedContainer(String name) {
		super(name);
	}

	/**
	 * @return the container
	 */
	public UIContainer getContainer() {
		return container;
	}

	/**
	 * @param container the container to set
	 */
	public void setContainer(UIContainer container) {
		this.container = container;
		container.addEmbeddingComponent(this);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.ui.component.UIComponent#isDispatchingEvents()
	 */
	@Override
	public boolean isDispatchingEvents() { return false; }
	
	

	@Override
	public boolean isLargeWidthRequired() {
		return true;
	}

	/**
	 * @return
	 */
	public Set<UIContainer> getAllChildContainers() {
		Set<UIContainer> result = new LinkedHashSet<>();
		return collectAllChildContainers(result);
	}
	
	/**
	 * @return
	 */
	public List<UIContainer> getAllChildContainersWithDuplicates() {
		List<UIContainer> result = new ArrayList<>();
		return collectAllChildContainersWithDuplicates(result);
	}

    /**
     * @param containers
     * @return
     */
    protected Set<UIContainer> collectAllChildContainers(Set<UIContainer> containers) {

		if (container instanceof UIStructuralContainer) {
			UIStructuralContainer structuralContainer = (UIStructuralContainer) container;
			containers.addAll(structuralContainer.getAllChildContainers());
		} else if (container instanceof UIDataContainer) {
			UIDataContainer dataContainer = (UIDataContainer) container;
			containers.add(dataContainer);
			for (UIComponent component : dataContainer.getComponents()) {
				if (component instanceof UIEmbeddedContainer) {
					UIEmbeddedContainer embeddedContainer = (UIEmbeddedContainer) component;
					containers.addAll(embeddedContainer.getAllChildContainers());
				}
			}
		}

		return containers;
	}
    
    /**
     * @param containers
     * @return
     */
    protected List<UIContainer> collectAllChildContainersWithDuplicates(List<UIContainer> containers) {

		if (container instanceof UIStructuralContainer) {
			UIStructuralContainer structuralContainer = (UIStructuralContainer) container;
			containers.addAll(structuralContainer.getAllChildContainersWithDuplicates());
		} else if (container instanceof UIDataContainer) {
			UIDataContainer dataContainer = (UIDataContainer) container;
			containers.add(dataContainer);
			for (UIComponent component : dataContainer.getComponents()) {
				if (component instanceof UIEmbeddedContainer) {
					UIEmbeddedContainer embeddedContainer = (UIEmbeddedContainer) component;
					containers.addAll(embeddedContainer.getAllChildContainersWithDuplicates());
				}
			}
		}

		return containers;
	}

	/**
	 * @return
	 */
	public Set<UIDataContainer> getAllDataContainers() {
		Set<UIDataContainer> result = new LinkedHashSet<>();
		return collectAllDataContainers(result);
	}

	/**
	 * @param dataContainers
	 * @return
	 */
	protected Set<UIDataContainer> collectAllDataContainers(Set<UIDataContainer> dataContainers) {

		if (container instanceof UIStructuralContainer) {
			UIStructuralContainer structuralContainer = (UIStructuralContainer) container;
			dataContainers.addAll(structuralContainer.getAllDataContainers());
		} else if (container instanceof UIDataContainer) {
			UIDataContainer dataContainer = (UIDataContainer) container;
			for (UIComponent component : dataContainer.getComponents()) {
				if (component instanceof UIEmbeddedContainer) {
					UIEmbeddedContainer embeddedContainer = (UIEmbeddedContainer) component;
					dataContainers.addAll(embeddedContainer.getAllDataContainers());
				}
			}
		}

		return dataContainers;
	}

	/**
	 * @return
	 */
	public Set<ContainerFlow> getAllContainerFlows() {
		Set<ContainerFlow> result = new LinkedHashSet<>();

		if (container instanceof UIStructuralContainer) {
			UIStructuralContainer structuralContainer = (UIStructuralContainer) container;
			result.addAll(structuralContainer.getAllChildContainerFlows());
		} else if (container instanceof UIDataContainer) {
			UIDataContainer dataContainer = (UIDataContainer) container;
			for (UIComponent component : dataContainer.getComponents()) {
				if (component instanceof UIEmbeddedContainer) {
					UIEmbeddedContainer embeddedContainer = (UIEmbeddedContainer) component;
					result.addAll(embeddedContainer.getAllContainerFlows());
				}
			}
		}

		return result;
	}
}
