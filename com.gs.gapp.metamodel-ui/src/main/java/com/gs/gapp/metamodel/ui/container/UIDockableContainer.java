/**
 *
 */
package com.gs.gapp.metamodel.ui.container;

import com.gs.gapp.metamodel.basic.Placement;

/**
 * @author mmt
 *
 */
public class UIDockableContainer extends UIStructuralContainer {


	/**
	 *
	 */
	private static final long serialVersionUID = -7964026002228483074L;

	private Placement placement = Placement.TOP;

	/**
	 * @param name
	 */
	public UIDockableContainer(String name) {
		super(name);
	}

	/**
	 * @return the placement
	 */
	public Placement getPlacement() {
		return placement;
	}

	/**
	 * @param placement the placement to set
	 */
	public void setPlacement(Placement placement) {
		this.placement = placement;
	}
}
