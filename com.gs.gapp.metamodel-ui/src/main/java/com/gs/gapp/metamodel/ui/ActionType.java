package com.gs.gapp.metamodel.ui;

import com.gs.gapp.metamodel.ActionTypeI;

/**
 * This enum nominates action types to be used
 * for ui testing. One way of using this information
 * is to generate means to hold input data for tests
 * and data for assertions.
 * 
 * @author marcu
 *
 */
public enum ActionType implements ActionTypeI {

	/**
	 * Select a row in a table or select an entry in a list.
	 * Typically, this is mapped to a natural number representing
	 * the index of the table or list.
	 */
	SELECT,
	
    /**
     * Enter data into an input field. The input field can be used for
     * any data type: text, number, date, ....
     */
    ENTER,
    
    /**
     * Click on an action component, e.g. a button or a link.
     * Typically, this is mapped to a boolean field (true = click it).
     */
    CLICK,
    
    /**
     * Navigate to a different view. The navigation is not being done
     * by clicking on a component since we use the {@link ActionType#CLICK} enum entry
     * for this purpose. 
     */
    NAVIGATE,
    
    /**
     * Checking, whether a view is currently active.
     */
    IS_PAGE_PRESENT,
    
    /**
     * Checking, whether a component is part of the currently active view.
     */
    IS_PRESENT,
    
    /**
     * Checking, whether the value of the component is the correct one.
     */
    IS_VALUE_EQUAL,
    
    /**
     * Checking, whether the value of the component is not set.
     */
    IS_VALUE_NULL,
    
    /**
     * Checking, whether the value of the component _is_ set.
     */
    IS_VALUE_NOT_NULL,
    
    /**
     * Checking, whether the textual value of the component is _not_ empty.
     */
    IS_TEXT_NOT_EMPTY,
    
    /**
     * Checking, whether the textual value of the component is empty.
     */
    IS_TEXT_EMPTY,
    
    /**
     * Checking, whether the textual value of the component is the correct one.
     */
    IS_TEXT_EQUAL,
    
    /**
     * Checking, whether an informational message is being displayed on the view.
     */
    IS_INFO_MESSAGE,
    
    /**
     * Checking, whether a warning message is being displayed on the view.
     */
    IS_WARNING_MESSAGE,
    
    /**
     * Checking, whether an error message is being displayed on the view.
     */
    IS_ERROR_MESSAGE,
    
    /**
     * This type is being used for inputs/checks that are not yet part of the
     * rest of the set of action types.
     */
    CUSTOM,
    ;

	@Override
	public String getName() {
		return this.name();
	}
}
