/**
 *
 */
package com.gs.gapp.metamodel.ui;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.ui.component.UIActionComponent;
import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.container.UIContainer;


/**
 * @author mmt
 *
 */
public class ContainerFlow extends ModelElement {

	/**
	 *
	 */
	private static final long serialVersionUID = 2310392856850323563L;

	private UIContainer source;
	private UIContainer target;
	private ContainerFlowType type;

	private final Set<UIActionComponent> actionComponents = new LinkedHashSet<>();
	
	private final Set<UIComponent> components = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public ContainerFlow(String name) {
		super(name);
	}

	/**
	 * @return the actionComponents
	 */
	public Set<UIActionComponent> getActionComponents() {
		return actionComponents;
	}

	/**
	 * @param component
	 */
	public boolean addActionComponent(UIActionComponent component) {
		if (component == null) throw new NullPointerException("param 'component' must not be null");
		return this.actionComponents.add(component);
	}
	
	/**
	 * @return the components
	 */
	public Set<UIComponent> getComponents() {
		return components;
	}

	/**
	 * @param component
	 */
	public boolean addComponent(UIComponent component) {
		if (component == null) throw new NullPointerException("param 'component' must not be null");
		return this.components.add(component);
	}

	/**
	 * @return the source
	 */
	public UIContainer getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(UIContainer source) {
		if (source != null) source.addFlow(this);
		this.source = source;
	}

	/**
	 * @return the target
	 */
	public UIContainer getTarget() {
		return target;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(UIContainer target) {
		if (target != null) {
			target.addFlow(this);
		}
		this.target = target;
	}

	/**
	 * @return the type
	 */
	public ContainerFlowType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(ContainerFlowType type) {
		this.type = type;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ContainerFlow [source=" + source + ", target=" + target
				+ ", type=" + type + "]";
	}
}
