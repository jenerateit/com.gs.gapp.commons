/**
 *
 */
package com.gs.gapp.metamodel.ui.component;


/**
 * @author mmt
 *
 */
public class UICustomComponent extends UIComponent {



	/**
	 *
	 */
	private static final long serialVersionUID = 6018369459316162833L;

	/**
	 * @param name
	 */
	public UICustomComponent(String name) {
		super(name);
	}
}
