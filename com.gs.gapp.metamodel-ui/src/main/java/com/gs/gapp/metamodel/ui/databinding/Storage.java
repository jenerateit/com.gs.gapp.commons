/**
 *
 */
package com.gs.gapp.metamodel.ui.databinding;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

/**
 * @author mmt
 *
 */
public abstract class Storage extends ModelElement {

	private static final long serialVersionUID = 1533762616203653476L;
	
	private final Set<PersistenceModule> persistenceModules = new LinkedHashSet<>();
	
	private final Set<Entity> entities = new LinkedHashSet<>();
	
	private final Set<EntityField> entityFields = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public Storage(String name) {
		super(name);
	}

	/**
	 * @return the persistenceModules
	 */
	public Set<PersistenceModule> getPersistenceModules() {
		return persistenceModules;
	}

	/**
	 * @param persistenceModule
	 * @return
	 */
	public boolean addPersistenceModule(PersistenceModule persistenceModule) {
		return this.persistenceModules.add(persistenceModule);
	}
	
	/**
	 * @return the entities
	 */
	public Set<Entity> getEntities() {
		return entities;
	}

	/**
	 * @param entity
	 * @return
	 */
	public boolean addEntity(Entity entity) {
		return this.entities.add(entity);
	}
	
	/**
	 * @return the entityFields
	 */
	public Set<EntityField> getEntityFields() {
		return entityFields;
	}

	/**
	 * @param entityField
	 * @return
	 */
	public boolean addEntityField(EntityField entityField) {
		return this.entityFields.add(entityField);
	}
}
