/**
 *
 */
package com.gs.gapp.metamodel.ui.container;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * A view container represents a complete view (aka 'screen' or 'page').
 * A view container won't be re-used anywhere else.
 * 
 * @author mmt
 *
 */
public class UIViewContainer extends UIStructuralContainer {
	
	private static final long serialVersionUID = 3723342647495329980L;

	private final Set<UIMenuContainer> viewMenus = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public UIViewContainer(String name) {
		super(name);
	}

	public Set<UIMenuContainer> getViewMenus() {
		return viewMenus;
	}
	
	public boolean addViewMenu(UIMenuContainer menuContainer) {
		return this.viewMenus.add(menuContainer);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.ui.container.UIStructuralContainer#isMainContainer()
	 */
	@Override
	public boolean isMainContainer() {
		return true;
	}
}
