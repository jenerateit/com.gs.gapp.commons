/**
 *
 */
package com.gs.gapp.metamodel.ui.component;


/**
 * @author mmt
 *
 */
public class UIBooleanChoice extends UIComponent {


	/**
	 *
	 */
	private static final long serialVersionUID = -2993534171859448289L;
	
	private Type type;

	/**
	 * @param name
	 */
	public UIBooleanChoice(String name) {
		super(name);
	}
	
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * @author mmt
	 *
	 */
	public static enum Type {
		BUTTON,
		CHECK,
		SWITCH,
		;
	}
}
