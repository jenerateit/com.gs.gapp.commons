/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

import com.gs.gapp.metamodel.basic.typesystem.Enumeration;

/**
 * @author mmt
 *
 */
public class UIImage extends UIComponent {

	private static final long serialVersionUID = 3244254353826241729L;
	
	private ImageType imageType;

	/**
	 * @param name
	 */
	public UIImage(String name) {
		super(name);
	}
	
	/**
	 * @return true if the image is mapped to an entity field and the field doesn't have an enumeration type
	 */
	public boolean hasDynamicContent() {
		return (this.getEntityField() != null && !(this.getEntityField().getType() instanceof Enumeration))
				||
			   this.imageType == ImageType.IMAGE;
	}
	
	/**
	 * @return the imageType
	 */
	public ImageType getImageType() {
		return imageType;
	}

	/**
	 * @param imageType the imageType to set
	 */
	public void setImageType(ImageType imageType) {
		this.imageType = imageType;
	}

	public enum ImageType {
		SVG,
		DATA_URI,
		IMAGE,
		ANY,
		;
	}
}
