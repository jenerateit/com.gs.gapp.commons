package com.gs.gapp.metamodel.ui.container.data;

public interface PageableContainer {
	
	Integer getNumberOfRowsPerPage();

	void setNumberOfRowsPerPage(Integer numberOfRowsPerPage);
}
