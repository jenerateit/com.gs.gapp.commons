/**
 * 
 */
package com.gs.gapp.metamodel.ui;

/**
 * @author mmt
 *
 */
public enum ContainerFlowType {

	OPEN,
	REPLACE,
	PUSH,
	;
}
