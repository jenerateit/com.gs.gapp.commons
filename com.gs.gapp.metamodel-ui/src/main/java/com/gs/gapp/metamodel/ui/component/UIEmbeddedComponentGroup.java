/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.Orientation;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * @author mmt
 *
 */
public class UIEmbeddedComponentGroup extends UIComponent {

	private static final long serialVersionUID = 1687024584590814177L;
	
	private UIDataContainer dataContainer;

	private final Set<UIComponent> components = new LinkedHashSet<>();

	private Orientation orientation;

	/**
	 * @param name
	 */
	public UIEmbeddedComponentGroup(String name) {
		super(name);
	}

	/**
	 * @return the components
	 */
	public Set<UIComponent> getComponents() {
		return components;
	}

	/**
	 * @param component must not be null
	 * @return
	 * @throws NullPointerException when parameter component is null
	 */
	public boolean addComponent(UIComponent component) {
		if (component == null) throw new NullPointerException("parameter 'component' must not be null");

		return components.add(component);
	}

	/**
	 * @return the orientation
	 */
	public Orientation getOrientation() {
		return orientation;
	}

	/**
	 * @param orientation the orientation to set
	 */
	public void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}

	public UIDataContainer getDataContainer() {
		return dataContainer;
	}

	public void setDataContainer(UIDataContainer dataContainer) {
		this.dataContainer = dataContainer;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.ui.component.UIComponent#isReadOnly()
	 */
	@Override
	public boolean isReadOnly() {
		for (UIComponent component : this.components) {
			if (component.isReadOnly() == false) {
				return false;
			}
		}
		
		if (this.dataContainer != null) {
			for (UIComponent component : this.dataContainer.getComponents()) {
				if (component.isReadOnly() == false) {
					return false;
				}
			}
		}
		
		return true;
	}
}
