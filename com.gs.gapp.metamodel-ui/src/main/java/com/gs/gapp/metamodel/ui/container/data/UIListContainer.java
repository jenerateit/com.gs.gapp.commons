/**
 *
 */
package com.gs.gapp.metamodel.ui.container.data;

import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.container.SelectionModeEnum;

/**
 * @author mmt
 *
 */
public class UIListContainer extends UIDataContainer implements SelectableContainer, PageableContainer {

	private static final long serialVersionUID = -2670019358815986974L;
	
	private SelectionModeEnum selectionMode;
	
	private Integer numberOfRowsPerPage;

	/**
	 * @param name
	 */
	public UIListContainer(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.ui.container.data.UIDataContainer#isDispatchingEvents()
	 */
	@Override
	public boolean isDispatchingEvents() {
		// a list container is supposed to always dispatch events (row-clicked/item-selected)
		return true;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.ui.container.UIContainer#isScrollable()
	 */
	@Override
	public boolean isScrollable() {
		return false;
	}

	public SelectionModeEnum getSelectionMode() {
		return selectionMode;
	}

	public void setSelectionMode(SelectionModeEnum selectionMode) {
		this.selectionMode = selectionMode;
	}
	
	/**
	 * @return true if at least one of the list's components is _not_ readOnly 
	 */
	public boolean isEditable() {
		for (UIComponent component : getComponents()) {
			if (component.isReadOnly() == false) {
				return true;
			}
		}
		
		return false;
	}

	public Integer getNumberOfRowsPerPage() {
		return numberOfRowsPerPage;
	}

	public void setNumberOfRowsPerPage(Integer numberOfRowsPerPage) {
		this.numberOfRowsPerPage = numberOfRowsPerPage;
	}
}
