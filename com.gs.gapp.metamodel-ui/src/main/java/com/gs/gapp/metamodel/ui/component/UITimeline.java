/**
 *
 */
package com.gs.gapp.metamodel.ui.component;


/**
 * @author mmt
 *
 */
public class UITimeline extends UIComponent {

	private static final long serialVersionUID = 3244254353826241729L;

	/**
	 * @param name
	 */
	public UITimeline(String name) {
		super(name);
	}
	
	@Override
	public boolean isLargeWidthRequired() {
		return true;
	}
}
