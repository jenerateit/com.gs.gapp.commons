/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

import com.gs.gapp.metamodel.basic.Multiplicity;
import com.gs.gapp.metamodel.ui.container.UIContainer;

/**
 * @author mmt
 *
 */
public class UIChoice extends UIComponent {

	/**
	 *
	 */
	private static final long serialVersionUID = -7878046924532708487L;

	private boolean editable = false;

	private UIContainer container;

	private Multiplicity multiplicity;

	private Type type;
	
	private boolean preselected = false;
	
	/**
	 * @param name
	 */
	public UIChoice(String name) {
		super(name);
	}

	/**
	 * @return the editable
	 */
	public boolean isEditable() {
		return editable;
	}

	/**
	 * @param editable the editable to set
	 */
	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the container
	 */
	public UIContainer getContainer() {
		return container;
	}

	/**
	 * @param container the container to set
	 */
	public void setContainer(UIContainer container) {
		this.container = container;
	}

	/**
	 * @return the multiplicity
	 */
	public Multiplicity getMultiplicity() {
		return multiplicity;
	}
	
	/**
	 * This method determines the multiplicity by taking choice type,
	 * entity mapping and modeled multiplicity into account.
	 * 
	 * Note that the method doesn't check whether or ensures that the
	 * returned multiplicity is the same as the one returned
	 * from {@link #getMultiplicity()}.
	 * 
	 * This method always returns a value (never null).
	 * 
	 * @return the derived multiplicity that should be used to take decisions in converters/writers
	 */
	public Multiplicity getDerivedMultiplicity() {
		Multiplicity result = Multiplicity.SINGLE_VALUED;
		
		if (this.type == UIChoice.Type.PICK_LIST) {
			result = Multiplicity.MULTI_VALUED;			
		} else if (getEntityField() != null) {
			if (getEntityField().getCollectionType() == null) {
				result = Multiplicity.SINGLE_VALUED;
			} else {
				result = Multiplicity.MULTI_VALUED;
			}
		} else if (this.multiplicity != null) {
			result = this.multiplicity;
		}
		
		return result;
	}

	/**
	 * @param multiplicity the multiplicity to set
	 */
	public void setMultiplicity(Multiplicity multiplicity) {
		this.multiplicity = multiplicity;
	}
	
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public boolean isPreselected() {
		return preselected;
	}

	public void setPreselected(boolean preselected) {
		this.preselected = preselected;
	}

	/**
	 * @author mmt
	 *
	 */
	public static enum Type {
		BUTTON,
		CHECK,
		DROP_DOWN,
		LIST,
		AUTO_COMPLETE,
		PICK_LIST,
		;
	}
}
