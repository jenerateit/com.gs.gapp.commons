/**
 *
 */
package com.gs.gapp.metamodel.ui.component;


/**
 * @author mmt
 *
 */
public class UIVideo extends UIComponent {



	/**
	 *
	 */
	private static final long serialVersionUID = 3244254353826241729L;

	/**
	 * @param name
	 */
	public UIVideo(String name) {
		super(name);
	}
	
	@Override
	public boolean isLargeWidthRequired() {
		return true;
	}
}
