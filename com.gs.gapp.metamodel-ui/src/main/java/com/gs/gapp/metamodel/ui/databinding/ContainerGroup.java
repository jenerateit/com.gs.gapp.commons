/**
 *
 */
package com.gs.gapp.metamodel.ui.databinding;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * With ContainerGroup you can define groups of containers that share the same entity for data handling.
 * 
 * @author mmt
 *
 */
public class ContainerGroup extends ModelElement {


	/**
	 *
	 */
	private static final long serialVersionUID = -6334961423907383849L;

	private final Set<UIDataContainer> dataContainers = new LinkedHashSet<>();
	private final Set<ContainerGroup> dependentContainerGroups = new LinkedHashSet<>();
	private final Set<ContainerGroup> containerGroupsDependingOn = new LinkedHashSet<>();
	private Entity entity;
	private Function function;

	/**
	 * @param name
	 */
	public ContainerGroup(String name) {
		super(name);
	}

	/**
	 * @return the dataContainers
	 */
	public Set<UIDataContainer> getDataContainers() {
		return dataContainers;
	}

	/**
	 * @param container
	 * @return
	 */
	public boolean addDataContainer(UIDataContainer container) {
		if (container == null) throw new NullPointerException("param 'container' must not be null");
		if (container != null) container.addGroup(this);
		return this.dataContainers.add(container);
	}

	/**
	 * @return the dependentContainerGroups
	 */
	public Set<ContainerGroup> getDependentContainerGroups() {
		return dependentContainerGroups;
	}

	/**
	 * @param group
	 * @return
	 */
	public boolean addDependentContainerGroup(ContainerGroup group) {
		if (group == null) throw new NullPointerException("param 'group' must not be null");
		return this.dependentContainerGroups.add(group);
	}

	/**
	 * @return the entity
	 */
	public Entity getEntity() {
		return entity;
	}

	/**
	 * @param entity the entity to set
	 */
	public void setEntity(Entity entity) {
		this.entity = entity;
	}

	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public Set<ContainerGroup> getContainerGroupsDependingOn() {
		return containerGroupsDependingOn;
	}
	
	/**
	 * @param group
	 * @return
	 */
	public boolean addContainerGroupDependingOn(ContainerGroup group) {
		if (group == null) throw new NullPointerException("param 'group' must not be null");
		return this.containerGroupsDependingOn.add(group);
	}
}
