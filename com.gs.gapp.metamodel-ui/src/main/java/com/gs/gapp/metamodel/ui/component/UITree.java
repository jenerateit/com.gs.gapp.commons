/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

import com.gs.gapp.metamodel.basic.Multiplicity;

/**
 * @author mmt
 *
 */
public class UITree extends UIComponent {

	private static final long serialVersionUID = 3244254353826241729L;
	
	private Multiplicity multiplicity;

	/**
	 * @param name
	 */
	public UITree(String name) {
		super(name);
	}

	/**
	 * @return the multiplicity
	 */
	public Multiplicity getMultiplicity() {
		return multiplicity;
	}
	
	/**
	 * This method determines the multiplicity by taking
	 * entity mapping and modeled multiplicity into account.
	 * 
	 * Note that the method doesn't check whether or ensures that the
	 * returned multiplicity is the same as the one returned
	 * from {@link #getMultiplicity()}.
	 * 
	 * This method never returns null.
	 * 
	 * @return the derived multiplicity that should be used to take decisions in converters/writers
	 */
	public Multiplicity getDerivedMultiplicity() {
		Multiplicity result = Multiplicity.SINGLE_VALUED;
		
        if (getEntityField() != null) {
			if (getEntityField().getCollectionType() == null) {
				result = Multiplicity.SINGLE_VALUED;
			} else {
				result = Multiplicity.MULTI_VALUED;
			}
		} else if (this.multiplicity != null) {
			result = this.multiplicity;
		}
		
		return result;
	}

	/**
	 * @param multiplicity the multiplicity to set
	 */
	public void setMultiplicity(Multiplicity multiplicity) {
		this.multiplicity = multiplicity;
	}
	
	@Override
	public boolean isLargeWidthRequired() {
		return true;
	}
}
