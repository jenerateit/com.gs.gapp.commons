/**
 *
 */
package com.gs.gapp.metamodel.ui.component;


/**
 * @author mmt
 *
 */
public class UIFileDownload extends UIComponent {

	private static final long serialVersionUID = 3244254353826241729L;

	/**
	 * @param name
	 */
	public UIFileDownload(String name) {
		super(name);
	}
}
