/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.ui.ContainerFlowType;
import com.gs.gapp.metamodel.ui.CustomMessageType;
import com.gs.gapp.metamodel.ui.container.UIContainer;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;
import com.gs.gapp.metamodel.ui.container.data.UIGridContainer;
import com.gs.gapp.metamodel.ui.container.data.UIListContainer;
import com.gs.gapp.metamodel.ui.container.data.UITreeContainer;

/**
 * @author mmt
 *
 */
public class UIComponent extends ModelElement {

	private static final long serialVersionUID = -8051966047905097284L;

	private UIContainer owner;

	private String label;

	private EntityField entityField;
	
	private boolean optional = false;
	
	private boolean disablable = false;
	
	private boolean handleSelectionEvent = false;

	private boolean withoutLabel = false;
	
	private boolean dataEntryRequired = false;
	
	private com.gs.gapp.metamodel.basic.typesystem.Type dataType;
	
	private boolean readOnly = false;
	
	private final Set<CustomMessageType> customMessageTypes = new LinkedHashSet<>();
	
	private UIStructuralContainer navigationTarget;
	
	private ContainerFlowType containerFlowType;
	
	private final Set<EntityField> entityFieldForDisplay = new LinkedHashSet<>();
	
	private final Set<String> messageKeys = new LinkedHashSet<>();  // APPJSF-610
	
	private final Set<String> messageKeysForWarnings = new LinkedHashSet<>();  // APPJSF-610
	
	private final Set<String> messageKeysForErrors = new LinkedHashSet<>();  // APPJSF-610
	
	/**
	 * @param name
	 */
	public UIComponent(String name) {
		super(name);
	}

	/**
	 * @return the owner
	 */
	public UIContainer getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(UIDataContainer owner) {
		this.owner = owner;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the entityField
	 */
	public EntityField getEntityField() {
		return entityField;
	}

	/**
	 * @param entityField the entityField to set
	 */
	public void setEntityField(EntityField entityField) {
		this.entityField = entityField;
	}
	
	/**
	 * @return
	 */
	public Enumeration getMappedEnumerationType() {
		if (entityField != null && entityField.getType() instanceof Enumeration) {
			return (Enumeration) entityField.getType();
		}
		
		return null;
	}
	
	/**
	 * @return
	 */
	public PrimitiveType getMappedPrimitiveType() {
		if (entityField != null && entityField.getType() instanceof PrimitiveType) {
			return (PrimitiveType) entityField.getType();
		}
		
		return null;
	}
	
	/**
	 * @return
	 */
	public Entity getMappedEntityType() {
		if (entityField != null && entityField.getType() instanceof Entity) {
			return (Entity) entityField.getType();
		}
		
		return null;
	}
	
	/**
	 * @return null or the mapped entity field's complex type (if that type exactly _is_ a complex type and not a type that inherits from it)
	 */
	public ComplexType getMappedComplexType() {
		// we have to check for the exact runtime class since Enumeration and Entity are also instances of ComplexType 
		if (entityField != null && entityField.getType().getClass() == ComplexType.class) {
			return (ComplexType) entityField.getType();
		}
		
		return null;
	}

	/**
	 * TODO maybe we'll have to take additional properties into account in order to be able to decide whether a component dispatches events
	 *
	 * @return
	 */
	public boolean isDispatchingEvents() { return true; }

	/**
	 * This method lets the layout generation decide, whether a separate label needs to be taken into account.
	 * Individual component classes override this method in order to control the layout generation.
	 *
	 * @return true, if this component needs a separate label in the layout _and_ a label value has been set
	 */
	public boolean hasSeparateLabel() {
		return StringTools.isNotEmpty(getLabel());
	}

	public boolean isOptional() {
		return optional;
	}

	public void setOptional(boolean optional) {
		this.optional = optional;
	}

	public boolean isDataEntryRequired() {
		return dataEntryRequired;
	}

	public void setDataEntryRequired(boolean dataEntryRequired) {
		this.dataEntryRequired = dataEntryRequired;
	}
	
	public boolean isMappedToMandatoryEntityField() {
		return getEntityField() != null && !getEntityField().isNullable();
	}

	public boolean isWithoutLabel() {
		return withoutLabel;
	}

	public void setWithoutLabel(boolean withoutLabel) {
		this.withoutLabel = withoutLabel;
	}

	public boolean isDisablable() {
		return disablable;
	}

	public void setDisablable(boolean disablable) {
		this.disablable = disablable;
	}

	public boolean isHandleSelectionEvent() {
		return handleSelectionEvent;
	}

	public void setHandleSelectionEvent(boolean handleSelectionEvent) {
		this.handleSelectionEvent = handleSelectionEvent;
	}
	
	public boolean isInListContainer() {
		return (this.owner instanceof UIListContainer);
	}
	
	public boolean isInGridContainer() {
		return (this.owner instanceof UIGridContainer);
	}
	
	public boolean isInTreeContainer() {
		return (this.owner instanceof UITreeContainer);
	}

	public com.gs.gapp.metamodel.basic.typesystem.Type getDataType() {
		return dataType;
	}

	public void setDataType(com.gs.gapp.metamodel.basic.typesystem.Type dataType) {
		this.dataType = dataType;
	}

	public Set<CustomMessageType> getCustomMessageTypes() {
		return customMessageTypes;
	}
	
	public boolean addCustomMessageType(CustomMessageType messageType) {
		if (messageType != null) {
			return this.customMessageTypes.add(messageType);
		}
		
		return false;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public UIStructuralContainer getNavigationTarget() {
		return navigationTarget;
	}

	public void setNavigationTarget(UIStructuralContainer navigationTarget) {
		this.navigationTarget = navigationTarget;
	}

	public ContainerFlowType getContainerFlowType() {
		return containerFlowType;
	}

	public void setContainerFlowType(ContainerFlowType containerFlowType) {
		this.containerFlowType = containerFlowType;
	}
	
	public Set<EntityField> getEntityFieldForDisplay() {
		return entityFieldForDisplay;
	}

	public boolean addEntityFieldForDisplay(EntityField entityFieldForDisplay) {
		return this.entityFieldForDisplay.add(entityFieldForDisplay);
	}

	/**
	 * @return the messageKeys
	 */
	public Set<String> getMessageKeys() {
		return messageKeys;
	}

	/**
	 * @return the messageKeysForWarnings
	 */
	public Set<String> getMessageKeysForWarnings() {
		return messageKeysForWarnings;
	}

	/**
	 * @return the messageKeysForErrors
	 */
	public Set<String> getMessageKeysForErrors() {
		return messageKeysForErrors;
	}
	
	/**
	 * @return
	 */
	public int getTotalNumberOfMessageKeys() {
		return this.messageKeys.size() + this.messageKeysForWarnings.size() + this.messageKeysForErrors.size();
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#getQualifiedName()
	 */
	@Override
	public String getQualifiedName() {
		if (getOwner() != null) {
			return new StringBuilder(getOwner().getName()).append(".").append(this.getName()).toString();
		}
		
		return super.getQualifiedName();
	}
	
	/**
	 * This method tells the generator code, whether the component requires a lot of space to be displayed.
	 * 
	 * <p>When web pages are generated, charts normally require a large width to be displayed.
	 * In such a case, the generated code should not pose any limits to the available width
	 * for the component to be displayed.
	 * 
	 * @return
	 */
	public boolean isLargeWidthRequired() {
		return false;
	}
}
