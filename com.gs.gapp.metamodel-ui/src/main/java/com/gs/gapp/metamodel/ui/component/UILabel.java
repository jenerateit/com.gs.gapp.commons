/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author mmt
 *
 */
public class UILabel extends UIComponent {


	/**
	 *
	 */
	private static final long serialVersionUID = -5316532116878667520L;
	
	private Format format;

	/**
	 * @param name
	 */
	public UILabel(String name) {
		super(name);
		super.setReadOnly(true);  // by default, labels are not used for data entry
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.ui.component.UIComponent#isDispatchingEvents()
	 */
	@Override
	public boolean isDispatchingEvents() { return false; }
	
	public Format getFormat() {
		return format;
	}

	public void setFormat(Format format) {
		this.format = format;
	}

	/**
	 * @author mmt
	 *
	 */
	public enum Format {
		
		DATE ("Date"),
		TIME ("Time"),
		DATE_TIME ("DateTime"),
		TIMESTAMP ("Timestamp"),
		NUMBER ("Number"),
		DECIMAL_NUMBER ("DecimalNumber"),
		CURRENCY ("Currency"),
		PERCENT ("Percent"),
		HTML ("Html"),
		;
		
		private final static Map<String,Format> map = new HashMap<>();
		
		static {
			for (Format format : Format.values()) {
				map.put(format.getName().toLowerCase(), format);
			}
		}
		
		/**
		 * @param name
		 * @return
		 */
		public static Format get(String name) {
			return map.get(name.toLowerCase());
		}
		
		private final String name;
		
		private Format(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}
	
}
