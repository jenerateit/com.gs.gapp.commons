/**
 *
 */
package com.gs.gapp.metamodel.ui.container.data;


/**
 * @author mmt
 *
 */
public class UICustomContainer extends UIDataContainer {

	/**
	 *
	 */
	private static final long serialVersionUID = -2670019358815986974L;

	/**
	 * @param name
	 */
	public UICustomContainer(String name) {
		super(name);
	}
}
