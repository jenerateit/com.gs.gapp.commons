/**
 *
 */
package com.gs.gapp.metamodel.ui;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.container.StructuralContainerType;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * @author mmt
 *
 */
public class UiUtil {

	/**
	 *
	 */
    private UiUtil() {}

    /**
     * Create new persistence module instances that represent the given entities, entity fields and ui components.
     * This algorithm is kind of a normalization. That may be used during conversion from syntax models to syntax-free models.
     *
     * @param entities
     * @param entityFields
     * @param uiComponents
     * @return
     */
    public static Set<PersistenceModule> createNewPersistenceModules(Set<Entity> entities, Set<EntityField> entityFields, Set<UIComponent> uiComponents) {
    	Set<PersistenceModule> result = new LinkedHashSet<>();

    	// TODO implement this

    	return result;
    }

    /**
     * @param functionModules
     * @param functions
     * @return
     */
    public static Set<FunctionModule> createNewFunctionModules(Set<FunctionModule> functionModules, Set<Function> functions) {
    	Set<FunctionModule> result = new LinkedHashSet<>();

    	// TODO implement this

    	return result;
    }

    /**
     * @param dataContainer
     * @return
     */
    public static UIStructuralContainer createWrappingStructuralContainer(UIDataContainer dataContainer) {
    	UIStructuralContainer structuralContainer = new UIStructuralContainer(dataContainer.getTechnicalName() + "Controller");
    	structuralContainer.setOriginatingElement(dataContainer);
    	structuralContainer.setType(StructuralContainerType.SPLIT);
    	structuralContainer.addChildContainer(dataContainer);

    	return structuralContainer;
    }
}
