/**
 *
 */
package com.gs.gapp.metamodel.ui.databinding;


/**
 * @author mmt
 *
 */
public class RemoteStorage extends Storage {

	private static final long serialVersionUID = 5820165875834183925L;

	/**
	 * @param name
	 */
	public RemoteStorage(String name) {
		super(name);
	}
}
