package com.gs.gapp.metamodel.ui;

import java.util.Collection;
import java.util.LinkedHashSet;

import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.basic.Multiplicity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.ui.component.UIChoice;
import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * This validator checks, whether there are modeled choices
 * that have settings that contradict each other.
 * 
 * @author marcu
 *
 */
public class ValidatorChoiceSettingsForView implements ModelValidatorI {

	public ValidatorChoiceSettingsForView() {}

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(assertNoAmbiguousChoiceSettings(modelElements));
		return result;
	}

	/**
	 * @param rawElements
	 */
	private Collection<Message> assertNoAmbiguousChoiceSettings(Collection<Object> rawElements) {
		LinkedHashSet<Message> result = new LinkedHashSet<>();
		
		// --- collect all data containers with incorrect modeling
		for (Object element : rawElements) {
			if (element instanceof UIStructuralContainer) {
				UIStructuralContainer potentialMainContainer = (UIStructuralContainer) element;
				if (potentialMainContainer.isMainContainer()) {
					LinkedHashSet<Message> resultPerView = new LinkedHashSet<>();
					for (UIDataContainer dataContainer : potentialMainContainer.getAllChildDataContainers()) {
						LinkedHashSet<Message> resultPerDisplay = new LinkedHashSet<>();
						for (UIComponent component : dataContainer.getComponents()) {
							if (component instanceof UIChoice) {
								UIChoice choice = (UIChoice) component;
								Multiplicity modeledMultiplicity = choice.getMultiplicity();
								Multiplicity derivedMultiplicity = choice.getDerivedMultiplicity();
								EntityField mappedEntityField = choice.getEntityField();
								
								if (modeledMultiplicity != null && modeledMultiplicity != derivedMultiplicity) {
									if (resultPerView.isEmpty()) {
										Message mainMessage = UiMessage.INVALID_CHOICE_SETTINGS_SUMMARY_FOR_VIEW.getMessage(potentialMainContainer.getName(), potentialMainContainer.getModule().getName());
										resultPerView.add(mainMessage);
									}
									
									if (resultPerDisplay.isEmpty()) {
										Message displayMessage = UiMessage.INVALID_CHOICE_SETTINGS_SUMMARY_FOR_DISPLAY.getMessage(dataContainer.getName(), dataContainer.getModule().getName());
										resultPerDisplay.add(displayMessage);
									}
									
									Message messagePerDisplay = UiMessage.INVALID_CHOICE_SETTINGS.getMessage(choice.getName(),
											modeledMultiplicity,
											derivedMultiplicity,
											choice.getType(),
											(mappedEntityField == null ? "NONE" : mappedEntityField.getName()) +
										    (mappedEntityField == null ? "" : (mappedEntityField.getCollectionType() == null ? " (no collection type)" : " (collection type=" + mappedEntityField.getCollectionType() + ")")));
									resultPerDisplay.add(messagePerDisplay);
								}
							}
						}
						resultPerView.addAll(resultPerDisplay);
					}
					result.addAll(resultPerView);
				}//is main container
			}
		}
		
		return result;
	}
}
