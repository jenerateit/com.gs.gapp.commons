/**
 * 
 */
package com.gs.gapp.metamodel.ui.container.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author mmt
 *
 */
public enum FormLabelPositionEnum {
	
	LEADING ("leading"),
	INSIDE ("inside"),
	ONTOP ("ontop");
	
    private static final Map<String, FormLabelPositionEnum> stringToEnum =
    		new HashMap<>();
	
	static {
		for (FormLabelPositionEnum m : values()) {
			stringToEnum.put(m.getName(), m);
		}
	}
	
	/**
	 * @param name
	 * @return
	 */
	public static FormLabelPositionEnum fromString(String name) {
		return stringToEnum.get(name);
	}
	
	private final String name;
	
	private FormLabelPositionEnum(String name) {
		this.name=name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return
	 */
	public static List<String> getNames() {
		List<String> result = new ArrayList<>();
		
		for (FormLabelPositionEnum entry : values()) {
			result.add(entry.name());
		}
		
		return result;
	}
}
