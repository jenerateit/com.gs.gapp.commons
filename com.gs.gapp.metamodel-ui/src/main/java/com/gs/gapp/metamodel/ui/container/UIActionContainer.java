/**
 *
 */
package com.gs.gapp.metamodel.ui.container;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.ui.component.UIActionComponent;

/**
 * @author mmt
 *
 */
public class UIActionContainer extends UIContainer {



	/**
	 *
	 */
	private static final long serialVersionUID = 3723342647495329980L;

	private final Set<UIActionComponent> actionComponents = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public UIActionContainer(String name) {
		super(name);
	}

	/**
	 * @return the actionComponents
	 */
	public Set<UIActionComponent> getActionComponents() {
		return actionComponents;
	}
	
	/**
	 * @return all of the action container's action components, including the action components that belong to all of this action container's action container tree
	 */
	public Set<UIActionComponent> getAllActionComponents() {
		Set<UIActionComponent> result = new LinkedHashSet<>();
		
		Set<UIActionContainer> allActionContainers = getAllActionContainers();
		allActionContainers.add(this);
		
		for (UIActionContainer actionContainer : allActionContainers) {
			result.addAll(actionContainer.getActionComponents());
		}
		
		return result;
	}

	/**
	 * @param component
	 */
	public boolean addActionComponent(UIActionComponent component) {
		boolean result = this.actionComponents.add(component);
		component.setOwningActionContainer(this);
		return result;
	}

}
