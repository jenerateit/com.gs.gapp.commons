/**
 *
 */
package com.gs.gapp.metamodel.ui.container.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.ui.ActionPurpose;
import com.gs.gapp.metamodel.ui.ContainerFlow;
import com.gs.gapp.metamodel.ui.component.UIActionComponent;
import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.component.UIEmbeddedComponentGroup;
import com.gs.gapp.metamodel.ui.component.UIEmbeddedContainer;
import com.gs.gapp.metamodel.ui.container.UIActionContainer;
import com.gs.gapp.metamodel.ui.container.UIContainer;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;

/**
 * @author mmt
 *
 */
public class UIDataContainer extends UIContainer {

	private static final long serialVersionUID = 7826790216256043549L;

	private final Set<UIComponent> components = new LinkedHashSet<>();

	private Entity entity;
	private Function function;

	private final Set<UIComponent> sortBy = new LinkedHashSet<>();
	
	private final Map<UIComponent, FilterMode> filterBy = new LinkedHashMap<>();

	/**
	 * TODO do we really need this?
	 */
	private boolean interactive = true;

	private FormLabelPositionEnum formLabelPosition = FormLabelPositionEnum.INSIDE;
	
	private boolean Collapsible = false;
	
	private final Set<Field> fieldsWithImplicitComponents = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public UIDataContainer(String name) {
		super(name);
	}

	/**
	 * @return the actionComponents
	 */
	public Set<UIComponent> getComponents() {
		return components;
	}

	/**
	 * @return
	 */
	public Set<UIActionComponent> getActionComponents() {
		Set<UIActionComponent> result = new LinkedHashSet<>();

		for (UIComponent comp : components) {
			if (comp instanceof UIActionComponent)
				result.add((UIActionComponent) comp);
		}
		return result;
	}

	/**
	 * @param component
	 */
	public void addComponent(UIComponent component) {
		this.components.add(component);
		component.setOwner(this);
	}

	/**
	 * @return the entity
	 */
	public Entity getEntity() {
		return entity;
	}

	/**
	 * @param entity
	 *            the entity to set
	 */
	public void setEntity(Entity entity) {
		this.entity = entity;
	}

	/**
	 * @return the entity assigned to this data container, but if it is null, the
	 *         container's components are checked for entity fields assignments
	 */
	public Entity getEntityThroughComponents() {
		Entity result = null;
		if (getEntity() == null) {
			for (UIComponent component : getComponents()) {
				if (component.getEntityField() != null) {
					result = component.getEntityField().getOwner();
					break;
					/*
					 * TODO improve algorithm to find out the best fitting entity (mmt 20-Feb-2013)
					 * Entity newResult = component.getEntityField().getOwner(); if (result != null)
					 * { if (newResult.is) } else { result = newResult; }
					 */
				}
			}
		} else {
			result = getEntity();
		}
		return result;
	}

	/**
	 * @return
	 */
	public Collection<EntityField> getMappedEntityTypedFields() {
		LinkedHashSet<EntityField> result = new LinkedHashSet<>();
		for (UIComponent component : getComponents()) {
			if (component.getEntityField() != null) {
                result.add(component.getEntityField());
			}
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public Collection<EntityField> getMappedSingleValuedEntityTypedFields() {
		LinkedHashSet<EntityField> result = new LinkedHashSet<>();
		for (EntityField entityField : getMappedEntityTypedFields()) {
            if (entityField.getCollectionType() == null) {
                result.add(entityField);
			}
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public Collection<EntityField> getUnmappedMandatorySingleValuedEntityTypedFields() {
		LinkedHashSet<EntityField> result = new LinkedHashSet<>();
		
		if (getEntity() != null) {
			Collection<EntityField> mappedSingleValuedEntityTypedFields = getMappedSingleValuedEntityTypedFields();
			for (EntityField entityField : getEntity().getMandatorySingleValuedEntityTypedFields()) {
				if (!mappedSingleValuedEntityTypedFields.contains(entityField)) {
					result.add(entityField);
				}
			}
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public Collection<EntityField> getMappedMultiValuedEntityTypedFields() {
		LinkedHashSet<EntityField> result = new LinkedHashSet<>();
		for (EntityField entityField : getMappedEntityTypedFields()) {
            if (entityField.getCollectionType() != null) {
                result.add(entityField);
			}
		}
		
		return result;
	}

	/**
	 * @return true if there is at least one action component or at least one
	 *         editable component in this data container
	 */
	public boolean isDispatchingEvents() {
		boolean result = false;

		if (interactive == true) {
			for (UIComponent component : getComponents()) {
				if (component instanceof UIActionComponent) {
					result = true;
					break;
				}
				if (component.isDispatchingEvents()) {
					result = true;
					break;
				}
			}
		}

		return result;
	}

	/**
	 * @return the interactive
	 */
	public boolean isInteractive() {
		return interactive;
	}

	/**
	 * @param interactive
	 *            the interactive to set
	 */
	public void setInteractive(boolean interactive) {
		this.interactive = interactive;
	}

	/**
	 * @return
	 */
	public Set<ContainerFlow> getAllContainerFlows() {
		Set<ContainerFlow> result = new LinkedHashSet<>();
		result.addAll(this.getFlows());
		for (UIComponent component : getComponents()) {
			if (component instanceof UIEmbeddedContainer) {
				UIEmbeddedContainer embeddedContainer = (UIEmbeddedContainer) component;
				result.addAll(embeddedContainer.getAllContainerFlows());
			}
		}
		return result;
	}

	public Set<UIContainer> getAllChildContainers() {
		return getAllChildContainers(true);
	}

	/**
	 * @return
	 */
	public Set<UIContainer> getAllChildContainers(boolean includeEmbeddedContainers) {
		Set<UIContainer> result = new LinkedHashSet<>();
		return collectAllChildContainers(result, includeEmbeddedContainers);
	}
	
	/**
	 * @param includeEmbeddedContainers
	 * @return
	 */
	public List<UIContainer> getAllChildContainersWithDuplicates(boolean includeEmbeddedContainers) {
		List<UIContainer> result = new ArrayList<>();
		return collectAllChildContainersWithDuplicates(result, includeEmbeddedContainers);
	}

	/**
	 * @param containers
	 * @return
	 */
	protected Set<UIContainer> collectAllChildContainers(Set<UIContainer> containers,
			boolean includeEmbeddedContainers) {
		for (UIComponent component : getComponents()) {

			if (includeEmbeddedContainers) {
				if (component instanceof UIEmbeddedContainer) {
					UIEmbeddedContainer embeddedContainer = (UIEmbeddedContainer) component;
					containers.addAll(embeddedContainer.getAllChildContainers());
				} else if (component instanceof UIEmbeddedComponentGroup) {
					UIEmbeddedComponentGroup embeddedComponentGroup = (UIEmbeddedComponentGroup) component;
					if (embeddedComponentGroup.getDataContainer() != null) {
						containers.add(embeddedComponentGroup.getDataContainer());
						containers.addAll(embeddedComponentGroup.getDataContainer().getAllChildContainers(includeEmbeddedContainers));
					}
				}
			}
		}
		return containers;
	}
	
	/**
	 * @param containers
	 * @return
	 */
	protected List<UIContainer> collectAllChildContainersWithDuplicates(List<UIContainer> containers,
			boolean includeEmbeddedContainers) {
		for (UIComponent component : getComponents()) {

			if (includeEmbeddedContainers) {
				if (component instanceof UIEmbeddedContainer) {
					UIEmbeddedContainer embeddedContainer = (UIEmbeddedContainer) component;
					containers.addAll(embeddedContainer.getAllChildContainersWithDuplicates());
				} else if (component instanceof UIEmbeddedComponentGroup) {
					UIEmbeddedComponentGroup embeddedComponentGroup = (UIEmbeddedComponentGroup) component;
					if (embeddedComponentGroup.getDataContainer() != null) {
						containers.add(embeddedComponentGroup.getDataContainer());
						containers.addAll(embeddedComponentGroup.getDataContainer().getAllChildContainersWithDuplicates(includeEmbeddedContainers));
					}
				}
			}
		}
		return containers;
	}

	public Set<UIContainer> getChildContainers() {
		Set<UIContainer> result = new LinkedHashSet<>();

		for (UIComponent component : getComponents()) {
			if (component instanceof UIEmbeddedContainer) {
				UIEmbeddedContainer embeddedContainer = (UIEmbeddedContainer) component;
				result.add(embeddedContainer.getContainer());
			} else if (component instanceof UIEmbeddedComponentGroup) {
				UIEmbeddedComponentGroup embeddedComponentGroup = (UIEmbeddedComponentGroup) component;
				if (embeddedComponentGroup.getDataContainer() != null) {
					result.add(embeddedComponentGroup.getDataContainer());
				}
			}
		}
		return result;
	}

	public Set<UIStructuralContainer> getStructuralChildContainers() {
		Set<UIStructuralContainer> result = new LinkedHashSet<>();

		for (UIContainer container : getChildContainers()) {
			if (container instanceof UIStructuralContainer) {
				result.add((UIStructuralContainer) container);
			}
		}

		return result;
	}

	/**
	 * This method collects and returns all data containers that
	 * are linked to this data container through components of type
	 * EmbeddedComponentGroup or EmbeddedContainer components.
	 * 
	 * @return
	 */
	public Set<UIDataContainer> getDataChildContainers() {
		Set<UIDataContainer> result = new LinkedHashSet<>();

		for (UIContainer container : getChildContainers()) {
			if (container instanceof UIDataContainer) {
				result.add((UIDataContainer) container);
			}
		}

		return result;
	}

	/**
	 * @return
	 */
	public Set<UIContainer> getAllContainers() {
		Set<UIContainer> result = new LinkedHashSet<>();

		result.add(this);
		for (UIContainer childContainer : getAllChildContainers()) {
			result.add(childContainer);
			for (ContainerFlow flow : childContainer.getFlows()) {
				if (childContainer != flow.getTarget()) {
					if (flow.getTarget() instanceof UIStructuralContainer) {
						UIStructuralContainer structuralContainer = (UIStructuralContainer) flow.getTarget();
						result.addAll(structuralContainer.getAllContainers());
					} else if (flow.getTarget() instanceof UIDataContainer) {
						UIDataContainer dataContainer = (UIDataContainer) flow.getTarget();
						result.addAll(dataContainer.getAllContainers());
					}
				}
			}
		}
		return result;
	}

	/**
	 * @return
	 */
	public Set<UIComponent> getComponentsWithCustomMessages() {
		LinkedHashSet<UIComponent> componentsWithCustomMessages = new LinkedHashSet<>();
		for (UIComponent component : this.components) {
			if (component.getCustomMessageTypes().size() > 0) {
				componentsWithCustomMessages.add(component);
			}
		}

		return componentsWithCustomMessages;
	}

	/**
	 * @return the formLabelPosition
	 */
	public FormLabelPositionEnum getFormLabelPosition() {
		return formLabelPosition;
	}

	/**
	 * @param formLabelPosition
	 *            the formLabelPosition to set
	 */
	public void setFormLabelPosition(FormLabelPositionEnum formLabelPosition) {
		this.formLabelPosition = formLabelPosition;
	}

	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public Set<UIComponent> getSortBy() {
		return sortBy;
	}

	public boolean addSortBy(UIComponent uiComponent) {
		return sortBy.add(uiComponent);
	}

	public UIComponent getDefaultSortBy() {
		if (sortBy.size() > 0)
			return sortBy.iterator().next();

		return null;
	}

	/**
	 * @return true when the data container has at least one component that features
	 *         a label (which is used in forms)
	 */
	public boolean hasComponentsWithLabel() {
		for (UIComponent uiComponent : this.components) {
			if (!uiComponent.isWithoutLabel()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Collect all entities that have fields that this data container's ui
	 * components are linked to.
	 * 
	 * @return
	 */
	public Set<Entity> getEntities() {
		Set<Entity> result = new LinkedHashSet<>();

		if (getEntity() != null)
			result.add(getEntity());

		for (UIComponent component : components) {
			if (component.getEntityField() != null) {
				result.add(component.getEntityField().getOwner());
				if (component.getEntityField().getType() instanceof Entity) {
					result.add((Entity) component.getEntityField().getType());
				}
			}
		}

		return result;
	}
	
	/**
	 * Collect all action components of this data container and all its toolbars,
	 * embedded component groups and embedded containers that have the given action purpose.
	 * 
	 * @param actionPurpose
	 * @return
	 */
	public Collection<UIActionComponent> getActionComponents(ActionPurpose actionPurpose) {
		LinkedHashSet<UIActionComponent> result = new LinkedHashSet<>();
		for (UIActionContainer actionContainer : getAllActionContainers()) {
			for (UIActionComponent actionComponent : actionContainer.getAllActionComponents()) {
				if (actionComponent.getActionPurpose() == actionPurpose) {
					result.add(actionComponent);
				}
			}
		}
		
		for (UIComponent component : components) {
			if (component instanceof UIEmbeddedComponentGroup) {
				UIEmbeddedComponentGroup embeddedComponentGroup = (UIEmbeddedComponentGroup) component;
				result.addAll(embeddedComponentGroup.getDataContainer().getActionComponents());
			} else if (component instanceof UIEmbeddedContainer) {
				UIEmbeddedContainer embeddedContainer = (UIEmbeddedContainer) component;
				for (UIDataContainer dataContainer : embeddedContainer.getAllDataContainers()) {
					result.addAll(dataContainer.getActionComponents(actionPurpose));
				}
			} else if (component instanceof UIActionComponent) {
				UIActionComponent actionComponent = (UIActionComponent) component;
				if (actionComponent.getActionPurpose() == actionPurpose) {
					result.add(actionComponent);
				}
			}
		}
		
		return result;
	}

	/**
	 * @return the filterBy
	 */
	public Map<UIComponent, FilterMode> getFilterBy() {
		return filterBy;
	}
	
	/**
	 * @param uiComponent
	 */
	public void addFilterBy(UIComponent uiComponent, FilterMode filterMode) {
		if (uiComponent == null) throw new NullPointerException("param 'uiComponent' must not be null");
		if (filterMode == null) throw new NullPointerException("param 'filterMode' must not be null");
		
		filterBy.put(uiComponent, filterMode);
	}
	
	/**
	 * @return the collapsible
	 */
	public boolean isCollapsible() {
		return Collapsible;
	}

	/**
	 * @param collapsible the collapsible to set
	 */
	public void setCollapsible(boolean collapsible) {
		Collapsible = collapsible;
	}
	
	/**
	 * These fields are going to be used to automatically create UI components for them.
	 * 
	 * <p>The logic to create such UI components normally is implemented at the beginning or at the end
	 * of a model converter. If you have a complex generator family, it makes sense to create a class/component
	 * that is responsible for the implicit creation of UI components and can be easily reused in several generators.
	 * 
	 * @return
	 */
	public Set<Field> getFieldsWithImplicitComponents() {
		return fieldsWithImplicitComponents;
	}

    public boolean addFieldWithImplicitComponents(Field field) {
    	return this.fieldsWithImplicitComponents.add(field);
    }
    
    /**
     * @return
     */
    public List<UIComponent> getComponentsThatRequireLargeWidth() {
    	List<UIComponent> componentsThatRequireLargeWidth = this.components.stream()
	    	.filter(component -> component.isLargeWidthRequired())
	    	.collect(Collectors.toList());
    	return componentsThatRequireLargeWidth;
    }
    
    /**
     * @return
     */
    public Collection<ComplexType> getAppliedComplexDataTypes() {
    	return this.getComponents()
	    	    .stream()
	    	    .filter(component -> component.getDataType() != null)
	    	    .map(component -> component.getDataType())
	    	    .filter(dataType -> dataType instanceof ComplexType)
	    	    .map(ComplexType.class::cast)
	    	    .collect(Collectors.toCollection(LinkedHashSet::new));
    }

	/**
	 * @author marcu
	 *
	 */
	public enum FilterMode {
		STARTS_WITH,
		CONTAINS,
		EXACT,
		ENDS_WITH,
		LESS_THAN,
		LESS_THAN_OR_EQUAL,
		GREATER_THAN,
		GREATER_THAN_OR_EQUAL,
		EQUAL,
		IN,
		;
	}
}
