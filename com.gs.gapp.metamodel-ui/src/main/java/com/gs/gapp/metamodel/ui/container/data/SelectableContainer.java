package com.gs.gapp.metamodel.ui.container.data;

import com.gs.gapp.metamodel.ui.container.SelectionModeEnum;

public interface SelectableContainer {

	SelectionModeEnum getSelectionMode();

	void setSelectionMode(SelectionModeEnum selectionMode);
	
}
