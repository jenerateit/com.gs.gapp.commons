/**
 *
 */
package com.gs.gapp.metamodel.ui.databinding;


/**
 * @author mmt
 *
 */
public class LocalStorage extends Storage {

	private static final long serialVersionUID = 7404456190848244358L;

	/**
	 * @param name
	 */
	public LocalStorage(String name) {
		super(name);
	}
}
