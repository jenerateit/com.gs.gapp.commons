/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.basic.typesystem.Type;

/**
 * @author mmt
 *
 */
public class UITextField extends UIComponent implements TextValueInputable {

	/**
	 *
	 */
	private static final long serialVersionUID = 3008319968179637993L;

	private boolean secretContent = false;
	private String inputMask;
	private Integer length;
	
	/**
	 * @param name
	 */
	public UITextField(String name) {
		super(name);
	}

	/**
	 * @return the secretContent
	 */
	public boolean isSecretContent() {
		return secretContent;
	}

	/**
	 * @param secretContent the secretContent to set
	 */
	public void setSecretContent(boolean secretContent) {
		this.secretContent = secretContent;
	}

	public String getInputMask() {
		return inputMask;
	}

	public void setInputMask(String inputMask) {
		this.inputMask = inputMask;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}
	
	/**
	 * This method determines whether the text field is meant to be
	 * used to edit numeric data. This information can be used by
	 * generators to identify a UI component that is geared to handle
	 * numeric data.
	 * 
	 * @return
	 */
	public boolean isForNumericData() {
		Type relatedType = null;
		if (getEntityField() != null) {
			relatedType = getEntityField().getType();
		} else if (getDataType() != null) {
			relatedType = getDataType();
		}
		
		if (relatedType instanceof PrimitiveType &&
				((PrimitiveType)relatedType).getNumeric() != null) {
			return ((PrimitiveType)relatedType).getNumeric().booleanValue();
		}
		
		return false;
	}
}
