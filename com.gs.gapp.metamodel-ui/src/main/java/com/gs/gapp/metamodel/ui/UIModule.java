/**
 *
 */
package com.gs.gapp.metamodel.ui;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.Module;

/**
 * @author mmt
 *
 */
public class UIModule extends Module {

	/**
	 *
	 */
	private static final long serialVersionUID = -1785131088353088177L;

	/**
	 * @param name
	 */
	public UIModule(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#getNamespace()
	 */
	@Override
	public Namespace getNamespace() {
		return (Namespace) super.getNamespace();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#setNamespace(com.gs.gapp.metamodel.basic.Namespace)
	 */
	@Override
	public void setNamespace(com.gs.gapp.metamodel.basic.Namespace namespace) {
		if (!(namespace instanceof Namespace)) {
			throw new IllegalArgumentException("the namespace of a UIModule must be an instance of com.gs.gapp.metamodel.ui.Namespace");
		}
		super.setNamespace(namespace);
	}
	
	/**
	 * @return
	 */
	public Set<UIModule> getAllImportedUIModules() {
		Set<UIModule> result = new LinkedHashSet<>();
		collectAllImportedUIModules(result);
		return result;
	}

	/**
	 * @param uiModules
	 * @return
	 */
	private Set<UIModule> collectAllImportedUIModules(Set<UIModule> uiModules) {
		if (uiModules == null) throw new NullPointerException("parameter 'uiModules' must not be null");
		for (UIModule importedModule : getElements(UIModule.class)) {
			uiModules.add(importedModule);
			importedModule.collectAllImportedUIModules(uiModules);
		}

		return uiModules;
	}
}
