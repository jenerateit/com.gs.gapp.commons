/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

/**
 * @author mmt
 *
 */
public class UIChart extends UIComponent {

	/**
	 *
	 */
	private static final long serialVersionUID = -7878046924532708487L;

	private Type type;
	
	/**
	 * @param name
	 */
	public UIChart(String name) {
		super(name);
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	
	@Override
	public boolean isLargeWidthRequired() {
		return true;
	}

	/**
	 * @author mmt
	 *
	 */
	public static enum Type {
		BAR,
		POLAR_AREA,
		BUBBLE,
		RADAR,
		DONUT,
		LINE,
		PIE,
		SCATTER,
		;
	}
}
