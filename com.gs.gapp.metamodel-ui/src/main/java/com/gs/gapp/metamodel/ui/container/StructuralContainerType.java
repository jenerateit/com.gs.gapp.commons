/**
 * 
 */
package com.gs.gapp.metamodel.ui.container;

/**
 * @author mmt
 *
 */
public enum StructuralContainerType {
	TAB,
	SPLIT,
	WIZARD,
	DASHBOARD,
	;
}
