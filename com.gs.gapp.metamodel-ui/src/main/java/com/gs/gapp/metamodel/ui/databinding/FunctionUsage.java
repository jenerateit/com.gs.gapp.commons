/**
 *
 */
package com.gs.gapp.metamodel.ui.databinding;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.function.FunctionModule;

/**
 * @author mmt
 *
 */
public class FunctionUsage extends ModelElement {

	/**
	 *
	 */
	private static final long serialVersionUID = 4813008250796758824L;

	private final Set<FunctionModule> usedFunctionModules = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public FunctionUsage(String name) {
		super(name);
	}

	/**
	 * @return the usedFunctionModules
	 */
	public Set<FunctionModule> getUsedFunctionModules() {
		return usedFunctionModules;
	}

	/**
	 * @param functionModule
	 * @return
	 */
	public boolean addUsedFunctionModule(FunctionModule functionModule) {
		return this.usedFunctionModules.add(functionModule);
	}

}
