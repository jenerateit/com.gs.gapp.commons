/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

/**
 * @author mmt
 *
 */
public class UILinkComponent extends UIActionComponent {

    /**
	 *
	 */
	private static final long serialVersionUID = -4937262789957517074L;

	/**
	 * @param name
	 */
	public UILinkComponent(String name) {
		super(name);
	}
}
