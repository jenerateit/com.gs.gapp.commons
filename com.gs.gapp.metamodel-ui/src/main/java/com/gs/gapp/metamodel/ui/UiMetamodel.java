package com.gs.gapp.metamodel.ui;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;

import com.gs.gapp.metamodel.basic.MetamodelI;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.ui.component.UIActionComponent;
import com.gs.gapp.metamodel.ui.component.UIBooleanChoice;
import com.gs.gapp.metamodel.ui.component.UIButtonComponent;
import com.gs.gapp.metamodel.ui.component.UIChart;
import com.gs.gapp.metamodel.ui.component.UIChoice;
import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.component.UICustomComponent;
import com.gs.gapp.metamodel.ui.component.UIDateSelector;
import com.gs.gapp.metamodel.ui.component.UIEmbeddedComponentGroup;
import com.gs.gapp.metamodel.ui.component.UIEmbeddedContainer;
import com.gs.gapp.metamodel.ui.component.UIFileDownload;
import com.gs.gapp.metamodel.ui.component.UIFileUpload;
import com.gs.gapp.metamodel.ui.component.UIHTML;
import com.gs.gapp.metamodel.ui.component.UIImage;
import com.gs.gapp.metamodel.ui.component.UILabel;
import com.gs.gapp.metamodel.ui.component.UILinkComponent;
import com.gs.gapp.metamodel.ui.component.UIMap;
import com.gs.gapp.metamodel.ui.component.UIRange;
import com.gs.gapp.metamodel.ui.component.UITextArea;
import com.gs.gapp.metamodel.ui.component.UITextField;
import com.gs.gapp.metamodel.ui.component.UITimeline;
import com.gs.gapp.metamodel.ui.component.UITree;
import com.gs.gapp.metamodel.ui.component.UIVideo;
import com.gs.gapp.metamodel.ui.container.UIActionContainer;
import com.gs.gapp.metamodel.ui.container.UIContainer;
import com.gs.gapp.metamodel.ui.container.UIDockableContainer;
import com.gs.gapp.metamodel.ui.container.UIDockingContainer;
import com.gs.gapp.metamodel.ui.container.UIMenuContainer;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.UIViewContainer;
import com.gs.gapp.metamodel.ui.container.data.UICustomContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;
import com.gs.gapp.metamodel.ui.container.data.UIGridContainer;
import com.gs.gapp.metamodel.ui.container.data.UIListContainer;
import com.gs.gapp.metamodel.ui.container.data.UITreeContainer;
import com.gs.gapp.metamodel.ui.container.data.UITreeTableContainer;
import com.gs.gapp.metamodel.ui.databinding.ContainerGroup;
import com.gs.gapp.metamodel.ui.databinding.FunctionUsage;
import com.gs.gapp.metamodel.ui.databinding.LocalStorage;
import com.gs.gapp.metamodel.ui.databinding.RemoteStorage;
import com.gs.gapp.metamodel.ui.databinding.Storage;

/**
 * @author marcu
 *
 */
public enum UiMetamodel implements MetamodelI {

	INSTANCE,
	;
	
	private static final LinkedHashSet<Class<? extends ModelElementI>> metatypes = new LinkedHashSet<>();
	private static final Collection<Class<? extends ModelElementI>> collectionOfCheckedMetatypes = new LinkedHashSet<>();
	
	static {
		metatypes.add(UIModule.class);
		metatypes.add(Namespace.class);
		
		metatypes.add(UIActionContainer.class);
		metatypes.add(UIContainer.class);
		metatypes.add(UIDockableContainer.class);
		metatypes.add(UIDockingContainer.class);
		metatypes.add(UIMenuContainer.class);
		metatypes.add(UIStructuralContainer.class);
		metatypes.add(UIViewContainer.class);
		
		metatypes.add(UIActionComponent.class);
		metatypes.add(UIBooleanChoice.class);
		metatypes.add(UIButtonComponent.class);
		metatypes.add(UIChart.class);
		metatypes.add(UIChoice.class);
		metatypes.add(UIComponent.class);
		metatypes.add(UICustomComponent.class);
		metatypes.add(UIDateSelector.class);
		metatypes.add(UIEmbeddedComponentGroup.class);
		metatypes.add(UIEmbeddedContainer.class);
		metatypes.add(UIFileDownload.class);
		metatypes.add(UIFileUpload.class);
		metatypes.add(UIHTML.class);
		metatypes.add(UIImage.class);
		metatypes.add(UILabel.class);
		metatypes.add(UILinkComponent.class);
		metatypes.add(UIMap.class);
		metatypes.add(UIRange.class);
		metatypes.add(UITextArea.class);
		metatypes.add(UITextField.class);
		metatypes.add(UITimeline.class);
		metatypes.add(UITree.class);
		metatypes.add(UIVideo.class);
		
		metatypes.add(UICustomContainer.class);
		metatypes.add(UIDataContainer.class);
		metatypes.add(UIGridContainer.class);
		metatypes.add(UIListContainer.class);
		metatypes.add(UITreeContainer.class);
		metatypes.add(UITreeTableContainer.class);
		
		metatypes.add(ContainerGroup.class);
		metatypes.add(FunctionUsage.class);
		metatypes.add(LocalStorage.class);
		metatypes.add(RemoteStorage.class);
		metatypes.add(Storage.class);
		
		collectionOfCheckedMetatypes.add(UIModule.class);
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypes() {
		return Collections.unmodifiableCollection(metatypes);
	}

	@Override
	public boolean isIncluded(Class<? extends ModelElementI> metatype) {
		return metatypes.contains(metatype);
	}

	@Override
	public boolean isExtendingOneOfTheMetatypes(Class<? extends ModelElementI> metatype) {
		for (Class<? extends ModelElementI> metatypeOfMetamodel : metatypes) {
			if (metatypeOfMetamodel.isAssignableFrom(metatype)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypesForConversionCheck() {
		return collectionOfCheckedMetatypes;
	}
	
	/**
	 * @param metatype
	 * @return
	 */
	public Class<? extends Module> getModuleType(Class<? extends ModelElementI> metatype) {
		if (isIncluded(metatype)) {
		    return UIModule.class;
		}
		return null;
	}
}
