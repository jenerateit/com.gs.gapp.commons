/**
 * 
 */
package com.gs.gapp.metamodel.ui.container;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author mmt
 *
 */
public enum SelectionModeEnum {
	
	SINGLE ("Single"),
	MULTIPLE ("Multiple"),
	NONE ("None");
	
    private static final Map<String, SelectionModeEnum> stringToEnum =
    		new HashMap<>();
	
	static {
		for (SelectionModeEnum m : values()) {
			stringToEnum.put(m.getName().toLowerCase(), m);
		}
	}
	
	/**
	 * @param name
	 * @return
	 */
	public static SelectionModeEnum fromString(String name) {
		return stringToEnum.get(name.toLowerCase());
	}
	
	private final String name;
	
	private SelectionModeEnum(String name) {
		this.name=name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return
	 */
	public static List<String> getNames() {
		List<String> result = new ArrayList<>();
		
		for (SelectionModeEnum entry : values()) {
			result.add(entry.name());
		}
		
		return result;
	}
}
