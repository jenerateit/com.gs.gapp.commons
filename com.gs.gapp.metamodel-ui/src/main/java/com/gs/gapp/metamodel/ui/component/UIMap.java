/**
 *
 */
package com.gs.gapp.metamodel.ui.component;


/**
 * @author mmt
 *
 */
public class UIMap extends UIComponent {



	/**
	 *
	 */
	private static final long serialVersionUID = -3776273944848466800L;

	/**
	 * @param name
	 */
	public UIMap(String name) {
		super(name);
	}
	
	@Override
	public boolean isLargeWidthRequired() {
		return true;
	}
}
