package com.gs.gapp.metamodel.ui;

import com.gs.gapp.metamodel.basic.MessageI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.MessageStatus;

public enum UiMessage implements MessageI {

	MISSING_ENTITY_MAPPING_FOR_DISPLAY_SUMMARY (MessageStatus.ERROR, "0001a",
			"View/Page '%s' in module '%s' has display sections that have individual entity fields mapped but no entity for the display itself.",
			null),
	
	MISSING_ENTITY_MAPPING_FOR_DISPLAY (MessageStatus.ERROR, "0001b",
			"Display '%s' in module '%s' has %d entity fields mapped to components but no entity for the display itself.",
			"Add an entity mapping to the display or remove the entity field mappings."),
	
	INVALID_CHOICE_SETTINGS_SUMMARY_FOR_VIEW (MessageStatus.ERROR, "0002a",
			"*** View '%s' in module '%s' has display sections with choices that have ambiguous settings.",
			null),
	
	INVALID_CHOICE_SETTINGS_SUMMARY_FOR_DISPLAY (MessageStatus.ERROR, "0002b",
			"Display '%s' in module '%s' has choices with ambiguous settings.",
			null),
	
	INVALID_CHOICE_SETTINGS (MessageStatus.ERROR, "0002c",
			"Choice '%s' has a modeled multiplicity of '%s' versus a derived multiplicity of '%s'. " +
				    "The choice has ambiguous settings: choice type=%s, mapped entity field=%s",
			"Fix the choice settings."),
	
	DUPLICATE_CONTAINERS_IN_VIEW (MessageStatus.ERROR, "0003",
			"View/Page '%s' in module '%s' uses %d containers with the name '%s' when there is only one allowed.%s",
			"Change the names in the model or/and make sure that you do not use one and the same UI container more than once within a view/page."),
	;

	private final MessageStatus status;
	private final String organization = "GS";
	private final String section = "UI";
	private final String id;
	private final String problemDescription;
	private final String instruction;
	
	private UiMessage(MessageStatus status, String id, String problemDescription, String instruction) {
		this.status = status;
		this.id = id;
		this.problemDescription = problemDescription;
		this.instruction = instruction;
	}
	
	@Override
	public MessageStatus getStatus() {
		return status;
	}

	@Override
	public String getOrganization() {
		return organization;
	}

	@Override
	public String getSection() {
		return section;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getProblemDescription() {
		return problemDescription;
	}
	
	@Override
	public String getInstruction() {
		return instruction;
	}
}
 