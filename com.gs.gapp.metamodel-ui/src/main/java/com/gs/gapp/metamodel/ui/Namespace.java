/**
 *
 */
package com.gs.gapp.metamodel.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.ui.container.UIContainer;

/**
 * @author mmt
 *
 */
public class Namespace extends com.gs.gapp.metamodel.function.Namespace {

	/**
	 *
	 */
	private static final long serialVersionUID = 7609130794144048820L;

	private Set<UIContainer> containers = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public Namespace(String name) {
		super(name);
	}

	/**
	 * @return the containers
	 */
	public Set<UIContainer> getContainers() {
		return containers;
	}
	
	/**
	 * A convenience method to get all containers, sorted according to their natural ordering.
	 * 
	 * @return
	 */
	public Set<UIContainer> getContainersSorted() {
		ArrayList<UIContainer> elementsForSorting = new ArrayList<>(containers);
		Collections.sort(elementsForSorting);
		Set<UIContainer> elements = new LinkedHashSet<>(elementsForSorting);
		return elements;
	}

	/**
	 * @param container
	 */
	public void addContainer(UIContainer container) {
		this.containers.add(container);
	}


}
