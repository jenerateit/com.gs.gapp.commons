package com.gs.gapp.metamodel.ui.component;

/**
 * Marker interface to indicate that an input component
 * can handle direct input of text.
 * 
 * @author marcu
 *
 */
public interface TextValueInputable {

}
