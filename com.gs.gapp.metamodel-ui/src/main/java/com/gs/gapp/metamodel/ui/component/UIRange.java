/**
 *
 */
package com.gs.gapp.metamodel.ui.component;


/**
 * @author mmt
 *
 */
public class UIRange extends UIComponent implements TextValueInputable {

	/**
	 *
	 */
	private static final long serialVersionUID = 4439223676063491705L;

	private int lower = 0;
	private int upper = 100;
	private int stepping = 1;
	private Integer length;

	/**
	 * @param name
	 */
	public UIRange(String name) {
		super(name);
	}

	/**
	 * @return the lower
	 */
	public int getLower() {
		return lower;
	}

	/**
	 * @param lower the lower to set
	 */
	public void setLower(int lower) {
		this.lower = lower;
	}

	/**
	 * @return the upper
	 */
	public int getUpper() {
		return upper;
	}

	/**
	 * @param upper the upper to set
	 */
	public void setUpper(int upper) {
		this.upper = upper;
	}

	/**
	 * @return the stepping
	 */
	public int getStepping() {
		return stepping;
	}

	/**
	 * @param stepping the stepping to set
	 */
	public void setStepping(int stepping) {
		this.stepping = stepping;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}
}
