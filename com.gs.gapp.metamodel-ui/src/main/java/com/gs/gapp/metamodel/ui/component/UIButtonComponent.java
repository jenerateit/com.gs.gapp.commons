/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

/**
 * @author mmt
 *
 */
public class UIButtonComponent extends UIActionComponent {

    /**
	 *
	 */
	private static final long serialVersionUID = -4937262789957517074L;

	/**
	 * @param name
	 */
	public UIButtonComponent(String name) {
		super(name);
	}
}
