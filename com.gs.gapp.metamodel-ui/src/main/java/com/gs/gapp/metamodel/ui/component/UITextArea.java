/**
 *
 */
package com.gs.gapp.metamodel.ui.component;


/**
 * @author mmt
 *
 */
public class UITextArea extends UIComponent implements TextValueInputable {

	private Integer length;
	
	/**
	 *
	 */
	private static final long serialVersionUID = 3008319968179637993L;

	/**
	 * @param name
	 */
	public UITextArea(String name) {
		super(name);
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}
}
