/**
 * 
 */
package com.gs.gapp.metamodel.ui;

/**
 * @author mmt
 *
 */
public enum ActionPurpose {

	/**
	 * That's the default setting for action components.
	 */
	UNDEFINED,
	
	/*
	 * Actions with this purpose store data in a database or through a webservice.
	 */
	STORE,
	
	/*
	 * Actions with this purpose cancel the currently ongoing process, e.g. stepping through a wizard.
	 */
	CANCEL,
	
	/*
	 * Actions with this purpose delete data, either transient data or data that is already stored somewhere.
	 */
	DELETE,
	
	/*
	 * Actions with this purpose navigate to an external page or application. Navigations within
	 * one and the same application are accomplished by modeling flows.
	 */
	NAVIGATE,
	
	/*
	 * Actions with this purpose load data from a database or a webservice. Such an action could
	 * for instance be a search or a reload of currently displayed data.
	 */
	LOAD,
	
	/*
	 * Actions with this purpose create new data. After executing this action, the data can be transient
	 * or already stored somewhere.
	 */
	CREATE,
	
	/*
	 * Actions with this purpose assign existing data to other existing data. It is also possible
	 * that such an action is used to assign data that is not yet existing, which means something
	 * like "create and assign" in one go.
	 */
	ASSIGN,
	
	/**
	 * Actions with this purpose are downloading a file. 
	 */
	DOWNLOAD,
	
	/**
	 * The opposite of ASSIGN. Note that UNASSIGN
	 * doesn't delete something. Only the link between two
	 * "things" is removed.
	 */
	UNASSIGN,
	
	/**
	 * e.g. going one step back in a wizard or going back to
	 * the previous view 
	 */
	BACK,
	
	/**
	 * e.g. going one stop forward in a wizard or stepping
	 * through a set of data records
	 */
	NEXT,
	
	/**
	 * create a copy of something
	 */
	COPY,
	
	/**
	 * executing a search
	 */
	SEARCH,
	
	/**
	 * navigating to a different view to edit data
	 * OR
	 * changing the current view from re-only mode to edit mode
	 */
	EDIT,
	;
}
