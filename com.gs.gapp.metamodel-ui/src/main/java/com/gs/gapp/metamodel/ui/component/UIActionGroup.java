/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Denotes a group of action components.
 * 
 * @author mmt
 * @deprecated not used anymore, when you want to nest groups of actions, use a tree of UIActionContainers instead
 *
 */
@Deprecated
public class UIActionGroup extends UIActionComponent {

	/**
	 *
	 */
	private static final long serialVersionUID = -84621358250686306L;

	private final Set<UIActionComponent> actionComponents = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public UIActionGroup(String name) {
		super(name);
	}

	/**
	 * @return the actionComponents
	 */
	public Set<UIActionComponent> getActionComponents() {
		return actionComponents;
	}

	public boolean addActionComponent(UIActionComponent component) {
		return this.actionComponents.add(component);
	}
}
