/**
 *
 */
package com.gs.gapp.metamodel.ui.component;

import com.gs.gapp.metamodel.basic.TemporalType;

/**
 * @author mmt
 *
 */
public class UIDateSelector extends UIComponent {
	
	private Integer length;

	/**
	 *
	 */
	private static final long serialVersionUID = -7101123231446230010L;

	private TemporalType type;

	/**
	 * @param name
	 */
	public UIDateSelector(String name) {
		super(name);
	}

	/**
	 * @return the type
	 */
	public TemporalType getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(TemporalType type) {
		this.type = type;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}
}
