/**
 * 
 */
package com.gs.gapp.metamodel.ui;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.ui.component.UITextField;
import com.gs.gapp.metamodel.ui.container.UIViewContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * @author marcu
 *
 */
public class ValidatorDuplicateContainersInViewTest {

	@Test
	public void test() {
		Collection<Object> modelElements = new ArrayList<>();
		
		UIModule uiModule = new UIModule("MyUiModule");
		
		UIViewContainer viewContainer = new UIViewContainer("MyPage");
		modelElements.add(viewContainer);
		viewContainer.setModule(uiModule);
		
		UIDataContainer dataContainer1 = new UIDataContainer("MyDataContainer");
		modelElements.add(dataContainer1);
		dataContainer1.setModule(uiModule);
		UIDataContainer dataContainer2 = new UIDataContainer("MyDataContainer");
		modelElements.add(dataContainer2);
		dataContainer2.setModule(uiModule);
		
		UITextField textField1 = new UITextField("MyTextField1");
		dataContainer2.addComponent(textField1);
		UITextField textField2 = new UITextField("MyTextField1");
		dataContainer2.addComponent(textField2);
		
		UIDataContainer dataContainer3 = new UIDataContainer("MyDataContainer2");
		modelElements.add(dataContainer3);
		dataContainer3.setModule(uiModule);
		UIDataContainer dataContainer4 = new UIDataContainer("MyDataContainer2");
		modelElements.add(dataContainer4);
		dataContainer4.setModule(uiModule);
		
		viewContainer.addChildContainer(dataContainer1);
		viewContainer.addChildContainer(dataContainer2);
		viewContainer.addChildContainer(dataContainer3);
		viewContainer.addChildContainer(dataContainer4);
		
		
		
		Collection<Message> validationMessages = new ValidatorDuplicateContainersInView().validate(modelElements);
		
		validationMessages.stream().forEach(m -> System.out.println(m.getMessage()));
		
	}

}
