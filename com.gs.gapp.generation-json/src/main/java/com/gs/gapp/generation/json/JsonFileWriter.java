/**
 * 
 */
package com.gs.gapp.generation.json;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.json.JsonFile;

/**
 * @author mmt
 *
 */
public class JsonFileWriter extends AbstractWriter {
	
	@ModelElement
	private JsonFile jsonFile;

	/**
	 * 
	 */
	public JsonFileWriter() {}

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
    public void transform(TargetSection ts) throws WriterException {
		if (jsonFile.getContent() == null || jsonFile.getContent().length == 0) {
			throw new NullPointerException("text file object does not have any content (name:" + jsonFile.getUri().toString() + ")");
		}
		
		write(jsonFile.getContent());
	}
}
