/**
 * 
 */
package com.gs.gapp.generation.json;

import java.io.IOException;
import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jenerateit.target.AbstractTextTargetDocument;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author mmt
 *
 */
public class JsonFileTargetDocument extends AbstractTextTargetDocument {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2369637221123399820L;

	public static final TargetSection DOCUMENT = new TargetSection("document", 10);
	
	public static final ObjectMapper MAPPER = new ObjectMapper();

	private static final SortedSet<TargetSection> SECTIONS = new TreeSet<>(
			Arrays.asList(new TargetSection[] {
					DOCUMENT
			}));
	
	static {
		MAPPER.getFactory().configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
	}
	
	private StringBuilder jsonContent;

	/**
	 * 
	 */
	public JsonFileTargetDocument() {}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TextTargetDocumentI#getCommentEnd()
	 */
	@Override
	public CharSequence getCommentEnd() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TextTargetDocumentI#getCommentStart()
	 */
	@Override
	public CharSequence getCommentStart() {
		return "//";
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TextTargetDocumentI#getPrefixChar()
	 */
	@Override
	public char getPrefixChar() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TargetDocumentI#getTargetSections()
	 */
	@Override
	public SortedSet<TargetSection> getTargetSections() {
		return SECTIONS;
	}

	/**
	 * Clear the import cache and call the super class method {@link AbstractTextTargetDocument#analyzeDocument()}
	 * 
	 * @see AbstractTextTargetDocument#analyzeDocument()
	 */
	@Override
	protected void analyzeDocument() {
		this.jsonContent = new StringBuilder();
		
		super.analyzeDocument();
	}


	/**
	 * Calls the super class method to check for developer areas and 
	 * then check the given line for an import statement.
	 * 
	 * @param lineNumber number of the line of the current file
	 * @param line the line to check
	 * @param start the start position in the target document of the line
	 * @param end the end position in the target document of the line
	 * @see AbstractTextTargetDocument#analyzeLine(int, java.lang.String, int, int)
	 */
	@Override
	protected void analyzeLine(int lineNumber, String line, int start, int end) {
		super.analyzeLine(lineNumber, line, start, end);
		
		jsonContent.append(line);
	}

	public JsonNode getJsonContent() {
		if (jsonContent != null && jsonContent.length() > 0) {
			try {
				JsonNode jsonNode = MAPPER.readTree(jsonContent.toString());
				return jsonNode;
			} catch (IOException ex) {
				ex.printStackTrace();
				throw new WriterException("problems while reading existing JSON data", ex);
			}
		}
		
		return null;
	}
}
