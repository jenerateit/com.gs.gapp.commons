/**
 * 
 */
package com.gs.gapp.generation.json;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetLifecycleListenerI;

import com.gs.gapp.generation.basic.TargetModelizer;
import com.gs.gapp.generation.basic.target.BasicTextTarget;
import com.gs.gapp.metamodel.json.JsonFile;

/**
 * @author mmt
 *
 */
public class ImmutableJsonFileTarget extends BasicTextTarget<JsonFileTargetDocument> {
	
	private static final TargetLifecycleListenerI TARGET_MODELIZER = new TargetModelizer();
	
	public ImmutableJsonFileTarget() {
		super();
		if (com.gs.gapp.metamodel.basic.ModelElement.isAnalyticsMode()) {
		    addTargetLifecycleListener(TARGET_MODELIZER);
		}
	}
	
	@ModelElement
	private JsonFile jsonFile;

	@Override
	protected URI getTargetURI() {
		StringBuilder sb = new StringBuilder(getTargetRoot()).append("/").append(getTargetPrefix()).append("/").append(jsonFile.getUri());
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, this);
		}
	}

	public JsonFile getJsonFile() {
		return jsonFile;
	}
	
	
}
