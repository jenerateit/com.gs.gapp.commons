/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 *
 */

package com.gs.gapp.generation.json;

import java.io.IOException;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashSet;

import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.AbstractTarget;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetLifecycleListenerI;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.flipkart.zjsonpatch.DiffFlags;
import com.flipkart.zjsonpatch.JsonDiff;
import com.flipkart.zjsonpatch.JsonPatch;
import com.gs.gapp.metamodel.json.JsonFile;

/**
 * Helper class to merge new Json nodes from a previous {@link JsonFileTargetDocument} into a new {@link JsonFileTargetDocument}.
 *
 * @see AbstractTarget#addTargetLifecycleListener(TargetLifecycleListenerI)
 * @author mmt
 */
public final class JsonMerger implements TargetLifecycleListenerI {

	private final HashSet<Operation> operations = new HashSet<>();
	
	public JsonMerger() {
		operations.addAll(Arrays.asList(Operation.values()));
	}
	
	public JsonMerger(Operation ...appliedOperations) {
		if (appliedOperations != null) {
		    operations.addAll(Arrays.asList(appliedOperations));
		}
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TargetLifecycleListenerI#preTransform(org.jenerateit.target.TargetI)
	 */
	@Override
	public void preTransform(TargetI<?> target) {
		if (!MutableJsonFileTarget.class.isInstance(target)) {
			throw new JenerateITException("Got a preTransform(TargetI) event with a target of type '" +
					target.getClass() + "' but I expect a target of type '" + MutableJsonFileTarget.class + "'");

		} else {
			WriterI writer = target.getBaseWriter();
			if (!JsonFileWriter.class.isInstance(writer)) {
				throw new JenerateITException("Got a preTransform(TargetI) event with a writer of type '" +
						writer.getClass() + "' but I expect a writer of type '" + JsonFileWriter.class + "'");

			} else {
				
				MutableJsonFileTarget jsonFileTarget = MutableJsonFileTarget.class.cast(target);
				@SuppressWarnings("unused")
				JsonFileWriter jsonFileWriter = JsonFileWriter.class.cast(writer);
				JsonFile jsonFile = jsonFileTarget.getJsonFile();
				JsonFileTargetDocument newTargetDocument = jsonFileTarget.getNewTargetDocument();
				JsonFileTargetDocument prevTargetDocument = jsonFileTarget.getPreviousTargetDocument();

				if (newTargetDocument != null && prevTargetDocument != null) {  // only in case there is a previous target document, it makes sense to do the merging
					JsonNode previousJsonContent = prevTargetDocument.getJsonContent();
					if (previousJsonContent != null) {
						// only in this case the previous file has JSON content, we can merge the two JSON documents
						ObjectWriter writerWithDefaultPrettyPrinter = JsonFileTargetDocument.MAPPER.writerWithDefaultPrettyPrinter();
						try {
							JsonNode newJsonContent = JsonFileTargetDocument.MAPPER.readTree(jsonFile.getContent());
							ArrayNode patch = (ArrayNode) JsonDiff.asJson(newJsonContent, previousJsonContent, EnumSet.of(DiffFlags.OMIT_MOVE_OPERATION, DiffFlags.OMIT_COPY_OPERATION, DiffFlags.ADD_ORIGINAL_VALUE_ON_REPLACE));
							ArrayNode adaptedPatch = JsonFileTargetDocument.MAPPER.createArrayNode();
							for (int ii=0; ii<patch.size(); ii++) {
								ObjectNode patchOperation = (ObjectNode) patch.get(ii);
								
								TextNode textNode = (TextNode) patchOperation.get("op");
								Operation operation = Operation.valueOf(textNode.textValue().toUpperCase());
								if (operations.contains(operation)) {
								    adaptedPatch.add(patchOperation);
								}
							}
							
							if (jsonFileTarget.getTargetURI().toString().contains("KarteLesenShortTestInputData")) {
								@SuppressWarnings("unused")
								String previousContentAsString = writerWithDefaultPrettyPrinter.writeValueAsString(previousJsonContent);
								@SuppressWarnings("unused")
								String newContentAsString = writerWithDefaultPrettyPrinter.writeValueAsString(newJsonContent);
								@SuppressWarnings("unused")
								String patchContentAsString = writerWithDefaultPrettyPrinter.writeValueAsString(patch);
								@SuppressWarnings("unused")
								String adaptedPatchContentAsString = writerWithDefaultPrettyPrinter.writeValueAsString(adaptedPatch);
							}
							
							JsonPatch.applyInPlace(adaptedPatch, newJsonContent);
							String mergedJsonContent = writerWithDefaultPrettyPrinter.writeValueAsString(newJsonContent);
							jsonFile.setContent(mergedJsonContent.getBytes());
						} catch (IOException ex) {
							ex.printStackTrace();
							throw new WriterException("problems during the processing of JSON data", ex);
						}
					}
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TargetLifecycleListenerI#postTransform(org.jenerateit.target.TargetI)
	 */
	@Override
	public void postTransform(TargetI<?> target) {}

	@Override
	public void onFound(TargetI<?> target) {
		preTransform(target);
	}

	@Override
	public void onLoaded(TargetI<?> target) {}
	
	/**
	 * @author marcu
	 *
	 */
	public static enum Operation {
		ADD,
		REMOVE,
		REPLACE,
		MOVE,
		COPY,
		TEST;
	}

}
