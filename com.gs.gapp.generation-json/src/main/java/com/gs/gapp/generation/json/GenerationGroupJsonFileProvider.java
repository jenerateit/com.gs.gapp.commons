/**
 * 
 */
package com.gs.gapp.generation.json;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupProviderI;

/**
 * @author mmt
 *
 */
public class GenerationGroupJsonFileProvider implements GenerationGroupProviderI {

	/**
	 * 
	 */
	public GenerationGroupJsonFileProvider() {}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.GenerationGroupProviderI#getGenerationGroupConfig()
	 */
	@Override
	public GenerationGroupConfigI getGenerationGroupConfig() {
		return new GenerationGroupJsonFile();
	}
}
