package com.gs.gapp.generation.json;

import java.util.Set;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.WriterLocatorI;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.basic.AbstractGenerationGroup;
import com.gs.gapp.generation.basic.AbstractMetatypeFilter;
import com.gs.gapp.generation.basic.AbstractWriterLocator;
import com.gs.gapp.generation.basic.WriterMapper;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.json.JsonFile;

public class GenerationGroupJsonFile extends AbstractGenerationGroup implements GenerationGroupConfigI {
	
	private final WriterLocatorI writerLocator;

	public GenerationGroupJsonFile() {
		super(new ModelElementCache());
		
		addTargetClass(ImmutableJsonFileTarget.class);
		addTargetClass(MutableJsonFileTarget.class);
		
		writerLocator = new WriterLocatorJsonFile(getAllTargets());
	}
	
	@Override
	public WriterLocatorI getWriterLocator() {
		return writerLocator;
	}
	
	/**
	 * @author marcu
	 *
	 */
	public static class ImmutableFileWriterMapper extends WriterMapper {

		public ImmutableFileWriterMapper(Class<?> sourceClass, Class<? extends TargetI<? extends TargetDocumentI>> targetClass, Class<? extends WriterI> writerClass) {
			super(sourceClass, targetClass, writerClass);
		}

		@Override
		public boolean isResponsibleForElement(ModelElementI element) {
			boolean generallyResponsible = super.isResponsibleForElement(element);
			
			if (generallyResponsible) {
				if (element instanceof JsonFile) {
					JsonFile jsonFile = (JsonFile) element;
					if (!jsonFile.isImmutable()) {
						return false;
					}
				}
			}
			
			return generallyResponsible;
		}
	}
	
	/**
	 * @author marcu
	 *
	 */
	public static class MutableFileWriterMapper extends WriterMapper {

		public MutableFileWriterMapper(Class<?> sourceClass, Class<? extends TargetI<? extends TargetDocumentI>> targetClass, Class<? extends WriterI> writerClass) {
			super(sourceClass, targetClass, writerClass);
		}

		@Override
		public boolean isResponsibleForElement(ModelElementI element) {
			boolean generallyResponsible = super.isResponsibleForElement(element);
			
			if (generallyResponsible) {
				if (element instanceof JsonFile) {
					JsonFile jsonFile = (JsonFile) element;
					if (jsonFile.isImmutable()) {
						return false;
					}
				}
			}
			
			return generallyResponsible;
		}
	}
	
	/**
	 * @author marcu
	 *
	 */
	public static class WriterLocatorJsonFile extends AbstractWriterLocator implements WriterLocatorI {
		
		private MetatypeFilter metatypeFilter = new MetatypeFilter();
		
		private final boolean checkMetatypeFilter;

		public WriterLocatorJsonFile(Set<Class<? extends TargetI<?>>> targetClasses, boolean checkMetatypeFilter) {
			super(targetClasses);
			this.checkMetatypeFilter = checkMetatypeFilter;
			
			// --- generation decisions
			addWriterMapperForGenerationDecision( new ImmutableFileWriterMapper(JsonFile.class, ImmutableJsonFileTarget.class, JsonFileWriter.class) );
			addWriterMapperForGenerationDecision( new MutableFileWriterMapper(JsonFile.class, MutableJsonFileTarget.class, JsonFileWriter.class) );
			
		}
		
		public WriterLocatorJsonFile(Set<Class<? extends TargetI<?>>> targetClasses) {
			this(targetClasses, true);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * com.gs.gapp.generation.java.WriterLocatorJava#getWriterClass(java.io.
		 * Serializable, java.lang.Class)
		 */
		@Override
		public Class<? extends WriterI> getWriterClass(Object element, Class<? extends TargetI<?>> targetClass) {
			Class<? extends WriterI> result = null;
			

			if (checkMetatypeFilter && metatypeFilter.isTargetGeneratedForMetaType(element.getClass()) == false) {
				// not generating anything 
			} else {
				result = super.getWriterClass(element, targetClass);
//				System.out.println("result=" + (result == null ? "null" : result.getSimpleName()) + " for: element class=" + element.getClass().getSimpleName() + ", target class=" + targetClass.getSimpleName());
			}

			
			return result;
		}
	}
	
	/**
	 * @author marcu
	 *
	 */
	/**
	 * @author marcu
	 *
	 */
	public static class MetatypeFilter extends AbstractMetatypeFilter {

		{
			addMetaTypeWithTargetGeneration(JsonFile.class);
		}

	}
}
