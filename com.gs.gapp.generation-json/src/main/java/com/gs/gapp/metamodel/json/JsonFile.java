package com.gs.gapp.metamodel.json;

import com.gs.gapp.metamodel.anyfile.AnyFile;

public class JsonFile extends AnyFile {

	private static final long serialVersionUID = -2292435548347419755L;
	
	private final boolean immutable;
	
	public JsonFile(String name, String path, boolean immutable) {
		super(name, path, "json");
		this.immutable = immutable;
	}

	@Override
	public void setBinary(boolean binary) {
		if (binary) {
			throw new RuntimeException("attempted to set binary=true for a JSON file, which is not allowed");
		}
		super.setBinary(binary);
	}

	@Override
	public boolean isBinary() {
		return false;
	}

	public boolean isImmutable() {
		return immutable;
	}
	
}
