/**
 * 
 */
package com.gs.gapp.metamodel.openapi;

import io.swagger.models.ComposedModel;
import io.swagger.models.Swagger;

/**
 * @author mmt
 *
 */
public class ComposedModelWrapper extends AbstractModelWrapper {

	private static final long serialVersionUID = -5900331845065675162L;
	
	public ComposedModelWrapper(String name, ComposedModel composedModel, Swagger openApiModel) {
		super(name, composedModel, openApiModel);
	}

	public ComposedModel getComposedModel() {
		return (ComposedModel) getAbstractModel();
	}
}
