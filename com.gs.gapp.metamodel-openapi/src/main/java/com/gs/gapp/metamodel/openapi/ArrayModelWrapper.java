/**
 * 
 */
package com.gs.gapp.metamodel.openapi;

import io.swagger.models.ArrayModel;
import io.swagger.models.Swagger;

/**
 * @author mmt
 *
 */
public class ArrayModelWrapper extends AbstractModelWrapper {

	private static final long serialVersionUID = -5900331845065675162L;
	
	public ArrayModelWrapper(String name, ArrayModel arrayModel, Swagger openApiModel) {
		super(name, arrayModel, openApiModel);
	}

	public ArrayModel getArrayModel() {
		return (ArrayModel) getAbstractModel();
	}
}
