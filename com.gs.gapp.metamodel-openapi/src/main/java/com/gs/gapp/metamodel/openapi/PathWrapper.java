/**
 * 
 */
package com.gs.gapp.metamodel.openapi;

import io.swagger.models.Path;
import io.swagger.models.Swagger;

/**
 * @author mmt
 *
 */
public class PathWrapper extends AbstractWrapper {

	private static final long serialVersionUID = -5900331845065675162L;
	
	private final Path path;
	
	
	public PathWrapper(String name, Path path, Swagger openApiModel) {
		super(name, openApiModel);
		this.path = path;
	}

	public Path getPath() {
		return path;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PathWrapper other = (PathWrapper) obj;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		return true;
	}
}
