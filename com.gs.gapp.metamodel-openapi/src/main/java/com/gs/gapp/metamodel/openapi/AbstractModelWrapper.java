/**
 * 
 */
package com.gs.gapp.metamodel.openapi;

import io.swagger.models.AbstractModel;
import io.swagger.models.Swagger;

/**
 * @author mmt
 *
 */
public abstract class AbstractModelWrapper extends AbstractWrapper {

	private static final long serialVersionUID = -5900331845065675162L;
	
	private final AbstractModel abstractModel;
	
	public AbstractModelWrapper(String name, AbstractModel abstractModel, Swagger openApiModel) {
		super(name, openApiModel);
		this.abstractModel = abstractModel;
	}

	protected AbstractModel getAbstractModel() {
		return abstractModel;
	}
}
