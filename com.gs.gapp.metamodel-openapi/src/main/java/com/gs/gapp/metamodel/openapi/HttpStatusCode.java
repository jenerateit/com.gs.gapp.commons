package com.gs.gapp.metamodel.openapi;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO move this to a different project in the future, e.g. to a project 'metamodel-web' or 'metamodel-http'
 * 
 * @author mmt
 *
 */
public enum HttpStatusCode {

    // --- 1xx Informational ---
	/** <tt>100 Continue</tt> (HTTP/1.1 - RFC 2616) */
    SC_CONTINUE (100, "<tt>100 Continue</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>101 Switching Protocols</tt> (HTTP/1.1 - RFC 2616)*/
    SC_SWITCHING_PROTOCOLS (101, "<tt>101 Switching Protocols</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>102 Processing</tt> (WebDAV - RFC 2518) */
    SC_PROCESSING (102, "<tt>102 Processing</tt> (WebDAV - RFC 2518)"),

    // --- 2xx Success ---
    /** <tt>200 OK</tt> (HTTP/1.0 - RFC 1945) */
    SC_OK (200, "<tt>200 OK</tt> (HTTP/1.0 - RFC 1945)"),
    /** <tt>201 Created</tt> (HTTP/1.0 - RFC 1945) */
    SC_CREATED (201, "<tt>201 Created</tt> (HTTP/1.0 - RFC 1945)"),
    /** <tt>202 Accepted</tt> (HTTP/1.0 - RFC 1945) */
    SC_ACCEPTED (202, "<tt>202 Accepted</tt> (HTTP/1.0 - RFC 1945)"),
    /** <tt>203 Non Authoritative Information</tt> (HTTP/1.1 - RFC 2616) */
    SC_NON_AUTHORITATIVE_INFORMATION (203, "<tt>203 Non Authoritative Information</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>204 No Content</tt> (HTTP/1.0 - RFC 1945) */
    SC_NO_CONTENT (204, "<tt>204 No Content</tt> (HTTP/1.0 - RFC 1945)"),
    /** <tt>205 Reset Content</tt> (HTTP/1.1 - RFC 2616) */
    SC_RESET_CONTENT (205, "<tt>205 Reset Content</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>206 Partial Content</tt> (HTTP/1.1 - RFC 2616) */
    SC_PARTIAL_CONTENT (206, "<tt>206 Partial Content</tt> (HTTP/1.1 - RFC 2616)"),
    /**
     * <tt>207 Multi-Status</tt> (WebDAV - RFC 2518) or <tt>207 Partial Update
     * OK</tt> (HTTP/1.1 - draft-ietf-http-v11-spec-rev-01?)
     */
    SC_MULTI_STATUS (207, "<tt>207 Multi-Status</tt> (WebDAV - RFC 2518) or <tt>207 Partial Update OK</tt> (HTTP/1.1 - draft-ietf-http-v11-spec-rev-01?)"),
    
    // --- 3xx Redirection ---
    /** <tt>300 Mutliple Choices</tt> (HTTP/1.1 - RFC 2616) */
    SC_MULTIPLE_CHOICES (300, "<tt>300 Mutliple Choices</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>301 Moved Permanently</tt> (HTTP/1.0 - RFC 1945) */
    SC_MOVED_PERMANENTLY (301, "<tt>301 Moved Permanently</tt> (HTTP/1.0 - RFC 1945)"),
    /** <tt>302 Moved Temporarily</tt> (Sometimes <tt>Found</tt>) (HTTP/1.0 - RFC 1945) */
    SC_MOVED_TEMPORARILY (302, "<tt>302 Moved Temporarily</tt> (Sometimes <tt>Found</tt>) (HTTP/1.0 - RFC 1945)"),
    /** <tt>303 See Other</tt> (HTTP/1.1 - RFC 2616) */
    SC_SEE_OTHER (303, "<tt>303 See Other</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>304 Not Modified</tt> (HTTP/1.0 - RFC 1945) */
    SC_NOT_MODIFIED (304, "<tt>304 Not Modified</tt> (HTTP/1.0 - RFC 1945)"),
    /** <tt>305 Use Proxy</tt> (HTTP/1.1 - RFC 2616) */
    SC_USE_PROXY (305, "<tt>305 Use Proxy</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>307 Temporary Redirect</tt> (HTTP/1.1 - RFC 2616) */
    SC_TEMPORARY_REDIRECT (307, "<tt>307 Temporary Redirect</tt> (HTTP/1.1 - RFC 2616)"),

    // --- 4xx Client Error ---
    /** <tt>400 Bad Request</tt> (HTTP/1.1 - RFC 2616) */
    SC_BAD_REQUEST (400, "<tt>400 Bad Request</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>401 Unauthorized</tt> (HTTP/1.0 - RFC 1945) */
    SC_UNAUTHORIZED (401, "<tt>401 Unauthorized</tt> (HTTP/1.0 - RFC 1945)"),
    /** <tt>402 Payment Required</tt> (HTTP/1.1 - RFC 2616) */
    SC_PAYMENT_REQUIRED (402, "<tt>402 Payment Required</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>403 Forbidden</tt> (HTTP/1.0 - RFC 1945) */
    SC_FORBIDDEN (403, "<tt>403 Forbidden</tt> (HTTP/1.0 - RFC 1945)"),
    /** <tt>404 Not Found</tt> (HTTP/1.0 - RFC 1945) */
    SC_NOT_FOUND (404, "<tt>404 Not Found</tt> (HTTP/1.0 - RFC 1945)"),
    /** <tt>405 Method Not Allowed</tt> (HTTP/1.1 - RFC 2616) */
    SC_METHOD_NOT_ALLOWED (405, "<tt>405 Method Not Allowed</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>406 Not Acceptable</tt> (HTTP/1.1 - RFC 2616) */
    SC_NOT_ACCEPTABLE (406, "<tt>406 Not Acceptable</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>407 Proxy Authentication Required</tt> (HTTP/1.1 - RFC 2616) */
    SC_PROXY_AUTHENTICATION_REQUIRED (407, "<tt>407 Proxy Authentication Required</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>408 Request Timeout</tt> (HTTP/1.1 - RFC 2616) */
    SC_REQUEST_TIMEOUT (408, "<tt>408 Request Timeout</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>409 Conflict</tt> (HTTP/1.1 - RFC 2616) */
    SC_CONFLICT (409, "<tt>409 Conflict</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>410 Gone</tt> (HTTP/1.1 - RFC 2616) */
    SC_GONE (410, "<tt>410 Gone</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>411 Length Required</tt> (HTTP/1.1 - RFC 2616) */
    SC_LENGTH_REQUIRED (411, "<tt>411 Length Required</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>412 Precondition Failed</tt> (HTTP/1.1 - RFC 2616) */
    SC_PRECONDITION_FAILED (412, "<tt>412 Precondition Failed</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>413 Request Entity Too Large</tt> (HTTP/1.1 - RFC 2616) */
    SC_REQUEST_TOO_LONG (413, "<tt>413 Request Entity Too Large</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>414 Request-URI Too Long</tt> (HTTP/1.1 - RFC 2616) */
    SC_REQUEST_URI_TOO_LONG (414, "<tt>414 Request-URI Too Long</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>415 Unsupported Media Type</tt> (HTTP/1.1 - RFC 2616) */
    SC_UNSUPPORTED_MEDIA_TYPE (415, "<tt>415 Unsupported Media Type</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>416 Requested Range Not Satisfiable</tt> (HTTP/1.1 - RFC 2616) */
    SC_REQUESTED_RANGE_NOT_SATISFIABLE (416, "<tt>416 Requested Range Not Satisfiable</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>417 Expectation Failed</tt> (HTTP/1.1 - RFC 2616) */
    SC_EXPECTATION_FAILED (417, "<tt>417 Expectation Failed</tt> (HTTP/1.1 - RFC 2616)"),

    /** <tt>418 I'm a Teapot!</tt>*/
    SC_418_UNUSED (418, "<tt>418 I'm a Teapot!</tt>"),
    
    /**
     * Static constant for a 419 error.
     * <tt>419 Insufficient Space on Resource</tt>
     * (WebDAV - draft-ietf-webdav-protocol-05?)
     * or <tt>419 Proxy Reauthentication Required</tt>
     * (HTTP/1.1 drafts?)
     */
    SC_INSUFFICIENT_SPACE_ON_RESOURCE (419, "Static constant for a 419 error. <tt>419 Insufficient Space on Resource</tt> (WebDAV - draft-ietf-webdav-protocol-05?) or <tt>419 Proxy Reauthentication Required</tt> (HTTP/1.1 drafts?)"),
    /**
     * Static constant for a 420 error.
     * <tt>420 Method Failure</tt>
     * (WebDAV - draft-ietf-webdav-protocol-05?)
     */
    SC_METHOD_FAILURE (420, "Static constant for a 420 error. <tt>420 Method Failure</tt> (WebDAV - draft-ietf-webdav-protocol-05?)"),
    /** <tt>421 Misdirected Request</tt> (introduced in HTTP/2) */
    SC_MISSDIRECTED_REQUEST (421, "<tt>421 Misdirected Request</tt> (introduced in HTTP/2)"),
    /** <tt>422 Unprocessable Entity</tt> (WebDAV - RFC 2518) */
    SC_UNPROCESSABLE_ENTITY (422, "<tt>422 Unprocessable Entity</tt> (WebDAV - RFC 2518)"),
    /** <tt>423 Locked</tt> (WebDAV - RFC 2518) */
    SC_LOCKED (423, "<tt>423 Locked</tt> (WebDAV - RFC 2518)"),
    /** <tt>424 Failed Dependency</tt> (WebDAV - RFC 2518) */
    SC_FAILED_DEPENDENCY (424, "<tt>424 Failed Dependency</tt> (WebDAV - RFC 2518)"),
    /** <tt>425 Too Early</tt> */
    SC_TOO_EARLY (425, "<tt>425 Too Early</tt>"),
    /** <tt>426 Upgrade Required</tt> */
    SC_UPGRADE_REQUIRED (426, "<tt>426 Upgrade Required</tt>"),
    /** <tt>428 Precondition Required</tt> */
    SC_PRECONDITION_REQUIRED (428, "<tt>428 Precondition Required</tt>"),
    /** <tt>429 To Many Requests</tt> */
    SC_TOO_MANY_REQUESTS (429, "<tt>429 To Many Requests</tt>"),
    /** <tt>431 Request Header Fields Too Large</tt> */
    SC_REQUEST_HEADER_FIELDS_TOO_LARGE (431, "<tt>431 Request Header Fields Too Large</tt>"),
    /** <tt>451 Unavailable For Legal Reasons</tt> */
    SC_UNAVAILABLE_FOR_LEGAL_REASONS (451, "<tt>451 Unavailable For Legal Reasons</tt>"),
    
    // --- 5xx Server Error ---
    /** <tt>500 Server Error</tt> (HTTP/1.0 - RFC 1945) */
    SC_INTERNAL_SERVER_ERROR (500, "<tt>500 Server Error</tt> (HTTP/1.0 - RFC 1945)"),
    /** <tt>501 Not Implemented</tt> (HTTP/1.0 - RFC 1945) */
    SC_NOT_IMPLEMENTED (501, "<tt>501 Not Implemented</tt> (HTTP/1.0 - RFC 1945)"),
    /** <tt>502 Bad Gateway</tt> (HTTP/1.0 - RFC 1945) */
    SC_BAD_GATEWAY (502, "<tt>502 Bad Gateway</tt> (HTTP/1.0 - RFC 1945)"),
    /** <tt>503 Service Unavailable</tt> (HTTP/1.0 - RFC 1945) */
    SC_SERVICE_UNAVAILABLE (503, "<tt>503 Service Unavailable</tt> (HTTP/1.0 - RFC 1945)"),
    /** <tt>504 Gateway Timeout</tt> (HTTP/1.1 - RFC 2616) */
    SC_GATEWAY_TIMEOUT (504, "<tt>504 Gateway Timeout</tt> (HTTP/1.1 - RFC 2616)"),
    /** <tt>505 HTTP Version Not Supported</tt> (HTTP/1.1 - RFC 2616) */
    SC_HTTP_VERSION_NOT_SUPPORTED (505, "<tt>505 HTTP Version Not Supported</tt> (HTTP/1.1 - RFC 2616)"),

    /** <tt>507 Insufficient Storage</tt> (WebDAV - RFC 2518) */
    SC_INSUFFICIENT_STORAGE (507, "<tt>507 Insufficient Storage</tt> (WebDAV - RFC 2518)"),
	;
	
	private static final Map<Integer, HttpStatusCode> map = new HashMap<>();
	
	static {
		for (HttpStatusCode httpStatusCode : HttpStatusCode.values()) {
			map.put(httpStatusCode.getCode(), httpStatusCode);
		}
	}
	
	/**
	 * @param code
	 * @return
	 */
	public static HttpStatusCode get(Integer code) {
		return map.get(code);
	}
	
	/**
	 * @param codeAsString
	 * @return
	 */
	public static HttpStatusCode get(String codeAsString) {
		try {
			Integer code = Integer.parseInt(codeAsString);
			return get(code);
		} catch (Throwable th) {/*intentionally ignoring this*/}
		
		return null;
	}
	
	private final Integer code;
	private final String description;
	
	private HttpStatusCode(Integer code, String description) {
		this.code = code;
		this.description = description;
	}

	public Integer getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}
	
	public boolean isInformational() {
		return code >= 100 && code < 200;
	}
	
	public boolean isSuccess() {
		return code >= 200 && code < 300;
	}
	
	public boolean isRedirection() {
		return code >= 300 && code < 400;
	}
	
	public boolean isClientError() {
		return code >= 400 && code < 500;
	}
	
	public boolean isServerError() {
		return code >= 500 && code < 600;
	}
}
