/**
 * 
 */
package com.gs.gapp.metamodel.openapi;

import io.swagger.models.Swagger;
import io.swagger.models.properties.AbstractProperty;
import io.swagger.models.properties.Property;

/**
 * @author mmt
 *
 */
public class PropertyWrapper extends AbstractWrapper {

	private static final long serialVersionUID = -5900331845065675162L;
	
	private final AbstractProperty property;
	
	public PropertyWrapper(String name, Property property, Swagger openApiModel) {
		super(name, openApiModel);
		this.property = (AbstractProperty) property;
	}

	public AbstractProperty getProperty() {
		return property;
	}
}
