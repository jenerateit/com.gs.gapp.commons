/**
 * 
 */
package com.gs.gapp.metamodel.openapi;

import io.swagger.models.ModelImpl;
import io.swagger.models.Swagger;

/**
 * @author mmt
 *
 */
public class ModelImplWrapper extends AbstractModelWrapper {

	private static final long serialVersionUID = -5900331845065675162L;
	
	public ModelImplWrapper(String name, ModelImpl modelImpl, Swagger openApiModel) {
		super(name, modelImpl, openApiModel);
	}

	public ModelImpl getModelImpl() {
		return (ModelImpl) getAbstractModel();
	}
}
