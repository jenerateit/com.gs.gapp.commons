/**
 * 
 */
package com.gs.gapp.metamodel.openapi;

import io.swagger.models.Swagger;
import io.swagger.models.parameters.Parameter;

/**
 * @author mmt
 *
 */
public class ParameterWrapper extends AbstractWrapper {

	private static final long serialVersionUID = -5900331845065675162L;
	
	private final Parameter parameter;
	
	public ParameterWrapper(String name, Parameter parameter, Swagger openApiModel) {
		super(name, openApiModel);
		this.parameter = parameter;
	}

	public Parameter getParameter() {
		return parameter;
	}
}
