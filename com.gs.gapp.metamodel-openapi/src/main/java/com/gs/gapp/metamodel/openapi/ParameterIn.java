package com.gs.gapp.metamodel.openapi;

public enum ParameterIn {

	QUERY ("query"),
	HEADER ("header"),
	PATH ("path"),
	COOKIE ("cookie"),
	
	/**
	 * This is available in OpenAPI 2, but not in 3 anymore.
	 */
	FORM_DATA ("formData"),
	/**
	 * This is available in OpenAPI 2, but not in 3 anymore.
	 */
	BODY ("body"),
	;
	
	private final String name;
	
	private ParameterIn(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
