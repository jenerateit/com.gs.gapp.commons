/**
 * 
 */
package com.gs.gapp.metamodel.openapi;

import com.gs.gapp.metamodel.basic.ModelElement;

import io.swagger.models.Swagger;

/**
 * @author mmt
 *
 */
public abstract class AbstractWrapper extends ModelElement {

	private static final long serialVersionUID = -5900331845065675162L;
	
	private final Swagger openApiModel;
	
	public AbstractWrapper(String name, Swagger openApiModel) {
		super(name);
		this.openApiModel = openApiModel;
	}

	public Swagger getOpenApiModel() {
		return openApiModel;
	}
}
