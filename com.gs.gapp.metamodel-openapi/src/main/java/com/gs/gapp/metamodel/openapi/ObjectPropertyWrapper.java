/**
 * 
 */
package com.gs.gapp.metamodel.openapi;

import io.swagger.models.Swagger;
import io.swagger.models.properties.ObjectProperty;

/**
 * @author mmt
 *
 */
public class ObjectPropertyWrapper extends PropertyWrapper {

	private static final long serialVersionUID = -5900331845065675162L;
	
	private final ObjectProperty property;
	
	public ObjectPropertyWrapper(String name, ObjectProperty property, Swagger openApiModel) {
		super(name, property, openApiModel);
		this.property = property;
	}

	@Override
	public ObjectProperty getProperty() {
		return property;
	}
}
