/**
 * 
 */
package com.gs.gapp.metamodel.openapi;

import io.swagger.models.HttpMethod;
import io.swagger.models.Operation;
import io.swagger.models.Swagger;

/**
 * @author mmt
 *
 */
public class OperationWrapper extends AbstractWrapper {

	private static final long serialVersionUID = -5900331845065675162L;
	
	private final HttpMethod httpMethod;
	
	private final Operation operation;
	
	private final PathWrapper pathWrapper;
	
	public OperationWrapper(HttpMethod httpMethod, Operation operation, Swagger openApiModel, PathWrapper pathWrapper) {
		super(httpMethod.name(), openApiModel);
		this.httpMethod = httpMethod;
		this.operation = operation;
		this.pathWrapper = pathWrapper;
	}

	public HttpMethod getHttpMethod() {
		return httpMethod;
	}

	public Operation getOperation() {
		return operation;
	}

	public PathWrapper getPathWrapper() {
		return pathWrapper;
	}
	
	

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#getName()
	 */
	@Override
	public String getName() {
		return this.operation.getOperationId() != null && this.operation.getOperationId().length() > 0 ?
				this.operation.getOperationId() : super.getName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((httpMethod == null) ? 0 : httpMethod.hashCode());
		result = prime * result + ((operation == null) ? 0 : operation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OperationWrapper other = (OperationWrapper) obj;
		if (httpMethod != other.httpMethod)
			return false;
		if (operation == null) {
			if (other.operation != null)
				return false;
		} else if (!operation.equals(other.operation))
			return false;
		return true;
	}
}
