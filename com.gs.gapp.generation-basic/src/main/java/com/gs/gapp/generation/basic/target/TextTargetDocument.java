/*
 * Copyright (c) 2008 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.generation.basic.target;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jenerateit.target.AbstractTextTargetDocument;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.target.TextTargetDocumentI;

/**
 * {@link TargetDocumentI} implementation for a XML target document.
 *
 * @author hrr
 */
public class TextTargetDocument extends AbstractTextTargetDocument {

	/**
	 * The serial version UID.
	 */
	private static final long serialVersionUID = 4671879045492165602L;
	/** information about the whole text file */
	public static final TargetSection DOCUMENT = new TargetSection("document", 10);

	private static final SortedSet<TargetSection> SECTIONS = new TreeSet<>(
			Arrays.asList(new TargetSection[] {
					DOCUMENT
			}));

	/**
	 * Default constructor.
	 */
	public TextTargetDocument() {
		super();
	}

	/**
	 * Getter to request the XML comment end characters
	 *
	 * @return the comment end string
	 * @see TextTargetDocumentI#getCommentEnd()
	 */
	@Override
	public String getCommentEnd() {
		return null;
	}

	/**
	 * Getter to request the XML comment start characters
	 *
	 * @return the comment start string
	 * @see TextTargetDocumentI#getCommentStart()
	 */
	@Override
	public String getCommentStart() {
		return "---";
	}

	/**
	 * Returns all target sections this XML document supports
	 *
	 * @return the target sections of this XML target document implementation
	 * @see #DOCUMENT
	 * @see TargetDocumentI#getTargetSections()
	 */
	@Override
	public SortedSet<TargetSection> getTargetSections() {
		return SECTIONS;
	}

	@Override
	public char getPrefixChar() {
		return ' ';
	}

}
