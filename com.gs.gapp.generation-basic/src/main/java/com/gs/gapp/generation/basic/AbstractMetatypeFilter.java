/**
 *
 */
package com.gs.gapp.generation.basic;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;
import com.gs.gapp.metamodel.basic.Model;

/**
 * The purpose of classes that extend this class is to provide information that is
 * relevant to generation groups. By using this
 * class, code-duplication is avoided.
 *
 * @author mmt
 *
 */
public abstract class AbstractMetatypeFilter implements
		MetatypeFilterI {

	private final Set<Class<? extends Object>> metaTypesWithTargetGeneration = new LinkedHashSet<>();
	
	{
		addMetaTypeWithTargetGeneration(Model.class);
		addMetaTypeWithTargetGeneration(TransformationStepConfiguration.class);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.basic.GenerationGroupHelperI#getMetaTypesWithTargetGeneration()
	 */
	@Override
	public final Set<Class<? extends Object>> getMetaTypesWithTargetGeneration() {
		return metaTypesWithTargetGeneration;
	}

	/**
	 * @param clazz
	 * @return
	 */
	protected final boolean addMetaTypeWithTargetGeneration(Class<? extends Object> clazz) {
		return this.metaTypesWithTargetGeneration.add(clazz);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.generation.basic.GenerationGroupHelperI#isTargetGeneratedForMetaType(java.lang.Class)
	 */
	@Override
	public boolean isTargetGeneratedForMetaType(
			Class<? extends Object> metaType) {
		boolean result = false;

		for (Class<? extends Object> clazz : getMetaTypesWithTargetGeneration()) {
			if (clazz.isAssignableFrom(metaType)) {
				result = true;
				break;
			}
		}

		return result;
	}
}
