/**
 *
 */
package com.gs.gapp.generation.basic;

import java.util.Set;

/**
 * @author mmt
 *
 */
public interface MetatypeFilterI {

	/**
	 *
	 * @return set of meta types where there are target files generated for by a generation group
	 */
	public Set<Class<? extends Object>> getMetaTypesWithTargetGeneration();

	/**
	 *
	 * @param metaType
	 * @return true if the generation group (through its writer locater) provides generation support for the given meta type
	 */
	public boolean isTargetGeneratedForMetaType(Class<? extends Object> metaType);

}
