/**
 *
 */
package com.gs.gapp.generation.basic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jenerateit.generationgroup.WriterLocatorI;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.basic.target.TransformationStepAnalysisTarget;
import com.gs.gapp.generation.basic.target.TransformationStepAnalysisTarget.TransformationStepAnalysisWriter;
import com.gs.gapp.generation.basic.target.TransformationStepsAnalyticsTextTarget;
import com.gs.gapp.generation.basic.target.TransformationStepsAnalyticsTextTarget.TransformationStepsAnalyticsTextWriter;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;


/**
 * @author mmt
 *
 */
public abstract class AbstractWriterLocator implements WriterLocatorI {

	private final Set<Class<? extends TargetI<?>>> targetClasses = new LinkedHashSet<>();

	private final Set<WriterMapper> writerMappersForGenerationDecision = new LinkedHashSet<>();
	private final Map<Class<?>,Set<WriterMapper>> groupedWriterMappersForGenerationDecision = new LinkedHashMap<>();

	private final Set<WriterMapper> writerMappersForWriterDelegation = new LinkedHashSet<>();
	
	private final AbstractGenerationGroup generationGroup;

	/**
	 * @param generationGroup
	 */
	public AbstractWriterLocator(AbstractGenerationGroup generationGroup) {
		this.generationGroup = generationGroup;
		init();
	}
	
	/**
	 * @param targetClasses
	 */
	public AbstractWriterLocator(Set<Class<? extends TargetI<?>>> targetClasses) {
		this.targetClasses.addAll(targetClasses);
		this.generationGroup = null;
		init();
	}
	
	private void init() {
		// --- mapper for target generation decision
		if (ModelElement.isAnalyticsMode()) {
			addWriterMapperForGenerationDecision( new WriterMapper(TransformationStepConfiguration.class, TransformationStepsAnalyticsTextTarget.class, TransformationStepsAnalyticsTextWriter.class) );
		    addWriterMapperForGenerationDecision( new WriterMapper(Model.class, TransformationStepAnalysisTarget.class, TransformationStepAnalysisWriter.class) );
		}
	}

	/**
	 * @param element
	 * @param targetClass
	 * @return
	 */
	@Override
	public Class<? extends WriterI> getWriterClass(Object element,
			Class<? extends TargetI<?>> targetClass) {
		WriterMapper mapper = null;

		if (element instanceof ModelElementI) {
			ModelElementI modelElement = (ModelElementI) element;
			if (modelElement.isGenerated() == true) {
				Class<?> sourceClass = getEffectiveSourceClass(modelElement);

				if (isToBeGenerated(sourceClass, targetClass)) {
					mapper = identifyMapper(modelElement, sourceClass, targetClass);

					if (mapper == null) {
					    // recursively find the right mapper
	                    mapper = findWriterMapper(modelElement, sourceClass, targetClass);
					}
				} else {
					// there is no writer mapping for the given combination of source class and target class => avoid generation
				}
			} else {
				// There are cases, where intentionally a generated-flag is set to false and nonetheless an attempt is taken to get a writer instance
				// for the model element (e.g. when a converter-option tells the converter, not to generate certain files, the generated-flag is being used to get that).
				// => throwing an exception is not desired here
//				throw new RuntimeException("Detected attempt to obtain a writer instance for a model element where there is 'isGenerated()' returning false. element:'" + element + "', target class:'" + targetClass + "'");
			}
		}

		return mapper == null ? null : mapper.getWriterClass();
	}
	
	/**
	 * If the runtime class of the given model element is an anonymous
	 * class, this method returns its superclass.
	 * 
	 * @param element
	 * @return
	 */
	private Class<?> getEffectiveSourceClass(ModelElementI element) {
		Class<?> sourceClass = element.getClass();
		if (sourceClass.isAnonymousClass()) {
			sourceClass = sourceClass.getSuperclass();
		}
		return sourceClass;
	}

	/**
	 * @param modelElement
	 * @param sourceClass
	 * @param targetClass
	 * @return
	 */
	private WriterMapper findWriterMapper(ModelElementI modelElement, Class<?> sourceClass, Class<? extends TargetI<?>> targetClass) {
		WriterMapper result = null;
		List<Class<?>> classListForRecursiveSearch = new ArrayList<>();

    	// --- first check direct parents (first implemented interfaces then parent class
    	for (Class<?> implementedInterface : sourceClass.getInterfaces()) {
    		result = identifyMapper(modelElement, implementedInterface, targetClass);
    		if (result != null) break;
    		classListForRecursiveSearch.add(implementedInterface);
    	}

    	if (result == null) {
	    	Class<?> parentSourceClass = sourceClass.getSuperclass();
	    	if (parentSourceClass != null) {
	    		result = identifyMapper(modelElement, parentSourceClass, targetClass);
	    		if (result == null) {
	    			classListForRecursiveSearch.add(0, parentSourceClass);
	    		}
	    	}
    	}

    	if (result == null) {
    	    // continue searching recursively for parent classes and implemented interfaces
    		for (Class<?> parentType : classListForRecursiveSearch) {
    			result = findWriterMapper(modelElement, parentType, targetClass);
    		}
    	}


	    return result;
	}

	/**
	 * @param sourceClass
	 * @param targetClass
	 * @return
	 */
	private boolean isToBeGenerated(Class<?> sourceClass, Class<? extends TargetI<?>> targetClass) {
		boolean result = false;
		Set<WriterMapper> groupedMappers = groupedWriterMappersForGenerationDecision.get(sourceClass);
		if (groupedMappers != null) {
			for (WriterMapper mapper : groupedMappers) {
				if (mapper.getTargetClass() == targetClass) {
					// this means, the given combination is valid
					result = true;
					break;
				}
			}
		}

		return result;
	}

	/**
	 * @param modelElement
	 * @param sourceClass
	 * @param targetClass
	 * @return
	 */
	private WriterMapper identifyMapper(ModelElementI modelElement, Class<?> sourceClass, Class<? extends TargetI<?>> targetClass) {
		WriterMapper result = null;
		Set<WriterMapper> mappers = getWriterMappersForGenerationDecision(sourceClass, targetClass, modelElement);
		if (mappers.size() == 1) {
	    	result = mappers.iterator().next();
	    } else if (mappers.size() > 1) {
	    	throw new TargetException(mappers.size() + " writer mappers being responsible for the given source class and target class '" + sourceClass + "/" + targetClass + "' (mappers:" + mappers + ")");
	    }

	    return result;
	}

	/**
	 * @param element
	 * @param target
	 * @return
	 */
	@Override
	public Class<? extends WriterI> getWriterClass(Object element,
			TargetI<?> target) {

		Class<? extends WriterI> result = null;

		if (element instanceof ModelElementI) {
			ModelElementI modelElement = (ModelElementI) element;
			if (modelElement.isGenerated() == true) {
			    @SuppressWarnings("unchecked")
			    Set<WriterMapper> mappers = getWriterMappersForWriterDelegation(modelElement, (Class<? extends TargetI<?>>) target.getClass());
			    if (mappers.size() == 1) {
			    	result = mappers.iterator().next().getWriterClass();
			    } else if (mappers.size() > 1) {
			    	throw new TargetException("found more than one writer mapper being responsible for the given target (" + mappers + ")", target);
			    }
			} else {
				throw new RuntimeException("Detected attempt to obtain a writer instance for a model element where there is 'isGenerated()' returning false. element:'" + element + "', target instance:'" + target + "'");
			}
		}

		return result;
	}

	/**
	 * @return the writerMappersForGenerationDecision
	 */
	public final Set<WriterMapper> getWriterMappersForGenerationDecision() {
		return Collections.unmodifiableSet(writerMappersForGenerationDecision);
	}

	/**
	 * This method is to be used inside this class and its children. No other class
	 * is allowed to add WriterMapper instances. For this reason its visibility is 'protected'.
	 *
	 * @param writerMapper
	 * @return
	 */
	protected final boolean addWriterMapperForGenerationDecision(WriterMapper writerMapper) {
		boolean result = this.writerMappersForGenerationDecision.add(writerMapper);
		
		if (this.generationGroup != null) {
			// --- an easy way to make sure that all target classes are available in the generation group and the writer locator (mmt 05-May-2017) 
			this.generationGroup.addTargetClass(writerMapper.getTargetClass());
		}
		this.targetClasses.add(writerMapper.getTargetClass());
		
		Class<?> sourceClass = writerMapper.getSourceClass();
		Set<WriterMapper> groupedMappers = null;

		if (groupedWriterMappersForGenerationDecision.containsKey(sourceClass) == false) {
			groupedMappers = new LinkedHashSet<>();
			groupedWriterMappersForGenerationDecision.put(sourceClass, groupedMappers);
		} else {
			groupedMappers = groupedWriterMappersForGenerationDecision.get(sourceClass);
		}

		groupedMappers.add(writerMapper);

		// --- additionally, add the mapper to the writer delegation map
		addWriterMapperForWriterDelegation(writerMapper);

		return result;
	}

	/**
	 * @return the writerMappersForWriterDelegation
	 */
	public final Set<WriterMapper> getWriterMappersForWriterDelegation() {
		return Collections.unmodifiableSet(writerMappersForWriterDelegation);
	}

	/**
	 * This method is to be used inside this class and its children. No other class
	 * is allowed to add WriterMapper instances. For this reason its visibility is 'protected'.
	 *
	 * @param writerMapper
	 * @return
	 */
	protected final boolean addWriterMapperForWriterDelegation(WriterMapper writerMapper) {
		return this.writerMappersForWriterDelegation.add(writerMapper);
	}

	/**
	 * @param sourceClass
	 * @param targetClass
	 * @param element
	 * @return
	 */
	public final Set<WriterMapper> getWriterMappersForGenerationDecision(Class<?> sourceClass,
			Class<? extends TargetI<?>> targetClass, ModelElementI element) {
		return getWriterMappers(sourceClass, targetClass, groupedWriterMappersForGenerationDecision.get(sourceClass), element);
	}

	/**
	 * @param element
	 * @return
	 */
	public final Set<WriterMapper> getWriterMappersForWriterDelegation(ModelElementI element,
			Class<? extends TargetI<?>> targetClass) {
		return getWriterMappers(getEffectiveSourceClass(element), targetClass, writerMappersForWriterDelegation, element);
	}

	/**
	 * @param elementClass
	 * @param targetClass
	 * @return the one writer mapper that tells us, which writer class to be used, or null if the writer mapper is not responsible for the given combination of element class and target class
	 */
	public final Set<WriterMapper> getWriterMappers(Class<?> elementClass,
			Class<? extends TargetI<?>> targetClass, Set<WriterMapper> mappers, ModelElementI element) {
		Set<WriterMapper> foundMappers = new LinkedHashSet<>();

		if (mappers != null) {

			/* TODO use something like this if the reverse ordering of mappers would better serve the purpose
			WriterMapper<?,?,?>[] mappersArray = mappers.toArray(new WriterMapper<?,?,?>[0]);
			*/

			// --- 1. identify all writer mappers that potentially are responsible for handling the given element class and target class
			for (WriterMapper writerMapper : mappers) {
				if (writerMapper.isResponsible(elementClass, targetClass)) {
					if (element == null || writerMapper.isResponsibleForElement(element))
					foundMappers.add(writerMapper);
				}
			}

			// --- 2. if there is more than one writer mapper responsible, apply inheritance checks in order to find out, which one to choose for writing
			if (foundMappers.size() > 1) {
				Set<WriterMapper> mappersToBeRemoved = new LinkedHashSet<>();
				int firstCounter = 0;
				for (WriterMapper writerMapper : foundMappers) {
					int secondCounter = 0;
					for (WriterMapper otherWriterMapper : new LinkedHashSet<>(foundMappers)) {
					    if (writerMapper != otherWriterMapper && firstCounter < secondCounter) {
					    	if (writerMapper.overwrites(otherWriterMapper)) {
								mappersToBeRemoved.add(otherWriterMapper);
							} else if (otherWriterMapper.overwrites(writerMapper)) {
								mappersToBeRemoved.add(writerMapper);
							}
					    }
					    secondCounter++;
					}
					firstCounter++;
				}
				foundMappers.removeAll(mappersToBeRemoved);
			}
		}

		return foundMappers;
	}

	/**
	 * @return the targetClasses
	 */
	protected Set<Class<? extends TargetI<?>>> getTargetClasses() {
		return targetClasses;
	}

	public AbstractGenerationGroup getGenerationGroup() {
		return generationGroup;
	}
}
