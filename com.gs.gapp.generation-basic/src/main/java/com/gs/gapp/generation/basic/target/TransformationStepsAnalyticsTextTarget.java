package com.gs.gapp.generation.basic.target;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.DoNotWriteException;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetLifecycleListenerI;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.basic.TargetModelizer;
import com.gs.gapp.generation.basic.writer.ModelElementWriter;
import com.gs.gapp.metamodel.analytics.GenerationGroupTargetConfiguration;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;

public class TransformationStepsAnalyticsTextTarget extends BasicTextTarget<TextTargetDocument> {

	private static final TargetLifecycleListenerI TARGET_MODELIZER = new TargetModelizer();
	
	@ModelElement
	private TransformationStepConfiguration converterConfiguration;

	
	public TransformationStepsAnalyticsTextTarget() {
		super();
		if (com.gs.gapp.metamodel.basic.ModelElement.isAnalyticsMode()) {
            addTargetLifecycleListener(TARGET_MODELIZER);
		}
	}


	@Override
	public URI getTargetURI() {
		StringBuilder sb = new StringBuilder(getTargetRoot()).append("/").append("vd-reports/steps-").
				append(getGenerationGroup().getName()).append(getGenerationGroup().getTargetProject().getName()).append(".txt");
		try {
		    URI result = new URI(sb.toString().replace(" ", "-"));
		    
		    return result;
		} catch (URISyntaxException e) {
			throw createTargetException(e, this, converterConfiguration);
		}
	}

	public static class TransformationStepsAnalyticsTextWriter extends ModelElementWriter {

		@ModelElement
		private TransformationStepConfiguration converterConfiguration;


		/* (non-Javadoc)
		 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
		 */
		@Override
		public void transform(TargetSection ts) {
			// first we add one more ModelConverterConfiguration element to complete the tree of transformation steps
//			TransformationStepConfiguration modelConverterConfiguration =
//					new TransformationStepConfiguration(getTransformationTarget().getGenerationGroup().getName(), getGenerationGroupOptions());
//			this.converterConfiguration.setParentConfiguration(modelConverterConfiguration);
//			this.converterConfiguration.addOptionDefinitions(this.getGenerationGroupOptions().getOptionDefinitions());
			
			// there is only one target section, thus we do not need the if-else construct to differentiate the sections
			List<TransformationStepConfiguration> configurationsTopDown = new ArrayList<>();
			TransformationStepConfiguration currentConverterConfiguration = converterConfiguration;
			do {
				configurationsTopDown.add(currentConverterConfiguration);
				currentConverterConfiguration = currentConverterConfiguration.getParentConfiguration();
			} while (currentConverterConfiguration != null);

			Collections.reverse(configurationsTopDown);
			for (TransformationStepConfiguration aConverterConfiguration : configurationsTopDown) {
				wNL("converter config:" + aConverterConfiguration.toString());
	            // TODO continue here
			}
			
			for (TransformationStepConfiguration generationGroupStep : converterConfiguration.getChildConfigurations()) {
				wNL("converter config for generation group:" + generationGroupStep.toString());
				for (GenerationGroupTargetConfiguration targetConfiguration : generationGroupStep.getTargetConfigurations()) {
				    wNL(targetConfiguration.toString());
				}
			}
			
			if (!isDevelopmentMode()) {
				// --- this analytics file must not be generated when a cloud-connector has triggered the generation process - it includes intellectual property of a generator creator
	    		throw new DoNotWriteException("we intentionally never write a " + getTransformationTarget().getClass() + " target");
			}
		}
	}
}
