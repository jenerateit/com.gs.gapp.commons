/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.generation.basic.writer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jenerateit.JenerateITI;
import org.jenerateit.target.DeveloperAreaInfoI;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.target.TextTargetDocumentI;
import org.jenerateit.target.TextTargetI;
import org.jenerateit.util.StringTools;
import org.jenerateit.writer.AbstractTextWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.basic.AbstractGenerationGroup;
import com.gs.gapp.metamodel.basic.BasicMessage;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.options.GenerationGroupOptions;

/**
 * @author hrr
 *
 */
public abstract class ModelElementWriter extends AbstractTextWriter {
	
	private static final String pattern = ".{1,80}(?!\\S)";
	private static final Pattern r = Pattern.compile(pattern);
	
	public static Matcher getMatcherToSplitLines(String text) {
		return r.matcher(text);
	}

	@org.jenerateit.annotation.ModelElement
	private ModelElement modelElement;

	private GenerationGroupOptions generationGroupOptions;
	
	public ModelElementWriter() {
		super();
	}

	/**
	 *
	 */
	protected void wName() {
		w(modelElement.getName());
	}
	
	public String getName() {
		return modelElement.getName();
	}
	
	/**
	 * @return
	 */
	protected String getNameOfGeneratedBaseElement() {
		ModelElement modelElement = (ModelElement) getTransformationTarget().getBaseElement();
		return modelElement.getName();
	}
	
	/**
	 * Creates a quoted String.
	 *
	 * @param value the string to quote
	 */
	protected String getQuotedString(String value) {
		return new StringBuilder("\"").append(value).append("\"").toString();
	}
	/**
	 * Helper method to parse text segments for line breaks.
	 * The text segment will be split into separate lines.
	 * Leading and trailing empty lines will be discarded.
	 *
	 * @see BufferedReader#readLine()
	 * @param text the text segment to parse
	 * @return an array of lines
	 */
	protected CharSequence[] getLines(CharSequence text) {
		// TODO this is content that could be moved into the StringTools
		if (StringTools.isNotEmpty(text.toString())) {
			List<String> lines = new ArrayList<>();
			BufferedReader br = new BufferedReader(new StringReader(text.toString()));
			String s = null;
			try {
				while ((s = br.readLine()) != null) {
					lines.add(s);
				}

			} catch (IOException e) {
				throw new WriterException("Error while parse a text segment for line breaks",
						e, getTransformationTarget(), this);
			}
			
			if (lines.size() > 1) {
				String firstLine = lines.get(0);
				String lastLine = lines.get(lines.size()-1);
				if (lastLine == null || lastLine.trim().length() == 0) {
				    lines.remove(lines.size()-1);
				}
				if (firstLine == null || firstLine.trim().length() == 0) {
				    lines.remove(0);
				}
			}

			return lines.toArray(new String[lines.size()]);
		}
		return new String[] {};
	}
	
	/**
	 * Prefix every line in the given text block with a number of blanks.
	 * This method is useful when you define multi-line strings with Groovy
	 * or even more so when you combine multiple multi-line strings in
	 * a nested way.
	 * 
	 * @param textblock
	 * @param numberOfBlanks 0 or a positive number of blanks to be added to every line
	 * @return
	 */
	protected String indentWithBlanks(String textblock, int numberOfBlanks) {
		return indentWithBlanks(textblock, numberOfBlanks, false);
	}
	
	/**
	 * Prefix every line in the given text block with a number of blanks.
	 * This method is useful when you define multi-line strings with Groovy
	 * or even more so when you combine multiple multi-line strings in
	 * a nested way.
	 * 
	 * @param textblock
	 * @param numberOfBlanks 0 or a positive number of blanks to be added to every line
	 * @param removeEmptyLines if this is true, the algorithm additionally removes empty lines
	 * @return
	 */
	protected String indentWithBlanks(String textblock, int numberOfBlanks, boolean removeEmptyLines) {
		if (textblock == null) return null;
		if (numberOfBlanks < 0) throw new IllegalArgumentException("parameter 'numberOfBlanks must not have a negative value, it has " + numberOfBlanks);
		
		StringBuilder additionalIndentBuilder = new StringBuilder();
		for (int ii=0;ii<numberOfBlanks;ii++) { additionalIndentBuilder.append(" "); }
		String newline = "";
		StringBuilder result = new StringBuilder();
		for (CharSequence line : getLines(textblock)) {
			if (removeEmptyLines && line.toString().trim().isEmpty()) continue;
			
			result.append(newline);
			result.append(additionalIndentBuilder);result.append(line);
			newline = System.lineSeparator();
		}
		
		return result.toString();
	}

	/**
	 * Write comment(s) to the current working target section
	 * <b>Note:</b> The comment character will be added in front and the end of a line.
	 * If the line has line separators each line will be added separately with comment signs.
	 *
	 * @param comments the comment(s) to write
	 * @see #wComment(TargetSection, CharSequence...)
	 * @return an instance of this writer object
	 */
	public ModelElementWriter wComment(String... comments) {
		return wComment(null, comments);
	}

	/**
	 * Write comment(s) to the given target section ts.
	 * If ts is null the current working target section is used to write to.
	 * <b>Note:</b> The comment character will be added in front and the end of a line.
	 * If the line has line separators each line will be added separately with comment signs.
	 *
	 * @param comments the comment(s) to write
	 * @param ts the target section to write to
	 * @return an instance of this writer object
	 */
	public ModelElementWriter wComment(TargetSection ts, CharSequence... comments) {
		if (comments == null || comments.length == 0) {
			// nothing to be done here
		} else {
			TargetI<?> t = getTransformationTarget();
			if (TextTargetI.class.isInstance(t)) {
				TextTargetI<?> tt = (TextTargetI<?>) t;
				for (CharSequence c : comments) {
					for (CharSequence s : getLines(c)) {
						if (ts != null) {
							wNL(ts, tt.getCommentStart(), " ", s, " ", tt.getCommentEnd());
						} else {
							wNL(tt.getCommentStart(), " ", s, " ", tt.getCommentEnd());
						}
					}
				}
			} else {
				throw new WriterException("Can not write a comment in a Target of type '" + t.getClass().getName() + "'", t, this);
			}
		}
		return this;
	}

	/**
	 * Writes the {@link Object#toString() objects} in the output file as a comment
	 * if the {@link JenerateITI#isDevelopmentMode() development mode} of the corresponding
	 * {@link JenerateITI jenerateit} is switched on.
	 *
	 * @param objs the objects to write
	 * @see Object#toString()
	 * @see JenerateITI#isDevelopmentMode()
	 * @return an instance of this writer object
	 */
	public ModelElementWriter wDebug(Object... objs) {
		final JenerateITI jenerateIT = getTransformationTarget().getGenerationGroup().getTargetProject().getJenerateIT();
		if (jenerateIT != null && jenerateIT.isDevelopmentMode()) {
			for (Object o : objs) {
				wComment(o != null ? o.toString() : "null");
			}
		}
		return this;
	}

	/**
	 * @return
	 */
	public ModelElementCache getModelElementCache() {
		if (getTransformationTarget() != null) {
			if (getTransformationTarget().getGenerationGroup().getConfig() != null) {
				if (getTransformationTarget().getGenerationGroup().getConfig() instanceof AbstractGenerationGroup) {
					return ((AbstractGenerationGroup)getTransformationTarget().getGenerationGroup().getConfig()).getModelElementCache();
				}
			}
		}
		return null;
	}

	/**
	 * Gets an object that provides access to all options for the generation group this writer is being used within.
	 * 
	 * @return
	 */
	public GenerationGroupOptions getGenerationGroupOptions() {
		if (this.generationGroupOptions == null) {
			this.generationGroupOptions = new GenerationGroupOptions(this);
		}
		return generationGroupOptions;
	}
	
	/**
	 * @return
	 */
	public static boolean isDevelopmentMode() {
		String developmentMode = System.getProperty("org.jenerateit.server.developmentMode");
		return (developmentMode != null && developmentMode.length() > 0 && developmentMode.equalsIgnoreCase("true"));
	}
	
	/**
	 * @param developerAreaId
	 * @param section
	 * @param text
	 */
	public void writeIfDeveloperAreaIsEmpty(String developerAreaId, TargetSection section, CharSequence... text) {
		
		TextTargetDocumentI previousTargetDocument = getTextTransformationTarget().getPreviousTargetDocument();
		if (previousTargetDocument != null) {
			DeveloperAreaInfoI developerAreaInfo = previousTargetDocument.getDeveloperArea(developerAreaId);
			if (developerAreaInfo == null || developerAreaInfo.getUserContent() == null || developerAreaInfo.getUserContent().length() == 0) {
				getTextTransformationTarget().write(section, text);
			}
		} else {
			getTextTransformationTarget().write(section, text);
		}
	}
	
    /**
     * @param text
     */
    public void writeIfDeveloperAreaIsEmpty(CharSequence... text) {
    	if (getTextTransformationTarget() != null) {
    		TextTargetI<?> transformationTarget = getTextTransformationTarget();
    		String currentDeveloperAreaId = transformationTarget.getCurrentDeveloperAreaId();
    		if (currentDeveloperAreaId != null && !currentDeveloperAreaId.isEmpty() ) {
    		    writeIfDeveloperAreaIsEmpty(currentDeveloperAreaId, transformationTarget.getCurrentTransformationSection(), text);   			
    		}
    	}
	}
	
    /**
     * This method checks whether there is a previous target document available and if so, whether
     * it has some developer area content for the given developer area id. In case there is
     * a non-empty developer area, this method does not write anything.
     * 
     * @param developerAreaId
     * @param text
     */
    public void wForEmptyDa(String developerAreaId, CharSequence... text) {
    	if (isDeveloperAreaEmpty(developerAreaId)) {
    		w(text);
    	}
	}
    
    /**
     * This method checks whether there is a previous target document available and if so, whether
     * it has some developer area content for the given developer area id. In case there is
     * a non-empty developer area, this method does not write anything.
     * 
     * @param developerAreaId
     * @param text
     */
    public void wNlForEmptyDa(String developerAreaId, CharSequence... text) {
    	if (isDeveloperAreaEmpty(developerAreaId)) {
    		wNL(text);
    	}
	}
    
    /**
     * @param developerAreaId
     * @return
     */
    public boolean isDeveloperAreaEmpty(String developerAreaId) {
    	boolean developerAreaIsEmpty = true;
    	
    	TextTargetDocumentI previousTargetDocument = getTextTransformationTarget().getPreviousTargetDocument();
		if (previousTargetDocument != null) {
			DeveloperAreaInfoI developerAreaInfo = previousTargetDocument.getDeveloperArea(developerAreaId);
			if (developerAreaInfo != null && developerAreaInfo.getUserContent() != null && developerAreaInfo.getUserContent().length() > 0) {
				developerAreaIsEmpty = false;
			}
		}
			
		return developerAreaIsEmpty;
    }
    
    /**
	 * Convenience method to throw a {@link WriterException} in case an enum entry
	 * should have been handled in a switch but in fact wasn't. Such a situation indicates
	 * a bug, e.g. generator components with semantically incompatible versions.
	 * 
	 * @param enumEntry
	 */
	protected void throwUnhandledEnumEntryException(Enum<?> enumEntry) {
	    Message errorMessage = BasicMessage.UNHANDLED_ENUM_ENTRY
			.getMessageBuilder()
			.parameters(enumEntry.name(), enumEntry.getClass().getSimpleName(), this.getClass().getSimpleName())
			.build();
        throw new WriterException(errorMessage.getMessage());
	}
}