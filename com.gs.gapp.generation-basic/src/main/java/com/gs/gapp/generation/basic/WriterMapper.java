/**
 *
 */
package com.gs.gapp.generation.basic;

import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * The purpose of this class is to be able to declaratively define rules for mapping combinations of model element types
 * and target types to writer types. This way, those mapping rules can be automatically extracted and visualized.
 * Additionally, the code inside writer locators gets easier to read.
 *
 * @author mmt
 *
 */
public class WriterMapper {

	private Class<?> sourceClass;
	private Class<? extends TargetI<? extends TargetDocumentI>> targetClass;
	private Class<? extends WriterI> writerClass;



	/**
	 * @param sourceClass
	 * @param targetClass
	 * @param writerClass
	 */
	public WriterMapper(Class<?> sourceClass,
			Class<? extends TargetI<? extends TargetDocumentI>> targetClass,
			Class<? extends WriterI> writerClass) {
		super();
		this.sourceClass = sourceClass;
		this.targetClass = targetClass;
		this.writerClass = writerClass;
	}

	/**
	 * @param elementClass
	 * @param targetClass
	 * @return
	 */
	public final boolean isResponsible(Class<?> elementClass,
			Class<? extends TargetI<?>> targetClass) {
		boolean result = false;

		if (getSourceClass().isAssignableFrom(elementClass)) {
		//if (getSourceClass() == elementClass) {
			//if (getTargetClass() == targetClass) {
		    if (getTargetClass().isAssignableFrom(targetClass)) {
			//if (targetClass.isAssignableFrom(getTargetClass())) {
		    	result = true;
		    }
		}

		return result;
	}

	/**
	 * Overwrite this method if the decision to use this mapper should be depending
	 * on the model element's properties. When this method returns false for a given
	 * model element, the writer mapper will _not_ be taken into account, even though
	 * its sourceClass and targetClass settings might be saying something else.
	 * Note that this method returning true does not have any effect if the mapper's sourceClass and
	 * targetClass settings are not matching a given source class and target class.
	 *
	 * @param element
	 * @return
	 */
	public boolean isResponsibleForElement(ModelElementI element) {
        return getSourceClass().isAssignableFrom(element.getClass());
	}

	/**
	 * @param otherWriterMapper
	 * @return
	 */
	final boolean overwrites(WriterMapper otherWriterMapper) {
		if (otherWriterMapper == null) throw new NullPointerException("parameter 'otherWriterMapper' must not be null");
		boolean result = false;

		if (this != otherWriterMapper) {
			if (getClass() != otherWriterMapper.getClass()) {
				// first check, whether the mappers inherit from each other
				if (otherWriterMapper.getClass().isAssignableFrom(getClass())) {
                    result = true;
				}
			} else {
				// identical writer mapper classes - if the target classes are the same but the writer classes inherit from each other, we say one mapper overwrites the other
				if (otherWriterMapper.getTargetClass().isAssignableFrom(getTargetClass()) && otherWriterMapper.getWriterClass().isAssignableFrom(getWriterClass())) {
					result = true;
				}
			}

			if (result == false) {
				// the two mappers either have the same runtime class or
				// their classes do not have an inheritance relationship
				// => check for inheritance between their used target classes
				
				Boolean thisTargetClassExtendsOtherTargetClass = null;
				if (otherWriterMapper.getTargetClass() != getTargetClass()) {
					if (otherWriterMapper.getTargetClass().isAssignableFrom(getTargetClass())) {
						thisTargetClassExtendsOtherTargetClass = true;
					} else if (getTargetClass().isAssignableFrom(otherWriterMapper.getTargetClass())) {
						thisTargetClassExtendsOtherTargetClass = false;
					}
				}
				Boolean thisSourceClassExtendsOtherSourceClass = null;
				if (otherWriterMapper.getSourceClass() != getSourceClass()) {
					if (otherWriterMapper.getSourceClass().isAssignableFrom(getSourceClass())) {
						thisSourceClassExtendsOtherSourceClass = true;
					} else if (getSourceClass().isAssignableFrom(otherWriterMapper.getSourceClass())) {
						thisSourceClassExtendsOtherSourceClass = false;
					}
				}
				Boolean thisWriterClassExtendsOtherWriterClass = null;
				if (otherWriterMapper.getWriterClass() != getWriterClass()) {
					if (otherWriterMapper.getWriterClass().isAssignableFrom(getWriterClass())) {
						thisWriterClassExtendsOtherWriterClass = true;
					} else if (getWriterClass().isAssignableFrom(otherWriterMapper.getWriterClass())) {
						thisWriterClassExtendsOtherWriterClass = false;
					}
				}
				
				
				if (thisTargetClassExtendsOtherTargetClass != null && thisTargetClassExtendsOtherTargetClass ||
					thisSourceClassExtendsOtherSourceClass != null && thisSourceClassExtendsOtherSourceClass ||
				    thisWriterClassExtendsOtherWriterClass != null && thisWriterClassExtendsOtherWriterClass) {
					// at least one of the to-be-investigated classes extends the related class of the other writer mapper => it is worth to further investigate
					
					if (thisSourceClassExtendsOtherSourceClass != null && thisSourceClassExtendsOtherSourceClass) {
						result = true;
					} else if (thisTargetClassExtendsOtherTargetClass != null && thisTargetClassExtendsOtherTargetClass) {
						if (thisSourceClassExtendsOtherSourceClass == null || thisSourceClassExtendsOtherSourceClass) {
							result = true;
						} else {
							int a = 1;
						}
					} else if (thisWriterClassExtendsOtherWriterClass != null && thisWriterClassExtendsOtherWriterClass) {
						result = true;
					}
				}
				
				if (result == false) {
					// if we are here, the only left possiblity to check is whether the two used writer classes do inherit from each other
				}
			}
		}

		return result;
	}

	/**
	 * @return
	 */
	public final Class<?> getSourceClass() {
		return sourceClass;
	}

	/**
	 * @return
	 */
	public final Class<? extends TargetI<? extends TargetDocumentI>> getTargetClass() {
        return targetClass;
	}

	/**
	 * @return
	 */
	public final Class<? extends WriterI> getWriterClass() {
        return writerClass;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return new StringBuilder().append(getSourceClass() == null ? "null-sourceClass" : getSourceClass().getSimpleName()).append("/")
				.append(getTargetClass() == null ? "null-targetClass" : getTargetClass().getSimpleName()).append("/")
				.append(getWriterClass() == null ? "null-writerClass" : getWriterClass().getSimpleName()).toString();
	}
}
