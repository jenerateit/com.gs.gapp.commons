/**
 * 
 */
package com.gs.gapp.generation.basic.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.basic.snippets.DevDocTargetSnippets;
import com.gs.gapp.generation.basic.writer.ModelElementWriter;
import com.gs.gapp.metamodel.basic.DevDoc;

/**
 * @author mmt
 *
 */
public class DevDocTarget extends BasicTextTarget<TextTargetDocument> {
	
	@ModelElement
	private DevDoc devDoc;

	@Override
	protected URI getTargetURI() {
		StringBuilder sb = new StringBuilder(devDoc.getName()).append(".html");
		try {
			return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, (TargetI<? extends TargetDocumentI>) this);
		}
	}
	
	/**
	 * @author mmt
	 *
	 */
	public static class DevDocWriter extends ModelElementWriter {
	
		@ModelElement
		private DevDoc devDoc;

		@Override
		public void transform(TargetSection ts) throws WriterException {
			String htmlContent = DevDocTargetSnippets.getHtmlContent(devDoc);
			wNL(htmlContent);
		}
	}
}
