/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.generation.basic;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupProviderI;
import org.osgi.service.component.ComponentContext;


/**
 * @author hrr
 *
 */
public class GenerationGroupDevDocProvider implements GenerationGroupProviderI {

	/**
	 *
	 */
	public GenerationGroupDevDocProvider() {}
	
    public void startup(ComponentContext context) {}
    
    public void shutdown(ComponentContext context) {}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.GenerationGroupProviderI#getGenerationGroupClass()
	 */
	@Override
	public GenerationGroupConfigI getGenerationGroupConfig() {
		return new GenerationGroupDevDoc();
	}
}
