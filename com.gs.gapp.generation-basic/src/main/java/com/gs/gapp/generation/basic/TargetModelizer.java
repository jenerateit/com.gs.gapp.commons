/*
 *
 *  JenerateIT - Lightweight Generation
 *
 *  http://jenerateit.org
 *
 *  Copyright:
 *    2007-2010 Generative Software GmbH, Germany, http://www.generative-software.de
 *
 *  License:
 *    LGPL: http://www.gnu.org/licenses/lgpl.html
 *    EPL: http://www.eclipse.org/org/documents/epl-v10.php
 *    See the LICENSE file in the project's top-level directory for details.
 *
 *  Authors:
 *    Heinz Rohmer (hrr)
 *
 */

package com.gs.gapp.generation.basic;

import org.jenerateit.target.AbstractTarget;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetLifecycleListenerI;

import com.gs.gapp.generation.basic.target.TransformationStepAnalysisTarget;
import com.gs.gapp.generation.basic.target.TransformationStepsAnalyticsTextTarget;
import com.gs.gapp.metamodel.analytics.GenerationGroupTargetConfiguration;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;
import com.gs.gapp.metamodel.basic.ModelElementTraceabilityI;
import com.gs.gapp.metamodel.basic.TargetElement;
import com.gs.gapp.metamodel.basic.TargetElement.TargetStatus;
import com.gs.gapp.metamodel.basic.options.GenerationGroupOptions;

/**
 * This class adds model elements to the leaves of the conversion tree. Those new model elements represent
 * target files to be generated. This information is going to be used by the analytics components
 * to be able to generate a full-blown transformation tree report.
 *
 * @see AbstractTarget#addTargetLifecycleListener(TargetLifecycleListenerI)
 * @author mmt
 */
public final class TargetModelizer implements TargetLifecycleListenerI {

	@Override
	public void preTransform(TargetI<?> target) {
        createTargetElement(target, TargetStatus.TRANSFORMED);
	}

	private void createTargetElement(TargetI<?> target, TargetStatus targetStatus) {
		if (target != null) {
			if (target.getBaseElement() instanceof ModelElementTraceabilityI) {
				ModelElementTraceabilityI traceableModelElement = (ModelElementTraceabilityI) target.getBaseElement();

				TargetElement targetElement = new TargetElement(/*modelElement.getName()*/target.getTargetPath().toString(), target.getTargetPath(), targetStatus);
				boolean targetElementAlreadyThere = false;
				for (TargetElement existingTargetElement : traceableModelElement.getAllExtensionElementDeeply(TargetElement.class, true)) {
                    if (existingTargetElement.equals(targetElement)) {
                    	// target element is already present, do not add it
                    	if (existingTargetElement.getTargetStatus() != TargetStatus.TRANSFORMED) {
                    	    existingTargetElement.setTargetStatus(targetStatus);  // We need to override the status here since there is more than one lifecycle method called for a target. Only TRANSFORMED is a final status
                    	}
                    	targetElementAlreadyThere = true;
                    	break;
                    }
				}

				if (!targetElementAlreadyThere) {
				    targetElement.setOriginatingElement(traceableModelElement);
//				    System.out.println("adding originating element '" + traceableModelElement + "' for target element '" + targetElement.getUri() + "'");
				}
			} else {
                // TODO what to do in this case?
			}
		}
	}
	
	@Override
	public void postTransform(TargetI<?> target) {
		// we do not need to do anything here
	}

	@Override
	public void onFound(TargetI<?> target) {
		createTargetElement(target, TargetStatus.FOUND);
	}

	@Override
	public void onLoaded(TargetI<?> target) {
		createTargetElement(target, TargetStatus.LOADED);
//		System.out.println("loaded target '" + target.getTargetPath() + "' in generation group '" + target.getGenerationGroup().getConfig() + "'");
		
		if (target.getBaseElement() instanceof TransformationStepConfiguration) {
//			System.out.println("found target that's based on an instance of TransformationStepConfiguration (generation group '" + target.getGenerationGroup().getConfig() + "')");
			// --- provide a final transformation step configuration instance for the transformation step leaves (= generation groups)
			TransformationStepConfiguration transformationStepConfiguration = (TransformationStepConfiguration) target.getBaseElement();
			TransformationStepConfiguration generationGroupConfiguration =
					new TransformationStepConfiguration(target.getGenerationGroup().getName(), new GenerationGroupOptions(target));
			generationGroupConfiguration.setParentConfiguration(transformationStepConfiguration);
			
			if (target.getGenerationGroup().getConfig() instanceof AbstractGenerationGroup) {
				AbstractGenerationGroup abstractGenerationGroup = (AbstractGenerationGroup) target.getGenerationGroup().getConfig();
				if (abstractGenerationGroup.getWriterLocator() instanceof AbstractWriterLocator) {
					AbstractWriterLocator writerLocator = (AbstractWriterLocator) abstractGenerationGroup.getWriterLocator();
					
					for (WriterMapper writerMapper : writerLocator.getWriterMappersForGenerationDecision()) {
						if (TransformationStepsAnalyticsTextTarget.class == writerMapper.getTargetClass() ||
							TransformationStepAnalysisTarget.class == writerMapper.getTargetClass()) {
							continue;
						}
//						System.out.println("adding target configuration to generation group configuration '" + generationGroupConfiguration + "'");
				    	generationGroupConfiguration
				    	    .addTargetConfiguration(new GenerationGroupTargetConfiguration(writerMapper.getSourceClass(),
					    			                                                           writerMapper.getTargetClass(),
					    			                                                           writerMapper.getWriterClass(),
					    			                                                           abstractGenerationGroup.getClass().getName()));          
					}
				}
			}
		}
	}

}
