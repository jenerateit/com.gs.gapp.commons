package com.gs.gapp.generation.basic.target;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

import org.jenerateit.target.AbstractTextTarget;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TextTargetDocumentI;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.basic.BasicMessage;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.options.GenerationGroupOptions;

public abstract class BasicTextTarget<V extends TextTargetDocumentI> extends AbstractTextTarget<V> {

	/**
     * @param inputStream
     * @return
     */
    public static String readResource(InputStream inputStream) {
    	if (inputStream == null) throw new NullPointerException("parameter 'inputStream' must not be null");
    	
    	try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {     
		    String str = null;
		    StringBuilder sb = new StringBuilder(8192);
		    while ((str = reader.readLine()) != null) {
		        sb.append(str).append(System.getProperty("line.separator"));
		    }
		    return sb.toString();
		} catch (IOException ex) {
		    throw new WriterException("problem reading from the input stream '" + inputStream + "'", ex);
		}
    }
    
    /**
     * @param resource
     * @param clazz
     * @return
     */
    @SuppressWarnings("resource")
	public static String readResourceAsStream(String resource, Class<?> clazz) {
    	InputStream inputStream = clazz.getResourceAsStream(resource);
    	return readResource(inputStream);
    }

	private GenerationGroupOptions generationGroupOptions;

	public BasicTextTarget() {
		super();
	}

	/**
	 * @return
	 */
	public GenerationGroupOptions getGenerationGroupOptions() {
		if (this.generationGroupOptions == null) {
			this.generationGroupOptions = new GenerationGroupOptions(this);
		}
		return generationGroupOptions;
	}
	
	/**
	 * Convenience method, delegates the call to the respective generation group options method.
	 * @return this target's root for the file path - this is required when the generated file is not located in the root path of the cloud-connector
	 */
	public String getTargetRoot() {
		return this.getGenerationGroupOptions().getTargetRoot();
	}

	/**
	 * Convenience method, delegates the call to the respective generation group options method.
	 *
	 * @return this target's prefix for the file path
	 */
	public String getTargetPrefix() {
		return this.getGenerationGroupOptions().getTargetPrefix();
	}
	
	/**
	 * Convenience method, delegates the call to the respective generation group options method.
	 * 
	 * @return this target's prefix for the file path if the file is needed for testing - if that option is not set on the generation group, the target prefix is used instead
	 */
	public String getTargetPrefixForTests() {
		return this.getGenerationGroupOptions().getTargetPrefixForTests();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.AbstractTextTarget#getLineBreakChars()
	 */
	@Override
	public CharSequence getLineBreakChars() {
		CharSequence lineBreakChars = getGenerationGroupOptions().getLineBreakChars();
		if (lineBreakChars != null && lineBreakChars.length() > 0) {
			return lineBreakChars;
		}
		
		return super.getLineBreakChars();
	}
	
	protected TargetException createTargetException(URISyntaxException exc, TargetI<? extends TargetDocumentI> target, ModelElementI modelElement) {
		 Message errorMessage = BasicMessage.TARGET_FILE_PATH_CONTAINS_INVALID_CHARACTERS
			.getMessageBuilder()
			.modelElement(modelElement)
			.parameters(exc.getInput(), exc.getIndex())
			.build();
		addError(errorMessage.getMessage());  // This is a workaround since at the time being the message from the following target exception is not passed to the cloud connector. (mmt 05-May-2022)
        return new TargetException(errorMessage.getMessage(), exc, target);
	}
}
