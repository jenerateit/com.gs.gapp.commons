package com.gs.gapp.generation.basic;

/**
 * Objects that implement this interface allow to tell the caller whether the current generation of
 * a target should be prevented.
 * 
 * @author mmt
 *
 */
public interface GenerationPreventableByUserI {

	boolean isGenerationPreventedByUser();
}
