/**
 *
 */
package com.gs.gapp.generation.basic;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.WriterLocatorI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.basic.target.TransformationStepAnalysisTarget;
import com.gs.gapp.generation.basic.target.TransformationStepsAnalyticsTextTarget;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementCache;


/**
 * In here you find all functionality that all generation groups have in common.
 *
 * @author mmt
 *
 */
public abstract class AbstractGenerationGroup implements GenerationGroupConfigI {

	private final ModelElementCache modelElementCache;

	private final Set<Class<? extends TargetI<?>>> targetClasses = new LinkedHashSet<>();

	/**
	 *
	 */
	public AbstractGenerationGroup(ModelElementCache modelElementCache) {
		super();
		if (modelElementCache == null) throw new NullPointerException("parameter modelElementCache must not be null");
		this.modelElementCache = modelElementCache;
		
		if (ModelElement.isAnalyticsMode()) {
			addTargetClass(TransformationStepsAnalyticsTextTarget.class);
			addTargetClass(TransformationStepAnalysisTarget.class);
		}
	}
	
	/**
	 * @return
	 */
	public static boolean isDevelopmentMode() {
		String developmentMode = System.getProperty("org.jenerateit.server.developmentMode");
		return (developmentMode != null && developmentMode.length() > 0 && developmentMode.equalsIgnoreCase("true"));
	}

	/**
	 * The model element cache helps you when you are inside a model element converter and
	 * you need a certain model element where it is difficult to get it by traversing the
	 * object graph of model elements.
	 * Note that a model element will only be found in the cache if it got added there
	 * before. It is the model element converter's task to ensure that a model element
	 * gets added to the model element cache. Also, you might try to find a model element that
	 * did not get converted so far. In this case it would make more sense to use
	 * AbstractModelElementConverter.convertWithOtherConverter(), which triggers the
	 * conversion in case it has not beed done before.
	 *
	 * @return
	 */
	public ModelElementCache getModelElementCache() {
        return modelElementCache;
	}
	
	/* (non-Javadoc)
	 * @see org.jenerateit.util.DetailsI#getDescription()
	 */
	@Override
	public String getDescription() {
        return getName();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.util.DetailsI#getName()
	 */
	@Override
	public String getName() {
        return getClass().getSimpleName();
	}


	/**
	 * @return the targetClasses
	 */
	private Set<Class<? extends TargetI<?>>> getTargetClasses() {
		return Collections.unmodifiableSet(targetClasses);
	}


	protected boolean addTargetClass(Class<? extends TargetI<?>> targetClass) {
		return targetClasses.add(targetClass);
	}

	protected void addTargetClasses(Collection<Class<? extends TargetI<?>>> targetClasses) {
		this.targetClasses.addAll(targetClasses);
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.GenerationGroupConfigI#getAllTargets()
	 */
	@Override
	public Set<Class<? extends TargetI<?>>> getAllTargets() {
		return getTargetClasses();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.WriterLocatorI#getWriterClass(java.io.Serializable, java.lang.Class)
	 */
	@Override
	public Class<? extends WriterI> getWriterClass(Object element,
			Class<? extends TargetI<?>> targetClass) {
		return getWriterLocator().getWriterClass(element, targetClass);
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.WriterLocatorI#getWriterClass(java.io.Serializable, org.jenerateit.target.TargetI)
	 */
	@Override
	public Class<? extends WriterI> getWriterClass(Object element,
			TargetI<?> target) {
		return getWriterLocator().getWriterClass(element, target);
	}

	/**
	 * @return
	 */
	public abstract WriterLocatorI getWriterLocator();

	/**
	 * Get an instance of an object that has the purpose of creating model elements for a specific target technology.
	 *
	 * @return might return null, if a generation group does not provide a technology-specific model element creator
	 */
	public Object getModelElementCreator() {
		return null;
	}
}
