package com.gs.gapp.generation.basic.target;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.generation.basic.writer.ModelElementWriter;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionWithValue;

public class TransformationStepAnalysisTarget extends BasicTextTarget<TextTargetDocument> {
	
	@ModelElement
	private Model model;

	@Override
	protected URI getTargetURI() {
		StringBuilder sb = new StringBuilder(getTargetRoot()).append("/").append("vd-reports/options-").
				append(getGenerationGroup().getName()).append(getGenerationGroup().getTargetProject().getName()).append(".txt");
		String file = sb.toString().replaceAll(" ",  "_");
		try {
			
			return new URI(file);
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + file, e, this);
		}
	}
	
	
	public static class TransformationStepAnalysisWriter extends ModelElementWriter {
		
		@ModelElement
		private Model model;

		@Override
		public void transform(TargetSection ts) throws WriterException {
			wNL("*** Analysis of transformation step options ***");
			
			
//			for (Object propertyKey : System.getProperties().keySet()) {
//			    wNL(propertyKey.toString());
//			}
			
			Model currentModel = model;
			
			while (currentModel != null) {
				wNL("*** ", currentModel.getName());
				for (OptionDefinitionWithValue optionDefinitionWithValue : currentModel.getElements(OptionDefinitionWithValue.class)) {
					OptionDefinition<?> optionDefinition = optionDefinitionWithValue.getOptionDefinition();
					Serializable optionValue = optionDefinitionWithValue.getOptionValue();
					wNL(optionDefinition.getKey(), ":", optionValue != null ? optionValue.toString() : "!! MISSING !!", ")");
				}
				
				currentModel = currentModel.getOriginatingElement(Model.class);
			}
		}
		
	}

}
