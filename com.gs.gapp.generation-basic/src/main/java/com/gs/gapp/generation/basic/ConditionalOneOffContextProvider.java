package com.gs.gapp.generation.basic;

import java.io.Serializable;

import org.jenerateit.annotation.ContextProviderI;
import org.jenerateit.exception.JenerateITException;
import org.jenerateit.target.ElementNotSupportedException;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetI;

/**
 * You can use this provider in order to achieve the following:
 * If the target document implements the interface GenerationPreventableByUserI _and_ the user
 * has added the string "DO_NOT_GENERATE" to a previously generated file, that file is not going to be overwritten.
 * Instead, a purely generated file with the file name postfix "_generated" added is going to be written.
 * This functionality represents a one-off feature that is not statically set up for a target but can
 * be controlled by the user, on a file by file basis.
 * 
 * @author mmt
 *
 */
public class ConditionalOneOffContextProvider implements ContextProviderI {

	/* (non-Javadoc)
	 * @see org.jenerateit.annotation.ContextProviderI#getAllContext(java.lang.Class, java.lang.Object)
	 */
	@Override
	public Serializable[] getAllContext(Class<? extends TargetI<? extends TargetDocumentI>> targetClass, Object modelElement) throws ElementNotSupportedException, JenerateITException {

		return new Serializable[] {"", "_generated"};

	}
}
