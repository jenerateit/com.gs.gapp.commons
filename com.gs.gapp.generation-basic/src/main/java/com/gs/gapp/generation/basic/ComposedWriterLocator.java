package com.gs.gapp.generation.basic;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.generationgroup.WriterLocatorI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

public class ComposedWriterLocator implements WriterLocatorI {

	private final Set<WriterLocatorI> writerLocators = new LinkedHashSet<>();

	public ComposedWriterLocator(WriterLocatorI... writerLocatorArray) {
		for (WriterLocatorI writerLocator : writerLocatorArray) {
		    writerLocators.add(writerLocator);
		}
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object element,
			Class<? extends TargetI<?>> targetClass) {
		Class<? extends WriterI> result = null;
		for (WriterLocatorI writerLocator : writerLocators) {
			result = writerLocator.getWriterClass(element, targetClass);
			if (result != null) break;
		}
		return result;
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object element,
			TargetI<?> target) {
		Class<? extends WriterI> result = null;
		for (WriterLocatorI writerLocator : writerLocators) {
			result = writerLocator.getWriterClass(element, target);
			if (result != null) break;
		}
		return result;
	}

	/**
	 * @return the writerLocators
	 */
	public Set<WriterLocatorI> getWriterLocators() {
		return Collections.unmodifiableSet(writerLocators);
	}

}
