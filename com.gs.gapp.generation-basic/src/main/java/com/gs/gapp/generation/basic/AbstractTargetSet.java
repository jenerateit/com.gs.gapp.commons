package com.gs.gapp.generation.basic;

import java.util.Collection;
import java.util.LinkedHashSet;

import org.jenerateit.target.TargetI;

/**
 * Instances of children of this class are supposed to define groups of targets. With this it becomes easier
 * to configure and maintain a set of different generation groups that have lots of target classes in common.
 */
abstract public class AbstractTargetSet extends LinkedHashSet<Class<? extends TargetI<?>>> {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	public AbstractTargetSet() {
		super();
	}

	/**
	 * @param c
	 */
	public AbstractTargetSet(Collection<? extends Class<TargetI<?>>> c) {
		super(c);
	}

	/**
	 * @param initialCapacity
	 * @param loadFactor
	 */
	public AbstractTargetSet(int initialCapacity, float loadFactor) {
		super(initialCapacity, loadFactor);
	}

	/**
	 * @param initialCapacity
	 */
	public AbstractTargetSet(int initialCapacity) {
		super(initialCapacity);
	}
}
