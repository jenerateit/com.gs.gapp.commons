/**
 *
 */
package com.gs.gapp.generation.basic;

import org.jenerateit.generationgroup.WriterLocatorI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.generation.basic.target.DevDocTarget;
import com.gs.gapp.metamodel.basic.DevDoc;
import com.gs.gapp.metamodel.basic.ModelElementCache;


/**
 * In here you find all functionality that all generation groups have in common.
 *
 * @author mmt
 *
 */
public class GenerationGroupDevDoc extends AbstractGenerationGroup {

    /**
	 *
	 */
	public GenerationGroupDevDoc() {
		super(new ModelElementCache());
		addTargetClass(DevDocTarget.class);
	}
	
	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.WriterLocatorI#getWriterClass(java.io.Serializable, java.lang.Class)
	 */
	@Override
	public Class<? extends WriterI> getWriterClass(Object element, Class<? extends TargetI<?>> targetClass) {
		if (targetClass == DevDocTarget.class && element instanceof DevDoc) {
			return DevDocTarget.DevDocWriter.class;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.WriterLocatorI#getWriterClass(java.io.Serializable, org.jenerateit.target.TargetI)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Class<? extends WriterI> getWriterClass(Object element, TargetI<?> target) {
		return getWriterClass(element, (Class<? extends TargetI<?>>) target.getClass());
	}

	@Override
	public WriterLocatorI getWriterLocator() {
		return null;
	}
}
