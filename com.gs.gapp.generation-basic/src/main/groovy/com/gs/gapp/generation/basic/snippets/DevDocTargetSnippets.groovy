package com.gs.gapp.generation.basic.snippets

import com.gs.gapp.metamodel.basic.DevDoc

class DevDocTargetSnippets {

	public static String getHtmlContent(DevDoc devDoc) {
		
		String entries = "";
		String newLine = "";
		
		for (DevDoc.Entry entry : devDoc.getEntries()) {
			entries = entries + newLine + getEntry(entry);
			newLine = "\n";
		}
		
		String result =
"""<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${devDoc.getTitle()}</title>
<style type="text/css">
body {
	font-family: Georgia, "Times New Roman", Times, serif;
	color: white;
	background-color: #000000
}

div {
    padding: 5px;
}

.border {
    border: 1px solid;
}

.bold {
    font-weight: bold;
}

.left-to-right {
    width: 48%;
    display: inline-block;
}

</style>
</head>
<body>
	<h3>${devDoc.getTitle()}</h3>
	<div style="width: 1000px;">
		<div class="border bold">
			<div class="left-to-right">Topic or	Question</div>
			<div class="left-to-right">Answer</div>
		</div>
${entries}
	</div>


</body>
</html>
"""
	}
	

	private static String getEntry(DevDoc.Entry entry) {
		String result ="""
		<div class="border">
		    <div class="left-to-right">${entry.getTopic()}</div>
		    <div class="left-to-right">${entry.getAnswer()}</div>
	    </div>"""
        return result;
	}

}
