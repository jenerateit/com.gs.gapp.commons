/**
 * 
 */
package com.gs.gapp.metamodel.anyfile;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import org.jenerateit.target.TargetException;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * Instances of this class represent files that are going to be created through generation.
 * The instances contain name, path, file extension and the content. Typically, those instances
 * define the whole path of the file and are not going to be prefixed by the value of a
 * generation group's transformation option.
 * 
 * 
 * 
 * @author mmt
 *
 */
public class AnyFile extends ModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -139671188807763605L;
	
	private final String path;
	
	private final String extension;
	
	private byte[] content;
	
	private boolean binary;

	/**
	 * @param name
	 * @param path
	 */
	public AnyFile(String name, String path, String extension) {
		super(name);
		this.path = path;
		this.extension = extension;
		this.binary = false;
	}

	public String getPath() {
		return path;
	}
	
	private String getUrlEncodedPath() {
		if (path != null) {
			String pathForProcessing = path.replace("\\", "/");  // make sure that any backslashes are not creating problems
			StringBuilder encodedPathStringBuilder = new StringBuilder();
			String[] pathSegments = pathForProcessing.split("/");
			if (pathSegments != null && pathSegments.length > 0) {
				for (String pathSegment : pathSegments) {
					
					try {
						String encodedPathSegment = URLEncoder.encode(pathSegment, StandardCharsets.UTF_8.name());
						if (pathSegment.length() == 0 ||
							encodedPathStringBuilder.length() > 0 && encodedPathStringBuilder.charAt(encodedPathStringBuilder.length()-1) != '/') {
							encodedPathStringBuilder.append("/");
						}
						encodedPathStringBuilder.append(encodedPathSegment);
					} catch (UnsupportedEncodingException ex) {
						throw new TargetException("Error while encoding path '" + getPath() + "' for TextFile " + this.toString(), ex);
					}
				}
			} else {
				encodedPathStringBuilder.append(pathForProcessing);
			}
			
			return encodedPathStringBuilder.toString();
		}
		
		return path;
	}
	
	private String getUrlEncodedName() {
		if (getName() != null) {
		    try {
				return URLEncoder.encode(getName(), StandardCharsets.UTF_8.name());
			} catch (UnsupportedEncodingException ex) {
				throw new TargetException("Error while encoding name '" + getName() + "' for TextFile " + this.toString(), ex);
			}
		}
		
		return getName();
	}
	
	private String getUrlEncodedExtension() {
		if (getExtension() != null) {
		    try {
				return URLEncoder.encode(getExtension(), StandardCharsets.UTF_8.name());
			} catch (UnsupportedEncodingException ex) {
				throw new TargetException("Error while encoding extension '" + getExtension() + "' TextFile " + this.toString(), ex);
			}
		}
		
		return getExtension();
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}
	
	public void setBinary(boolean binary) {
		this.binary = binary;
	}

	public boolean isBinary() {
		return binary;
	}
	
	/**
	 * @return the uri that is composed by path, extension and name that is set for this instance of AnyFile (everything is url-encoded except for slashes)
	 */
	public URI getUri() {
        StringBuilder sb = new StringBuilder();
        
        if (getPath() != null) {
        	sb.append(getUrlEncodedPath());
        	if (sb.charAt(sb.length()-1) != '/') sb.append("/");
        }
        
        sb.append(getUrlEncodedName());
        if (getExtension() != null && getExtension().length() > 0) {
        	sb.append(".").append(getUrlEncodedExtension());
        }
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException ex) {
			throw new TargetException("Error while creating target URI for TextFile " + this.toString(), ex);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((path == null) ? 0 : path.hashCode()) + (binary == false ? 0 : 1);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AnyFile other = (AnyFile) obj;
		if (binary != other.binary)
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		return true;
	}

	public String getExtension() {
		return extension;
	}
	
}
