package com.gs.vd.converter.groovy.any;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.codehaus.groovy.control.CompilationUnit;
import org.codehaus.groovy.control.Phases;
import org.codehaus.groovy.tools.GroovyClass;
import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterI;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.wiring.BundleWiring;

import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.vd.jenerateit.modelaccess.groovy.GroovyScriptUnit;

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;


public abstract class GroovyToAnyConverter extends AbstractConverter {

	/**
	 * @param modelElementCache
	 */
	public GroovyToAnyConverter(ModelElementCache modelElementCache) {
		super(modelElementCache);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelElementConversion(java.util.Set)
	 */
	@Override
	protected void onPerformModelElementConversion(Set<?> modelElements) {
		// intentionally not executing standard conversion logic
		//super.onPerformModelElementConversion(modelElements);
		ClassLoader parent = FrameworkUtil.getBundle(this.getClass()).adapt(BundleWiring.class).getClassLoader();
		GroovyClassLoader loader = new GroovyClassLoader(parent);
		loader.clearCache();
		loader.clearAssertionStatus();
		loader.setShouldRecompile(true);
		
		Set<Class<?>> groovyClasses = parseClasses(modelElements, loader);
		
		for (Class<?> groovyClass : groovyClasses) {
			// let's call some method on the instances
			GroovyObject groovyObject;
			try {
				if (ModelConverterI.class.isAssignableFrom(groovyClass)) {
					groovyObject = (GroovyObject) groovyClass.newInstance();
					ModelConverterI mc = (ModelConverterI) groovyObject;
					Set<Object> conversionResult = mc.convert(null, null);
					for (Object element : conversionResult) {
						if (element instanceof ModelElementI) {
					        ModelElementI modelElementFromGroovyScript = (ModelElementI) element;
					        // finally, adding model element to this converter's cache
							this.addModelElement(modelElementFromGroovyScript);
						}
					}
				}
			} catch (Throwable th) {
				th.printStackTrace();
				throw new ModelConverterException("execution of groovy code to create the platform specific test model failed (groovy source: " + groovyClass.getName() + ")", th);
			}
		}
		
		if (loader != null) {
			loader.clearCache();
			loader.clearAssertionStatus();
		}
	}
	
	protected Set<Class<?>> parseClasses(Set<?> modelElements, GroovyClassLoader loader) {
		Set<Class<?>> groovyClasses = new LinkedHashSet<>();
		CompilationUnit compilationUnit = new CompilationUnit(loader);
		
		for (Object element : modelElements) {
			if (element instanceof GroovyScriptUnit) {
				GroovyScriptUnit scriptUnit = (GroovyScriptUnit) element;
				compilationUnit.addSource(scriptUnit.getName(), scriptUnit.getCode());
			}
		}
		
		compilationUnit.compile(Phases.CLASS_GENERATION);
				
		for (Object o : compilationUnit.getClasses()) {
			if (o instanceof GroovyClass) {
				GroovyClass groovyClass = (GroovyClass) o;
				Class<?> clazz = loader.defineClass(groovyClass.getName(), groovyClass.getBytes());
				groovyClasses.add(clazz);
			}
		}
				
		return groovyClasses;
	}


	@Override
	protected List<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> result =
				new ArrayList<>();
		
		return result;
	}
}
