package com.gs.gapp.metamodel.basic.typesystem;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.Module;

/**
 * @author marcu
 *
 */
public class TestValidatorDuplicateFields {
	
	/**
	 * 
	 */
	@Test
	public void test() {
		Model model = new Model("MyModel");
		
		final Module module = new Module("M");
		model.addElement(module);
		
		final ComplexType ComplexType0 = new ComplexType("e0");
		module.addElement(ComplexType0);
		new Field("NaMe0", ComplexType0);
		new Field("Name0", ComplexType0);
		new Field("name0", ComplexType0);
		
		final ComplexType ComplexType1 = new ComplexType("e1");
		module.addElement(ComplexType1);
		new Field("Name1", ComplexType1);
		new Field("NAMe1", ComplexType1);
		new Field("Name1", ComplexType1);
		
		final ComplexType ComplexType2 = new ComplexType("e2");
		module.addElement(ComplexType2);
		new Field("Name2", ComplexType2);
		new Field("Name2", ComplexType2);
		new Field("namE2", ComplexType2);
		
		Collection<Object> modelElements = new ArrayList<>();
		modelElements.add(ComplexType0);
		modelElements.add(ComplexType1);
		modelElements.add(ComplexType2);
		
		Collection<Message> errorMessages = new ValidatorDuplicateFields().validate(modelElements);
		for (Message errorMessage : errorMessages) {
			System.out.println("Error:" + errorMessage.getMessage());
		}
		assertTrue("not enought or too many error messages found", errorMessages.size() == 4);
	}
}
