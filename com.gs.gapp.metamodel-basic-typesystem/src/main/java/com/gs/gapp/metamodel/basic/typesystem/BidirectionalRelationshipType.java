/**
 * 
 */
package com.gs.gapp.metamodel.basic.typesystem;

/**
 * Wherever you have a type X's field A that is of type Y and in Y you do _not_ have a field B that
 * is of type X and relates to the field A, you can use BidirectionalRelationshipType to specify
 * the type of relation as if there were such a field B.
 * Example: A bidirectional relation between field A (with CollectionType set) and field B (also with CollectionType set)
 * represents a MANY_TO_MANY relationship, _without_ having to set a value of BidirectionalRelationshipType on the field A.
 * However, if field B does not exist, the semantics could be either MANY_TO_ONE or MANY_TO_MANY. In this case you have to set
 * a value of BidirectionalRelationshipType in order to resolve this ambiguosity.
 * 
 * @author mmt
 *
 */
public enum BidirectionalRelationshipType {
	ONE_TO_ONE,
	ONE_TO_MANY,
	MANY_TO_ONE,
	MANY_TO_MANY,
	;

}
