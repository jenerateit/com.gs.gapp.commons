package com.gs.gapp.metamodel.basic.typesystem;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;

import com.gs.gapp.metamodel.basic.MetamodelI;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;

/**
 * @author marcu
 *
 */
public enum BasicTypesystemMetamodel implements MetamodelI {

	INSTANCE,
	;
	
	private static final LinkedHashSet<Class<? extends ModelElementI>> metatypes = new LinkedHashSet<>();
	private static final Collection<Class<? extends ModelElementI>> collectionOfCheckedMetatypes = new LinkedHashSet<>();
	
	static {
		metatypes.add(ComplexType.class);
		metatypes.add(Enumeration.class);
		metatypes.add(EnumerationEntry.class);
		metatypes.add(ExceptionType.class);
		metatypes.add(Field.class);
		metatypes.add(PrimitiveType.class);
		
		collectionOfCheckedMetatypes.add(ComplexType.class);
		collectionOfCheckedMetatypes.add(Enumeration.class);
		collectionOfCheckedMetatypes.add(ExceptionType.class);
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypes() {
		return Collections.unmodifiableCollection(metatypes);
	}

	@Override
	public boolean isIncluded(Class<? extends ModelElementI> metatype) {
		return metatypes.contains(metatype);
	}

	@Override
	public boolean isExtendingOneOfTheMetatypes(Class<? extends ModelElementI> metatype) {
		for (Class<? extends ModelElementI> metatypeOfMetamodel : metatypes) {
			if (metatypeOfMetamodel.isAssignableFrom(metatype)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypesForConversionCheck() {
		return collectionOfCheckedMetatypes;
	}
	
	/**
	 * @param metatype
	 * @return
	 */
	public Class<? extends Module> getModuleType(Class<? extends ModelElementI> metatype) {
		if (isIncluded(metatype)) {
		    return Module.class;
		}
		return null;
	}
}
