/**
 *
 */
package com.gs.gapp.metamodel.basic.typesystem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/**
 * @author mmt
 *
 */
public class ComplexType extends Type {

	/**
	 *
	 */
	private static final long serialVersionUID = 469992155078333397L;

	private Set<Field> fields = new LinkedHashSet<>();

	private ComplexType parent;

	private boolean abstractType = false;
	
	private Boolean externalizable;
	
	private Boolean binaryCoding;
	
	private Endianness binaryCodingEndianness;
	
	private ComplexType owner;
	
	private boolean validated;

	// --- AMMBASIC-6
	private CollectionType collectionType;
	private Type valueType;
	private Type keyType; // only relevant when collection type is KEYVALUE
	private int dimension = 1; // only relevant when collection type is ARRAY

	/**
	 * @param name
	 */
	public ComplexType(String name) {
		super(name);
	}

	/**
	 * @return the fields
	 */
	public Set<Field> getFields() {
		return fields;
	}

	/**
	 * @return
	 */
	public Set<Field> getAllFields() {
		Set<Field> result = new LinkedHashSet<>();

		ComplexType aParent = this;
		while (aParent != null) {
			result.addAll(aParent.getFields());
			aParent = aParent.getParent();
		}

		return result;
	}
	
	/**
	 * @return the list of all ancestors, including this ComplexType (which is at the end of the list)
	 */
	public List<? extends ComplexType> getAncestors() {
		List<ComplexType> result = new ArrayList<>();
		
		ComplexType parent = this;
		while (parent != null) {
			result.add(0, parent);
			parent = parent.getParent();
		}
		
		return result;
	}
	
	/**
	 * @return all fields, including fields of all ancestors, ordering: fields of ancestors before fields of descendants
	 */
	public Set<? extends Field> getAllFieldsAncestorsFirst() {
		Set<Field> result = new LinkedHashSet<>();

		for (ComplexType type : getAncestors()) {
			for (Field field : type.getFields()) {
				if (!result.add(field)) {
					// field could not be added since the result set already contained a different object that is equal to the object to be added
					if (result.remove(field)) {
						result.add(field);
					}
				}
			}
		}
		
		return result;
	}

	/**
	 * @param field the field to add
	 * @return true if this set did not already contain the specified element
	 */
	protected boolean addFields(Field field) {
		return this.fields.add(field);
	}

	/**
	 * @return the parent
	 */
	public ComplexType getParent() {
		return parent;
	}
	
	/**
	 * @return
	 */
	public Collection<ComplexType> getAllParents() {
		LinkedHashSet<ComplexType> parents = new LinkedHashSet<>();
		ComplexType aParent = this.getParent();
		while (aParent != null) {
			parents.add(aParent);
			aParent = aParent.getParent();
		}
		return parents;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(ComplexType parent) {
		this.parent = parent;
	}
	
	/**
	 * A complex type can be owned by another complex type. Note that there isn't
	 * a meaningful conversion/generation target for this construct in all programming
	 * languages. For Java for instance, a complex type that is owned by another complex type
	 * will result in an inner or a nested class. In case of Delphi, a nested type declaration
	 * will be generated: see <a href="http://docwiki.embarcadero.com/RADStudio/Sydney/en/Nested_Type_Declarations">Nested Type Declarations</a>
	 * 
	 * 
	 * @return the owner
	 */
	public ComplexType getOwner() {
		return owner;
	}

	/**
	 * @param owner the owner to set
	 */
	public void setOwner(ComplexType owner) {
		this.owner = owner;
	}

	/**
	 * @return the abstractType
	 */
	public boolean isAbstractType() {
		return abstractType;
	}

	/**
	 * @param abstractType the abstractType to set
	 */
	public void setAbstractType(boolean abstractType) {
		this.abstractType = abstractType;
	}
	
	public boolean isValidated() {
		return validated;
	}

	public void setValidated(boolean validated) {
		this.validated = validated;
	}

	/**
	 * @return
	 */
	public Set<ComplexType> getAllUsedComplexTypes() {
		LinkedHashSet<ComplexType> result = new LinkedHashSet<>();
		collectAllComplexTypes(result);
		return result;
	}
	
	/**
	 * @param result
	 */
	private void collectAllComplexTypes(LinkedHashSet<ComplexType> result) {
		result.addAll(this.getAllParents());
		for (Field field : getAllFields()) {
			if (field.getType() instanceof ComplexType && !result.contains(field.getType())) {
				ComplexType complexType = (ComplexType) field.getType();
				result.add(complexType);
				result.addAll(complexType.getAllParents());
				complexType.collectAllComplexTypes(result);
			}
		}
	}

	/**
	 * @return the externalizable
	 */
	public Boolean getExternalizable() {
		if (externalizable != null) {
			return externalizable;
		}
		
		ComplexType parent = this.getParent();
		while (parent != null) {
			if (parent.externalizable != null) {
				return parent.externalizable;
			}
			parent = parent.getParent();
		}
		
		return null;
	}

	/**
	 * @param externalizable the externalizable to set
	 */
	public void setExternalizable(Boolean externalizable) {
		this.externalizable = externalizable;
	}

	/**
	 * @return the binaryCoding
	 */
	public Boolean getBinaryCoding() {
		return binaryCoding;
	}

	/**
	 * @param binaryCoding the binaryCoding to set
	 */
	public void setBinaryCoding(Boolean binaryCoding) {
		this.binaryCoding = binaryCoding;
	}

	/**
	 * @return the binaryCodingEndianness
	 */
	public Endianness getBinaryCodingEndianness() {
		return binaryCodingEndianness;
	}

	/**
	 * @param binaryCodingEndianness the binaryCodingEndianness to set
	 */
	public void setBinaryCodingEndianness(Endianness binaryCodingEndianness) {
		this.binaryCodingEndianness = binaryCodingEndianness;
	}

	public CollectionType getCollectionType() {
		return collectionType;
	}

	public void setCollectionType(CollectionType collectionType) {
		this.collectionType = collectionType;
	}

	public Type getValueType() {
		return valueType;
	}

	public void setValueType(Type valueType) {
		this.valueType = valueType;
	}

	public Type getKeyType() {
		return keyType;
	}

	public void setKeyType(Type keyType) {
		this.keyType = keyType;
	}

	public int getDimension() {
		return dimension;
	}

	public void setDimension(int dimension) {
		this.dimension = dimension;
	}
}
