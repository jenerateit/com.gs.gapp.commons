/*
 * CascadeType.java
 *
 * Created on 24. Oktober 2006, 21:12
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.gs.gapp.metamodel.basic.typesystem;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author mmt
 */
public enum CollectionType {
	ARRAY ("array"),
    LIST ("list"),
    SET ("set"),
    SORTED_SET ("sortedset"),
    KEYVALUE ("keyvalue"),
    ;
    
    private static final Map<String, CollectionType> stringToEnum =
		new LinkedHashMap<>();
	
	static {
		for (CollectionType collectionType : values()) {
			stringToEnum.put(collectionType.name.toLowerCase(), collectionType);
		}
	}
	
	private final String name;

	/**
	 * @param name
	 */
	private CollectionType(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public static CollectionType fromString(String name) {
		return stringToEnum.get(name.trim().toLowerCase());
	}
}
