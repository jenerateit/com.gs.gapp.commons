package com.gs.gapp.metamodel.basic.typesystem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelValidatorI;

/**
 * This validator checks whether there are types or enumerations that
 * have multiple fields with the same name.
 * 
 * @author marcu
 *
 */
public class ValidatorDuplicateFields implements ModelValidatorI {
	
	private final Set<Class<?>> clazzesToCheck = new HashSet<>();

	public ValidatorDuplicateFields() {
		clazzesToCheck.add(ComplexType.class);
		clazzesToCheck.add(Enumeration.class);
	}
	
	public ValidatorDuplicateFields(Class<?> ...classes) {
		if (classes != null) {
			java.util.stream.Stream.of(classes).forEach(clazz -> clazzesToCheck.add(clazz));
					
		}
	}

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(assertNoDuplicateFields(modelElements));
		return result;
	}

	/**
	 * @param rawElements
	 * @return
	 */
	private Collection<Message> assertNoDuplicateFields(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();
		
		Message summaryMessage = null;
		
		for (Object element : rawElements) {
			if (element instanceof ComplexType && clazzesToCheck.contains(element.getClass())) {
				ComplexType complexType = (ComplexType) element;  // note that Enumeration inherits from ComplexType
				Map<String,ArrayList<Field>> fieldsMap = new LinkedHashMap<>();
				for (Field field : complexType.getFields()) {
					ArrayList<Field> fields = fieldsMap.get(field.getName().toLowerCase());
					if (fields == null) {
						fields = new ArrayList<>();
						fieldsMap.put(field.getName().toLowerCase(), fields);
					}
					fields.add(field);
				}
				
				for (String fieldName : fieldsMap.keySet()) {
					ArrayList<Field> fields = fieldsMap.get(fieldName);
					if (fields.size() > 1) {
						StringBuilder fieldAndTypeNames = new StringBuilder();
						String comma = "";
						for (Field aField : fields) {
							fieldAndTypeNames.append(comma).append("'").append(aField.getName()).append("'").
							    append(" in type ").append("'").append(complexType.getName()).append("'");
							comma = ", ";
						}
						
						if (summaryMessage == null) {
							summaryMessage = TypesystemMessage.DUPLICATE_TYPE_FIELD_NAMES_SUMMARY.getMessageBuilder()
									.build();
							result.add(summaryMessage);
						}
						
						Message message = TypesystemMessage.DUPLICATE_TYPE_FIELD_NAMES.getMessageBuilder()
								.parameters(fields.size(), fieldName, fieldAndTypeNames.toString()).build();
						result.add(message);
					}
				}
			}
		}

		return result;
	}
}
