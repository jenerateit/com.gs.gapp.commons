/**
 *
 */
package com.gs.gapp.metamodel.basic.typesystem;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author mmt
 *
 */
public class Enumeration extends ComplexType {

	/**
	 *
	 */
	private static final long serialVersionUID = -7709148489374322095L;
	
	public static final String FIELD_NAME_FOR_ENUM_ENTRY_VALUE = "enumEntryValue";

	private final Set<EnumerationEntry> enumerationEntries = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public Enumeration(String name) {
		super(name);
	}

	/**
	 * @return the enumerationLiterals
	 */
	public Set<EnumerationEntry> getEnumerationEntries() {
		return enumerationEntries;
	}

	/**
	 * @param enumerationEntry
	 * @return
	 */
	public boolean addEnumerationEntry(EnumerationEntry enumerationEntry) {
		return this.enumerationEntries.add(enumerationEntry);
	}
}
