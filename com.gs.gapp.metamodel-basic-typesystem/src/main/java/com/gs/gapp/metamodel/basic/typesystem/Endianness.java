package com.gs.gapp.metamodel.basic.typesystem;

public enum Endianness {

	BIG_ENDIAN,
	LITTLE_ENDIAN,
	;
}
