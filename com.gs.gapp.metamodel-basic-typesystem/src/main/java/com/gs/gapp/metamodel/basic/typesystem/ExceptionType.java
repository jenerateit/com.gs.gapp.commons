/**
 *
 */
package com.gs.gapp.metamodel.basic.typesystem;


/**
 * @author mmt
 *
 */
public class ExceptionType extends ComplexType {
	
	private String defaultExceptionMessage;

	/**
	 *
	 */
	private static final long serialVersionUID = -6754932602639420906L;

	/**
	 * @param name
	 */
	public ExceptionType(String name) {
		super(name);
	}

	/**
	 * This message can be used in generated code, to define a message text that
	 * describes the meaning of the exception a bit more verbose than just by the
	 * name of a class.
	 * 
	 * @return the defaultExceptionMessage
	 */
	public String getDefaultExceptionMessage() {
		return defaultExceptionMessage;
	}

	/**
	 * @param defaultExceptionMessage the defaultExceptionMessage to set
	 */
	public void setDefaultExceptionMessage(String defaultExceptionMessage) {
		this.defaultExceptionMessage = defaultExceptionMessage;
	}
}
