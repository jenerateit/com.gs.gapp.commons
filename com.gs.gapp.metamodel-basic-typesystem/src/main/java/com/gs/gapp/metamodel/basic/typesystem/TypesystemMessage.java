package com.gs.gapp.metamodel.basic.typesystem;

import com.gs.gapp.metamodel.basic.MessageI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.MessageStatus;

public enum TypesystemMessage implements MessageI {

	DUPLICATE_TYPE_FIELD_NAMES_SUMMARY (MessageStatus.ERROR, "0002a",
			"Multiple fields with identical names (names may differ in upper and lower case), in one and the same type, are not allowed and cannot be used for code generation.",
			"Modify the model by changing the name of one or more fields."),
	DUPLICATE_TYPE_FIELD_NAMES (MessageStatus.ERROR, "0002b",
			"Found %d fields with the name '%s': '%s'.",
			"Modify the model by changing the name of one or more fields."),
	
	
	FIELD_INCOMPLETE_KEY_VALUE_COLLECTION_TYPE (MessageStatus.WARNING, "1001",
			"The field '%s' has the key-value collection type modeled, but the provided information is incomplete (key type is missing)."
			+ " The string type will be used as the type for the key.",
			"If the string-typed key is OK for you, nothing has to be done. Otherwise, edit the field in the model and set the key type there."),
	;

	private final MessageStatus status;
	private final String organization = "GS";
	private final String section = "TYPESYSTEM";
	private final String id;
	private final String problemDescription;
	private final String instruction;
	
	private TypesystemMessage(MessageStatus status, String id, String problemDescription, String instruction) {
		this.status = status;
		this.id = id;
		this.problemDescription = problemDescription;
		this.instruction = instruction;
	}
	
	@Override
	public MessageStatus getStatus() {
		return status;
	}

	@Override
	public String getOrganization() {
		return organization;
	}

	@Override
	public String getSection() {
		return section;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getProblemDescription() {
		return problemDescription;
	}
	
	@Override
	public String getInstruction() {
		return instruction;
	}
}
 