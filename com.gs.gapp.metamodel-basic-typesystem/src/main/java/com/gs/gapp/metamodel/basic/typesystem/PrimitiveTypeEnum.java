/**
 *
 */
package com.gs.gapp.metamodel.basic.typesystem;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

/**
 * @author mmt
 *
 */
public enum PrimitiveTypeEnum {
	/** one bit (e.g. java = boolean; Delphi = Boolean) */
	UINT1 ("uint1", true),
	/** unsigned integer number with one byte (e.g. java = short; Delphi = Byte) */
	UINT8 ("uint8", true),
	/** signed integer number with one byte (e.g. java = byte; Delphi = ShortInt) */
	SINT8 ("sint8", true),
	/** unsigned integer number with two bytes (e.g. java = int; Delphi = Word) */
	UINT16 ("uint16", true),
	/** signed integer number with two bytes (e.g. java = short; Delphi = SmallInt) */
	SINT16 ("sint16", true),
	/** unsigned integer number with 4 bytes (e.g. java = long; Delphi = Cardinal) */
	UINT32 ("uint32", true),
	/** signed integer number with 4 bytes (e.g. java = int; Delphi = Integer) */
	SINT32 ("sint32", true),
	/** signed integer number with 8 bytes (e.g. java = BigDecimal; Delphi = UInt64) */
	UINT64 ("uint64", true),
	/** signed integer number with 8 bytes (e.g. java = long; Delphi = Int64) */
	SINT64 ("sint64", true),
	/** unsigned integer number with 16 bytes  */
	UINT128 ("uint128", true),
	/** signed integer number with 16 bytes */
	SINT128 ("sint128", true),

	/** signed floating point number with 2 bytes */
	FLOAT16 ("float16", true),
	/** signed floating point number with 4 bytes (e.g. java = float; Delphi = Single) */
	FLOAT32 ("float32", true),
	/** signed floating point number with 8	 bytes (e.g. java = double; Delphi = Double) */
	FLOAT64 ("float64", true),
	/** signed floating point number with 16 bytes (e.g. java = BigDecimal; Delphi = ??Extended??) */
	FLOAT128 ("float128", true),

	/** a single character */
	CHARACTER ("character"),
	/** a string (collection of characters) */
	STRING ("string"),

	/** date (e.g. Year, Month, Day) */
	DATE ("Date"),
	/** time (e.g. Hour, Minute, Seconds, Milliseconds, ...) */
	TIME ("Time"),
	/** a date and time (e.g. Year, Month, Day, Hour, Minute, Seconds, Milliseconds, ...) */
	DATETIME ("Datetime"),
	
	/** UUID, Universally unique identifier */
	UUID ("uuid"),

//	STRING ("String"),
//	INTEGER ("Integer", true),
//	LONG ("Long", true),
//	FLOAT ("Float", true),
//	DOUBLE ("Double", true),
//	BOOLEAN ("Boolean"),
//	TIME ("Time"),
//	DATE ("Date"),
//	DATETIME ("Datetime"),
//	TIMESTAMP ("Timestamp"),
//	BIG_DECIMAL ("BigDecimal", true),
//	BYTE ("Byte"),
	;

	private static final Map<String, PrimitiveTypeEnum> stringToEnum =
		new HashMap<>();

	private static final Map<String, PrimitiveTypeEnum> stringToEnumCaseInsensitive =
			new HashMap<>();

	static {
		for (PrimitiveTypeEnum t : values()) {
			stringToEnum.put(t.name, t);
			stringToEnumCaseInsensitive.put(t.name.toLowerCase(), t);
			PrimitiveType primitiveType = new PrimitiveType(t.name);
			primitiveType.setOriginatingElement(t);  // This is part of the solution for issue AJAVA-18
			primitiveType.setNumeric(t.getNumeric());
			primitiveType.setInGlobalCache(true);
			t.setPrimitiveType(primitiveType);
			
			if (t == DATE || t == DATETIME || t == TIME) {
				t.temporal = true;
			} else {
				t.temporal = false;
			}
		}
	}

	private final String name;

	private Boolean numeric;
	
	private boolean temporal;

	private PrimitiveType primitiveType;

	/**
	 * @param name
	 */
	private PrimitiveTypeEnum(String name) {
		this.name = name;
	}

	private PrimitiveTypeEnum(String name, Boolean numeric) {
		this.name = name;
		this.numeric = numeric;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * @return
	 */
	public static PrimitiveTypeEnum fromString(String name) {
		return stringToEnum.get(name);
	}

	/**
	 * @param name
	 * @return
	 */
	public static PrimitiveTypeEnum fromStringCaseInsensitive(String name) {
		return stringToEnumCaseInsensitive.get(name.toLowerCase());
	}
	
	/**
	 * @param type
	 * @return
	 */
	public static PrimitiveTypeEnum getEnumEntry(Type type) {
		return Stream.of(values()).filter(p -> p.getPrimitiveType() == type).findFirst().orElse(null);
	}

	/**
	 * @return the primitiveType
	 */
	public PrimitiveType getPrimitiveType() {
		return primitiveType;
	}

	/**
	 * @param primitiveType the primitiveType to set
	 */
	private void setPrimitiveType(PrimitiveType primitiveType) {
		this.primitiveType = primitiveType;
	}

	/**
	 * @return the numeric
	 */
	public Boolean getNumeric() {
		return numeric;
	}

	public boolean isTemporal() {
		return temporal;
	}
}
