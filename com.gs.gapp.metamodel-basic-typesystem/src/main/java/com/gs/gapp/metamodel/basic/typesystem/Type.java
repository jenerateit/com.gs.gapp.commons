/**
 *
 */
package com.gs.gapp.metamodel.basic.typesystem;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * @author mmt
 *
 */
public abstract class Type extends ModelElement {

	/**
	 *
	 */
	private static final long serialVersionUID = 1017702271904268804L;

	/**
	 * @param name
	 */
	public Type(String name) {
		super(name);
	}

}
