package com.gs.gapp.metamodel.basic.typesystem;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * Instances of this type hold constraints for primitive types.
 * 
 * If a contraints object transitively relates to a primitive type through
 * a chain of constraints objects, the constraints are summarized. The first
 * constraint that is being found in the chain of constraints is taken.
 * This mechanism lets you overwrite reuse and overwrite existing constraints. 
 * 
 *
 */
public class PrimitiveTypeConstraints extends ModelElement {

	private static final long serialVersionUID = -4740061102058091646L;
	
	private final PrimitiveType constrainedType;
	
	// --- constraints that are related to numberic primitive types
	private Boolean minValueInclusive;
	private Boolean maxValueInclusive;
	private Long minValue;
	private Long maxValue;
	private Double minDoubleValue;
	private Double maxDoubleValue;
	private BigDecimal minBigDecimalValue;
	private BigDecimal maxBigDecimalValue;
	private Integer totalDigits;
	private Integer fractionDigits;
	
	// --- constraints related to strings
	private String pattern;
	private Long minLength;
	private Long maxLength;
	private Long exactLength;
	private WhitespacePreserve whitespacePreserve;

	// --- constraints that are common to all primitive types
	private Boolean mandatory;	
	private String defaultValue;
	

	public PrimitiveTypeConstraints(String name, PrimitiveType constrainedType) {
		super(name);
		if (constrainedType == null) {
			throw new NullPointerException("A primitive type constraints object must have a constrained type set, but parameter 'constrainedType' is null.");
		}
		this.constrainedType = constrainedType;
	}

	/**
	 * @return the constrainedType
	 */
	public PrimitiveType getConstrainedType() {
		return constrainedType;
	}
	
	/**
	 * @return chain of constraints
	 */
	public List<PrimitiveTypeConstraints> getChainOfConstraints() {
		ArrayList<PrimitiveTypeConstraints> result = new ArrayList<>();
		
		PrimitiveTypeConstraints currentConstraints = this;
		do {
			result.add(currentConstraints);
			PrimitiveType primitiveType = currentConstraints.getConstrainedType();
			if (primitiveType != null) {
				currentConstraints = primitiveType.getConstraints();
			} else {
				currentConstraints = null;
			}
		} while (currentConstraints != null);
		
		return result;
	}
	
	/**
	 * @return
	 */
	public PrimitiveType getRootConstrainedType() {
		PrimitiveType result = this.constrainedType;
		while (result.getConstraints() != null) {
			result = result.getConstraints().getConstrainedType();
		};
		
		if (!result.isBuiltInPrimitiveType()) {
			throw new RuntimeException("The root constrained type of a chain of constraints must be a built-in primitive type, but it isn't (" + result.getName() + ").");
		}
		
		return result;
	}
	
	/**
	 * @return the minValue
	 */
	public Long getMinValue() {
		return minValue;
	}
	
	/**
	 * @return the minValueInclusive
	 */
	public Long getEffectiveMinValue() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.minValue != null) {
				return constraints.minValue;
			}
		}
		return null;
	}

	/**
	 * @param minValue the minValue to set
	 */
	public void setMinValue(Long minValue) {
		this.minValue = minValue;
	}

	/**
	 * @return the maxValue
	 */
	public Long getMaxValue() {
		return maxValue;
	}
	
	/**
	 * @return the maxValue
	 */
	public Long getEffectiveMaxValue() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.maxValue != null) {
				return constraints.maxValue;
			}
		}
		return null;
	}

	/**
	 * @param maxValue the maxValue to set
	 */
	public void setMaxValue(Long maxValue) {
		this.maxValue = maxValue;
	}

	/**
	 * @return the minDoubleValue
	 */
	public Double getMinDoubleValue() {
		return minDoubleValue;
	}
	
	/**
	 * @return the minDoubleValue
	 */
	public Double getEffectiveMinDoubleValue() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.minDoubleValue != null) {
				return constraints.minDoubleValue;
			}
		}
		return null;
	}

	/**
	 * @return the maxDoubleValue
	 */
	public Double getMaxDoubleValue() {
		return maxDoubleValue;
	}
	
	/**
	 * @return the maxDoubleValue
	 */
	public Double getEffectiveMaxDoubleValue() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.maxDoubleValue != null) {
				return constraints.maxDoubleValue;
			}
		}
		return null;
	}

	/**
	 * @return the minBigDecimalValue
	 */
	public BigDecimal getMinBigDecimalValue() {
		return minBigDecimalValue;
	}
	
	/**
	 * @return the minBigDecimalValue
	 */
	public BigDecimal getEffectiveMinBigDecimalValue() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.minBigDecimalValue != null) {
				return constraints.minBigDecimalValue;
			}
		}
		return null;
	}

	/**
	 * @return the maxBigDecimalValue
	 */
	public BigDecimal getMaxBigDecimalValue() {
		return maxBigDecimalValue;
	}
	
	/**
	 * @return the maxBigDecimalValue
	 */
	public BigDecimal getEffectiveMaxBigDecimalValue() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.maxBigDecimalValue != null) {
				return constraints.maxBigDecimalValue;
			}
		}
		return null;
	}

	/**
	 * @return the totalDigits
	 */
	public Integer getTotalDigits() {
		return totalDigits;
	}
	
	/**
	 * @return the totalDigits
	 */
	public Integer getEffectiveTotalDigits() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.totalDigits != null) {
				return constraints.totalDigits;
			}
		}
		return null;
	}

	/**
	 * @param totalDigits the totalDigits to set
	 */
	public void setTotalDigits(Integer totalDigits) {
		this.totalDigits = totalDigits;
	}

	/**
	 * @return the fractionDigits
	 */
	public Integer getFractionDigits() {
		return fractionDigits;
	}
	
	/**
	 * @return the fractionDigits
	 */
	public Integer getEffectiveFractionDigits() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.fractionDigits != null) {
				return constraints.fractionDigits;
			}
		}
		return null;
	}

	/**
	 * @param fractionDigits the fractionDigits to set
	 */
	public void setFractionDigits(Integer fractionDigits) {
		this.fractionDigits = fractionDigits;
	}

	/**
	 * @return the pattern
	 */
	public String getPattern() {
		return pattern;
	}
	
	/**
	 * @return the pattern
	 */
	public String getEffectivePattern() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.pattern != null) {
				return constraints.pattern;
			}
		}
		return null;
	}

	/**
	 * @param pattern the pattern to set
	 */
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	/**
	 * @return the minLength
	 */
	public Long getMinLength() {
		return minLength;
	}
	
	/**
	 * @return the minLength
	 */
	public Long getEffectiveMinLength() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.minLength != null) {
				return constraints.minLength;
			}
		}
		return null;
	}

	/**
	 * @param minLength the minLength to set
	 */
	public void setMinLength(Long minLength) {
		this.minLength = minLength;
	}

	/**
	 * @return the maxLength
	 */
	public Long getMaxLength() {
		return maxLength;
	}
	
	/**
	 * @return the maxLength
	 */
	public Long getEffectiveMaxLength() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.maxLength != null) {
				return constraints.maxLength;
			}
		}
		return null;
	}

	/**
	 * @param maxLength the maxLength to set
	 */
	public void setMaxLength(Long maxLength) {
		this.maxLength = maxLength;
	}

	/**
	 * @return the exactLength
	 */
	public Long getExactLength() {
		return exactLength;
	}
	
	/**
	 * @return the exactLength
	 */
	public Long getEffectiveExactLength() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.exactLength != null) {
				return constraints.exactLength;
			}
		}
		return null;
	}

	/**
	 * @param exactLength the exactLength to set
	 */
	public void setExactLength(Long exactLength) {
		this.exactLength = exactLength;
	}

	/**
	 * @return the whitespacePreserve
	 */
	public WhitespacePreserve getWhitespacePreserve() {
		return whitespacePreserve;
	}
	
	/**
	 * @return the whitespacePreserve
	 */
	public WhitespacePreserve getEffectiveWhitespacePreserve() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.whitespacePreserve != null) {
				return constraints.whitespacePreserve;
			}
		}
		return null;
	}

	/**
	 * @param whitespacePreserve the whitespacePreserve to set
	 */
	public void setWhitespacePreserve(WhitespacePreserve whitespacePreserve) {
		this.whitespacePreserve = whitespacePreserve;
	}

	/**
	 * @return the mandatory
	 */
	public Boolean getMandatory() {
		return mandatory;
	}
	
	/**
	 * @return the whitespacePreserve
	 */
	public Boolean getEffectiveMandatory() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.mandatory != null) {
				return constraints.mandatory;
			}
		}
		return null;
	}

	/**
	 * @param mandatory the mandatory to set
	 */
	public void setMandatory(Boolean mandatory) {
		this.mandatory = mandatory;
	}

	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}
	
	/**
	 * @return the defaultValue
	 */
	public String getEffectiveDefaultValue() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.defaultValue != null) {
				return constraints.defaultValue;
			}
		}
		return null;
	}

	/**
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @return the minValueInclusive
	 */
	public Boolean getMinValueInclusive() {
		return minValueInclusive;
	}
	
	/**
	 * @return the minValueInclusive
	 */
	public Boolean getEffectiveMinValueInclusive() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.minValueInclusive != null) {
				return constraints.minValueInclusive;
			}
		}
		return Boolean.TRUE;
	}

	/**
	 * @param minValueInclusive the minValueInclusive to set
	 */
	public void setMinValueInclusive(Boolean minValueInclusive) {
		this.minValueInclusive = minValueInclusive;
	}

	/**
	 * @return the maxValueInclusive
	 */
	public Boolean getMaxValueInclusive() {
		return maxValueInclusive;
	}
	
	/**
	 * @return the maxValueInclusive
	 */
	public Boolean getEffectiveMaxValueInclusive() {
		for (PrimitiveTypeConstraints constraints : getChainOfConstraints()) {
			if (constraints.maxValueInclusive != null) {
				return constraints.maxValueInclusive;
			}
		}
		return Boolean.TRUE;
	}

	/**
	 * @param maxValueInclusive the maxValueInclusive to set
	 */
	public void setMaxValueInclusive(Boolean maxValueInclusive) {
		this.maxValueInclusive = maxValueInclusive;
	}



	/**
	 * @author marcu
	 *
	 */
	public enum WhitespacePreserve {
		/**
		 * Whitespaces remain untouched.
		 */
		PRESERVE,
		/**
		 * Whitespaces are replaced with spaces.
		 */
		REPLACE,
		/**
		 * Whitespaces are simply removed.
		 */
		COLLAPSE,
		;
	}
}
