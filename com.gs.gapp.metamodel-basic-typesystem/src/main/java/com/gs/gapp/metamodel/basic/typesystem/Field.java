/**
 *
 */
package com.gs.gapp.metamodel.basic.typesystem;

import java.math.BigDecimal;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * @author mmt
 *
 */
public class Field extends ModelElement {

	/**
	 *
	 */
	private static final long serialVersionUID = -1710940416786555681L;

	private final ComplexType owner;

	private Type type;

	private CollectionType collectionType;

	private Boolean relevantForEquality = false;

	private Boolean readOnly = false;

	private Boolean initializedWhenDeclared = false;

	private Long cardinality;
	
	private BidirectionalRelationshipType bidirectionalRelationshipType;
	
	private int length;

	private int minLength = 0;

	private boolean nullable = true;
	
    private Integer MinimumValue;
	
	private Integer MaximumValue;
	
	private BigDecimal MinimumDecimalValue;
	
	private BigDecimal MaximumDecimalValue;
	
	private String defaultValue;
	
	private Boolean expandable;
	
	private Type keyType = null;
	
	private int dimension = 1;
	
	private Boolean externalizable;
	
	private Long numberOfBytes;
	
	/**
	 * @param name
	 */
	public Field(String name, ComplexType owner) {
		super(name);
		if (owner == null) throw new NullPointerException("parameter 'owner' must not be null");
		this.owner = owner;
		
		// also adding it vice versa
		owner.addFields(this);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#getQualifiedName()
	 */
	@Override
	public String getQualifiedName() {
		if (getOwner() != null) {
			return new StringBuilder(getOwner().getName()).append(".").append(this.getName()).toString();
		}
		
		return super.getQualifiedName();
	}



	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * @return the collectionType
	 */
	public CollectionType getCollectionType() {
		return collectionType;
	}

	/**
	 * @param collectionType the collectionType to set
	 */
	public void setCollectionType(CollectionType collectionType) {
		this.collectionType = collectionType;
	}

	/**
	 * @return the relevantForEquality
	 * It is deprecated because Business Id mechanism was implemented
	 */
	@Deprecated
	public Boolean getRelevantForEquality() {
		return relevantForEquality;
	}

	/**
	 * @param relevantForEquality the relevantForEquality to set
	 */
	public void setRelevantForEquality(Boolean relevantForEquality) {
		this.relevantForEquality = relevantForEquality;
	}

	/**
	 * @return the readOnly
	 */
	public Boolean getReadOnly() {
		return readOnly;
	}

	/**
	 * @param readOnly the readOnly to set
	 */
	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	/**
	 * @return the initializedWhenDeclared
	 */
	public Boolean getInitializedWhenDeclared() {
		return initializedWhenDeclared;
	}

	/**
	 * @param initializedWhenDeclared the initializedWhenDeclared to set
	 */
	public void setInitializedWhenDeclared(Boolean initializedWhenDeclared) {
		this.initializedWhenDeclared = initializedWhenDeclared;
	}

	/**
	 * @return the cardinality
	 */
	public Long getCardinality() {
		return cardinality;
	}

	/**
	 * @param cardinality the cardinality to set
	 */
	public void setCardinality(Long cardinality) {
		this.cardinality = cardinality;
	}

	/**
	 * @return the owner
	 */
	public ComplexType getOwner() {
		return owner;
	}

	public BidirectionalRelationshipType getBidirectionalRelationshipType() {
		return bidirectionalRelationshipType;
	}

	public void setBidirectionalRelationshipType(
			BidirectionalRelationshipType bidirectionalRelationshipType) {
		this.bidirectionalRelationshipType = bidirectionalRelationshipType;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getMinLength() {
		return minLength;
	}

	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	public boolean isNullable() {
		return nullable;
	}

	public void setNullable(boolean nullable) {
		this.nullable = nullable;
	}

	public BigDecimal getMinimumDecimalValue() {
		return MinimumDecimalValue;
	}

	public void setMinimumDecimalValue(BigDecimal minimumDecimalValue) {
		MinimumDecimalValue = minimumDecimalValue;
	}

	public BigDecimal getMaximumDecimalValue() {
		return MaximumDecimalValue;
	}

	public void setMaximumDecimalValue(BigDecimal maximumDecimalValue) {
		MaximumDecimalValue = maximumDecimalValue;
	}

	public Integer getMinimumValue() {
		return MinimumValue;
	}

	public void setMinimumValue(Integer minimumValue) {
		MinimumValue = minimumValue;
	}

	public Integer getMaximumValue() {
		return MaximumValue;
	}

	public void setMaximumValue(Integer maximumValue) {
		MaximumValue = maximumValue;
	}

	/**
	 * @return the defaultValue
	 */
	public String getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @param defaultValue the defaultValue to set
	 */
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	/**
	 * @return the expandable
	 */
	public Boolean getExpandable() {
		return expandable;
	}

	/**
	 * @param expandable the expandable to set
	 */
	public void setExpandable(Boolean expandable) {
		this.expandable = expandable;
	}

	/**
	 * @return the type of the key if the collection type is keyvalue
	 */
	public Type getKeyType() {
		return keyType;
	}

	/**
	 * @param keyType the type of the key if the collection type is keyvalue
	 */
	public void setKeyType(Type keyType) {
		this.keyType = keyType;
	}
	
	/**
	 * @return the dimension of the collection type if set
	 */
	public int getDimension() {
		return dimension;
	}
	
	/**
	 * @param dimension of the collection type if set
	 */
	public void setDimension(int dimension) {
		this.dimension = dimension;
	}

	/**
	 * @return the externalizable
	 */
	public Boolean getExternalizable() {
		if (externalizable != null) {
			return externalizable;
		} else if (getType() instanceof ComplexType) {
			return ((ComplexType)getType()).getExternalizable();
		}
		
		return null;
	}

	/**
	 * @param externalizable the externalizable to set
	 */
	public void setExternalizable(Boolean externalizable) {
		this.externalizable = externalizable;
	}

	/**
	 * @return the numberOfBytes
	 */
	public Long getNumberOfBytes() {
		return numberOfBytes;
	}

	/**
	 * @param numberOfBytes the numberOfBytes to set
	 */
	public void setNumberOfBytes(Long numberOfBytes) {
		this.numberOfBytes = numberOfBytes;
	}
}
