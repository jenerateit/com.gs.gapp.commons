package com.gs.gapp.metamodel.basic.typesystem;

import java.util.Collection;
import java.util.LinkedHashSet;

import com.gs.gapp.metamodel.basic.AbstractModelFilter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementWrapper;
import com.gs.gapp.metamodel.basic.ModelFilterI;

/**
 * The "modeled types filter" removes all model elements that are types of the basic typesystem metamodel
 * in case those elements have an originating element that is of type {@link ModelElementWrapper}.
 * This means, only types that are explicitely modeled are going to be passed on.
 * 
 * @author marcu
 *
 */
public class ModeledTypesFilter extends AbstractModelFilter implements ModelFilterI {

	public ModeledTypesFilter() {}

	@Override
	public Collection<?> filter(Collection<?> modelElements) {
		Collection<Object> result = new LinkedHashSet<>();
		Collection<?> mandatoryElements = AbstractModelFilter.extractMandatoryElements(modelElements);
		for (Object element : modelElements) {
			if (element instanceof ModelElementI) {
				ModelElementI modelElement = (ModelElementI) element;
				
				if (BasicTypesystemMetamodel.INSTANCE.isIncluded(modelElement.getClass()) &&
						BasicTypesystemMetamodel.INSTANCE.getMetatypesForConversionCheck().contains(modelElement.getClass())) {
					addBasicTypesystemModelElement(result, modelElement);
				} else {
					result.add(element);	
				}
				
			} else {
				result.add(element);
			}
		}
		result.addAll(mandatoryElements);
		return result;
	}

	/**
	 * @param result
	 * @param modelElement
	 * @return
	 */
	private boolean addBasicTypesystemModelElement(Collection<Object> result, ModelElementI modelElement) {
		boolean added = false;
		ComplexType complexType = null;
		ExceptionType exceptionType = null;
		Enumeration enumeration = null;
		
		if (modelElement instanceof ExceptionType) {
			exceptionType = (ExceptionType ) modelElement;
			if (!(exceptionType.getOriginatingElement() instanceof ModelElementWrapper)) {
				result.add(modelElement);
				added = true;
			}
		} else if (modelElement instanceof Enumeration) {
			enumeration = (Enumeration) modelElement;
			if (!(enumeration.getOriginatingElement() instanceof ModelElementWrapper)) {
				result.add(modelElement);
				added = true;
			}
		} else if (modelElement instanceof ComplexType) {
			complexType = (ComplexType) modelElement;
			if (!(complexType.getOriginatingElement() instanceof ModelElementWrapper)) {
				result.add(modelElement);
				added = true;
			}
		}
		
		return added;
	}
}
