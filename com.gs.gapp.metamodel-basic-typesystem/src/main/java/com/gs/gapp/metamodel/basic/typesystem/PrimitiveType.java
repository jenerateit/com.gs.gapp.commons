/**
 *
 */
package com.gs.gapp.metamodel.basic.typesystem;


/**
 * PrimitiveType instances are the basic building blocks for types.
 * A primitive type doesn't have any fields.
 */
public class PrimitiveType extends Type {


	private Boolean numeric;
	
	private PrimitiveTypeConstraints constraints;

	/**
	 *
	 */
	private static final long serialVersionUID = 3910100625847761460L;

	/**
	 * @param name
	 */
	public PrimitiveType(String name) {
		super(name);
	}

	/**
	 * @return the numeric
	 */
	public Boolean getNumeric() {
		return numeric;
	}

	/**
	 * @param numeric the numeric to set
	 */
	public void setNumeric(Boolean numeric) {
		this.numeric = numeric;
	}

	/**
	 * @return the constraints
	 */
	public PrimitiveTypeConstraints getConstraints() {
		return constraints;
	}

	/**
	 * @param constraints the constraints to set
	 */
	public void setConstraints(PrimitiveTypeConstraints constraints) {
		if (isBuiltInPrimitiveType() && constraints != null) {
			throw new IllegalArgumentException("Detected attempt to set type constraints for the built-in primitive type '" + getName() + "'. This is not allowed.");
		}
		this.constraints = constraints;
	}
	
	/**
	 * @return
	 */
	public boolean isBuiltInPrimitiveType() {
		return this.getOriginatingElement() instanceof PrimitiveTypeEnum;
	}
}
