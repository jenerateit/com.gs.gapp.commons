package com.gs.gapp.metamodel.basic.typesystem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.gs.gapp.metamodel.basic.ModelCompleterI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;

public class TypesystemCompleter implements ModelCompleterI {

	@Override
	public Collection<Message> complete(Collection<Object> modelElements) {
		final List<Message> messages = new ArrayList<>();
		
		modelElements.stream().forEach(modelElement -> {
			if (modelElement instanceof Field) {
				completeFields((Field) modelElement, messages);
			}
		});
		
		return messages;
	}
	
	/**
	 * @param field
	 * @param messages
	 */
	private void completeFields(Field field, List<Message> messages) {
		if (field.getCollectionType() == CollectionType.KEYVALUE) {
			if (field.getKeyType() == null) {
				field.setKeyType(PrimitiveTypeEnum.STRING.getPrimitiveType());
					Message warningMessage =
						TypesystemMessage.FIELD_INCOMPLETE_KEY_VALUE_COLLECTION_TYPE.getMessageBuilder()
							.modelElement(field)
							.parameters(field.getQualifiedName())
							.build();
					messages.add(warningMessage);
			}
		}
	}
}
