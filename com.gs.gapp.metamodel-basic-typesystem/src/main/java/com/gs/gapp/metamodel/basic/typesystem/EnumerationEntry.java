/**
 * 
 */
package com.gs.gapp.metamodel.basic.typesystem;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * @author mmt
 *
 */
public class EnumerationEntry extends ModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 607979484637806942L;

	/**
	 * @param name
	 */
	public EnumerationEntry(String name) {
		super(name);
	}
}
