package com.gs.gapp.converter.persistence.basic;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

public class PersistenceModuleToBasicModuleConverter<S extends PersistenceModule, T extends Module> extends
    AbstractPersistenceToBasicElementConverter<S, T> {

	public PersistenceModuleToBasicModuleConverter(
			AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		for (Entity entity : originalModelElement.getEntities()) {
			@SuppressWarnings("unused")
			ComplexType complexType =
					this.convertWithOtherConverter(ComplexType.class, entity);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new Module(originalModelElement.getName());
		result.setOriginatingElement(originalModelElement);
		return result;
	}

}
