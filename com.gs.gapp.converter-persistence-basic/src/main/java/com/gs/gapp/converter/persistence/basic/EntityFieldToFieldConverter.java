package com.gs.gapp.converter.persistence.basic;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;

public class EntityFieldToFieldConverter<S extends EntityField, T extends Field> extends
    AbstractPersistenceToBasicElementConverter<S, T> {

	public EntityFieldToFieldConverter(AbstractConverter modelConverter,
			boolean previousResultingElementRequired, boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly, createAndConvertInOneGo);
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S entityField, T field) {
		super.onConvert(entityField, field);

		// --- type
		if (entityField.getType() instanceof PrimitiveType) {
			field.setType(entityField.getType());
		} else if (entityField.getType() instanceof Enumeration) {
			field.setType(entityField.getType());
		} else if (entityField.getType() instanceof Entity) {
			ComplexType type = null;
			Entity entity = (Entity) entityField.getType();
			
			if (entityField.getOwner().getName().equalsIgnoreCase("AccessRightFilter")) {
				System.out.println();
			}
			
			Boolean owningTypePartOfBusinessLogicOnly = isEntityPartOfBusinessLogicOnly(entityField.getOwner());
			Boolean fieldTypePartOfStorage = isEntityPartOfStorage(entity);
			
			if (owningTypePartOfBusinessLogicOnly != null && fieldTypePartOfStorage != null &&
					owningTypePartOfBusinessLogicOnly && fieldTypePartOfStorage) {
				type = entity;
			} else {
				type = this.convertWithOtherConverter(ComplexType.class, entity);
			}
			
			if (type != null) {
				field.setType(type);
			} else {
                throw new ModelConverterException("was not able to convert entity '" + entityField.getType() + "', conversion result was null", entityField);
			}
		} else {
			field.setType(entityField.getType());
		}

		// --- options
		field.setCollectionType(entityField.getCollectionType());
		field.setDimension(entityField.getDimension());
		field.setKeyType(entityField.getKeyType());
		field.setReadOnly(entityField.getReadOnly());
		field.setNullable(entityField.isNullable());
		field.setExpandable(entityField.getExpandable());
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement,
			ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement,
				previousResultingModelElement);

		if (previousResultingModelElement == null) {
			result = false;
		} else if (result) {
			if (!(previousResultingModelElement instanceof ComplexType)) {
				result = false;
			}
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S entityField, ModelElementI previousResultingModelElement) {
		ComplexType owner = null;
		if (previousResultingModelElement != null && previousResultingModelElement instanceof ComplexType) {
			owner = (ComplexType) previousResultingModelElement;
		} else {
			throw new ModelConverterException("parameter 'previousResultingModelElement' either null or not instanceof ComplexType");
		}
		
		if (entityField.getType() == null) {
			throw new ModelConverterException("entity field '" + entityField + "' (" + entityField.getOwner() + ") does not have a type set"); 
		}

		@SuppressWarnings("unchecked")
		T result = (T) new Field(entityField.getName(), owner);
		result.setOriginatingElement(entityField);
		return result;
	}
}
