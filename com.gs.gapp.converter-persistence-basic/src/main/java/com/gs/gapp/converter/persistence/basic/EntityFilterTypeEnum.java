package com.gs.gapp.converter.persistence.basic;

import java.util.HashMap;

public enum EntityFilterTypeEnum {

	ALL ("All"),
	NONE ("None"),
	SELECTED ("Selected"),
	;

	private static HashMap<String, EntityFilterTypeEnum> entryMap =
			new HashMap<>();

	static {
		for (EntityFilterTypeEnum enumEntry : values()) {
            entryMap.put(enumEntry.getName().toLowerCase(), enumEntry);
		}
	}

	private final String name;

	private EntityFilterTypeEnum(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * @return
	 */
	public static EntityFilterTypeEnum forName(String name) throws IllegalArgumentException {
		if (name != null) {
            return entryMap.get(name.toLowerCase());
		}
		throw new IllegalArgumentException("no EntityFilterTypeEnum enum entry found for '" + name + "'");
	}
}
