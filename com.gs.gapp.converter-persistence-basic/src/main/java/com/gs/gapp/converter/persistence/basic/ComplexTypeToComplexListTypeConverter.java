package com.gs.gapp.converter.persistence.basic;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

public class ComplexTypeToComplexListTypeConverter<S extends ComplexType, T extends ComplexType> extends
    AbstractPersistenceToBasicElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public ComplexTypeToComplexListTypeConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		// --- parent
		// there is no parent here

		// --- namespace
		originalModelElement.getModule().getNamespace().addElement(resultingModelElement);

		// --- fields
		Field field = new Field(StringTools.firstLowerCase(originalModelElement.getName()+ "List"), resultingModelElement);
		field.setOriginatingElement(originalModelElement);
	    field.setType(originalModelElement);
	    field.setCollectionType(CollectionType.LIST);

	    originalModelElement.getModule().addElement(resultingModelElement);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new ComplexType(originalModelElement.getName() + "List");
		return result;
	}
}
