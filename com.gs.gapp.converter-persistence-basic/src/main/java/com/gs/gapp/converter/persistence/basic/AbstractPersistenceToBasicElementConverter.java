/**
 *
 */
package com.gs.gapp.converter.persistence.basic;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.dsl.persistence.PersistenceOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.ServiceImplementation;
import com.gs.gapp.metamodel.function.ServiceInterface;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.product.Capability;
import com.gs.gapp.metamodel.product.ServiceApplication;

/**
 * This abstract model element converter is meant to be used when you want to convert
 * from an object that is of type ModelElement to another object that
 * also is of type ModelElement.
 *
 * @author mmt
 *
 */
public abstract class AbstractPersistenceToBasicElementConverter<S extends ModelElementI, T extends ModelElementI> extends
		AbstractM2MModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public AbstractPersistenceToBasicElementConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/**
	 * @param modelConverter
	 * @param modelElementConverterBehavior
	 */
	public AbstractPersistenceToBasicElementConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	/**
	 * @param modelConverter
	 * @param previousResultingElementRequired
	 */
	public AbstractPersistenceToBasicElementConverter(AbstractConverter modelConverter,
			boolean previousResultingElementRequired, boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly, createAndConvertInOneGo);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#getModelConverter()
	 */
	@Override
	protected PersistenceToBasicConverter getModelConverter() {
		return (PersistenceToBasicConverter) super.getModelConverter();
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#getConverterOptions()
	 */
	@Override
	protected PersistenceToBasicConverterOptions getConverterOptions() {
		return getModelConverter().getConverterOptions();
	}
	
	/**
	 * @param entity
	 * @return
	 */
	protected boolean isEntityExternalized(Entity entity) {
		boolean result = false;
		switch (getConverterOptions().getEntityFilterType()) {
		case ALL:
            result = true;
			break;
		case NONE:
			result = false;
			break;
		case SELECTED:
			OptionDefinition<Boolean>.OptionValue optionValue =
			    PersistenceOptionEnum.OPTION_DEFINITION_EXTERNALIZE.extractOptionValue(entity);
			if (optionValue != null && optionValue.getOptionValue().equals(true)) {
				result = true;
			} else {
				Capability capabilityContext = getModelConverter().getCapabilityContext();
				// --- Check whether the capability has some information about whether the entity should be converted to
				// --- other model elements in order to support de-/serialization of the entity data.
				if (capabilityContext != null) {
					for (ServiceApplication serviceApplication : capabilityContext.getServiceApplications()) {
						for (ServiceImplementation serviceImplementation : serviceApplication.getServices()) {
							ServiceInterface serviceInterface = serviceImplementation.getServiceInterface();
							if (serviceInterface.contains(entity)) {
								result = true;
								break;
							}
						}
						if (result == true) break;
					}
				} else {
					// TODO Here we could add logic to further decide, whether an entity is going to be externalized, e.g. by checking whether a ServiceApplication is available in the model. (mmt 16-Sep-2019)
				}
			}
			break;
		default:
			throw new ModelConverterException("unhandled entity filter type '" + getConverterOptions().getEntityFilterType() + "' found");
		}
		
		return result;
	}
	
	/**
	 * @param entity
	 * @return null when it cannot be found out, whether the given entity is part of the service implementation
	 */
	protected Boolean isEntityPartOfServiceImpl(Entity entity) {
		Capability capability = this.getModelConverter().getCapabilityContext();
		if (capability != null) {
			return capability.isEntityPartOfServiceImpl(entity);
		}
		
		return null;  // means that it's unknown
	}
	
	/**
	 * @param entity
	 * @return null when it cannot be found out, whether the given entity is part of the business logic
	 */
	protected Boolean isEntityPartOfBusinessLogic(Entity entity) {
		Capability capability = this.getModelConverter().getCapabilityContext();
		if (capability != null) {
			return capability.isEntityPartOfBusinessLogic(entity);
		}
		
		return null;  // means that it's unknown
	}
	
	/**
	 * @param entity
	 * @return null when it cannot be found out, whether the given entity is part of the storage
	 */
	protected Boolean isEntityPartOfStorage(Entity entity) {
		Capability capability = this.getModelConverter().getCapabilityContext();
		if (capability != null) {
			return capability.isEntityPartOfStorage(entity);
		}
		
		return null;  // means that it's unknown
	}
	
	/**
	 * @param entity
	 * @return
	 */
	protected Boolean isEntityPartOfBusinessLogicOnly(Entity entity) {
		Capability capability = this.getModelConverter().getCapabilityContext();
		if (capability != null) {
			return capability.isEntityPartOfBusinessLogicOnly(entity);
		}
		
		return null;
	}
	
	/**
	 * @param entity
	 * @return
	 */
	protected Boolean isEntityPartOfStorageAndBusinessLogicOnly(Entity entity) {
		Capability capability = this.getModelConverter().getCapabilityContext();
		if (capability != null) {
			return capability.isEntityPartOfStorageAndBusinessLogicOnly(entity);
		}
		
		return null;
	}
}
