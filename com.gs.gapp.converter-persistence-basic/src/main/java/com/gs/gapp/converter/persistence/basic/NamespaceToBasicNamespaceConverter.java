package com.gs.gapp.converter.persistence.basic;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Namespace;
import com.gs.gapp.metamodel.converter.AbstractConverter;

public class NamespaceToBasicNamespaceConverter<S extends com.gs.gapp.metamodel.persistence.Namespace, T extends Namespace> extends
    AbstractPersistenceToBasicElementConverter<S, T> {

	public NamespaceToBasicNamespaceConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		
		String postfix = getConverterOptions().getNamespacePostfix() == null ? "" : getConverterOptions().getNamespacePostfix();
		
		@SuppressWarnings("unchecked")
		T result = (T) new Namespace(originalModelElement.getName() + postfix);
		result.setOriginatingElement(originalModelElement);
		return result;
	}
}
