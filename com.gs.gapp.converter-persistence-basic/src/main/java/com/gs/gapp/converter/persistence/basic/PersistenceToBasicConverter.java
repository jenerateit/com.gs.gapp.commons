package com.gs.gapp.converter.persistence.basic;

import java.util.List;
import java.util.Set;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.product.Capability;

/**
 * @author mmt
 *
 */
public class PersistenceToBasicConverter extends AbstractAnalyticsConverter {

	private PersistenceToBasicConverterOptions converterOptions;
	/**
	 *
	 */
	public PersistenceToBasicConverter() {
		super(new ModelElementCache());
	}
	
	/**
	 * @param modelElementCache
	 */
	public PersistenceToBasicConverter(ModelElementCache modelElementCache) {
		super(modelElementCache);
	}

	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.BasicConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result =
				super.onGetAllModelElementConverters();

//		switch (getConverterOptions().getFunctionParamType()) {
//		case COMPLEX_TYPE:
//			result.add(new PersistenceModuleToBasicModuleConverter<PersistenceModule, Module>(this));
//			result.add(new NamespaceToNamespaceConverter<com.gs.gapp.metamodel.persistence.Namespace, com.gs.gapp.metamodel.basic.Namespace>(this));
//			// --- master element converters
//			// If we want to have complex types as function parameter types, we have to provide the converters for that
//			result.add(new EntityToComplexTypeConverter<Entity, ComplexType>(this, false, false, true));
//			
//			// --- slave element converters
//			result.add(new ComplexTypeToComplexListTypeConverter<ComplexType, ComplexType>(this, false, true, true));
//			result.add(new EntityFieldToFieldConverter<EntityField, Field>(this, true, true, true));
//			result.add(new EntityRelationEndToFieldConverter<EntityRelationEnd, Field>(this, true, true, true));
//			break;
//		case ENTITY:
//			// do nothing here, we already have entity type available
//			break;
//		default:
//			throw new ModelConverterException("unhandled function param type '" + getConverterOptions().getFunctionParamType() + "'");
//        }
		
		
		result.add(new PersistenceModuleToBasicModuleConverter<>(this));
		result.add(new NamespaceToBasicNamespaceConverter<>(this));

		// --- master element converters
		result.add(new EntityToComplexTypeConverter<>(this, false, false, true));
		
		// --- slave element converters
		if (getConverterOptions().isListBeanGenerationSupported()) {
		    result.add(new ComplexTypeToComplexListTypeConverter<>(this));
		}
		result.add(new EntityFieldToFieldConverter<>(this, true, true, true));
		result.add(new EntityRelationEndToFieldConverter<>(this, true, true, true));


		return result;
	}

    /* (non-Javadoc)
     * @see com.gs.gapp.converter.analytics.AbstractAnalyticsConverter#getConverterOptions()
     */
    @Override
	public PersistenceToBasicConverterOptions getConverterOptions() {
    	if (this.converterOptions == null) {
    		this.converterOptions = new PersistenceToBasicConverterOptions(getOptions());
    	}
		return converterOptions;
	}
    
    /* (non-Javadoc)
	 * @see com.gs.gapp.converter.analytics.AbstractAnalyticsConverter#onInitOptions()
	 */
	@Override
	protected void onInitOptions() {
		super.onInitOptions();
		this.converterOptions = new PersistenceToBasicConverterOptions(getOptions());
	}
    
    /**
     * @return
     */
    public Capability getCapabilityContext() {
    	return getCapabilityContext(getModel());
	}
    
    /**
     * @param model
     * @return
     */
    private Capability getCapabilityContext(Model model) {
		model = model.getOriginatingElement(Model.class);
		if (model == null) {
			return null;
		}
		Set<Capability> capabilities = model.getElements(Capability.class);
		if (capabilities == null || capabilities.size() == 0) {
			// go back one more model to check whether that one contains Capabilities
			return getCapabilityContext(model);
		}
		
		// if we are here, we found at least one capability
		Capability result = null;
		
		if (capabilities.size() == 1) {
			result = capabilities.iterator().next();
		} else if (converterOptions.getNameOfFilteredCapability() != null) {
			for (Capability capability : capabilities) {
				if (converterOptions.getNameOfFilteredCapability().equalsIgnoreCase(capability.getName())) {
					result = capability;
					break;
				}
			}
		}

		return result;
	}
}
