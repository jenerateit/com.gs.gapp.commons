package com.gs.gapp.converter.persistence.basic;

import java.io.Serializable;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.metamodel.analytics.AbstractAnalyticsConverterOptions;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;

public class PersistenceToBasicConverterOptions extends AbstractAnalyticsConverterOptions {
	
	public static final String OPTION_NAMESPACE_POSTFIX = "namespace-postfix";
	public static final String OPTION_LIST_BEANS = "list-beans";
	public static final String PARAMETER_ENTITY_FILTER = "entity-filter";
	
	public static final OptionDefinitionString OPTION_DEF_FILTER_KEEP_CAPABILITY =
			new OptionDefinitionString("filter.keep-capability", "set the name of the capability for which generation results should be kept", false, false);
	
	public PersistenceToBasicConverterOptions(ModelConverterOptions options) {
		super(options);
	}
	
	/**
	 * @return
	 */
	public String getNamespacePostfix() {
		Serializable postfix = getOptions().get(OPTION_NAMESPACE_POSTFIX);

		String result = (postfix == null || postfix.toString().length() == 0 ? "basic" : postfix.toString());
		
		if (result != null && result.startsWith(".") == false) result = "." + result;
		return result;
	}
	
	/**
	 * @return
	 */
	public boolean isListBeanGenerationSupported() {
		String listBeanGenerationSupported = (String) getOptions().get(OPTION_LIST_BEANS);
		validateBooleanOption(listBeanGenerationSupported, OPTION_LIST_BEANS);
		return (listBeanGenerationSupported != null) && listBeanGenerationSupported.equalsIgnoreCase("true");
	}

	/**
	 * @return
	 */
	public String getNameOfFilteredCapability() {
		Serializable option = getOptions().get(OPTION_DEF_FILTER_KEEP_CAPABILITY.getKey());
		if (option != null) {
			return option.toString();
		}
		
		return null;
	}
	
	/**
	 * @return
	 */
	public EntityFilterTypeEnum getEntityFilterType() {
		String entityFilterTypeString = (String) getOptions().get(PARAMETER_ENTITY_FILTER);
		EntityFilterTypeEnum result = null;
		if (entityFilterTypeString != null) {
			try {
		        result = EntityFilterTypeEnum.forName(entityFilterTypeString);
			} catch (IllegalArgumentException ex) {
				throwIllegalEnumEntryException((String) entityFilterTypeString, PARAMETER_ENTITY_FILTER, EntityFilterTypeEnum.values());
			}
		}

		return result == null ? EntityFilterTypeEnum.ALL : result;
	}

}
