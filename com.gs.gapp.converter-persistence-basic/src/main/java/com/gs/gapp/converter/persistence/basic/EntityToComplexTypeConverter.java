package com.gs.gapp.converter.persistence.basic;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.persistence.EntityRelationEnd;

public class EntityToComplexTypeConverter<S extends Entity, T extends ComplexType> extends
    AbstractPersistenceToBasicElementConverter<S, T> {

	public EntityToComplexTypeConverter(AbstractConverter modelConverter,
			boolean previousResultingElementRequired,
			boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly,
				createAndConvertInOneGo);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S entity, T complexType) {
		super.onConvert(entity, complexType);
		
		if (entity.getOwner() != null) {
			// AJAX-186
			ComplexType outerComplexType = convertWithOtherConverter(ComplexType.class, entity.getOwner());
			complexType.setOwner(outerComplexType);
		}

		// --- convert module, to make sure the module is there at this point in time
		Module basicModule = this.convertWithOtherConverter(Module.class, entity.getModule());

		// --- parent
		if (entity.getParent() != null) {
			ComplexType parentType = this.convertWithOtherConverter(ComplexType.class, entity.getParent());
			if (parentType != null) {
				complexType.setParent(parentType);
			}
		}


		
		// --- namespace
		com.gs.gapp.metamodel.basic.Namespace namespace =
				this.convertWithOtherConverter(com.gs.gapp.metamodel.basic.Namespace.class, entity.getModule().getNamespace(),
						new Class<?>[] {NamespaceToBasicNamespaceConverter.class} );
		namespace.addElement(complexType);
		basicModule.setNamespace(namespace);

		// --- fields
		for (EntityField entityField : entity.getEntityFields()) {
			if (entityField.getExternalizable() != null && entityField.getExternalizable() == Boolean.FALSE) {
				continue;
			}
			if (entityField.getType() instanceof ComplexType) {
				ComplexType complexTypeForExternalizableCheck = (ComplexType) entityField.getType();
				if (complexTypeForExternalizableCheck.getExternalizable() != null && complexTypeForExternalizableCheck.getExternalizable().equals(Boolean.FALSE)) {
					continue;
				}
			}
			
			
			if (entityField instanceof EntityRelationEnd) {
				// relations have to be handled differently
				EntityRelationEnd entityRelationEnd = (EntityRelationEnd) entityField;
				if (entityRelationEnd.getCollectionType() == null) {
				    // it is a to-one relation => include it
					@SuppressWarnings("unused")
					Field field = convertWithOtherConverter(Field.class, entityField, complexType);
				} else {
					// it is a to-many relationship => add a list to hold the related beans
					Field field = new Field(entityRelationEnd.getName(), complexType);
					field.setOriginatingElement(entityRelationEnd);

					Entity entityTypeOfRelation = null;
					if (entityRelationEnd.getType() instanceof Entity) {
						entityTypeOfRelation = (Entity) entityRelationEnd.getType();
						ComplexType fieldComplexType = null;
						
						Boolean owningTypePartOfBusinessLogicOnly = isEntityPartOfBusinessLogicOnly(entity);
						Boolean fieldTypePartOfStorage = isEntityPartOfStorage(entityTypeOfRelation);
						
						if (owningTypePartOfBusinessLogicOnly != null && fieldTypePartOfStorage != null &&
								owningTypePartOfBusinessLogicOnly && fieldTypePartOfStorage) {
							fieldComplexType = entityTypeOfRelation;
						} else {
							fieldComplexType = this.convertWithOtherConverter(ComplexType.class, entityTypeOfRelation);
						}
						
						field.setType(fieldComplexType);
						// ids would not be sufficient for update of owned collections (mmt 25-Jun-2013)
//						EntityField idField = entityTypeOfRelation.getAllIdFields().iterator().next();
//						field.setType(idField.getType());
					} else {
						throw new ModelConverterException("entity relation does not have an entity type but '" + entityRelationEnd.getType() + "'");
					}

					field.setCollectionType(entityRelationEnd.getCollectionType());  // use the collection type that was originally modelled for the entity relation

					// TODO the following would not be a good way of solving the issue - do not add additional, artificial fields (on the android side this is required for data classes for DAOs, though)
					// --- add additional field in case the relation is not bidirectional
//					if (entityRelationEnd.getOtherRelationEnd() == null) {
//						Field oppositeField = new Field(StringTools.firstLowerCase(originalModelElement.getName()));
//						oppositeField.setOriginatingElement(entityRelationEnd);
//						oppositeField.setType(originalModelElement.getAllIdFields().iterator().next().getType());
//						ComplexType otherEndComplexType = convertWithOtherConverter(ComplexType.class, entityTypeOfRelation);
//						otherEndComplexType.addFields(oppositeField);
//					}
				}
			} else {
				@SuppressWarnings("unused")
				Field field = convertWithOtherConverter(Field.class, entityField, complexType);
			}
		}

		basicModule.addElement(complexType);

		// --- convert a list type for the given complex type
		this.convertWithOtherConverter(ComplexType.class, complexType);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S entity, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new ComplexType(entity.getName());	
		if(entity.isAbstractType()) {
			result.setAbstractType(true);
		}
		return result;
	}
}
