package com.gs.gapp.converter.basic;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;

public class BasicNormalizationConverterProvider implements ModelConverterProviderI {

	@Override
	public ModelConverterI getModelConverter() {
		return new BasicNormalizationConverter();
	}
}
