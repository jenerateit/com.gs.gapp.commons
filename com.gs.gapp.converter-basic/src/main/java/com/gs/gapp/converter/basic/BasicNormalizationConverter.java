package com.gs.gapp.converter.basic;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.dsl.basic.BasicOptionEnum;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

public class BasicNormalizationConverter extends AbstractAnalyticsConverter /*AbstractConverter*/ {


	public BasicNormalizationConverter() {
		super(new ModelElementCache());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelConsolidation(java.util.Set)
	 */
	@Override
	protected Set<Object> onPerformModelConsolidation(
			Set<?> normalizedElements) {
		Set<Object> result = super.onPerformModelConsolidation(normalizedElements);

		// Here we add all incoming elements to the result set, since this converter is not supposed
		// to create a totally new set of model elements but to add a few missing elements only.
		for (Object normalizedElement : normalizedElements) {
//			if (normalizedElement instanceof ModelElementI && !(normalizedElement instanceof Model) && !(normalizedElement instanceof TransformationStepConfiguration)) {
			if (normalizedElement instanceof ModelElementI && !(normalizedElement instanceof Model)) {
				ModelElementI normalizedModelElement = (ModelElementI) normalizedElement;
				result.add(normalizedModelElement);
				
				if (!(normalizedElement instanceof TransformationStepConfiguration)) {
				    getModel().addElement(normalizedModelElement);
				}
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#normalize(java.util.Collection)
	 */
	@Override
	protected Set<Object> onPerformModelNormalization(Collection<Object> rawElements) {
		Set<Object> result = new LinkedHashSet<>( super.onPerformModelNormalization(rawElements) );

		for (Object object : result) {
			if (object instanceof ComplexType) {
				ComplexType complexType = (ComplexType) object;
				for (Field field : complexType.getFields()) {
					OptionDefinition<Long>.OptionValue cardinalityOption = field.getOption(BasicOptionEnum.CARDINALITY.getName(), Long.class);
					if (cardinalityOption != null) {
						// if the cardinality is set but there is no collection type set, we assume that the field has an array type
						field.setCollectionType(CollectionType.ARRAY);
					}
				}
			}
		}

		return result;
	}
}
