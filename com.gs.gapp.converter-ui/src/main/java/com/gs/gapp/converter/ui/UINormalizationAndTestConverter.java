/**
 *
 */
package com.gs.gapp.converter.ui;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.converter.ui.test.AbstractTestToTestConverter;
import com.gs.gapp.converter.ui.test.TestCompletionConverter;
import com.gs.gapp.converter.ui.test.UIComponentToActionConverter;
import com.gs.gapp.converter.ui.test.UICustomContainerToStepConverter;
import com.gs.gapp.converter.ui.test.UIDataContainerToStepConverter;
import com.gs.gapp.converter.ui.test.UIGridContainerToStepConverter;
import com.gs.gapp.converter.ui.test.UIListContainerToActionConverter;
import com.gs.gapp.converter.ui.test.UIListContainerToStepConverter;
import com.gs.gapp.converter.ui.test.UIStructuralContainerToAbstractTestConverter;
import com.gs.gapp.converter.ui.test.UIStructuralContainerToAbstractTestConverter.UIViewContainerToAbstractTestConverter;
import com.gs.gapp.converter.ui.test.UiModuleToTestModuleConverter;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
public class UINormalizationAndTestConverter extends AbstractAnalyticsConverter {



	/**
	 * 
	 */
	public UINormalizationAndTestConverter() {
		super(new ModelElementCache());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformElementPredefinition()
	 */
	@Override
	protected Set<ModelElementI> onPerformElementPredefinition(Collection<?> rawElements) {
		Set<ModelElementI> predefinedElements = super.onPerformElementPredefinition(rawElements);
		return predefinedElements;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		// --- data containers
		result.add(new UIDataContainerToUIStructuralContainerConverter<>(this));
		
		// --- test stuff
		result.add(new UiModuleToTestModuleConverter<>(this));
		result.add(new AbstractTestToTestConverter<>(this));
		result.add(new UIStructuralContainerToAbstractTestConverter<>(this));
		result.add(new UIViewContainerToAbstractTestConverter<>(this));
		result.add(new TestCompletionConverter<>(this));
		result.add(new UIDataContainerToStepConverter<>(this));
		result.add(new UIListContainerToStepConverter<>(this));
		result.add(new UIGridContainerToStepConverter<>(this));
		result.add(new UICustomContainerToStepConverter<>(this));
		result.add(new UIComponentToActionConverter<>(this));
		result.add(new UIListContainerToActionConverter<>(this));
		
		return result;
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelConsolidation(java.util.Set)
	 */
	@Override
	protected Set<Object> onPerformModelConsolidation(
			Set<?> normalizedElements) {
		Set<Object> result = super.onPerformModelConsolidation(normalizedElements);

		// Here we add all incoming elements to the result set, since this converter is not supposed
		// to create a totally new set of model elements but to add a few missing elements only.
		for (Object normalizedElement : normalizedElements) {
			if (normalizedElement instanceof Model ||
					normalizedElement instanceof TransformationStepConfiguration ||
					normalizedElement instanceof ElementConverterConfigurationTreeNode) {
					continue;
			}
			
			
			if (normalizedElement instanceof ModelElementI && !(normalizedElement instanceof Model)) {
				ModelElementI normalizedModelElement = (ModelElementI) normalizedElement;
				result.add(normalizedModelElement);
				getModel().addElement(normalizedModelElement);
			}
		}
		
//		for (Object o : result) {
//			if (o instanceof Test) {
//				Test test = (Test) o;
//				System.out.println("TEST:" + test.getName());
//			}
//		}

		return result;
	}


}
