/**
 *
 */
package com.gs.gapp.converter.ui.test;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.test.Step;
import com.gs.gapp.metamodel.ui.container.data.UIListContainer;

/**
 * @author mmt
 *
 */
public class UIListContainerToStepConverter<S extends UIListContainer, T extends Step>
    extends UIDataContainerToStepConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIListContainerToStepConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S listContainer, T step) {
		super.onConvert(listContainer, step);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S listContainer, ModelElementI previouslyResultingElement) {
		return super.onCreateModelElement(listContainer, previouslyResultingElement);
	}
}
