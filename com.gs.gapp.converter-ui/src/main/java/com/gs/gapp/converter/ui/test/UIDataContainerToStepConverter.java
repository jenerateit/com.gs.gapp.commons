/**
 *
 */
package com.gs.gapp.converter.ui.test;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.test.Step;
import com.gs.gapp.metamodel.test.Step.Action;
import com.gs.gapp.metamodel.test.Test;
import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * @author mmt
 *
 */
public class UIDataContainerToStepConverter<S extends UIDataContainer, T extends Step>
    extends AbstractM2MModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIDataContainerToStepConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY);
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S dataContainer, T step) {
		super.onConvert(dataContainer, step);
		
		Action containerAction = convertWithOtherConverter(Action.class, dataContainer);
		if (containerAction != null) {
			step.addAction(containerAction);
		}
		
		for (UIComponent component : dataContainer.getComponents()) {
			Action action = convertWithOtherConverter(Action.class, component);
			step.addAction(action);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S dataContainer, ModelElementI previouslyResultingElement) {
		
		Test owningTest = null;
		
		if (previouslyResultingElement instanceof Test) {
			owningTest = (Test) previouslyResultingElement;
		} else {
			throw new ModelConverterException("previous resulting model element is not of type 'Test'");
		}
		
		@SuppressWarnings("unchecked")
		T step = (T) new Step(StringTools.firstUpperCase(dataContainer.getName()), owningTest);
		if (owningTest.getOriginatingElement(UIStructuralContainer.class) != null) {
			step.setPageContainer(owningTest.getOriginatingElement(UIStructuralContainer.class));
		} else if (owningTest.getSteps().size() > 0 && owningTest.getSteps().iterator().next().getPageContainer() != null) {
			step.setPageContainer(owningTest.getSteps().iterator().next().getPageContainer());
		} else {
			throw new ModelConverterException("not able to identify a page container for the new step " + step + " for test " + owningTest + " and data container " + dataContainer);
		}
		return step;
	}
}
