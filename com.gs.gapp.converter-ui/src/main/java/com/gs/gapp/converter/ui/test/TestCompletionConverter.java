/**
 *
 */
package com.gs.gapp.converter.ui.test;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.test.Step;
import com.gs.gapp.metamodel.test.Test;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * This converter checks whether a given Test element has enough steps and
 * actions modeled. If it can only find one single step that nominates
 * a page-under-test and doesn't include any actions, it adds more steps and actions.
 * This is a similar logic to what happens in {@link UIStructuralContainerToAbstractTestConverter}.
 * 
 * @author mmt
 *
 */
public class TestCompletionConverter<S extends Test>
    extends AbstractM2MModelElementConverter<S, S> {

	/**
	 * @param modelConverter
	 */
	public TestCompletionConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);

		if (result) {
			Test test = (Test) originalModelElement;
			if (test.isAbstractType() && test.getSteps().size() == 1 &&
					test.getSteps().iterator().next().getActions().size() == 0 &&
					test.getSteps().iterator().next().getPageContainer() == previousResultingModelElement) {
				// This is the only case where we automatically complete the test object.
			} else {
				result = false;
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S testIn, S testOut) {
		super.onConvert(testIn, testOut);
		
		UIStructuralContainer structuralContainer = testIn.getInitialStep().getPageContainer();
		// Add one step for each child data container and an action for each ui component of a data container.
		for (UIDataContainer dataContainer : structuralContainer.getAllChildDataContainers()) {
			@SuppressWarnings("unused")
			Step step = convertWithOtherConverter(Step.class, dataContainer, testIn);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected S onCreateModelElement(S test, ModelElementI previouslyResultingElement) {
		return test;
	}
}
