/**
 *
 */
package com.gs.gapp.converter.ui.test;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.test.Step;
import com.gs.gapp.metamodel.ui.container.data.UICustomContainer;

/**
 * @author mmt
 *
 */
public class UICustomContainerToStepConverter<S extends UICustomContainer, T extends Step>
    extends UIDataContainerToStepConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UICustomContainerToStepConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S customContainer, T step) {
		super.onConvert(customContainer, step);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S customContainer, ModelElementI previouslyResultingElement) {
		return super.onCreateModelElement(customContainer, previouslyResultingElement);
	}
}
