/**
 *
 */
package com.gs.gapp.converter.ui.test;

import com.gs.gapp.metamodel.ActionTypeI;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.test.Step.Action;
import com.gs.gapp.metamodel.ui.ActionType;
import com.gs.gapp.metamodel.ui.component.UIActionComponent;
import com.gs.gapp.metamodel.ui.component.UIBooleanChoice;
import com.gs.gapp.metamodel.ui.component.UIChoice;
import com.gs.gapp.metamodel.ui.component.UIComponent;
import com.gs.gapp.metamodel.ui.component.UIDateSelector;
import com.gs.gapp.metamodel.ui.component.UIRange;
import com.gs.gapp.metamodel.ui.component.UITextArea;
import com.gs.gapp.metamodel.ui.component.UITextField;

/**
 * @author mmt
 *
 */
public class UIComponentToActionConverter<S extends UIComponent, T extends Action>
    extends AbstractM2MModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIComponentToActionConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	

	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S component, T action) {
		super.onConvert(component, action);
		
		
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S component, ModelElementI previouslyResultingElement) {
		
		// --- TODO determine ActionTypI
		ActionTypeI actionType = ActionType.IS_VALUE_EQUAL;  // that's the default value
		
		if (component instanceof UIActionComponent || component instanceof UIBooleanChoice) {
			actionType = ActionType.CLICK;
		} else if (component instanceof UIDateSelector || component instanceof UITextArea || component instanceof UITextField || component instanceof UIRange ) {
			actionType = ActionType.ENTER;
		} else if (component instanceof UIChoice) {
			actionType = ActionType.SELECT;
		}
		
		@SuppressWarnings("unchecked")
		T action = (T) new Action(actionType, component);
		return action;
	}
}
