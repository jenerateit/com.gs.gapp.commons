/**
 *
 */
package com.gs.gapp.converter.ui.test;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.test.Step;
import com.gs.gapp.metamodel.ui.container.data.UIGridContainer;

/**
 * @author mmt
 *
 */
public class UIGridContainerToStepConverter<S extends UIGridContainer, T extends Step>
    extends UIDataContainerToStepConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIGridContainerToStepConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S gridContainer, T step) {
		super.onConvert(gridContainer, step);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S gridContainer, ModelElementI previouslyResultingElement) {
		return super.onCreateModelElement(gridContainer, previouslyResultingElement);
	}
}
