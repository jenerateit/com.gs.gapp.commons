/**
 *
 */
package com.gs.gapp.converter.ui.test;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.test.Test;
import com.gs.gapp.metamodel.test.TestModule;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;

/**
 * @author mmt
 *
 */
public class AbstractTestToTestConverter<S extends Test, T extends Test>
    extends AbstractM2MModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public AbstractTestToTestConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}
	
	

	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);

		if (result) {
			if (previousResultingModelElement != null) {
				result = false;  // this converter must not be used in case a previousResultingModelElement has been provided (see TestCompletionConverter.java)
			} else {
				Test abstractTest = (Test) originalModelElement;
				if (!abstractTest.isAbstractType()) {
					result = false;  // here we create tests only for abstract test types 
				} else {
				
					UIStructuralContainer uiStructuralContainer = abstractTest.getOriginatingElement(UIStructuralContainer.class);
					if (uiStructuralContainer == null) {
					    uiStructuralContainer = abstractTest.getInitialStep().getPageContainer();
					}
					
					if (uiStructuralContainer == null) {
						result = false;
					}
				}
			}
		}

		return result;
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S abstractTest, T test) {
		super.onConvert(abstractTest, test);
		
		UIStructuralContainer uiStructuralContainer = abstractTest.getOriginatingElement(UIStructuralContainer.class);
		if (uiStructuralContainer != null) {
			String comment = "This is an automatically created, executable test that is related to the view '" + uiStructuralContainer.getName() + "'.";
			if (uiStructuralContainer.getBody() != null && uiStructuralContainer.getBody().length() > 0) {
				test.setBody(uiStructuralContainer.getBody());
				test.addToBody(System.lineSeparator());
				test.addToBody(comment);
			} else {
				test.setBody(comment);
			}
		} else {
		    uiStructuralContainer = abstractTest.getInitialStep().getPageContainer();
		    if (uiStructuralContainer != null) {
		    	String comment = "This is a modeled, executable test that is related to the view '" + uiStructuralContainer.getName() + "'.";
		    	if (uiStructuralContainer.getBody() != null && uiStructuralContainer.getBody().length() > 0) {
					test.setBody(uiStructuralContainer.getBody());
					test.addToBody(System.lineSeparator());
					test.addToBody(comment);
				} else {
					test.setBody(comment);
				}
		    }
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S abstractTest, ModelElementI previouslyResultingElement) {
		String testName = null;
		UIStructuralContainer uiStructuralContainer = abstractTest.getOriginatingElement(UIStructuralContainer.class);
		if (uiStructuralContainer == null) {
		    uiStructuralContainer = abstractTest.getInitialStep().getPageContainer();
		    testName = abstractTest.getName() + "Impl";
		} else {
			testName = uiStructuralContainer.getName() + "Test";
		}
		
		@SuppressWarnings("unchecked")
		T test = (T) new Test(StringTools.firstUpperCase(testName));
		test.setParent(abstractTest);
		
		TestModule testModule = (TestModule) abstractTest.getModule();
		test.setModule(testModule);
		testModule.addTest(test);
		
		return test;
	}
}
