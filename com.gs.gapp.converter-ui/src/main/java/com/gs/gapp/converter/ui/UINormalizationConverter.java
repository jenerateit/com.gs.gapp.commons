/**
 *
 */
package com.gs.gapp.converter.ui;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
public class UINormalizationConverter extends AbstractAnalyticsConverter {



	/**
	 * 
	 */
	public UINormalizationConverter() {
		super(new ModelElementCache());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformElementPredefinition()
	 */
	@Override
	protected Set<ModelElementI> onPerformElementPredefinition(Collection<?> rawElements) {
		Set<ModelElementI> predefinedElements = super.onPerformElementPredefinition(rawElements);
		return predefinedElements;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		// --- data containers
		result.add(new UIDataContainerToUIStructuralContainerConverter<>(this));

		return result;
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelConsolidation(java.util.Set)
	 */
	@Override
	protected Set<Object> onPerformModelConsolidation(
			Set<?> normalizedElements) {
		Set<Object> result = super.onPerformModelConsolidation(normalizedElements);

		// Here we add all incoming elements to the result set, since this converter is not supposed
		// to create a totally new set of model elements but to add a few missing elements only.
		for (Object normalizedElement : normalizedElements) {
			if (normalizedElement instanceof ModelElementI && !(normalizedElement instanceof Model)) {
				ModelElementI normalizedModelElement = (ModelElementI) normalizedElement;
				if (normalizedModelElement instanceof TransformationStepConfiguration ||
					normalizedModelElement instanceof ElementConverterConfigurationTreeNode) {
					continue;
				}
				
				result.add(normalizedModelElement);
				getModel().addElement(normalizedModelElement);
			}
		}

		return result;
	}


}
