/**
 *
 */
package com.gs.gapp.converter.ui.test;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.test.Step.Action;
import com.gs.gapp.metamodel.ui.container.UIContainer;

/**
 * @author mmt
 *
 */
public abstract class UIContainerToActionConverter<S extends UIContainer, T extends Action>
    extends AbstractM2MModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIContainerToActionConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}
	

	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
	}
}
