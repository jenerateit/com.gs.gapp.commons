package com.gs.gapp.converter.ui.test;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.test.Namespace;
import com.gs.gapp.metamodel.test.TestModule;
import com.gs.gapp.metamodel.ui.UIModule;

public class UiModuleToTestModuleConverter<S extends UIModule, T extends TestModule> extends AbstractM2MModelElementConverter<S, T> {

	public UiModuleToTestModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T testModule = (T) new TestModule(originalModelElement.getName());
	
		Namespace namespace = new Namespace(originalModelElement.getNamespace().getName());
		testModule.setNamespace(namespace);
		namespace.setModule(testModule);
		
		
		return testModule;
	}
}
