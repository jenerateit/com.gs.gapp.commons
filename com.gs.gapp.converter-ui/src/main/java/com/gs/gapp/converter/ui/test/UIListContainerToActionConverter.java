/**
 *
 */
package com.gs.gapp.converter.ui.test;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.test.Step.Action;
import com.gs.gapp.metamodel.ui.ActionType;
import com.gs.gapp.metamodel.ui.container.data.UIListContainer;

/**
 * @author mmt
 *
 */
public class UIListContainerToActionConverter<S extends UIListContainer, T extends Action>
    extends UIContainerToActionConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIListContainerToActionConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	

	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean responsible = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		if (responsible) {
			UIListContainer listContainer = (UIListContainer) originalModelElement;
			if (listContainer.getSelectionMode() == null) {
				responsible = false;
			} else {
				switch (listContainer.getSelectionMode()) {
				case MULTIPLE:
				case SINGLE:
					break;
				case NONE:
					responsible = false;  // without a selection we do not need a selection event being handled
					break;
				default:
					throwUnhandledEnumEntryException(listContainer.getSelectionMode());
				}
			}
		}
		
		return responsible;
	}

	@Override
	protected void onConvert(S listContainer, T action) {
		super.onConvert(listContainer, action);
	}
	
    @Override
	protected T onCreateModelElement(S listContainer, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new Action(ActionType.SELECT, listContainer);
		return result;
	}
}
