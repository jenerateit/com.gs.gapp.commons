/**
 *
 */
package com.gs.gapp.converter.ui;

import java.util.HashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.ui.ContainerFlow;
import com.gs.gapp.metamodel.ui.UIModule;
import com.gs.gapp.metamodel.ui.UiUtil;
import com.gs.gapp.metamodel.ui.container.UIActionContainer;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * @author mmt
 *
 */
public class UIDataContainerToUIStructuralContainerConverter<S extends UIDataContainer, T extends UIStructuralContainer>
    extends AbstractM2MModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIDataContainerToUIStructuralContainerConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S dataContainer, T structuralContainer) {
		super.onConvert(dataContainer, structuralContainer);
		// nothing else to be done here
	}



	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);
		boolean needStructuralContainerWrapper = false;

		if (result) {
			UIDataContainer dataContainer = (UIDataContainer) originalModelElement;
			if (dataContainer.isTargetOfAtLeastOneFlow()) {
				needStructuralContainerWrapper = true;
			}
		}

		return result && needStructuralContainerWrapper;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S dataContainer, ModelElementI previouslyResultingElement) {

		T wrapper = (T) UiUtil.createWrappingStructuralContainer(dataContainer);

		com.gs.gapp.metamodel.ui.Namespace namespace = ((UIModule)dataContainer.getModule()).getNamespace();
		namespace.addContainer(wrapper);

		if (dataContainer.isTargetOfAtLeastOneFlow()) {
			Set<ContainerFlow> flowsToBeRemoved = new HashSet<>();
			for (ContainerFlow flow : dataContainer.getFlows()) {
				if (flow.getTarget() == dataContainer) {
					flow.setTarget(wrapper);
					flowsToBeRemoved.add(flow);
				}
			}
			for (ContainerFlow flowToBeRemoved : flowsToBeRemoved) {
				dataContainer.removeFlow(flowToBeRemoved);
			}
		} else if (dataContainer.getParentContainers().size() == 0) {
			// here there is nothing special to do ... the pure existence of a structural container as parent of the data container is sufficient
		}

		// --- handle action containers
		for (UIActionContainer actionContainer : dataContainer.getActionContainers()) {
			wrapper.addActionContainer(actionContainer);
		}
		// --- also, clear of the action containers from the original data container
		for (UIActionContainer actionContainer : wrapper.getActionContainers()) {
			dataContainer.getActionContainers().remove(actionContainer);
		}

		wrapper.setModule(dataContainer.getModule());

		return wrapper;
	}
}
