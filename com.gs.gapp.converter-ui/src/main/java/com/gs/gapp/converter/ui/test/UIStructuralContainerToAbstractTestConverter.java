/**
 *
 */
package com.gs.gapp.converter.ui.test;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.test.Step;
import com.gs.gapp.metamodel.test.Test;
import com.gs.gapp.metamodel.test.TestModule;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;
import com.gs.gapp.metamodel.ui.container.UIViewContainer;
import com.gs.gapp.metamodel.ui.container.data.UIDataContainer;

/**
 * @author mmt
 *
 */
public class UIStructuralContainerToAbstractTestConverter<S extends UIStructuralContainer, T extends Test>
    extends AbstractM2MModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public UIStructuralContainerToAbstractTestConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement, false);

		if (result) {
			UIStructuralContainer container = (UIStructuralContainer) originalModelElement;
			if (!container.isMainContainer()) {
				result = false;  // we create tests only for main containers (aka web pages in case of web applications) 
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S structuralContainer, T test) {
		super.onConvert(structuralContainer, test);
		
		test.addToBody("This is an automatically created, abstract test that is related to the view '" + structuralContainer.getName() + "'.");
		
		// --- set the test prelude parameters type in case there is one modeled for the structural container
		if (structuralContainer.getTestPreludeParameters() != null) {
		    test.setPreludeParameters(structuralContainer.getTestPreludeParameters());
		}
		
		// --- making sure that the views that are needed for the test preparation (= the prelude) are set on the test element for further processing
		for (UIStructuralContainer preludeView : structuralContainer.getTestPreludeViews()) {
			test.addPreludeView(preludeView);
		}
		
		Step initialStep = new Step("openPage", test);
		initialStep.setPageContainer(structuralContainer);
		

		// Add one step for each child data container and an action for each ui component of a data container.
		for (UIDataContainer dataContainer : structuralContainer.getAllChildDataContainers()) {
			 @SuppressWarnings("unused")
			Step step = convertWithOtherConverter(Step.class, dataContainer, test);
		}
		
		// --- trigger the indirect creation of a concrete test, inheriting from the abstract test
		@SuppressWarnings("unused")
		Test concreteTest = convertWithOtherConverter(Test.class, test);
		
		// --- APPJSF-359 (mmt 17-Apr-2019)
		// --- trigger the "autocompletion" of a test that has only one step and no actions
		// --- note that here we have to pass a third parameter since the "autocompletion" converter must not be confused with the converter that creates a concrete, non-abstract test
		for (Object incomingModelElement : getModelConverter().getIncomingModelElements()) {
			if (incomingModelElement instanceof Test) {
		        Test incomingTest = (Test) incomingModelElement;
		        if (incomingTest.isAbstractType() && incomingTest.getSteps().size() == 1 && incomingTest.getSteps().iterator().next().getActions().size() == 0) {
				    incomingTest = convertWithOtherConverter(Test.class, incomingTest, structuralContainer);
				    if (incomingTest != null) {
				        // --- trigger the indirect creation of a concrete test, inheriting from the abstract test
					    concreteTest = convertWithOtherConverter(Test.class, incomingTest);
				    }
		        }
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S structuralContainer, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T test = (T) new Test("Abstract" + StringTools.firstUpperCase(structuralContainer.getName()) + "Test");
		
		test.setAbstractType(true);
		
		TestModule testModule = convertWithOtherConverter(TestModule.class, structuralContainer.getModule());
		if (testModule == null) throw new ModelConverterException("could not successfully convert a ui module to a test module", structuralContainer.getModule());
		
		test.setModule(testModule);
		testModule.addTest(test);
		
		return test;
	}
	
	/**
	 * @author marcu
	 *
	 * @param <S>
	 * @param <T>
	 */
	public static class UIViewContainerToAbstractTestConverter<S extends UIViewContainer, T extends Test> extends UIStructuralContainerToAbstractTestConverter<S,T> {

		public UIViewContainerToAbstractTestConverter(AbstractConverter modelConverter) {
			super(modelConverter);
		}

		/* (non-Javadoc)
		 * @see com.gs.gapp.converter.ui.test.UIStructuralContainerToAbstractTestConverter#onCreateModelElement(com.gs.gapp.metamodel.ui.container.UIStructuralContainer, com.gs.gapp.metamodel.basic.ModelElementI)
		 */
		@Override
		protected T onCreateModelElement(S viewContainer, ModelElementI previouslyResultingElement) {
			return super.onCreateModelElement(viewContainer, previouslyResultingElement);
		}
	}
}
