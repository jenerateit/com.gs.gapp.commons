package com.gs.gapp.metamodel.converter;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;

import com.gs.gapp.metamodel.basic.options.ConverterOptions;

/**
 * This model converter does nothing else than returning the converter input as its output.
 * You still can apply filtering and validation for this model converter step with
 * these options: {@link ConverterOptions#PARAMETER_FILTER_BY_NAME_AND_TYPE}, {@link ConverterOptions#PARAMETER_VALIDATION_OCCURRENCE}
 * 
 * @author marcu
 *
 */
//@Component
public class PassThroughConverterProvider implements ModelConverterProviderI {

	public PassThroughConverterProvider() {}

	@Override
	public ModelConverterI getModelConverter() {
		return new PassThroughConverter();
	}

}
