package com.gs.gapp.metamodel.converter;

/**
 * In the converter cache it is not sufficient to use the original model element as the key of the cache map since
 * it is possible to convert one original model element into one or more new model elements. For this reason, the
 * converter itself needs to be included to determine an entry in the converter cache map. This class
 * serves as the key class for the converter cache map.
 *
 */
public final class OriginalModelElementKey {

	/**
	 *
	 */
	private final Object originalModelElement;

	private final Object contextualModelElement;

	/**
	 *
	 */
	private final Class<?> modelElementConverterClass;

	/**
	 * @param originalModelElement
	 * @param contextualModelElement
	 * @param modelElementConverterClass
	 */
	public OriginalModelElementKey(Object originalModelElement, Object contextualModelElement, Class<?> modelElementConverterClass) {
		if (originalModelElement == null) throw new NullPointerException("original model element must not be null");
		if (contextualModelElement == null); // that's perfectly fine
		if (modelElementConverterClass == null) throw new NullPointerException("model element converter class must not be null");

		this.originalModelElement = originalModelElement;
		this.contextualModelElement = contextualModelElement;
		this.modelElementConverterClass = modelElementConverterClass;
	}

	/**
	 * @param originalModelElement
	 * @param modelElementConverterClass
	 */
	public OriginalModelElementKey(Object originalModelElement, Class<?> modelElementConverterClass) {
		if (originalModelElement == null) throw new NullPointerException("original model element must not be null");
		if (modelElementConverterClass == null) throw new NullPointerException("model element converter class must not be null");

		this.originalModelElement = originalModelElement;
		this.contextualModelElement = null;
		this.modelElementConverterClass = modelElementConverterClass;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		boolean result = false;
		if (obj instanceof OriginalModelElementKey) {
			OriginalModelElementKey omek = (OriginalModelElementKey) obj;

			result = (this.originalModelElement == omek.originalModelElement &&
					  this.contextualModelElement == omek.contextualModelElement &&
					  this.modelElementConverterClass == omek.modelElementConverterClass);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
//		return (originalModelElement.hashCode() + "_" + (contextualModelElement == null ? "" : contextualModelElement.hashCode() + "_") + modelElementConverterClass.getName()).hashCode();
		// fra 15-Oct-19 changed to identity hash code to prevent errors, due to bad programmed original model elements (changing hash codes)
		//               also the equals method checks the identity so it should be better to check the identity hash code as well
		return (System.identityHashCode(originalModelElement) + "_" + (contextualModelElement == null ? "" : System.identityHashCode(contextualModelElement) + "_") + modelElementConverterClass.getName()).hashCode();
	}

	/**
	 * @return the modelElementConverterClass
	 */
	public final Class<?> getModelElementConverterClass() {
		return modelElementConverterClass;
	}

	/**
	 * @return the originalModelElement
	 */
	public final Object getOriginalModelElement() {
		return originalModelElement;
	}

	/**
	 * @return the contextualModelElement
	 */
	public final Object getContextualModelElement() {
		return contextualModelElement;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OriginalModelElementKey [originalModelElement="
				+ originalModelElement + ", contextualModelElement="
				+ contextualModelElement + ", modelElementConverterClass="
				+ modelElementConverterClass + "]";
	}
}
