/**
 * 
 */
package com.gs.gapp.metamodel.converter;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.ConverterOptions;

/**
 * This model converter does nothing else than returning the converter input as its output.
 * You still can apply filtering and validation for this model converter step with
 * these options: {@link ConverterOptions#PARAMETER_FILTER_BY_NAME_AND_TYPE}, {@link ConverterOptions#PARAMETER_VALIDATION_OCCURRENCE}
 * 
 * @author marcu
 *
 */
public class PassThroughConverter extends AbstractConverter {

	/**
	 *
	 */
	public PassThroughConverter() {
		super(new ModelElementCache());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		return Collections.emptyList();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelConsolidation(java.util.Set)
	 */
	@Override
	protected Set<Object> onPerformModelConsolidation(Set<?> normalizedElements) {
		LinkedHashSet<Object> result = new LinkedHashSet<>();
		result.addAll(normalizedElements);
		return result;
	}
}
