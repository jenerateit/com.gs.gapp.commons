/**
 *
 */
package com.gs.gapp.metamodel.converter;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.OptionsI;
import com.gs.gapp.metamodel.basic.options.ConverterOptions;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;

/**
 * This abstract model element converter is meant to be used when you want to convert
 * from an object that is of type ModelElement to another object that
 * also is of type ModelElement.
 *
 * @author mmt
 *
 */
public abstract class AbstractM2MModelElementConverter<S extends ModelElementI, T extends ModelElementI> extends
		AbstractModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public AbstractM2MModelElementConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/**
	 * @param modelConverter
	 * @param modelElementConverterBehavior
	 */
	public AbstractM2MModelElementConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}


	/**
	 * @param modelConverter
	 * @param previousResultingElementRequired
	 * @param indirectConversionOnly
	 * @param createAndConvertInOneGo
	 */
	public AbstractM2MModelElementConverter(AbstractConverter modelConverter, boolean previousResultingElementRequired, boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly, createAndConvertInOneGo);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onConvert(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {

		convertDoc(originalModelElement, resultingModelElement);

		if (originalModelElement instanceof ModelElement) {
			ModelElement modelElement = (ModelElement) originalModelElement;
			// Generalized mechanism to avoid the generation of target files for certain model elements.
			// This is required when model files need to be in the list of model files to be generated in
			// case they contain model elements that are referenced by other model elements/files. (mmt 17-Dec-2012)
			if (modelElement.getModule() != null && getConverterOptions() != null &&
				getConverterOptions().isModuleToBeGenerated(modelElement.getModule().getQualifiedName()) == false) {
				resultingModelElement.setGenerated(false);
			}
			
			if (originalModelElement.isGenerated() == false) {
				resultingModelElement.setGenerated(false);
			}
		}

		if (originalModelElement instanceof OptionsI && resultingModelElement instanceof OptionsI) {
			OptionsI originalElementWithOptions = (OptionsI) originalModelElement;
			OptionsI elementWithOptions = (OptionsI) resultingModelElement;
			// --- make sure that all options are going to be preserved through all model conversion steps
			for (OptionDefinition<?>.OptionValue optionValue : originalElementWithOptions.getOptions()) {
				elementWithOptions.addOptions(optionValue);
			}
		}
	}

	/**
	 * @param originalModelElement
	 * @param resultingModelElement
	 */
	protected void convertDoc(ModelElementI originalModelElement, ModelElementI resultingModelElement) {
		if (resultingModelElement.getBody() == null || resultingModelElement.getBody().length() == 0) {
			String originalBody = originalModelElement.getBody();
			if (originalBody != null && !originalBody.isEmpty()) {
				if (originalBody.contains(System.lineSeparator())) {
					// Uncomment this if you want to get rid of trailing empty lines in model element documentation. (mmt 15-Jun-2022)
//					StringBuilder bodyWithRemovedEmptyLinesAtEnd = removeTrailingEmptyLines(originalBody);
//					resultingModelElement.setBody(bodyWithRemovedEmptyLinesAtEnd.toString());
					resultingModelElement.setBody(originalBody);
				} else {
		            resultingModelElement.setBody(originalBody);
				}
			}
		} else {
			// the resulting model element already has a body set, so we must not override that value
		}
	}

	protected StringBuilder removeTrailingEmptyLines(String originalText) {
		// There might be trailing empty lines. Set them to null and
		// then make sure that lines that are null are not added to the body.
		String[] lines = originalText.split("[\r\n]+");
		for (int ii=lines.length-1 ; ii >= 0; ii--) {
			if (lines[ii] != null && lines[ii].trim().isEmpty()) {
				lines[ii] = null;
			} else if (lines[ii] != null && !lines[ii].trim().isEmpty()) {
				break;
			}
		}
		
		StringBuilder bodyWithRemovedEmptyLinesAtEnd = new StringBuilder();
		for (String partOfBody : lines) {
			if (partOfBody != null) {
				if (bodyWithRemovedEmptyLinesAtEnd.length() > 0) {
					bodyWithRemovedEmptyLinesAtEnd.append(System.lineSeparator());
				}
				bodyWithRemovedEmptyLinesAtEnd.append(partOfBody);
			}
		}
		return bodyWithRemovedEmptyLinesAtEnd;
	}
	
	/**
	 * @return
	 */
	protected ConverterOptions getConverterOptions() {
		return getModelConverter().getConverterOptions();
	}
}
