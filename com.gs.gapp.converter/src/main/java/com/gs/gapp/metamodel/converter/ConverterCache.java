package com.gs.gapp.metamodel.converter;

import java.util.LinkedHashMap;

/**
 * Holds instances of converted model elements in a map
 * in order to be able to reuse those instances and to
 * identify those model elements that had already been converted.
 * Please note that the sole fact that an object is part of this cache
 * doesn't mean that the conversion process for that object had been
 * completed already.
 *
 */
@SuppressWarnings("hiding")
public class ConverterCache<OriginalModelElementKey,ModelElement> extends LinkedHashMap<OriginalModelElementKey,ModelElement> {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public ConverterCache() {
		super();
	}
}
