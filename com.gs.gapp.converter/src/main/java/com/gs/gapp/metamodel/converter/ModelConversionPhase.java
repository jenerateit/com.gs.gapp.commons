/**
 * 
 */
package com.gs.gapp.metamodel.converter;

/**
 * This enumeration defines the model conversion phases. The order of
 * the enum constants determines the order of execution inside model converters
 * that inherit from AbstractConverter.
 * 
 * @author mmt
 *
 */
public enum ModelConversionPhase {

	/**
	 * initial phase, nothing done so far
	 */
	START,
	
	/**
	 * optionally, remove some elements from the collection of input model elements
	 */
	FILTER,
	
	/**
	 * e.g. getting all elements that reside inside a package or derive
	 * certain attribute values by having a look at other attributes
	 */
	NORMALIZATION,
	
	/**
	 * check whether the provided model (meta-model version, notation version) is compatible to the model converter version
	 */
	COMPATIBILITY_CHECK,
	
	/**
	 * create instances of meta-model types that do not have corresponding elements in the input model
	 */
	ELEMENT_PREDEFINITION,
	
	/**
	 * call the model element converters
	 */
	VALIDATION_BEFORE_CONVERSION,
	
	/**
	 * call the model element converters
	 */
	CONVERSION,
	
	/**
	 * do some post-processing, e.g. removing superfluous model elements, setting flags, etc.
	 */
	CONSOLIDATION,
	
	/**
	 * validate the conversion result, subsequent model converters rely on a passed input model to have been validated before
	 */
	VALIDATION,
	;
}
