package com.gs.gapp.metamodel.converter;

import com.gs.gapp.metamodel.basic.MessageI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.MessageStatus;

public enum ConverterMessage implements MessageI {

	UNKNOWN_ERROR (MessageStatus.ERROR,
			"0001",
			"An unknown error has occurred: %s",
			null),
	
	ELEMENT_CONVERSION_CREATES_AMBIGUOUS_RESULT (MessageStatus.ERROR,
			"0002",
			"A model element converter creates two different results '%s' and '%s' for one and the same input element '%s'.",
			null),

	UNKNOWN_ERROR_ELEMENT_CONVERTER (MessageStatus.ERROR,
			"0003",
			"An unknown error occurred in the element converter %s: %s",
			null),
	
	MODEL_VALIDATION_ERROR (MessageStatus.ERROR,
			"0004",
			"The validation of the input model, before the model conversion, failed in model converter %s.",
			"Check the other error messages to find out which validation rules are not fulfilled."),
	
	NO_MODEL_CONVERTER_FOUND_ERROR (MessageStatus.ERROR,
			"0005",
			"Tried to get hold of the model converter during element conversion, but could not find one.",
			"Please contact the support of the generator's producer and provide it with the following information: the model converter '%s' could not be found in the element converter '%s'."),
	;

	private final MessageStatus status;
	private final String organization = "GS";
	private final String section = "CONVERTER";
	private final String id;
	private final String problemDescription;
	private final String instruction;
	
	private ConverterMessage(MessageStatus status, String id, String problemDescription, String instruction) {
		this.status = status;
		this.id = id;
		this.problemDescription = problemDescription;
		this.instruction = instruction;
	}
	
	@Override
	public MessageStatus getStatus() {
		return status;
	}

	@Override
	public String getOrganization() {
		return organization;
	}

	@Override
	public String getSection() {
		return section;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getProblemDescription() {
		return problemDescription;
	}
	
	@Override
	public String getInstruction() {
		return instruction;
	}
}
 