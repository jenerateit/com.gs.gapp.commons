package com.gs.gapp.metamodel.converter;

public enum ModelElementConverterBehavior {

	DEFAULT,
	PREV_ELEMENT_REQUIRED,
	INDIRECT_CONVERSION_ONLY,
	CREATE_AND_CONVERT_IN_ONE_GO,
	
	PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO,
	PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY,
	PREV_ELEMENT_REQUIRED___CREATE_AND_CONVERT_IN_ONE_GO,
	
	INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO,
	;

	/**
	 * @return
	 */
	public boolean isPreviousResultingElementRequired() {
		switch (this) {
		case CREATE_AND_CONVERT_IN_ONE_GO:
		case DEFAULT:
		case INDIRECT_CONVERSION_ONLY:
		case INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO:
			return false;
			
		case PREV_ELEMENT_REQUIRED:
		case PREV_ELEMENT_REQUIRED___CREATE_AND_CONVERT_IN_ONE_GO:
		case PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY:
		case PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO:
			return true;
			
		default:
			throw new RuntimeException("unhandled enum entry '" + this + "' detected");
		}
	}
	
	/**
	 * @return
	 */
	public boolean isIndirectConversionOnly() {
		switch (this) {
		
		case INDIRECT_CONVERSION_ONLY:
		case INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO:
		case PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY:
		case PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO:
			return true;

		case DEFAULT:
		case CREATE_AND_CONVERT_IN_ONE_GO:
		case PREV_ELEMENT_REQUIRED:
		case PREV_ELEMENT_REQUIRED___CREATE_AND_CONVERT_IN_ONE_GO:
			return false;
			
		default:
			throw new RuntimeException("unhandled enum entry '" + this + "' detected");
	    }
	}
	
	/**
	 * @return
	 */
	public boolean isCreateAndConvertInOneGo() {
		switch (this) {
		
		case CREATE_AND_CONVERT_IN_ONE_GO:
		case INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO:
		case PREV_ELEMENT_REQUIRED___CREATE_AND_CONVERT_IN_ONE_GO:
		case PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO:
			return true;

		case DEFAULT:
		case PREV_ELEMENT_REQUIRED:
		case INDIRECT_CONVERSION_ONLY:
		case PREV_ELEMENT_REQUIRED___INDIRECT_CONVERSION_ONLY:
		
			return false;
			
		default:
			throw new RuntimeException("unhandled enum entry '" + this + "' detected");
	    }
	}
}