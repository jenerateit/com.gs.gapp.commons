/**
 * 
 */
package com.gs.gapp.metamodel.converter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.gs.gapp.metamodel.basic.ModelElementI;

/**
 * With this annotation you can declaratively define source and target type
 * to be used by a model element converter. If you use generics for the model
 * element converter class, you won't need to use this annotation. Reflection
 * automatically detects the source and target types then.
 *  
 * @author mmt
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface ConversionMapping {
	Class<? extends Object> source();
	Class<? extends ModelElementI> target();
}
