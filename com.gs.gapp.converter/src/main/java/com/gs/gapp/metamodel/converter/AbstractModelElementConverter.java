package com.gs.gapp.metamodel.converter;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.BasicMessage;
import com.gs.gapp.metamodel.basic.ConversionDetails;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementTraceabilityI;
import com.gs.gapp.metamodel.basic.ModelElementWrapper;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;


/**
 * @author mmt
 *
 */
public abstract class AbstractModelElementConverter<S extends Object, T extends ModelElementI> {
	
	/**
	 * Convenience method to throw a {@link ModelConverterException} in case an enum entry
	 * should have been handled in a switch but in fact wasn't. Such a situation indicates
	 * a bug, e.g. generator components with semantically incompatible versions.
	 * 
	 * @param enumEntry
	 */
	protected void throwUnhandledEnumEntryException(Enum<?> enumEntry) {
	    Message errorMessage = BasicMessage.UNHANDLED_ENUM_ENTRY
			.getMessageBuilder()
			.parameters(enumEntry.name(), enumEntry.getClass().getSimpleName(), this.getClass().getSimpleName())
			.build();
        throw new ModelConverterException(errorMessage.getMessage());
	}

	/**
	 * Every model element converter has got access to its corresponding model converter.
	 * With this a model element converter can add model elements to the model
	 * element cache, that is owned by the model converter.
	 */
	private final AbstractConverter modelConverter;

	private final boolean previousResultingElementRequired;

	private final boolean indirectConversionOnly;
	
	private final boolean createAndConvertInOneGo;
	
	private final ModelElementConverterBehavior modelElementConverterBehavior;

	/**
	 * @param modelConverter
	 */
	public AbstractModelElementConverter(AbstractConverter modelConverter) {
		if (modelConverter == null) throw new NullPointerException("convert parameter must not be null");
		this.modelConverter = modelConverter;
		this.previousResultingElementRequired = ModelElementConverterBehavior.DEFAULT.isPreviousResultingElementRequired();
		this.indirectConversionOnly = ModelElementConverterBehavior.DEFAULT.isIndirectConversionOnly();
		this.createAndConvertInOneGo = ModelElementConverterBehavior.DEFAULT.isCreateAndConvertInOneGo();
		this.modelElementConverterBehavior = ModelElementConverterBehavior.DEFAULT;
	}
	
	public AbstractModelElementConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		if (modelConverter == null) throw new NullPointerException("convert parameter must not be null");
		this.modelConverter = modelConverter;
		this.previousResultingElementRequired = modelElementConverterBehavior.isPreviousResultingElementRequired();
		this.indirectConversionOnly = modelElementConverterBehavior.isIndirectConversionOnly();
		this.createAndConvertInOneGo = modelElementConverterBehavior.isCreateAndConvertInOneGo();
		this.modelElementConverterBehavior = modelElementConverterBehavior;
	}
	
	/**
	 * @param modelConverter
	 * @param previousResultingElementRequired
	 * @param indirectConversionOnly
	 * @param createAndConvertInOneGo
	 */
	public AbstractModelElementConverter(AbstractConverter modelConverter, boolean previousResultingElementRequired,
			boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		if (modelConverter == null) throw new NullPointerException("convert parameter must not be null");
		this.modelConverter = modelConverter;
		this.previousResultingElementRequired = previousResultingElementRequired;
		this.indirectConversionOnly = indirectConversionOnly;
		this.createAndConvertInOneGo = createAndConvertInOneGo;
		this.modelElementConverterBehavior = null;  // set this to null in order to have a means for older converters to use the default behavior for indirect conversion regarding CREATE_AND_CONVERT_IN_ONE_GO (mmt 03-Jul-2015)
	}

	/**
	 * @return the converter from where this model element converter got instantiated
	 */
	protected AbstractConverter getModelConverter() {
		return modelConverter;
	}
	
	/**
	 * @return
	 */
	protected Set<Object> getFilteredModelElements() {
		return this.modelConverter.getFilteredModelElements();
	}
	
	/**
	 * @param modelElement
	 * @return
	 */
	protected boolean isFiltered(Object modelElement) {
		return getFilteredModelElements().contains(modelElement);
	}

	/**
	 * This method returns the model element that is the result of the conversion
	 * of an original model element by using this converter. The method does _not_
	 * perform the actual conversion. It is the converter's convert() method
	 * that performs the conversion.
	 *
	 * @param originalModelElement
	 * @return the conversion result
	 */
	public final ModelElementI getConvertedModelElement(Object originalModelElement) {
		return modelConverter.getConvertedModelElementFromConversionCache(originalModelElement, this);
	}

	/**
	 * @param originalModelElement
	 * @param modelElement
	 */
	protected void setConvertedModelElement(Object originalModelElement, ModelElementI modelElement) {
		modelConverter.setConvertedModelElementInConversionCache(originalModelElement, this, modelElement);
	}
	
	/**
	 * This method returns the model element that is the result of the conversion
	 * of an original model element by using this converter. The method does _not_
	 * perform the actual conversion. It is the converter's convert() method
	 * that performs the conversion.
	 *
	 * @param originalModelElement
	 * @return the conversion result
	 */
	public final ModelElementI getCreatedModelElement(Object originalModelElement, ModelElementI previouslyCreatedModelElement) {
		return modelConverter.getConvertedModelElementFromCreationCache(originalModelElement, previouslyCreatedModelElement, this);
	}

	/**
	 * @param originalModelElement
	 * @param modelElement
	 */
	protected void setCreatedModelElement(Object originalModelElement, ModelElementI previouslyCreatedModelElement, ModelElementI modelElement) {
		modelConverter.setConvertedModelElementInCreationCache(originalModelElement, previouslyCreatedModelElement, this, modelElement);
	}

	/**
	 * This method checks if this converter is responsible for conversion. This method may be overwritten by clients.
	 * By default it checks if the source class is identical.
	 *
	 * @param originalModelElement
	 * @param previousResultingModelElement
	 * @return true if this converter is responsible for the conversion otherwise false.
	 * @see #isResponsibleFor(Object, ModelElementI, boolean)
	 */
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		return isResponsibleForSourceClass(originalModelElement, true);
	}


	/**
	 * @param originalModelElement
	 * @param previousResultingModelElement
	 * @param checkSourceClassToBeIdentical
	 * @return
	 */
	public final boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement, boolean checkSourceClassToBeIdentical) {
		return isResponsibleForSourceClass(originalModelElement, checkSourceClassToBeIdentical);
	}

	/**
	 * This method checks if this converter is responsible for conversion. This method may be overwritten by clients.
	 * By default the {@link #isResponsibleFor(Object, ModelElementI)} method is called.
	 *
	 * @param originalModelElement
	 * @param previousResultingModelElement
	 * @param resultClass
	 * @return true if this converter is responsible for the conversion otherwise false.
	 * @see #isResponsibleFor(Object, ModelElementI, Class, boolean, boolean)
	 */
	public boolean isResponsibleFor(
			Object originalModelElement,
			ModelElementI previousResultingModelElement,
			Class<? extends ModelElementI> resultClass) {
		
		return isResponsibleFor(originalModelElement, previousResultingModelElement);
	}
	
	/**
	 * 
	 * @param originalModelElement
	 * @param previousResultingModelElement
	 * @param resultClass
	 * @param checkSourceClassToBeIdentical
	 * @param checkTargetClassToBeIdentical
	 * @return
	 */
	public final boolean isResponsibleFor(
			Object originalModelElement,
			ModelElementI previousResultingModelElement,
			Class<? extends ModelElementI> resultClass,
			boolean checkSourceClassToBeIdentical,
			boolean checkTargetClassToBeIdentical) {
		
		boolean result = isResponsibleForSourceClass(originalModelElement, checkSourceClassToBeIdentical);
		
		if(result && checkTargetClassToBeIdentical) {
			result = isTargetClassIdentical(resultClass);
		}
		
		return result;
	}
	
	private boolean isResponsibleForSourceClass(Object originalModelElement, boolean identical) {
		if (identical) {
			return isSourceClassIdentical(originalModelElement);
		} else {
			return getSourceClass().isInstance(originalModelElement);
		}
	}

	private boolean isSourceClassIdentical(Object originalModelElement) {
		if (originalModelElement == null) {
			throw new NullPointerException("parameter 'originalModelElement' must not be null");
		}
		boolean result = false;
		Class<?> sourceClass = this.getSourceClass();
		
		if (sourceClass != null &&
			    (sourceClass == originalModelElement.getClass()) ||
				(sourceClass.isInterface() && sourceClass.isInstance(originalModelElement))) {
			result = true;
		}
		
		return result;
	}
	
	private boolean isTargetClassIdentical(Class<? extends ModelElementI> resultClass) {
		boolean result = false;
		Class<?> targetClass = this.getTargetClass();

		if(targetClass != null && resultClass != null) {
			result = targetClass.equals(resultClass);
		}
		
		return result;
	}
	
	/**
	 * @param otherElementConverter
	 * @return
	 */
	protected AbstractModelElementConverter<?,?> getHiddenModelElementConverter(AbstractModelElementConverter<?,?> otherElementConverter) {
		AbstractModelElementConverter<?,?> result = null;

		if (otherElementConverter.getTargetClass() == this.getTargetClass()) {
			if (this.getSourceClass() != otherElementConverter.getSourceClass()) {
				if (otherElementConverter.getSourceClass().isAssignableFrom(this.getSourceClass())) {
					result = otherElementConverter;
				} else if (this.getSourceClass().isAssignableFrom(otherElementConverter.getSourceClass())) {
					result = this;
				}
			}
		}

		return result;
	}

	/**
	 * @param originalModelElement
	 * @return
	 */
	protected final T create(Object originalModelElement) {
		return create(originalModelElement, null);
	}
	
	/**
	 * @param originalModelElement
	 * @param result
	 */
	protected final void convert(final Object originalModelElement, final ModelElementI result) {
		this.convert(originalModelElement, result, null);
	}
	
	@SuppressWarnings("unchecked")
	protected final void convert(final Object originalModelElement, final ModelElementI existingResult, final ModelElementI previousResultingModelElement) {
		if (originalModelElement == null) throw new NullPointerException("parmeter 'originalModelElement' must not be null");
		if (existingResult == null) throw new NullPointerException("parmeter 'result' must not be null");
		
		try {
			OriginalModelElementKey key = null;
			
//			OriginalModelElementKey keyWihtoutContextualElement = null;
//			boolean resultIsFromCache = false;

			// --- initialize the key for caching and lookup-table logic
			key = new OriginalModelElementKey(originalModelElement, previousResultingModelElement, getClass());
			
			
//			keyWihtoutContextualElement = new OriginalModelElementKey(originalModelElement, getClass());

//			// --- get element from cache, since the resulting model element could already have been successfully created
//			ModelElementI result = (T) getCreationCache().get(key);
//			
//			if (result == null) {
//				// --- execute the model element conversion
//				if (getConverterLocks().contains(keyWihtoutContextualElement)) {
//					// circular calls to createModelElement()
//					throw new ModelConverterException("Circular call to getOrCreateModelElement() detected. You need to check and modify your converter logic to fix this. Original model element:" + originalModelElement);
//				} else {
//					getConverterLocks().add(keyWihtoutContextualElement);
//					try {
//					    result = createModelElement(originalModelElement, previousResultingModelElement);
//	                    postProcessElements(originalModelElement, result);
//					} finally {
//						getConverterLocks().remove(keyWihtoutContextualElement);
//					}
//				}
//			} else {
//				resultIsFromCache = true;
//			}

//			// --- initialize the key for caching and lookup-table logic
//			key = new OriginalModelElementKey(originalModelElement, getClass());
			
//			ModelElementI existingResult = (T) getConversionCache().get(key);
			
			if (getConversionCache().containsKey(key) == false) {
				// first put it on the cache in order to ensure that the onConvert() is not getting called again for the same combination of element and element converter
				getConversionCache().put(key, existingResult);
				try {
				    onConvert((S)originalModelElement, (T) existingResult, previousResultingModelElement);
				    validateResultingModelElement((T) existingResult);
				} catch (Throwable th) {
					if (th instanceof ModelConverterException) {
						throw th;
					} else {
						Message message = ConverterMessage.UNKNOWN_ERROR_ELEMENT_CONVERTER.getMessageBuilder()
								.modelElement(existingResult)
								.parameters(
										this.getClass().getName(),
										th.getMessage())
								.throwable(th)
								.build();
						throw new ModelConverterException(message.getMessage(), th);
					}
				}
			}
		} catch (ClassCastException ex) {
			ex.printStackTrace();
			throw new ModelConverterException(
					"class cast exception occured during model element conversion process, did you implement onCreateModelElement() in your model element converter class and let it return an appropriate type?",
					ex);
		}
	}

	/**
	 * This method performs the actual conversion of a given original model element.
	 * If the converter is not responsible for the given original model element, then
	 * the method returns null. And if the model element already got converted
	 * with the given model element converter, then the converted model element
	 * is retrieved from a cache before it is returned.
	 *
	 * @param originalModelElement
	 * @param previousResultingModelElement
	 * @return the result of the original model element's conversion, null if the converter is not
	 *         responsible for the original model element.
	 *
	 */
	@SuppressWarnings("unchecked")
	public final T create(Object originalModelElement, ModelElementI previousResultingModelElement) {
		T result = null;
		if (!modelConverter.isIgnored(originalModelElement)) {

			result = (T) getCreatedModelElement(originalModelElement, previousResultingModelElement);
			if (result == null) {
		        result = doCreation((S)originalModelElement, previousResultingModelElement);
			} else {
				// If we get into this else-block, then we are in a situation where
				// we avoid an infinite loop by re-using an already converted model element,
				// even if it is not yet completely initialized.
			}
		}

		return result;
	}

	/**
	 * @return
	 */
	public final Class<?> getSourceClass() {
		Class<?> result = null;

		ConversionMapping mapping = getClass().getAnnotation(ConversionMapping.class);
		result = (mapping == null ? null : mapping.source());

		if (result == null) {
			result = (Class<?>) getTypeArgumentOfParameterizedSupertype(2, 0, this.getClass());
		}

		return result;
	}

	private Type getTypeArgumentOfParameterizedSupertype(int numberOfActualTypeArguments, int indexOfActualTypeArgument, Class<?> clazz) {
		Type result = null;

		Type genericSuperclass = this.getClass().getGenericSuperclass();
		while (genericSuperclass != null) {
			if (genericSuperclass instanceof ParameterizedType) {

				ParameterizedType parameterizedSuperclass = (ParameterizedType) genericSuperclass;
				if (parameterizedSuperclass.getActualTypeArguments().length == numberOfActualTypeArguments) {
					Type actualTypeArgument = parameterizedSuperclass.getActualTypeArguments()[indexOfActualTypeArgument];
					if (actualTypeArgument instanceof TypeVariable) {
						Type bounds0Type = ((TypeVariable<?>) actualTypeArgument).getBounds()[0];
						if (bounds0Type instanceof ParameterizedType) {
							ParameterizedType parameterizedType = (ParameterizedType) bounds0Type;
							result = parameterizedType.getRawType();
						} else {
							result = bounds0Type;
						}
					} else if (actualTypeArgument instanceof Class<?>) {
						result = actualTypeArgument;
					}
					break;
				} else {
					Type rawType = parameterizedSuperclass.getRawType();
					if (rawType instanceof Class) {
						Class<?> rawClass = (Class<?>) rawType;
						genericSuperclass = rawClass.getGenericSuperclass();
					} else {
						genericSuperclass = null;
					}
				}
			} else {
				genericSuperclass = null;
			}
		}

		return result;
	}

	/**
	 * @return
	 */
	@SuppressWarnings({ "unchecked" })
	public final Class<? extends ModelElementI> getTargetClass() {
        Class<? extends ModelElementI> result = null;

        ConversionMapping mapping = getClass().getAnnotation(ConversionMapping.class);
		result = (mapping == null ? null : mapping.target());

		if (result == null) {
			result = (Class<? extends ModelElementI>) getTypeArgumentOfParameterizedSupertype(1, 0, this.getClass());
			if (result == null) {
				result = (Class<? extends ModelElementI>) getTypeArgumentOfParameterizedSupertype(2, 1, this.getClass());
			}
		}

		return result;
	}

	/**
	 * @param resultClass
	 * @param originalModelElement
	 * @param converterClassArr
	 * @return
	 */
	protected final <M extends ModelElementI> M convertWithOtherConverter(Class<M> resultClass,
            Object originalModelElement,
            Class<?>... converterClassArr) {
		if (originalModelElement == null) {
			throw new NullPointerException("parameter 'originalModelElement' must not be null");
		}
		
		return convertWithOtherConverter(resultClass, originalModelElement, null, converterClassArr);
	}

	/**
	 * This method allows for converting a model element while the conversion process within
	 * a model element converter is ongoing. Its purpose is to return one single resulting element.
	 * An exception (ModelConverterException) is going to be thrown if there is more than one
	 * model element converter feeling responsible for the given element and returning a new model element as result.
	 *
	 * @param <M>
	 * @param resultClass the type of the return value
	 * @param originalModelElement the model element that should be converted
	 * @param existingResultingModelElement
	 * @param converterClassArr an optional array of converter classes in order to limit the number of converters that are taken into account
	 * @return
	 */
	protected final <M extends ModelElementI> M convertWithOtherConverter(Class<M> resultClass,
			                                                             Object originalModelElement,
			                                                             ModelElementI existingResultingModelElement,
			                                                             Class<?>... converterClassArr) {
		
		if (originalModelElement == null) {
			throw new NullPointerException("parameter 'originalModelElement' must not be null");
		}

		Collection<M> result = convertWithOtherConverters(resultClass, originalModelElement, existingResultingModelElement, converterClassArr);

		if (result == null || result.size() == 0) {
			return null;
		} else if (result.size() == 1) {
			Iterator<M> iterator = result.iterator();
			if (iterator == null) {
				System.out.println("Iterator is null!!! Why???");
			}
			return iterator.next();
		} else {
			throw new ModelConverterException("found more than one conversion result for result class " + resultClass + " and original model element " + originalModelElement + " (class=" + originalModelElement.getClass() + "), result:" + result);
		}
	}

	/**
	 * @param resultClass
	 * @param originalModelElement
	 * @param converterClassArr
	 * @return
	 */
	protected final <M extends ModelElementI> Collection<M> convertWithOtherConverters(
			Class<M> resultClass, Object originalModelElement,
			Class<?>... converterClassArr) {
		if (originalModelElement == null) throw new NullPointerException("parameter 'originalModelElement' must not be null");
		
		return convertWithOtherConverters(resultClass, originalModelElement, null, converterClassArr);
	}

	/**
	 * This method allows for converting a model element while the conversion process within
	 * a model element converter is ongoing. Its purpose is to return a collection of resulting element.
	 *
	 * @param resultClass
	 * @param originalModelElement
	 * @param existingResultingModelElement
	 * @param converterClassArr
	 * @return
	 */
	protected final <M extends ModelElementI> Collection<M> convertWithOtherConverters(
			Class<M> resultClass, Object originalModelElement, ModelElementI existingResultingModelElement,
			Class<?>... converterClassArr) {
		
		if (originalModelElement instanceof ModelElementI &&
				getBehaviorForIndirectConversion((ModelElementI) originalModelElement) == BehaviorForIndirectConversion.DONT_CONVERT) {
			return null;
		}
		
		Collection<M> result = new LinkedHashSet<>();
		StringBuilder sb = new StringBuilder();
		List<Class<?>> converterList = null;

		if (converterClassArr != null) {
			converterList = Arrays.asList(converterClassArr);
		}

		for (AbstractModelElementConverter<? extends Object, ? extends ModelElementI> modelElementConverter :
			    getModelConverter().getModelElementConverters(originalModelElement, resultClass, existingResultingModelElement, converterClassArr)) {
			if (sb.length() > 0) {
				sb.append("|"); // this is there for being able to log
								// detailed information
			}
			
			if (converterList == null || converterList.size() == 0 || converterExtendsConverterInList(modelElementConverter, converterList)) {

				ModelElementI alreadyCreatedModelElement = modelConverter.getConvertedModelElementFromCreationCache(originalModelElement, existingResultingModelElement, modelElementConverter);
				
				if (alreadyCreatedModelElement == null) {
					ModelElementI modelElement = modelElementConverter.create(originalModelElement, existingResultingModelElement);
					
					if (modelElement != null && resultClass.isAssignableFrom(modelElement.getClass())) {
						if (modelElementConverter.isCreateAndConvertInOneGoForIndirectConversion()) {
							sb.append(modelElementConverter.getClass());
							modelElementConverter.convert(originalModelElement, modelElement, existingResultingModelElement);
						} else {
							// --- delayed conversion
							OriginalModelElementKey key = new OriginalModelElementKey(originalModelElement, existingResultingModelElement, modelElementConverter.getClass());
//							if (modelElementConverter.getClass().getSimpleName().equals("UILinkComponentToCommandLinkComponentConverter") &&
//									originalModelElement.getClass().getSimpleName().equals("UILinkComponent")) {
//								System.out.println("add to delayed conversion, key:" + key + ", model element converter:" + modelElementConverter);
//							}
							getModelConverter().addToDelayedConversion(key, modelElementConverter);	
						}
						result.add(resultClass.cast(modelElement));
					}
				} else {
					// in this case we do not need to call convert() from within here - the model element got already created, so the convert() call will definitely follow soon
					result.add(resultClass.cast(alreadyCreatedModelElement));
				}
			}
		}

		if (originalModelElement instanceof ModelElementI &&
				getBehaviorForIndirectConversion((ModelElementI) originalModelElement) == BehaviorForIndirectConversion.CONVERT_BUT_DONT_GENERATE) {
			result.stream().forEach(m -> {
				System.out.println("The element '" + m.toString() + "' has been created but will not be generated. Most probably this behavior is cause by the original element '" + originalModelElement + "' having been filtered in the model converter '" + this.getClass().getSimpleName() + "'");
				m.setGenerated(false);
			});
		}
		
		return result;
	}

	/**
	 * Checks whether a given converter object's class is assignable from one of the classes in the list of converter classes
	 *
	 * @param converter model element converter
	 * @param listOfConverterClasses
	 * @return
	 */
	private boolean converterExtendsConverterInList(AbstractModelElementConverter<? extends Object, ? extends ModelElementI> converter,
			                                        List<Class<?>> listOfConverterClasses) {
		boolean result = false;

		if (listOfConverterClasses != null) {
			for (Class<?> aConverterClass : listOfConverterClasses) {
				if (converter.getClass().isAssignableFrom(aConverterClass)) {
					result = true;
					break;
				}
			}

		}

		return result;
	}


	/**
	 *
	 * @see doConversion(Object originalModelElement, ModelElement reusableModelElement);
	 *
	 * @param originalModelElement the model element that should be converted
	 * @param previousResultingModelElement
	 * @return the conversion result
	 */
	private final T doCreation(S originalModelElement, ModelElementI previousResultingModelElement) {
		T modelElement = getOrCreateModelElement(originalModelElement, previousResultingModelElement);
		return modelElement;
	}

	/**
	 * This method holds the actual implementation of the conversion logic.
	 *
	 * @param originalModelElement
	 * @param resultingModelElement
	 */
	protected abstract void onConvert(S originalModelElement, T resultingModelElement);
	
	/**
	 * This method holds the actual implementation of the conversion logic.
	 *
	 * @param originalModelElement
	 * @param resultingModelElement
	 */
	protected void onConvert(S originalModelElement, T resultingModelElement, ModelElementI previousResultingModelElement) {
		onConvert(originalModelElement, resultingModelElement);  // the default implementation is to not pass the previous resulting element
	}


	/**
	 * Creates a new model element, unless the given reusableModelElement
	 * is not null and is assignable from the return type of
	 * the class' method createModelElement().
	 *
	 * @param originalModelElement
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected final T getOrCreateModelElement(final S originalModelElement, final ModelElementI previousResultingModelElement) {
		if (originalModelElement == null) {
			throw new NullPointerException("parameter originalModelElement must not be null");
		}

		T result = null;
		OriginalModelElementKey key = null;
		OriginalModelElementKey keyWihtoutContextualElement = null;
		boolean resultIsFromCache = false;

		// --- initialize the key for caching and lookup-table logic
		key = new OriginalModelElementKey(originalModelElement, previousResultingModelElement, getClass());
		keyWihtoutContextualElement = new OriginalModelElementKey(originalModelElement, getClass());

		// --- get element from cache, since the resulting model element could already have been successfully created
		result = (T) getCreationCache().get(key);


		if (result == null) {
			// --- execute the model element conversion
			if (getConverterLocks().contains(keyWihtoutContextualElement)) {
				// circular calls to createModelElement()
				throw new ModelConverterException("Circular call to getOrCreateModelElement() detected. You need to check and modify your converter logic to fix this. Original model element:" + originalModelElement);
			} else {
				getConverterLocks().add(keyWihtoutContextualElement);
				try {
					validateOriginalModelElement(originalModelElement);
				    result = createModelElement(originalModelElement, previousResultingModelElement);
				    if (result != null && originalModelElement == result && !this.isCreateAndConvertInOneGo()) {
				    	// an element converter that returns the same object as was provided as the original model element, _must_ have CREATE_AND_CONVERT_IN_ONE_GO set
				    	// in order to force the element conversion to be completed before any delayed conversion takes place. (mmt 17-Feb-2017)
				    	throw new ModelConverterException("The converter returns the identical object as was provided, which is allowed. However, in this case the element converter has to have CREATE_AND_CONVERT_IN_ONE_GO being set");
				    }
                    postProcessElements(originalModelElement, result);
				} finally {
					getConverterLocks().remove(keyWihtoutContextualElement);
				}
			}
		} else {
			resultIsFromCache = true;
		}

        // --- finally, adding the result to the creation cache and set the conversion details
		if (result != null && result != originalModelElement) {

			// --- check for the rules that prevent duplicated conversions, one with and another one without a contextual element
			if (previousResultingModelElement != null && getCreationCache().containsKey(keyWihtoutContextualElement)) {
				// a resulting model element got created/found with _and_ without a contextual element, which is not allowed
				// 1st creation: without contextual element, 2nd creation: with contextual element
				throw new ModelConverterException("For the given original model element '" + originalModelElement + "' the converter '" + this + "' created a result for the given contextual element '" + previousResultingModelElement + "', but the same converter already had created a result for the contextual element being null");
			} else if (previousResultingModelElement == null && getContextualConverterLookupTable().contains(keyWihtoutContextualElement)) {
				// a resulting model element got created/found without a contextual element but the same converter already had converted the same element _with_ a contextual element, which is not allowed 
				// 1st creation: with contextual element, 2nd creation: without contextual element
				throw new ModelConverterException("For the given original model element '" + originalModelElement + "' the converter '" + this + "' created a result for contextual element being null, but the same converter already had created a result for the contextual element being not-null");
			}

			if (!getCreationCache().containsKey(key)) {
			    getCreationCache().put(key, result);
//			    System.out.println("creation cache now has " + getCreationCache().size() + " elements");
			    if (previousResultingModelElement != null) {
			    	// we keep in mind that the result got created by means of a converter that needs a contextual element
			    	// TODO it might be of help to additionally keep the information about which objects were used as contextual elements
			        getContextualConverterLookupTable().add(keyWihtoutContextualElement);
			    }
			}
			if (!resultIsFromCache) {
				setConversionDetails(result);
			}
		}

		return result;
	}

	/**
	 * @param originalModelElement
	 */
	private final void validateOriginalModelElement(S originalModelElement) {
		onValidateOriginalModelElement(originalModelElement);
	}

	/**
	 * Override this method if you want to explicitely perform validation checks for the original model element
	 * before doing anything else. 
	 * 
	 * @param originalModelElement
	 */
	protected void onValidateOriginalModelElement(S originalModelElement) {
		return;
	}
	
	/**
	 * @param resultingModelElement
	 */
	private final void validateResultingModelElement(T resultingModelElement) {
		onValidateResultingModelElement(resultingModelElement);
	}

	/**
	 * Override this method if you want to explicitely perform validation checks for the resulting model element
	 * before continuing. 
	 * 
	 * @param resultingModelElement
	 */
	protected void onValidateResultingModelElement(T resultingModelElement) {
		return;
	}

	/**
	 * Within this method, source and target element are linked to each other in order to get
	 * a model element tree in the end.
	 *
	 * @param sourceElement
	 * @param targetElement
	 */
	protected void postProcessElements(final S sourceElement, final T targetElement) {
		if (sourceElement != null && targetElement != null && sourceElement != targetElement) {
			if (targetElement instanceof ModelElementTraceabilityI) {
				ModelElementTraceabilityI traceableElement = (ModelElementTraceabilityI) targetElement;

				Object originatingElement = null;
				if (sourceElement instanceof ModelElementTraceabilityI) {
                    originatingElement = sourceElement;
				} else {
					// We need to use the model element wrapper here in order to be able to find subsequent
					// elements from the element wrapper (the original object itself would not provide appropriate methods/functionality).
					
					if (traceableElement.getOriginatingElement() != null && traceableElement.getOriginatingElement() instanceof ModelElementWrapper) {
						ModelElementWrapper wrappedOriginatingElement = (ModelElementWrapper) traceableElement.getOriginatingElement();
						if (wrappedOriginatingElement.getWrappedElement() == sourceElement) {
							// no need to set the originating element
							originatingElement = null;
						} else {
//							originatingElement = new ModelElementWrapper(sourceElement);
							originatingElement = new ModelElementWrapper(new Object());
						}
					} else {
//					    originatingElement = new ModelElementWrapper(sourceElement);
					    originatingElement = new ModelElementWrapper(new Object());
					}
				}

				if (originatingElement != null) {
                    traceableElement.setOriginatingElement(originatingElement);
				}
			}
		}
	}


	/**
	 * @param modelElement
	 */
	protected void setConversionDetails(ModelElementI modelElement) {
		modelElement.setConversionDetails( new ConversionDetails(getModelConverter().getClass().getName(),
				                                                 this.getClass().getName(),
				                                                 getModelConverter().getOptions()) );
	}

	/**
	 * If a model element converter wants to avoid that one or more different model element converters
	 * execute the conversion for the same model element, this method has to return the set of model element converter
	 * classes that should not do the conversion.
	 *
	 * @return
	 */
	protected Set<java.lang.Class<?>> getExcludedModelElementConverters() {
		return new LinkedHashSet<>();
	}


	/**
	 * @return
	 */
	private final T createModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		T result = null;

		if (this.previousResultingElementRequired && previousResultingModelElement == null) {
			throw new ModelConverterException("The model element converter '" + this + "' requires an instance of a previous resulting model element in order to be able to create a new model element. Instead, it was called with a null value. The original model element was '" + originalModelElement + "'");
		}
		
		
		try {
			result = onCreateModelElement(originalModelElement, previousResultingModelElement);
			
			if (result == null) {
				Message message = ConverterMessage.UNKNOWN_ERROR.getMessageBuilder()
						.modelElement(originalModelElement instanceof ModelElementI ? (ModelElementI) originalModelElement : null)
						.parameters("onCreateModelElement() has returned null instead of an object, original model element:" + originalModelElement + ", previoiusResultingModelElement:" + previousResultingModelElement)
						.build();
				throw new ModelConverterException(message.getMessage());
			}
		} catch (Throwable th) {
			ModelElementI modelElement = null;
			if (th instanceof ModelConverterException && ((ModelConverterException) th).getElement() instanceof ModelElementI) {
				modelElement = (ModelElementI) ((ModelConverterException) th).getElement();
			} else if (originalModelElement instanceof ModelElementI) {
				modelElement = (ModelElementI) originalModelElement;
			}
			
			
			if (th instanceof ModelConverterException) {
				throw th;
			} else {
				Message message = ConverterMessage.UNKNOWN_ERROR_ELEMENT_CONVERTER.getMessageBuilder()
						.modelElement(modelElement)
						.parameters(
								this.getClass().getName(),
								th.getMessage())
						.throwable(th)
						.build();
				throw new ModelConverterException(message.getMessage(), th);
			}
		}
		

		return result;
	}

	/**
	 * @param originalModelElement
	 * @param previousResultingModelElement
	 * @return
	 */
	protected abstract T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement);
	
	/**
	 * @return
	 */
	protected ConverterCache<OriginalModelElementKey, ModelElementI> getConversionCache() {
		return modelConverter.getConversionCache();
	}
	
	/**
	 * @return
	 */
	protected ConverterCache<OriginalModelElementKey, ModelElementI> getCreationCache() {
		return modelConverter.getCreationCache();
	}

	/**
	 * @return
	 */
	protected Set<OriginalModelElementKey> getConverterLocks() {
		return modelConverter.getConverterLocks();
	}

	/**
	 * @return
	 */
	protected Set<OriginalModelElementKey> getContextualConverterLookupTable() {
		return modelConverter.getContextualConverterLookupTable();
	}


	/**
	 * @param modelElement
	 */
	protected void addModelElement(ModelElementI modelElement) {
		this.getModelConverter().addModelElement(modelElement, null);
	}

	/**
	 * @param modelElement
	 * @param namespace
	 */
	protected void addModelElement(ModelElementI modelElement, String namespace) {
		this.getModelConverter().addModelElement(modelElement, namespace);
	}

	/**
	 * Convenience method to get the project model element, which is a singleton per combination of target project and model converter
	 *
	 * @return
	 */
	protected Model getModel() {
		return getModelConverter().getModel();
	}

	/**
	 * @return the previousResultingElementRequired
	 */
	protected boolean isPreviousResultingElementRequired() {
		return previousResultingElementRequired;
	}

	public boolean isIndirectConversionOnly() {
		return indirectConversionOnly;
	}

	public boolean isCreateAndConvertInOneGo() {
		return createAndConvertInOneGo;
	}
	
	protected void addError(String message) {
		getModelConverter().addError(message);
	}
	
	protected void addError(String message, Throwable th) {
		getModelConverter().addError(message, th);
	}
	
	protected void addWarning(String message) {
		getModelConverter().addWarning(message);
	}
	
	protected void addWarning(String message, Throwable th) {
		getModelConverter().addWarning(message, th);
	}
	
	protected void addInfo(String message) {
		getModelConverter().addInfo(message);
	}

	/**
	 * TODO not that this field returning null in some cases is just a temporary solution (mmt 03-Jul-2015)
	 * @return null for those model element converters, that _always_ should do a create-and-convert-in-one-go for indirect conversion 
	 */
	public ModelElementConverterBehavior getModelElementConverterBehavior() {
		return modelElementConverterBehavior;
	}
	
	public boolean isCreateAndConvertInOneGoForIndirectConversion() {
		return modelElementConverterBehavior != null && isCreateAndConvertInOneGo() || modelElementConverterBehavior == null && isIndirectConversionOnly();  
	}

	/**
	 * <p>For filtered elements the indirect conversion is NOT disabled but its generation is prevented.
	 * You can change this behavior in your model element converter by overwriting this method.
	 * 
	 * @param modelElement
	 * @return
	 */
	protected BehaviorForIndirectConversion getBehaviorForIndirectConversion(ModelElementI modelElement) {
		if (isFiltered(modelElement)) {
			return BehaviorForIndirectConversion.CONVERT_BUT_DONT_GENERATE;
		}
		return BehaviorForIndirectConversion.CONVERT_AND_GENERATE;
	}

	/**
	 * @param value
	 * @return
	 */
	public static String removeTrailingSpaces(String value) {
        if (value == null) {
            return null;
        }
        
        int len = value.length();
        for (; len > 0; len--) {
            if (!Character.isWhitespace(value.charAt(len - 1))) {
                break;
            }
        }
        
        if (len < value.length()) {
            return value.substring(0, len);
        }
        
        return value;
    }
	
	/**
	 *
	 */
	public enum BehaviorForIndirectConversion {
		CONVERT_AND_GENERATE,
		DONT_CONVERT,
		CONVERT_BUT_DONT_GENERATE,
		;
	}
}
