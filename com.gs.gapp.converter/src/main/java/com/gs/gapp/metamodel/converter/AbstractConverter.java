package com.gs.gapp.metamodel.converter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.jenerateit.modelconverter.MessageProviderModelConverter;
import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;
import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.analytics.ElementConverterConfiguration;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;
import com.gs.gapp.metamodel.basic.BasicMessage;
import com.gs.gapp.metamodel.basic.GeneratorInfo;
import com.gs.gapp.metamodel.basic.MessageI.MessageBuilder;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementTraceabilityI;
import com.gs.gapp.metamodel.basic.ModelFilterI;
import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.ModelValidatorI.RuleI;
import com.gs.gapp.metamodel.basic.Namespace;
import com.gs.gapp.metamodel.basic.options.ConverterOptions;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionWithValue;



/**
 * This abstract converter class provides
 *
 */
public abstract class AbstractConverter extends MessageProviderModelConverter {


	/**
	 *
	 */
	transient private List<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> allModelElementConverters;

	private final ModelElementCache modelElementCache;

	private ModelConverterOptions options;
	
	private ConverterOptions converterOptions;

	private final Set<OptionDefinition<?>> optionDefinitions = new LinkedHashSet<>();

	private Model model;

	/**
	 * The creation cache ensures that a model element converter creates a new model element only once.
	 */
	private final transient ConverterCache<OriginalModelElementKey,ModelElementI> creationCache =
	    	new ConverterCache<>();
	
	/**
     * The converter cache has the purpose of ensuring that one and the same input element for the conversion is not
     * converted more than once. Otherwise this could happen if a converter calls another converter and that other converter
     * would also be called by the conversion harness.
     *
     * @see AbstractConverter#clientConvert()
     */
    private final transient ConverterCache<OriginalModelElementKey,ModelElementI> conversionCache =
    	new ConverterCache<>();

    private final transient Set<OriginalModelElementKey> converterLocks =
        	new HashSet<>();

    private final transient Set<OriginalModelElementKey> contextualConverterLookupTable =
        	new HashSet<>();
    
    private final transient Map<OriginalModelElementKey, AbstractModelElementConverter<? extends Object,? extends ModelElementI>> delayedConversionLookupTable =
        	new LinkedHashMap<>();


    /**
     * Sometimes you need a model representation of an artifact (e.g. a class) where
     * the model-element/artifact is _not_ already part of the input model. This
     * set is supposed to hold instances of such model elements. It is this class'
     * sub-classes' responsibility to fill this set.
     */
    private final transient Set<ModelElementI> predefinedModelElements = new LinkedHashSet<>();

    /**
     *
     */
    private String notationVersion;

    /**
     *
     */
    private ModelConversionPhase conversionPhase = ModelConversionPhase.START;
    
    private final transient Set<Object> unfilteredModelElements = new LinkedHashSet<>();
    private final transient Set<Object> filteredModelElements = new LinkedHashSet<>();
    private final transient Set<Object> incomingModelElements = new LinkedHashSet<>();
    

	/**
	 * @param modelElementCache
	 */
	public AbstractConverter(ModelElementCache modelElementCache) {
		super();
		if (modelElementCache == null) throw new NullPointerException("parameter modelElementCache must not be null");
		this.modelElementCache = modelElementCache;

//    	addOptionDefinition(new OptionDefinition<String>(PARAMETER_DEFAULT_NAMESPACE, false, ""));
//    	addOptionDefinition(new OptionDefinition<String>(PARAMETER_MODULES_NOT_FOR_GENERATION, false, ""));
	}

	/**
	 * @param originalModelElement
	 * @param converterClass
	 * @return the model element that was the result of a conversion with an instance of the
	 *         given converter class. Note that this method does _not_ perform the actual
	 *         conversion but simplifies the access to the converter cache.
	 */
	ModelElementI getConvertedModelElementFromConversionCache(Object originalModelElement,
	                                               @SuppressWarnings("rawtypes") AbstractModelElementConverter elementConverter) {

		if (originalModelElement == null) throw new NullPointerException("param originalModelElement must not be null");
		if (elementConverter == null) throw new NullPointerException("param elementConverter must not be null");

		OriginalModelElementKey omek =
			new OriginalModelElementKey(originalModelElement, elementConverter.getClass());

	    if (conversionCache.containsKey(omek)) {
	    	return conversionCache.get(omek);
	    } else {
	    	return null;
	    }
	}

	/**
	 * @param originalModelElement the model element that is supposed to be converted
	 * @param converterClass the class that was used for the conversion of the original model element
	 * @param modelElement the conversion result
	 */
	void setConvertedModelElementInConversionCache(Object originalModelElement, @SuppressWarnings("rawtypes") AbstractModelElementConverter elementConverter,
			                             ModelElementI modelElement) {

		if (originalModelElement == null) throw new NullPointerException("param originalModelElement must not be null");
		if (elementConverter == null) throw new NullPointerException("param elementConverter must not be null");
		if (modelElement == null) { throw new NullPointerException("parameter modelElement must not be null"); }

		OriginalModelElementKey omek = new OriginalModelElementKey(originalModelElement, elementConverter.getClass());
		ModelElementI previousModelElement = conversionCache.put(omek, modelElement);

		if (previousModelElement != null && previousModelElement != modelElement) {
			Message message = ConverterMessage.ELEMENT_CONVERSION_CREATES_AMBIGUOUS_RESULT
			    .getMessageBuilder()
			    .modelElement(originalModelElement instanceof ModelElementI ? (ModelElementI) originalModelElement : null)
			    .parameters(previousModelElement.toString(), modelElement.toString(), originalModelElement.toString())
			    .build();
			throw new ModelConverterException(message.getMessage());
		}
	}

	/**
	 * @return the converterCache
	 */
	protected ConverterCache<OriginalModelElementKey, ModelElementI> getConversionCache() {
		return conversionCache;
	}

	/**
	 * @param originalModelElement
	 * @param converterClass
	 * @return the model element that was the result of a conversion with an instance of the
	 *         given converter class. Note that this method does _not_ perform the actual
	 *         conversion but simplifies the access to the converter cache.
	 */
	ModelElementI getConvertedModelElementFromCreationCache(Object originalModelElement, ModelElementI previousResultingModelElement,
	                                               @SuppressWarnings("rawtypes") AbstractModelElementConverter elementConverter) {

		if (originalModelElement == null) throw new NullPointerException("param originalModelElement must not be null");
		if (elementConverter == null) throw new NullPointerException("param elementConverter must not be null");

		OriginalModelElementKey omek =
			new OriginalModelElementKey(originalModelElement, previousResultingModelElement, elementConverter.getClass());

	    if (creationCache.containsKey(omek)) {
	    	return creationCache.get(omek);
	    } else {
	    	OriginalModelElementKey omekWithoutContextualElement =
					new OriginalModelElementKey(originalModelElement, elementConverter.getClass());
	    	if (creationCache.containsKey(omekWithoutContextualElement)) {
	    		return creationCache.get(omekWithoutContextualElement);
	    	}
	    	 
	    	return null;
	    }
	}

	/**
	 * @param originalModelElement the model element that is supposed to be converted
	 * @param converterClass the class that was used for the conversion of the original model element
	 * @param modelElement the conversion result
	 */
	void setConvertedModelElementInCreationCache(Object originalModelElement, ModelElementI previousResultingModelElement,
			                             @SuppressWarnings("rawtypes") AbstractModelElementConverter elementConverter,
			                             ModelElementI modelElement) {

		if (originalModelElement == null) throw new NullPointerException("param originalModelElement must not be null");
		if (elementConverter == null) throw new NullPointerException("param elementConverter must not be null");
		if (modelElement == null) { throw new NullPointerException("parameter modelElement must not be null"); }

		OriginalModelElementKey omek = new OriginalModelElementKey(originalModelElement,
																	previousResultingModelElement,
																	elementConverter.getClass());
		ModelElementI previousModelElement = creationCache.put(omek, modelElement);

		if (previousModelElement != null && previousModelElement != modelElement) {
			MessageBuilder messageBuilder = ConverterMessage.ELEMENT_CONVERSION_CREATES_AMBIGUOUS_RESULT.getMessageBuilder();
			messageBuilder.parameters(previousModelElement.toString(), modelElement.toString(), originalModelElement.toString());
			Message message = messageBuilder.build();
			throw new ModelConverterException(message.getMessage(), originalModelElement);
		}
	}
	
	void addToDelayedConversion(OriginalModelElementKey key, AbstractModelElementConverter<? extends Object, ? extends ModelElementI> modelElementConverter) {
		this.delayedConversionLookupTable.put(key, modelElementConverter);
	}
	
	/**
	 * @return the converterCache
	 */
	protected ConverterCache<OriginalModelElementKey, ModelElementI> getCreationCache() {
		return creationCache;
	}

	/**
	 * clears all caches and lookup tables - needs to be called as the very first step of the whole conversion procedure for a model converter instance
	 */
	protected void clearCaches() {
		if (this.creationCache != null) {
			this.creationCache.clear();
		}
		if (this.conversionCache != null) {
			this.conversionCache.clear();
		}
		if (this.contextualConverterLookupTable != null) {
			this.contextualConverterLookupTable.clear();
		}
		if (this.converterLocks != null) {
			this.converterLocks.clear();
		}
		if (this.delayedConversionLookupTable != null) {
			this.delayedConversionLookupTable.clear();
		}
	}


	/**
	 * @return the predefinedModelElements
	 */
	protected final Set<ModelElementI> getPredefinedModelElements() {
		return Collections.unmodifiableSet(predefinedModelElements);
	}

	/**
	 * @param converterClassArr
	 * @return a set of model element converters that are responsible for the conversion
	 *         of the given original model element
	 */
	public final Set<AbstractModelElementConverter<? extends Object,? extends ModelElementI>>
	                 getModelElementConverters(Object originalModelElement, Class<?>... converterClassArr) {
		return getModelElementConverters(originalModelElement, null, null, converterClassArr);
	}
	/**
	 * @param originalModelElement
	 * @param resultClass
	 * @param existingResultingModelElement
	 * @param converterClassArr
	 * @return a set of model element converters that are responsible for the conversion
	 *         of the given original model element
	 */
	public final Set<AbstractModelElementConverter<? extends Object,? extends ModelElementI>>
	                 getModelElementConverters(Object originalModelElement, Class<? extends ModelElementI> resultClass, ModelElementI existingResultingModelElement, Class<?>... converterClassArr) {
		if (originalModelElement == null) throw new NullPointerException("parameter 'originalModelElement' must not be null");
		
		Collection<Class<?>> converterClassCollection = null;
		if (converterClassArr != null && converterClassArr.length > 0) {
			converterClassCollection = Arrays.asList(converterClassArr);
		}

		Set<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> result =
			new LinkedHashSet<>();

		// --- identify potential model element converters for the given parameters
		for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> modelElementConverter : getAllModelElementConverters()) {
			if (modelElementConverter.isPreviousResultingElementRequired() && existingResultingModelElement == null) continue; // optimization

			if (converterClassCollection != null && converterClassCollection.size() > 0 && !converterClassCollection.contains(modelElementConverter.getClass())) {
				continue;  // no need to call isResponsibleFor(), this is one way to avoid a stack overflow in case convertWithOtherConverter() gets called from isResponsibleFor()
			}
			if (modelElementConverter.isResponsibleFor(originalModelElement, existingResultingModelElement, resultClass)) {
				result.add(modelElementConverter);
			}
		}

		// --- shrink the list of potential converters by taking inheritance and other configurations into account
		if (converterClassArr == null || converterClassArr.length == 0) {
		    result = removeOverwrittenConverters(result);
		} else {
			for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> converter : new LinkedHashSet<>(result)) {
				if (!converterClassCollection.contains(converter.getClass())) {
					result.remove(converter);
				}
			}
		}
		result = removeExcludedConverters(result);

		if (result.size() > 1) {
			Set<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> convertersToBeRemoved =
					new LinkedHashSet<>();

			// finally, remove converters with identical target class but different source classes that inherit from each other
			AbstractModelElementConverter<? extends Object,? extends ModelElementI> hiddenElementConverter = null;
			for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> converter1 : new LinkedHashSet<>(result)) {
				for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> converter2 : new LinkedHashSet<>(result)) {
					hiddenElementConverter = converter1.getHiddenModelElementConverter(converter2);
					if (hiddenElementConverter != null) {
						convertersToBeRemoved.add(hiddenElementConverter);
					}
				}
			}
			result.removeAll(convertersToBeRemoved);
		}

		if (resultClass != null) {
			for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> converter : new LinkedHashSet<>(result)) {
                if (resultClass.isAssignableFrom(converter.getTargetClass()) == false) {
                	result.remove(converter);
                }
			}
		}


		return result;
	}

	/**
	 * If more than one converter object is responsible for the conversion of an original model element and
	 * one of those converter objects inherits from the other one, then the converter that is the parent
	 * of the other one is getting removed from the list of converters.
	 *
	 * @param list of converters
	 * @return set of converters with overwritten converters having been removed from the set
	 */
	private Set<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> removeOverwrittenConverters(Set<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> converters) {
		Set<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> convertersToBeRemoved = new LinkedHashSet<>();

		int firstCounter = 0;
		for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> modelElementConverter : converters) {
			int secondCounter = 0;
			for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> otherModelElementConverter : new LinkedHashSet<>(converters)) {
				if (modelElementConverter != otherModelElementConverter && secondCounter > firstCounter) {
					if (modelElementConverter.getClass().isInstance(otherModelElementConverter)) {
						convertersToBeRemoved.add(modelElementConverter);
					} else if (otherModelElementConverter.getClass().isInstance(modelElementConverter)) {
						convertersToBeRemoved.add(otherModelElementConverter);
					}
				}
				secondCounter++;
			}
			firstCounter++;
		}

		converters.removeAll(convertersToBeRemoved);

		return converters;
	}

	/**
	 * Makes sure that model converter classes that are marked to be excluded through the implementation of
	 * getExcludedModelElementConverters() in GSSPAbstractModelElementConverter are getting removed from the
	 * list of all model element converters.
	 *
	 * @param converters
	 * @return
	 */
	@SuppressWarnings("unlikely-arg-type")
	private Set<AbstractModelElementConverter<? extends Object,? extends ModelElementI>>
	            removeExcludedConverters(Set<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> converters) {

		// --- first round of exclusion handling
		Set<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> convertersToBeRemoved = new LinkedHashSet<>();
		Set<java.lang.Class<?>> excludedConverterTypes = new LinkedHashSet<>();
		for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> modelElementConverter : converters) {
			Set<Class<?>> excludedModelElementConverters =
				modelElementConverter.getExcludedModelElementConverters();
			if (excludedModelElementConverters != null) {
			    excludedConverterTypes.addAll(excludedModelElementConverters);
			}
		}

		for (java.lang.Class<?> excludedConverterType : excludedConverterTypes) {
			for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> modelElementConverter : converters) {
				if (excludedConverterType.isAssignableFrom(modelElementConverter.getClass())) {
					convertersToBeRemoved.add(modelElementConverter);
				}
			}
		}

		converters.removeAll(convertersToBeRemoved);


		// --- second round of exclusion handling
		List<Class<AbstractModelElementConverter<?,?>>> listOfExcludedConverterTypes = new ArrayList<>();
		LinkedHashMap<Class<AbstractModelElementConverter<?,?>>, List<Class<AbstractModelElementConverter<?,?>>>> exclusions = getModelElementConverterExclusions();
		for (Class<AbstractModelElementConverter<?,?>> key : exclusions.keySet()) {
			if (converters.contains(key)) {
				listOfExcludedConverterTypes.addAll(exclusions.get(key));
			}
		}

		convertersToBeRemoved = new LinkedHashSet<>();
		for (Class<AbstractModelElementConverter<?,?>> excludedConverterType : listOfExcludedConverterTypes) {
			for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> modelElementConverter : converters) {
				if (excludedConverterType == modelElementConverter.getClass()) {
					convertersToBeRemoved.add(modelElementConverter);
				}
			}
		}

		converters.removeAll(convertersToBeRemoved);

		return converters;
	}

	/**
	 * @param originalModelElement
	 * @param expectedType
	 * @return
	 */
	public final Set<ModelElementI> getConvertedModelElements(Object originalModelElement, Class<?> expectedType) {
		if (originalModelElement == null) throw new NullPointerException("parameter originalModelElement must not be null");
		if (expectedType == null) throw new NullPointerException("parameter expectedType must not be null");

		Set<ModelElementI> result = new LinkedHashSet<>();

		for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> modelElementConverter : getModelElementConverters(originalModelElement)) {
			// note that the following create() call does not always convert but instead accesses the converter cache
			ModelElementI modelElement = modelElementConverter.create(originalModelElement);
			if (modelElement != null) {
				if (expectedType.isAssignableFrom(modelElement.getClass())) {
		            result.add(modelElement);
				} else {
					// This case is not a mistake, thus we do not throw an exception here (mmt 8-Sep-2009)
//					throw new RuntimeException("ERROR: type " + expectedType.getName() + " is not assignable from " + modelElement.getClass().getName());
				}
			}
		}

		return result;
	}

	/**
	 * @param originalModelElement
	 * @param expectedType
	 * @return
	 */
	public final ModelElementI getSingleConvertedModelElement(Object originalModelElement, Class<?> expectedType) {
		ModelElementI result = null;
		for (ModelElementI modelElement : getConvertedModelElements(originalModelElement, expectedType)) {
			if (result != null) {
				throw new ModelConverterException("more than one conversion result for model element " + originalModelElement + " and expected type " + expectedType.getName());
			}
            result = modelElement;
		}

		return result;
	}

	/**
	 * Checks whether the model converter's version is compatible with the notation version
	 * of the model that serves as the input for the model conversion.
	 * TODO revise this method, maybe make it private, as of 27-Jun-2012 this method does not get called
	 */
	protected void validateConversionCompatibility() {
		// TODO make this check being more general (e.g. in separate method), also use compatibility matrix instead of using simple equality (17-Aug-2009)
		if (StringTools.isEmpty(getInputMetaModelVersion())) {
			throw new ModelConverterException("No notation version found on given input model. Model notation/converter compatibility check cannot be made.");
		} else {
			if (StringTools.isEmpty(getVersion())) {
				// this happens when the converter classes are not packaged inside jar files or there is no MANIFEST.MF in the jar file or the MANIFEST.MF file doesn't have implementation version specified
			} else {
				if (!getInputMetaModelVersion().equals(getVersion())) {
				    throw new ModelConverterException("version of modeling language notation (" + getInputMetaModelVersion() + ") is not compatible with model converter version (" + getVersion() + ")");
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.modelconverter.MessageProviderModelConverter#clientConvert(java.util.Collection, org.jenerateit.modelconverter.ModelConverterOptions)
	 */
	@Override
	protected Set<Object> clientConvert(Collection<?> rawElements,
			ModelConverterOptions options) throws ModelConverterException {
		
		try {
			Set<Object> result = null;
			Set<Object> normalizedElements = new LinkedHashSet<>();
	
			this.clearCaches();
			this.clearModelElementCache();
			
			// keep the incoming model elements in mind in order to be able to query this information later on (mmt 31-Mar-2019)
			incomingModelElements.addAll(rawElements);
	
			this.options = options != null ? options : new ModelConverterOptions(0);
			// --- 0. initialize options
			initOptions();
			
			// --- 1. Filter some model elements
			this.conversionPhase = ModelConversionPhase.FILTER;
			Collection<?> rawElementsAfterFilter = performElementFilter(new ArrayList<>(rawElements));
			
			// --- 2. Perform optional model normalization (mandatory elements, owned elements ...)
			this.conversionPhase = ModelConversionPhase.NORMALIZATION;
			Set<Object> resultOfNormalization = performModelNormalization(new ArrayList<>(rawElementsAfterFilter));
			if (resultOfNormalization != null) {
				normalizedElements.addAll(resultOfNormalization);
			}
			// - make sure that the collection of resulting elements for the given elements does not contain any
			// - resulting elements that got linked by a previous execution of the model conversion
			clearElementTree(normalizedElements);
	
			// --- 3. Perform optional version compatibility check (model <---> converter)
			this.conversionPhase = ModelConversionPhase.COMPATIBILITY_CHECK;
			performCompatibilityCheck(normalizedElements);
	
			// --- 4. Perform provisioning of predefined model elements
			this.conversionPhase = ModelConversionPhase.ELEMENT_PREDEFINITION;
			performElementPredefinition(rawElementsAfterFilter);
			
			// --- 5. Perform model validation before converting anything
			performValidationBeforeConversion(normalizedElements);
	
			// ---------------------------------------------------------------------------------------------------------
			// -------------------------- this is the main task of a model converter -----------------------------------
			// --- 6. Perform element conversion
			this.conversionPhase = ModelConversionPhase.CONVERSION;
			performModelElementConversion(normalizedElements);
			// ---------------------------------------------------------------------------------------------------------
	
			// --- 7. Perform model consolidation
			this.conversionPhase = ModelConversionPhase.CONSOLIDATION;
			result = performModelConsolidation(normalizedElements);
	
			ensureModelSingleton(result);
	
			// --- 8. Perform model validation
			this.conversionPhase = ModelConversionPhase.VALIDATION;
			performModelValidation(result);
	
			return result;
		
		} catch (Throwable th) {
			th.printStackTrace();
			if (th instanceof ModelConverterException) {
				throw th;
			} else {
				Message message = ConverterMessage.UNKNOWN_ERROR
				    .getMessageBuilder()
				    .throwable(th)
				    .parameters(th.getMessage())
				    .build();
				addMessage(message);
			}
		}
		
		return Collections.emptySet();
	}

	/**
	 * @param normalizedElements
	 */
	private final void performValidationBeforeConversion(Set<Object> normalizedElements) {
		onPerformValidationBeforeConversion(normalizedElements);
		if (getErrors().size() > 0) {
			Message errorMessage = ConverterMessage.MODEL_VALIDATION_ERROR.getMessageBuilder()
					.parameters(this.getClass().getName()).build();
			throw new ModelConverterException(errorMessage.getMessage());
		}
	}

	/**
	 * The default implementation of the model validation before the the model conversion
	 * ensures that there is at most one element of type {@link Model} in the collection
	 * of in put model elements.
	 * 
	 * When you overwrite this method, instances of {@link ModelFilterI} can be used
	 * to perform the filtering task in a reusable manner.
	 * 
	 * @param normalizedElements
	 */
	protected void onPerformValidationBeforeConversion(Set<Object> normalizedElements) {
		Collection<RuleI> occurrenceRules = getConverterOptions().getOccurrenceRules();
		if (occurrenceRules != null && occurrenceRules.size() > 0) {
			Collection<Message> validationResult = new ModelValidatorI.DefaultValidator().validate(normalizedElements, occurrenceRules);
			addMessages(validationResult);
			if (getErrors().size() > 0) {
				throw new ModelConverterException("validation of number of occurrencies found errors");
			}
		}
		
		Set<Object> collectedInstancesOfModelType =
				normalizedElements.stream().filter(element -> element instanceof Model).collect(Collectors.toSet());
		if (collectedInstancesOfModelType.size() > 1) {
			throw new ModelConverterException("a set of input elements for a model converter must include at most one instance of type 'Model', but " + collectedInstancesOfModelType.size() + " were found:" + collectedInstancesOfModelType);
		}
	}

	/**
	 * The default filter mechanism limits the model elements of certain types to only the ones
	 * that match a given name (or regexp). Those filter rules can be specified by the converter
	 * option {@link ConverterOptions#PARAMETER_FILTER_BY_NAME_AND_TYPE}.
	 * 
	 * @param rawElements
	 * @return either all elements that were passed as 'rawElements' or a subset of them, in case a filter got applied
	 */
	private final Collection<?> performElementFilter(ArrayList<?> rawElements) {
		Collection<?> result = onPerformElementFilter(rawElements);
		if (result != null && rawElements != null && result.size() != rawElements.size()) {
			// something got filtered => keep the set of filtered elements in mind in order to be able to use this information in convertWithOtherConverter() calls.
			for (Object aRawElement : rawElements) {
				if (!result.contains(aRawElement)) {
					filteredModelElements.add(aRawElement);
				}
			}
			// also provide the opposite part of the raw elements, which represent elements that _are_ further processed by the model converter
			unfilteredModelElements.addAll(rawElements);
			unfilteredModelElements.removeAll(filteredModelElements);
		}
		
		return result;
	}
	
	/**
	 * This method allows to make the collection of model elements smaller.
	 * In principle, a filter can have two strategies:
	 * - remove every model element that doesn't fulfill a certain criteria (exclusion)
	 * - remove every model element that does fulfill a certain criteria (inclusion)
	 * 
	 * @param rawElements
	 * @return if not overwritten, this method returns the same set of elements that gets passed to it
	 */
	protected Collection<?> onPerformElementFilter(ArrayList<?> rawElements) {
		Collection<ModelFilterI.RuleI> filterRules = getConverterOptions().getFilterByNameAndTypeRules();
		if (filterRules != null && filterRules.size() > 0) {
			Collection<?> filterResult = new ModelFilterI.DefaultFilter().filter(rawElements, filterRules);
			if (filterResult != null) {
			    return filterResult;
			}
		}
		
		return rawElements;
	}

	/**
	 * 
	 */
	private final void initOptions() {
        onInitOptions();		
	}
	
	/**
	 * Override this method in order to initialize converter specific objects to handle options.
	 */
	protected void onInitOptions() {
		this.converterOptions = new ConverterOptions(getOptions());
	}

	/**
	 * The result of a model converter must only hold one single instance of type 'Model', namely the on
	 * that you get when you call <converter>.getModel(). This method cleans up the result set to enforce this rule.
	 *
	 * @param result
	 */
	private void ensureModelSingleton(Set<Object> result) {
		if (result != null) {
			Set<Object> modelsToBeRemoved = new HashSet<>();
			for (Object obj : result) {
				if (obj instanceof Model && obj != this.model) {
					modelsToBeRemoved.add(obj);
				}
			}

			result.removeAll(modelsToBeRemoved);
		}
	}

	private void clearElementTree(Set<Object> normalizedElements) {
		for (Object element : normalizedElements) {
			if (element instanceof ModelElementTraceabilityI) {
				// clear any remaining resulting model elements that stem from a previous transformation or transformation-step
				ModelElementTraceabilityI traceableModelElement = (ModelElementTraceabilityI) element;
				traceableModelElement.clearResultingElements(this.getModelConverterId());
			}
		}
	}

	/**
	 * @param result
	 */
	private final void performModelValidation(Set<Object> result) {
        onPerformModelValidation(result);
	}

	/**
	 * By implementing this method you can ensure that the given result set
	 * of model elements represents a valid model. If you find an invalid
	 * model element in the result set, then you can throw a
	 * ModelConverterException or you can simply call addError(), addWarning()
	 * and addInfo() methods to let JenerateIT know about the issues.
	 *
	 * @param result
	 */
	protected void onPerformModelValidation(Set<Object> result) {
		// there is no default implementation - as this feature is not always needed, we do not provide an abstract method but an empty implementation
	}

	/**
	 *
	 */
	private final void performElementPredefinition(Collection<?> rawElements) {
		if (this.predefinedModelElements.size() == 0) {
			Set<ModelElementI> predefinedModelElements = onPerformElementPredefinition(rawElements);
			if (predefinedModelElements != null) {
		        this.predefinedModelElements.addAll(predefinedModelElements);
		    }
		}
	}

	/**
	 * This is the place to manually define model elements that are required during conversion
	 * and also afterwards but are not part of the source model and are not accessible
	 * from the source model's model elements.
	 */
	protected Set<ModelElementI> onPerformElementPredefinition(Collection<?> rawElements) {
		Set<ModelElementI> result = new LinkedHashSet<>();

		Model previousModel = null;
		if (rawElements != null) {
			for (Object rawElement : rawElements) {
				if (rawElement instanceof Model) {
					previousModel = (Model) rawElement;
					break;
				}
			}
		}
		// --- there is always one single Model instance per model converter, where this Model instance holds references to all elements that get created
		// --- or passed through this model converter
		this.model = new Model(this.getClass().getSimpleName());  // TODO use name of chosen generator or name of target project instead (mmt 13-May-2013)
		
		// --- provide information about the generator and the generator version
		GeneratorInfo generatorInfo = null;
		if (this.getOptions().containsKey("vd.generator.id") &&
				this.getOptions().containsKey("vd.generator.version.short")) {
			generatorInfo = new GeneratorInfo(this.getOptions().get("vd.generator.id").toString(),
					this.getOptions().get("vd.generator.version.short").toString());
		} else {
		    generatorInfo = getConverterOptions().getGeneratorInfo();
		}
		
		if (generatorInfo != null) {
			this.model.setGeneratorInfo(generatorInfo);
		}
//		ConversionDetails conversionDetails = new ConversionDetails(this.getClass().getName(), null, this.getOptions());  //this must not be done, otherwise the element conversion tree does not show the model elements of type "Model"
//		this.model.setConversionDetails(conversionDetails);
		if (previousModel != null) {
			model.setOriginatingElement(previousModel);
		}
		result.add(model);
		this.getModelElementCache().add(model);

		// --- additional create the default namespace
		Namespace defaultNamespace = new Namespace(getConverterOptions().getNameOfDefaultNamespace());
		model.setDefaultNamespace(defaultNamespace);
		this.getModelElementCache().add(defaultNamespace);

		/* TODO this might be done in a different way (element converter)
		// --- collect element converter configuration
		ModelConverterConfiguration modelConverterConfiguration =
				new ModelConverterConfiguration(this.getClass().getName());
		for (AbstractModelElementConverter<? extends Object,? extends ModelElement> elementConverter : getAllModelElementConverters()) {
			boolean isIsResponsibleOverwritten = false;
			Class<?> sourceClass = elementConverter.getSourceClass();
			Class<?> targetClass = elementConverter.getTargetClass();
			@SuppressWarnings("unused")
			Method method = null;

			try {
				method = elementConverter.getClass().getDeclaredMethod("isResponsibleFor");
				isIsResponsibleOverwritten = true;
			} catch (SecurityException e) {
				// ignore this exception
			} catch (NoSuchMethodException e) {
				// ignore this exception
			}

			ElementConverterConfiguration elementConverterConfiguration =
					new ElementConverterConfiguration(sourceClass, targetClass, isIsResponsibleOverwritten);
			modelConverterConfiguration.addElementConverterConfiguration(elementConverterConfiguration);
		}

		result.add(modelConverterConfiguration);
		this.getModelElementCache().add(modelConverterConfiguration);
		*/

		return result;
	}

	/**
	 *
	 *
	 * @param normalizedElements
	 * @return
	 */
	private final Set<Object> performModelConsolidation(Set<?> normalizedElements) {
		if (com.gs.gapp.metamodel.basic.ModelElement.isAnalyticsMode()) {
			// --- collect all possible transformation step options in order to be able to analyze their usage during generator development in the VD IDE
			if (this.model != null) {
				for (OptionDefinitionWithValue optionDefinitionWithValue : getConverterOptions().getOptionDefinitionsWithValue()) {
					this.model.addElement(optionDefinitionWithValue);
				}
			}
		}
		Collection<ModelElementI> elementsHandledByElementConverters = getCreationCache().values();
		Set<Object> consolidatedElements = onPerformModelConsolidation(normalizedElements);
		
		if (com.gs.gapp.metamodel.basic.ModelElement.isAnalyticsMode()) {
			if (consolidatedElements != null) {
				// Special treatment for elements that were not created within this model converter
				// but within a leading model converter. It is possible that the consolidated
				// elements contain such elements when in onPerformModelConsolication() previously
				// available elements have been added to the set of elements (kind of a "pass-through"
				// functionality).
				// This special treatment here is adding information that is later used
				// by the generation analytics functionality, to create the model element
				// converter tree.
				TransformationStepConfiguration transformationStepConfiguration = null;
				ElementConverterConfigurationTreeNode rootNode = null;
				for (Object anElement : consolidatedElements) {
					if (anElement instanceof ElementConverterConfigurationTreeNode) {
						ElementConverterConfigurationTreeNode elementConverterConfigurationTreeNode = (ElementConverterConfigurationTreeNode) anElement;
						if (elementConverterConfigurationTreeNode.getElementConverterConfiguration() == null) {
							// identified the root node
							rootNode = elementConverterConfigurationTreeNode;
						}
					} else if (anElement instanceof TransformationStepConfiguration) {
						transformationStepConfiguration = (TransformationStepConfiguration) anElement;
					}
				}
				
				if (rootNode != null && transformationStepConfiguration != null) {
					for (Object anElement : consolidatedElements) {
						if (anElement instanceof ModelElementI) {
							ModelElementI modelElement = (ModelElementI) anElement;
							if (modelElement.getConversionDetails() != null &&
								!elementsHandledByElementConverters.contains(modelElement) &&  // any element that got created (or passed through) by an element converter are not relevant here
								!modelElement.getConversionDetails().getModelConverterClassName().equals(this.getClass().getName())) {
								// found a model element in the result where that model element got created by a model element converter,
								// but not within the execution of the current model converter.
								
								// --- creating a pseudo element converter configuration (in fact, there is no real PASSTHROUGH element converter)
								ElementConverterConfiguration elementConverterConfiguration =
										new ElementConverterConfiguration(modelElement.getClass(), modelElement.getClass(),
										                                  modelElement.getClass().getSimpleName() + "-PASSTHROUGH",
										                                  modelElement.getClass().getSimpleName() + "-PASSTHROUGH",
										                                  false);
								
								
								// --- finally add missing ElementConverterConfigurationTreeNode instances
								for (ElementConverterConfigurationTreeNode potentialLeadingNode : transformationStepConfiguration.getParentConfiguration().getTreeNodes()) {
									
									// TODO what to do if there are several model converters in a row that pass through the same model elements? (mmt 14-Nov-2018)
									if (modelElement.getConversionDetails().getModelElementConverterClassname().equals(potentialLeadingNode.getElementConverterConfiguration().getQualifiedElementConverterClassName()) &&
										potentialLeadingNode.branchContainsElementConverterConfiguration(elementConverterConfiguration) == false) {
										ElementConverterConfigurationTreeNode newNode =
								    			new ElementConverterConfigurationTreeNode(elementConverterConfiguration,
								    					                                  elementConverterConfiguration.getModelConverterConfiguration());
								    	newNode.setParent(potentialLeadingNode);
								    	transformationStepConfiguration.addTreeNode(newNode);
								    	
								    	transformationStepConfiguration.addElementConverterConfiguration(elementConverterConfiguration);
								    	
								    	break;
									}
								}
							}
						}
					}
				}
			}
		}
		
		// This was used for easier debugging, uncomment it when you need to debug this again. (mmt 28-Jun-2022)
//		final Set<Class<?>> filteredMetaTypes = getFilteredMetaTypes();
//		if (filteredMetaTypes != null && !filteredMetaTypes.isEmpty()) {
//			System.out.println(this.getClass().getName() + ", elements in result before filtering: " + consolidatedElements.size());
//			consolidatedElements = consolidatedElements.stream()
//			    .filter(element -> !filteredMetaTypes.contains(element.getClass()))
//			    .collect(Collectors.toSet());
//			System.out.println(this.getClass().getName() + ", elements in result after filtering: " + consolidatedElements.size());
//		}
		
		return consolidatedElements;
	}
	
	protected Set<Class<?>> getFilteredMetaTypes() {
		return Collections.emptySet();
	}


	/**
	 * Determines, which elements should be the result of the model conversion. By
	 * default, all elements of the converter-cache are getting returned. By overwriting
	 * this method, you can affect the result set of the model conversion. For example you
	 * can avoid certain model element being part of the result set. Another example would be
	 * to only let those element being part of the result set that fulfill certain criteria
	 * in its corresponding source element in the set of normalized elements that are getting
	 * passed to this method.
	 * 
	 * 
	 * Note: The normalizedElements are the incoming normalized Elements of the converter. Only
	 * the call to <code>super.onPerformModelConsolidation(normalizedElements)</code> returns the
	 * resulting elements.
	 *
	 * @param normalizedElements
	 * @return
	 */
	protected Set<Object> onPerformModelConsolidation(
			Set<?> normalizedElements) {
		Set<Object> result = new LinkedHashSet<>();

		if (getCreationCache() != null) {
			for (ModelElementI modelElement : getCreationCache().values()) {
				if (!(modelElement instanceof Model)) {
					result.add(modelElement);
				}
				if (getModel() != modelElement) {
				    getModel().addElement(modelElement);
				}
			}
		}

		if (getModelElementCache() != null) {
			for (ModelElementI modelElement : getModelElementCache().getAllModelElements()) {
				if (!(modelElement instanceof Model)) {
					result.add(modelElement);
				}
				if (getModel() != modelElement) {
				    getModel().addElement(modelElement);
				}
			}
		}

		// --- finally, we have to add this converter's model element of type 'Model' in order to make it available for element converters in subsequent model converters
		// --- note that there must be only on single 'Model' instance in the result set
		if (getModel() != null) {
		    result.add(getModel());
		}

		/* TODO this might be done in a different way (element converter)
		// --- link this converter's configuration element with the previous' converter's configuration element
		for (Object normalizedElement : normalizedElements) {
			if (normalizedElement instanceof ModelConverterConfiguration) {
				ModelConverterConfiguration previousConverterConfiguration = (ModelConverterConfiguration) normalizedElement;
				ModelConverterConfiguration thisConverterConfiguration =
						(ModelConverterConfiguration) getModelElementCache().findModelElement(getClass().getName(), ModelConverterConfiguration.class);
				if (thisConverterConfiguration != null && previousConverterConfiguration != null) {
                    thisConverterConfiguration.setParentConfiguration(previousConverterConfiguration);
				}
				break;
			}
		}
		*/

        return result;
	}



	/**
	 * @param modelElements
	 * @return
	 */
	private void performModelElementConversion(Set<?> modelElements) {
		onPerformModelElementConversion(modelElements);
	}

	/**
	 * This method is the central point from where the model conversion is triggered and controlled.
	 * Here model element converters are taken and the given set of model elements is
	 * getting passed to them in order to perform all conversion steps.
	 * Finally, converted model elements are added to the converter cache.
	 *
	 * Note that the algorithm takes the order of model element converters into account.
	 *
	 * @param modelElements
	 */
	protected void onPerformModelElementConversion(Set<?> modelElements) {

		Map<AbstractModelElementConverter<? extends Object,? extends ModelElementI>, Set<Object>> converterToElementMap =
			new LinkedHashMap<>();


        // --- check the element converters and the model elements in order to organize them in a map for simplification of the subsequent processing
		for (Object originalModelElement : modelElements) {
			if (originalModelElement == null) continue; // AMC-15 ignore null elements to make conversion more resilient (mmt 17-Mar-2021)
			
			Set<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> modelElementConverters =
				getModelElementConverters(originalModelElement);

			if (modelElementConverters != null) {
				for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> modelElementConverter : modelElementConverters) {
					
					if (modelElementConverter.isIndirectConversionOnly()) continue;  // ignore this element converter - it will be indirectly calld through convertWithOtherConverter()
					
					Set<Object> modelElementsPerConverter = converterToElementMap.get(modelElementConverter);
					if (modelElementsPerConverter == null) {
						modelElementsPerConverter = new LinkedHashSet<>();
						converterToElementMap.put(modelElementConverter, modelElementsPerConverter);
					}
					modelElementsPerConverter.add(originalModelElement);
				}
			}
		}

		// --- finally, process all model element converters
		List<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> modelElementConverters =
			getAllModelElementConverters();

		for (int pass=1; pass<3; pass++) {
			for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> modelElementConverter : modelElementConverters) {
				if (modelElementConverter.isPreviousResultingElementRequired()) continue; // optimization that allows the usage of an element converter to be restricted to call of convertWithOtherConverter()
	
				Set<?> modelElementsPerConverter = converterToElementMap.get(modelElementConverter);
				if (modelElementsPerConverter != null) {
					for (Object originalModelElement : modelElementsPerConverter) {
						OriginalModelElementKey omek =
							new OriginalModelElementKey(originalModelElement, modelElementConverter.getClass());
						if (getCreationCache().containsKey(omek)) {
							// skip this element, it already got created with the given converter
							if (pass == 2 && (modelElementConverter.isCreateAndConvertInOneGo() == false)) {
								// second pass, now we can execute the rest of the conversion
								modelElementConverter.convert(originalModelElement, getCreationCache().get(omek));
							}
						} else {
							ModelElementI modelElement = modelElementConverter.create(originalModelElement);
							// The convert method could have added the model element and the
							// converted object to the conversion cache already. For this reason
							// we always have to check, whether the conversion cache already has
							// an appropriate mapping.
							if (modelElement != null && !getCreationCache().containsKey(omek)) {
							    getCreationCache().put(omek, modelElement);
//							    System.out.println("creation cache now has " + getCreationCache().size() + " elements");
							}
							
							if (modelElementConverter.isCreateAndConvertInOneGo()) {
								modelElementConverter.convert(originalModelElement, modelElement);
							}
						}
					}
				}
			}//all model element converters
		}//2 passes in order to be able to handle separated creation and conversion

		// --- finally, execute delayed conversions
		while (delayedConversionLookupTable.size() > 0) {
			Map<OriginalModelElementKey, AbstractModelElementConverter<? extends Object,? extends ModelElementI>> originalLookupTableContent = new LinkedHashMap<>(delayedConversionLookupTable);
			delayedConversionLookupTable.clear();
			for (OriginalModelElementKey key : originalLookupTableContent.keySet()) {
				AbstractModelElementConverter<? extends Object,? extends ModelElementI> modelElementConverter = originalLookupTableContent.get(key);
				
				ModelElementI alreadyCreatedModelElement = getConvertedModelElementFromCreationCache(key.getOriginalModelElement(), (ModelElementI) key.getContextualModelElement(), modelElementConverter);
				if (alreadyCreatedModelElement != null) {
				    modelElementConverter.convert(key.getOriginalModelElement(), alreadyCreatedModelElement, (ModelElementI) key.getContextualModelElement());
				} else {
					throw new ModelConverterException("found an attempt to execute a delayed conversion where the resulting model element seems to be not yet available (not yet created, not available on the creation cache). original element:" + key.getOriginalModelElement() + ", converter:" + modelElementConverter + ", contextual model element:" + key.getContextualModelElement());
				}
			}
		}
	}

	/**
	 * TODO this may be extended by the application of a compatibility matrix (mmt 26-Nov-2010)
	 */
	private final void performCompatibilityCheck(Set<?> elements) {
        onPerformCompatibilityCheck(elements);
	}

	/**
	 * This methods lets you use information in
	 * - the set of normalized elements
	 * - the model-converter configuration to
	 * - this instance of model converter
	 * to perform checks that find out, whether the given information
	 * is compatible with each other.
	 */
	protected void onPerformCompatibilityCheck(Set<?> elements) {
		// there is no default implementation - as this feature is not always needed, we do not provide an abstract method but an empty implementation
	}

	/**
	 * @param rawElements
	 * @return
	 */
	private final Set<Object> performModelNormalization(Collection<Object> originalElements) {
		Set<Object> result = new LinkedHashSet<>();
        Set<Object> mandatoryElements = findMandatoryElements(originalElements);
		Set<Object> normalizedSetForSelectedElements = null;
		Set<Object> normalizedSetForMandatoryElements = null;

        if (originalElements != null) {
		    normalizedSetForSelectedElements = onPerformModelNormalization(originalElements);
		    if (normalizedSetForSelectedElements != null) {
		        result.addAll(normalizedSetForSelectedElements);
		    }
		}

		if (mandatoryElements != null) {
		    normalizedSetForMandatoryElements = onPerformModelNormalization(new ArrayList<>(mandatoryElements));
		    if (normalizedSetForMandatoryElements != null) {
		        result.addAll(normalizedSetForMandatoryElements);
		    }
		}

		return result;
	}


	/**
	 * @return
	 */
	public final List<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> getAllModelElementConverters() {
		if (this.allModelElementConverters == null) {
			this.allModelElementConverters = new ArrayList<>();
			List<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> converters = onGetAllModelElementConverters();
			if (converters != null) {
			    this.allModelElementConverters.addAll(converters);
			}
		}
		return this.allModelElementConverters;
	}

	/**
	 * @return all model element converter instances that are part of this model converter
	 */
	protected abstract List<AbstractModelElementConverter<? extends Object,? extends ModelElementI>> onGetAllModelElementConverters();

	/**
	 * @param originalModelElement
	 * @return
	 */
	public boolean isIgnored(Object originalModelElement) {
		return false;
	}
	
	/**
	 * @return
	 */
	public static boolean isDevelopmentMode() {
		String developmentMode = System.getProperty("org.jenerateit.server.developmentMode");
		return (developmentMode != null && developmentMode.length() > 0 && developmentMode.equalsIgnoreCase("true"));
	}

	/**
	 * You can overwrite this method to change/increase the set of elements
	 * that are in a later step being used as input for the model element conversion
	 * procedure. You can for example collect all elements that are contained within
	 * a given package/module model element and return all of them, as being the normlized set
	 * of model elements.
	 *
	 * @param rawElements
	 * @return
	 */
	protected Set<Object> onPerformModelNormalization(Collection<Object> rawElements) {
        Set<Object> result = new LinkedHashSet<>();
        if (rawElements != null) {
        	for (Object rawElement : rawElements) {
        		if (rawElement == null) continue; // AMC-15 ignore null elements to make conversion more resilient (mmt 17-Mar-2021)
        		
        	    result.add(rawElement);
        	}
        }
        return result;
	}


	/**
	 * This method uses the passed collection of model elements to
	 * identify model elements that are required to be in the set of
	 * normalized elements. If you overwrite this method, you typically
	 * use one of the given elements and access other elements from there
	 * in order to find and identify mandatory elements that are not yet part
	 * of the given collection of elements.
	 *
	 * @param rawElements
	 * @return
	 */
	protected Set<Object> findMandatoryElements(Collection<Object> rawElements) {
		Set<Object> result = new LinkedHashSet<>();
		
		if (!doRawElementsContainModelInstance(rawElements)) {
//		    result.add( new Model() );   commented this since a Model instance is created in AbstractConverter already (mmt 28-Aug-2018)  
		}
		
		return result;
	}
	
	/**
	 * @param rawElements
	 * @return
	 */
	private final boolean doRawElementsContainModelInstance(Collection<Object> rawElements) {
		if (rawElements != null) {
			for (Object rawElement : rawElements) {
				if (rawElement instanceof Model) {
					return true;
				}
			}
		}
		
		return false;
	}

	/**
	 * @return the meta-model's version of the input model
	 */
	public String getInputMetaModelVersion() {
		return notationVersion;
	}

	/**
	 * @return
	 */
	public String getVersion() {
		return getClass().getPackage().getImplementationVersion();
	}

	/**
	 * @return the conversionPhase
	 */
	public final ModelConversionPhase getConversionPhase() {
		return conversionPhase;
	}

	/**
	 * The model element cache helps you when you are inside a model element converter and
	 * you need a certain model element where it is difficult to get it by traversing the
	 * object graph of model elements.
	 * Note that a model element will only be found in the cache if it got added there
	 * before. It is the model element converter's task to ensure that a model element
	 * gets added to the model element cache. Also, you might try to find a model element that
	 * did not get converted so far. In this case it would make more sense to use
	 * AbstractModelElementConverter.convertWithOtherConverter(), which triggers the
	 * conversion in case it has not beed done before.
	 *
	 * @return
	 */
	public ModelElementCache getModelElementCache() {
        return modelElementCache;
	}

	protected void clearModelElementCache() {
		if (this.modelElementCache != null) this.modelElementCache.clear();
	}

	/**
	 * Adding a model element to the model element cache. Any model element that got added
	 * to the model element cache can be looked up at a later time by its name or/and namespace.
	 *
	 * @param modelElement
	 * @param namespace
	 */
	protected void addModelElement(ModelElementI modelElement, String namespace) {
		modelElementCache.add(modelElement, namespace);
	}

	/**
	 * @param modelElement
	 */
	protected void addModelElement(ModelElementI modelElement) {
		addModelElement(modelElement, null);
	}

	/**
	 * @return the options
	 */
	public ModelConverterOptions getOptions() {
		return options;
	}

	/**
	 * Convenience method to get the project model element, which is a singleton per combination of target project and model converter
	 *
	 * @return
	 */
	public Model getModel() {
		return this.model;
	}

	/**
	 * This method provides the possibility to exclude certain model element converters from being used, especially
	 * for such model element converters that are not in an inheritance relationship with each other.
	 * The rule is: if a model element leads to the usage of an element converter that is part of the keys of the map
	 * that this method returns, then all the element converters that are in the corresponding map-entry's list are not
	 * being used for the same element.
	 * Note that if there exists a rule for element converter exclusion that _always_ has to be applied, no matter which
	 * model converter is being used, then the method AbstractModelElementConverter.getExcludedModelElementConverters()
	 * should be used (overwritten) instead.
	 *
	 * @return
	 */
	protected LinkedHashMap<Class<AbstractModelElementConverter<?,?>>, List<Class<AbstractModelElementConverter<?,?>>>> getModelElementConverterExclusions() {
		LinkedHashMap<Class<AbstractModelElementConverter<?,?>>, List<Class<AbstractModelElementConverter<?,?>>>> result =
				new LinkedHashMap<>();

		return result;
	}

	/**
	 * Gets all option definitions that his converter provides/understands. This feature
	 * is experimental. Maybe we need a static was or a declarative way to define
	 * option definitions instead of what is provided here. At the moment this only
	 * serves as a first attempt for an implementation and a place to document the currently
	 * available options. (mmt 11-Dec-2012)
	 *
	 * @return the optionDefinitions as an unmodifiable set
	 */
	public final Set<OptionDefinition<?>> getOptionDefinitions() {
		return Collections.unmodifiableSet(optionDefinitions);
	}

	/**
	 * @param optionDefinition
	 */
	protected final void addOptionDefinition(OptionDefinition<?> optionDefinition) {
		if (optionDefinition == null) throw new NullPointerException("parameter 'optionDefinition' must not be null");
		this.optionDefinitions.add(optionDefinition);
	}

	public String getModelConverterId() {
		return this.getClass().getName() + (this.getOptions() == null ? "" : this.getOptions().hashCode()+"");
	}

	/**
	 * The set of converter locks serves as a mechanism to avoid circular calls to onCreateModelElement(...) and onConvert(...).
	 * Whenever an original model element is just being converted by an element converter, that original model element together
	 * with the element converter class is used as the key in a lookup-table. As long as the lookup table includes a key, the corresponding
	 * element converter must not be called for the same original model element.
	 *
	 * @return the converterLocks
	 */
	protected Set<OriginalModelElementKey> getConverterLocks() {
		return converterLocks;
	}

	/**
	 * The contextual converter lookup table stores instances of OriginalModelElementKey where the key's contextual model element
	 * is always null. This lookup table enforces one single rule:
	 * when for an original model element an element converter has been executed _with_ a contextual element, the same element converter
	 * must _not_ be executed for the same original model element _without_ a contextual element. The same holds true the other way round:
	 * once an original model element has been converter _without_ a contextual element, it must not be converted again with the same converter
	 * _with_ a contextual element. But for the latter rule, the converter cache is sufficient to be used.
	 *
	 * @return the contextualConverterLookupTable
	 */
	protected Set<OriginalModelElementKey> getContextualConverterLookupTable() {
		return contextualConverterLookupTable;
	}

	public ConverterOptions getConverterOptions() {
		return converterOptions;
	}

	@Override
	public void addError(String message, Throwable t) {
		super.addError(message, t);
	}

	@Override
	public void addError(String message) {
		super.addError(message);
	}
	
	@Override
	public void addInfo(String message) {
		super.addInfo(message);
	}

	@Override
	public void addWarning(String message, Throwable t) {
		super.addWarning(message, t);
	}

	@Override
	public void addWarning(String message) {
		super.addWarning(message);
	}

	@Override
	public List<String> getErrors() {
		return super.getErrors();
	}

	@Override
	public List<String> getInfos() {
		return super.getInfos();
	}

	@Override
	public List<String> getWarnings() {
		return super.getWarnings();
	}
	
	/**
	 * Convenience method to add a set of messages in on go.
	 * 
	 * @param messages
	 */
	public void addMessages(Collection<Message> messages) {
		for (Message message : messages) {
			addMessage(message);
		}
	}
	
	/**
	 * @param message
	 */
	public void addMessage(Message message) {
		switch (message.getStatus()) {
		case ERROR:
			addError(message.getMessage(), message.getException());
			break;
		case INFO:
			addInfo(message.getMessage());  // by design an info cannot take an exception 
			break;
		case WARNING:
			addWarning(message.getMessage(), message.getException());
			break;
		default:
			throwUnhandledEnumEntryException(message.getStatus());
		}
	}

	/**
	 * This method returns a subset of the elements that were passed to this model converter
	 * but are not going to be processed by it. The collection only holds model elements
	 * when a filter has been applied. If no filter has been applied,
	 * this method simply returns an empty collection.
	 * 
	 * @return the collection of elements that are _not_ going to be processed by this model converter, in case a filter has been applied
	 */
	public Set<Object> getFilteredModelElements() {
		return filteredModelElements;
	}

	/**
	 * If no filter has been applied, this method returns an empty collection.
	 * 
	 * @return the collection of elements that _are_ going to be processed by this model converter, in case a filter has been applied
	 */
	public Set<Object> getUnfilteredModelElements() {
		return unfilteredModelElements;
	}
	
	/**
	 * @param resultType
	 * @return
	 */
	public final <T extends ModelElementI> Set<T> getFilteredModelElements(Class<T> resultType) {
		return getFilteredModelElements(resultType, false);
	}
	
	/**
	 * @param resultType
	 * @param exactMatch
	 * @return
	 */
	public final <T extends ModelElementI> Set<T> getFilteredModelElements(Class<T> resultType, boolean exactMatch) {
		Set<T> elements = new LinkedHashSet<>();

		for (Object aModelElement : filteredModelElements) {
			if (exactMatch && resultType == aModelElement.getClass()) {
				elements.add(resultType.cast(aModelElement));
			} else if (!exactMatch && resultType.isAssignableFrom(aModelElement.getClass())) {
				elements.add(resultType.cast(aModelElement));
			}
		}

		return elements;
	}
	
	/**
	 * @param resultType
	 * @return null or the one element of type 'resultType' that has been found in the collection of unfiltered model elements
	 * @throws ModelConverterException
	 */
	public final <T extends ModelElementI> T getFilteredModelElement(Class<T> resultType) {
		T result = null;
		
		Set<T> elements = getFilteredModelElements(resultType);
		if (elements.size() == 1) {
			result = elements.iterator().next();
		} else if (elements.size() > 1) {
			throw new ModelConverterException("found more than one element of type '" + resultType + "' in collection of unfiltered elements:" + elements);
		}
		
		return result;
	}
	
	/**
	 * @param resultType
	 * @return
	 */
	public final <T extends ModelElementI> Set<T> getModelElementsToBeProcessed(Class<T> resultType) {
		return getModelElementsToBeProcessed(resultType, false);
	}
	
	/**
	 * @param resultType
	 * @param exactMatch
	 * @return
	 */
	public final <T extends ModelElementI> Set<T> getModelElementsToBeProcessed(Class<T> resultType, boolean exactMatch) {
		Set<T> elements = new LinkedHashSet<>();

		if (unfilteredModelElements.size() > 0) {
			for (Object aModelElement : unfilteredModelElements) {
				if (exactMatch && resultType == aModelElement.getClass()) {
					elements.add(resultType.cast(aModelElement));
				} else if (!exactMatch && resultType.isAssignableFrom(aModelElement.getClass())) {
					elements.add(resultType.cast(aModelElement));
				}
			}
		} else {
			for (Object aModelElement : incomingModelElements) {
				if (exactMatch && resultType == aModelElement.getClass()) {
					elements.add(resultType.cast(aModelElement));
				} else if (!exactMatch && resultType.isAssignableFrom(aModelElement.getClass())) {
					elements.add(resultType.cast(aModelElement));
				}
			}
		}

		return elements;
	}
	
	/**
	 * @param resultType
	 * @return null or the one element of type 'resultType' that has been found in the collection of unfiltered model elements
	 * @throws ModelConverterException
	 */
	public final <T extends ModelElementI> T getModelElementToBeProcessed(Class<T> resultType) {
		T result = null;
		
		Set<T> elements = getModelElementsToBeProcessed(resultType);
		if (elements.size() == 1) {
			result = elements.iterator().next();
		} else if (elements.size() > 1) {
			throw new ModelConverterException("found more than one element of type '" + resultType + "' in collection of model elements to be processed:" + elements);
		}
		
		return result;
	}

	/**
	 * @return the collection of elements that got passed to this model converter, no matter whether this model converter applies a filter or not
	 */
	public Set<Object> getIncomingModelElements() {
		return incomingModelElements;
	}
	
	/**
	 * Convenience method to throw a {@link ModelConverterException} in case an enum entry
	 * should have been handled in a switch but in fact wasn't. Such a situation indicates
	 * a bug, e.g. generator components with semantically incompatible versions.
	 * 
	 * @param enumEntry
	 */
	protected void throwUnhandledEnumEntryException(Enum<?> enumEntry) {
	    Message errorMessage = BasicMessage.UNHANDLED_ENUM_ENTRY
			.getMessageBuilder()
			.parameters(enumEntry.name(), enumEntry.getClass().getSimpleName(), this.getClass().getSimpleName())
			.build();
        throw new ModelConverterException(errorMessage.getMessage());
	}
}
