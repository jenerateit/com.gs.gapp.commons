package com.gs.gapp.converter.iot.persistence;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.metamodel.analytics.AbstractAnalyticsConverterOptions;

/**
 * @author mmt
 *
 */
public class IotToPersistenceConverterOptions extends AbstractAnalyticsConverterOptions {
	
	public IotToPersistenceConverterOptions(ModelConverterOptions options) {
		super(options);
	}
}
