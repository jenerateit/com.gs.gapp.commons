/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.converter.iot.persistence;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;

/**
 * @author mmt
 *
 */
public class IotToPersistenceConverterProvider implements ModelConverterProviderI {

	/**
	 *
	 */
	public IotToPersistenceConverterProvider() {
		super();
	}

	@Override
	public ModelConverterI getModelConverter() {
		return new IotToPersistenceConverter();
	}
}
