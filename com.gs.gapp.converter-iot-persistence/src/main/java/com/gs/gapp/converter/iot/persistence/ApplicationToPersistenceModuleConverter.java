/**
 * 
 */
package com.gs.gapp.converter.iot.persistence;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.iot.Application;
import com.gs.gapp.metamodel.iot.device.Device;
import com.gs.gapp.metamodel.iot.device.HardwareUsage;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.persistence.EntityRelationEnd;
import com.gs.gapp.metamodel.persistence.Namespace;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.gs.gapp.metamodel.persistence.Relation;

/**
 * @author mmt
 *
 */
public class ApplicationToPersistenceModuleConverter<S extends Application, T extends PersistenceModule> extends AbstractM2MModelElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public ApplicationToPersistenceModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElementI, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected void onConvert(S application, T persitstenceModule) {
		super.onConvert(application, persitstenceModule);
		
		// --- namespace
		Namespace persistenceNamespace = new Namespace(application.getModule().getNamespace().getName());
		persitstenceModule.setNamespace(persistenceNamespace);
		
		Entity baseEntity = new Entity("BaseEntity");
		baseEntity.setAbstractType(true);
		baseEntity.setUtilityFields(true);
		baseEntity.setMappedSuperclass(true);
		persistenceNamespace.addEntity(baseEntity);
		
		EntityField pk = new EntityField("pk", baseEntity);
		pk.setType(PrimitiveTypeEnum.UINT64.getPrimitiveType());
		pk.setPartOfId(true);
		
		EntityField accountName = new EntityField("accountName", baseEntity);
		accountName.setType(PrimitiveTypeEnum.STRING.getPrimitiveType());
		
		EntityField clientId = new EntityField("clientId", baseEntity);
		clientId.setType(PrimitiveTypeEnum.STRING.getPrimitiveType());
		clientId.setNullable(false);
		
		EntityField appId = new EntityField("appId", baseEntity);
		appId.setType(PrimitiveTypeEnum.STRING.getPrimitiveType());
		appId.setNullable(false);
		
		EntityField resourceId = new EntityField("resourceId", baseEntity);
		resourceId.setType(PrimitiveTypeEnum.STRING.getPrimitiveType());
		resourceId.setNullable(false);
		
		EntityRelationEnd geoCoordinatesField = new EntityRelationEnd("geoCoordinates", baseEntity);
		Relation geoCoordinatesRelation = new Relation("GeoCoordinatesRelation");
		geoCoordinatesRelation.setRoleA(geoCoordinatesField);
		geoCoordinatesField.setRelation(geoCoordinatesRelation);
		Entity geoCoordinates = new Entity("GeoCoordinates");
		EntityField lng = new EntityField("lng", geoCoordinates);
		lng.setType(PrimitiveTypeEnum.SINT32.getPrimitiveType());
		EntityField lat = new EntityField("lat", geoCoordinates);
		lat.setType(PrimitiveTypeEnum.SINT32.getPrimitiveType());
		EntityField alt = new EntityField("alt", geoCoordinates);
		alt.setType(PrimitiveTypeEnum.SINT32.getPrimitiveType());
		persistenceNamespace.addEntity(geoCoordinates);
		geoCoordinatesField.setType(geoCoordinates);
		
		EntityRelationEnd measurementInfoField = new EntityRelationEnd("measurementInfo", baseEntity);
		Relation measurementInfoRelation = new Relation("MeasurementInfoRelation");
		measurementInfoRelation.setRoleA(measurementInfoField);
		measurementInfoField.setRelation(measurementInfoRelation);
		Entity measurementInfo = new Entity("MeasurementInfo");
		EntityField timeOfMeasurement = new EntityField("timeOfMeasurement", measurementInfo);
		timeOfMeasurement.setType(PrimitiveTypeEnum.DATETIME.getPrimitiveType());
		EntityField timeZoneOfMeasurement = new EntityField("timeZoneOfMeasurement", measurementInfo);
		timeZoneOfMeasurement.setType(PrimitiveTypeEnum.STRING.getPrimitiveType());
		persistenceNamespace.addEntity(measurementInfo);
		measurementInfoField.setType(measurementInfo);
		
		EntityField timeOfMessageReception = new EntityField("timeOfMessageReception", baseEntity);
		timeOfMessageReception.setType(PrimitiveTypeEnum.DATETIME.getPrimitiveType());
		
		
		for (Device device : application.getDevices()) {
			for (HardwareUsage usage : device.getAllHardwareUsages()) {
				Entity usageEntity = new Entity(usage.getName() + "Entity");
				persistenceNamespace.addEntity(usageEntity);
				usageEntity.setParent(baseEntity);
				EntityRelationEnd dataField = new EntityRelationEnd("data", usageEntity);
				Relation dataRelation = new Relation("dataRelation");
				dataRelation.setRoleA(dataField);
				dataField.setRelation(dataRelation);
				Entity data = new Entity(usage.getName());
				persistenceNamespace.addEntity(data);
				
				for(Field field : usage.getImplementation().getStatusDataStructure().getFields()) {
					EntityField entityField = new EntityField(field.getName(), data);
					entityField.setType(field.getType());
				}

				dataField.setType(data);
			}
		}

	
		
		
		
		
		for (Entity entity : persistenceNamespace.getEntities()) {
			addModelElement(entity);
			entity.setOriginatingElement(persitstenceModule);
			entity.setNamespace(persistenceNamespace);
			entity.setModule(persitstenceModule);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected T onCreateModelElement(S application, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new PersistenceModule(application.getName() + "Persistence");
		return result;
	}
}
