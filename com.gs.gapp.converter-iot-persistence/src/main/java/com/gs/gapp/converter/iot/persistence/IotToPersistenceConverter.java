/**
 *
 */
package com.gs.gapp.converter.iot.persistence;

import java.util.List;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.iot.Application;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

/**
 * @author mmt
 *
 */
public class IotToPersistenceConverter extends AbstractAnalyticsConverter {

	/**
	 * 
	 */
	public IotToPersistenceConverter() {
		super(new ModelElementCache());
	}

	private IotToPersistenceConverterOptions converterOptions;
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.BasicConverter#onGetAllModelElementConverters()
	 */
	@SuppressWarnings("unused")
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		result.add(new ApplicationToPersistenceModuleConverter<Application, PersistenceModule>(this));
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.function.java.FunctionToJavaConverter#getConverterOptions()
	 */
	@Override
	public IotToPersistenceConverterOptions getConverterOptions() {
		if (this.converterOptions == null) {
			this.converterOptions = new IotToPersistenceConverterOptions(getOptions());
		}
		return this.converterOptions;
	}
	
}
