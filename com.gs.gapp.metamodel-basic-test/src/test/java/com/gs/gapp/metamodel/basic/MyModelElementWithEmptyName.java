/**
 * 
 */
package com.gs.gapp.metamodel.basic;


/**
 * @author mmt
 *
 */
public class MyModelElementWithEmptyName extends ModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1132290633835158381L;

	/**
	 * @param name
	 */
	public MyModelElementWithEmptyName(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#getName()
	 */
	@Override
	public String getName() {
        return "";
	}
	
	
	
	

}
