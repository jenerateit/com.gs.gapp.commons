package com.gs.gapp.metamodel.basic;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


/**
 * @author mmt
 *
 */
public class ModelElementCacheTest {
	
	private static ModelElementCache modelElementCache;
	
	@BeforeClass
	public static void setUp() {
		modelElementCache = new ModelElementCache();
	}
	
	@Before
	public void setUpBeforeMethod() {
		modelElementCache.clear();
	}

	@Test
	public void testAddWithNamespace() {
		
		int numberOfElements = 100;
		Set<MyModelElement> modelElements = new LinkedHashSet<>();
		
		for (int ii=0; ii<numberOfElements; ii++) {
			MyModelElement modelElement = new MyModelElement("Model Element " + ii, "my.namespace");
			modelElements.add(modelElement);
			modelElementCache.add(modelElement, "my.namespace");
		}
		Assert.assertTrue("not all elements added to cache", modelElementCache.getAllModelElements().size() == numberOfElements);
		
		ModelElementI modelElement3 = modelElementCache.findModelElement("Model Element 3");
		Assert.assertNotNull("model element no 3 not found", modelElement3);
		
		Set<ModelElementI> modelElementsPerNamespace = modelElementCache.findModelElementsByNamespace("my.namespace");
		Assert.assertTrue("not enough model elements found for namespace 'my.namespace'", modelElementsPerNamespace.size() == numberOfElements);
	}
	
	@Test
	public void testAddWithQualifiedName() {
		MyModelElement modelElement1 = new MyModelElement("element 1", "a.b.c");
		MyModelElement modelElement2 = new MyModelElement("element 2", "x.y.z");
		
		modelElementCache.addByQualifiedName(modelElement1, "a.b.c." + modelElement1.getName());
		modelElementCache.addByQualifiedName(modelElement2, "x.y.z." + modelElement2.getName());
		
		ModelElementI modelElement = modelElementCache.findModelElement(null, "a.b.c");
		Assert.assertNotNull("no model element found for namespace 'a.b.c'", modelElement);
		Assert.assertEquals(modelElement.getName(), "element 1", "wrong model element found for namespace 'a.b.c'");
		
		modelElement = modelElementCache.findModelElement("element 1");
		Assert.assertNotNull("no model element found for name 'element 1'", modelElement);
		Assert.assertEquals(modelElement.getName(), "element 1", "wrong model element found for name 'element 1'");
	}
	
	@Test
	public void testIllegalAddByQualifiedName() {
		ModelElement modelElement = new MyModelElement("sample model element");
		try {
			modelElementCache.addByQualifiedName(modelElement, null);
			Assert.fail("if full qualified name is null or empty string, then adding a model element should fail");
		} catch (Throwable th) {
			// if we are here, then it is fine
		}
	}
	
	@Test
	public void testAddingNullElement() {
		@SuppressWarnings("unused")
		ModelElement modelElement = new MyModelElement("sample model element");
		try {
			modelElementCache.addByQualifiedName(null, "a.b.c.sample model element");
			Assert.fail("if model element is null, then adding a model element should fail");
		} catch (Throwable th) {
			// if we are here, then it is fine
		}
	}
	
	@Test
	public void testAddingElementWithEmptyName() {
		try {
			ModelElement modelElement = new MyModelElementWithEmptyName("test model element with emtpy name for getName()");
			modelElementCache.addByQualifiedName(modelElement, "a.b.c.");
			Assert.fail("if model element has empty name, then adding a model element should fail");
		} catch (Throwable th) {
			// if we are here, then it is fine
		}
	}
	
	@Test
	public void testFindFindingTooManyElements() {
		MyModelElement modelElement1 = new MyModelElement("element 1", "a");
		MyModelElement modelElement2 = new MyModelElement("element 1", "b"); // same name, intentionally
		
		modelElementCache.add(modelElement1, "a");
		modelElementCache.add(modelElement2, "b"); // different namespace, that's ok
		
		try {
			@SuppressWarnings("unused")
			ModelElementI modelElement = modelElementCache.findModelElement("element 1");
			Assert.fail("if two model elements have the same name, then finding a single model element should fail");
		} catch (Throwable th) {
			// if we are here, then it is fine
		}
	}
}
