/**
 * 
 */
package com.gs.gapp.metamodel.basic;


/**
 * @author mmt
 *
 */
public class MyModelElement extends ModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1132290633835158381L;

	private String name2;
	
	/**
	 * @param name
	 */
	public MyModelElement(String name, String name2) {
		super(name);
		this.name2 = name2;
	}
	
	/**
	 * @param name
	 */
	public MyModelElement(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((name2 == null) ? 0 : name2.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MyModelElement other = (MyModelElement) obj;
		if (name2 == null) {
			if (other.name2 != null) {
				return false;
			}
		} else if (!name2.equals(other.name2)) {
			return false;
		}
		return true;
	}
}
