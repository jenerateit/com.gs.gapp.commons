//package com.gs.gapp.metamodel.doclet;
//
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileWriter;
//import java.util.ArrayList;
//import java.util.List;
//
//import com.sun.javadoc.ClassDoc;
//import com.sun.javadoc.Doclet;
//import com.sun.javadoc.LanguageVersion;
//import com.sun.javadoc.RootDoc;
//
//@SuppressWarnings("restriction")
//public class ExtractClassDoc extends Doclet {
//
//	/**
//	 * @param root
//	 * @return
//	 */
//	public static boolean start(RootDoc root) {
//		System.out.println("start with root " + root + ", # of classes:"
//				+ root.classes().length);
//		List<StringBuilder> linesOfHtml = new ArrayList<StringBuilder>();
//		ClassDoc[] classes = root.classes();
//		for (ClassDoc classDoc : classes) {
//			if (classDoc.isAbstract()) continue;  // ignore abstract targets
//			if (classDoc.name().toLowerCase().endsWith("target")) {
//				System.out.println("### " + classDoc.qualifiedName());
//				System.out.println("doc:" + (classDoc.commentText() == null || classDoc.commentText().length() == 0 ? "no JavaDoc" : classDoc.commentText()));
//
//				if (linesOfHtml.size() == 0) {
//					linesOfHtml
//							.add(new StringBuilder(
//									"<table class=\"gs-table-with-border\" rules=\"all\"><thead><tr><th>Target</th><th>JavaDoc</th></tr></thead>"));
//				}
//
//				String name = classDoc.name().substring(0, classDoc.name().length()-"target".length());
//				linesOfHtml.add(new StringBuilder("<tr><td>").append("<a name=\"").append(name).append("\"></a>")
//						.append(name).append("</td><td>")
//						.append(classDoc.commentText().replaceAll("[\r\n]", "")).append("</td></tr>"));
//			}
//		}
//
//		if (linesOfHtml.size() > 0) {
//			linesOfHtml.add(new StringBuilder("</table>"));
//		}
//
//		writeTargetJavaDocs(linesOfHtml);
//
//		return true;
//	}
//
//	/**
//	 * @param linesOfHtml
//	 */
//	private static void writeTargetJavaDocs(List<StringBuilder> linesOfHtml) {
//		if (linesOfHtml.size() > 0) {
//			BufferedWriter writer = null;
//			try {
//				File documentationDirectory = new File("documentation");
//				documentationDirectory.mkdirs();
//				File htmlFile = new File(documentationDirectory, "target-javadoc.html");
//
//				System.out.println(htmlFile.getCanonicalPath());
//
//				writer = new BufferedWriter(new FileWriter(htmlFile));
//				for (StringBuilder sb : linesOfHtml) {
//				    writer.write(sb.toString());
//				}
//			} catch (Exception ex) {
//				ex.printStackTrace();
//			} finally {
//				try {
//					// Close the writer regardless of what happens...
//					writer.close();
//				} catch (Exception ex) {
//					// intentionally ignore this exception
//				}
//			}
//		}
//	}
//
//	/**
//	 * NOTE: Without this method present and returning LanguageVersion.JAVA_1_5,
//	 * Javadoc will not process generics because it assumes
//	 * LanguageVersion.JAVA_1_1
//	 * 
//	 * @return language version (hard coded to LanguageVersion.JAVA_1_5)
//	 */
//	public static LanguageVersion languageVersion() {
//		return LanguageVersion.JAVA_1_5;
//	}
//
//}
