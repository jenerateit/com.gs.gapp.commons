package com.gs.gapp.metamodel.basic.options;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;


public class OptionDefinitionTest {

	@Test
	public void createDefinitions() {

		String key = "stringOption";
		OptionDefinitionString stringDefinition = new OptionDefinitionString(key, null, true, "default");

		Assert.assertEquals(key, stringDefinition.getKey());
		Assert.assertEquals(key, stringDefinition.getName());
	}

	@Test
	public void createValues() {

		OptionDefinitionString stringDefinition = new OptionDefinitionString("stringOption", null, true, "default");
		String optionStringValue = "the-option-value";

		OptionDefinition<String>.OptionValue optionValue = stringDefinition. new OptionValue(Arrays.asList(new String[] {optionStringValue}));

		Assert.assertEquals(optionStringValue, optionValue.getOptionValues().get(0));

	}
}
