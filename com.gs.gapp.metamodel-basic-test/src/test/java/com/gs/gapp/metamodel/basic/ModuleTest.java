package com.gs.gapp.metamodel.basic;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;


public class ModuleTest {

	public static final String NAME = "test";
	
	@Test
	public void constructorName() {
		Module m = new Module(NAME);
		
		Assert.assertEquals(m.getName(), NAME);
		Assert.assertNull(m.getOriginatingElement());
		Assert.assertTrue(m.isGenerated());
	}


	@Test
	public void constructorOriginName() {
		BigDecimal origin = new BigDecimal(1);
		Module m = new Module(NAME);
		m.setOriginatingElement(origin);
		
		Assert.assertEquals(m.getName(), NAME);
		Assert.assertNotNull(m.getOriginatingElement());
		Assert.assertEquals(m.getOriginatingElement(), origin);
		Assert.assertTrue(m.isGenerated());
	}

	public void addElementTwice() {
		BigDecimal origin = new BigDecimal(1);
		Module m = new Module(NAME);
		m.setOriginatingElement(origin);

		Module ma = new Module("m1");
		ma.setOriginatingElement(origin);
		Assert.assertTrue(m.addElement(ma));
		Assert.assertEquals(m.getElements().size(), 1);
		Assert.assertFalse(m.addElement(ma));
		Assert.assertEquals(m.getElements().size(), 1);
		
		ma = new Module("m1");
		ma.setOriginatingElement(origin);
		Assert.assertFalse(m.addElement(ma));
		Assert.assertEquals(m.getElements().size(), 1);
	}
	
	@Test
	public void addElements() {
		BigDecimal origin = new BigDecimal(1);
		Module m = new Module(NAME);
		m.setOriginatingElement(origin);

		Module ma1 = new Module("m1");
		ma1.setOriginatingElement(origin);
		Module ma2 = new Module("m2");
		ma2.setOriginatingElement(origin);
		Assert.assertTrue(m.addElement(ma1));
		Assert.assertEquals(m.getElements().size(), 1);
		Assert.assertTrue(m.addElement(ma2));
		Assert.assertEquals(m.getElements().size(), 2);	
	}
	
	@Test
	public void documentation() {
		BigDecimal origin = new BigDecimal(1);
		DocumentationI d = new Module(NAME);
		((ModelElement)d).setOriginatingElement(origin);

		Assert.assertNull(d.getBody());
		d.setBody("test for docu");
		Assert.assertNotNull(d.getBody());
		Assert.assertEquals(d.getBody(), "test for docu");
	}
	
	@Test
	public void hashCodeTest() {
		BigDecimal origin = new BigDecimal(1);
		
		Module m1 = new Module(NAME);
		m1.setOriginatingElement(origin);
		int hc = m1.hashCode();
		Assert.assertEquals(m1.hashCode(), hc);
		Assert.assertEquals(m1.hashCode(), hc);
		Assert.assertEquals(m1.hashCode(), hc);

		Module m2 = new Module(NAME);
		m2.setOriginatingElement(origin);
		Assert.assertEquals(m1.hashCode(), m2.hashCode());
		
		m2 = new Module(NAME);
		Assert.assertEquals(m1.hashCode(), m2.hashCode());
		m2.setGenerated(false);
		Assert.assertEquals(m1.hashCode(), m2.hashCode());
		
		m2 = new Module("abc");
		Assert.assertFalse(m1.hashCode() == m2.hashCode());
		
		
		m2 = new Module(NAME);
		m2.setOriginatingElement(origin);
		Module ma1 = new Module("m1");
		ma1.setOriginatingElement(origin);
		Module ma2 = new Module("m2");
		ma2.setOriginatingElement(origin);
		m1.addElement(ma1);
		Assert.assertEquals(m1.hashCode(), m2.hashCode());
		m2.addElement(ma1);
		Assert.assertEquals(m1.hashCode(), m2.hashCode());
		
		m1.addElement(ma2);
		Assert.assertEquals(m1.hashCode(), m2.hashCode());
		m2.addElement(ma2);
		Assert.assertEquals(m1.hashCode(), m2.hashCode());
	}

	@Test
	public void equalsTest() {
		BigDecimal origin = new BigDecimal(1);
		
		Module m1 = new Module(NAME);
		m1.setOriginatingElement(origin);
		Module m2 = new Module(NAME);
		m2.setOriginatingElement(origin);
		Assert.assertTrue(m1.equals(m2));
		Assert.assertTrue(m2.equals(m1));
		
		m2 = new Module(NAME);
		m2.setOriginatingElement(origin);
		Assert.assertTrue(m1.equals(m2));
		m2.setGenerated(false);
		Assert.assertTrue(m1.equals(m2));
		
		m2 = new Module("abc");
		m2.setOriginatingElement(origin);
		Assert.assertFalse(m1.equals(m2));

		m2 = new Module(NAME);
		m2.setOriginatingElement(origin);
		Module ma1 = new Module("m1");
		ma1.setOriginatingElement(origin);
		Module ma2 = new Module("m2");
		ma2.setOriginatingElement(origin);
		m1.addElement(ma1);
		Assert.assertTrue(m1.equals(m2));
		m2.addElement(ma1);
		Assert.assertTrue(m1.equals(m2));
		
		m1.addElement(ma2);
		Assert.assertTrue(m1.equals(m2));
		m2.addElement(ma2);
		Assert.assertTrue(m1.equals(m2));
	}
}
