package com.gs.gapp.metamodel.function;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs.gapp.metamodel.basic.ModelValidatorI;

/**
 * AJAX-147
 * 
 * @author marcu
 *
 */
public class ValidatorPathParameters implements ModelValidatorI {

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(assertPathParameterVariablesMatch(modelElements));
		return result;
	}

	/**
	 * @param rawElements
	 */
	private Collection<Message> assertPathParameterVariablesMatch(Collection<Object> rawElements) {
		final Collection<Message> result = new LinkedHashSet<>();
		
		rawElements.stream().filter(element -> element instanceof Function).map(function -> (Function) function)
			.forEach(function -> {
				String pathValue = ServiceModelProcessing.getEffectivePath(function);
				Set<FunctionParameter> pathParams = ServiceModelProcessing.getFunctionInPathParameters(function);
				
				if (pathValue == null || pathValue.isEmpty() || pathValue.length() == 1) {
					if (!pathParams.isEmpty()) {
						// no path defined for the function but the function has path parameters => invalid (we presume that there are no path templates on the resource class level)
						Message message = FunctionMessage.PATH_PARAMETER_VARIABLES_BUT_NO_TEMPLATE.getMessageBuilder()
								.modelElement(function)
								.parameters(function.getName(), function.getModule().getName())
								.build();
						result.add(message);
					}
			    } else {
			    	// There might be a path template. We have to make sure that for every variable therein, a path param option is set.
			    	
                    long countOfVariables = pathValue.chars().filter(ch -> ch == '{').count();
			    	if (countOfVariables != pathParams.size()) {
			    		// number of variables in path template and number of path params does not match
			    		Message message = FunctionMessage.PATH_PARAMETER_TEMPLATE_IS_WRONG.getMessageBuilder()
			    				.modelElement(function)
			    				.parameters(pathValue, function.getName(), function.getModule().getName())
			    				.build();
						result.add(message);
			    	} else {
			    		Set<FunctionParameter> pathParamsFiltered = pathParams.stream()
			    		    .filter(param -> pathValue.contains("{"+param.getName()+"}") || pathValue.contains("{"+param.getName()+":"))
			                .collect(Collectors.toSet());
			    		if (pathParamsFiltered.size() != pathParams.size()) {
			    			// there are parameter variables in the path template that do not match the naming for the in parameters
			    			Message message = FunctionMessage.PATH_PARAMETER_TEMPLATE_IS_WRONG.getMessageBuilder()
			    					.modelElement(function)
			    					.parameters(pathValue, function.getName(), function.getModule().getName())
			    					.build();
							result.add(message);
			    		}
			    	}
			    }
				
				// --- compare function path with function module path
				FunctionModule functionModule = function.getOwner();
				String functionModulePath = ServiceModelProcessing.getPath(functionModule);
				String functionPath = ServiceModelProcessing.getPath(function);
				if (functionModulePath != null && functionPath != null) {
					if (functionModulePath.toLowerCase().endsWith(functionPath.toLowerCase())) {
						result.add(FunctionMessage.WARNING_FUNCTION_PATH_ALREADY_PART_OF_MODULE_PATH.getMessageBuilder()
								.modelElement(functionModule)
								.parameters(function.getName(),
										    functionModule.getName(),
										    functionPath,
										    functionModulePath)
								.build());
					}
				}
			});

		return result;
	}
}
