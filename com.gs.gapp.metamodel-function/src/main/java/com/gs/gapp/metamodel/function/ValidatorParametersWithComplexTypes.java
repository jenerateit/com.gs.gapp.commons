package com.gs.gapp.metamodel.function;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs.gapp.dsl.rest.RestOptionEnum.ParamTypeEnum;
import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;

/**
 * AJAX-147
 * 
 * 
 *
 */
public class ValidatorParametersWithComplexTypes implements ModelValidatorI {

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(assertParametersDontUseComplexTypes(modelElements));
		return result;
	}

	/**
	 * @param rawElements
	 */
	private Collection<Message> assertParametersDontUseComplexTypes(Collection<Object> rawElements) {
		final Collection<Message> result = new LinkedHashSet<>();
		
		rawElements.stream().filter(element -> element instanceof Function).map(function -> (Function) function)
			.forEach(function -> {
				final Set<FunctionParameter> nonBodyParams = new LinkedHashSet<>();
				Set<FunctionParameter> nonBodyInParams = ServiceModelProcessing.getFunctionInParameters(function, ParamTypeEnum.COOKIE, ParamTypeEnum.HEADER, ParamTypeEnum.PATH, ParamTypeEnum.QUERY);
				Set<FunctionParameter> cookieOutParams = ServiceModelProcessing.getFunctionOutCookieParameters(function);
				Set<FunctionParameter> headerOutParams = ServiceModelProcessing.getFunctionOutHeaderParameters(function);
				nonBodyParams.addAll(nonBodyInParams);
				nonBodyParams.addAll(cookieOutParams);
				nonBodyParams.addAll(headerOutParams);
				
				LinkedHashSet<FunctionParameter> unsupportedParams = nonBodyParams.stream()
					.filter(param -> param.getType() instanceof ComplexType && !(param.getType() instanceof Enumeration))
				    .collect(Collectors.toCollection(LinkedHashSet::new));
				
				if (!unsupportedParams.isEmpty()) {
				    String unsupportedParamsAsString = unsupportedParams.stream().map(param -> param.getName() + ":" + param.getType()).collect(Collectors.joining(","));
				    Message message = FunctionMessage.ERROR_NON_BODY_PARAMETERS_WITH_COMPLEX_TYPES.getMessageBuilder()
				    		.modelElement(function)
							.parameters(function.getQualifiedName(), unsupportedParamsAsString)
							.build();
					result.add(message);
				}
			});
				

		return result;
	}
}
