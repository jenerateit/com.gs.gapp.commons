package com.gs.gapp.metamodel.function;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.StringUtils;
import org.jenerateit.util.StringTools;
import com.gs.gapp.metamodel.basic.MetamodelI;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
//import com.gs.gapp.metamodel.persistence.Namespace;
import com.gs.gapp.metamodel.basic.ParameterType;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;

/**
 * @author marcu
 *
 */
public enum FunctionMetamodel implements MetamodelI {

	INSTANCE,
	;
	
	private static final LinkedHashSet<Class<? extends ModelElementI>> metatypes = new LinkedHashSet<>();
	private static final Collection<Class<? extends ModelElementI>> collectionOfCheckedMetatypes = new LinkedHashSet<>();
	
	static {
		metatypes.add(BusinessException.class);
		metatypes.add(BusinessLogic.class);
		metatypes.add(Function.class);
		metatypes.add(FunctionModule.class);
		metatypes.add(FunctionParameter.class);
		metatypes.add(Namespace.class);
		metatypes.add(Service.class);
		metatypes.add(ServiceClient.class);
		metatypes.add(ServiceImplementation.class);
		metatypes.add(ServiceInterface.class);
		metatypes.add(Storage.class);
		metatypes.add(TechnicalException.class);
		
		collectionOfCheckedMetatypes.add(Function.class);
		collectionOfCheckedMetatypes.add(FunctionModule.class);
		collectionOfCheckedMetatypes.add(Namespace.class);
		collectionOfCheckedMetatypes.add(ServiceClient.class);
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypes() {
		return Collections.unmodifiableCollection(metatypes);
	}

	@Override
	public boolean isIncluded(Class<? extends ModelElementI> metatype) {
		return metatypes.contains(metatype);
	}

	@Override
	public boolean isExtendingOneOfTheMetatypes(Class<? extends ModelElementI> metatype) {
		for (Class<? extends ModelElementI> metatypeOfMetamodel : metatypes) {
			if (metatypeOfMetamodel.isAssignableFrom(metatype)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypesForConversionCheck() {
		return collectionOfCheckedMetatypes;
	}
	
	/**
	 * @param metatype
	 * @return
	 */
	public Class<? extends Module> getModuleType(Class<? extends ModelElementI> metatype) {
		if (isIncluded(metatype)) {
		    return FunctionModule.class;
		}
		return null;
	}
	
	/**
	 * @param result
	 */
	public void createTypesForInAndOutParameters(LinkedHashSet<Object> result) {
		for (Object element : new ArrayList<>(result)) {
			if (element instanceof FunctionModule) {
				FunctionModule functionModule = (FunctionModule) element;
				for (Function function : functionModule.getFunctions()) {
					if (function.getFunctionInParameters().size() > 1) {
						// introduce new complex type to hold all in parameters
						ComplexType inputType = new ComplexType("InputFor" + StringUtils.capitalize(function.getName()));
						result.add(inputType);
						inputType.setOriginatingElement(function);
						inputType.setModule(function.getModule());
						for (FunctionParameter functionParameter : function.getFunctionInParameters()) {
							final Field field = new Field(functionParameter.getName(), inputType);
							field.setOriginatingElement(functionParameter);
							field.setType(functionParameter.getType());
							field.setCollectionType(functionParameter.getCollectionType());
						}
						function.removeAllInParameters();
						FunctionParameter inputParameter = new FunctionParameter("input");
						inputParameter.setType(inputType);
						inputParameter.setParameterType(ParameterType.IN);
						function.addFunctionInParameter(inputParameter);
					}
					
					if (function.getFunctionOutParameters().size() > 1) {
						// introduce new complex type to hold all out parameters
						ComplexType outputType = new ComplexType("OutputFor" + StringUtils.capitalize(function.getName()));
						result.add(outputType);
						outputType.setOriginatingElement(function);
						outputType.setModule(function.getModule());
						for (FunctionParameter functionParameter : function.getFunctionOutParameters()) {
							final Field field = new Field(functionParameter.getName(), outputType);
							field.setOriginatingElement(functionParameter);
							field.setType(functionParameter.getType());
							field.setCollectionType(functionParameter.getCollectionType());
						}
						function.removeAllOutParameters();
						FunctionParameter outputParameter = new FunctionParameter("output");
						outputParameter.setType(outputType);
						outputParameter.setParameterType(ParameterType.OUT);
						function.addFunctionOutParameter(outputParameter);
					}
				}
		    }
		}
	}
	
	/**
	 * @param modelElements
	 * @return
	 */
	public Set<Object> createExceptionParent(final Set<Object> modelElements) {
		// --- identify all service interfaces
		List<ServiceInterface> serviceInterfaces = modelElements.stream()
		    .filter(ServiceInterface.class::isInstance)
		    .map(ServiceInterface.class::cast)
		    .collect(Collectors.toList());
		
		// --- create all abstract exception types
		final Map<ServiceInterface, AbstractBusinessException> abstractBusinessExceptions = new HashMap<>();
		final Map<ServiceInterface, AbstractTechnicalException> abstractTechnicalExceptions = new HashMap<>();
		serviceInterfaces.stream().forEach(serviceInterface -> {
			AbstractBusinessException businessException = new AbstractBusinessException("Abstract" + StringTools.firstUpperCase(serviceInterface.getName()) + "BusinessException");
			businessException.setModule(serviceInterface.getModule());
			businessException.setOriginatingElement(serviceInterface);
			abstractBusinessExceptions.put(serviceInterface, businessException);
			addFields(businessException);
			modelElements.add(businessException);
			
			AbstractTechnicalException technicalException = new AbstractTechnicalException("Abstract" + StringTools.firstUpperCase(serviceInterface.getName()) + "TechnicalException");
			technicalException.setModule(serviceInterface.getModule());
			technicalException.setOriginatingElement(serviceInterface);
			abstractTechnicalExceptions.put(serviceInterface, technicalException);
			addFields(technicalException);
			modelElements.add(technicalException);
		});
		
		// --- make all exception types use the respective abstract exception as its parent
		serviceInterfaces.stream().forEach(serviceInterface -> {
			serviceInterface.getAllUsedExceptionTypes().stream().forEach(exceptionType -> {
				if (exceptionType instanceof BusinessException) {
					exceptionType.setParent(abstractBusinessExceptions.get(serviceInterface));
				} else if (exceptionType instanceof TechnicalException) {
					exceptionType.setParent(abstractTechnicalExceptions.get(serviceInterface));
				}
			});
		});
		
		return modelElements;
		
	}

	private void addFields(ExceptionType exceptionType) {
		// --- Here we add standard fields for a textual status and a numeric status.
		// --- Note that although these fields look like being invented for exceptions
		// --- that are going to be used for generated webservices, they actually are not.
		// --- It is common practice to use an error number along with a short text to tell
		// --- the user something about the root cause in a brief manner. A Java exception's
		// --- already existing text parameter 'message' is going to be used for a more elaborate
		// --- error message that should always be interpreted by a human being.
		Stream.of(AbstractCapabilityException.FieldEnum.values()).forEach(fieldEnum -> {
			Field statusField = new Field(fieldEnum.getName(), exceptionType);
			statusField.setReadOnly(true);
			
			switch (fieldEnum) {
			case STATUS_CODE:
				statusField.setType(PrimitiveTypeEnum.STRING.getPrimitiveType());
				break;
			case STATUS_NUMBER:
				statusField.setType(PrimitiveTypeEnum.SINT64.getPrimitiveType());
				break;
			default:
				break;
			}
		});
	}
}
