package com.gs.gapp.metamodel.function;

import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;

public class BusinessException extends ExceptionType {

	private static final long serialVersionUID = 7195100445001398368L;

	public BusinessException(String name) {
		super(name);
	}

}
