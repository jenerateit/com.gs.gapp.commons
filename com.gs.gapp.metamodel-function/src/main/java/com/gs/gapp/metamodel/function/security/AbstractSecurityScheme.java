/**
 * 
 */
package com.gs.gapp.metamodel.function.security;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * @author mmt
 *
 */
public abstract class AbstractSecurityScheme extends ModelElement {

	private static final long serialVersionUID = -2310715163067089530L;
	
	private String description;
	
	public AbstractSecurityScheme(String name) {
		super(name);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
