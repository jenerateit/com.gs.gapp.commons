package com.gs.gapp.metamodel.function;

public class AbstractTechnicalException extends AbstractCapabilityException {

	private static final long serialVersionUID = 7195100445001398368L;

	public AbstractTechnicalException(String name) {
		super(name);
	}
}
