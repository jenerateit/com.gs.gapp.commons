package com.gs.gapp.metamodel.function;

public class AbstractBusinessException extends AbstractCapabilityException {

	private static final long serialVersionUID = 7195100445001398368L;

	public AbstractBusinessException(String name) {
		super(name);
	}
}
