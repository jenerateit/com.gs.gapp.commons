package com.gs.gapp.metamodel.function;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * A service model element holds two sets of function modules.
 * One set nominates the function modules that this service provides (service implementation).
 * The other set nominates the function modules that this service consumes (service client).
 * The purpose of a service element is to be able to have two things connected to each other:
 * - the definition of a service implementation (provided function modules)
 * - the definition of other service implementations that need to be called when this service implementation is getting called  
 * 
 * @author mmt
 *
 */
public class Service extends ModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -866661199337493246L;
	
	private Set<FunctionModule> consumers = new LinkedHashSet<>();
	private Set<FunctionModule> providers = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public Service(String name) {
		super(name);
	}

	public Set<FunctionModule> getConsumers() {
		return consumers;
	}

	protected void addConsumers(FunctionModule... consumers) {
		if (consumers != null) {
		    this.consumers.addAll(Arrays.asList(consumers));
		}
	}

	public Set<FunctionModule> getProviders() {
		return providers;
	}

	protected void addProviders(FunctionModule... providers) {
		if (providers != null) {
		    this.providers.addAll(Arrays.asList(providers));
		}
	}

}
