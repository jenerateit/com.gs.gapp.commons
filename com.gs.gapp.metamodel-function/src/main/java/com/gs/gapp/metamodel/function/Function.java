/**
 *
 */
package com.gs.gapp.metamodel.function;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.enums.StorageFunction;

/**
 * @author mmt
 *
 */
public class Function extends ModelElement {


	/**
	 *
	 */
	private static final long serialVersionUID = -4844834229770034646L;

	private final Set<FunctionParameter> functionInParameters = new LinkedHashSet<>();

	private final Set<FunctionParameter> functionOutParameters = new LinkedHashSet<>();

	private final Set<BusinessException> businessExceptions = new LinkedHashSet<>();
	
	private final Set<TechnicalException> technicalExceptions = new LinkedHashSet<>();

	private StorageFunction storageFunction;

	private boolean idempotent = false;
	
	private final FunctionModule owner;
	
	private Entity entity;

	/**
	 * @param name
	 */
	public Function(String name, FunctionModule owner) {
		super(name);
		this.owner = owner;
		
		owner.addFunction(this);
	}

	/**
	 * @return the functionInParameters
	 */
	public Set<FunctionParameter> getFunctionInParameters() {
		return functionInParameters;
	}


	/**
	 * @param functionInParameter
	 * @return
	 */
	public boolean addFunctionInParameter(FunctionParameter functionInParameter) {
		return this.functionInParameters.add(functionInParameter);
	}

	/**
	 * @return the functionOutParameters
	 */
	public Set<FunctionParameter> getFunctionOutParameters() {
		return functionOutParameters;
	}

	/**
	 * Convenience method to have an easy means to get the entity type of a single out parameter.
	 * 
	 * @return the entity type of a single out parameter, null if there is more or less than 1 out parameter or its type is not an entity type
	 */
	public Entity getEntityTypedOutParameter() {
		if (functionOutParameters.size() == 1 && functionOutParameters.iterator().next().getType() instanceof Entity) {
			return (Entity) functionOutParameters.iterator().next().getType();
		}
		
		return null;
	}

	/**
	 * @param functionOutParameter
	 * @return
	 */
	public boolean addFunctionOutParameter(FunctionParameter functionOutParameter) {
		return this.functionOutParameters.add(functionOutParameter);
	}
	
	/**
	 * 
	 */
	public void removeAllParameters() {
		this.functionInParameters.clear();
		this.functionOutParameters.clear();
	}
	
	/**
	 * 
	 */
	public void removeAllInParameters() {
		this.functionInParameters.clear();
	}
	
	/**
	 * 
	 */
	public void removeAllOutParameters() {
		this.functionOutParameters.clear();
	}

	/**
	 * @return the business exceptions
	 */
	public Set<BusinessException> getBusinessExceptions() {
		return businessExceptions;
	}


	/**
	 * @param businessException
	 * @return
	 */
	public boolean addBusinessException(BusinessException businessException) {
		return this.businessExceptions.add(businessException);
	}
	
	/**
	 * @return the technical exceptions
	 */
	public Set<TechnicalException> getTechnicalExceptions() {
		return technicalExceptions;
	}


	/**
	 * @param technicalException
	 * @return
	 */
	public boolean addTechnicalException(TechnicalException technicalException) {
		return this.technicalExceptions.add(technicalException);
	}

	/**
	 * @return the storageFunction
	 */
	public StorageFunction getStorageFunction() {
		return storageFunction;
	}

	/**
	 * @param storageFunction the storageFunction to set
	 */
	public void setStorageFunction(StorageFunction storageFunction) {
		this.storageFunction = storageFunction;
	}

	/**
	 * @return the idempotent
	 */
	public boolean isIdempotent() {
		return idempotent;
	}

	/**
	 * @param idempotent the idempotent to set
	 */
	public void setIdempotent(boolean idempotent) {
		this.idempotent = idempotent;
	}

	public FunctionModule getOwner() {
		return owner;
	}

	/**
	 * The entity that this function is related to.
	 * The semantics of this relationship depends on the
	 * generator being used. Typically, you use this when
	 * a generator creates basic functionality for a given entity
	 * and you want to allow for additional function modeling that adds to
	 * this basic functionality.
	 * Examples:
	 * - additional methods in Java classes (by using FunctionToJavaMethodConverter)
	 * - additional routines in Delphi classes (by using FunctionToDelphiMethodConverter)
	 * 
	 * @return
	 */
	public Entity getEntity() {
		return entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}
	
	/**
	 * @return
	 */
	public Set<ComplexType> getAllUsedComplexTypes() {
		LinkedHashSet<ComplexType> result = new LinkedHashSet<>();
		collectAllComplexTypes(result);
		return result;
	}
	
	/**
	 * @param result
	 */
	private void collectAllComplexTypes(LinkedHashSet<ComplexType> result) {
		LinkedHashSet<FunctionParameter> allFunctionParameters = new LinkedHashSet<>();
		allFunctionParameters.addAll(getFunctionInParameters());
		allFunctionParameters.addAll(getFunctionOutParameters());
		
		for (FunctionParameter functionParameter : allFunctionParameters) {
			if (functionParameter.getType() instanceof ComplexType && !result.contains(functionParameter.getType())) {
				ComplexType complexType = (ComplexType) functionParameter.getType();
				result.add(complexType);
				result.addAll(complexType.getAllUsedComplexTypes());
				result.addAll(complexType.getAllParents());
			}
		}
		
		result.addAll(getBusinessExceptions());
		result.addAll(getTechnicalExceptions());
	}
	
	/**
	 * @return
	 */
	public Set<Entity> getAllUsedEntities() {
		LinkedHashSet<Entity> result = new LinkedHashSet<>();
		collectAllEntities(result);
		return result;
	}
	
	/**
	 * @param result
	 */
	private void collectAllEntities(LinkedHashSet<Entity> result) {
		LinkedHashSet<FunctionParameter> allFunctionParameters = new LinkedHashSet<>();
		allFunctionParameters.addAll(getFunctionInParameters());
		allFunctionParameters.addAll(getFunctionOutParameters());
		
		for (FunctionParameter functionParameter : allFunctionParameters) {
			if (functionParameter.getType() instanceof Entity && !result.contains(functionParameter.getType())) {
				Entity entity = (Entity) functionParameter.getType();
				result.add(entity);
				for (ComplexType usedComplexType : entity.getAllUsedComplexTypes()) {
					if (usedComplexType instanceof Entity) {
						result.add((Entity) usedComplexType);
					}
				}
				
				for (ComplexType aParent : entity.getAllParents()) {
					if (aParent instanceof Entity) {
						result.add((Entity) aParent);
					}
				}
			}
		}
	}
	
	@Override
	public void setModule(Module module) {
		if (module instanceof FunctionModule) {
		    super.setModule(module);
		}
	}

	@Override
	public String getQualifiedName() {
		if (this.getOwner() != null) {
		    return new StringBuilder(this.getOwner().getName()).append(".").append(this.getName()).toString();
		}
		return super.getQualifiedName();
	}
}
