package com.gs.gapp.metamodel.function.security;

public class ApiKeySecurityScheme extends AbstractSecurityScheme {

	private static final long serialVersionUID = 7398647385871374782L;

	private final ApiKeyLocation in;
	
	public ApiKeySecurityScheme(String name, ApiKeyLocation in) {
		super(name);
		this.in = in;
	}
	
	public ApiKeyLocation getIn() {
		return in;
	}

	/**
	 * @author mmt
	 *
	 */
	public static enum ApiKeyLocation {
		QUERY,
		HEADER,
		;
	}
}
