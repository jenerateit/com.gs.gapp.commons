package com.gs.gapp.metamodel.function;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * The purposes of a service client are:
 * - trigger the generation of a reusable service client library (done by the provider of a service)
 * - trigger the generation of a custom client library with manual changes that are specific to the one application that accesses the service (done by the consumer of a service)
 * - maintain in the model a countable number of service implementations that can be accessed by an application that uses a generated client library
 * 
 * @author mmt
 *
 */
public class ServiceClient extends ModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -866661199337493246L;
	
	private final ServiceInterface serviceInterface;
	
	private final Set<ServiceImplementation> consumedServiceImplementations = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public ServiceClient(String name, ServiceInterface serviceInterface) {
		super(name);
		if (serviceInterface == null) throw new NullPointerException("parameter 'serviceInterface' must not be null");
		this.serviceInterface = serviceInterface;
	}

	public ServiceInterface getServiceInterface() {
		return serviceInterface;
	}
	
	/**
	 * Nominates a countable number of service implementations that this service client is meant to
	 * use/access. This is only being used when the information about all possible service implementations
	 * should be part of the model and not dynamically being configured at runtime of a generated application.
	 * 
	 * @return
	 */
	public Set<ServiceImplementation> getConsumedServiceImplementations() {
		return consumedServiceImplementations;
	}

	/**
	 * @param consumedServiceImplementations
	 */
	public void addConsumedServiceImplementations(ServiceImplementation... consumedServiceImplementations) {
		if (getServiceInterface() == null) throw new NullPointerException("it is not allowed to add consumed service implementations when the service interface has not been set yet");
		
		if (consumedServiceImplementations != null) {
		    this.consumedServiceImplementations.addAll(Arrays.asList(consumedServiceImplementations));
		}
	}
	
	/**
	 * The service client's interface needs to be implemented by all related service implementations.
	 */
	public void validateServiceRelations() {
		if (this.serviceInterface != null) {
			LinkedHashSet<ServiceImplementation> incompatibleServiceImplementations = this.consumedServiceImplementations.stream()
			    .filter(serviceImplementation -> serviceImplementation.getServiceInterface() != this.serviceInterface)
			    .collect(Collectors.toCollection(LinkedHashSet::new));
			if (!incompatibleServiceImplementations.isEmpty()) {
				String incompatibleServiceImplementationNames = incompatibleServiceImplementations.stream()
				    .map(serviceImplementation -> serviceImplementation.getName())
				    .collect(Collectors.joining(","));
				
				FunctionMessage.ERROR_INCOMPATIBLE_SERVICE_IMPLEMENTATIONS_FOUND.getMessageBuilder()
					.modelElement(this)
					.parameters(this.getName(),
					            this.serviceInterface.getName(),
					            incompatibleServiceImplementationNames);
			}
		}
	}
}
