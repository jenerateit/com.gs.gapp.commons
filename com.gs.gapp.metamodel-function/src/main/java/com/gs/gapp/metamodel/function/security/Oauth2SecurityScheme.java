package com.gs.gapp.metamodel.function.security;

import java.net.URL;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class Oauth2SecurityScheme extends AbstractSecurityScheme {

	private static final long serialVersionUID = -6219478939019555338L;
	
	private final AbstractOauthFlow flow;

	public Oauth2SecurityScheme(String name, AbstractOauthFlow flow) {
		super(name);
		this.flow = flow;
	}
	
	public AbstractOauthFlow getFlow() {
		return flow;
	}

	/**
	 * @author mmt
	 *
	 */
	public static class OauthScope {
		private final String name;
		private final String shortDescription;
		
		public OauthScope(String name, String shortDescription) {
			super();
			this.name = name;
			this.shortDescription = shortDescription;
		}

		public String getName() {
			return name;
		}

		public String getShortDescription() {
			return shortDescription;
		}
	}
	
	/**
	 * @author mmt
	 *
	 */
	public static abstract class AbstractOauthFlow {
		
		private URL refreshUrl;
		private final Set<OauthScope> scopes = new LinkedHashSet<>();
		
		protected AbstractOauthFlow(OauthScope...oauthScopes) {
			if (oauthScopes != null) {
				this.scopes.addAll(Arrays.asList(oauthScopes));
			}
		}
		
		public URL getRefreshUrl() {
			return refreshUrl;
		}
		public void setRefreshUrl(URL refreshUrl) {
			this.refreshUrl = refreshUrl;
		}
		public Set<OauthScope> getScopes() {
			return scopes;
		}
		
		public boolean addScope(OauthScope scope) {
			return this.scopes.add(scope);
		}
	}
	
	/**
	 * @author mmt
	 *
	 */
	public static class ImplicitOauthFlow extends AbstractOauthFlow {
		private final URL authorizationUrl;

		public ImplicitOauthFlow(URL authorizationUrl, OauthScope...oauthScopes) {
			super(oauthScopes);
			this.authorizationUrl = authorizationUrl;
		}

		public URL getAuthorizationUrl() {
			return authorizationUrl;
		}
	}
	
    /**
     * @author mmt
     *
     */
    public static class PasswordOauthFlow extends AbstractOauthFlow {
    	private final URL tokenUrl;

		public PasswordOauthFlow(URL tokenUrl, OauthScope...oauthScopes) {
			super(oauthScopes);
			this.tokenUrl = tokenUrl;
		}

		public URL getTokenUrl() {
			return tokenUrl;
		}
	}
    
    /**
     * @author mmt
     *
     */
    public static class ClientCredentialsOauthFlow extends AbstractOauthFlow {
    	private final URL tokenUrl;

		public ClientCredentialsOauthFlow(URL tokenUrl, OauthScope...oauthScopes) {
			super(oauthScopes);
			this.tokenUrl = tokenUrl;
		}

		public URL getTokenUrl() {
			return tokenUrl;
		}
	}
    
    /**
     * @author mmt
     *
     */
    public static class AuthorizationCodeOauthFlow extends AbstractOauthFlow {
    	private final URL authorizationUrl;
    	private final URL tokenUrl;
    	
    	public AuthorizationCodeOauthFlow(URL authorizationUrl, URL tokenUrl, OauthScope...oauthScopes) {
			super(oauthScopes);
			this.authorizationUrl = authorizationUrl;
			this.tokenUrl = tokenUrl;
		}

    	public URL getAuthorizationUrl() {
			return authorizationUrl;
		}
    	
		public URL getTokenUrl() {
			return tokenUrl;
		}
	}
}
