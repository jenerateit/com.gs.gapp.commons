/**
 *
 */
package com.gs.gapp.metamodel.function;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ParameterType;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.Type;

/**
 * @author mmt
 *
 */
public class FunctionParameter extends ModelElement {

	/**
	 *
	 */
	private static final long serialVersionUID = 1804796004893528886L;

	private Type type;

	private CollectionType collectionType;

	private ParameterType parameterType;
	
	private Type keyType = null;
	
	private int dimension = 1;
	
	private boolean mandatory = false;

	/**
	 * @param name
	 */
	public FunctionParameter(String name) {
		super(name);
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * @return the collectionType
	 */
	public CollectionType getCollectionType() {
		return collectionType;
	}

	/**
	 * @param collectionType the collectionType to set
	 */
	public void setCollectionType(CollectionType collectionType) {
		this.collectionType = collectionType;
	}

	/**
	 * @return the parameterType
	 */
	public ParameterType getParameterType() {
		return parameterType;
	}

	/**
	 * @param parameterType the parameterType to set
	 */
	public void setParameterType(ParameterType parameterType) {
		this.parameterType = parameterType;
	}


	/**
	 * @return the type of the key if the collection type is keyvalue
	 */
	public Type getKeyType() {
		return keyType;
	}

	/**
	 * @param keyType the type of the key if the collection type is keyvalue
	 */
	public void setKeyType(Type keyType) {
		this.keyType = keyType;
	}
	
	/**
	 * @return the dimension of the collection type if set
	 */
	public int getDimension() {
		return dimension;
	}
	
	/**
	 * @param dimension of the collection type if set
	 */
	public void setDimension(int dimension) {
		this.dimension = dimension;
	}

	/**
	 * @return the mandatory
	 */
	public boolean isMandatory() {
		return mandatory;
	}

	/**
	 * @param mandatory the mandatory to set
	 */
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
}
