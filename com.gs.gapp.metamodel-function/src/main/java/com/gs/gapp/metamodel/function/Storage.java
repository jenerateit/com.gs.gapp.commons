package com.gs.gapp.metamodel.function;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

/**
 * A storage element holds function and persistence modules that serve the purpose to store and retrieve data.
 * 
 * @author mmt
 *
 */
public class Storage extends ModelElement {

	private static final long serialVersionUID = -866661199337493246L;
	
	private final Set<FunctionModule> functionModules = new LinkedHashSet<>();
	private final Set<PersistenceModule> persistenceModules = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public Storage(String name) {
		super(name);
	}

	public Set<FunctionModule> getFunctionModules() {
		return functionModules;
	}

	public void addFunctionModules(FunctionModule... functionModules) {
		if (functionModules != null) {
		    this.functionModules.addAll(Arrays.asList(functionModules));
		}
	}
	
	public Set<PersistenceModule> getPersistenceModules() {
		return persistenceModules;
	}

	public void addPersistenceModules(PersistenceModule... persistenceModules) {
		if (persistenceModules != null) {
		    this.persistenceModules.addAll(Arrays.asList(persistenceModules));
		}
	}
	
	/**
	 * @param entity
	 * @return
	 */
	public boolean contains(Entity entity) {
		for (PersistenceModule persistenceModule : persistenceModules) {
			if (persistenceModule.getEntities().contains(entity)) return true;
		}
		
		return false;
	}
	
	/**
	 * @param persistenceNamespace
	 * @return
	 */
	public boolean contains(com.gs.gapp.metamodel.persistence.Namespace persistenceNamespace) {
		for (PersistenceModule persistenceModule : persistenceModules) {
			if (persistenceModule.getNamespace() == persistenceNamespace) return true;
		}
		
		return false;
	}
	
	/**
	 * @param function
	 * @return
	 */
	public boolean contains(Function function) {
		for (FunctionModule functionModule : functionModules) {
			if (functionModule.getFunctions().contains(function)) return true;
		}
		
		return false;
	}
	
	/**
	 * @param functionNamespace
	 * @return
	 */
	public boolean contains(com.gs.gapp.metamodel.function.Namespace functionNamespace) {
		for (FunctionModule functionModule : functionModules) {
			if (functionModule.getNamespace() == functionNamespace) return true;
		}
		
		return false;
	}
	
	/**
	 * @return
	 */
	public Set<ComplexType> getAllUsedComplexTypes() {
		final LinkedHashSet<ComplexType> result = new LinkedHashSet<>();
		this.getPersistenceModules().forEach(module -> result.addAll(module.getAllUsedComplexTypes()));
		this.getFunctionModules()   .forEach(module -> result.addAll(module.getAllUsedComplexTypes()));
		return result;
	}
}
