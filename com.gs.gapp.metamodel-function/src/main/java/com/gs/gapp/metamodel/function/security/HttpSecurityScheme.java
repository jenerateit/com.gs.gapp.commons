package com.gs.gapp.metamodel.function.security;

public class HttpSecurityScheme extends AbstractSecurityScheme {

	private static final long serialVersionUID = -5099039131812927438L;
	
	private final HttpAuthenticationScheme authenticationScheme;
	
	private String bearerFormat;

	public HttpSecurityScheme(HttpAuthenticationScheme authenticationScheme) {
		super(HttpSecurityScheme.class.getSimpleName());
		this.authenticationScheme = authenticationScheme;
	}
	
	public String getBearerFormat() {
		return bearerFormat;
	}

	public void setBearerFormat(String bearerFormat) {
		this.bearerFormat = bearerFormat;
	}

	public HttpAuthenticationScheme getAuthenticationScheme() {
		return authenticationScheme;
	}

	/**
	 * @see http://www.iana.org/assignments/http-authschemes/http-authschemes.xhtml
	 * @author mmt
	 *
	 */
	public static enum HttpAuthenticationScheme {
		BASIC ("Basic"),
		BEARER ("Bearer"),
		DIGEST ("Digest"),
		HOBA ("HOBA"),
		MUTUAL ("Mutual"),
		NEGOTIATE ("Negotiate"),
		OAUTH ("OAuth"),
		SCRAM_SHA_1 ("SCRAM-SHA-1"),
		SCRAM_SHA_256 ("SCRAM-SHA-256"),
		;
		
		private final String name;
		
		private HttpAuthenticationScheme(String name) {
			this.name= name;
		}

		public String getName() {
			return name;
		}
	}
}
