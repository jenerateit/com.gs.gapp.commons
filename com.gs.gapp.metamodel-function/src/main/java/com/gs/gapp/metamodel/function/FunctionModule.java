/**
 *
 */
package com.gs.gapp.metamodel.function;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.gs.gapp.metamodel.persistence.enums.StorageFunction;

/**
 * @author mmt
 *
 */
public class FunctionModule extends Module {

	/**
	 *
	 */
	private static final long serialVersionUID = 2898208776049989583L;

	private final Set<Function> functions = new LinkedHashSet<>();
	
	private final Set<PersistenceModule> persistenceModules = new LinkedHashSet<>();

	private final Set<Entity> consumedEntities = new LinkedHashSet<>();
	private final Set<FunctionModule> consumedFunctionModules = new LinkedHashSet<>();
	private final Set<ServiceInterface> consumedServiceInterfaces = new LinkedHashSet<>();
	private final Set<Storage> consumedStorages = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public FunctionModule(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#getNamespace()
	 */
	@Override
	public Namespace getNamespace() {
		return (Namespace) super.getNamespace();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#setNamespace(com.gs.gapp.metamodel.basic.Namespace)
	 */
	@Override
	public void setNamespace(com.gs.gapp.metamodel.basic.Namespace namespace) {
		if (!(namespace instanceof Namespace)) {
			throw new IllegalArgumentException("the namespace of a FunctionModule must be an instance of com.gs.gapp.metamodel.function.Namespace");
		}
		super.setNamespace(namespace);
	}

	/**
	 * @return the functions
	 */
	public Set<Function> getFunctions() {
		return functions;
	}


	/**
	 * @param function
	 * @return
	 */
	public boolean addFunction(Function function) {
		boolean result = this.functions.add(function);
		if (result) {
			function.setModule(this);
		}
		return result;
	}

	/**
	 * @return
	 */
	public Set<PersistenceModule> getPersistenceModules() {
		return persistenceModules;
	}
	
	/**
	 * @param persistenceModule
	 * @return
	 */
	public boolean addPersistenceModule(PersistenceModule persistenceModule) {
		return this.persistenceModules.add(persistenceModule);
	}
	
	public Set<Entity> getConsumedEntities() {
		return consumedEntities;
	}
	
	public boolean addConsumedEntity(Entity entity) {
		return this.consumedEntities.add(entity);
	}
	
	public Set<FunctionModule> getConsumedFunctionModules() {
		return consumedFunctionModules;
	}
	
	public boolean addConsumedFunctionModule(FunctionModule functionModule) {
		return this.consumedFunctionModules.add(functionModule);
	}
	
	/**
	 * Being able to associate service interfaces to function modules is a convenience measure.
	 * Sometimes it is easier like this instead of assigning several function modules to
	 * a function module via {@link FunctionModule#consumedFunctionModules}
	 * 
	 * @return
	 */
	public Set<ServiceInterface> getConsumedServiceInterfaces() {
		return Collections.unmodifiableSet(consumedServiceInterfaces);
	}
	
	public boolean addConsumedServiceInterface(ServiceInterface serviceInterface) {
		return this.consumedServiceInterfaces.add(serviceInterface);
	}
	
	/**
	 * Identify and return the function module's function that has the given storage function set.
	 * 
	 * @param storageFunction
	 * @return
	 */
	public Function getFunction(StorageFunction storageFunction) {
		if (storageFunction == null) throw new NullPointerException("parameter 'storageFunction' must not be null");
			
		for (Function function : this.functions) {
			if (function.getStorageFunction() == storageFunction) return function;
		}
		
		return null;
	}

	public Set<Storage> getConsumedStorages() {
		return consumedStorages;
	}
	
	public boolean addConsumedStorage(Storage storage) {
		return this.consumedStorages.add(storage);
	}
	
	/**
	 * @return
	 */
	public Set<ComplexType> getAllUsedComplexTypes() {
		LinkedHashSet<ComplexType> result = new LinkedHashSet<>();
		for (Function function : getFunctions()) {
			result.addAll(function.getAllUsedComplexTypes());
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<ExceptionType> getAllUsedExceptionTypes() {
		LinkedHashSet<ExceptionType> result = new LinkedHashSet<>();
		for (Function function : getFunctions()) {
			result.addAll(function.getBusinessExceptions());
			result.addAll(function.getTechnicalExceptions());
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public Set<Entity> getAllUsedEntities() {
		LinkedHashSet<Entity> result = new LinkedHashSet<>();
		for (Function function : getFunctions()) {
			result.addAll(function.getAllUsedEntities());
		}
		
		return result;
	}
	
	/**
	 * @param entity
	 * @return
	 */
	public boolean uses(Entity entity) {
		for (Function function : this.functions) {
			if (function.getAllUsedComplexTypes().contains(entity)) {
				return true;
			}
		}
		
		return false;
	}
}
