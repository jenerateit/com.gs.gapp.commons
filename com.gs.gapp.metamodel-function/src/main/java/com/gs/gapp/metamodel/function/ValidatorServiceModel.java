package com.gs.gapp.metamodel.function;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs.gapp.metamodel.basic.ModelValidatorI;

/**
 * This validator checks a function model in reagards to contradictions and missing settings that
 * make it non-processable by generator components that generate code for webservice implmentations and clients.
 */
public class ValidatorServiceModel implements ModelValidatorI {

	public ValidatorServiceModel() {}

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(assertValidHttpVerb(modelElements));
		result.addAll(assertValidExceptions(modelElements));
		return result;
	}
	
	public Collection<Message> validateFunction(Function function) {
	    Optional<String> httpVerb = ServiceModelProcessing.getModeledHttpVerb(function);
	    if (httpVerb.isPresent()) {
	    	Set<Message> result = new HashSet<>();
	    	boolean isMultipartConsumed = ServiceModelProcessing.isMultipartConsumed(function);
	    	boolean isMultipartProduced = ServiceModelProcessing.isMultipartProduced(function);
	    	boolean isMultipartUsed = isMultipartConsumed || isMultipartProduced;
	    	Set<FunctionParameter> functionInBodyParameters = ServiceModelProcessing.getFunctionInBodyParameters(function);
	    	Set<FunctionParameter> functionOutBodyParameters = ServiceModelProcessing.getFunctionOutBodyParameters(function);
	    	boolean isIdempotent = ServiceModelProcessing.isIdempotent(function);
	    	
	    	switch (httpVerb.get().toUpperCase()) {
	    	case "GET":
	    		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/GET
	    		if (!functionInBodyParameters.isEmpty()) {
	    			Message errorMessage = FunctionMessage.ERROR_HTTP_VERB_MUST_NOT_HAVE_REQUEST_PARAMETERS_IN_THE_BODY.getMessageBuilder()
		    			.modelElement(function)
		    			.parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
		    			.build();
	    			result.add(errorMessage);
	    		}
	    		if (functionOutBodyParameters.isEmpty()) {
	    			Message warningMessage = FunctionMessage.WARNING_HTTP_VERB_DOESNT_HAVE_RESPONSE_PARAMETERS_IN_THE_BODY.getMessageBuilder()
	    			    .modelElement(function)
	    			    .parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
	    			    .build();
	    			result.add(warningMessage);
	    		}
	    		if (isMultipartConsumed) {
	    			Message errorMessage = FunctionMessage.ERROR_HTTP_GET_MUST_NOT_CONSUME_MULTIPART.getMessageBuilder()
		    			.modelElement(function)
		    			.parameters(function.getName(), function.getModule().getName())
		    			.build();
	    			result.add(errorMessage);
	    		}
	    		break;
	    	case "POST":
	    		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST
	    		if (functionInBodyParameters.isEmpty()) {
	    			Message errorMessage = FunctionMessage.WARNING_HTTP_VERB_DOESNT_HAVE_REQUEST_PARAMETERS_IN_THE_BODY.getMessageBuilder()
	    			    .modelElement(function)
	    			    .parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
	    			    .build();
	    			result.add(errorMessage);
	    		}
	    		if (functionOutBodyParameters.isEmpty()) {
	    			Message warningMessage = FunctionMessage.WARNING_HTTP_VERB_DOESNT_HAVE_RESPONSE_PARAMETERS_IN_THE_BODY.getMessageBuilder()
	    			    .modelElement(function)
	    			    .parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
	    			    .build();
	    			result.add(warningMessage);
	    		}
	    		if (isIdempotent ) {
	    			// TODO error POST cannot be idempotent
	    			Message errorMessage = FunctionMessage.ERROR_HTTP_POST_CANNOT_BE_IDEMPOTENT.getMessageBuilder()
	    			    .modelElement(function)
	    			    .parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
	    			    .build();
	    			result.add(errorMessage);
	    		}
	    		break;
	    	case "DELETE":
	    		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/DELETE
	    		if (isMultipartUsed) {
	    			Message errorMessage = FunctionMessage.ERROR_HTTP_VERB_MUST_NOT_BE_USED_WITH_MULTIPART.getMessageBuilder()
		    			.modelElement(function)
		    			.parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
		    			.build();
	    			result.add(errorMessage);
	    		}
	    		break;
	    	case "HEAD":
	    		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/HEAD
	    		if (!functionInBodyParameters.isEmpty()) {
	    			Message errorMessage = FunctionMessage.ERROR_HTTP_VERB_MUST_NOT_HAVE_REQUEST_PARAMETERS_IN_THE_BODY.getMessageBuilder()
		    			.modelElement(function)
		    			.parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
		    			.build();
	    			result.add(errorMessage);
	    		}
	    		if (!functionOutBodyParameters.isEmpty()) {
	    			Message errorMessage = FunctionMessage.ERROR_HTTP_VERB_MUST_NOT_HAVE_RESPONSE_PARAMETERS_IN_THE_BODY.getMessageBuilder()
		    			.modelElement(function)
		    			.parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
		    			.build();
	    			result.add(errorMessage);
	    		}
	    		if (isMultipartUsed) {
	    			Message errorMessage = FunctionMessage.ERROR_HTTP_VERB_MUST_NOT_BE_USED_WITH_MULTIPART.getMessageBuilder()
		    			.modelElement(function)
		    			.parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
		    			.build();
	    			result.add(errorMessage);
	    		}
	    		break;
	    	case "OPTIONS":
	    		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/OPTIONS
	    		if (!functionInBodyParameters.isEmpty()) {
	    			Message errorMessage = FunctionMessage.ERROR_HTTP_VERB_MUST_NOT_HAVE_REQUEST_PARAMETERS_IN_THE_BODY.getMessageBuilder()
		    			.modelElement(function)
		    			.parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
		    			.build();
	    			result.add(errorMessage);
	    		}
	    		if (isMultipartUsed) {
	    			Message errorMessage = FunctionMessage.ERROR_HTTP_VERB_MUST_NOT_BE_USED_WITH_MULTIPART.getMessageBuilder()
		    			.modelElement(function)
		    			.parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
		    			.build();
	    			result.add(errorMessage);
	    		}
	    		break;
	    	case "PUT":
	    		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PUT
	    		if (isMultipartUsed) {
	    			Message errorMessage = FunctionMessage.ERROR_HTTP_VERB_MUST_NOT_BE_USED_WITH_MULTIPART.getMessageBuilder()
		    			.modelElement(function)
		    			.parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
		    			.build();
	    			result.add(errorMessage);
	    		}
	    		if (functionInBodyParameters.isEmpty()) {
	    			Message errorMessage = FunctionMessage.WARNING_HTTP_VERB_DOESNT_HAVE_REQUEST_PARAMETERS_IN_THE_BODY.getMessageBuilder()
	    			    .modelElement(function)
	    			    .parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
	    			    .build();
	    			result.add(errorMessage);
	    		}
	    		break;
	    	case "PATCH":
	    		// https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PATCH
	    		if (functionInBodyParameters.isEmpty()) {
	    			Message errorMessage = FunctionMessage.WARNING_HTTP_VERB_DOESNT_HAVE_REQUEST_PARAMETERS_IN_THE_BODY.getMessageBuilder()
	    			    .modelElement(function)
	    			    .parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
	    			    .build();
	    			result.add(errorMessage);
	    		}
	    		if (isMultipartUsed) {
	    			Message errorMessage = FunctionMessage.ERROR_HTTP_VERB_MUST_NOT_BE_USED_WITH_MULTIPART.getMessageBuilder()
		    			.modelElement(function)
		    			.parameters(function.getName(), function.getModule().getName(), httpVerb.get().toUpperCase())
		    			.build();
	    			result.add(errorMessage);
	    		}
	    		break;
	    	default:
	    		break;
	    	}
	    	
	    	return result;
	    }
	    
	    return Collections.emptySet();
	}

	/**
	 * Checks, whether every function has a valid HTTP verb set.
	 * 
	 * <p>Example for an _invalid_ HTTP verb:
	 * The function has input parameters that are supposed to be part of the request body and the modeled verb is GET.
	 * 
	 * @param rawElements
	 * @return messages that are related to validated functions
	 */
	private Collection<Message> assertValidHttpVerb(Collection<Object> rawElements) {
		return rawElements.stream().filter(Function.class::isInstance)
		    .map(Function.class::cast)
		    .flatMap(function -> validateFunction(function).stream())
		    .collect(Collectors.toSet());
	}
	
	private Collection<Message> assertValidExceptions(Collection<Object> rawElements) {
		final LinkedHashSet<Message> result = new LinkedHashSet<>();
		final LinkedHashSet<BusinessException>  businessExceptions  = new LinkedHashSet<>();
		final LinkedHashSet<TechnicalException> technicalExceptions = new LinkedHashSet<>();
		
		rawElements.stream().filter(Function.class::isInstance)
		    .map(Function.class::cast)
		    .forEach(function -> {
		    	businessExceptions.addAll(function.getBusinessExceptions());
		    	technicalExceptions.addAll(function.getTechnicalExceptions());
		    });
		
		result.addAll( businessExceptions.stream()
			.filter(businessException -> {
				return
						ServiceModelProcessing.getModeledStatusCode(businessException).isPresent() &&
				        (ServiceModelProcessing.getModeledStatusCode(businessException).get() < 400L
				        ||
				        ServiceModelProcessing.getModeledStatusCode(businessException).get() > 499L);
				        
				}).map(businessException -> FunctionMessage.WARNING_BUSINESS_EXCEPTION_HAS_WRONG_HTTP_STATUS_CODE.getMessageBuilder()
						.modelElement(businessException)
						.parameters(businessException.getName(),
								    ServiceModelProcessing.getModeledStatusCode(businessException).get())
						.build()).collect(Collectors.toCollection(LinkedHashSet::new)) );
		
		result.addAll( technicalExceptions.stream()
				.filter(technicalException -> {
					return
							ServiceModelProcessing.getModeledStatusCode(technicalException).isPresent() &&
					        ServiceModelProcessing.getModeledStatusCode(technicalException).get() >= 400L &&
					        ServiceModelProcessing.getModeledStatusCode(technicalException).get() <= 499L;
					        
					}).map(technicalException -> FunctionMessage.WARNING_TECHNICAL_EXCEPTION_HAS_WRONG_HTTP_STATUS_CODE.getMessageBuilder()
							.modelElement(technicalException)
							.parameters(technicalException.getName(),
									    ServiceModelProcessing.getModeledStatusCode(technicalException).get())
							.build()).collect(Collectors.toCollection(LinkedHashSet::new)) );
		
		return result;
	}
}
