package com.gs.gapp.metamodel.function;

import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;

public class TechnicalException extends ExceptionType {

	private static final long serialVersionUID = 7195100445001398368L;

	public TechnicalException(String name) {
		super(name);
	}

}
