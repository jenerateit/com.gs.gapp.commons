/**
 *
 */
package com.gs.gapp.metamodel.function;


/**
 * @author mmt
 *
 */
public class Namespace extends com.gs.gapp.metamodel.persistence.Namespace {

	/**
	 *
	 */
	private static final long serialVersionUID = 7609130794144048820L;

	/**
	 * @param name
	 */
	public Namespace(String name) {
		super(name);
	}

}
