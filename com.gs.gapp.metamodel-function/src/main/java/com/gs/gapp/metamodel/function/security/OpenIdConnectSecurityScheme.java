package com.gs.gapp.metamodel.function.security;

import java.net.URL;

public class OpenIdConnectSecurityScheme extends AbstractSecurityScheme {

	private static final long serialVersionUID = 6551257899814182441L;
	
	private final URL openIdConnectUrl;

	public OpenIdConnectSecurityScheme(URL openIdConnectUrl) {
		super(OpenIdConnectSecurityScheme.class.getSimpleName());
		this.openIdConnectUrl = openIdConnectUrl;
	}

	public URL getOpenIdConnectUrl() {
		return openIdConnectUrl;
	}
}
