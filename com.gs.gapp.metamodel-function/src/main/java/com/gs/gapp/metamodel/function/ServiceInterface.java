package com.gs.gapp.metamodel.function;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

/**
 * A service interface element references some function modules, persistence modules and entities.
 * All such referenced model elements as a whole define a service interface. Note that entities
 * can easily be converted to function modules. That's the reason why persistence modules and entities
 * are referenced, too.
 * 
 * A service interface can be referenced from {@link ServiceClient} and {@link ServiceImplementation} objects.
 * There can be more than one {@link ServiceClient} and more than one {@link ServiceImplementation} object that
 * references a ServiceInterface object.
 * 
 * @author mmt
 *
 */
public class ServiceInterface extends ModelElement {

	private static final long serialVersionUID = -866661199337493246L;
	
	private final Set<FunctionModule> functionModules = new LinkedHashSet<>();
	private final Set<PersistenceModule> persistenceModules = new LinkedHashSet<>();
	private final Set<Entity> entities = new LinkedHashSet<>();
	
	private final Set<ServiceImplementation> serviceImplementations = new LinkedHashSet<>();
	
	private final Set<ServiceClient> serviceClients = new LinkedHashSet<>();
	
	private URL termsOfService;
	
	private Contact contact;
	
	private License license;
	
	/**
	 * @param name
	 */
	public ServiceInterface(String name) {
		super(name);
	}

	public Set<FunctionModule> getFunctionModules() {
		return functionModules;
	}

	public void addFunctionModules(FunctionModule... functionModules) {
		if (functionModules != null) {
		    this.functionModules.addAll(Arrays.asList(functionModules));
		}
	}
	
	public Set<PersistenceModule> getPersistenceModules() {
		return persistenceModules;
	}

	public void addPersistenceModules(PersistenceModule... persistenceModules) {
		if (persistenceModules != null) {
		    this.persistenceModules.addAll(Arrays.asList(persistenceModules));
		}
	}
	
	public Set<Entity> getEntities() {
		return entities;
	}

	public void addEntities(Entity... entities) {
		if (entities != null) {
		    this.entities.addAll(Arrays.asList(entities));
		}
	}
	
	public Set<ServiceImplementation> getServiceImplementations() {
		return serviceImplementations;
	}

	public void addServiceImplementations(ServiceImplementation... serviceImplementations) {
		if (serviceImplementations != null) {
		    this.serviceImplementations.addAll(Arrays.asList(serviceImplementations));
		}
	}
	
	public Set<ServiceClient> getServiceClients() {
		return serviceClients;
	}

	public void addServiceClients(ServiceClient... serviceClients) {
		if (serviceClients != null) {
		    this.serviceClients.addAll(Arrays.asList(serviceClients));
		}
	}

	public URL getTermsOfService() {
		return termsOfService;
	}

	public void setTermsOfService(URL termsOfService) {
		this.termsOfService = termsOfService;
	}
	
	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public License getLicense() {
		return license;
	}

	public void setLicense(License license) {
		this.license = license;
	}
	
	/**
	 * @param entity
	 * @return
	 */
	public boolean contains(Entity entity) {
		for (PersistenceModule persistenceModule : persistenceModules) {
			if (persistenceModule.getEntities().contains(entity)) return true;
		}

		if (entities.contains(entity)) return true;
		
		return false;
	}
	
	/**
	 * @param entity
	 * @return
	 */
	public boolean containsFunctionParameterType(Entity entity) {
		// alternatively, the entity can be a parameter of  a function
		for (FunctionModule functionModule : functionModules) {
			for (Function function : functionModule.getFunctions()) {
				for (FunctionParameter param : function.getFunctionInParameters()) {
					if (param.getType() == entity) {
						return true;
					}
				}
                for (FunctionParameter param : function.getFunctionOutParameters()) {
                	if (param.getType() == entity) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	/**
	 * @param exceptionType
	 * @return
	 */
	public boolean usesExceptionType(ExceptionType exceptionType) {
		for (FunctionModule functionModule : functionModules) {
			for (Function function : functionModule.getFunctions()) {
				for (BusinessException businessException : function.getBusinessExceptions()) {
					if (businessException == exceptionType || businessException.getOriginatingElement() == exceptionType) {
						return true;
					}
				}
				for (TechnicalException technicalException : function.getTechnicalExceptions()) {
					if (technicalException == exceptionType || technicalException.getOriginatingElement() == exceptionType) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	/**
	 * @param persistenceNamespace
	 * @return
	 */
	public boolean contains(com.gs.gapp.metamodel.persistence.Namespace persistenceNamespace) {
		for (PersistenceModule persistenceModule : persistenceModules) {
			if (persistenceModule.getNamespace() == persistenceNamespace) return true;
		}
		
		return false;
	}
	
	/**
	 * @param function
	 * @return
	 */
	public boolean contains(Function function) {
		for (FunctionModule functionModule : getAllFunctionModules()) {
			if (functionModule.getFunctions().contains(function)) return true;
		}
		
		return false;
	}
	
	/**
	 * @param functionNamespace
	 * @return
	 */
	public boolean contains(com.gs.gapp.metamodel.function.Namespace functionNamespace) {
		for (FunctionModule functionModule : getAllFunctionModules()) {
			if (functionModule.getNamespace() == functionNamespace) return true;
		}
		
		return false;
	}
	
	/**
	 * @param enumeration
	 * @return
	 */
	public boolean usesEnumeration(Enumeration enumeration) {
		for (FunctionModule functionModule : getAllFunctionModules()) {
			Collection<?> allUsedComplexTypes = functionModule.getAllUsedComplexTypes();
			if (allUsedComplexTypes.contains(enumeration)) {
				return true;
			}
		}
		
		for (PersistenceModule persistenceModule : getPersistenceModules()) {
			for (Entity entity : persistenceModule.getEntities()) {
				Collection<?> allUsedComplexTypes = entity.getAllUsedComplexTypes();
				if (allUsedComplexTypes.contains(enumeration)) {
					return true;
				}
			}
		}
		
		for (Entity entity : entities) {
			Collection<?> allUsedComplexTypes = entity.getAllUsedComplexTypes();
			if (allUsedComplexTypes.contains(enumeration)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * @param complexType
	 * @return
	 */
	public boolean usesComplexType(ComplexType complexType) {
		for (FunctionModule functionModule : getAllFunctionModules()) {
			Collection<?> allUsedComplexTypes = functionModule.getAllUsedComplexTypes();
			if (allUsedComplexTypes.contains(complexType)) {
				return true;
			}
		}
		
		for (PersistenceModule persistenceModule : getPersistenceModules()) {
			for (Entity entity : persistenceModule.getEntities()) {
				Collection<ComplexType> allUsedComplexTypes = new LinkedHashSet<>();
				allUsedComplexTypes.add(entity);
				allUsedComplexTypes.addAll(entity.getAllUsedComplexTypes());
				if (allUsedComplexTypes.contains(complexType)) {
					return true;
				}
			}
		}
		
		for (Entity entity : entities) {
			Collection<ComplexType> allUsedComplexTypes = new LinkedHashSet<>();
			allUsedComplexTypes.add(entity);
			allUsedComplexTypes.addAll(entity.getAllUsedComplexTypes());
			if (allUsedComplexTypes.contains(complexType)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * @return
	 */
	public Collection<FunctionModule> getAllFunctionModules() {
		Collection<FunctionModule> result = new LinkedHashSet<>();
		
		result.addAll(functionModules);
		for (PersistenceModule persistenceModule : persistenceModules) {
			for (Entity entity : persistenceModule.getEntities()) {
				if (entity.getSingleExtensionElement(FunctionModule.class) != null) {
					result.add(entity.getSingleExtensionElement(FunctionModule.class));
				}
			}
		}
		for (Entity entity : entities) {
			if (entity.getSingleExtensionElement(FunctionModule.class) != null) {
				result.add(entity.getSingleExtensionElement(FunctionModule.class));
			}
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public Collection<ComplexType> getAllUsedComplexTypes() {
		LinkedHashSet<ComplexType> result = new LinkedHashSet<>();
		for (FunctionModule functionModule : getAllFunctionModules()) {
			result.addAll(functionModule.getAllUsedComplexTypes());
		}
		
		for (PersistenceModule persistenceModule : persistenceModules) {
			for (Entity entity : persistenceModule.getEntities()) {
				result.add(entity);
				result.addAll(entity.getAllUsedComplexTypes());
			}
		}
		
		for (Entity entity : entities) {
			result.add(entity);
			result.addAll(entity.getAllUsedComplexTypes());
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public Collection<ExceptionType> getAllUsedExceptionTypes() {
		LinkedHashSet<ExceptionType> result = new LinkedHashSet<>();
		for (FunctionModule functionModule : getAllFunctionModules()) {
			result.addAll(functionModule.getAllUsedExceptionTypes());
		}
		
		for (PersistenceModule persistenceModule : persistenceModules) {
			for (Entity entity : persistenceModule.getEntities()) {
				FunctionModule functionModule = entity.getSingleExtensionElement(FunctionModule.class);
				if (functionModule != null) {
					result.addAll(functionModule.getAllUsedExceptionTypes());
				}
			}
		}
		
		return result;
	}
	
	/**
	 * @return a map that hold lists of functions where more than one function has the same name
	 */
	public Map<String,List<Function>> getFunctionsWithAmbiguousNaming() {
		LinkedHashMap<String,List<Function>> result = new LinkedHashMap<>();
		
		for (FunctionModule functionModule : this.getAllFunctionModules()) {
			for (Function function : functionModule.getFunctions()) {
				List<Function> functions = result.get(function.getName());
			    if (functions == null) {
			    	functions = new ArrayList<>();
			    	result.put(function.getName(), functions);
			    }
			    
			    functions.add(function);
			}
		}
		
		ArrayList<String> functionNamesWithSingleMethodOnly = new ArrayList<>();
		for (Entry<String, List<Function>> entry : result.entrySet()) {
			if (entry.getValue().size() <= 1) {
				functionNamesWithSingleMethodOnly.add(entry.getKey());
			}
		}
		result.keySet().removeAll(functionNamesWithSingleMethodOnly);
		
		return result;
	}
	
	/**
	 * @param function
	 * @return
	 */
	public boolean isFunctionNameAmbiguous(Function function) {
		
		// --- if there is only one function module, the names cannot be ambiguous => immediately return false (better performance)
		if (this.getAllFunctionModules().size() <= 1) {
			return false;
		}
		
		List<Function> functions = getFunctionsWithAmbiguousNaming().get(function.getName());
		if (functions != null && functions.contains(function)) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * @return
	 */
	public boolean isMultipartConsumed() {
		Optional<Function> functionConsumingMultipart = getAllFunctionModules().stream()
		    .flatMap(functionModule -> functionModule.getFunctions().stream())
		    .filter(function -> ServiceModelProcessing.isMultipartConsumed(function))
		    .findFirst();
		    
		return functionConsumingMultipart.isPresent();
	}
	
	/**
	 * @return
	 */
	public boolean isMultipartFormDataConsumed() {
		Optional<Function> functionConsumingMultipartFormData = getAllFunctionModules().stream()
		    .flatMap(functionModule -> functionModule.getFunctions().stream())
		    .filter(function -> ServiceModelProcessing.isMultipartFormDataConsumed(function))
		    .findFirst();
		    
		return functionConsumingMultipartFormData.isPresent();
	}
	
	/**
	 * @return
	 */
	public boolean isMultipartMixedConsumed() {
		Optional<Function> functionConsumingMultipartMixed = getAllFunctionModules().stream()
		    .flatMap(functionModule -> functionModule.getFunctions().stream())
		    .filter(function -> ServiceModelProcessing.isMultipartMixedConsumed(function))
		    .findFirst();
		    
		return functionConsumingMultipartMixed.isPresent();
	}
	
	/**
	 * @return
	 */
	public boolean isMultipartRelatedConsumed() {
		Optional<Function> functionConsumingMultipartRelated = getAllFunctionModules().stream()
		    .flatMap(functionModule -> functionModule.getFunctions().stream())
		    .filter(function -> ServiceModelProcessing.isMultipartRelatedConsumed(function))
		    .findFirst();
		    
		return functionConsumingMultipartRelated.isPresent();
	}
	
	/**
	 * @return
	 */
	public boolean isFormUrlEncodedConsumed() {
		Optional<Function> functionConsumingFormUrlEncoded = getAllFunctionModules().stream()
		    .flatMap(functionModule -> functionModule.getFunctions().stream())
		    .filter(function -> ServiceModelProcessing.isFormUrlEncodedConsumed(function))
		    .findFirst();
		    
		return functionConsumingFormUrlEncoded.isPresent();
	}

    /**
     * @return
     */
    public boolean isMultipartProduced() {
    	Optional<Function> functionProducingMultipart = getAllFunctionModules().stream()
		    .flatMap(functionModule -> functionModule.getFunctions().stream())
		    .filter(function -> ServiceModelProcessing.isMultipartProduced(function))
		    .findFirst();
    		    
    	return functionProducingMultipart.isPresent();
	}
    
    /**
	 * @return
	 */
	public boolean isMultipartFormDataProduced() {
		Optional<Function> functionProducingMultipartFormData = getAllFunctionModules().stream()
		    .flatMap(functionModule -> functionModule.getFunctions().stream())
		    .filter(function -> ServiceModelProcessing.isMultipartFormDataProduced(function))
		    .findFirst();
		    
		return functionProducingMultipartFormData.isPresent();
	}
	
	/**
	 * @return
	 */
	public boolean isMultipartMixedProduced() {
		Optional<Function> functionProducingMultipartMixed = getAllFunctionModules().stream()
		    .flatMap(functionModule -> functionModule.getFunctions().stream())
		    .filter(function -> ServiceModelProcessing.isMultipartMixedProduced(function))
		    .findFirst();
		    
		return functionProducingMultipartMixed.isPresent();
	}
	
	/**
	 * @return
	 */
	public boolean isMultipartRelatedProduced() {
		Optional<Function> functionProducingMultipartRelated = getAllFunctionModules().stream()
		    .flatMap(functionModule -> functionModule.getFunctions().stream())
		    .filter(function -> ServiceModelProcessing.isMultipartRelatedProduced(function))
		    .findFirst();
		    
		return functionProducingMultipartRelated.isPresent();
	}
	
	/**
	 * @return
	 */
	public boolean isFormUrlEncodedProduced() {
		Optional<Function> functionProducingFormUrlEncoded = getAllFunctionModules().stream()
		    .flatMap(functionModule -> functionModule.getFunctions().stream())
		    .filter(function -> ServiceModelProcessing.isFormUrlEncodedProduced(function))
		    .findFirst();
		    
		return functionProducingFormUrlEncoded.isPresent();
	}

	/**
	 * @author mmt
	 *
	 */
	public static class Contact {
		private String name;
		private URL url;
		private String email;
		
		public Contact() {}
		
		public Contact(String name, URL url, String email) {
			super();
			this.name = name;
			this.url = url;
			this.email = email;
		}

		public String getName() {
			return name;
		}

		public URL getUrl() {
			return url;
		}

		public String getEmail() {
			return email;
		}

		public void setName(String name) {
			this.name = name;
		}

		public void setUrl(URL url) {
			this.url = url;
		}

		public void setEmail(String email) {
			this.email = email;
		}
	}
	
	/**
	 * @author mmt
	 *
	 */
	public static class License {
		private final String name;
		private URL url;
		
		public License(String name, URL url) {
			super();
			if (name == null || name.length() == 0) throw new IllegalArgumentException("parameter 'name' must neither be null nor an empty string");
			this.name = name;
			this.url = url;
		}

		public URL getUrl() {
			return url;
		}

		public void setUrl(URL url) {
			this.url = url;
		}

		public String getName() {
			return name;
		}
	}
}
