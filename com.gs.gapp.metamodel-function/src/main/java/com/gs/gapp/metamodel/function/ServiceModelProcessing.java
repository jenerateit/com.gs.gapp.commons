package com.gs.gapp.metamodel.function;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.gs.gapp.dsl.jaxrs.ContextParamTypeEnum;
import com.gs.gapp.dsl.jaxrs.JaxrsOptionEnum;
import com.gs.gapp.dsl.rest.MediaTypeEnum;
import com.gs.gapp.dsl.rest.PurposeEnum;
import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.dsl.rest.RestOptionEnum.ParamTypeEnum;
import com.gs.gapp.dsl.rest.TransportTypeEnum;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.ParameterType;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveType;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.basic.typesystem.Type;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.enums.StorageFunction;

/**
 * @author mmt
 *
 */
public class ServiceModelProcessing {
	
	/**
	 * @param function
	 * @return
	 */
	public static List<ContextParamTypeEnum> getContextParamTypes(Function function) {
		final List<ContextParamTypeEnum> result = new ArrayList<>();
		OptionDefinition<String>.OptionValue contextParamTypesOptionValue =
				function.getOption(JaxrsOptionEnum.CONTEXT_PARAM_TYPES.getName(), String.class);
		if (contextParamTypesOptionValue != null) {
			for (String aContextParamType : contextParamTypesOptionValue.getOptionValues()) {
				ContextParamTypeEnum contextParamTypeEnum = ContextParamTypeEnum.getByName(aContextParamType);
				if (contextParamTypeEnum != null) {
					result.add(contextParamTypeEnum);
				}
			}
		}
		
		return result;
	}
	
	/**
	 * @param function
	 * @return
	 */
	public static Optional<String> getModeledHttpVerb(Function function) {
		OptionDefinition<String>.OptionValue operationOptionValue = function.getOption(RestOptionEnum.OPERATION.getName(), String.class);
		if (operationOptionValue != null && operationOptionValue.getOptionValue() != null && operationOptionValue.getOptionValue().length() > 0) {
			return Optional.of(operationOptionValue.getOptionValue());
		}
		return Optional.empty();
	}
	
	/**
	 * @param function
	 * @return
	 */
	public static Optional<Long> getModeledSuccessStatusCode(Function function) {
		OptionDefinition<Long>.OptionValue statusCodeOptionValue = function.getOption(RestOptionEnum.STATUS_CODE.getName(), Long.class);
		if (statusCodeOptionValue != null && statusCodeOptionValue.getOptionValue() != null) {
			return Optional.of(statusCodeOptionValue.getOptionValue());
		}
		return Optional.empty();
	}
	
	/**
	 * @param exceptionType
	 * @return
	 */
	public static Optional<Long> getModeledStatusCode(ExceptionType exceptionType) {
		OptionDefinition<Long>.OptionValue statusCodeOptionValue = exceptionType.getOption(RestOptionEnum.STATUS_CODE.getName(), Long.class);
		if (statusCodeOptionValue != null && statusCodeOptionValue.getOptionValue() != null) {
			return Optional.of(statusCodeOptionValue.getOptionValue());
		}
		return Optional.empty();
	}
	
	/**
	 * @param function
	 * @return
	 */
	public static boolean isIdempotent(Function function) {
		OptionDefinition<Boolean>.OptionValue idempotent = function.getOption(RestOptionEnum.IDEMPOTENT.getName(), Boolean.class);
		if (idempotent != null && idempotent.getOptionValue() != null && idempotent.getOptionValue() == true) {
			return true;
		}
		return false;
	}
	
	/**
	 * @param function
	 * @return
	 */
	public static Optional<PurposeEnum> getPurpose(Function function) {
		Optional<PurposeEnum> result = Optional.empty();
		OptionDefinition<String>.OptionValue purpose = function.getOption(RestOptionEnum.PURPOSE.getName(), String.class);
		if (purpose != null && purpose.getOptionValue() != null) {
			// --- the http verb is going to be derived from purpose and idempotent settings
			PurposeEnum purposeEnum = PurposeEnum.getByName(purpose.getOptionValue());
			if (purposeEnum != null) {
				result = Optional.of(purposeEnum);
			}
		}
		
	    return result;
	}
	
	/**
     * @param function
     * @param paramType
     * @return
     */
    public static Set<FunctionParameter> getFunctionInParameters(Function function, RestOptionEnum.ParamTypeEnum paramType) {
    	Set<FunctionParameter> result = new LinkedHashSet<>();
    	
    	for (FunctionParameter inParameter : function.getFunctionInParameters()) {
    		RestOptionEnum.ParamTypeEnum paramTypeEnumEntry = RestOptionEnum.ParamTypeEnum.BODY;  // That's the default, in case the model doesn't contain information about a parameter's type.
    		OptionDefinition<String>.OptionValue paramTypeOptionValue = inParameter.getOption(RestOptionEnum.PARAM_TYPE.getName(), String.class);
    		if (paramTypeOptionValue != null && paramTypeOptionValue.getOptionValue() != null) {
    			paramTypeEnumEntry = RestOptionEnum.ParamTypeEnum.getByName(paramTypeOptionValue.getOptionValue());
    		}
    		if (paramTypeEnumEntry == paramType) {
    			result.add(inParameter);
    		}
		}
    	
    	return result;
    }
    
    /**
     * @param function
     * @param paramTypes
     * @return
     */
    public static Set<FunctionParameter> getFunctionInParameters(Function function, RestOptionEnum.ParamTypeEnum... paramTypes) {
    	final Set<FunctionParameter> result = new LinkedHashSet<>();
    	
    	if (paramTypes != null && paramTypes.length > 0) {
    		Stream.of(paramTypes).forEach(paramType -> result.addAll(getFunctionInParameters(function, paramType)));
    	} else {
    		result.addAll(function.getFunctionInParameters());
    	}
    	
    	return result;
    }
    
    /**
     * @param function
     * @param transportType
     * @return
     */
    public static Set<FunctionParameter> getFunctionOutParameters(Function function, TransportTypeEnum transportType) {
    	Set<FunctionParameter> result = new LinkedHashSet<>();
    	
    	for (FunctionParameter outParameter : function.getFunctionOutParameters()) {
    		TransportTypeEnum transportTypeEnumEntry = TransportTypeEnum.BODY;  // that's the default
    		OptionDefinition<String>.OptionValue transportTypeOptionValue = outParameter.getOption(RestOptionEnum.TRANSPORT_TYPE.getName(), String.class);
    		if (transportTypeOptionValue != null && transportTypeOptionValue.getOptionValue() != null) {
    			transportTypeEnumEntry = TransportTypeEnum.getByName(transportTypeOptionValue.getOptionValue());
    		}
    		if (transportTypeEnumEntry == transportType) {
    			result.add(outParameter);
    		}
		}
    	
    	return result;
    }
    
    /**
     * @param function
     * @return
     */
    public static Set<String> getConsumedMultipartMediatypes(Function function) {
    	return getConsumedMediatypes(function).stream()
    	    .filter(mediaType -> mediaType.startsWith("multipart/"))
    	    .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public static Set<String> getModeledConsumedMediatypes(FunctionParameter param, String pattern) {
    	return getModeledConsumedMediatypes(param).stream()
    		      .filter(mediaType -> pattern != null && mediaType.toLowerCase().startsWith(pattern.toLowerCase()))
    		      .collect(Collectors.toCollection(LinkedHashSet::new));
    }
    
    public static Set<String> getModeledConsumedMediatypes(Function function, String pattern) {
    	return getModeledConsumedMediatypes(function).stream()
    		      .filter(mediaType -> pattern != null && mediaType.toLowerCase().startsWith(pattern.toLowerCase()))
    		      .collect(Collectors.toCollection(LinkedHashSet::new));
    }
    
    public static Set<String> getModeledConsumedMediatypes(FunctionModule functionModule, String pattern) {
    	return getModeledConsumedMediatypes(functionModule).stream()
    		      .filter(mediaType -> pattern != null && mediaType.toLowerCase().startsWith(pattern.toLowerCase()))
    		      .collect(Collectors.toCollection(LinkedHashSet::new));
    }
    
	public static Set<String> getConsumedMediatypes(Function function) {
		Set<String> consumesMediaTypesAsStrings = new LinkedHashSet<>();		
		Set<FunctionParameter> functionInBodyOrPartParameters = getFunctionInParameters(function, ParamTypeEnum.BODY, ParamTypeEnum.PART);
		Set<String> modeledConsumedMediatypes = getModeledConsumedMediatypes(function);
		if (functionInBodyOrPartParameters.size() > 1) {
			// AJAX-196 we need to consume a multipart media type, but if is already set, we do not have to set it again
			if (modeledConsumedMediatypes.stream()
				.filter(mediatype -> mediatype.startsWith("multipart/") || "application/x-www-form-urlencoded".equals(mediatype))
				.findAny()
				.isPresent()) {
				consumesMediaTypesAsStrings.addAll(modeledConsumedMediatypes);
			} else {
				// This is the default in case there is no specific information provided in the model.
			    consumesMediaTypesAsStrings.add(MediaTypeEnum.MULTIPART_FORMDATA.getMediaTypeAsString());
			}
		} else {
			consumesMediaTypesAsStrings.addAll(modeledConsumedMediatypes);
		}
		return consumesMediaTypesAsStrings;
	}
	
	/**
	 * @param function
	 * @param pattern
	 * @return
	 */
	public static Set<String> getConsumedMediatypes(Function function, String pattern) {
		return getConsumedMediatypes(function).stream()
		    .filter(mediaType -> pattern != null && mediaType.toLowerCase().startsWith(pattern.toLowerCase()))
		    .collect(Collectors.toCollection(LinkedHashSet::new));
	}
    
	public static Set<String> getModeledConsumedMediatypes(Function function) {
		final Set<String> result = new LinkedHashSet<>();
		OptionDefinition<String>.OptionValue consumesMediaTypes = function.getOption(RestOptionEnum.CONSUMES.getName(), String.class);
		if (consumesMediaTypes != null) {
			for (String mediaTypeAsString : consumesMediaTypes.getOptionValues()) {
				MediaTypeEnum mediaTypeEnum = MediaTypeEnum.get(mediaTypeAsString);
				if (mediaTypeEnum != null) {
				    result.add(mediaTypeEnum.getMediaTypeAsString());
				} else {
					result.add(mediaTypeAsString);
				}
			}
		}
		
		// --- TODO Currently, we don't have any validation for modeled media types. (mmt 06-Apr-2022)
		OptionDefinition<String>.OptionValue optionValueConsumedMediaTypes =
				function.getOption(com.gs.vd.modeler.dsl.descriptor.rest.FunctionDescriptor.OptionDescriptor.CONSUMEDMEDIATYPES.getCode(), String.class);
		if (optionValueConsumedMediaTypes != null) {
		    optionValueConsumedMediaTypes.getOptionValues().forEach(mediaType -> result.add(mediaType));
		}
		
		return result;
	}
	
	/**
	 * @param parameter
	 * @return
	 */
	public static Set<String> getModeledConsumedMediatypes(FunctionParameter parameter) {
		final Set<String> result = new LinkedHashSet<>();
		OptionDefinition<String>.OptionValue consumesMediaTypes = parameter.getOption(RestOptionEnum.CONSUMES.getName(), String.class);
		if (consumesMediaTypes != null) {
			for (String mediaTypeAsString : consumesMediaTypes.getOptionValues()) {
				MediaTypeEnum mediaTypeEnum = MediaTypeEnum.get(mediaTypeAsString);
				if (mediaTypeEnum != null) {
				    result.add(mediaTypeEnum.getMediaTypeAsString());
				} else {
					result.add(mediaTypeAsString);
				}
			}
		}
		
		// --- TODO Currently, we don't have any validation for modeled media types. (mmt 06-Apr-2022)
		OptionDefinition<String>.OptionValue optionValueConsumedMediaTypes =
				parameter.getOption(com.gs.vd.modeler.dsl.descriptor.rest.FunctionDescriptor.OptionDescriptor.CONSUMEDMEDIATYPES.getCode(), String.class);
		if (optionValueConsumedMediaTypes != null) {
		    optionValueConsumedMediaTypes.getOptionValues().forEach(mediaType -> result.add(mediaType));
		}
		
		return result;
	}
	
	/**
	 * @param functionModule
	 * @return
	 */
	public static Set<String> getModeledConsumedMediatypes(FunctionModule functionModule) {
		final Set<String> result = new LinkedHashSet<>();
		OptionDefinition<String>.OptionValue consumesMediaTypes = functionModule.getOption(RestOptionEnum.CONSUMES.getName(), String.class);
		if (consumesMediaTypes != null) {
			for (String mediaTypeAsString : consumesMediaTypes.getOptionValues()) {
				MediaTypeEnum mediaTypeEnum = MediaTypeEnum.get(mediaTypeAsString);
				if (mediaTypeEnum != null) {
				    result.add(mediaTypeEnum.getMediaTypeAsString());
				} else {
					result.add(mediaTypeAsString);
				}
			}
		}
		
		// --- TODO Currently, we don't have any validation for modeled media types. (mmt 06-Apr-2022)
		OptionDefinition<String>.OptionValue optionValueConsumedMediaTypes =
				functionModule.getOption(com.gs.vd.modeler.dsl.descriptor.rest.FunctionDescriptor.OptionDescriptor.CONSUMEDMEDIATYPES.getCode(), String.class);
		if (optionValueConsumedMediaTypes != null) {
		    optionValueConsumedMediaTypes.getOptionValues().forEach(mediaType -> result.add(mediaType));
		}
		
		return result;
	}
	
	public static Set<String> getModeledProducedMediatypes(FunctionModule functionModule) {
		OptionDefinition<String>.OptionValue producesMediaTypes = functionModule.getOption(RestOptionEnum.PRODUCES.getName(), String.class);
		Set<String> producesMediaTypesAsStrings = new LinkedHashSet<>();
		if (producesMediaTypes != null && producesMediaTypes.getOptionValues() != null && producesMediaTypes.getOptionValues().size() > 0) {
			for (String mediaTypeAsString : producesMediaTypes.getOptionValues()) {
				MediaTypeEnum mediaTypeEnum = MediaTypeEnum.get(mediaTypeAsString);
				if (mediaTypeEnum != null) {
				    producesMediaTypesAsStrings.add(mediaTypeEnum.getMediaTypeAsString());
				} else {
					producesMediaTypesAsStrings.add(mediaTypeAsString);
				}
			}
		}
		
		// --- TODO Currently, we don't have any validation for modeled media types. (mmt 18-Jul-2022)
		OptionDefinition<String>.OptionValue optionValueProducedMediaTypes =
				functionModule.getOption(com.gs.vd.modeler.dsl.descriptor.rest.FunctionDescriptor.OptionDescriptor.PRODUCEDMEDIATYPES.getCode(), String.class);
		if (optionValueProducedMediaTypes != null) {
		    optionValueProducedMediaTypes.getOptionValues().forEach(mediaType -> producesMediaTypesAsStrings.add(mediaType));
		}
		
		return producesMediaTypesAsStrings;
	}

	public static Set<String> getProducedMediatypes(Function function) {
		Set<String> producesMediaTypesAsStrings = new LinkedHashSet<>();
		Set<FunctionParameter> functionOutBodyParameters = getFunctionOutParameters(function, TransportTypeEnum.BODY);
		Set<String> modeledProducedMediatypes = getModeledProducedMediatypes(function);
		if (functionOutBodyParameters.size() > 1) {
			// AJAX-196 we need to produce a multipart media type, but if is already set, we do not have to set it again
			if (modeledProducedMediatypes
				.stream()
				.filter(mediatype -> mediatype.startsWith("multipart/") || "application/x-www-form-urlencoded".equals(mediatype))
				.findAny().isPresent()) {
				producesMediaTypesAsStrings.addAll(modeledProducedMediatypes);
			} else {
				producesMediaTypesAsStrings.add(MediaTypeEnum.MULTIPART_FORMDATA.getMediaTypeAsString());
			}
		} else {
			producesMediaTypesAsStrings.addAll(getModeledProducedMediatypes(function));
		}
		return producesMediaTypesAsStrings;
	}
	
	public static Set<String> getModeledProducedMediatypes(Function function) {
		Set<String> result = new LinkedHashSet<>();
		OptionDefinition<String>.OptionValue producesMediaTypes = function.getOption(RestOptionEnum.PRODUCES.getName(), String.class);
		if (producesMediaTypes != null) {
			for (String mediaTypeAsString : producesMediaTypes.getOptionValues()) {
				MediaTypeEnum mediaTypeEnum = MediaTypeEnum.get(mediaTypeAsString);
				if (mediaTypeEnum != null) {
				    result.add(mediaTypeEnum.getMediaTypeAsString());
				} else {
					result.add(mediaTypeAsString);
				}
			}
		}
		
		// --- TODO Currently, we don't have any validation for modeled media types. (mmt 06-Apr-2022)
		OptionDefinition<String>.OptionValue optionValueProducedMediaTypes =
				function.getOption(com.gs.vd.modeler.dsl.descriptor.rest.FunctionDescriptor.OptionDescriptor.PRODUCEDMEDIATYPES.getCode(), String.class);
		if (optionValueProducedMediaTypes != null) {
		    optionValueProducedMediaTypes.getOptionValues().forEach(mediaType -> result.add(mediaType));
		}
				
		return result;
	}
	
	/**
	 * @param parameter
	 * @return
	 */
	public static Set<String> getModeledProducedMediatypes(FunctionParameter parameter) {
		final Set<String> result = new LinkedHashSet<>();
		OptionDefinition<String>.OptionValue producesMediaTypes = parameter.getOption(RestOptionEnum.PRODUCES.getName(), String.class);
		if (producesMediaTypes != null) {
			for (String mediaTypeAsString : producesMediaTypes.getOptionValues()) {
				MediaTypeEnum mediaTypeEnum = MediaTypeEnum.get(mediaTypeAsString);
				if (mediaTypeEnum != null) {
				    result.add(mediaTypeEnum.getMediaTypeAsString());
				} else {
					result.add(mediaTypeAsString);
				}
			}
		}
		
		// --- TODO Currently, we don't have any validation for modeled media types. (mmt 06-Apr-2022)
		OptionDefinition<String>.OptionValue optionValueProducedMediaTypes =
				parameter.getOption(com.gs.vd.modeler.dsl.descriptor.rest.FunctionDescriptor.OptionDescriptor.PRODUCEDMEDIATYPES.getCode(), String.class);
		if (optionValueProducedMediaTypes != null) {
		    optionValueProducedMediaTypes.getOptionValues().forEach(mediaType -> result.add(mediaType));
		}
		
		return result;
	}
	
	/**
	 * @param param
	 * @param pattern
	 * @return
	 */
	public static Set<String> getModeledProducedMediatypes(FunctionParameter param, String pattern) {
    	return getModeledProducedMediatypes(param).stream()
    		      .filter(mediaType -> pattern != null && mediaType.toLowerCase().startsWith(pattern.toLowerCase()))
    		      .collect(Collectors.toCollection(LinkedHashSet::new));
    }
	
	/**
	 * @param function
	 * @param pattern
	 * @return
	 */
	public static Set<String> getModeledProducedMediatypes(Function function, String pattern) {
    	return getModeledProducedMediatypes(function).stream()
    		      .filter(mediaType -> pattern != null && mediaType.toLowerCase().startsWith(pattern.toLowerCase()))
    		      .collect(Collectors.toCollection(LinkedHashSet::new));
    }
	
	/**
	 * @param functionModule
	 * @param pattern
	 * @return
	 */
	public static Set<String> getModeledProducedMediatypes(FunctionModule functionModule, String pattern) {
    	return getModeledProducedMediatypes(functionModule).stream()
    		      .filter(mediaType -> pattern != null && mediaType.toLowerCase().startsWith(pattern.toLowerCase()))
    		      .collect(Collectors.toCollection(LinkedHashSet::new));
    }
	
	public static Set<String> getProducedMediatypes(ExceptionType exceptionType) {
		final Set<String> result = new LinkedHashSet<>();
		OptionDefinition<String>.OptionValue producesMediaTypes = exceptionType.getOption(RestOptionEnum.PRODUCED_MEDIA_TYPES.getName(), String.class);
		if (producesMediaTypes != null) {
			producesMediaTypes.getOptionValues().forEach(mediaType -> result.add(mediaType));
		}
		
		// --- TODO Currently, we don't have any validation for modeled media types. (mmt 06-Apr-2022)
		OptionDefinition<String>.OptionValue optionValueProducedMediaTypes =
				exceptionType.getOption(com.gs.vd.modeler.dsl.descriptor.rest.FunctionDescriptor.OptionDescriptor.PRODUCEDMEDIATYPES.getCode(), String.class);
		if (optionValueProducedMediaTypes != null) {
		    optionValueProducedMediaTypes.getOptionValues().forEach(mediaType -> result.add(mediaType));
		}
				
		return result;
	}
	
	/**
	 * @param function
	 * @return
	 */
	public static boolean isMultipartUsed(Function function) {
		return isMultipartConsumed(function) || isMultipartProduced(function);
	}
	
	/**
	 * @param function
	 * @return
	 */
	public static boolean isFormUrlEncodedUsed(Function function) {
		return isFormUrlEncodedConsumed(function) || isFormUrlEncodedProduced(function);
	}
	
	public static boolean isMultipartConsumed(Function function) {
    	Set<String> consumedMediatypes = getConsumedMediatypes(function);
    	return consumedMediatypes.stream().filter(mediatype -> mediatype.startsWith("multipart/")).findAny().isPresent();
    }
	
	public static boolean isMultipartFormDataConsumed(Function function) {
    	Set<String> consumedMediatypes = getConsumedMediatypes(function);
    	return consumedMediatypes.stream().filter(mediatype -> mediatype.startsWith("multipart/form-data")).findAny().isPresent();
    }
	
	public static boolean isMultipartMixedConsumed(Function function) {
    	Set<String> consumedMediatypes = getConsumedMediatypes(function);
    	return consumedMediatypes.stream().filter(mediatype -> mediatype.startsWith("multipart/mixed")).findAny().isPresent();
    }
	
	public static boolean isMultipartRelatedConsumed(Function function) {
    	Set<String> consumedMediatypes = getConsumedMediatypes(function);
    	return consumedMediatypes.stream().filter(mediatype -> mediatype.startsWith("multipart/related")).findAny().isPresent();
    }
	
	public static boolean isFormUrlEncodedConsumed(Function function) {
    	Set<String> consumedMediatypes = getConsumedMediatypes(function);
    	return consumedMediatypes.stream().filter(mediatype -> mediatype.startsWith("application/x-www-form-urlencoded")).findAny().isPresent();
    }
	
	public static boolean isMultipartProduced(Function function) {
    	Set<String> producedMediatypes = getProducedMediatypes(function);
    	return producedMediatypes.stream().filter(mediatype -> mediatype.startsWith("multipart/")).findAny().isPresent();
    }
	
	public static boolean isMultipartFormDataProduced(Function function) {
    	Set<String> producedMediatypes = getProducedMediatypes(function);
    	return producedMediatypes.stream().filter(mediatype -> mediatype.startsWith("multipart/form-data")).findAny().isPresent();
    }
	
	public static boolean isMultipartMixedProduced(Function function) {
    	Set<String> producedMediatypes = getProducedMediatypes(function);
    	return producedMediatypes.stream().filter(mediatype -> mediatype.startsWith("multipart/mixed")).findAny().isPresent();
    }
	
	public static boolean isMultipartRelatedProduced(Function function) {
    	Set<String> producedMediatypes = getProducedMediatypes(function);
    	return producedMediatypes.stream().filter(mediatype -> mediatype.startsWith("multipart/related")).findAny().isPresent();
    }
	
	public static boolean isFormUrlEncodedProduced(Function function) {
    	Set<String> producedMediatypes = getProducedMediatypes(function);
    	return producedMediatypes.stream().filter(mediatype -> mediatype.startsWith("application/x-www-form-urlencoded")).findAny().isPresent();
    }
	
	public static boolean isReadableTextConsumed(FunctionParameter functionParameter) {
		return !getModeledConsumedMediatypes(functionParameter, "text/").isEmpty();
	}
	
	public static boolean isXmlConsumed(FunctionParameter functionParameter) {
		return !getModeledConsumedMediatypes(functionParameter, "text/xml").isEmpty() ||
				!getModeledConsumedMediatypes(functionParameter, "application/xml").isEmpty();
	}
	
	public static boolean isXmlConsumed(Function function) {
		return !getModeledConsumedMediatypes(function, "text/xml").isEmpty() ||
				!getModeledConsumedMediatypes(function, "application/xml").isEmpty();
	}
	
	public static boolean isXmlConsumed(FunctionModule functionModule) {
		return !getModeledConsumedMediatypes(functionModule, "text/xml").isEmpty() ||
				!getModeledConsumedMediatypes(functionModule, "application/xml").isEmpty();
	}
	
	public static boolean isJsonConsumed(FunctionParameter functionParameter) {
		return !getModeledConsumedMediatypes(functionParameter, "text/json").isEmpty() ||
				!getModeledConsumedMediatypes(functionParameter, "application/json").isEmpty();
	}
	
	public static boolean isJsonConsumed(Function function) {
		return !getModeledConsumedMediatypes(function, "text/json").isEmpty() ||
				!getModeledConsumedMediatypes(function, "application/json").isEmpty();
	}
	
	public static boolean isJsonConsumed(FunctionModule functionModule) {
		return !getModeledConsumedMediatypes(functionModule, "text/json").isEmpty() ||
				!getModeledConsumedMediatypes(functionModule, "application/json").isEmpty();
	}
	
	public static boolean isImageConsumed(FunctionParameter functionParameter) {
		return !getModeledConsumedMediatypes(functionParameter, "image/").isEmpty();
	}
	
	public static boolean isBinaryConsumed(FunctionParameter functionParameter) {
		return !getModeledConsumedMediatypes(functionParameter, "application/octet-stream").isEmpty();
	}
	
	public static boolean isAudioConsumed(FunctionParameter functionParameter) {
		return !getModeledConsumedMediatypes(functionParameter, "audio/").isEmpty();
	}
	
	public static boolean isVideoConsumed(FunctionParameter functionParameter) {
		return !getModeledConsumedMediatypes(functionParameter, "video/").isEmpty();
	}
	
	public static boolean isFontConsumed(FunctionParameter functionParameter) {
		return !getModeledConsumedMediatypes(functionParameter, "font/").isEmpty();
	}
	
	public static boolean isMessageConsumed(FunctionParameter functionParameter) {
		return !getModeledConsumedMediatypes(functionParameter, "message/").isEmpty();
	}
	
	// --- from here one PRODUCED
	public static boolean isReadableTextProduced(FunctionParameter functionParameter) {
		return !getModeledProducedMediatypes(functionParameter, "text/").isEmpty();
	}
	
	public static boolean isXmlProduced(FunctionParameter functionParameter) {
		return !getModeledProducedMediatypes(functionParameter, "text/xml").isEmpty() ||
				!getModeledProducedMediatypes(functionParameter, "application/xml").isEmpty();
	}
	
	public static boolean isXmlProduced(Function function) {
		return !getModeledProducedMediatypes(function, "text/xml").isEmpty() ||
				!getModeledProducedMediatypes(function, "application/xml").isEmpty();
	}
	
	public static boolean isXmlProduced(FunctionModule functionModule) {
		return !getModeledProducedMediatypes(functionModule, "text/xml").isEmpty() ||
				!getModeledProducedMediatypes(functionModule, "application/xml").isEmpty();
	}
	
	public static boolean isJsonProduced(FunctionParameter functionParameter) {
		return !getModeledProducedMediatypes(functionParameter, "text/json").isEmpty() ||
				!getModeledProducedMediatypes(functionParameter, "application/json").isEmpty();
	}
	
	public static boolean isJsonProduced(Function function) {
		return !getModeledProducedMediatypes(function, "text/json").isEmpty() ||
				!getModeledProducedMediatypes(function, "application/json").isEmpty();
	}
	
	public static boolean isJsonProduced(FunctionModule functionModule) {
		return !getModeledProducedMediatypes(functionModule, "text/json").isEmpty() ||
				!getModeledProducedMediatypes(functionModule, "application/json").isEmpty();
	}
	
	public static boolean isImageProduced(FunctionParameter functionParameter) {
		return !getModeledProducedMediatypes(functionParameter, "image/").isEmpty();
	}
	
	public static boolean isBinaryProduced(FunctionParameter functionParameter) {
		return !getModeledProducedMediatypes(functionParameter, "application/octet-stream").isEmpty();
	}
	
	public static boolean isAudioProduced(FunctionParameter functionParameter) {
		return !getModeledProducedMediatypes(functionParameter, "audio/").isEmpty();
	}
	
	public static boolean isVideoProduced(FunctionParameter functionParameter) {
		return !getModeledProducedMediatypes(functionParameter, "video/").isEmpty();
	}
	
	public static boolean isFontProduced(FunctionParameter functionParameter) {
		return !getModeledProducedMediatypes(functionParameter, "font/").isEmpty();
	}
	
	public static boolean isMessageProduced(FunctionParameter functionParameter) {
		return !getModeledProducedMediatypes(functionParameter, "message/").isEmpty();
	}
	
	public static Set<FunctionParameter> getFunctionInBodyAndPartParameters(Function function) {
		return ServiceModelProcessing.getFunctionInParameters(function, RestOptionEnum.ParamTypeEnum.BODY, RestOptionEnum.ParamTypeEnum.PART);
    }
	
	public static Set<FunctionParameter> getFunctionInBodyParameters(Function function) {
		return ServiceModelProcessing.getFunctionInParameters(function, RestOptionEnum.ParamTypeEnum.BODY);
    }
	
	public static Set<FunctionParameter> getFunctionInHeaderParameters(Function function) {
		return ServiceModelProcessing.getFunctionInParameters(function, RestOptionEnum.ParamTypeEnum.HEADER);
    }
	
	public static Set<FunctionParameter> getFunctionInCookieParameters(Function function) {
		return ServiceModelProcessing.getFunctionInParameters(function, RestOptionEnum.ParamTypeEnum.COOKIE);
    }
	
	public static Set<FunctionParameter> getFunctionInFieldParameters(Function function) {
		return ServiceModelProcessing.getFunctionInParameters(function, RestOptionEnum.ParamTypeEnum.FIELD);
    }
	
	public static Set<FunctionParameter> getFunctionInPartParameters(Function function) {
		return ServiceModelProcessing.getFunctionInParameters(function, RestOptionEnum.ParamTypeEnum.PART);
    }
	
	public static Set<FunctionParameter> getFunctionInPathParameters(Function function) {
		return ServiceModelProcessing.getFunctionInParameters(function, RestOptionEnum.ParamTypeEnum.PATH);
    }
	
	public static Set<FunctionParameter> getFunctionInQueryParameters(Function function) {
		return ServiceModelProcessing.getFunctionInParameters(function, RestOptionEnum.ParamTypeEnum.QUERY);
    }
	
	public static Set<FunctionParameter> getFunctionInQueryNameParameters(Function function) {
		return ServiceModelProcessing.getFunctionInParameters(function, RestOptionEnum.ParamTypeEnum.QUERY_NAME);
    }
	
	public static Set<FunctionParameter> getFunctionInUrlParameters(Function function) {
		return ServiceModelProcessing.getFunctionInParameters(function, RestOptionEnum.ParamTypeEnum.URL);
    }
	
	public static Set<FunctionParameter> getFunctionInMatrixParameters(Function function) {
		return ServiceModelProcessing.getFunctionInParameters(function, RestOptionEnum.ParamTypeEnum.MATRIX);
    }
	
	public static Set<FunctionParameter> getFunctionOutBodyParameters(Function function) {
		return ServiceModelProcessing.getFunctionOutParameters(function, TransportTypeEnum.BODY);
    }
	
	public static Set<FunctionParameter> getFunctionOutHeaderParameters(Function function) {
		return ServiceModelProcessing.getFunctionOutParameters(function, TransportTypeEnum.HEADER);
    }
	
	public static Set<FunctionParameter> getFunctionOutCookieParameters(Function function) {
		return ServiceModelProcessing.getFunctionOutParameters(function, TransportTypeEnum.COOKIE);
    }
	
	/**
	 * @param functionParameter
	 * @return
	 */
	public static String getPath(FunctionParameter functionParameter) {
		OptionDefinition<String>.OptionValue path = functionParameter.getOption(RestOptionEnum.PATH.getName(), String.class);
		String pathValue = functionParameter.getName();
		if (path != null) {
			pathValue = path.getOptionValue() != null ? path.getOptionValue() : "";
		}
		return pathValue;
	}
	
	/**
	 * @param function
	 * @return
	 */
	public static String getPath(Function function) {
		String pathValue = null;
		OptionDefinition<String>.OptionValue path = function.getOption(RestOptionEnum.PATH.getName(), String.class);
		if (path != null) {
			pathValue = path.getOptionValue() != null ? path.getOptionValue() : "";
		}
		
		if (pathValue == null) {
			// use a different way to deduce a sensful path value
			if (function.getOwner().getOriginatingElement(Entity.class) == null) {
				pathValue = function.getName().toLowerCase();
			}
		}
		
		return pathValue == null ? "" : pathValue;
	}
	
	/**
	 * <p>When a PATH option is set for the function module, use that one (even if it is only "").
	 * When _no_ PATH option is set for the function module, then use the function module's name (all lowercase).
	 * 
	 * @param functionModule
	 * @return
	 */
	public static String getPath(FunctionModule functionModule) {
		OptionDefinition<String>.OptionValue pathOptionValue = functionModule.getOption(RestOptionEnum.PATH.getName(), String.class);
		String path = functionModule.getName().toLowerCase();
		if (pathOptionValue != null) {
			path = pathOptionValue.getOptionValue() != null ? pathOptionValue.getOptionValue() : "";
		}
		return path;
	}
	
	/**
	 * @param functionParameter
	 * @return
	 */
	public static Optional<RestOptionEnum.ParamTypeEnum> getParamType(FunctionParameter functionParameter) {
		OptionDefinition<String>.OptionValue paramTypeOptionValue = functionParameter.getOption(RestOptionEnum.PARAM_TYPE.getName(), String.class);
		
		if (paramTypeOptionValue != null && paramTypeOptionValue.getOptionValue() != null) {
			RestOptionEnum.ParamTypeEnum paramTypeEnum = RestOptionEnum.ParamTypeEnum.getByName(paramTypeOptionValue.getOptionValue());
			return Optional.of(paramTypeEnum);
		}
		
		return Optional.empty();
	}
	
	/**
	 * @param pathValue
	 * @return
	 */
	public static String normalizePathValue(String pathValue) {
		if (pathValue != null && !pathValue.isEmpty()) {
			// Even when the path is a slash only, we have to replace the leading slash in order to apply any leading path, too. 
		    pathValue = pathValue.replaceAll("^/+", "");
		    // When there are multiple subsequent slashes, we are going to replace them with single slashes.
			pathValue = pathValue.replaceAll("[/]+", "/");
		}
		return pathValue;
	}
	
	/**
	 * @param paramName
	 * @return
	 */
	public static String normalizeParameterName(String paramName) {
		if (paramName != null && !paramName.isEmpty()) {
			// Even when the path is a slash only, we have to replace the leading slash in order to apply any leading path, too. 
			paramName = paramName.replaceAll("\\s", "_");
		}
		return paramName;
	}
	
	/**
	 * @param modelElements
	 * @return
	 */
	public static Set<FunctionModule> getServiceInterfaceFunctionModules(Set<?> modelElements) {
		final Set<FunctionModule> result = modelElements.stream()
			.filter(ServiceInterface.class::isInstance)
			.map(ServiceInterface.class::cast)
			.flatMap(serviceInterface -> serviceInterface.getFunctionModules().stream())
			.collect(Collectors.toCollection(LinkedHashSet::new));
		
		Set<FunctionModule> functionModulesOfServiceInterfaces = modelElements.stream()
			.filter(ServiceImplementation.class::isInstance)
			.map(ServiceImplementation.class::cast)
			.map(serviceImplementation -> serviceImplementation.getServiceInterface())
			.filter(serviceInterface -> serviceInterface != null)
			.flatMap(serviceInterface -> serviceInterface.getFunctionModules().stream())
			.collect(Collectors.toCollection(LinkedHashSet::new));
        result.addAll(functionModulesOfServiceInterfaces);
        
        functionModulesOfServiceInterfaces = modelElements.stream()
    			.filter(ServiceClient.class::isInstance)
    			.map(ServiceClient.class::cast)
    			.map(serviceClient -> serviceClient.getServiceInterface())
    			.filter(serviceInterface -> serviceInterface != null)
    			.flatMap(serviceInterface -> serviceInterface.getFunctionModules().stream())
    			.collect(Collectors.toCollection(LinkedHashSet::new));
        result.addAll(functionModulesOfServiceInterfaces);
        
//        Set<FunctionModule> functionModulesForEntities = modelElements.stream()
//    			.filter(FunctionModule.class::isInstance)
//    			.map(FunctionModule.class::cast)
//    			.filter(functionModule -> functionModule.getOriginatingElement(Entity.class) != null)
//    			.collect(Collectors.toCollection(LinkedHashSet::new));
//        result.addAll(functionModulesForEntities);
		
		return result;
	}
	
	/**
	 * @param modelElements
	 * @return a collection of messages that inform about what has happened during completion
	 */
	public static Set<Message> completeServiceOptions(Set<?> modelElements) {
		Set<Message> messages = new LinkedHashSet<>();
		
		Set<FunctionModule> relevantFunctionModules = getServiceInterfaceFunctionModules(modelElements);
		
		if (!relevantFunctionModules.isEmpty()) {
			relevantFunctionModules.stream()
				.forEach(functionModule -> {
					System.out.println("module:" + functionModule.getName());
					functionModule.getFunctions().stream()
					    .forEach(function -> {
					    	
					        for (FunctionParameter functionParameter : function.getFunctionInParameters()) {
					        	messages.addAll(completeParameterType(function, functionParameter));
					        	messages.addAll(completeMediaType(function, functionParameter));
					        }
					        
					        for (FunctionParameter functionParameter : function.getFunctionOutParameters()) {
					        	messages.addAll(completeParameterType(function, functionParameter));
					        	messages.addAll(completeMediaType(function, functionParameter));
					        }
					        
					        messages.addAll(completeMediaType(function));
					        
					        if (!getModeledHttpVerb(function).isPresent()) {
						        if (isMultipartConsumed(function)) {
						        	addOperationPost(messages, function, "The function consumes multipart data.");
						        } else if (isFormUrlEncodedConsumed(function)) {
						        	addOperationPost(messages, function, "The function consumes form URL encoded data.");
						        } else if (!getBodyFileOutParameters(function).isEmpty()) {
						        	addOperationGet(messages, function, "The function has no HTTP verb modeled and returns some data.");
						        } else {
						        	// There is no OPERATION set, so most probably it is a GET. However, if there is no return param, then it is a POST.
						        	if (function.getFunctionOutParameters().isEmpty() || !getFunctionInBodyParameters(function).isEmpty()) {
						        		addOperationPost(messages, function, "The function doesn't return any data.");
						        	} else {
						        		addOperationGet(messages, function, "The function has no HTTP verb modeled and returns some data.");
						        	}
						        }
					        }
					        
					        // --- Finally, if it is an HTTP GET and there are in-parameters that don't have a param type set, those parameters are considered to be query params (unless they are complex types).
					        // --- Calling the completeParameterType() again does the job for us.
					        for (FunctionParameter functionParameter : function.getFunctionInParameters()) {
					        	messages.addAll(completeParameterType(function, functionParameter));
					        }
					        
					        Optional<Long> modeledSuccessStatusCode = getModeledSuccessStatusCode(function);
					        if (!modeledSuccessStatusCode.isPresent()) {
					        	Optional<String> modeledHttpVerb = getModeledHttpVerb(function);
					        	if (modeledHttpVerb.isPresent()) {
					        		switch (modeledHttpVerb.get()) {
					        		case "POST":
					        			if (function.getStorageFunction() == StorageFunction.CREATE) {
						        			OptionDefinition<Long>.OptionValue status201 = RestOptionEnum.STATUS_CODE.getDefinition(Long.class). new OptionValue(201L);
								        	function.addOptions(status201);
					        			} else {
					        				OptionDefinition<Long>.OptionValue status200 = RestOptionEnum.STATUS_CODE.getDefinition(Long.class). new OptionValue(200L);
								        	function.addOptions(status200);
					        			}
					        			break;
					        		case "OPTIONS":
					        			OptionDefinition<Long>.OptionValue status204 = RestOptionEnum.STATUS_CODE.getDefinition(Long.class). new OptionValue(204L);
							        	function.addOptions(status204);
					        			break;
					        		default:
					        			OptionDefinition<Long>.OptionValue status200 = RestOptionEnum.STATUS_CODE.getDefinition(Long.class). new OptionValue(200L);
							        	function.addOptions(status200);
					        			break;
					        		}
					        	} else {
					        		OptionDefinition<Long>.OptionValue status200 = RestOptionEnum.STATUS_CODE.getDefinition(Long.class). new OptionValue(200L);
						        	function.addOptions(status200);
					        	}
					        }
					    });
				});
		}
		
		return messages;
	}

	public static void addOperationGet(Set<Message> messages, Function function, String infoMessage) {
		OptionDefinition<String>.OptionValue getOperation = RestOptionEnum.OPERATION.getDefinition(String.class). new OptionValue("GET");
		function.addOptions(getOperation);
		Message completedHttpVerbMessage = FunctionMessage.INFO_COMPLETED_HTTP_VERB.getMessageBuilder()
			.modelElement(function)
			.parameters(function.getQualifiedName(), "GET", infoMessage)
			.build();
		messages.add(completedHttpVerbMessage);
	}

	public static void addOperationPost(Set<Message> messages, Function function, String infoMessage) {
		OptionDefinition<String>.OptionValue postOperation = RestOptionEnum.OPERATION.getDefinition(String.class). new OptionValue("POST");
		function.addOptions(postOperation);
		Message completedHttpVerbMessage = FunctionMessage.INFO_COMPLETED_HTTP_VERB.getMessageBuilder()
			.modelElement(function)
			.parameters(function.getQualifiedName(), "POST", infoMessage)
			.build();
		messages.add(completedHttpVerbMessage);
	}
	
	/**
	 * @param function
	 * @param statusCode
	 */
	public static void setStatusCodeIfNotPresent(Function function, Long statusCode) {
		Optional<Long> modeledSuccessStatusCode = getModeledSuccessStatusCode(function);
		if (!modeledSuccessStatusCode.isPresent()) {
			OptionDefinition<Long>.OptionValue status = RestOptionEnum.STATUS_CODE.getDefinition(Long.class). new OptionValue(statusCode);
        	function.addOptions(status);
		}
	}
	
	/**
	 * @param function
	 * @param functionParameter
	 * @return the set of messages that inform about completions
	 */
	public static Set<Message> completeMediaType(Function function, FunctionParameter functionParameter) {
		Set<Message> messages = new LinkedHashSet<>();
		Type paramDataType = functionParameter.getType();
		Optional<ParamTypeEnum> paramTypeEnum = getParamType(functionParameter);
		
		if (paramTypeEnum.isPresent() && paramTypeEnum.get() != ParamTypeEnum.BODY && paramTypeEnum.get() != ParamTypeEnum.FIELD) {
			// Only for body and field parameters it makes sense to provide information about the media type.
			return messages;
		}
		
		if (functionParameter.getParameterType() == ParameterType.IN) {
		    Set<String> modeledConsumedMediaTypes = getModeledConsumedMediatypes(functionParameter);
		    if (modeledConsumedMediaTypes.isEmpty()) {
		    	// Only complete the media type when there is none set.
		    	if (paramDataType == PrimitiveTypeEnum.STRING.getPrimitiveType()) {
		    		OptionDefinition<String>.OptionValue consumesTextPlain = RestOptionEnum.OPTION_DEFINITION_CONSUMES. new OptionValue("text/plain");
		    		functionParameter.addOptions(consumesTextPlain);
		    		Message completedInParamMediaTypeMessage = FunctionMessage.INFO_COMPLETED_IN_PARAM_MEDIA_TYPE.getMessageBuilder()
			        	.modelElement(functionParameter)
			        	.parameters(functionParameter.getName(),
			        			    function.getQualifiedName(),
			        				"string",
			        				"text/plain")
			        	.build();
			        messages.add(completedInParamMediaTypeMessage);
		    	} else if (paramDataType == PrimitiveTypeEnum.SINT8.getPrimitiveType() && functionParameter.getCollectionType() == CollectionType.ARRAY) {
		    		OptionDefinition<String>.OptionValue consumesOctetStream = RestOptionEnum.OPTION_DEFINITION_CONSUMES. new OptionValue("application/octet-stream");
		    		functionParameter.addOptions(consumesOctetStream);
		    		Message completedInParamMediaTypeMessage = FunctionMessage.INFO_COMPLETED_IN_PARAM_MEDIA_TYPE.getMessageBuilder()
			        	.modelElement(functionParameter)
			        	.parameters(functionParameter.getName(),
			        			    function.getQualifiedName(),
			        				"array of bytes",
			        				"application/octet-stream")
			        	.build();
		    		messages.add(completedInParamMediaTypeMessage);
		    	} else if (paramDataType instanceof PrimitiveType) {
		    		OptionDefinition<String>.OptionValue consumesTextPlain = RestOptionEnum.OPTION_DEFINITION_CONSUMES. new OptionValue("text/plain");
		    		functionParameter.addOptions(consumesTextPlain);
		    		Message completedInParamMediaTypeMessage = FunctionMessage.INFO_COMPLETED_IN_PARAM_MEDIA_TYPE.getMessageBuilder()
			        	.modelElement(functionParameter)
			        	.parameters(functionParameter.getName(),
			        			    function.getQualifiedName(),
			        				"primitive",
			        				"text/plain")
			        	.build();
		    		messages.add(completedInParamMediaTypeMessage);
		    	} else if (paramDataType instanceof ComplexType) {
		    		addConsumedStandardMediaTypesIfNotPresent(functionParameter);
		    	}
		    }
		} else if (functionParameter.getParameterType() == ParameterType.OUT) {
		    Set<String> modeledProducedMediaTypes = getModeledProducedMediatypes(functionParameter);
		    if (modeledProducedMediaTypes.isEmpty()) {
		    	if (paramDataType == PrimitiveTypeEnum.STRING.getPrimitiveType()) {
		    		OptionDefinition<String>.OptionValue producesTextPlain = RestOptionEnum.OPTION_DEFINITION_PRODUCES. new OptionValue("text/plain");
		    		functionParameter.addOptions(producesTextPlain);
		    		Message completedOutParamMediaTypeMessage = FunctionMessage.INFO_COMPLETED_OUT_PARAM_MEDIA_TYPE.getMessageBuilder()
			        	.modelElement(functionParameter)
			        	.parameters(functionParameter.getName(),
			        			    function.getQualifiedName(),
			        				"string",
			        				"text/plain")
			        	.build();
			        messages.add(completedOutParamMediaTypeMessage);
		    	} else if (paramDataType == PrimitiveTypeEnum.SINT8.getPrimitiveType() && functionParameter.getCollectionType() == CollectionType.ARRAY) {
		    		OptionDefinition<String>.OptionValue producesOctetStream = RestOptionEnum.OPTION_DEFINITION_PRODUCES. new OptionValue("application/octet-stream");
		    		functionParameter.addOptions(producesOctetStream);
		    		Message completedOutParamMediaTypeMessage = FunctionMessage.INFO_COMPLETED_OUT_PARAM_MEDIA_TYPE.getMessageBuilder()
			        	.modelElement(functionParameter)
			        	.parameters(functionParameter.getName(),
			        			    function.getQualifiedName(),
			        				"array of bytes",
			        				"application/octet-stream")
			        	.build();
			        messages.add(completedOutParamMediaTypeMessage);
		    	} else if (paramDataType instanceof PrimitiveType) {
		    		OptionDefinition<String>.OptionValue consumesTextPlain = RestOptionEnum.OPTION_DEFINITION_CONSUMES. new OptionValue("text/plain");
		    		functionParameter.addOptions(consumesTextPlain);
		    		Message completedOutParamMediaTypeMessage = FunctionMessage.INFO_COMPLETED_OUT_PARAM_MEDIA_TYPE.getMessageBuilder()
			        	.modelElement(functionParameter)
			        	.parameters(functionParameter.getName(),
			        			    function.getQualifiedName(),
			        				"primitive",
			        				"text/plain")
			        	.build();
			        messages.add(completedOutParamMediaTypeMessage);
		    	} else if (paramDataType instanceof ComplexType) {
		    		addProducedStandardMediaTypesIfNotPresent(functionParameter);
		    	}
		    }
		}
		
		return messages;
	}
	
	public static void addProducedStandardMediaTypesIfNotPresent(FunctionParameter param) {
		if (!isJsonProduced(param) && !isXmlProduced(param)) {
			ArrayList<String> producedMediaTypes = new ArrayList<>();
			producedMediaTypes.add(MediaTypeEnum.APPLICATION_XML.getName());
			producedMediaTypes.add(MediaTypeEnum.APPLICATION_JSON.getName());
		    OptionDefinition<String>.OptionValue producesOptionValue = RestOptionEnum.OPTION_DEFINITION_PRODUCES. new OptionValue(producedMediaTypes);
		    param.addOptions(producesOptionValue);
		}
	}
	
	public static void addProducedStandardMediaTypesIfNotPresent(Function function) {
		if (!isJsonProduced(function) && !isXmlProduced(function)) {
			ArrayList<String> producedMediaTypes = new ArrayList<>();
			producedMediaTypes.add(MediaTypeEnum.APPLICATION_XML.getName());
			producedMediaTypes.add(MediaTypeEnum.APPLICATION_JSON.getName());
		    OptionDefinition<String>.OptionValue producesOptionValue = RestOptionEnum.OPTION_DEFINITION_PRODUCES. new OptionValue(producedMediaTypes);
		    function.addOptions(producesOptionValue);
		}
	}
	
	public static void addProducedStandardMediaTypesIfNotPresent(FunctionModule functionModule) {
		if (!isJsonProduced(functionModule) && !isXmlProduced(functionModule)) {
			ArrayList<String> producedMediaTypes = new ArrayList<>();
			producedMediaTypes.add(MediaTypeEnum.APPLICATION_XML.getName());
			producedMediaTypes.add(MediaTypeEnum.APPLICATION_JSON.getName());
		    OptionDefinition<String>.OptionValue producesOptionValue = RestOptionEnum.OPTION_DEFINITION_PRODUCES. new OptionValue(producedMediaTypes);
		    functionModule.addOptions(producesOptionValue);
		}
	}
	
	public static void addConsumedStandardMediaTypesIfNotPresent(FunctionParameter param) {
		if (!isJsonConsumed(param) && !isXmlConsumed(param)) {
			ArrayList<String> consumedMediaTypes = new ArrayList<>();
			consumedMediaTypes.add(MediaTypeEnum.APPLICATION_XML.getName());
			consumedMediaTypes.add(MediaTypeEnum.APPLICATION_JSON.getName());
		    OptionDefinition<String>.OptionValue consumesOptionValue = RestOptionEnum.OPTION_DEFINITION_CONSUMES. new OptionValue(consumedMediaTypes);
		    param.addOptions(consumesOptionValue);
		}
	}
	
	public static void addConsumedStandardMediaTypesIfNotPresent(Function function) {
		if (!isJsonConsumed(function) && !isXmlConsumed(function)) {
			ArrayList<String> consumedMediaTypes = new ArrayList<>();
			consumedMediaTypes.add(MediaTypeEnum.APPLICATION_XML.getName());
			consumedMediaTypes.add(MediaTypeEnum.APPLICATION_JSON.getName());
		    OptionDefinition<String>.OptionValue consumesOptionValue = RestOptionEnum.OPTION_DEFINITION_CONSUMES. new OptionValue(consumedMediaTypes);
		    function.addOptions(consumesOptionValue);
		}
	}
	
	public static void addConsumedStandardMediaTypesIfNotPresent(FunctionModule functionModule) {
		if (!isJsonConsumed(functionModule) && !isXmlConsumed(functionModule)) {
			ArrayList<String> consumedMediaTypes = new ArrayList<>();
			consumedMediaTypes.add(MediaTypeEnum.APPLICATION_XML.getName());
			consumedMediaTypes.add(MediaTypeEnum.APPLICATION_JSON.getName());
		    OptionDefinition<String>.OptionValue consumesOptionValue = RestOptionEnum.OPTION_DEFINITION_CONSUMES. new OptionValue(consumedMediaTypes);
		    functionModule.addOptions(consumesOptionValue);
		}
	}
	
	/**
	 * @param function
	 * @param functionParameter
	 * @return the set of messages that inform about what happened during completion
	 */
	public static Set<Message> completeParameterType(Function function, FunctionParameter functionParameter) {
		Set<Message> messages = new LinkedHashSet<>();
		
		Optional<ParamTypeEnum> paramType = getParamType(functionParameter);
    	if (!paramType.isPresent()) {
    		if (functionParameter.getParameterType() == ParameterType.IN) {
	    		if (getModeledHttpVerb(function).isPresent()
	    				&&
	    				(getModeledHttpVerb(function).get().equalsIgnoreCase("GET") || getModeledHttpVerb(function).get().equalsIgnoreCase("PUT") || getModeledHttpVerb(function).get().equalsIgnoreCase("PATCH"))
	    				&&
	    				functionParameter.getType() instanceof PrimitiveType && getModeledConsumedMediatypes(functionParameter).isEmpty()) {
		    		// primitive parameters that have not parameter type set are going to be interpreted as query parameters for GET
	    			functionParameter.addOptions( RestOptionEnum.PARAM_TYPE.getDefinition(String.class). new OptionValue(RestOptionEnum.ParamTypeEnum.QUERY.getName()));
	    			Message completedQueryParamTypeMessage = FunctionMessage.INFO_COMPLETED_QUERY_PARAM_TYPE.getMessageBuilder()
			        	.modelElement(functionParameter)
			        	.parameters(functionParameter.getParameterType() == ParameterType.IN ? "IN" : "OUT",
			        			    functionParameter.getName(),
			        			    function.getQualifiedName())
			        	.build();
			        messages.add(completedQueryParamTypeMessage);
		    	} else {
		    		// Every parameter that has no parameter type modeled is treated as a parameter that holds data of the HTTP request body.
		    		// Making sure that it is set makes it easier to handle it at a later time during model conversion.
		    		functionParameter.addOptions( RestOptionEnum.PARAM_TYPE.getDefinition(String.class). new OptionValue(RestOptionEnum.ParamTypeEnum.BODY.getName()));
		    		Message completedBodyParamTypeMessage = FunctionMessage.INFO_COMPLETED_BODY_PARAM_TYPE.getMessageBuilder()
			        	.modelElement(functionParameter)
			        	.parameters(functionParameter.getParameterType() == ParameterType.IN ? "IN" : "OUT",
			        			    functionParameter.getName(),
			        			    function.getQualifiedName())
			        	.build();
			        messages.add(completedBodyParamTypeMessage);
		    	}
    		}
    	} else if (paramType.get() == ParamTypeEnum.BODY && getModeledHttpVerb(function).isPresent() && getModeledHttpVerb(function).get().equalsIgnoreCase("GET") &&
				functionParameter.getType() instanceof PrimitiveType &&
				(getModeledConsumedMediatypes(functionParameter).isEmpty() || isReadableTextConsumed(functionParameter))) {
    		// --- for GET a body parameter is not allowed => change the param type to QUERY (at least for primitives)
    		OptionDefinition<String>.OptionValue paramTypeOptionValue = functionParameter.getOption(RestOptionEnum.PARAM_TYPE.getName(), String.class);
    		paramTypeOptionValue.setOptionValue(ParamTypeEnum.QUERY.getName());
    		Message changeBodyToQueryParamTypeMessage = FunctionMessage.INFO_CHANGED_BODY_TO_QUERY_PARAM_TYPE.getMessageBuilder()
	        	.modelElement(functionParameter)
	        	.parameters(functionParameter.getParameterType() == ParameterType.IN ? "IN" : "OUT",
	        			    functionParameter.getName(),
	        			    function.getQualifiedName())
	        	.build();
	        messages.add(changeBodyToQueryParamTypeMessage);
    	}
    	
    	return messages;
	}
	
	private static OptionDefinition<String>.OptionValue createConsumesOptionValue(String mediaType) {
		if (mediaType != null && !mediaType.isEmpty()) {
			List<String> mediaTypes = new ArrayList<>();
			if (mediaType.startsWith("application/json")) {
				mediaTypes.add("application/xml");
				mediaTypes.add(mediaType);
			} else if (mediaType.startsWith("application/xml")) {
				mediaTypes.add(mediaType);
				mediaTypes.add("application/json");
			} else {
				mediaTypes.add(mediaType);
			}
			
			OptionDefinition<String>.OptionValue consumesMediaType = RestOptionEnum.OPTION_DEFINITION_CONSUMES. new OptionValue(mediaTypes);
			return consumesMediaType;
		}
		return null;
	}
	
	private static OptionDefinition<String>.OptionValue createProducesOptionValue(String mediaType) {
		if (mediaType != null && !mediaType.isEmpty()) {
			List<String> mediaTypes = new ArrayList<>();
			if (mediaType.startsWith("application/json")) {
				mediaTypes.add("application/xml");
				mediaTypes.add(mediaType);
			} else if (mediaType.startsWith("application/xml")) {
				mediaTypes.add(mediaType);
				mediaTypes.add("application/json");
			} else {
				mediaTypes.add(mediaType);
			}
			
			OptionDefinition<String>.OptionValue producesMediaType = RestOptionEnum.OPTION_DEFINITION_PRODUCES. new OptionValue(mediaTypes);
			return producesMediaType;
		}
		return null;
	}
	
	
	/**
	 * @param function
	 * @return set of messages that inform about what happened during completion
	 */
	public static Set<Message> completeMediaType(Function function) {
		Set<Message> messages = new LinkedHashSet<>();
		
	    Set<String> modeledConsumedMediaTypes = getModeledConsumedMediatypes(function);
	    if (modeledConsumedMediaTypes.isEmpty()) {
	    	// Only complete the media type when there is none set.
	    	Optional<String> guessedConsumedMediaType = guessConsumedMediaType(function);
	    	if (guessedConsumedMediaType.isPresent()) {
	    		OptionDefinition<String>.OptionValue consumesMediaType = createConsumesOptionValue(guessedConsumedMediaType.get());
	    		if (consumesMediaType != null) {
		    		function.addOptions(consumesMediaType);
		    		Message completeConsumedMediaTypeMessage = FunctionMessage.INFO_COMPLETED_IN_CONSUMED_MEDIA_TYPE.getMessageBuilder()
	    	        	.modelElement(function)
	    	        	.parameters(function.getQualifiedName(), guessedConsumedMediaType.get())
	    	        	.build();
	    	        messages.add(completeConsumedMediaTypeMessage);
	    		}
	    	} else if (!isMultipartProduced(function)) {
	    		final List<String> mediaTypes = new ArrayList<>();
	    		// take over the media types of one of the parameters (the one that holds the data for the response)
	    		getFunctionInBodyParameters(function).stream()
	    		    .filter(param -> !getModeledConsumedMediatypes(param).isEmpty())
	    		    .flatMap(param -> getModeledConsumedMediatypes(param).stream())
	    		    .forEach(consumedMediaType -> {
	    		    	mediaTypes.add(consumedMediaType);
	    		    });
	    		if (mediaTypes != null && !mediaTypes.isEmpty()) {
		    		OptionDefinition<String>.OptionValue consumesOptionValue = RestOptionEnum.OPTION_DEFINITION_CONSUMES. new OptionValue(mediaTypes);
				    function.addOptions(consumesOptionValue);
	    		}
	    	}
	    }
	    	
	    Set<String> modeledProducedMediaTypes = getModeledProducedMediatypes(function);
	    if (modeledProducedMediaTypes.isEmpty()) {
	    	Optional<String> guessedProducedMediaType = guessProducedMediaType(function);
	    	if (guessedProducedMediaType.isPresent()) {
	    		OptionDefinition<String>.OptionValue producesMediaType = createProducesOptionValue(guessedProducedMediaType.get());
		    	if (producesMediaType != null) {
	    		    function.addOptions(producesMediaType);
		    		Message completeProducedMediaTypeMessage = FunctionMessage.INFO_COMPLETED_OUT_CONSUMED_MEDIA_TYPE.getMessageBuilder()
	    	        	.modelElement(function)
	    	        	.parameters(function.getQualifiedName(), guessedProducedMediaType.get())
	    	        	.build();
	    	        messages.add(completeProducedMediaTypeMessage);
		    	}
	    	} else if (!isMultipartProduced(function)) {
	    		final List<String> mediaTypes = new ArrayList<>();
	    		// take over the media types of one of the parameters (the one that holds the data for the response)
	    		getFunctionOutBodyParameters(function).stream()
	    		    .filter(param -> !getModeledProducedMediatypes(param).isEmpty())
	    		    .flatMap(param -> getModeledProducedMediatypes(param).stream())
	    		    .forEach(producedMediaType -> {
	    		    	mediaTypes.add(producedMediaType);
	    		    });
	    		if (mediaTypes != null && !mediaTypes.isEmpty()) {
		    		OptionDefinition<String>.OptionValue producesOptionValue = RestOptionEnum.OPTION_DEFINITION_PRODUCES. new OptionValue(mediaTypes);
				    function.addOptions(producesOptionValue);
	    		}
	    	}
	    }
	    
	    return messages;
	}
	
	/**
	 * This method decides which param type to assume in case the incoming
	 * model doesn't have a parameter type modeled.
	 * 
	 * @param function
	 * @param functionParameter
	 * @return
	 */
	public static ParamTypeEnum guessParamType(Function function, FunctionParameter functionParameter) {
		Optional<ParamTypeEnum> modeledParamType = getParamType(functionParameter);
		ParamTypeEnum result = null;
		if (modeledParamType.isPresent()) {
			result = modeledParamType.get();
		} else {
			if (getModeledHttpVerb(function).isPresent() && "DELETE".equalsIgnoreCase(getModeledHttpVerb(function).get())) {
				result = ParamTypeEnum.QUERY;
			} else {
				Entity entityOrigin = functionParameter.getOriginatingElementDeeply(Entity.class, true);
				if (entityOrigin != null) {
					result = ParamTypeEnum.BODY;
				} else if (functionParameter.getType() instanceof PrimitiveType) {
					result = ParamTypeEnum.QUERY;
				} else if (functionParameter.getType() instanceof Enumeration) {
					result = ParamTypeEnum.QUERY;
				} else {
					result = ParamTypeEnum.BODY;
				}
			}
		}
		
		return result;
	}
	
	/**
	 * @param function
	 * @return
	 */
	public static Optional<String> guessConsumedMediaType(Function function) {
		Set<String> modeledConsumedMediatypes = getModeledConsumedMediatypes(function);
		if (modeledConsumedMediatypes.isEmpty()) {
			Set<FunctionParameter> bodyAndPartParameters = getFunctionInBodyAndPartParameters(function);
			if (bodyAndPartParameters.size() > 1) {
				// No media type modeled and more than one body parameter identified => we have to decide whether we want form url encoded or multipart.
				if (getBodyFileInParameters(function).isEmpty()) {
					return Optional.of("application/x-www-form-urlencoded");
				} else {
					return Optional.of("multipart/form-data");
				}
			} else if (bodyAndPartParameters.size() == 1) {
				// No consumed media type is modeled and the function has one single body parameter => we assume a default media type, unless the body parameter has a media type set
				FunctionParameter bodyFunctionParameter = bodyAndPartParameters.iterator().next();
				Set<String> modeledConsumedMediatypesOfParam = getModeledConsumedMediatypes(bodyFunctionParameter);
				if (!modeledConsumedMediatypesOfParam.isEmpty()) {
					// return empty since the caller then has to take care of doing something with the modeled consumed media types of the parameter
					return Optional.empty();
				} else if (bodyFunctionParameter.getType() instanceof ComplexType) {
					@SuppressWarnings("unused")
					ComplexType inParamType = (ComplexType) bodyFunctionParameter.getType();
				    return Optional.of("application/json");
				} else {
				    return Optional.of("text/plain");
				}
			}
		}
		
		return Optional.empty();
	}
	
	/**
	 * @param function
	 * @return
	 */
	public static Optional<String> guessProducedMediaType(Function function) {
		Set<String> modeledProducedMediatypes = getModeledProducedMediatypes(function);
		if (modeledProducedMediatypes.isEmpty()) {
			Set<FunctionParameter> bodyParameters = getFunctionOutBodyParameters(function);
			if (bodyParameters.size() > 1) {
				// No media type modeled and more than one body parameter identified => we have to decide whether we want form url encoded or multipart.
				if (getBodyFileOutParameters(function).isEmpty()) {
					return Optional.of("application/x-www-form-urlencoded");
				} else {
					return Optional.of("multipart/form-data");
				}
			} else if (bodyParameters.size() == 1) {
				// No consumed media type is modeled and the function has one single body parameter => we assume a default media type, unless the body parameter has a media type set
				FunctionParameter bodyFunctionParameter = bodyParameters.iterator().next();
				Set<String> modeledProducedMediatypesOfParam = getModeledProducedMediatypes(bodyFunctionParameter);
 				if (!modeledProducedMediatypesOfParam.isEmpty()) {
 				    // return empty since the caller then has to take care of doing something with the modeled consumed media types of the parameter
					return Optional.empty();
 				} else if (bodyFunctionParameter.getType() instanceof ComplexType) {
					@SuppressWarnings("unused")
					ComplexType outParamType = (ComplexType) bodyFunctionParameter.getType();
					return Optional.of("application/json");
				} else {
					return Optional.of("text/plain");
				}
			}
		}
		
		return Optional.empty();
	}
	
	/**
	 * @param function
	 * @return
	 */
	public static Set<FunctionParameter> getBodyFileInParameters(Function function) {
		final LinkedHashSet<FunctionParameter> result = new LinkedHashSet<>();
		
		getFunctionInBodyAndPartParameters(function).stream()
		    .forEach(functionParameter -> {
		    	if (isHoldingFileContent(function, functionParameter)) {
		    		result.add(functionParameter);
		    	}
		    });
		
		return result;
	}
	
	/**
	 * @param function
	 * @return
	 */
	public static Set<FunctionParameter> getBodyFileOutParameters(Function function) {
		final LinkedHashSet<FunctionParameter> result = new LinkedHashSet<>();
		
		getFunctionOutBodyParameters(function).stream()
		    .forEach(functionParameter -> {
		    	if (isHoldingFileContent(function, functionParameter)) {
		    		result.add(functionParameter);
		    	}
		    });
		
		return result;
	}
	
	/**
	 * @param function
	 * @param functionParameter
	 * @return
	 */
	public static boolean isHoldingFileContent(Function function, FunctionParameter functionParameter) {
		boolean result = false;
		Type paramType = functionParameter.getType();
		
		if (functionParameter.getParameterType() == ParameterType.IN) {
			Set<String> plainTextMediaTypes = getModeledConsumedMediatypes(functionParameter, "text/plain");
	        Set<String> mediaTypes = getModeledConsumedMediatypes(functionParameter);
	        
			if (paramType instanceof PrimitiveType) {
				if (paramType == PrimitiveTypeEnum.SINT8.getPrimitiveType() && functionParameter.getCollectionType() == CollectionType.ARRAY) {
					result = true;
				} else if (functionParameter.getCollectionType() == null && paramType == PrimitiveTypeEnum.STRING.getPrimitiveType() && (plainTextMediaTypes.isEmpty() && !mediaTypes.isEmpty())) {
					result = true;
				}
			} else {
				result = true;
			}
		} else if (functionParameter.getParameterType() == ParameterType.OUT) {
			Set<String> plainTextMediaTypes = getModeledProducedMediatypes(functionParameter, "text/plain");
	        Set<String> mediaTypes = getModeledProducedMediatypes(functionParameter);
	        
			if (paramType instanceof PrimitiveType) {
				if (paramType == PrimitiveTypeEnum.SINT8.getPrimitiveType() && functionParameter.getCollectionType() == CollectionType.ARRAY) {
					result = true;
				} else if (functionParameter.getCollectionType() == null && paramType == PrimitiveTypeEnum.STRING.getPrimitiveType() && (plainTextMediaTypes.isEmpty() && !mediaTypes.isEmpty())) {
					result = true;
				}
			} else {
				result = true;
			}
		}
		
		return result;
	}
	
	/**
	 * @param function
	 * @param functionParameter
	 * @return
	 */
	public static boolean isHoldingJsonOrXmlContent(Function function, FunctionParameter functionParameter) {
		boolean result = false;
		Type paramType = functionParameter.getType();
		Set<String> jsonMediaType = getModeledConsumedMediatypes(functionParameter, "application/json");
		Set<String> xmlMediaType = getModeledConsumedMediatypes(functionParameter, "application/xml");
		Set<String> jsonTextMediaType = getModeledConsumedMediatypes(functionParameter, "text/json");
		Set<String> xmlTextMediaType = getModeledConsumedMediatypes(functionParameter, "text/xml");
        Set<String> mediaTypes = getModeledConsumedMediatypes(functionParameter);
        
        if (!jsonMediaType.isEmpty() || !xmlMediaType.isEmpty() ||
        	!jsonTextMediaType.isEmpty() || !xmlTextMediaType.isEmpty()) {
        	result = true;
        } else if (paramType instanceof ComplexType && mediaTypes.isEmpty()) {
        	result = true;
        }
		return result;
	}
	
	/**
	 * This method constructs the modeled path by combining information from the function,
	 * its owning function module and the function's parameters. 
	 * 
	 * @param function
	 * @return
	 */
	public static String getEffectivePath(Function function) {
		String modeledPathValue = ServiceModelProcessing.getPath(function);
		String pathValue = null;
		if (modeledPathValue != null && !modeledPathValue.isEmpty()) {
			pathValue = modeledPathValue;  // there is an explicit path value set, so we do not need to construct one in the subsequent parameter processing loop
		} else {
			pathValue = "";
			for (FunctionParameter functionParam : function.getFunctionInParameters()) {
				Optional<ParamTypeEnum> paramType = ServiceModelProcessing.getParamType(functionParam);
				if (paramType.isPresent()) {
					RestOptionEnum.ParamTypeEnum paramTypeEnum = RestOptionEnum.ParamTypeEnum.getByName(paramType.get().getName().replace("Param", ""));
					switch (paramTypeEnum) {
					case PATH:
						pathValue += "/{" + functionParam.getName().toLowerCase() + "}";
						break;
					default:
						break;
					}
				}
			}
		}
		
		String modeledPathValueForFunctionModule = ServiceModelProcessing.getPath((FunctionModule)function.getModule());
		String pathFromFunctionModuleName = function.getModule().getName().toLowerCase();
		if (modeledPathValueForFunctionModule != null && !modeledPathValueForFunctionModule.isEmpty()) {
			pathFromFunctionModuleName = modeledPathValueForFunctionModule;
		}
	    pathValue = pathFromFunctionModuleName + "/" + pathValue;

	    // --- remove duplicate slashes and a leading slash
	    pathValue = ServiceModelProcessing.normalizePathValue(pathValue);
	    
	    
	    return pathValue;
	}
}
