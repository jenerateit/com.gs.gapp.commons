package com.gs.gapp.metamodel.function;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Optional;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelValidatorI;

/**
 * AJAX-199
 * 
 * <p>This validator checks, whether there are any modeled HTTP status codes that are not in the valid
 * range of 100 to 599.
 *
 */
public class ValidatorHttpStatusCode implements ModelValidatorI {

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(assertHttpStatusCodesAreValid(modelElements));
		return result;
	}
	
	private boolean isHttpStatusCodeValid(Long statusCode) {
		return 100L <= statusCode && statusCode <= 599L;
	}

	/**
	 * @param rawElements
	 */
	private Collection<Message> assertHttpStatusCodesAreValid(Collection<Object> rawElements) {
		final Collection<Message> result = new LinkedHashSet<>();
		
		Map<ModelElement,Message> messages = new LinkedHashMap<>();
		
		rawElements.stream().filter(Function.class::isInstance).map(Function.class::cast)
			.forEach(function -> {
				Optional<Long> successStatusCode = ServiceModelProcessing.getModeledSuccessStatusCode(function);
				if (successStatusCode.isPresent() && !isHttpStatusCodeValid(successStatusCode.get())) {
					if (!messages.containsKey(function)) {
						Message errorMessage = FunctionMessage.ERROR_INVALID_HTTP_STATUS_CODE.getMessageBuilder()
							.modelElement(function)
							.parameters(successStatusCode.get(), "function " + function.getQualifiedName())
							.build();
						messages.put(function, errorMessage);
					}
				}
				
				function.getBusinessExceptions().forEach(businessException -> {
					Optional<Long> businessExceptionStatusCode = ServiceModelProcessing.getModeledStatusCode(businessException);
					if (businessExceptionStatusCode.isPresent() && !isHttpStatusCodeValid(businessExceptionStatusCode.get())) {
						if (!messages.containsKey(businessException)) {
							Message errorMessage = FunctionMessage.ERROR_INVALID_HTTP_STATUS_CODE.getMessageBuilder()
								.modelElement(businessException)
								.parameters(successStatusCode.get(), "business exception " + businessException.getQualifiedName())
								.build();
							messages.put(businessException, errorMessage);
						}
					}
				});
				
                function.getTechnicalExceptions().forEach(technicalException -> {
                	Optional<Long> technicalExceptionStatusCode = ServiceModelProcessing.getModeledStatusCode(technicalException);
                	if (technicalExceptionStatusCode.isPresent() && !isHttpStatusCodeValid(technicalExceptionStatusCode.get())) {
						if (!messages.containsKey(technicalException)) {
							Message errorMessage = FunctionMessage.ERROR_INVALID_HTTP_STATUS_CODE.getMessageBuilder()
								.modelElement(technicalException)
								.parameters(successStatusCode.get(), "exception " + technicalException.getQualifiedName())
								.build();
							messages.put(technicalException, errorMessage);
						}
					}
				});
			});
		
		result.addAll(messages.values());

		return result;
	}
}
