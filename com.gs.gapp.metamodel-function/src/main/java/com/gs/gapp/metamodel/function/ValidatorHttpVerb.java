package com.gs.gapp.metamodel.function;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Optional;

import com.gs.gapp.metamodel.basic.ModelValidatorI;

/**
 * AJAX-199
 * 
 * <p>This validator checks, whether there are any modeled HTTP status codes that are not in the valid
 * range of 100 to 599.
 *
 */
public class ValidatorHttpVerb implements ModelValidatorI {
	
	private static enum SUPPORTED_HTTP_VERBS {
		DELETE,
		GET,
		HEAD,
		OPTIONS,
		POST,
		PUT,
		PATCH,
		;
	}

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(assertHttpVerbsAreValid(modelElements));
		return result;
	}
	
	/**
	 * @param rawElements
	 */
	private Collection<Message> assertHttpVerbsAreValid(Collection<Object> rawElements) {
		final Collection<Message> result = new LinkedHashSet<>();
		
		rawElements.stream().filter(Function.class::isInstance).map(Function.class::cast)
			.forEach(function -> {
				Optional<String> httpVerb = ServiceModelProcessing.getModeledHttpVerb(function);
				if (httpVerb.isPresent()) {
				    SUPPORTED_HTTP_VERBS verb = null;
				    try {
					    verb = SUPPORTED_HTTP_VERBS.valueOf(httpVerb.get());
				    } catch (IllegalArgumentException ex) {/*ignore it intentionally*/}
					
					if (verb == null) {
						Message errorMessage = FunctionMessage.ERROR_INVALID_HTTP_VERB.getMessageBuilder()
							.modelElement(function)
							.parameters(httpVerb.get(), function.getQualifiedName())
							.build();
						result.add(errorMessage);
					}
				}
			});
		
		return result;
	}
}
