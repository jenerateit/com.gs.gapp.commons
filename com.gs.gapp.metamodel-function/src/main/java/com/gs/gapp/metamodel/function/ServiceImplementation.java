package com.gs.gapp.metamodel.function;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * A service implementation consumes data from or provides data to a caller of the service.
 * The service implementation optionally can follow one of the patterns that are defined in
 * {@link ServicePattern}.
 * 
 * A caller of a service only knows very little if not nothing about the usage
 * of the pattern.
 * 
 * A service implementation can be exposed to remote clients or it can be used internally
 * by making in-process calls. Conceptually, you can also combine the two kinds of usages.
 * From a technical point of view, service exposure and internal usage of services might require a different/separate
 * implementation.
 * 
 * @author mmt
 *
 */
public class ServiceImplementation extends ModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = -866661199337493246L;
	
	private final ServiceInterface serviceInterface;
	
	private final Set<ServiceClient> usedServiceClients = new LinkedHashSet<>();
	
	private ServicePattern servicePattern = ServicePattern.SKELETON;
	
	private final Set<ServiceInterface> consumedServiceInterfaces = new LinkedHashSet<>();
	
	/**
	 * @param name
	 */
	public ServiceImplementation(String name, ServiceInterface serviceInterface) {
		super(name);
		if (serviceInterface == null) throw new NullPointerException("parameter 'serviceInterface' must not be null");
		this.serviceInterface = serviceInterface;
	}

	public ServiceInterface getServiceInterface() {
		return serviceInterface;
	}
	
	/**
	 * @return
	 */
	public Set<ServiceClient> getUsedServiceClients() {
		return usedServiceClients;
	}

	/**
	 * @param usedServiceClients
	 */
	public void addUsedServiceClients(ServiceClient... usedServiceClients) {
	    this.usedServiceClients.addAll(Arrays.asList(usedServiceClients));
	}

	public ServicePattern getServicePattern() {
		return servicePattern;
	}

	public void setServicePattern(ServicePattern servicePattern) {
		this.servicePattern = servicePattern;
	}

	/**
	 * A service implementation can use one or more service interfaces.
	 * At runtime of a generated application a decision is taken
	 * on which service implementations to use that match the given
	 * service interfaces.
	 * 
	 * @return an unmodifiable set of service interfaces
	 */
	public Set<ServiceInterface> getConsumedServiceInterfaces() {
		return Collections.unmodifiableSet(this.consumedServiceInterfaces);
	}
	
	public boolean addConsumedServiceInterface(ServiceInterface serviceInterface) {
		return this.consumedServiceInterfaces.add(serviceInterface);
	}
	
	/**
	 * @author mmt
	 *
	 */
	public static enum ServicePattern {
		/**
		 * This is the default service pattern, where the whole
		 * service implementation needs to be coded by hand.
		 */
		SKELETON,
		/**
		 * Read data from a database and write data to a database.
		 */
		PERSISTENCE,
		/**
		 * Adapt one interface to another. This solves
		 * the problem of non-compatible interfaces.
		 */
		ADAPTER,
		/**
		 * Hide complexity behind a facade, This makes
		 * an interface simpler to use.
		 */
		FACADE,
		/**
		 * Provides the same interface as the service that it calls.
		 * It additionally does some housekeeping. It could for instance
		 * add exception handling functionality.
		 */
		PROXY,
		/**
		 * The service implementation chooses one of a set of service implementations
		 * that all implement the same interface.
		 */
		SELECTOR,
		/**
		 * Calling several other services in a certain order.
		 * A special case of a mashup is to call a set of services
		 * that all implement the same interface.
		 */
		MASHUP,
		/**
		 * This kind of service implementation provides test data without accessing
		 * a database or any other service. Its only purpose is to be able to test
		 * implementations that use certain service interfaces without the need to
		 * set up a complicated service landscape or databases.
		 */
		MOCK,
		;
	}
}
