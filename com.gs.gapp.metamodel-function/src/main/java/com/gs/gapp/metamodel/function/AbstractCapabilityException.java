package com.gs.gapp.metamodel.function;

import java.util.HashMap;
import java.util.Map;

import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;

public class AbstractCapabilityException extends ExceptionType {

	private static final long serialVersionUID = 7195100445001398368L;

	public AbstractCapabilityException(String name) {
		super(name);
		this.setAbstractType(true);
	}
	
    public static enum FieldEnum {
		
		STATUS_NUMBER ("statusNumber", "an error number that indicates, which error has occurred that caused this exception to be risen"),
		STATUS_CODE ("statusCode",     "a short error text that indicates, which error has occurred that caused this exception to be risen"),
		;
		
        private static final Map<String, FieldEnum> stringToEnum = new HashMap<>();
		
		static {
			for (FieldEnum enumEntry : values()) {
				stringToEnum.put(enumEntry.getName(), enumEntry);
			}
		}
		
		/**
		 * @param name
		 * @return
		 */
		public static FieldEnum fromString(String name) {
			return stringToEnum.get(name);
		}
		
		private final String name;
		private final String documentation;
		
		/**
		 * @param name
		 * @param documentation
		 */
		private FieldEnum(String name, String documentation) {
			this.name = name;
			this.documentation = documentation;
		}

		public String getName() {
			return name;
		}

		public String getDocumentation() {
			return documentation;
		}
	}
}
