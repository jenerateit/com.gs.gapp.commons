package com.gs.gapp.metamodel.function;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelValidatorI;

/**
 * AJAX-199
 * 
 * <p>This validator checks, whether there are service functions in the model
 * that use more than two path parameters.
 *
 */
public class ValidatorNumberOfPathParameters implements ModelValidatorI {
	
	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(checkPathParameters(modelElements));
		return result;
	}
	
	/**
	 * @param rawElements
	 */
	private Collection<Message> checkPathParameters(Collection<Object> rawElements) {
		final Collection<Message> result = new LinkedHashSet<>();
		
		rawElements.stream().filter(Function.class::isInstance).map(Function.class::cast)
			.forEach(function -> {
				Set<FunctionParameter> pathParameters = ServiceModelProcessing.getFunctionInPathParameters(function);
				if (pathParameters.size() > 2) {
					Message warningMessage = FunctionMessage.WARNING_HIGH_NUMBER_OF_PATH_PARAMETERS.getMessageBuilder()
						.modelElement(function)
						.parameters(function.getQualifiedName(), pathParameters.size())
						.build();
					result.add(warningMessage);
				}
			});
		
		return result;
	}
}
