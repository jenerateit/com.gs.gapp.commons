package com.gs.gapp.metamodel.function;

import com.gs.gapp.metamodel.basic.MessageI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.MessageStatus;

/**
 * @author marcu
 *
 */
public enum FunctionMessage implements MessageI {
	
	// ------------------ ERRORS -----------------------
	
	/**
	 * AJAX-147
	 */
	PATH_PARAMETER_TEMPLATE_IS_WRONG (MessageStatus.ERROR, "0001",
			"The path template '%s' of function '%s' in function module '%s' uses variables that do not match the set of in-parameters.",
			"Check your model and correct the template and/or the variables."),
	
	/**
	 * AJAX-147
	 */
	PATH_PARAMETER_VARIABLES_BUT_NO_TEMPLATE (MessageStatus.ERROR, "0002",
			"The function '%s' in function module '%s' has at least one in-parameter for path variables, but the function doesn't have a template set.",
			"Check your model and provide a path template or remove the in-parameters that represent path variables."),
	
	/**
	 * AJAX-147
	 */
	ERROR_HTTP_VERB_MUST_NOT_BE_USED_WITH_MULTIPART (MessageStatus.ERROR, "0003",
			"The function '%s' in function module '%s' uses the HTTP verb %s. At the same time it uses a multipart media type. This combination is not allowed.",
			"Please check your model and change the number of IN or OUT parameters or set a different media type or set a different HTTP verb."),
	
	/**
	 * AJAX-147
	 */
	ERROR_HTTP_VERB_MUST_NOT_HAVE_REQUEST_PARAMETERS_IN_THE_BODY (MessageStatus.ERROR, "0004",
			"The function '%s' in function module '%s' uses the HTTP verb %s. At the same time it has input parameters defined for the request body. This combination is not allowed.",
			"Please check your model and remove the input parameters or set a different HTTP verb."),
	
	/**
	 * AJAX-147
	 */
	ERROR_HTTP_GET_MUST_NOT_CONSUME_MULTIPART (MessageStatus.ERROR, "0005",
			"The function '%s' in function module '%s' uses the HTTP verb GET. At the same time it consumes a multipart media type. This combination is not allowed.",
			"Please check your model and get rid of the IN parameters for the request body or set the HTTP verb to POST."),
	
	/**
	 * AJAX-147
	 */
	ERROR_HTTP_VERB_MUST_NOT_HAVE_RESPONSE_PARAMETERS_IN_THE_BODY (MessageStatus.ERROR, "0006",
			"The function '%s' in function module '%s' uses the HTTP verb %s. At the same time it has output parameters defined for the response body. This combination is not allowed.",
			"Please check your model and remove the output parameters or set a different HTTP verb."),
	
	/**
	 * AJAX-147
	 */
	ERROR_HTTP_POST_CANNOT_BE_IDEMPOTENT (MessageStatus.ERROR, "0007",
			"The function '%s' in function module '%s' uses the HTTP verb POST and claims to be idempotent. POST by nature cannot be idempotent.",
			"Please check your model and remove the idempotent setting or set a different HTTP verb."),
	
	/**
	 * AJAX-199
	 */
	ERROR_INVALID_HTTP_STATUS_CODE (MessageStatus.ERROR, "0008",
			"An invalid HTTP status code '%s' has been found in the input model. It has been found to having been assigned to this model element: %s.",
			"Please check your model and model a valid HTTP status code, which at least has to be in the range between 100 and 599. Please check this link for more information: https://developer.mozilla.org/de/docs/Web/HTTP/Status"),
	
	/**
	 * AJAX-199
	 */
	ERROR_INVALID_HTTP_VERB (MessageStatus.ERROR, "0009",
			"An invalid HTTP verb '%s' has been found in the input model, being assigned to the function %s.",
			"Please check your model and model a valid HTTP verb. Supported HTTP verbs are: DELETE, GET, HEAD, OPTIONS, POST, PUT, PATCH."),
	
	ERROR_NON_BODY_PARAMETERS_WITH_COMPLEX_TYPES (MessageStatus.ERROR, "0010",
			"The function '%s' has at least one non-body parameter defined that uses a complex type: %s. The generator doesn't support this yet.",
			"Please check your model and consider using a different type for the parameters, e.g. a string, and take care of the de-/serialization yourself."
			+ "If your generator supports the generation for a media type multipart/*, you can also model more than one body parameter to transfer more complex data."),
	
	ERROR_INCOMPATIBLE_SERVICE_IMPLEMENTATIONS_FOUND (MessageStatus.ERROR, "0011",
			"The service client '%s' that implements the API '%s' consumes service implementations that do not implement that API: %s",
			"Please check your model and fix it."),
	
	// ------------------ WARNINGS -----------------------
	/**
	 * AJAX-147
	 */
	WARNING_HTTP_VERB_DOESNT_HAVE_RESPONSE_PARAMETERS_IN_THE_BODY (MessageStatus.WARNING, "1001",
			"The function '%s' in function module '%s' uses the HTTP verb %s. But it doesn't have any parameters defined for the response body.",
			"If this is not what you desire: please check your model and add some output parameters or choose a different HTTP verb."),
	
	/**
	 * AJAX-147
	 */
	WARNING_HTTP_VERB_DOESNT_HAVE_REQUEST_PARAMETERS_IN_THE_BODY (MessageStatus.WARNING, "1002",
			"The function '%s' in function module '%s' uses the HTTP verb %s. But it doesn't have any parameters defined for the request body.",
			"If this is not what you desire: please check your model and add some input parameters or choose a different HTTP verb."),
	
	
	WARNING_FUNCTION_PATH_ALREADY_PART_OF_MODULE_PATH (MessageStatus.WARNING, "1003",
			"The function '%s' in function module '%s' has a path %s set, which is already contained in the function module's path %s.",
			"It is unlikely that this is what you desire. Please check your model and modify the path settings."),
	
	WARNING_BUSINESS_EXCEPTION_HAS_WRONG_HTTP_STATUS_CODE (MessageStatus.WARNING, "1004",
			"The business exception '%s' has an HTTP status %s set that normally is used for technical exceptions.",
			"Please consider to use a different HTTP status in the range of 400 <= status <= 499."),
	
	WARNING_TECHNICAL_EXCEPTION_HAS_WRONG_HTTP_STATUS_CODE (MessageStatus.WARNING, "1005",
			"The technical exception '%s' has an HTTP status %s set that normally is used for business exceptions.",
			"Please consider to use a different HTTP status outside the range of 400 <= status <= 499."),
			
	WARNING_HIGH_NUMBER_OF_PATH_PARAMETERS (MessageStatus.WARNING, "1006",
			"The function '%s' has %s path parameters. It is recommended to not use more than 2 path parameters.",
			"Please consider to decrease the number of path parameters in the function model."),
	
	// ------------------ INFOS --------------------------
	INFO_COMPLETED_HTTP_VERB (MessageStatus.INFO, "2001",
			"There is no HTTP verb modeled for the function '%s'. The HTTP verb %s is going to be used. Reason: %s",
			null),
	
	INFO_COMPLETED_QUERY_PARAM_TYPE (MessageStatus.INFO, "2002",
			"There is no parameter type modeled for the %s parameter '%s' of function '%s', the parameter's REST-type is primitive and the HTTP verb is GET." +
	        " The parameter's REST-type is now set to QUERY.",
			null),
	
	INFO_COMPLETED_BODY_PARAM_TYPE (MessageStatus.INFO, "2003",
			"There is no parameter type modeled for the %s parameter '%s' of function '%s'. The parameter's REST-type is now set to BODY.",
			null),
	
	INFO_CHANGED_BODY_TO_QUERY_PARAM_TYPE (MessageStatus.INFO, "2004",
			"The REST-type of the %s parameter '%s' of function '%s' has originally been set to BODY, but the HTTP verb has been set to GET. A GET request must not have a body." +
	        " For this reason the REST-param type is now changed to QUERY.",
			null),
	
	INFO_COMPLETED_IN_PARAM_MEDIA_TYPE (MessageStatus.INFO, "2005",
			"The input parameter '%s' of function '%s' doesn't have a media type set and the parameter type is '%s'." +
	        " The parameter's media type is now set to %s.",
			null),
	
	INFO_COMPLETED_OUT_PARAM_MEDIA_TYPE (MessageStatus.INFO, "2006",
			"The output parameter '%s' of function '%s' doesn't have a media type set and the parameter type is '%s'." +
	        " The parameter's media type is now set to %s.",
			null),
	
	INFO_COMPLETED_IN_CONSUMED_MEDIA_TYPE (MessageStatus.INFO, "2007",
			"The function '%s' doesn't have a consumed media type set." +
	        " The function's consumed media type is now set to %s.",
			null),
	
	INFO_COMPLETED_OUT_CONSUMED_MEDIA_TYPE (MessageStatus.INFO, "2008",
			"The function '%s' doesn't have a produced media type set." +
	        " The function's produced media type is now set to %s.",
			null),
	;

	private final MessageStatus status;
	private final String organization = "GS";
	private final String section = "FUNCTION";
	private final String id;
	private final String problemDescription;
	private final String instruction;
	
	private FunctionMessage(MessageStatus status, String id, String problemDescription, String instruction) {
		this.status = status;
		this.id = id;
		this.problemDescription = problemDescription;
		this.instruction = instruction;
	}
	
	@Override
	public MessageStatus getStatus() {
		return status;
	}

	@Override
	public String getOrganization() {
		return organization;
	}

	@Override
	public String getSection() {
		return section;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getProblemDescription() {
		return problemDescription;
	}
	
	@Override
	public String getInstruction() {
		return instruction;
	}
}
 