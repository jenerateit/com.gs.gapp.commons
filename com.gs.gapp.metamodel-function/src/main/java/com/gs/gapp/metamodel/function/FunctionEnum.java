package com.gs.gapp.metamodel.function;

import java.util.HashMap;

import com.gs.gapp.metamodel.persistence.enums.StorageFunction;

public enum FunctionEnum {

	DELETE ("delete", StorageFunction.DELETE),
	DELETE_BULK ("deleteBulk", StorageFunction.DELETE_MANY),
	DELETE_ALL ("deleteAll", StorageFunction.DELETE_ALL),
	READ ("read", StorageFunction.READ),
	READ_LIST ("readList", StorageFunction.READ_MANY),
	UPDATE ("update", StorageFunction.UPDATE),
	UPDATE_BULK ("updateBulk", StorageFunction.UPDATE_MANY),
	CREATE ("create", StorageFunction.CREATE),
	CREATE_BULK ("createBulk", StorageFunction.CREATE_MANY),
	MULTI_PURPOSE ("process", StorageFunction.MULTI_PURPOSE),
	/**
	 * Search function is going to be created when an entity has one or more
	 * fields being marked as "searchable". 
	 */
	SEARCH ("search", null),
	;

	private static HashMap<String, FunctionEnum> entryMap =
			new HashMap<>();
	
	private static HashMap<StorageFunction, FunctionEnum> entryMapStorageFunction =
			new HashMap<>();

	public static String PARAM_NAME_ID = "id";
	public static String PARAM_NAME_BEAN = "bean";
	public static String PARAM_NAME_ENTITY = "entity";
	public static String PARAM_NAME_OFFSET = "offset";
	public static String PARAM_NAME_LIMIT = "limit";
	public static String PARAM_NAME_IDS = "ids";
	public static String PARAM_NAME_BEANS = "beans";


	static {
		for (FunctionEnum enumEntry : values()) {
            entryMap.put(enumEntry.getName().toLowerCase(), enumEntry);
            entryMapStorageFunction.put(enumEntry.getStorageFunction(), enumEntry);
		}
	}



	private final String name;
	private final StorageFunction storageFunction;

	private FunctionEnum(String name, StorageFunction storageFunction) {
		this.name = name;
		this.storageFunction = storageFunction;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * @return
	 */
	public static FunctionEnum forName(String name) {
		if (name != null) {
            return entryMap.get(name.toLowerCase());
		}
		return null;
	}
	
	/**
	 * @param storageFunction
	 * @return
	 */
	public static FunctionEnum forStorageFunction(StorageFunction storageFunction) {
		if (storageFunction != null) {
            return entryMapStorageFunction.get(storageFunction);
		}
		return null;
	}

	public StorageFunction getStorageFunction() {
		return storageFunction;
	}
}
