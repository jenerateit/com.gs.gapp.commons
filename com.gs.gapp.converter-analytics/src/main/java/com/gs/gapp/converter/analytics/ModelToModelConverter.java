package com.gs.gapp.converter.analytics;

import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;

/**
 * The sole purpose of this element-converter is to easily be able to include ModelTo&lt;something&gt;Converter element-converters
 * in the element converter tree analysis. A new instance of type Model is not created in this element-converter
 * but in the AbstractConverter model-converter class.
 * 
 * @author mmt
 *
 * @param <S>
 * @param <T>
 */
public class ModelToModelConverter<S extends Model, T extends Model> extends
		AbstractM2MModelElementConverter<S, T> {

	public ModelToModelConverter(
			AbstractConverter modelConverter) {
		super(modelConverter);
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) getModelConverter().getModel();
		return result;
	}
}
