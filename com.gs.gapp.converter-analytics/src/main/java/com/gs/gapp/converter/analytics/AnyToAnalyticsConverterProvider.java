/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.converter.analytics;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;
import org.osgi.service.component.annotations.Component;

/**
 * @author hrr
 *
 */
@Component
public class AnyToAnalyticsConverterProvider implements ModelConverterProviderI {

	/**
	 *
	 */
	public AnyToAnalyticsConverterProvider() {
		super();
	}

	@Override
	public ModelConverterI getModelConverter() {
		return new AnyToAnalyticsConverter();
	}
}
