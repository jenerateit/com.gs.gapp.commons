package com.gs.gapp.converter.analytics;

import com.gs.gapp.metamodel.analytics.ModelElementTreeVisualizationTextDocument;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;

public class ModelToModelElementTreeVisualizationTextDocumentConverter<T extends ModelElementTreeVisualizationTextDocument> extends
		AbstractM2MModelElementConverter<Model, T> {

	public ModelToModelElementTreeVisualizationTextDocumentConverter(
			AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(Model originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);


	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(Model originalModelElement, ModelElementI previouslyResultingElement) {
		T result;

		result = (T) new ModelElementTreeVisualizationTextDocument(originalModelElement.getName());
        result.setOriginatingElement(originalModelElement);
		return result;
	}
}
