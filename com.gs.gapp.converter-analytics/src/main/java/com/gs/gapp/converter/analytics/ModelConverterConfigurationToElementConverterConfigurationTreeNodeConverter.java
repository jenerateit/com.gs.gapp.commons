package com.gs.gapp.converter.analytics;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.analytics.ElementConverterConfiguration;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

/**
 * This model element converter creates a tree of instances of {@link ElementConverterConfigurationTreeNode} that
 * represent the set of model element converters of a given model converter. Information about the model converter
 * is provided by the instance of {@link TransformationStepConfiguration} class.
 * 
 * @author mmt
 *
 * @param <S>
 * @param <T>
 */
public class ModelConverterConfigurationToElementConverterConfigurationTreeNodeConverter<S extends TransformationStepConfiguration, T extends ElementConverterConfigurationTreeNode> extends
		AbstractM2MModelElementConverter<S, T> {

	public ModelConverterConfigurationToElementConverterConfigurationTreeNodeConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S transformationStepConfiguration, T elementConverterConfigurationTreeNode) {
		super.onConvert(transformationStepConfiguration, elementConverterConfigurationTreeNode);

		// --- loop through the model converter configurations and setup the tree
		Set<ElementConverterConfigurationTreeNode> potentialLeadingNodes = new LinkedHashSet<>();
		Set<ElementConverterConfigurationTreeNode> potentialLeadingNodesOfPreviousModelConverter = new LinkedHashSet<>();
		if (transformationStepConfiguration.getParentConfiguration() != null) {
			// we are going to use the previous' model converter configuration instance's root node in order to get a full element converter tree in the end
			potentialLeadingNodesOfPreviousModelConverter.addAll(transformationStepConfiguration.getParentConfiguration().getTreeNodes());
		} else {
			potentialLeadingNodesOfPreviousModelConverter.add(elementConverterConfigurationTreeNode);  // this is the root node of the whole tree
		}

		Set<ElementConverterConfigurationTreeNode> addedNodes = new LinkedHashSet<>();
		int counter = 0;
		
		// --- Repeatedly looping throug the model converter's element converter configurations in order to find element
		// --- converters where one element converter can take the output of the other element converter as input.
		// --- The do-while loop at latest stops when there are 100 loops reached, making sure that there is not an infinite loop.
		do {

			potentialLeadingNodes.clear();
			if (counter == 0) {
				// only for the very first time we need to check the element converters of the previous model converter
			    potentialLeadingNodes.addAll(potentialLeadingNodesOfPreviousModelConverter);
			} else if (counter > 0) {
				potentialLeadingNodes.addAll(addedNodes);
			}

			addedNodes.clear();
			for (ElementConverterConfigurationTreeNode potentialLeadingNode : potentialLeadingNodes) {
				for (ElementConverterConfiguration elementConverterConfiguration : transformationStepConfiguration.getElementConverterConfigurations()) {

				    if (potentialLeadingNode.isRoot() ||
				    		(potentialLeadingNode.getElementConverterConfiguration() != null &&
				    		 potentialLeadingNode.getElementConverterConfiguration().canBeFollowedBy(elementConverterConfiguration))) {

				    	if (potentialLeadingNode.branchContainsElementConverterConfiguration(elementConverterConfiguration) == false) {
					    	ElementConverterConfigurationTreeNode newNode =
					    			new ElementConverterConfigurationTreeNode(elementConverterConfiguration,
					    					                                  elementConverterConfiguration.getModelConverterConfiguration());
					    	newNode.setParent(potentialLeadingNode);
					    	addedNodes.add(newNode);
				    	}
				    }
			    }
			}

			transformationStepConfiguration.addTreeNodes(addedNodes);
			counter++;
		} while (counter <= 100 && addedNodes.size() > 0);

		return;
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (result) {
			TransformationStepConfiguration modelConverterConfiguration = (TransformationStepConfiguration) originalModelElement;
			if (modelConverterConfiguration.getModel() != null && (modelConverterConfiguration.getModel() != getModelConverter().getModel())) {
				result = false;  // The model converter configuration that got created by a leading model converter must not be converted by this element converter
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S transformationStepConfiguration, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new ElementConverterConfigurationTreeNode(transformationStepConfiguration.getName(), null, transformationStepConfiguration);  // root node of the tree of element converter configuration tree nodes
		return result;
	}
}
