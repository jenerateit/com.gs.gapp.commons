package com.gs.gapp.converter.analytics;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.gs.gapp.metamodel.analytics.ModelElementTreeNode;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelElementTraceabilityI;
import com.gs.gapp.metamodel.basic.TargetElement;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

/**
 * This model element converter is creating a tree of instances of {@link ModelElementTreeNode}.
 * That tree represents the tree of model elements that are used/created during the execution
 * of a generator. The collected information can be used to generate code/configuration that visualizes that
 * tree of model elements.
 * 
 * @author mmt
 *
 * @param <T>
 */
public class ModelToModelElementTreeNodeConverter<T extends ModelElementTreeNode> extends
		AbstractM2MModelElementConverter<Model, T> {

	public ModelToModelElementTreeNodeConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.DEFAULT);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(Model model, T modelElementTreeNode) {
		super.onConvert(model, modelElementTreeNode);

		// --- here we need to create the whole tree of model elements, resultingModelElement is the tree's root node
		Map<ModelElementI, ModelElementTreeNode> map = new LinkedHashMap<>();
		ModelElementTreeNode treeNode = null;
		ModelElementTreeNode previousTreeNode = null;
		ModelElementI currentModelElement = null;
		Object originatingElement = null;
		boolean foundExistingNode = false;

		Set<ModelElementI> elementsToBeProcessed = new LinkedHashSet<>(model.getElements());
		elementsToBeProcessed.add(model);  // We need to process the model itself, too. But that model element of type "Model" is not in the result set of getElements()
		for (ModelElementI modelElement : elementsToBeProcessed) {

			if (modelElement instanceof Model) {
				@SuppressWarnings("unused")
				Model aModel = (Model) modelElement;
			}
			currentModelElement = modelElement;
            previousTreeNode = null;
            foundExistingNode = false;
			do {

				if (map.containsKey(currentModelElement)) {
					treeNode = map.get(currentModelElement);
					foundExistingNode = true;
				} else {
					foundExistingNode = false;
				    treeNode = new ModelElementTreeNode(currentModelElement);
				    map.put(modelElement, treeNode);
				}

				if (previousTreeNode != null) {
					previousTreeNode.setParent(treeNode);
				}

				if (foundExistingNode == false) {
					previousTreeNode = treeNode;
					if (currentModelElement instanceof ModelElementTraceabilityI) {
						ModelElementTraceabilityI traceableElement = (ModelElementTraceabilityI) currentModelElement;
						originatingElement = traceableElement.getOriginatingElement();
						if (originatingElement instanceof ModelElementI) {
							currentModelElement = (ModelElementI) originatingElement;
						} else {
							currentModelElement = null;
						}
					} else {
						currentModelElement = null;
					}
				} else {
					break;  // no need to continue backtracking here, since there are already tree node elements
				}
			} while (currentModelElement != null);

			if (foundExistingNode == false && previousTreeNode.getParent() == null) {
				previousTreeNode.setParent(modelElementTreeNode);
			}

			// --- from here traverse the model element's resulting elements tree
            traverseResultingModelElements(map.get(modelElement), modelElement);
		}

		return;
	}

	/**
	 * @param currentTreeNode
	 * @param currentModelElement
	 */
	private void traverseResultingModelElements(ModelElementTreeNode currentTreeNode, ModelElementI currentModelElement) {
		if (currentTreeNode.getModelElement() instanceof ModelElementTraceabilityI) {
			ModelElementTraceabilityI traceableElement = (ModelElementTraceabilityI) currentTreeNode.getModelElement();
			for (ModelElementI resultingModelElement : traceableElement.getResultingModelElements()) {
				if (resultingModelElement instanceof TargetElement) {
					@SuppressWarnings("unused")
					TargetElement targetElement = (TargetElement) resultingModelElement;
				}
				ModelElementTreeNode newTreeNode = new ModelElementTreeNode(resultingModelElement);
				newTreeNode.setParent(currentTreeNode);
				traverseResultingModelElements(newTreeNode, resultingModelElement);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(Model model, ModelElementI previouslyResultingElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new ModelElementTreeNode(model);
		return result;
	}
}
