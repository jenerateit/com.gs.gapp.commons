package com.gs.gapp.converter.analytics;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.analytics.MxGraphJavaScriptLib;
import com.gs.gapp.metamodel.anyfile.AnyFile;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

/**
 * This converter creates an instance of {@link AnyFile} that contains
 * the mxGraph JavaScript framework. This framework is going to be used
 * to visualize the inner workings of a generator (model element tree,
 * model element converter configuration tree).
 * 
 * @author mmt
 *
 * @param <S>
 * @param <T>
 */
public class ModelToMxGraphJavaScriptConverter<S extends Model, T extends MxGraphJavaScriptLib> extends AbstractM2MModelElementConverter<S, T> {

	public ModelToMxGraphJavaScriptConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.CREATE_AND_CONVERT_IN_ONE_GO);
	}

	@Override
	protected T onCreateModelElement(S model, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T result = (T) new MxGraphJavaScriptLib("mxClient.min", "js", "js");
		
		String javaScriptFileName = "/mxClient.min.js";
		InputStream inputStream = getClass().getResourceAsStream(javaScriptFileName);
		String javaScriptContent = new BufferedReader(new InputStreamReader(inputStream))
				  .lines().collect(Collectors.joining("\n"));
		result.setContent(javaScriptContent.getBytes());
		
		if (result.getContent() == null || result.getContent().length == 0) {
			throw new ModelConverterException("failed to read mxGraph JavaScript framework '" + javaScriptFileName + "'");
		}
		
		return result;
	}
}
