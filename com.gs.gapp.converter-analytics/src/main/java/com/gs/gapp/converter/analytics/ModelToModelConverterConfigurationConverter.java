package com.gs.gapp.converter.analytics;

import java.lang.reflect.Method;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.dsl.IMetatyped;
import com.gs.gapp.metamodel.analytics.ElementConverterConfiguration;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * This model element converter creates an object that holds information about its owning
 * model converter (kept in an instance of {@link TransformationStepConfiguration}).
 * The object of type {@link TransformationStepConfiguration} is part of a tree of such
 * objects. The nodes of that tree are linked to each other by means of
 * {@link TransformationStepConfiguration#getParentConfiguration()} and
 * {@link TransformationStepConfiguration#getChildConfigurations()}.
 * The purpose of the created tree of transformation step configurations is to
 * be able to create a visualization of the transformation step tree, giving the
 * generator developer insights to the inner workings of a generator.
 * 
 * @author mmt
 *
 * @param <T>
 */
public class ModelToModelConverterConfigurationConverter<T extends TransformationStepConfiguration> extends
		AbstractM2MModelElementConverter<Model, T> {

	public ModelToModelConverterConfigurationConverter(
			AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(Model model, T transformationStepConfiguration) {
		super.onConvert(model, transformationStepConfiguration);

		// --- iterate over the model converter's element converters in order to collect their meta data
		for (AbstractModelElementConverter<? extends Object,? extends ModelElementI> elementConverter : getModelConverter().getAllModelElementConverters()) {
				boolean isIsResponsibleOverwritten = false;
				Class<?> sourceClass = elementConverter.getSourceClass();
				Class<?> targetClass = elementConverter.getTargetClass();
				IMetatyped metatypedConverter = null;
				
				if (elementConverter instanceof IMetatyped) {
					metatypedConverter = (IMetatyped) elementConverter;
				}
				isIsResponsibleOverwritten = isIsResponsibleOverwritten(elementConverter);

				ElementConverterConfiguration elementConverterConfiguration = null;
				if (metatypedConverter != null) {
					elementConverterConfiguration =
							new ElementConverterConfiguration(sourceClass,
									                          targetClass,
									                          elementConverter.getClass().getSimpleName(),
									                          elementConverter.getClass().getName(),
									                          isIsResponsibleOverwritten,
									                          metatypedConverter);
				} else {
					elementConverterConfiguration =
							new ElementConverterConfiguration(sourceClass,
									                          targetClass,
									                          elementConverter.getClass().getSimpleName(),
									                          elementConverter.getClass().getName(),
									                          isIsResponsibleOverwritten);
				}
				
				transformationStepConfiguration.addElementConverterConfiguration(elementConverterConfiguration);
		}

		// --- link the resulting model converter configuration to the configuration of the previous model converter configuration
		Set<TransformationStepConfiguration> previousModelConverterConfigurations = model.getElements(TransformationStepConfiguration.class);
		if (previousModelConverterConfigurations.size() == 1) {
			TransformationStepConfiguration previousConfiguration = previousModelConverterConfigurations.iterator().next();
			transformationStepConfiguration.setParentConfiguration(previousConfiguration);
		} else if (previousModelConverterConfigurations.size() > 1) {
			throw new ModelConverterException("a previous model element of type 'Model' must not contain more than one element of type 'TransformationStepConfig'");
		}

		// --- finally, construct the element converter tree, which then can be use for further conversions or for the generation of tree visualizations
		@SuppressWarnings("unused")
		ElementConverterConfigurationTreeNode elementConverterConfigurationRootNode =
				convertWithOtherConverter(ElementConverterConfigurationTreeNode.class, transformationStepConfiguration);
	}

	/**
	 * @param elementConverter
	 * @return
	 */
	private boolean isIsResponsibleOverwritten(
			AbstractModelElementConverter<? extends Object, ? extends ModelElementI> elementConverter) {
		boolean isIsResponsibleOverwritten = false;
		Method method = null;

		try {
			method = elementConverter.getClass().getDeclaredMethod("isResponsibleFor", Object.class);
			isIsResponsibleOverwritten = true;
		} catch (SecurityException e) {
			// ignore this exception
		} catch (NoSuchMethodException e) {
			// ignore this exception
		}

		if (method == null) {
			try {
				method = elementConverter.getClass().getDeclaredMethod("isResponsibleFor", Boolean.TYPE);
				isIsResponsibleOverwritten = true;
			} catch (SecurityException e) {
				// ignore this exception
			} catch (NoSuchMethodException e) {
				// ignore this exception
			}
		}

		if (method == null) {
			try {
				method = elementConverter.getClass().getDeclaredMethod("isResponsibleFor", Boolean.TYPE, ModelElementI.class);
				isIsResponsibleOverwritten = true;
			} catch (SecurityException e) {
				// ignore this exception
			} catch (NoSuchMethodException e) {
				// ignore this exception
			}
		}
		return isIsResponsibleOverwritten;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(Model model, ModelElementI previouslyResultingElement) {
		T result;
		result = (T) new TransformationStepConfiguration(getModelConverter().getClass().getSimpleName(), getModelConverter().getConverterOptions());
		return result;
	}
}
