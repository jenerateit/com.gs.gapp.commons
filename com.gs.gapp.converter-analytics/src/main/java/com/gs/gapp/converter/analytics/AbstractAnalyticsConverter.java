package com.gs.gapp.converter.analytics;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.metamodel.analytics.AbstractAnalyticsConverterOptions;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * This model converter provides model element converters that have the sole purpose
 * of collecting information about the inner workings of generators. That information
 * can then be used by a special model converter and generation group to provide
 * a visualization of that inner workings. The visualization helps generator
 * developers to understand the generators' logic.
 * 
 * @author mmt
 *
 */
public abstract class AbstractAnalyticsConverter extends AbstractConverter {

	private AbstractAnalyticsConverterOptions converterOptions;
	
	public AbstractAnalyticsConverter(ModelElementCache modelElementCache) {
		super(modelElementCache);
	}

	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result =
				new ArrayList<>();

		if (ModelElement.isAnalyticsMode()) {
			/* TODO most probably we will never need those, remove if it is clear that this is the case (mmt 05-Jun-2013)
			result.add( new ModelToModelElementTreeNodeConverter<Model, ModelElementTreeNode>(this) );
			result.add( new ModelToModelElementTreeVisualizationXMLDocumentConverter<Model, ModelElementTreeVisualizationXMLDocument>(this) );
			result.add( new ModelToModelElementTreeVisualizationTextDocumentConverter<Model, ModelElementTreeVisualizationTextDocument>(this) );
*/

			result.add( new ModelToModelConverter<>(this) );
	        result.add( new ModelToModelConverterConfigurationConverter<>(this) );
	        result.add( new ModelConverterConfigurationToElementConverterConfigurationTreeNodeConverter<>(this) );
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onInitOptions()
	 */
	@Override
	protected void onInitOptions() {
		super.onInitOptions();
		this.converterOptions = new AbstractAnalyticsConverterOptions(getOptions());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#getConverterOptions()
	 */
	@Override
	public AbstractAnalyticsConverterOptions getConverterOptions() {
		return converterOptions;
	}
}
