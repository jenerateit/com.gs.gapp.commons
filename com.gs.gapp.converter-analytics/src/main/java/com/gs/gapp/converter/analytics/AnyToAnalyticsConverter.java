package com.gs.gapp.converter.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.metamodel.analytics.AnyToAnalyticsConverterOptions;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;


/**
 * @author mmt
 *
 */
public class AnyToAnalyticsConverter extends AbstractConverter {

	private AnyToAnalyticsConverterOptions converterOptions;
	/**
	 *
	 */
	public AnyToAnalyticsConverter() {
		super(new ModelElementCache());
	}
	
	

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onInitOptions()
	 */
	@Override
	protected void onInitOptions() {
		this.converterOptions = new AnyToAnalyticsConverterOptions(getOptions());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.BasicConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result =
			new ArrayList<>();

		result.add( new ModelToMxGraphJavaScriptConverter<>(this) );
		result.add( new ModelToModelElementTreeNodeConverter<>(this) );
		
		/* TODO most probably we do not need those, unless we want to generate a HTML report in addition to a DOT report (mmt 06-Jun-2013)
		result.add(new ModelToModelElementTreeVisualizationXMLDocumentConverter<Model, ModelElementTreeVisualizationXMLDocument>(this));
		result.add(new ModelToModelElementTreeVisualizationTextDocumentConverter<Model, ModelElementTreeVisualizationTextDocument>(this));
		*/

		return result;
	}

	@Override
	protected Set<ModelElementI> onPerformElementPredefinition(Collection<?> rawElements) {
		return super.onPerformElementPredefinition(rawElements);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#clientConvert(java.util.Collection, org.jenerateit.modelconverter.ModelConverterOptions)
	 */
	@Override
	protected Set<Object> clientConvert(Collection<?> rawElements,
			ModelConverterOptions options) throws ModelConverterException {
		return super.clientConvert(rawElements, options);
	}

	@Override
	public AnyToAnalyticsConverterOptions getConverterOptions() {
		return converterOptions;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#normalize(java.util.Collection)
	 */
	/* TODO normally, this should not be necessary, since from now on the previous converter's 'Model' instance is getting passed on
	@Override
	protected Set<Object> normalize(Collection<Object> rawElements) {
		Set<Object> result = super.normalize(rawElements);

		for (Object rawElement : rawElements) {
			if (rawElement instanceof ModelElement) {
				ModelElement modelElement = (ModelElement) rawElement;
				if (modelElement.getModel() != null) {
					Model model = modelElement.getModel();
					// we need to add the previous' converter's model object (singleton in the context of the converter) since this converter's model element converters depend on that element to be available
					result.add(model);
					break;
				}
			}
		}

		return result;
	}
	*/
}
