/**
 * 
 */
package com.gs.gapp.metamodel.test;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;

/**
 * @author mmt
 *
 */
public class TestModule extends Module {

	private static final long serialVersionUID = -2114162947088002424L;
	
	private final Set<Test> tests = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public TestModule(String name) {
		super(name);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#getNamespace()
	 */
	@Override
	public Namespace getNamespace() {
		return (Namespace) super.getNamespace();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#setNamespace(com.gs.gapp.metamodel.basic.Namespace)
	 */
	@Override
	public void setNamespace(com.gs.gapp.metamodel.basic.Namespace namespace) {
		if (!(namespace instanceof Namespace)) {
			throw new IllegalArgumentException("the namespace of a TestModule must be an instance of com.gs.gapp.metamodel.test.Namespace");
		}
		super.setNamespace(namespace);
	}

	public Set<Test> getTests() {
		return Collections.unmodifiableSet(tests);
	}
	
	public boolean addTest(Test tests) {
		return this.tests.add(tests);
	}
	
	/**
	 * @param uiStructuralContainer
	 * @return
	 */
	public Set<Test> getTestsThatInvolveView(UIStructuralContainer uiStructuralContainer) {
		Set<Test> result = new LinkedHashSet<>();
		
		for (Test test : getTests()) {
			if (test.isViewInvolved(uiStructuralContainer)) {
				result.add(test);
			}
		}
		
		return result;
	}
}
