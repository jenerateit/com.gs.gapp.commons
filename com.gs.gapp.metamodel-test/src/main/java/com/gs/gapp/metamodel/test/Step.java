package com.gs.gapp.metamodel.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.gs.gapp.metamodel.ActionTypeI;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;

/**
 * @author mmt
 *
 */
public class Step extends ModelElement {

	private static final long serialVersionUID = 4176028991377038698L;
	
	private final Test owner;
	
	private UIStructuralContainer pageContainer;
	
	private final List<Action> actions = new ArrayList<>();
	
	private Boolean openThePageContainer;
	
	public Step(String name, Test owner) {
		super(name);
		this.owner = owner;
		this.owner.addStep(this);
	}

	public Test getOwner() {
		return owner;
	}
	
	/**
	 * @param action
	 * @return
	 */
	public boolean addAction(Action action) {
		return actions.add(action);
	}
	
	/**
	 * @return an unmodifiable list of actions
	 */
	public List<Action> getActions() {
		return Collections.unmodifiableList(actions);
	}
	
	public UIStructuralContainer getPageContainer() {
		return pageContainer;
	}

	public void setPageContainer(UIStructuralContainer pageContainer) {
		this.pageContainer = pageContainer;
	}

	/**
	 * @return the openThePageContainer
	 */
	public Boolean getOpenThePageContainer() {
		return openThePageContainer;
	}

	/**
	 * @param openThePageContainer the openThePageContainer to set
	 */
	public void setOpenThePageContainer(Boolean openThePageContainer) {
		this.openThePageContainer = openThePageContainer;
	}

	/**
	 * @author mmt
	 *
	 */
	public static class Action extends ModelElement {

		private static final long serialVersionUID = 347372132212117024L;
		
		private final ModelElement target;
		
		private final ActionTypeI type;
		
	
		public Action(ActionTypeI type, ModelElement target) {
			super(type.getName());
			this.target = target;
			this.type = type;
		}

		public ModelElement getTarget() {
			return target;
		}

		public ActionTypeI getType() {
			return type;
		}
	}
}
