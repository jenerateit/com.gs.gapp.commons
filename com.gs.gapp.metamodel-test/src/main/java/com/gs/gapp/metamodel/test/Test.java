package com.gs.gapp.metamodel.test;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.test.Step.Action;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;

public class Test extends ComplexType {

	private static final long serialVersionUID = 4176028991377038698L;
	
	private final Set<Step> steps = new LinkedHashSet<>();
	
	private ComplexType preludeParameters;
	
	private final Set<UIStructuralContainer> preludeViews = new LinkedHashSet<>();
	
	public Test(String name) {
		super(name);
	}
	
	void addStep(Step step) {
		this.steps.add(step);
	}

	public Set<Step> getSteps() {
		return Collections.unmodifiableSet(steps);
	}
	
	public Step getInitialStep() {
		if (steps.size() > 0) {
			return steps.iterator().next();
		}
		
		return null;
	}
	
	public ComplexType getPreludeParameters() {
		return preludeParameters;
	}

	public void setPreludeParameters(ComplexType preludeParameters) {
		this.preludeParameters = preludeParameters;
	}

	
	/**
	 * 
	 * @return true in case the given test isn't abstract and has at least one step (possibly through its parent test)
	 */
	public boolean isTestExecutable() {
		boolean hasAParentWithSteps = false;
		if (this.getParent() instanceof Test) {
			Test parentTest = (Test) this.getParent();
			while (parentTest != null) {
				
				if (parentTest.getSteps().size() > 0) {
					hasAParentWithSteps = true;
					break;
				}
				
				if (parentTest.getParent() instanceof Test) {
					parentTest = (Test) parentTest.getParent();
				} else {
					parentTest = null;
				}
			}
		}
		
	    return !this.isAbstractType() && (this.getSteps().size() > 0 || hasAParentWithSteps);
	}

	public Set<UIStructuralContainer> getPreludeViews() {
		return preludeViews;
	}
	
	public boolean addPreludeView(UIStructuralContainer uiStructuralContainer) {
		return this.preludeViews.add(uiStructuralContainer);
	}
	
	/**
	 * @param uiStructuralContainer
	 * @return
	 */
	public boolean isViewInvolved(UIStructuralContainer uiStructuralContainer) {
		boolean result = false;
		
		if (preludeViews.contains(uiStructuralContainer)) {
			result = true;
		} else {
			for (Step step : steps) {
				if (step.getPageContainer() == uiStructuralContainer) {
					result = true;
					break;
				} else {
					for (Action action : step.getActions()) {
						if (action.getTarget() == uiStructuralContainer) {
							result = true;
							break;
						}
					}
				}
			}
		}
		
		if (result == false && getParent() instanceof Test) {
			Test parentTest = (Test) getParent();
			result = parentTest.isViewInvolved(uiStructuralContainer);
		}
		
		return result;
	}
}
