package com.gs.gapp.metamodel.test;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.AbstractModelFilter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelFilterI;
import com.gs.gapp.metamodel.product.Capability;
import com.gs.gapp.metamodel.product.UiApplication;
import com.gs.gapp.metamodel.ui.container.UIStructuralContainer;

/**
 * The ui application filter removes all model elements that are types of the Ui metamodel
 * in case those elements do not belong to {@link Capability#getUiApplications()}.
 * 
 * Note that if the passed collection of model elements does not contain any capability, no
 * filtering takes place whatsoever.
 * 
 * @author marcu
 *
 */
public class UiApplicationTestFilter extends AbstractModelFilter implements ModelFilterI {

	private final int uiApplicationIndex;
	
	public UiApplicationTestFilter() {
		this.uiApplicationIndex = -1;
	}
	
	public UiApplicationTestFilter(int uiApplicationIndex) {
		this.uiApplicationIndex = uiApplicationIndex;
	}

	@Override
	public Collection<?> filter(Collection<?> modelElements) {
		Collection<Capability> capabilities = getElementsForMetatype(Capability.class, modelElements);
		if (capabilities.size() == 0) return modelElements;  // capabilities are the key factor for filtering, so, when there are none => don't filter
		
		Collection<Object> result = new LinkedHashSet<>();
		Collection<?> mandatoryElements = AbstractModelFilter.extractMandatoryElements(modelElements);
		LinkedHashSet<UiApplication> uiApplications = new LinkedHashSet<>();
		for (Capability capability : capabilities) {
			int ii = 0;
			for (UiApplication uiApplication : capability.getUiApplications()) {
				if (getUiApplicationIndex() < 0 || getUiApplicationIndex() == ii) {
			        uiApplications.add(uiApplication);
				}
			    ii++;
			}
		}
		
		for (Object element : modelElements) {
			if (element instanceof ModelElementI) {
				ModelElementI modelElement = (ModelElementI) element;
				
				if (modelElement instanceof UiApplication) {
					UiApplication uiApplication = (UiApplication) modelElement;
					if (uiApplications.contains(uiApplication)) {
						result.add(uiApplication);
					}
				} else if (TestMetamodel.INSTANCE.isConversionChecked(modelElement.getClass())) {
					addTestModelElement(result, uiApplications, modelElement);
				} else {
					result.add(element);	
				}
				
			} else {
				result.add(element);
			}
		}
		
		result.addAll(mandatoryElements);
		return result;
	}
	
	/**
	 * @param result
	 * @param uiApplications
	 * @param modelElement
	 * @return
	 */
	private boolean addTestModelElement(Collection<Object> result, LinkedHashSet<UiApplication> uiApplications, ModelElementI modelElement) {
		Test test = null;
		TestModule testModule = null;
		
		if (modelElement instanceof Test) {
			test = (Test) modelElement;
		} else if (modelElement instanceof TestModule) {
			testModule = (TestModule) modelElement;
		}
		
		boolean included = false;
		for (UiApplication uiApplication : uiApplications) {
			if (testModule != null) {
				for (UIStructuralContainer uiStructuralContainer : uiApplication.getAllStructuralContainers()) {
					Set<Test> tests = testModule.getTestsThatInvolveView(uiStructuralContainer);
					if (tests.size() > 0) {
						included = true;
						break;
					}
				}
			}
			if (test != null) {
				for (UIStructuralContainer uiStructuralContainer : uiApplication.getAllStructuralContainers()) {
					if (test.isViewInvolved(uiStructuralContainer)) {
						included = true;
						break;
					}
				}
			}
            if (included) break;
		}
		
		if (included) {
			result.add(modelElement);
		}
		
		return included;
	}

	public int getUiApplicationIndex() {
		return uiApplicationIndex;
	}
}
