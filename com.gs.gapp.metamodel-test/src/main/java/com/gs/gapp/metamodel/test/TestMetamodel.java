package com.gs.gapp.metamodel.test;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;

import com.gs.gapp.metamodel.basic.MetamodelI;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;

/**
 * @author marcu
 *
 */
public enum TestMetamodel implements MetamodelI {

	INSTANCE,
	;
	
	private static final LinkedHashSet<Class<? extends ModelElementI>> metatypes = new LinkedHashSet<>();
	private static final Collection<Class<? extends ModelElementI>> collectionOfCheckedMetatypes = new LinkedHashSet<>();
	
	static {
		metatypes.add(Namespace.class);
		metatypes.add(Step.class);
		metatypes.add(Test.class);
		metatypes.add(TestModule.class);
		
		collectionOfCheckedMetatypes.add(Test.class);
		collectionOfCheckedMetatypes.add(TestModule.class);
		
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypes() {
		return Collections.unmodifiableCollection(metatypes);
	}

	@Override
	public boolean isIncluded(Class<? extends ModelElementI> metatype) {
		return metatypes.contains(metatype);
	}

	@Override
	public boolean isExtendingOneOfTheMetatypes(Class<? extends ModelElementI> metatype) {
		for (Class<? extends ModelElementI> metatypeOfMetamodel : metatypes) {
			if (metatypeOfMetamodel.isAssignableFrom(metatype)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypesForConversionCheck() {
		return collectionOfCheckedMetatypes;
	}
	
	/**
	 * @param metatype
	 * @return
	 */
	public Class<? extends Module> getModuleType(Class<? extends ModelElementI> metatype) {
		if (isIncluded(metatype)) {
		    return TestModule.class;
		}
		return null;
	}
}
