package com.gs.gapp.metamodel;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Assert;
import org.junit.Test;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * @author marcu
 *
 */
public class TestModelElementSorting {
	
	/**
	 * 
	 */
	@Test
	public void testSortByName() {
		ArrayList<ModelElement> modelElements = new ArrayList<>();
		ModelElement modelElement = new ModelElement("element3") {
			private static final long serialVersionUID = -8429774821237116518L;
		};
		modelElements.add(modelElement);
		
		modelElement = new ModelElement("element2") {
			private static final long serialVersionUID = -8429774821237116518L;
		};
		modelElements.add(modelElement);
		
		modelElement = new ModelElement("element1") {
			private static final long serialVersionUID = -8429774821237116518L;
		};
		modelElements.add(modelElement);
		
		modelElement = new ModelElement("Element1") {
			private static final long serialVersionUID = -8429774821237116518L;
		};
		modelElements.add(modelElement);
			
		Collections.sort(modelElements);
		
		Assert.assertTrue("collection of model elements is not of size 4", modelElements.size() == 4);
		Assert.assertTrue("first element in collection is not the one with name 'Element1'", "Element1".equals(modelElements.get(0).getName()));
	}
	
	/**
	 * 
	 */
	@Test
	public void testSortBySortValue() {
		ArrayList<ModelElement> modelElements = new ArrayList<>();
		ModelElement modelElement = new ModelElement("element3") {
			private static final long serialVersionUID = -8429774821237116518L;
		};
		modelElement.setSortValue(2);
		modelElements.add(modelElement);
		
		modelElement = new ModelElement("element2") {
			private static final long serialVersionUID = -8429774821237116518L;
		};
		modelElement.setSortValue(3);
		modelElements.add(modelElement);
		
		modelElement = new ModelElement("element1") {
			private static final long serialVersionUID = -8429774821237116518L;
		};
		modelElement.setSortValue(1);
		modelElements.add(modelElement);
			
		Collections.sort(modelElements);
		
		Assert.assertTrue("collection of model elements is not of size 3", modelElements.size() == 3);
		Assert.assertTrue("last element in collection is not the one with name 'element2' (which has sort value set to 3)", "element2".equals(modelElements.get(2).getName()));
		
	}

	/**
	 * 
	 */
	@Test
	public void testSortBySortValueAndName() {
		ArrayList<ModelElement> modelElements = new ArrayList<>();
		ModelElement modelElement = new ModelElement("element3") {
			private static final long serialVersionUID = -8429774821237116518L;
		};
		modelElement.setSortValue(1);
		modelElements.add(modelElement);
		
		modelElement = new ModelElement("element2") {
			private static final long serialVersionUID = -8429774821237116518L;
		};
		modelElement.setSortValue(2);
		modelElements.add(modelElement);
		
		modelElement = new ModelElement("element1") {
			private static final long serialVersionUID = -8429774821237116518L;
		};
		modelElement.setSortValue(3);
		modelElements.add(modelElement);
		
		modelElement = new ModelElement("y") {
			private static final long serialVersionUID = -8429774821237116518L;
		};
		modelElements.add(modelElement);
		
		modelElement = new ModelElement("x") {
			private static final long serialVersionUID = -8429774821237116518L;
		};
		modelElements.add(modelElement);
			
		Collections.sort(modelElements);
		
		Assert.assertTrue("collection of model elements is not of size 5", modelElements.size() == 5);
		Assert.assertTrue("first element in collection is not the one with name 'x' (which has no sort value set)", "x".equals(modelElements.get(0).getName()));
		Assert.assertTrue("last element in collection is not the one with name 'element1' (which has sort value set to 3)", "element1".equals(modelElements.get(4).getName()));
		
	}

}
