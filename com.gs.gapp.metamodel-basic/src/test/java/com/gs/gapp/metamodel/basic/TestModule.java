package com.gs.gapp.metamodel.basic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestModule {
	
	private static final String MODULE_NAME = "Module";
	private static final String NAMESPACE = "com.gs.gapp";
	
	private Module module;
	private Namespace namespace;

	@Before
	public void setUp() throws Exception {
		this.module = new Module(MODULE_NAME);
		this.namespace = new Namespace(NAMESPACE);
		this.namespace.setModule(module);
		this.module.setNamespace(namespace);
	}

	@After
	public void tearDown() throws Exception {
		this.module = null;
		this.namespace = null;
	}

	@Test
	public void testQualifiedName() {
		String qualifiedName = this.module.getQualifiedName();
		assertNotNull("no qualified module name", qualifiedName);
		assertEquals("qualified module name is not as expected", qualifiedName, TestModule.NAMESPACE + "." + TestModule.MODULE_NAME);
	}
}
