package com.gs.gapp.metamodel.basic;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.LinkedHashSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class TestModel {
	
	private abstract static class AbstractMetatype extends ModelElement {

		private static final long serialVersionUID = 4944199977932399984L;

		public AbstractMetatype(String name) {
			super(name);
		}
	}
	
	private static class Metatype1 extends AbstractMetatype {

		private static final long serialVersionUID = 4944199977932399984L;

		public Metatype1(String name) {
			super(name);
		}
	}
	
	private static class Metatype2 extends AbstractMetatype {

		private static final long serialVersionUID = 4944199977932399984L;

		public Metatype2(String name) {
			super(name);
		}
	}
	
	private static class Metatype3 extends AbstractMetatype {

		private static final long serialVersionUID = 4944199977932399984L;

		public Metatype3(String name) {
			super(name);
		}
	}
	
	private Model model;
	private Model childModel;
	private Model anotherChildModel;
	
	private Model model1;
    private Model model11;
    private Model model12;
    private Model model111;
    private Model model112;
    private Model model121;
    private Model model122;
	
    private Metatype1 elementOfType1;
    private Metatype2 elementOfType2;
    private Metatype3 elementOfType3;
	

	@Before
	public void setUp() throws Exception {
		this.model = new Model("Model1");
		this.childModel = new Model("Model2");
		this.childModel.setOriginatingElement(this.model);
		this.anotherChildModel = new Model("Model3");
		this.anotherChildModel.setOriginatingElement(this.childModel);
		
		createModelTree();
		
	}

	private void createModelTree() {
		model1 = new Model("model1");
	    model11 = new Model("model11");
	    model12 = new Model("model12");
	    model111 = new Model("model111");
	    model112 = new Model("model112");
	    model121 = new Model("model121");
	    model122 = new Model("model122");
		
	    model11.setOriginatingElement(model1);
	    model12.setOriginatingElement(model1);
	    model111.setOriginatingElement(model11);
	    model112.setOriginatingElement(model11);
	    model121.setOriginatingElement(model12);
	    model122.setOriginatingElement(model12);
	    
	    this.elementOfType1 = new Metatype1("ElementOfType1");
	    model1.addElement(this.elementOfType1);
	    this.elementOfType2 = new Metatype2("ElementOfType2");
	    model11.addElement(this.elementOfType2);
	    this.elementOfType3 = new Metatype3("ElementOfType3");
	    model122.addElement(this.elementOfType3);
	}

	@After
	public void tearDown() throws Exception {
		this.model = null;
		this.childModel = null;
		this.anotherChildModel = null;
	}

	@Test
	public void testRootModel() {
		Model rootModel = this.anotherChildModel.getRootModel();
		assertNotNull("no root model found", rootModel);
		assertEquals("root model is not the expected model but a wrong one", this.model, rootModel);
	}
	
	/**
	 * @see <a href="https://github.com/shamanland/simple-string-obfuscator">Simple String Obfuscator</a> 
	 */
	@Test
	public void testHideStringFromDecompilation() {
		String string = new Object() {
		    int t;
		    @Override
			public String toString() {
		        byte[] buf = new byte[5];
		        t = 1220204165; buf[0] = (byte) (t >>> 4);
		        t = 1731395377; buf[1] = (byte) (t >>> 15);
		        t = -1241489993; buf[2] = (byte) (t >>> 23);
		        t = 56640078; buf[3] = (byte) (t >>> 19);
		        t = 350056403; buf[4] = (byte) (t >>> 8);
		        return new String(buf);
		    }
		}.toString();
		
		System.out.println(string);
	}
	
	@Test
	@Ignore("hashCode() in ModelElement needs to be fixed to make this test succeed")
	public void testEquals() {
		
		Model m1 = new Model("Model1");
		Model m2 = new Model("Model1");
		
		LinkedHashSet<Model> models = new LinkedHashSet<>();
		models.add(m1);
		models.add(m2);
		
		assertTrue("set doesn't contain only one element", models.size() == 1);
		
		
	}

	/**
	 * 
	 */
	@Test
	public void testGetModelElementsFromSubsequentModels() {
		LinkedHashSet<Metatype1> elementsType1 = this.model1.getElementsFromSubsequentModels(Metatype1.class);
	    assertTrue("more than one ore no element at all found for type " + Metatype1.class,
	    		elementsType1 != null && elementsType1.size() == 1 && elementsType1.iterator().next() == this.elementOfType1);
	    
	    LinkedHashSet<Metatype2> elementsType2 = this.model1.getElementsFromSubsequentModels(Metatype2.class);
	    assertTrue("more than one ore no element at all found for type " + Metatype2.class,
	    		elementsType2 != null && elementsType2.size() == 1 && elementsType2.iterator().next() == this.elementOfType2);
	    
	    LinkedHashSet<Metatype3> elementsType3 = this.model1.getElementsFromSubsequentModels(Metatype3.class);
	    assertTrue("more than one ore no element at all found for type " + Metatype3.class,
	    		elementsType3 != null && elementsType3.size() == 1 && elementsType3.iterator().next() == this.elementOfType3);
	    
	    Metatype3 elementType3 = this.model1.getElementFromSubsequentModels(Metatype3.class);
	    assertTrue("could not find a model element of type " + Metatype3.class + " from subsequent models", elementType3 == this.elementOfType3);
	    
		
	    elementsType1 = this.model11.getElementsFromSubsequentModels(Metatype1.class);
	    assertTrue("shouldn't have found any element for type " + Metatype1.class + " in model11 but found " + elementsType1.size(),
	    		elementsType1 != null && elementsType1.size() == 0);
	    
	    LinkedHashSet<AbstractMetatype> elementsAbstractMetatype = this.model1.getElementsFromSubsequentModels(AbstractMetatype.class);
	    assertTrue("less or more than 3 elements found for metatype " + AbstractMetatype.class + " in model1:" + elementsAbstractMetatype.size(),
	    		elementsAbstractMetatype != null && elementsAbstractMetatype.size() == 3);
	    
	    Metatype1 elementType1 = this.model112.getElementFromPrecedingModels(Metatype1.class);
	    assertTrue("could not find a model element of type " + Metatype1.class + " from preceding models", elementType1 == this.elementOfType1);
	}
	
	/**
	 * 
	 */
	@Test
	public void testGetModelElementsFromPrecedingModels() {
		LinkedHashSet<AbstractMetatype> elementsAbstractMetatype = this.model112.getElementsFromPrecedingModels(AbstractMetatype.class);
		assertTrue("less or more than 2 elements found for metatype " + AbstractMetatype.class + " in model112:" + elementsAbstractMetatype.size(),
	    		elementsAbstractMetatype != null && elementsAbstractMetatype.size() == 2);
	}
}
