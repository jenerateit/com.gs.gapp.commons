package com.gs.gapp.metamodel.basic;

import java.util.stream.Stream;

import org.junit.Test;

import com.gs.gapp.metamodel.basic.BasicMetaModelUtil.NamingCaseRule;

public class TestNamingCaseRules {
	
	private static final String[] IDENTIFIERS = {
			"A name",
			"a B c D e F",
			"AaBbCcDdEe",
			"AaBbC cDdEe",
			"AaBbC.cDdEe",
			"AaBbC-cDdEe",
			"AaBbC_cDdEe",
			"AaBbCDdEe",
			"AA BB CC DD",
			"option.leading-lines",
			"Black & White",
			"   Test mit führenden Leerzeichen   ",
			"...",
			"___",
			"___ ___ ___",
			"+1",
			"a.b",
			"aaa.bbb.ccc",
			"ThisIsCamelCase",
			"this_is_snake_case",
			"this-is-kebap-case",
			"THISISUPPERCASE",
			"thisislowercase"
	};
	
	private void normalize(NamingCaseRule rule) {
		System.out.println();
		System.out.println("### " + rule + " ###");
		Stream.of(IDENTIFIERS).forEach(identifier -> {
			String normalizedIdentifier = rule.normalize(identifier);
			System.out.println(identifier +  " ===> " + normalizedIdentifier);
		});
	}
	
	@Test
	public void testUpper() {
		final NamingCaseRule rule = BasicMetaModelUtil.NamingCaseRule.UPPER;
		normalize(rule);
	}
	
	@Test
	public void testLower() {
		final NamingCaseRule rule = BasicMetaModelUtil.NamingCaseRule.LOWER;
		normalize(rule);
	}
	
	@Test
	public void testCamel() {
		final NamingCaseRule rule = BasicMetaModelUtil.NamingCaseRule.CAMEL;
		normalize(rule);
	}
	
	@Test
	public void testKebap() {
		final NamingCaseRule rule = BasicMetaModelUtil.NamingCaseRule.KEBAP;
		normalize(rule);
	}
	
	@Test
	public void testSnake() {
		final NamingCaseRule rule = BasicMetaModelUtil.NamingCaseRule.SNAKE;
		normalize(rule);
	}
	
	@Test
	public void testPascal() {
		final NamingCaseRule rule = BasicMetaModelUtil.NamingCaseRule.PASCAL;
		normalize(rule);
	}
}
