package com.gs.gapp.metamodel.basic;

import org.junit.Assert;
import org.junit.Test;

public class MetaModelITechnicalNameTest {
	
	private static class TestModel extends ModelElement implements ModelElementI {

		private static final long serialVersionUID = 1L;

		public TestModel(String name) {
			super(name);
		}
		
	}
	
	@Test
	public void testTechnicalName() {
		ModelElement a = new TestModel("foo");
		Assert.assertEquals("foo", a.getTechnicalName());
		ModelElement b = new TestModel("foo bar");
		Assert.assertEquals("foobar", b.getTechnicalName());
		ModelElement c = new TestModel("Foo Bar");
		Assert.assertEquals("FooBar", c.getTechnicalName());
		ModelElement d = new TestModel("$!%&/O");
		Assert.assertEquals("O", d.getTechnicalName());
		ModelElement e = new TestModel("Foo-Bar");
		Assert.assertEquals("Foo-Bar", e.getTechnicalName());
		ModelElement f = new TestModel("Foo_Bar");
		Assert.assertEquals("Foo_Bar", f.getTechnicalName());
		ModelElement g = new TestModel("_Foo-Bar_");
		Assert.assertEquals("_Foo-Bar_", g.getTechnicalName());
		ModelElement h = new TestModel("Foo - Bar");
		Assert.assertEquals("Foo-Bar", h.getTechnicalName());
		ModelElement i = new TestModel("Foo _ Bar");
		Assert.assertEquals("Foo_Bar", i.getTechnicalName());
	}
}
