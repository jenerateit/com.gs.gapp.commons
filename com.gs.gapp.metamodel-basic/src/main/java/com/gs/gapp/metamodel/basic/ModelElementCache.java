package com.gs.gapp.metamodel.basic;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.jenerateit.util.StringTools;

/**
 *
 */
public class ModelElementCache {

    private final Map<String,Set<ModelElementI>> nameMap = new LinkedHashMap<>();
    private final Map<String,Set<ModelElementI>> namespaceMap = new LinkedHashMap<>();
    private final Map<String,ModelElementI> qualifiedNameMap = new LinkedHashMap<>();

    private final Set<ModelElementI> allElements = new LinkedHashSet<>();

	/**
	 * 
	 */
	public ModelElementCache() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#clear()
	 */
	public void clear() {
		nameMap.clear();
		namespaceMap.clear();
		qualifiedNameMap.clear();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#findModelElement(java.lang.String)
	 */
	public ModelElementI findModelElement(String name) {
		String namespace = null;
		if (isQualifiedName(name)) {
			namespace = name.substring(0, name.lastIndexOf("."));
			name = name.substring(name.lastIndexOf(".")+1);
		}
		return findModelElement(name, namespace);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#findModelElement(java.lang.String, java.lang.String)
	 */
	public ModelElementI findModelElement(String name, String namespace) {
		return findModelElement(name, namespace, (Class<?>)null);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#findModelElement(java.lang.String, java.lang.Class)
	 */
	public ModelElementI findModelElement(String name, Class<?> typeof) {
		String namespace = null;
		if (isQualifiedName(name)) {
			namespace = name.substring(0, name.lastIndexOf("."));
			name = name.substring(name.lastIndexOf(".")+1);
		}
		return findModelElement(name, namespace, typeof);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#findModelElementByQualifiedName(java.lang.String)
	 */
	public ModelElementI findModelElementByQualifiedName(String qualifiedName) {
        return findModelElement(null, null, qualifiedName, (Class<?>)null);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#findModelElementByQualifiedName(java.lang.String, java.lang.Class)
	 */
	public ModelElementI findModelElementByQualifiedName(String qualifiedName,
			Class<?> typeof) {
        return findModelElement(null, null, qualifiedName, typeof);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#findModelElement(java.lang.String, java.lang.String, java.lang.Class)
	 */
	public ModelElementI findModelElement(String name, String namespace,
			Class<?> typeof) {
		return findModelElement(name, namespace, (String)null, typeof);
	}

	/**
	 * @param name
	 * @param namespace
	 * @param qualifiedName
	 * @param typeof
	 * @return
	 */
	private ModelElementI findModelElement(String name, String namespace, String qualifiedName,
			Class<?> typeof) {
		Set<ModelElementI> foundModelElements = findModelElements(name, namespace, qualifiedName, typeof);
		if (foundModelElements == null || foundModelElements.size() == 0) {
			return null;
		} else if (foundModelElements.size() == 1) {
			return foundModelElements.iterator().next();
		} else {
			throw new RuntimeException("found more than one model element with name " + name + ", namespace " + namespace + ", qualified name " + qualifiedName + " , typeof " + typeof);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#findModelElements(java.lang.String)
	 */
	public Set<ModelElementI> findModelElements(String name) {
		return findModelElements(name, (String)null, (String)null, (Class<?>)null);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#findModelElements(java.lang.String, java.lang.Class)
	 */
	public Set<ModelElementI> findModelElements(String name, Class<?> typeof) {
		return findModelElements(name, (String)null, (String)null, typeof);
	}

	/**
	 * @param name
	 * @param namespace
	 * @param qualifiedName
	 * @param typeof
	 * @return
	 */
	private Set<ModelElementI> findModelElements(String name, String namespace, String qualifiedName, Class<?> typeof) {

		if (StringTools.isEmpty(name) && StringTools.isEmpty(namespace) && StringTools.isEmpty(qualifiedName)) {
			// no valid lookup criteria specified
			throw new RuntimeException("no valid criteria given for name, namespace and/or qualified name, name = '" + name + "', namespace = '" + namespace + "', qualified name = '" + qualifiedName + "', typeof = '" + typeof + "'");
		}

		String finalQualifiedName = null;
		if (isQualifiedName(name) && StringTools.isEmpty(namespace) && StringTools.isEmpty(qualifiedName)) {
			qualifiedName = name;
			namespace = name.substring(0, name.lastIndexOf("."));
			name = name.substring(name.lastIndexOf(".")+1);
		}

		if (StringTools.isEmpty(qualifiedName) && !StringTools.isEmpty(name) && !StringTools.isEmpty(namespace)) {
			finalQualifiedName = new StringBuilder(namespace).append(".").append(name).toString();
		} else if (StringTools.isNotEmpty(qualifiedName)) {
			finalQualifiedName = qualifiedName;
		}

		ModelElementI modelElement = null;
		Set<ModelElementI> result = new LinkedHashSet<>();

		// --- check for the qualified name first
		if (StringTools.isNotEmpty(finalQualifiedName)) {
			modelElement = qualifiedNameMap.get(finalQualifiedName);
			if (modelElement != null &&
				(typeof == null || typeof.isAssignableFrom(modelElement.getClass())) /*&&
				(StringTools.isEmpty(name) || name.equals(modelElement.getName()))*/ ) {
				result.add(modelElement);
			}
		} else {
			// qualified name was not given -> check for the simple name then
		    if (StringTools.isNotEmpty(name) && nameMap.containsKey(name)) {
				Set<ModelElementI> elementsWithName = nameMap.get(name);
				if (typeof != null) {
					for (ModelElementI tempModelElement : elementsWithName) {
						if (typeof.isAssignableFrom(tempModelElement.getClass())) {
							result.add(tempModelElement);
						}
					}
				} else {
					result.addAll(elementsWithName);
				}
		    } else if (StringTools.isNotEmpty(namespace) && namespaceMap.containsKey(namespace)) {
				Set<ModelElementI> elementsWithNamespace = namespaceMap.get(namespace);
				if (typeof != null) {
					for (ModelElementI tempModelElement : elementsWithNamespace) {
						if (typeof.isAssignableFrom(tempModelElement.getClass())) {
							result.add(tempModelElement);
						}
					}
				} else {
					result.addAll(elementsWithNamespace);
				}
			}
		}

		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#add(com.gs.gapp.metamodel.basic.ModelElement)
	 */
	public void add(ModelElementI... modelElements) {
		if (modelElements != null) {
			for (ModelElementI modelElement : modelElements) {
				add(modelElement);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#add(com.gs.gapp.metamodel.basic.ModelElement)
	 */
	public void add(ModelElementI modelElement) {
		add(modelElement, null); // TODO ModeleElement does not have the notion of namespace anymore. Do we need something similar here?
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#add(com.gs.gapp.metamodel.basic.ModelElement, java.lang.String)
	 */
	public void add(ModelElementI modelElement, String namespace) {
		add(modelElement, namespace, null);
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#addByQualifiedName(com.gs.gapp.metamodel.basic.ModelElement, java.lang.String)
	 */
	public void addByQualifiedName(ModelElementI modelElement, String qualifiedName) {
		if (StringTools.isEmpty(qualifiedName)) throw new IllegalArgumentException("qualified name must neither be null nor empty");

		String namespace = qualifiedName.indexOf(".") > 0 ? qualifiedName.substring(0, qualifiedName.lastIndexOf(".")) : null;
		add(modelElement, namespace, qualifiedName);
	}


	/**
	 * @param modelElement
	 * @param namespace
	 * @param qualifiedName
	 */
	private void add(final ModelElementI modelElement, final String namespace, final String qualifiedName) {
		if (modelElement == null) throw new NullPointerException("model element must not be null");
		if (StringTools.isEmpty(modelElement.getName())) throw new IllegalArgumentException("model element must have a non-null and not empty name");

		String finalQualifiedName = null;
		String name = modelElement.getName();
		String finalNamespace = null;

		// TODO is modelElement.getName() == name ? If not, add this model Element with name and modelElement.getName() ?????

		if (StringTools.isEmpty(qualifiedName) && !StringTools.isEmpty(namespace)) {
			finalQualifiedName = new StringBuilder(namespace).append(".").append(modelElement.getName()).toString();
		} else if (StringTools.isNotEmpty(qualifiedName)) {
			finalQualifiedName = qualifiedName;
		}

		if (!StringTools.isEmpty(finalQualifiedName) && StringTools.isEmpty(namespace) && finalQualifiedName.indexOf(".") > 0) {
			finalNamespace = finalQualifiedName.substring(0, finalQualifiedName.lastIndexOf("."));
		} else if (StringTools.isNotEmpty(namespace)) {
			finalNamespace = namespace;
		}



		if (StringTools.isNotEmpty(finalQualifiedName)) {
			ModelElementI previousModelElement = qualifiedNameMap.put(finalQualifiedName, modelElement);
            if (previousModelElement != null && previousModelElement != modelElement) {
            	throw new RuntimeException("a different model element " + previousModelElement + " was already present on qualified name map for qualified name " + finalQualifiedName);
            }
		}

		if (StringTools.isNotEmpty(finalNamespace)) {
			if (!namespaceMap.containsKey(finalNamespace)) {
				namespaceMap.put(finalNamespace, new LinkedHashSet<ModelElementI>());
			}

			// the following if-block is for debugging/investigation only
			if (namespaceMap.get(finalNamespace).contains(modelElement)) {
				for (ModelElementI existingElement : namespaceMap.get(finalNamespace)) {
					if (existingElement.equals(modelElement)) {
//						System.out.println("existing element:" + existingElement);
//						System.out.println("element to be added:" + modelElement);
						break;
					}
				}
			}

			if (!namespaceMap.get(finalNamespace).add(modelElement)) {
				throw new RuntimeException("model element " + modelElement + " was already present on namespace map for namespace " + finalNamespace);
			}
		}


		if (!nameMap.containsKey(name)) {
			nameMap.put(name, new LinkedHashSet<ModelElementI>());
		}
		if (!nameMap.get(name).add(modelElement)) {
			throw new RuntimeException("model element " + modelElement + " was already present on name map for name " + name);
		}

		// --- finally add the model element to the set that simply holds all elements that were ever added
		this.allElements.add(modelElement);
		
//		System.out.println("# of elements in model cache:" + this.allElements.size());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#getAllModelElements()
	 */
	public Set<ModelElementI> getAllModelElements() {
		return Collections.unmodifiableSet(this.allElements);
	}


	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementCacheI#findModelElementsByNamespace(java.lang.String)
	 */
	public Set<ModelElementI> findModelElementsByNamespace(String namespace) {
        return this.findModelElements((String)null, namespace, (String)null, (Class<?>)null);
	}

	/**
	 * @param name
	 * @return
	 */
	public boolean isQualifiedName(String name) {
		if (name == null) {
			return false;
		} else {
		    return (name.indexOf(".") > 0);
		}
	}

	/**
	 * @param qualifiedName
	 * @return
	 */
	public String getName(String qualifiedName) {
		if (StringTools.isEmpty(qualifiedName)) throw new IllegalArgumentException("qualified name must neither be null nor empty");
		if (isQualifiedName(qualifiedName)) {
		    return qualifiedName.substring(qualifiedName.lastIndexOf(".")+1);
		} else {
			return qualifiedName;
		}
	}

	/**
	 * @param qualifiedName
	 * @return
	 */
	public String getNamespace(String qualifiedName) {
		if (StringTools.isEmpty(qualifiedName)) throw new IllegalArgumentException("qualified name must neither be null nor empty");
		if (isQualifiedName(qualifiedName)) {
		    return qualifiedName.substring(0, qualifiedName.lastIndexOf("."));
		} else {
			return "";
		}
	}
}
