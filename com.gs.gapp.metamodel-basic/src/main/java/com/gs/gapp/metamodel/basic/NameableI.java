package com.gs.gapp.metamodel.basic;

/**
 * This interface is going to be used wherever it is sufficient to get the name of an object.
 * 
 * <p>Typically, this interface is going to be used for simple helper classes
 * that are going to be used during model conversion in a transformation step,
 * where it would be overkill to let a class extend {@link ModelElement}
 * or implemenet {@link ModelElementI} or {@link ModelElementTraceabilityI}.
 * 
 */
public interface NameableI {
   
    /**
     * @return the name of an object
     */
    String getName();
}
