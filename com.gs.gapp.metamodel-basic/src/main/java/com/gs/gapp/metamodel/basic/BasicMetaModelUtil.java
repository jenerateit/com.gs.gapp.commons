package com.gs.gapp.metamodel.basic;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class BasicMetaModelUtil {

	public static String normalizeFileName(String name) {
		return name.replaceAll("[\\\\/:*?\"<>|]", "");
	}
	
	public static String normalizeFileNameNoSpaces(String name) {
		return normalizeFileNameNoSpaces(name, "_");
	}
	
	public static String normalizeFileNameNoSpaces(String name, String character) {
		return normalizeFileName(name).replace(" ", character);
	}
	
	public static String normalizePathNoSpaces(String path, String character) {
		if (path == null || path.isEmpty()) {
			return path;
		}
		String[] paths = path.split("\\/");
		return Arrays.stream(paths)
				.map(p -> normalizeFileNameNoSpaces(p, character))
				.collect(Collectors.joining("/"));
		
	}
	
	public enum NamingCaseRule {
		CAMEL ("Camel"),
		PASCAL ("Pascal"),
		SNAKE ("Snake"),
		KEBAP ("Kebap"),
		UPPER ("Upper"),
		LOWER ("Lower"),
		;
		
		private static final char[] WORD_DELIMITERS = {'.', '-', '_'};
		
        private static final Map<String, NamingCaseRule> stringToEnum = new HashMap<>();
		
		static {
			for (NamingCaseRule m : values()) {
				stringToEnum.put(m.getName().toLowerCase(), m);
			}
		}
		
		/**
	     * Converts an array of delimiters to a hash set of code points. Code point of space(32) is added
	     * as the default value. The generated hash set provides O(1) lookup time.
	     *
	     * @param delimiters  set of characters to determine capitalization, null means whitespace
	     * @return Set<Integer>
	     */
	    private static Set<Integer> toDelimiterSet(final char... delimiters) {
	        final Set<Integer> delimiterHashSet = new HashSet<>();
	        delimiterHashSet.add(Character.codePointAt(new char[]{' '}, 0));
	        if (delimiters == null || delimiters.length == 0) {
	            return delimiterHashSet;
	        }

	        for (int index = 0; index < delimiters.length; index++) {
	            delimiterHashSet.add(Character.codePointAt(delimiters, index));
	        }
	        return delimiterHashSet;
	    }
		
		/**
		 * @param name
		 * @return
		 */
		public static NamingCaseRule fromString(String name) {
			if (name != null) {
			    NamingCaseRule result = stringToEnum.get(name.toLowerCase());
			    if (result == null) {
			    	throw new IllegalArgumentException("no enum entry found for string '" + name + "'");
			    }
			    return result;
			}
			return null;
		}
		
		public static String camelToSnake(String identifier) {
			if (identifier != null) {
		        String regex = "([a-z])([A-Z]+)";
		        String replacement = "$1_$2";
		 
		        // Replace the given regex
		        // with replacement string
		        // and convert it to lower case.
		        identifier = identifier.replaceAll(regex, replacement).toLowerCase();
		 
		        return identifier;
			}
			return null;
	    }
		
		public static String toCamel(String identifier, final boolean capitalizeFirstLetter, final char... delimiters) {
	        if (identifier.isEmpty()) {
	            return identifier;
	        }
	        final int strLen = identifier.length();
	        final int[] newCodePoints = new int[strLen];
	        int outOffset = 0;
	        final Set<Integer> delimiterSet = toDelimiterSet(delimiters);
	        boolean capitalizeNext = capitalizeFirstLetter;
	        for (int index = 0; index < strLen;) {
	            final int codePoint = identifier.codePointAt(index);

	            if (delimiterSet.contains(codePoint)) {
	                capitalizeNext = outOffset != 0;
	                index += Character.charCount(codePoint);
	            } else if (capitalizeNext || outOffset == 0 && capitalizeFirstLetter) {
	                final int titleCaseCodePoint = Character.toTitleCase(codePoint);
	                newCodePoints[outOffset++] = titleCaseCodePoint;
	                index += Character.charCount(titleCaseCodePoint);
	                capitalizeNext = false;
	            } else {
	                newCodePoints[outOffset++] = codePoint;
	                index += Character.charCount(codePoint);
	            }
	        }

	        return new String(newCodePoints, 0, outOffset);
	    }
		
		private final String name;
		
		/**
		 * @param name
		 */
		private NamingCaseRule(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
		
		public String normalize(String identifier) {
			if (identifier != null) {
				String prelimResult = null;
				switch (this) {
				case CAMEL:
					return toCamel(identifier, false, WORD_DELIMITERS);
				case KEBAP:
					prelimResult = toCamel(identifier, false, WORD_DELIMITERS);
					prelimResult = camelToSnake(prelimResult);
					return prelimResult.replace('_', '-');
				case LOWER:
					return identifier.toLowerCase();
				case PASCAL:
					return toCamel(identifier, true, WORD_DELIMITERS);
				case SNAKE:
					prelimResult = toCamel(identifier, false, WORD_DELIMITERS);
					return camelToSnake(prelimResult);
				case UPPER:
					return identifier.toUpperCase();
				default:
					break;
				}
			}
			
			return null;
		}
	}
}
