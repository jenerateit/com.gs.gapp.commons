package com.gs.gapp.metamodel.basic.options;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.metamodel.basic.GeneratorInfo;
import com.gs.gapp.metamodel.basic.ModelFilterI;
import com.gs.gapp.metamodel.basic.ModelValidatorI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.RuleI;

/**
 * @author mmt
 *
 */
public class ConverterOptions extends AbstractOptions {
	
	/**
	 * Elements that do not have a namespace that stems from the model but nonetheless require
	 * a namespace are supposed to use the default namespace that has been configured as
	 * a converter parameter.
	 */
	public static final String PARAMETER_DEFAULT_NAMESPACE = "default-namespace";

	/**
	 * Comma-separated list of names of modules that should _not_ be generated (generated = false).
	 * Wildcards are allowed and naming is case-insensitive.
	 */
	public static final String PARAMETER_MODULES_NOT_FOR_GENERATION = "modules-not-for-generation";
	
	/**
	 * Comma-separated list of names of modules that _should_ be generated (generated = true).
	 * Wildcards are allowed and naming is case-insensitive.
	 */
	public static final String PARAMETER_MODULES_FOR_GENERATION = "modules-for-generation";
	
	/**
	 * Provides some validation rules to check the number of occurrencies of certain model element types.
	 * format: [simple type name]=[min occurrence],[max occurrence];[simple type name]=[min occurrence],[max occurrence]; ...
	 */
	public static final String PARAMETER_VALIDATION_OCCURRENCE = "validation.occurrence";
	
	/**
	 * Provides some filter rules to stop some model elements of a certain type to go through.
	 * The model element name (or a regular expression) is applied to do the exclusion.
	 * 
	 * format: [simple type name]=[element name or regexp];[simple type name]=[element name or regexp]; ...
	 */
	public static final String PARAMETER_FILTER_BY_NAME_AND_TYPE = "filter.by-name-and-type";
	
	/**
	 * Provides the Bundle-SymbolicName and the Bundle-Version of a generator (genset) as it is found in the manifest of a genset bundle.
	 * @deprecated vd.generator.id  vd.generator.version is used and set by the gu service
	 */
	@Deprecated
	public static final String PARAMETER_VD_GENERATOR_INFO = "vd.generator.info";
	
	/**
	 * Provides info if the model is enhanced (default is true)
	 */
	public static final String PARAMETER_AGNOSTIC_ENHANCEMENT = "agnostic.enhancement";
	
	private final ModelConverterOptions options;

	/**
	 * @param options
	 */
	public ConverterOptions(ModelConverterOptions options) {
		super();
		if (options == null) throw new NullPointerException("parameter 'options' must not be null");
		this.options = options;
		
	}

	/**
	 * @return
	 */
	public ModelConverterOptions getOptions() {
		return options;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.AbstractOptions#getOptionValue(java.lang.String)
	 */
	@Override
	protected Serializable getOptionValue(String key) {
		return this.options.get(key);
	}

	/**
	 * Try to find the option in the list of options. 
	 * If not found the default value value will be used.
	 * 
	 * @param key the key of the option to look for
	 * @return the option value if found or the default value
	 */
	protected Serializable getOptionValueOrDefault(final String key) {
		Serializable result = getOptionValue(key);
		if (result == null) {
			for (final OptionDefinition<?> od : getOptionDefinitions()) {
				if (key.compareTo(od.getKey()) == 0) {
					result = od.getDefaultValue();
					break;
				}
			}
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.AbstractOptions#getProvidedOptionKeys()
	 */
	@Override
	protected List<String> getProvidedOptionKeys() {
		return new ArrayList<>( options.keySet() );
	}

	/**
	 * @return
	 */
	public String getNameOfDefaultNamespace() {
		String defaultNamespace = (String) getOptions().get(PARAMETER_DEFAULT_NAMESPACE);
		return defaultNamespace == null ? "com.vd" : defaultNamespace;
	}
	
	/**
	 * @param moduleName
	 * @return
	 */
	public boolean isModuleToBeGenerated(String moduleName) {
	    boolean result = true;
	    
        // --- first handle the positive list of module names
	    String modulesForGeneration = (String) getOptions().get(PARAMETER_MODULES_FOR_GENERATION);
        if (modulesForGeneration != null && modulesForGeneration.trim().length() > 0) {
        	result = false;  // if there is a positive list, we change initial assumption to false, since only if the module name is in the list, it can be generated
        	String[] patterns = modulesForGeneration.split(",");
            if (patterns != null) {
            	for (String pattern : patterns) {
            		if (pattern == null) continue;
            		if (moduleName.toLowerCase().matches(pattern.trim())) {
            			result = true;
            			break;
            		}
            	}
            }
        }
        
        
        // --- then handle the negative list of module names
        String modulesNotForGeneration = (String) getOptions().get(PARAMETER_MODULES_NOT_FOR_GENERATION);
        if (result == true && modulesNotForGeneration != null && modulesNotForGeneration.trim().length() > 0) {
            String[] patterns = modulesNotForGeneration.trim().split(",");
            if (patterns != null) {
            	for (String pattern : patterns) {
            		if (pattern == null) continue;
            		if (moduleName.toLowerCase().matches(pattern.trim())) {
            			result = false;
            			break;
            		}
            	}
            }
        }

		return result;
	}

	/**
	 * @return
	 */
	public Collection<RuleI> getOccurrenceRules() {
		Collection<RuleI> rules = new LinkedHashSet<>();
		String validationExpression = (String) getOptions().get(PARAMETER_VALIDATION_OCCURRENCE);
		if (validationExpression != null && validationExpression.length() > 0) {
			Collection<RuleI> occurrenceRules = ModelValidatorI.RuleOccurrence.createRulesFromExpression(validationExpression);
			if (occurrenceRules != null) rules.addAll(occurrenceRules);
		}
		return rules;
	}
	
	/**
	 * @return
	 */
	public Collection<ModelFilterI.RuleI> getFilterByNameAndTypeRules() {
		Collection<ModelFilterI.RuleI> rules = new LinkedHashSet<>();
		String filterExpression = (String) getOptions().get(PARAMETER_FILTER_BY_NAME_AND_TYPE);
		if (filterExpression != null && filterExpression.length() > 0) {
			Collection<ModelFilterI.RuleI> occurrenceRules = ModelFilterI.RuleElementsByNameAndType.createRulesFromExpression(filterExpression);
			if (occurrenceRules != null) rules.addAll(occurrenceRules);
		}
		return rules;
	}
	
	/**
	 * @return
	 * @deprecated use AbstractConverter#getModel()#getGeneratorInfo()
	 */
	@Deprecated
	public GeneratorInfo getGeneratorInfo() {
		GeneratorInfo generatorInfo = null;
		
		String idAndVersion = (String) getOptions().get(PARAMETER_VD_GENERATOR_INFO);
		
		if (idAndVersion != null && idAndVersion.length() > 0) {
			String[] idAndVersionArray = idAndVersion.split(":");
			if (idAndVersionArray != null && idAndVersionArray.length == 2) {
				generatorInfo = new GeneratorInfo(idAndVersionArray[0], idAndVersionArray[1]);
			}
		}
		return generatorInfo;
	}


	/**
	 * Provides info if the model is enhanced (default is true)
	 * @return 
	 */
	public boolean isAgnosticEnhancement() {
		String enhancement = (String) getOptions().get(PARAMETER_AGNOSTIC_ENHANCEMENT);
		validateBooleanOption(enhancement, PARAMETER_AGNOSTIC_ENHANCEMENT);
		return enhancement == null || "true".equalsIgnoreCase(enhancement);
	}
}
