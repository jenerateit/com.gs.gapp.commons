/**
 * 
 */
package com.gs.gapp.metamodel.basic;

/**
 * @author mmt
 *
 */
public enum TemporalType {
	TIME,
	DATE,
	DATETIME,
	;

}
