/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel;


/**
 * @author hrr
 *
 */
public interface VisitorI<T> {

	/**
	 * Visit the given object.
	 * 
	 * @param e the object to visit
	 * @return true if the objects members should be visited; false they should be skipped
	 */
	VisitorState visit(T e);
}
