/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel;

/**
 * Results a {@link AcceptableI}'s {@link AcceptableI#accept(VisitorI)} method has.
 * 
 * @author hrr
 *
 */
public enum AcceptState {

	/**
	 * Go on with the next items.
	 */
	CONTINUE,
	
	/**
	 * Stop visiting and return.
	 */
	INTERRUPT
}
