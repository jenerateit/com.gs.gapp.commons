/**
 *
 */
package com.gs.gapp.metamodel.basic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.AcceptState;

/**
 * A "Model" is not being modeled, no matter which modeling tool is being used.
 * The purpose of a Model instance is to have a central model element from where _all_ model
 * elements can be reached by traversing the object net. An instance of Model is the root element
 * of all model elements that are created or passed through by a model converter.
 * A model instance can be used to trigger the generation of files where there must be exactly
 * one of them per target project (e.g. persistence.xml for JPA generation).
 *
 * @author mmt
 *
 */
public class Model extends ModelElement {

	/**
	 *
	 */
	private static final long serialVersionUID = -1318043288464383174L;

	public static final String MODEL = "model-initial";
	
	private final Set<ModelElementI> elements = new LinkedHashSet<>();
	
	private final Set<ModelElementI> selectedElements = new LinkedHashSet<>();

	private Namespace defaultNamespace;
	
	private GeneratorInfo generatorInfo;
	
	/**
	 * @param name
	 */
	public Model(String name) {
		super(name == null ? MODEL : name);
	}
	
	/**
	 * 
	 */
	public Model() {
		super(MODEL);
	}
	
	/**
	 * @return
	 */
	public Model getRootModel() {
		Model result = this;
		
		while (result.getOriginatingElement(Model.class) != null) {
			result = result.getOriginatingElement(Model.class);
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public GeneratorInfo getLeadingGeneratorInfo() {
        Model aModel = this;
		
		while (aModel != null) {
			if (aModel.getGeneratorInfo() != null) return aModel.getGeneratorInfo();
			aModel = aModel.getOriginatingElement(Model.class);
		}
		
		return null;
	}

	/**
	 * Add an element to this module.
	 *
	 * @param e the element to add
	 * @return true if this set did not already contain the specified element
	 */
	public boolean addElement(ModelElementI e) {
		if (e instanceof Model) {
			// not going to add models to models (mmt 10-Dec-2012)
			return false;
		}
		
		if (e instanceof ModelElement) {
			ModelElement modelElement = (ModelElement) e;
			if (modelElement.isInGlobalCache()) {
				return true;  // a model element that is meant to be in a global cache cannot be a member of a model at the same time since we do not have the concept of global models (mmt 27-Aug-2018)
			}
		}

		boolean add = this.elements.add(e);
		
		if (e.getModel() != this) {
		    e.setModel(this);
		}
		
		return add;
	}

	/**
	 * Getter for the elements in this module.
	 *
	 * @return the elements
	 */
	public Set<ModelElementI> getElements() {
		return elements;
	}
	
	/**
	 * @param resultType
	 * @return
	 */
	public final <T extends ModelElementI> Set<T> getElements(Class<T> resultType) {
		return getElements(resultType, false);
	}
	
	/**
	 * @return the selectedElements (never returns null)
	 */
	public Set<ModelElementI> getSelectedElements() {
		return selectedElements;
	}
	
	/**
	 * @param resultType
	 * @return the selected elements of a given type (never returns null)
	 */
	public final <T extends ModelElementI> Set<T> getSelectedElements(Class<T> resultType) {
		return getSelectedElements(resultType, false);
	}

	/**
	 * A convenience method to get all elements that are instances of the given type
	 * and are in the model's set of model elements.
	 *
	 * @param resultType
	 * @param exactMatch if true, only model elements with an exactly matching type are returned
	 * @return elements of a given type (never returns null)
	 */
	public final <T extends ModelElementI> Set<T> getElements(Class<T> resultType, boolean exactMatch) {
		Set<T> elements = new LinkedHashSet<>();

		for (ModelElementI element : getElements()) {
			if (exactMatch) {
				if (resultType.equals(element.getClass())) {
					elements.add(resultType.cast(element));
				}
			} else {
				if (resultType.isAssignableFrom(element.getClass())) {
					elements.add(resultType.cast(element));
				}
			}
		}

		return elements;
	}
	
	/**
	 * A convenience method to get all selected elements that are instances of the given type
	 * and are in the model's set of selected model elements.
	 *
	 * @param resultType
	 * @param exactMatch if true, only selected model elements with an exactly matching type are returned
	 * @return selected elements of a given type (never returns null)
	 */
	public final <T extends ModelElementI> Set<T> getSelectedElements(Class<T> resultType, boolean exactMatch) {
		Set<T> elements = new LinkedHashSet<>();

		for (ModelElementI element : getSelectedElements()) {
			if (exactMatch) {
				if (resultType.equals(element.getClass())) {
					elements.add(resultType.cast(element));
				}
			} else {
				if (resultType.isAssignableFrom(element.getClass())) {
					elements.add(resultType.cast(element));
				}
			}
		}

		return elements;
	}

	/**
	 * Collects all elements that are of type T by searching through all of the
	 * model's elements and all of the model's resulting models' elements (transitively).
	 * 
	 * @param resultType
	 * @return elements of a given type from subsequent models (never returns null)
	 */
	public final <T extends ModelElementI> LinkedHashSet<T> getElementsFromSubsequentModels(Class<T> resultType) {
		return getElementsFromSubsequentModels(resultType, false);
	}
	
	/**
	 * Collects all elements that are of type T by searching through all of the
	 * model's elements and all of the model's resulting models' elements (transitively).
	 * 
	 * @param resultType
	 * @param exactMatch
	 * @return elements of a given type from subsequent models (never returns null)
	 */
	public final <T extends ModelElementI> LinkedHashSet<T> getElementsFromSubsequentModels(Class<T> resultType, boolean exactMatch) {
		LinkedHashSet<T> elements = new LinkedHashSet<>();
		
		this.collectElementsFromSubsequentModels(resultType, elements, exactMatch);

		return elements;
	}
	
	/**
	 * @param resultType
	 * @return an element of a given type of a subsequent model, or null if there is none
	 */
	public final <T extends ModelElementI> T getElementFromSubsequentModels(Class<T> resultType) {
		return getElementFromSubsequentModels(resultType, false);
	}
	
	/**
	 * @param resultType
	 * @param exactMatch
	 * @return ean element of a given type of a subsequent model, or null if there is none
	 */
	public final <T extends ModelElementI> T getElementFromSubsequentModels(Class<T> resultType, boolean exactMatch) {
		LinkedHashSet<T> elements = getElementsFromSubsequentModels(resultType);
		
		if (elements.size() == 0) {
			return null;
		} else if (elements.size() == 1) {
		    return elements.iterator().next();
		} else {
			throw new ModelConverterException("found more than one model element in subsequent models of " + this + " (searching for type " + resultType + ")");
		}
	}
	
	/**
	 * @param resultType
	 * @param elements
	 * @param exactMatch
	 * @return
	 */
	private <T extends ModelElementI> Set<T> collectElementsFromSubsequentModels(Class<T> resultType, LinkedHashSet<T> elements, boolean exactMatch) {
		for (ModelElementI element : getElements()) {
			if (exactMatch) {
				if (resultType.equals(element.getClass())) {
					elements.add(resultType.cast(element));
				}
			} else {
				if (resultType.isAssignableFrom(element.getClass())) {
					elements.add(resultType.cast(element));
				}
			}
		}
		
		for (Model model : this.getResultingModelElements(Model.class)) {
			model.collectElementsFromSubsequentModels(resultType, elements, exactMatch);
		}
		
		return elements;
	}
	
	/**
	 * Collects all model elements that are of the passed type by looking into
	 * this model and all the models you bet by going back through the
	 * "getOriginatingElement(Model.class)" chain of calls.
	 *  
	 * @param resultType
	 * @return elements of a given type of preceding models (never returns null)
	 */
	public final <T extends ModelElementI> LinkedHashSet<T> getElementsFromPrecedingModels(Class<T> resultType) {
        return getElementsFromPrecedingModels(resultType, false);
	}
	
	/**
	 * Collects all model elements that are of the passed type by looking into
	 * this model and all the models you bet by going back through the
	 * "getOriginatingElement(Model.class)" chain of calls.
	 *  
	 * @param resultType
	 * @param exactMatch
	 * @return elements of a given type of preceding models (never returns null)
	 */
	public final <T extends ModelElementI> LinkedHashSet<T> getElementsFromPrecedingModels(Class<T> resultType, boolean exactMatch) {
        LinkedHashSet<T> elements = new LinkedHashSet<>();
        
        Model model = this;
        
        while (model != null) {
        	for (ModelElementI element : model.getElements()) {
        		if (exactMatch) {
        			if (resultType.equals(element.getClass())) {
	    				elements.add(resultType.cast(element));
	    			}
        		} else {
	    			if (resultType.isAssignableFrom(element.getClass())) {
	    				elements.add(resultType.cast(element));
	    			}
        		}
    		}
        	
        	model = model.getOriginatingElement(Model.class);
        }
		
		return elements;
	}
	
	/**
	 * @param resultType
	 * @return an element of a given type of a preceding model, null if there is none
	 */
	public final <T extends ModelElementI> T getElementFromPrecedingModels(Class<T> resultType) {
		return getElementFromPrecedingModels(resultType, false);
	}
	
	/**
	 * @param resultType
	 * @param exactMatch
	 * @return an element of a given type of a preceding model, null if there is none 
	 */
	public final <T extends ModelElementI> T getElementFromPrecedingModels(Class<T> resultType, boolean exactMatch) {
		LinkedHashSet<T> elements = getElementsFromPrecedingModels(resultType, exactMatch);
		
		if (elements.size() == 0) {
			return null;
		} else if (elements.size() == 1) {
		    return elements.iterator().next();
		} else {
			throw new ModelConverterException("found more than one model element in preceding models of " + this + " (searching for type " + resultType + ")");
		}
	}
	
	/**
	 * A convenience method to get all elements that are instances of the given type
	 * and are in the model's set of model elements. The collection of model elements
	 * are returned in their natural ordering.
	 *
	 * @param resultType
	 * @return a set of sorted elements of a given type (never returns null)
	 */
	public final <T extends ModelElementI> Set<T> getElementsSorted(Class<T> resultType) {
		ArrayList<T> elementsForSorting = new ArrayList<>();

		for (ModelElementI element : getElements()) {
			if (resultType.isAssignableFrom(element.getClass())) {
				elementsForSorting.add(resultType.cast(element));
				
			}
		}

		Collections.sort(elementsForSorting);
		Set<T> elements = new LinkedHashSet<>(elementsForSorting);
		
		return elements;
	}
	
	/**
	 * Finding a single element of the given type in the model's
	 * collection of elements. If there is more than one element
	 * found, the method throw a {@link ModelConverterException}.
	 * You should only call this method when you expect there to
	 * be at most one element of the given type.
	 * 
	 * @param resultType
	 * @return an element of a given type, null if there is none
	 */
	public final <T extends ModelElementI> T getElement(Class<T> resultType) {
        return getElement(resultType, false);
	}
	
	/**
	 * Finding a single element of the given type in the model's
	 * collection of elements. If there is more than one element
	 * found, the method throw a {@link ModelConverterException}.
	 * You should only call this method when you expect there to
	 * be at most one element of the given type.
	 * 
	 * @param resultType
	 * @param exactMatch
	 * @return an element of a given type, null if there is none
	 */
	public final <T extends ModelElementI> T getElement(Class<T> resultType, boolean exactMatch) {
		T result = null;

		Set<T> elementsOfType = getElements(resultType, exactMatch);
		
		if (elementsOfType == null || elementsOfType.size() == 0) {
			// no element found => return null
		} else if (elementsOfType.size() == 1) {
			result = elementsOfType.iterator().next();
		} else {
			throw new ModelConverterException("found more than one element of type '" + resultType + "' in model '" + this);
		}

		return result;
	}

	/**
	 * @param e
	 */
	public void removeElement(ModelElementI e) {
		if (this.elements.remove(e)) {
			e.setModel(null);
		}
	}


	/**
	 * Visit method to accept all elements.
	 *
	 * @param visitor the visitor
	 * @return the visitor result
	 * @see com.gs.gapp.metamodel.basic.ModelElement#visitChilds(com.gs.gapp.metamodel.basic.ModelElementVisitorI)
	 */
	@Override
	protected AcceptState visitChilds(ModelElementVisitorI visitor) {
		if (super.visitChilds(visitor) == AcceptState.CONTINUE) {
			for (ModelElementI e : this.elements) {
				if (e.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}
			return AcceptState.CONTINUE;
		}
		return AcceptState.INTERRUPT;
	}

	/**
	 * Compares the super class and the elements.
	 *
	 * @param element the element to compare to
	 * @return a negative integer, zero, or a positive integer as this object is less than, equal to,
	 * or greater than the specified object.
	 * @see ModelElement#compareTo(ModelElementI)
	 */
	@Override
	public int compareTo(ModelElementI element) {
		int comp = super.compareTo(element);
		if (comp == 0) {
			if (Model.class.isInstance(element)) {
				Model m = Model.class.cast(element);
				return this.elements.size() - m.elements.size();
			}
		}
		return comp;
	}

	/**
	 * @return the defaultNamespace
	 */
	public Namespace getDefaultNamespace() {
		return defaultNamespace;
	}

	/**
	 * @param defaultNamespace the defaultNamespace to set
	 */
	public void setDefaultNamespace(Namespace defaultNamespace) {
		this.defaultNamespace = defaultNamespace;
		if (defaultNamespace != null) {
			defaultNamespace.setModel(this);
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#clearResultingElements(java.lang.String)
	 */
	@Override
	public void clearResultingElements(String modelConverterId) {
		super.clearResultingElements(modelConverterId);
	}

	public GeneratorInfo getGeneratorInfo() {
		return generatorInfo;
	}

	public void setGeneratorInfo(GeneratorInfo generatorInfo) {
		this.generatorInfo = generatorInfo;
	}
}
