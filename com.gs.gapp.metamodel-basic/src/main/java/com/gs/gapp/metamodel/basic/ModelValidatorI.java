package com.gs.gapp.metamodel.basic;

import java.util.Collection;
import java.util.LinkedHashSet;

import javax.validation.constraints.NotNull;

import org.jenerateit.modelconverter.ModelConverterException;

/**
 * Classes that implement this interface have the purpose of
 * validating a given set of model elements.
 * 
 * @author marcu
 *
 */
public interface ModelValidatorI {

	/**
	 * Validate some model elements.
	 * 
	 * @param modelElements
	 * @return a collection of validation messages, if the given model is valid, an empty collection shall be returend
	 */
	@NotNull
	Collection<Message> validate(Collection<Object> modelElements);
	
	/**
	 * @param modelElements
	 * @param rules
	 * @return
	 */
	@NotNull
	default Collection<Message> validate(Collection<Object> modelElements, Collection<RuleI> rules) {
		Collection<Message> result = new LinkedHashSet<>();
		
		if (modelElements != null && rules != null) {
			for (RuleI rule : rules) {
				result.addAll(rule.validate(modelElements));
			}
		}
			
		return result;
	}
	
	/**
	 * @author marcu
	 *
	 */
	public static class Message {
		private final MessageStatus status;
		private final String message;
		private final Throwable th;
		
		/**
		 * @param status
		 * @param message
		 */
		public Message(MessageStatus status, String message) {
			super();
			this.status = status;
			this.message = message;
			this.th = null;
		}
		
		/**
		 * @param status
		 * @param message
		 * @param th
		 */
		public Message(MessageStatus status, String message, Throwable th) {
			super();
			if (status == MessageStatus.INFO && th != null) throw new ModelConverterException("An INFO message includes an exception (" + th.getMessage() + "), which is not allowed. An ERROR or WARNING message type has to be used instead.");
			
			this.status = status;
			this.message = message;
			this.th = th;
		}

		public MessageStatus getStatus() {
			return status;
		}

		public String getMessage() {
			return message;
		}

		public Throwable getException() {
			return th;
		}
	}
	
	/**
	 * @author marcu
	 *
	 */
	public static enum MessageStatus {
		ERROR, WARNING, INFO;
	}
	
	/**
	 * @author marcu
	 *
	 */
	public static interface RuleI {
		Collection<Message> validate(Collection<Object> modelElements);
	}
	
	/**
	 * @author marcu
	 *
	 */
	public static class RuleOccurrence implements RuleI {
		
		/**
		 * Creates a collection of rules by parsing an expression of this type:
		 * [simple type name]=[min occurrence],[max occurrence];[simple type name]=[min occurrence],[max occurrence]; ...
		 * 
		 * @param expression
		 * @return
		 */
		public static Collection<RuleI> createRulesFromExpression(String expression) {
			Collection<RuleI> result = new LinkedHashSet<>();
			
			if (expression != null && expression.length() > 0) {
				try {
					String[] rulesAsStrings = expression.split(";");
					for (String ruleAsString : rulesAsStrings) {
						int min = -1;
						int max = -1;
						String[] simpleTypeNameAndOccurrence = ruleAsString.split("=");
						String[] minAndMax = simpleTypeNameAndOccurrence[1].split(",");
						
						if (minAndMax[0].length() > 0) {
							min = Integer.parseInt(minAndMax[0]);
						}
						
						if (minAndMax.length == 2 && minAndMax[1].length() > 0) {
							max = Integer.parseInt(minAndMax[1]);
						}
						
						RuleOccurrence ruleOccurrence = new RuleOccurrence(simpleTypeNameAndOccurrence[0].trim(), min, max);
						result.add(ruleOccurrence);
					}
				} catch (Throwable th) {
					Message errorMessage = BasicMessage.INCORRECT_MODEL_VALIDATION_EXPRESSION.getMessageBuilder()
					    .parameters(expression, th.getMessage())
					    .build();
					throw new ModelConverterException(errorMessage.getMessage());
				}
			}
			
			return result;
		}
		
		private final String simpleTypeName;
		private final String collectionPropertyName;
		private final int minOccurrence;
		private final int maxOccurrence;
		
		/**
		 * @param simpleTypeName
		 * @param minOccurrence
		 * @param maxOccurrence
		 */
		public RuleOccurrence(String simpleTypeName, int minOccurrence, int maxOccurrence) {
			super();
			this.simpleTypeName = simpleTypeName;
			this.collectionPropertyName = null;
			this.minOccurrence = minOccurrence;
			this.maxOccurrence = maxOccurrence;
		}
		
		/**
		 * @param simpleTypeName
		 * @param collectionPropertyName
		 * @param minOccurrence
		 * @param maxOccurrence
		 */
		public RuleOccurrence(String simpleTypeName, String collectionPropertyName, int minOccurrence,
				int maxOccurrence) {
			super();
			this.simpleTypeName = simpleTypeName;
			this.collectionPropertyName = collectionPropertyName;
			this.minOccurrence = minOccurrence;
			this.maxOccurrence = maxOccurrence;
			
			throw new ModelConverterException("validation of occurrencies of collections of model elements is not yet implemented");
		}

		public String getCollectionPropertyName() {
			return collectionPropertyName;
		}

		public String getSimpleTypeName() {
			return simpleTypeName;
		}

		public int getMinOccurrence() {
			return minOccurrence;
		}

		public int getMaxOccurrence() {
			return maxOccurrence;
		}

		@Override
		public Collection<Message> validate(Collection<Object> modelElements) {
			Collection<Message> result = new LinkedHashSet<>();
			
			LinkedHashSet<String> names = new LinkedHashSet<>();
			int counter = 0;
			for (Object element : modelElements) {
				if (element.getClass().getSimpleName().equalsIgnoreCase(simpleTypeName)) {
					counter++;
					if (element instanceof ModelElementI) {
						ModelElementI modelElement = (ModelElementI) element;
						names.add(modelElement.getName());
					} else {
						names.add(element.toString());
					}
				}
			}
		
			if (minOccurrence >= 0 && minOccurrence > counter) {
				// too few model elements found
				StringBuilder sb = new StringBuilder();
				for (String name : names) {
					if (sb.length() > 0) sb.append(", ");
					sb.append("'").append(name).append("'");
				}
				if (sb.length() > 0) sb.insert(0, " :");
				Message message = new Message(MessageStatus.ERROR,
						"At least " + minOccurrence + " model element(s) of type '" + simpleTypeName + "' have to be passed to the generator. Only " + counter + " was/were found" + sb + "." );
				result.add(message);
			}
			
			if (maxOccurrence >= 0 && maxOccurrence < counter) {
				// too many model elements found
				StringBuilder sb = new StringBuilder();
				for (String name : names) {
					if (sb.length() > 0) sb.append(", ");
					sb.append("'").append(name).append("'");
				}
				Message message = new Message(MessageStatus.ERROR,
						"At most " + maxOccurrence + " model element(s) of type '" + simpleTypeName + "' can be processed by the generator. But " + counter + " was/were found: " + sb + ".");
				result.add(message);
			}
			
			return result;
		}
	}
	
	/**
	 * @author marcu
	 *
	 */
	public static final class DefaultValidator implements ModelValidatorI {

		@Override
		public Collection<Message> validate(Collection<Object> modelElements) {
			return validate(modelElements, null);
		}
	}
}
