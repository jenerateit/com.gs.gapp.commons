package com.gs.gapp.metamodel.basic;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.jenerateit.modelconverter.ModelConverterOptions;

public class ConversionDetails {
	
	private static final SimpleDateFormat DATE_FORMAT_LONG = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private final String modelElementConverterClassname;
	private final String modelConverterClassName;
	private final ModelConverterOptions modelConverterOptions;
	private final String timestamp;

	/**
	 * @param modelConverterClassName
	 * @param modelElementConverterClassname
	 */
	public ConversionDetails(String modelConverterClassName,
			String modelElementConverterClassname, ModelConverterOptions modelConverterOptions) {
		super();
		this.modelConverterClassName = modelConverterClassName;
		this.modelElementConverterClassname = modelElementConverterClassname;
		this.modelConverterOptions = modelConverterOptions;
		this.timestamp = DATE_FORMAT_LONG.format(new Date());
	}
	/**
	 * @return the modelElementConverterClassname
	 */
	public final String getModelElementConverterClassname() {
		return modelElementConverterClassname;
	}
	/**
	 * @return the modelConverterClassName
	 */
	public final String getModelConverterClassName() {
		return modelConverterClassName;
	}

	public boolean hasIdenticalModelConverter(ConversionDetails otherDetails) {
		if (otherDetails == null) return false;
		return this.modelConverterClassName != null &&
				otherDetails.modelConverterClassName != null &&
				this.modelConverterClassName.equals(otherDetails.modelConverterClassName) &&
				otherDetails.modelConverterOptions.equals(this.modelConverterOptions);
	}

	/**
	 * @return the modelConverterOptions
	 */
	public ModelConverterOptions getModelConverterOptions() {
		return modelConverterOptions;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((modelConverterClassName == null) ? 0
						: modelConverterClassName.hashCode());
		result = prime
				* result
				+ ((modelConverterOptions == null) ? 0 : modelConverterOptions
						.hashCode());
		result = prime
				* result
				+ ((modelElementConverterClassname == null) ? 0
						: modelElementConverterClassname.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConversionDetails other = (ConversionDetails) obj;
		if (modelConverterClassName == null) {
			if (other.modelConverterClassName != null)
				return false;
		} else if (!modelConverterClassName
				.equals(other.modelConverterClassName))
			return false;
		if (modelConverterOptions == null) {
			if (other.modelConverterOptions != null)
				return false;
		} else if (!modelConverterOptions.equals(other.modelConverterOptions))
			return false;
		if (modelElementConverterClassname == null) {
			if (other.modelElementConverterClassname != null)
				return false;
		} else if (!modelElementConverterClassname
				.equals(other.modelElementConverterClassname))
			return false;
		return true;
	}
	

	@Override
	public String toString() {
		return "ConversionDetails [modelElementConverterClassname=" + modelElementConverterClassname
				+ ", modelConverterClassName=" + modelConverterClassName + ", modelConverterOptions="
				+ modelConverterOptions + ", timestamp=" + timestamp + "]";
	}
	public String getModelConverterId() {
		return this.modelConverterClassName + (this.modelConverterOptions == null ? "" : this.modelConverterOptions.hashCode()+"");
	}
	public String getTimestamp() {
		return timestamp;
	}

}
