/**
 * 
 */
package com.gs.gapp.metamodel.basic;

/**
 * @author mmt
 *
 */
public enum Placement {

	LEFT,
	TOP,
	RIGHT,
	BOTTOM,
	;
	
}
