package com.gs.gapp.metamodel.basic.options;

import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jenerateit.target.TargetI;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;



/**
 * A central container to provide generation group options in a way that makes them easier to
 * use within writers and targets. Using this class and extending it lets you provide and use well-defined
 * and documented methods to access options.
 * 
 * @author mmt
 *
 */
public class GenerationGroupOptions extends AbstractOptions {
	
	public static final OptionDefinitionBoolean OPTION_DEF_ENABLE_GENERATION_ANALYTICS =
			new OptionDefinitionBoolean("enable-generation-analytics",
					                    "'true' allows for the generation of files that visualize converter-tree and element-tree");

	public static final OptionDefinitionString OPTION_DEF_TARGET_URI_ROOT =
			new OptionDefinitionString("target-uri-root",
					                   "a root path for all generated file - this can be useful when not generating into IDE-projects");

	public static final OptionDefinitionString OPTION_DEF_TARGET_URI_PREFIX =
			new OptionDefinitionString("target.uri.prefix",
					                   "the source path, where all files are going to be written to");

	public static final OptionDefinitionString OPTION_DEF_TARGET_URI_PREFIX_FOR_TESTS =
			new OptionDefinitionString("target.uri.prefix.for.tests",
					                   "the source path, where all files are going to be written to that are related for tests");
	
	public static final OptionDefinitionString OPTION_DEF_LINE_BREAK_STYLE =
			new OptionDefinitionString("line.break.style",
					                   "can be one of 'MAC', 'MAC_OLD', 'WINDOWS' or 'UNIX', with this option you control, which characters are being used for line breaks in generated code");

	private final Map<String, Serializable> options = new LinkedHashMap<>();
	
	/**
	 * Use this constructor to instantiate the options object from a writer.
	 * 
	 * @param writer
	 */
	public GenerationGroupOptions(AbstractWriter writer) {
		super();
		if (writer == null) throw new NullPointerException("parameter 'writer' must not be null");
		init(writer, null);
	}
	
	/**
	 * Use this constructor to instantiate the options object from a target.
	 * 
	 * @param target
	 */
	public GenerationGroupOptions(TargetI<?> target) {
		super();
		if (target == null) throw new NullPointerException("parameter 'target' must not be null");
		init(null, target);
	}
	
	/**
	 * This method prepares the options-map (Map<String,Serializable>). That map is used in order to be able to
	 * access generation group options, even when the AbstractTarget or AbstractWriter instances that are passed
	 * to the constructors are not in a valid state anymore.
	 * 
	 * @param writer
	 * @param target
	 */
	private void init(AbstractWriter writer, TargetI<?> target) {
		// --- prepare a Map<String,Serializable> since that map is not yet available through the VD-API
		for (OptionDefinition<? extends Serializable> optionDef : getOptionDefinitions()) {
			 this.options.put(optionDef.getKey(), this.getRawOption(optionDef.getKey(), writer, target));
		}
	}
	
	/**
	 * Reads an option for the given option string and returns it as a simple Serializable object.
	 * This method is intended to be used by children of this class in order not having to duplicate
	 * code that checks for null values and other stuff.
	 * 
	 * @param option
	 * @return
	 * @throws NullPointerException when parameter 'option' is null or when there is not yet a transformation target instance available.
	 */
	private final Serializable getRawOption(String option, AbstractWriter writer, TargetI<?> target) {
		if (option == null) throw new NullPointerException("parameter 'option' must not be null");
		
		TargetI<?> transformationTarget = null;
		if (writer != null) {
			transformationTarget = writer.getTransformationTarget();
			if (transformationTarget == null) throw new NullPointerException("tried to read a generation group option before a transformation target instance is available");
		} else if (target != null) {
			transformationTarget = target;
		}
		return transformationTarget.getGenerationGroup().getOption(option);
	}
	
	/**
	 * @param option
	 * @return
	 */
	@Override
	protected final Serializable getOptionValue(String option) {
		return this.options.get(option);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.AbstractOptions#getProvidedOptionKeys()
	 */
	@Override
	protected List<String> getProvidedOptionKeys() {
		return new ArrayList<>( options.keySet() );
	}
	
	/**
	 * This method returns all options that are available for the generation group where this class is going to be used.
	 * Overwrite this method and add your own definitions when you extend this class;
	 * 
	 * @return a set of option definitions, never returns null
	 */
	@Override
	public Set<OptionDefinition<? extends Serializable>> getOptionDefinitions() {
		return new LinkedHashSet<OptionDefinition<?>>(OptionDefinitionEnum.getDefinitions());
	}
	
	/**
	 * @return
	 */
	public boolean isGenerationAnalyticsEnabled() {
		String option = (String) getOptionValue(OptionDefinitionEnum.OPTION_ENABLE_GENERATION_ANALYTICS.getName());
		validateBooleanOption(option, OptionDefinitionEnum.OPTION_ENABLE_GENERATION_ANALYTICS.getName());
		return option == null ? false : Boolean.parseBoolean(option);
	}
	
	/**
	 * @return
	 */
	public String getTargetRoot() {
		Serializable option = getOptionValue(OptionDefinitionEnum.OPTION_TARGET_URI_ROOT.getName());
		if (option != null) {
			return option.toString();
		}
		return "";
	}

	/**
	 * This method might be overwritten in order to set a different prefix for certain targets.
	 * unit-test classes are a good example for this, since they usually are located in a
	 * different source folder than regular classes.
	 *
	 * @return this target's prefix for the file path
	 */
	public String getTargetPrefix() {
		Serializable option = getOptionValue(OptionDefinitionEnum.OPTION_TARGET_URI_PREFIX.getName());
		if (option != null) {
			return option.toString();
		}
		return getDefaultTargetPrefix();
	}
	
	/**
	 * @return
	 */
	protected String getDefaultTargetPrefix() {
		return "generated-files";
	}
	
	/**
	 * @return
	 */
	public String getTargetPrefixForTests() {
		Serializable option = getOptionValue(OptionDefinitionEnum.OPTION_TARGET_URI_PREFIX_FOR_TESTS.getName());
		if (option != null) {
			return option.toString();
		}
		return getDefaultTargetPrefixForTests();
	}
	
	/**
	 * @return
	 */
	protected String getDefaultTargetPrefixForTests() {
		return "generated-files-for-tests";
	}
	
	/**
	 * Get the characters to be used for every line break
	 * during generation.
	 * 
	 * @return
	 */
	public CharSequence getLineBreakChars() {
		Serializable option = getOptionValue(OptionDefinitionEnum.OPTION_LINE_BREAK_STYLE.getName());
		PlatformForLineBreakStyle platform = getDefaultPlatformLineBreakStyle();
		if (option != null) {
			try {
			    platform = PlatformForLineBreakStyle.valueOf(option.toString().toUpperCase());
			} catch (IllegalArgumentException ex) {
				throwIllegalEnumEntryException((String) option, OptionDefinitionEnum.OPTION_LINE_BREAK_STYLE.getName(), PlatformForLineBreakStyle.values());
			}
		}
		
		return platform.getLineBreakChars();
	}
	
	/**
	 * @return
	 */
	protected PlatformForLineBreakStyle getDefaultPlatformLineBreakStyle() {
		return PlatformForLineBreakStyle.UNIX;
	}
	
	@Override
	protected void throwOptionValidationException(Message errorMessage) {
		throw new WriterException(errorMessage.getMessage());
	}
	
	/**
	 * @author mmt
	 *
	 */
	public static enum OptionDefinitionEnum {

		/**
		 * This option can be true or false. If it is set to true, special targets and writers
		 * are going to be used that analyze what's going on in the transformation steps.
		 * The analysis results will be written to .xml, .log and similar files. Those files
		 * are meant to support developers of generators during their daily work.
		 */
		OPTION_ENABLE_GENERATION_ANALYTICS ( OPTION_DEF_ENABLE_GENERATION_ANALYTICS ),
		
		/**
		 * This option is used when a target file should be written to a directory that is not located within a cloud-connector's root path.
		 */
		OPTION_TARGET_URI_ROOT ( OPTION_DEF_TARGET_URI_ROOT ),
		
		/**
		 * <p>
		 * This key may be used within the generation group options
		 * to add a fixed prefix in front of the target {@link URI}.
		 * </p>
		 * <p>
		 * For example an prefix for maven projects:
		 * </p>
		 * <pre><code>
		 * &lt;options&gt;
		 *     &lt;target.uri.prefix&gt;src/main/java&lt;/target.uri.prefix&gt;
		 * &lt;/options&gt;
		 * </code></pre>
		 */
		OPTION_TARGET_URI_PREFIX ( OPTION_DEF_TARGET_URI_PREFIX ),
		
		/**
		 * <p>
		 * This key may be used within the generation group options
		 * to add a fixed prefix in front of the target {@link URI}.
		 * </p>
		 * <p>
		 * For example an prefix for maven projects:
		 * </p>
		 * <pre><code>
		 * &lt;options&gt;
		 *     &lt;target.uri.prefix.for.tests&gt;src/test/java&lt;/target.uri.prefix.for.tests&gt;
		 * &lt;/options&gt;
		 * </code></pre>
		 */
		OPTION_TARGET_URI_PREFIX_FOR_TESTS ( OPTION_DEF_TARGET_URI_PREFIX_FOR_TESTS ),
		
		/**
		 * <p>
		 * This key may be used within the generation group options
		 * to control, which line break characters are going to be used during
		 * code generation.
		 * Valid option values are: MAC, MAC_OLD, WINDOWS, UNIX (the value is evaluated in a case-insensitive manner)
		 * </p>
		 */
		OPTION_LINE_BREAK_STYLE ( OPTION_DEF_LINE_BREAK_STYLE ),

		;

		private static final Map<String, OptionDefinitionEnum> stringToEnum = new HashMap<>();

		static {
			for (OptionDefinitionEnum m : values()) {
				stringToEnum.put(m.getName(), m);
			}
		}
		
		/**
		 * @return
		 */
		public static Set<OptionDefinition<? extends Serializable>> getDefinitions() {
			Set<OptionDefinition<? extends Serializable>> result = new LinkedHashSet<>();
			for (OptionDefinitionEnum m : values()) {
				result.add(m.getDefinition());
			}
			return result;
		}

		/**
		 * @param datatypeName
		 * @return
		 */
		public static OptionDefinitionEnum fromString(String datatypeName) {
			return stringToEnum.get(datatypeName);
		}

		private final OptionDefinition<? extends Serializable> definition;
		
		private OptionDefinitionEnum(OptionDefinition<? extends Serializable> definition) {
			this.definition = definition;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return this.getDefinition().getName();
		}

		public OptionDefinition<? extends Serializable> getDefinition() {
			return definition;
		}
	}

	protected enum PlatformForLineBreakStyle {
		UNIX    ("\n"),
		WINDOWS ("\r\n"),
		MAC_OLD ("\r"),
		MAC     ("\n"),
		;
		
		private final CharSequence lineBreakChars;
		
		private PlatformForLineBreakStyle(CharSequence lineBreakChars) {
			this.lineBreakChars = lineBreakChars;
		}

		public CharSequence getLineBreakChars() {
			return lineBreakChars;
		}
	}
}
