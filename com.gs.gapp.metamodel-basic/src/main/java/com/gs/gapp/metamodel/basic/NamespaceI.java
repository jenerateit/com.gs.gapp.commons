/**
 * 
 */
package com.gs.gapp.metamodel.basic;

import java.util.Set;

/**
 * @author mmt
 *
 */
public interface NamespaceI extends ModelElementI {

	/**
	 * Add an element to this module.
	 * 
	 * @param e the element to add
	 * @return true if this set did not already contain the specified element
	 */
	public boolean addElement(ModelElement e);
	
	
	/**
	 * Getter for the elements in this module.
	 * 
	 * @return the elements
	 */
	public Set<ModelElement> getElements();
	
	/**
	 * A convenience method to get all elements that are instances of the given type
	 * and are in the modules set of model elements.
	 * 
	 * @param resultType
	 * @return
	 */
	public <T extends ModelElement> Set<T> getElements(Class<T> resultType);
	
	/**
	 * @param e
	 */
	public void removeElement(ModelElement e);
}
