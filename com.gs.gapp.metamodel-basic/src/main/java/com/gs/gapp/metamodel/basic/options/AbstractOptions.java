package com.gs.gapp.metamodel.basic.options;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.jenerateit.exception.JenerateITException;
import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.BasicMessage;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;


/**
 * @author mmt
 *
 */
public abstract class AbstractOptions {

	public AbstractOptions() {}

	/**
	 * @return
	 */
	public Set<OptionDefinition<? extends Serializable>> getOptionDefinitions() {
    	Set<OptionDefinition<? extends Serializable>> result = new LinkedHashSet<>();
    	
    	return result;
    }
	
	/**
	 * @return
	 */
	public final List<OptionDefinitionWithValue> getOptionDefinitionsWithValue() {
		List<OptionDefinitionWithValue> result = new ArrayList<>();
		
		Set<String> keys = new HashSet<>();
		
		for (OptionDefinition<? extends Serializable> optionDefinition : getOptionDefinitions()) {
			result.add( new OptionDefinitionWithValue(optionDefinition, getOptionValue(optionDefinition.getKey())) );
			keys.add(optionDefinition.getKey());
		}
		
		// --- determine option keys that are provided in a genset but in fact not known as an option
		List<String> unknownOptionKeys = new ArrayList<>( getProvidedOptionKeys() );
		unknownOptionKeys.removeAll(keys);
		
		if (unknownOptionKeys.size() > 0) {
			StringBuilder sb = new StringBuilder();
			for (String unknownKey : unknownOptionKeys) {
				if (sb.length() > 0) {
					sb.append(", ");
				}
				sb.append(unknownKey);
			}
			// --- TODO re-activate this exception once all transformation step option classes have an implementation to return a collection of all possible options (mmt 18-Mar-2015)
//			throw new RuntimeException("option keys are being used that the transformation step does not know of:" + sb.toString());
		}
		
		return result;
	}
	
	/**
	 * @param optionValue
	 * @param optionName
	 */
	protected void validateBooleanOption(String optionValue, String optionName) throws JenerateITException {
		if (optionValue == null || "true".equalsIgnoreCase(optionValue) || "false".equalsIgnoreCase(optionValue)) {
			// The boolean values is valid.
		} else {
			Message errorMessage = BasicMessage.UNKNOWN_OPTION_VALUE
					.getMessageBuilder()
					.parameters(optionValue, optionName, "true or false (case insensitive)")
					.build();
			throwOptionValidationException(errorMessage);
		}
	}
	
	protected void validateBooleanOption(Serializable optionValue, String optionName) throws JenerateITException {
		if (optionValue == null || "true".equalsIgnoreCase(optionValue.toString()) || "false".equalsIgnoreCase(optionValue.toString())) {
			// The boolean values is valid.
		} else {
			Message errorMessage = BasicMessage.UNKNOWN_OPTION_VALUE
					.getMessageBuilder()
					.parameters(optionValue.toString(), optionName, "true or false (case insensitive)")
					.build();
			throwOptionValidationException(errorMessage);
		}
	}
	
	protected void validateNumericOption(Serializable optionValue, String optionName) throws JenerateITException {
		if (optionValue == null) {
			// no value at all is valid
		} else {
			try {
			    Integer.parseInt(optionValue.toString());
			} catch (NumberFormatException ex) {
				Message errorMessage = BasicMessage.OPTION_VALUE_IS_NOT_A_NUMBER
						.getMessageBuilder()
						.parameters(optionValue.toString(), optionName)
						.build();
				throwOptionValidationException(errorMessage);
			}
		}
	}
	
	/**
	 * @param optionValue
	 * @param optionName
	 * @param possibleValues
	 * @throws JenerateITException
	 */
	protected void throwIllegalEnumEntryException(String optionValue, String optionName, Enum<?>[] possibleValues) throws JenerateITException {
		Message errorMessage = BasicMessage.UNKNOWN_OPTION_VALUE
				.getMessageBuilder()
				.parameters(optionValue, optionName, Arrays.toString(possibleValues))
				.build();
		throwOptionValidationException(errorMessage);
	}
	
	/**
	 * @param optionValue
	 * @param optionName
	 * @param possibleValues
	 * @throws JenerateITException
	 */
	protected void throwIllegalEnumEntryException(String optionValue, String optionName, List<String> possibleValues) throws JenerateITException {
		Message errorMessage = BasicMessage.UNKNOWN_OPTION_VALUE
				.getMessageBuilder()
				.parameters(optionValue, optionName, possibleValues.stream().collect(Collectors.joining(", ")))
				.build();
		throwOptionValidationException(errorMessage);
	}
	
	protected void throwOptionValidationException(Message errorMessage) {
		throw new ModelConverterException(errorMessage.getMessage());
	}
	
	abstract Serializable getOptionValue(String key);
	
	abstract List<String> getProvidedOptionKeys();
}
