/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.basic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.AcceptState;
import com.gs.gapp.metamodel.AcceptableI;
import com.gs.gapp.metamodel.VisitorState;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinition.OptionValue;

/**
 * @author hrr
 *
 */
public abstract class ModelElement
	implements ModelElementI, ModelElementTraceabilityI, OptionsI, Comparable<ModelElementI> {

	public final static int DEFAULT_SORT_VALUE = -1;
	
	/*
	 * the serial version UID
	 */
	private static final long serialVersionUID = -7902679373271939622L;

	/*
	 * An originating element is the element that during model enhancement/model conversion
	 * lead to the creation of this model element.
	 */
	private transient Object originatingElement;

	private final transient Set<Object> additionalOriginatingElements = new LinkedHashSet<>();

	private final transient Set<ModelElementI> resultingElements = new LinkedHashSet<>();

	private String creatorClassname;

	private Model model;

	private ManualModificationStatus manualModificationStatus = ManualModificationStatus.UNCHANGED;

	private ConversionDetails conversionDetails;
	
	/*
	 * If this flag is set to false, then there
	 * is never any file being generated
	 * for this model element.
	 * It is not allowed to set 'generated' to true after having set it to 'false'.
	 * An IllegalArgumentException is being thrown when such an attempt is being made.
	 */
	private boolean generated = true;
	// the name of this model element
	private final String name;
	// Documentation
	private String documentation = null;

	private Module module;

	private final Set<OptionDefinition<?>.OptionValue> options = new LinkedHashSet<>();
	
	private Version version;
	private Version sinceVersion;
	private Version deprecatedSinceVersion;
	
	private boolean inGlobalCache = false;
	
	private int sortValue = DEFAULT_SORT_VALUE;
	
	/**
	 * Constructor.
	 *
	 * @param name the name of this element
	 */
	public ModelElement(String name) {
		super();

		if (!StringTools.isText(name)) {
			Message errorMessage = BasicMessage.NO_NAME_PROVIDED_FOR_A_MODEL_ELEMENT.getMessageBuilder()
				.parameters(this.getClass().getName())
				.build();
			throw new ModelConverterException(errorMessage.getMessage());
		}
		this.name = name;
	}

	/**
	 * Getter for the name of this model element.
	 *
	 * @return the name of this element
	 */
	@Override
	public String getName() {
		return name;
	}
	
	/**
	 * @return
	 */
	public String getQualifiedName() {
		return getName();
	}

	@Override
	public String getId() {
		return new StringBuilder(getClass().getName().replace(".", "_").replace("$", "_").replace("-", "_")).append("_").append(getName().replace(".", "_").replace("$", "_").replace("/", "_").replace("-", "_")).toString();
	}

	/**
	 * Getter for the generated flag.
	 * This flag indicates if this element needs to be included in the generation process.
	 *
	 * @return true if this element needs to be generated otherwise false
	 */
	@Override
	public boolean isGenerated() {
		if (this.module != null && this.module.isGenerated() == false) return false;
		
		return this.generated;
		
		// this was an attempt to implement a mechanism to prevent the duplicate generation of files (setting generated to false by a converter)
//		return isOriginatingElementGenerated();
	}
	
	/**
	 * @return
	 */
	@Override
	public final boolean isAnOriginatingElementNotGenerated() {
		if (this.getOriginatingElement() instanceof ModelElementI) {
			ModelElementI originatingElement = (ModelElementI) this.getOriginatingElement();
			boolean isGenerated = originatingElement.isGenerated();
			if (isGenerated == false) {
				return true;
			} else {
				return originatingElement.isAnOriginatingElementNotGenerated();
			}
		}
		
		return false;
	}

	/**
	 * Setter for the generated flag.
	 * This flag indicates if this element needs to be included in the generation process.
	 *
	 * @param generated the generated to set
	 */
	@Override
	public final void setGenerated(boolean generated) {
		if (this.generated == false && generated == true) {
			Message errorMessage = BasicMessage.ATTEMPT_TO_ENABLE_GENERATION_FOR_A_NON_GENERATED_ELEMENT.getMessageBuilder()
				.parameters(this.getClass().getName())
				.build();
			throw new ModelConverterException(errorMessage.getMessage());
		}
		this.generated = generated;
	}

	/**
	 * Getter for the origin element this element is created for.
	 *
	 * @return the originatingElement the origin element
	 */
	@Override
	public Object getOriginatingElement() {
		return originatingElement;
	}

	/**
	 * @param <T>
	 * @param clazz
	 * @return
	 */
	@Override
	public <T> T getOriginatingElement(Class<T> clazz) {
		T result = null;

		if (clazz.isInstance(this.getOriginatingElement())) {
			result = clazz.cast(this.getOriginatingElement());
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public Object getRootOriginatingElement() {
		Object result = null;
		
		ModelElementTraceabilityI traceableElement = this;
		while (traceableElement.getOriginatingElement() instanceof ModelElementTraceabilityI) {
			traceableElement = (ModelElementTraceabilityI) traceableElement.getOriginatingElement();
		}
		
		if (traceableElement.getOriginatingElement() != null) {
			result = traceableElement.getOriginatingElement();
		} else {
			result = traceableElement;
		}
		
		return result;
	}
	
	/**
	 * Convenience method to get hold of the link that points to the original model element
	 * that indirectly caused the generation of code for the model element.
	 * 
	 * <p>
	 * Most often, this method will return a URL that points to a web application.
	 * 
	 * @return a link that points to th original model element or null if this is not applicable
	 */
	public String getLinkToOriginalModelElement() {
		Object rootOriginatingElement = this.getRootOriginatingElement();
		if (rootOriginatingElement instanceof ModelElementWrapper) {
			// add information where to find the element in the model
			ModelElementWrapper wrapper = (ModelElementWrapper) rootOriginatingElement;
			if (wrapper.getLink() != null) {
				return wrapper.getLink();
			}
		}
		
		return null;
	}

	/**
	 * @param originatingElement must not be null, if null is passed, the method does nothing
	 */
	@Override
	public final void setOriginatingElement(final Object originatingElement) {
		
		if (originatingElement == null) return;  // the originating element must not be set to null
		if (originatingElement == this) return;  // Attempt to set the originatingElement to itself. This is not allowed, but it is not worth to throw an exception.
		if (this.isInGlobalCache()) return;  // a model element that is available in a global cache, must not have an original element set
		if (this.originatingElement != null && this.originatingElement == originatingElement) return;  // Attempt to set the originatingElement to the same object again. There is nothing to be done here.
		
	    if (originatingElement instanceof ModelElement) {
			ModelElement modelElement = (ModelElement) originatingElement;
	    	if (modelElement.isInGlobalCache()) {
	    		return;  // a model element that is in the global cache must not act as an originating element, otherwise we get memory leaks (mmt 27-Aug-2018)
	    	}
	    }
	    
	    if (originatingElement != null && "PrimitiveType".equalsIgnoreCase(originatingElement.getClass().getSimpleName())) {
//	    	System.out.println("primitive type must not have originating element");
	    }
	    
	    if (this.getClass().getName().startsWith("com.gs.gapp.metamodel.basic.options")) {
	    	return;  // a model element that is related to option definitions must not have an originating element, otherwise we get memory leaks (mmt 27-Aug-2018)
	    }

		
		if (this.originatingElement != null && this.originatingElement instanceof ModelElementWrapper && originatingElement instanceof ModelElementWrapper) {
			ModelElementWrapper wrapper = (ModelElementWrapper) this.originatingElement;
			ModelElementWrapper wrapperParam = (ModelElementWrapper) originatingElement;
			if (wrapper.equals(wrapperParam)) {
				return;  // not need to overwrite a wrapped element with another wrapper instance that wraps the same element
			}
		}

		if (this.originatingElement != null && this.originatingElement != originatingElement) {
//			throw new IllegalArgumentException("a previously set originating element must not be overwritten (previous:" + this.originatingElement + ", new:" + originatingElement + ")");
			// Changed logic from throwing an exception to maintaining a set of additional originating elements. Reason for this: it is possible that multiple different originating elements
			// result in one and the same resulting model element. An example for this is uint16->Short and int16->Short for BasicToJavaConverter. (mmt 30-Sep-2013)
			this.additionalOriginatingElements.add(originatingElement);
		} else {
		    this.originatingElement = originatingElement;
		}

		// Note that we intentionally do not use this.originatingElement here since it is possible that this.originatingElement has not been set to the parameter originatingElement!
		if (originatingElement instanceof ModelElementWrapper) {
			if (this.getOriginatingElement() instanceof ModelElementWrapper) {
				ModelElementWrapper modelElementWrapper = (ModelElementWrapper) this.getOriginatingElement();
				modelElementWrapper.addResultingModelElement(this);
			}
		} else if (originatingElement instanceof ModelElement) {
			if (this.getOriginatingElement() instanceof ModelElement) {
				ModelElement originatingModelElement = (ModelElement) this.getOriginatingElement();
			    originatingModelElement.addResultingModelElement(this);
			}
		}
	}

	/**
	 * @param modelElement
	 * @return
	 */
	public boolean hasOriginatingElement(Serializable modelElement) {
		if (this == modelElement) {
			return true;
		} else {
			if (getOriginatingElement() == null) {
				return false;
			} else {
				if (getOriginatingElement() instanceof ModelElement) {
				    ModelElement thisOriginatingElement = (ModelElement) getOriginatingElement();
					return thisOriginatingElement.hasOriginatingElement(modelElement);
				} else if (getOriginatingElement() instanceof ModelElementWrapper) {
					@SuppressWarnings("unused")
					ModelElementWrapper modelElementWrapper = (ModelElementWrapper) getOriginatingElement();
					return false;  // assuming that a wrapper does not have an originating element, this is fine
				} else {
					return false;
				}
			}
		}
	}

	/**
	 * @param element
	 */
	protected final void addResultingModelElement(ModelElement element) {
		if (this.isInGlobalCache()) {
			// elements that are available in a global cache, must not have any resulting model elements, otherwise we have a memory leak (mmt 04-Nov-2016)
			return;
		}
		
		this.resultingElements.add(element);
	}

	/**
	 * @param resultingModelElement
	 */
	protected final void removeResultingModelElement(ModelElementI resultingModelElement) {
		if (resultingModelElement != null) {
			@SuppressWarnings("unused")
			boolean resultingElementContainsElement = this.resultingElements.contains(resultingModelElement);
//			if (resultingElementContainsElement) {
//			    System.out.println("resulting elements contain the elment " + resultingModelElement);
//			}

		    boolean removed = this.resultingElements.remove(resultingModelElement);
		    if (!removed) {
//		    	System.out.println("not removed " + resultingModelElement + ", hashCode:" + resultingModelElement.hashCode());
		    } else {
//		    	System.out.println("removed " + resultingModelElement);
		    }
		}
	}

	/**
	 * @param modelConverterId
	 */
	@Override
	public void clearResultingElements(String modelConverterId) {
		if (modelConverterId == null || modelConverterId.length() == 0) {
			this.resultingElements.clear();
		} else {
			for (ModelElementI resultingElement : new LinkedHashSet<>(this.resultingElements)) {
				if (resultingElement.getConversionDetails() != null && resultingElement.getConversionDetails().getModelConverterId().equals(modelConverterId)) {
                    this.removeResultingModelElement(resultingElement);
				}
			}
		}
	}

	/**
	 * @return
	 */
	@Override
	public Set<ModelElementI> getResultingModelElements() {
		return Collections.unmodifiableSet(this.resultingElements);
	}


	/**
	 * @param type
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends ModelElement> Set<T> getResultingModelElements(Class<T> type) {
		Set<T> result = new LinkedHashSet<>();
		for (ModelElementI element : getResultingModelElements()) {
			if (type.isAssignableFrom(element.getClass())) {
				result.add((T) element);
			}
		}
		return result;
	}
	
	/**
	 * This method returns null or the one element in the collection of resulting elements
	 * that is of the given type _and_ got added last.
	 * 
	 * @param type
	 * @param exactMatch the method checks for class equality when set to true
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends ModelElement> T getLatestResultingModelElement(Class<T> type, boolean exactMatch) {
		T result = null;
		for (ModelElementI element : getResultingModelElements()) {
			if (exactMatch && type == element.getClass()) {
				result = (T) element;
			} else if (type.isAssignableFrom(element.getClass())) {
				result = (T) element;
			}
		}
		return result;
	}
	
	/**
	 * This method returns null or the one element in the collection of resulting elements
	 * that is of the given type _and_ got added first.
	 * 
	 * @param type
	 * @param exactMatch the method checks for class equality when set to true
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends ModelElement> T getEarliestResultingModelElement(Class<T> type, boolean exactMatch) {
		T result = null;
		for (ModelElementI element : getResultingModelElements()) {
			if (exactMatch && type == element.getClass()) {
				result = (T) element;
			} else if (type.isAssignableFrom(element.getClass())) {
				result = (T) element;
			}
			
			if (result != null) {
				break;
			}
		}
		return result;
	}

	/**
	 * This method trys to find a single object of given type in the set of resulting model elements.
	 * @param type
	 * @return derived object
	 */
	@Override
	public <T extends ModelElementI> T getSingleExtensionElement(Class<T> type) {
		return getSingleExtensionElement(type, false);
	}

	/**
	 * This method trys to find a single object of given type in the set of resulting model elements.
	 * @param type
	 * @param exactMatch if true, only checks for elements be of the exact same class as the given one
	 * @return derived object
	 */
	@Override
	public <T extends ModelElementI> T getSingleExtensionElement(Class<T> type, boolean exactMatch) {
		return getSingleExtensionElement(type, exactMatch, null);
	}
	
	/**
	 * This method trys to find a single object of given type in the set of resulting model elements.
	 * @param type
	 * @param exactMatch if true, only checks for elements be of the exact same class as the given one
	 * @param conversionDetails an optional conversion detail object to resolve ambiguosities
	 * @return derived object
	 */
	public <T extends ModelElementI> T getSingleExtensionElement(Class<T> type, boolean exactMatch, ConversionDetails conversionDetails) {
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");

		T result = null;
		for (ModelElementI resultingModelElement : getResultingModelElements()) {
			if (exactMatch && type == resultingModelElement.getClass() ||
				!exactMatch && type.isAssignableFrom(resultingModelElement.getClass())) {
				
				if (conversionDetails == null || resultingModelElement.getConversionDetails() != null &&
						resultingModelElement.getConversionDetails().hasIdenticalModelConverter(conversionDetails)) {
					if (result != null) {
						Message errorMessage = BasicMessage.AMBIGUOUS_ELEMENT_CONVERSION_DETECTED.getMessageBuilder()
							.parameters(this.toString(),
									    result.toString(),
									    resultingModelElement.toString(),
									    type.getName(),
									    resultingModelElement.getConversionDetails().toString())
							.build();
						throw new ModelConverterException(errorMessage.getMessage());
					}
				    result = type.cast(resultingModelElement);
				}
			}
		}
		return result;
	}

	/**
	 * This method trys to find a single object of given type in the set of resulting model elements.
	 * @param type
	 * @return derived object
	 */
	public <T extends ModelElement> T getSingleExtensionElementDeeply(Class<T> type) {
		return getSingleExtensionElementDeeply(type, false);
	}

	/**
	 * This method tries to find a single object of given type in the set of resulting model elements.
	 * @param type
	 * @param exactMatch if true, only checks for elements be of the exact same class as the given one
	 * @return derived object
	 */
	public <T extends ModelElement> T getSingleExtensionElementDeeply(Class<T> type, boolean exactMatch) {
		return getSingleExtensionElementDeeply(type, null, null, exactMatch);
	}

	/**
	 * @return
	 */
	public boolean hasAlreadyTargets() {
		List<TargetElement> allExtensionElementDeeply = this.getAllExtensionElementDeeply(TargetElement.class, true);
		return allExtensionElementDeeply.size() > 0;
	}

	/**
	 * @param type
	 * @param subsequentType
	 * @param exactMatch
	 * @return
	 */
	public <T1 extends ModelElementI, T2 extends ModelElementI> T1 getSingleExtensionElementDeeply(Class<T1> type, Class<T2> subsequentType, boolean exactMatch) {
		return getSingleExtensionElementDeeply(type, subsequentType, null, exactMatch);
	}


	/**
	 * @param type
	 * @param potentialResultingElement
	 * @param exactMatch
	 * @return
	 */
	public <T extends ModelElementI> T getSingleExtensionElementDeeply(Class<T> type, ModelElementI potentialResultingElement, boolean exactMatch) {
		return getSingleExtensionElementDeeply(type, null, potentialResultingElement, exactMatch);
	}


	/**
	 * This method tries to find a single object of given type in the set of resulting model elements.
	 * Only those objects that have another resulting model element of the given 'subsequentType' are getting collected.
	 *
	 * @param type
	 * @param subsequentType
	 * @param exactMatch
	 * @return
	 */
	@Override
	public <T1 extends ModelElementI, T2 extends ModelElementI> T1 getSingleExtensionElementDeeply(Class<T1> type, Class<T2> subsequentType, ModelElementI potentialResultingElement, boolean exactMatch) {
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");

		T1 result = null;
		List<T1> resultList = new ArrayList<>();

		collectExtensionElements(resultList, type, subsequentType, potentialResultingElement, exactMatch);

		if (resultList.size() == 1) {
			result = resultList.get(0);
		} else if (resultList.size() > 1) {
			Message errorMessage = BasicMessage.AMBIGUOUS_DEEP_ELEMENT_CONVERSION_DETECTED.getMessageBuilder()
			.parameters(this.toString(),
					    resultList.get(0).toString(),
					    resultList.get(1).toString(),
					    type.getName(),
					    resultList.get(0).getConversionDetails().toString(),
					    resultList.get(1).getConversionDetails().toString())
			.build();
			throw new ModelConverterException(errorMessage.getMessage());
		}

		return result;
	}

	/**
	 * @param type
	 * @param exactMatch
	 * @return
	 */
	@Override
	public <T1 extends ModelElementI, T2 extends ModelElementI> List<T1> getAllExtensionElementDeeply(Class<T1> type, boolean exactMatch) {
		return getAllExtensionElementDeeply(type, null, null, exactMatch);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementTraceabilityI#getAllExtensionElementDeeply(java.lang.Class, java.lang.Class, com.gs.gapp.metamodel.basic.ModelElementI, boolean)
	 */
	@Override
	public <T1 extends ModelElementI, T2 extends ModelElementI> List<T1> getAllExtensionElementDeeply(Class<T1> type, Class<T2> subsequentType, ModelElementI potentialResultingElement, boolean exactMatch) {
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");

		List<T1> resultList = new ArrayList<>();

		collectExtensionElements(resultList, type, subsequentType, potentialResultingElement, exactMatch);

		return resultList;
	}

	/**
	 * @param type
	 * @param exactMatch
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T extends ModelElementI> T getOriginatingElementDeeply(Class<T> type, boolean exactMatch) {
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");

		T result = null;

		ModelElementTraceabilityI element = this;
		while (element != null) {

			if (element.getOriginatingElement() instanceof ModelElementI) {
				element = (ModelElementTraceabilityI) element.getOriginatingElement();
				if (exactMatch == false && type.isAssignableFrom(element.getClass()) || exactMatch == true && type == element.getClass()) {
					result = (T) element;
					break;
				}
			} else if (element.getOriginatingElement() instanceof ModelElementWrapper) {
				ModelElementWrapper modelElementWrapper = (ModelElementWrapper) element.getOriginatingElement();
				if (exactMatch == false && type.isAssignableFrom(modelElementWrapper.getWrappedElement().getClass()) ||
					exactMatch == true && type == modelElementWrapper.getWrappedElement().getClass()) {
					result = (T) modelElementWrapper.getWrappedElement();
					break;
				}
				element = modelElementWrapper;
			} else {
				element = null;
			}
		}

		return result;
	}

	/**
	 * @param resultList
	 * @param type
	 * @param exactMatch
	 */
	@SuppressWarnings("unchecked")
	private final <T3 extends ModelElementI, T4 extends ModelElementI> List<T3> collectExtensionElements(List<T3> resultList, Class<T3> type, Class<T4> subsequentType, ModelElementI potentialResultingElement, boolean exactMatch) {
		if (resultList == null) throw new NullPointerException("parameter 'resultList' must not be null");
		if (type == null) throw new NullPointerException("parameter 'type' must not be null");

		for (ModelElementI resultingModelElement : new ArrayList<>(getResultingModelElements())) {
			if (resultingModelElement instanceof ModelElementTraceabilityI) {
				ModelElementTraceabilityI traceableElement = (ModelElementTraceabilityI) resultingModelElement;
				if (exactMatch && type == resultingModelElement.getClass() ||
					!exactMatch && type.isAssignableFrom(resultingModelElement.getClass())) {
					if (subsequentType == null && potentialResultingElement == null) {
					    resultList.add((T3)resultingModelElement);
					} else {
						boolean hasSubsequentType = true;
						if (subsequentType != null) {
							List<T4> extensionElementsForSubsequentType =
									traceableElement.getAllExtensionElementDeeply(subsequentType, null, potentialResultingElement, exactMatch);
							hasSubsequentType = extensionElementsForSubsequentType.size() > 0;
						}
						boolean hasPotentialResultingElement = true;
						if (potentialResultingElement != null) {
							hasPotentialResultingElement = traceableElement.hasExtensionElement(potentialResultingElement);
						}
						if (hasSubsequentType && hasPotentialResultingElement) {
							resultList.add((T3) resultingModelElement);
						}
					}
				}
				if (traceableElement instanceof ModelElement) {
				    ModelElement modelElement = (ModelElement) traceableElement;
					modelElement.collectExtensionElements(resultList, type, subsequentType, potentialResultingElement, exactMatch);
				}
			}
		}

		return resultList;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementTraceabilityI#hasExtensionElement(com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	public final boolean hasExtensionElement(ModelElementI element) {
		if (element == null) throw new NullPointerException("parameter 'element' must not be null");

		boolean result = false;

		for (ModelElementI resultingModelElement : getResultingModelElements()) {
			if (resultingModelElement instanceof ModelElementTraceabilityI) {
				ModelElementTraceabilityI traceableElement = (ModelElementTraceabilityI) resultingModelElement;
				if (traceableElement == element) {
				    result = true;
				    break;
				} else {
					result = traceableElement.hasExtensionElement(element);
					if (result) break;
				}
			}
		}

		return result;
	}

	/**
	 * Runs the visitor by executing {@link AcceptableI#accept(com.gs.gapp.metamodel.VisitorI)}.
	 *
	 * @param visitor the visitor
	 * @see AcceptableI#accept(com.gs.gapp.metamodel.VisitorI)
	 */
	@Override
	public final AcceptState accept(ModelElementVisitorI visitor) {
		VisitorState state = visitor.visit(this);
		switch (state) {
		case CONTINUE:
			return visitChilds(visitor);

		case INTERRUPT:
			return AcceptState.INTERRUPT;

		case STOP_PATH:
			return AcceptState.CONTINUE;

		default:
			throw new RuntimeException("The Visitor state '" + state + "' is unknown");
		}
	}

	/**
	 * Runs the visitor for child model elements. Client classes may override this method.
	 *
	 * @param visitor the visitor
	 * @return the state of the visited child nodes
	 */
	protected AcceptState visitChilds(ModelElementVisitorI visitor) {
		return AcceptState.CONTINUE;
	}

	/**
	 * Setter for the documentation body.
	 * <b>NOTE:</b> The documentation is not part of {@link #hashCode()}, {@link #equals(Object)} and {@link #compareTo(ModelElementI)}.
	 *
	 * @param body the documentation body
	 * @see com.gs.gapp.metamodel.basic.DocumentationI#setBody(java.lang.String)
	 */
	@Override
	public void setBody(String body) {
		this.documentation = body;
	}
	
	/**
	 * This is a convenience method that sets the documentation if there is not
	 * yet one and that adds to an already existing documentation if there _is_
	 * already a documentation (adding a line break before in this case).
	 * 
	 * @param textToAdd the text to set or add to existing documentation
	 */
	public void addToBody(String textToAdd) {
		if (this.documentation == null || this.documentation.length() == 0) {
			this.documentation = textToAdd == null ? "" : textToAdd;
		} else {
			this.documentation = this.documentation + System.lineSeparator() + textToAdd;
		}
	}

	/**
	 * Getter for the documentation body.
	 * <b>NOTE:</b> The documentation is not part of {@link #hashCode()}, {@link #equals(Object)} and {@link #compareTo(ModelElementI)}.
	 *
	 * @return the documentation body
	 * @see com.gs.gapp.metamodel.basic.DocumentationI#getBody()
	 */
	@Override
	public String getBody() {
		return this.documentation;
	}

	/**
	 * @return the module
	 */
	@Override
	public Module getModule() {
		return module;
	}

	/**
	 * A model element's module can only be set once. Once it is set, it must not be overwritten.
	 * 
	 * <p>Note that an element's module - once it is set - must not be reset to null afterwards.
	 * 
	 * @param module the module to set
	 */
	public void setModule(Module module) {
		if (this.module != null && module == null) {
			Message errorMessage = BasicMessage.ATTEMPT_TO_RESET_MODULE_TO_NULL.getMessageBuilder()
				.parameters(this.getQualifiedName(), this.module.getName())
				.build();
			throw new ModelConverterException(errorMessage.getMessage());
		}
		
		if (module != null) {
			if (this.isInGlobalCache()) {
				return;  // a model element that is in the global cache must not belong to a module since we do not have a concept of a global modules (mmt 27-Aug-2018)
			}
			
//			if (module.getClass().getPackage() == this.getClass().getPackage() ||
//					this.getClass().getPackage().getName().startsWith(module.getClass().getPackage().getName())) {

			if (this.module == null) {
				this.module = module;
				if (module != null && !module.getElements().contains(this)) {
					module.addElement(this);
				}
			} else if (this.module != module) {
				Message errorMessage = BasicMessage.ATTEMPT_TO_OVERWRITE_MODULE.getMessageBuilder()
					.parameters(this.module.getName(), this.getQualifiedName(), module.getName())
					.build();
				throw new ModelConverterException(errorMessage.getMessage());
			}
			
//			} else {
//				throw new RuntimeException("detected attempt to set a model element's module where the element's runtime class' package (" +
//			        this.getClass() + ") is not compatible with the module's runtime class' package (" + module.getClass() + ").");
//			}
		}
	}

	/**
	 * Compares the name of both elements.
	 *
	 * @param element the element to compare with
	 * @return a negative integer, zero, or a positive integer as this object is less than,
	 * equal to, or greater than the specified object.
	 */
	@Override
	public int compareTo(ModelElementI element) {
		if (element == null) throw new NullPointerException("cannot compare a null object to another object");
		
		int result = 0;
		Class<?> thisClass = this.getClass();
		Class<?> otherClass = element.getClass();
		
		if (thisClass.isAnonymousClass()) thisClass = thisClass.getSuperclass();
		if (otherClass.isAnonymousClass()) otherClass = otherClass.getSuperclass();
			
		if (thisClass != otherClass) {
			result = thisClass.getName().compareTo(otherClass.getName());
		} else {
			int nameComparison = this.name.compareTo(element.getName());
			int sortValueComparison = this.sortValue - element.getSortValue();
			
			if (nameComparison != 0 && sortValueComparison != 0) {
				result = sortValueComparison;
			} else if (nameComparison == 0 && sortValueComparison != 0) {
				result = 0;
			} else if (nameComparison != 0 && sortValueComparison == 0) {
				// no matter whether there has been set a sort value or not (default value = DEFAULT_SORT_VALUE), if sort value says, the objects are the same, then compare their names
				result = nameComparison;
			} else {
				// both comparisons result in a 0 => comparison has no other choice than to return 0
				result = 0;
			}
		}
		
		return result;
	}

	/**
	 * Calculate a hash code.
	 *
	 * @return the hash code
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + name.hashCode();
		return result;
	}

	/**
	 * Compare the names.
	 *
	 * @param obj the object to compare to
	 * @return true if this object is the same as the obj argument; false otherwise.
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;

		ModelElement other = ModelElement.class.cast(obj);
		return name != null ? name.equals(other.name) : name == null && other.name == null;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
        return new StringBuilder(this.getName()).append("/").append(getClass().getSimpleName()).toString();
	}

	/**
	 * @return the model
	 */
	@Override
	public Model getModel() {
		return model;
	}

	/**
	 * @param model the model to set
	 */
	@Override
	public void setModel(Model model) {
		if (this.isInGlobalCache()) {
			// a model element that is meant to be in a global cache, cannot belong to a specific model object since we do not have the concept of global models (mmt 27-Aug-2018)
		} else {
		    this.model = model;
		}
	}


	/**
	 * A creatorClassname indicates the name of the class that is responsible for creating an instance
	 * of a model element. Typically this is the name of a class that extends AbstractModelElementConverter.
	 *
	 * @return the creatorClassname
	 */
	public String getCreatorClassname() {
		return creatorClassname;
	}

	/**
	 * @param classname
	 */
	public void setCreatorClassname(String classname) {
		if (this.creatorClassname != null && this.creatorClassname.length() > 0 && (this.creatorClassname.equals(classname) == false)) {
			throw new ModelConverterException("it is not allowed to overwrite a creatorClassname (" + classname + ") once it is already set (" + this.creatorClassname + ")", this);
		}
		this.creatorClassname = classname;
	}

	/**
	 * @return the manualModificationStatus
	 */
	public ManualModificationStatus getManualModificationStatus() {
		return manualModificationStatus;
	}

	/**
	 * @param manualModificationStatus the manualModificationStatus to set
	 */
	public void setManualModificationStatus(ManualModificationStatus manualModificationStatus) {
		this.manualModificationStatus = manualModificationStatus;
	}

	/**
	 * @return the options
	 */
	@Override
	public Set<OptionDefinition<?>.OptionValue> getOptions() {
		return Collections.unmodifiableSet(options);
	}

	/**
	 * @param option
	 * @return
	 */
	private boolean addOption(OptionDefinition<?>.OptionValue option) {
		return this.options.add(option);
	}
	
	/**
	 * @param options
	 */
	@Override
	public void addOptions(OptionDefinition<?>.OptionValue ... options) {
		if (options != null) {
			for (OptionDefinition<?>.OptionValue option : options) {
				addOption(option);
			}
		}
	}

	/**
	 * @param <T>
	 * @param option
	 * @return
	 */
	public <T extends Serializable> boolean addOptionOrValue(OptionDefinition<T>.OptionValue option) {
		boolean result = false;
		OptionDefinition<T>.OptionValue existingOptionValue = getOptionValue(option.getOptionDefinition());
		if (existingOptionValue == null) {
		    result = this.options.add(option);
		} else {
			option.getOptionValues().stream()
				.forEach(optionValue -> {
					existingOptionValue.addOptionValue(optionValue);
				});
			result = true;
		}
		
		return result;
	}
	
	/**
	 * @param optionName
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public <T extends Serializable> OptionDefinition<T>.OptionValue getOption(String optionName, Class<T> clazz) {
		for (OptionDefinition<?>.OptionValue value : getOptions()) {
			if (value.getOptionDefinition().getName().equalsIgnoreCase(optionName)) {
				if (value != null) {
					if (value.getOptionDefinition().getType() == clazz) {
						return (OptionValue) value;
					} else {
						if (value.getOptionDefinition().getType() == null) {
						    throw new ModelConverterException(BasicMessage.ERROR_VALUE_TYPE_OF_OPTION_DEFINITION_COULD_NOT_BE_DETERMINED.getMessageBuilder()
					    		.modelElement(this)
								.parameters(optionName)
								.build().getMessage());
						} else if (value.getOptionDefinition().getType() != clazz) {
							throw new ModelConverterException(BasicMessage.ERROR_VALUE_TYPE_OF_OPTION_DEFINITION_NOT_MATCHING_THE_REQUESTED_VALUE_TYPE.getMessageBuilder()
								.modelElement(this)
								.parameters(value.getOptionDefinition().getType().getSimpleName(),
										    optionName,
										    clazz.getSimpleName())
							    .build().getMessage());
						}
					}
				}
			}
		}
		return null;
	}
	
	/**
	 * @param <T>
	 * @param optionDefinition
	 * @return
	 */
	public <T extends Serializable> OptionDefinition<T>.OptionValue getOptionValue(OptionDefinition<T> optionDefinition) {
		return getOption(optionDefinition.getName(), optionDefinition.getType());
	}
	
	/**
	 * @return the conversionDetails
	 */
	@Override
	public ConversionDetails getConversionDetails() {
		return conversionDetails;
	}

	/**
	 * @param conversionDetails the conversionDetails to set
	 */
	@Override
	public void setConversionDetails(ConversionDetails conversionDetails) {
		if (this.conversionDetails != null && this.conversionDetails.equals(conversionDetails) == false) {
//			throw new ModelConverterException("it is not allowed to overwrite conversion details ('" + this.conversionDetails + "') with a different conversion detail ('" + conversionDetails + "') in case it has already been set", this);
		}
		this.conversionDetails = conversionDetails;
	}

	/**
	 * @return the additionalOriginatingElements
	 */
	public Set<Object> getAdditionalOriginatingElements() {
		return additionalOriginatingElements;
	}

	/**
	 * @return
	 */
	public Version getVersion() {
		return version;
	}

	/**
	 * @param version
	 */
	public void setVersion(Version version) {
		this.version = version;
		if (version != null) {
			version.addModelElement(this);
		}
	}

	/**
	 * @return
	 */
	public Version getSinceVersion() {
		return sinceVersion;
	}

	/**
	 * @param sinceVersion
	 */
	public void setSinceVersion(Version sinceVersion) {
		this.sinceVersion = sinceVersion;
		if (sinceVersion != null) {
			sinceVersion.addModelElementSince(this);
		}
	}

	/**
	 * @return
	 */
	public Version getDeprecatedSinceVersion() {
		return deprecatedSinceVersion;
	}

	/**
	 * @param deprecatedSinceVersion
	 */
	public void setDeprecatedSinceVersion(Version deprecatedSinceVersion) {
		this.deprecatedSinceVersion = deprecatedSinceVersion;
		if (deprecatedSinceVersion != null) {
			deprecatedSinceVersion.addModelElementDeprecatedSince(this);
		}
	}

	/**
	 * @return
	 */
	@Override
	public boolean isInGlobalCache() {
		return inGlobalCache;
	}

	/**
	 * @param inGlobalCache
	 */
	public void setInGlobalCache(boolean inGlobalCache) {
		if (this.inGlobalCache == true && inGlobalCache == false) {
			throw new IllegalArgumentException("tried to set 'inGlobalCache' to false where its original value is true - this is not allowed");
		}
		this.inGlobalCache = inGlobalCache;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementI#getSortValue()
	 */
	@Override
	public int getSortValue() {
		return sortValue;
	}

	/**
	 * @param sortValue
	 */
	public void setSortValue(int sortValue) {
		this.sortValue = sortValue;
	}
	
	/**
	 * @return
	 */
	public static boolean isAnalyticsMode() {
		String analyticsMode = System.getProperty("org.jenerateit.server.analyticsMode");
		return isDevelopmentMode() &&
				(analyticsMode != null && analyticsMode.length() > 0 && analyticsMode.equalsIgnoreCase("true"));
	}
	
	/**
	 * @return
	 */
	public static boolean isDevelopmentMode() {
		String developmentMode = System.getProperty("org.jenerateit.server.developmentMode");
		return (developmentMode != null && developmentMode.length() > 0 && developmentMode.equalsIgnoreCase("true"));
	}
}
