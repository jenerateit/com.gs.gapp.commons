package com.gs.gapp.metamodel.basic;

import java.util.List;
import java.util.Set;

public interface ModelElementTraceabilityI {

	/**
	 * Getter for the origin element this element is created for.
	 *
	 * @return the originatingElement the origin element
	 */
	Object getOriginatingElement();

	<T> T getOriginatingElement(Class<T> clazz);

	/**
	 * This setter is going to be used by element converters in order to set the originating element at a central place.
	 * When implementing onCreateModelElement(), noone has to take care of setting up that link.
	 *
	 * @param originatingElement
	 */
	void setOriginatingElement(Object originatingElement);

	/**
	 * Getter that returns all elements that are the result of model-to-model transformations
	 * that were triggered by this model element.
	 *
	 * @return
	 */
	Set<ModelElementI> getResultingModelElements();

	/**
	 * @param modelConverterId
	 */
	void clearResultingElements(String modelConverterId);

	/**
	 * This method trys to find a single object of given type in the set of resulting model elements.
	 * @param type
	 * @return derived object
	 */
	<T extends ModelElementI> T getSingleExtensionElement(Class<T> type);

	/**
	 * This method trys to find a single object of given type in the set of resulting model elements.
	 * @param type
	 * @param exactMatch if true, only checks for elements be of the exact same class as the given one
	 * @return derived object
	 */
	<T extends ModelElementI> T getSingleExtensionElement(Class<T> type, boolean exactMatch);

	/**
	 * @param type
	 * @param subsequentType
	 * @param potentialResultingElement
	 * @param exactMatch
	 * @return
	 */
	<T1 extends ModelElementI, T2 extends ModelElementI> T1 getSingleExtensionElementDeeply(Class<T1> type, Class<T2> subsequentType, ModelElementI potentialResultingElement, boolean exactMatch);

	/**
	 * @param type
	 * @param exactMatch
	 * @return
	 */
	<T1 extends ModelElementI, T2 extends ModelElementI> List<T1> getAllExtensionElementDeeply(Class<T1> type, boolean exactMatch);

	/**
	 * @param type
	 * @param subsequentType
	 * @param potentialResultingElement
	 * @param exactMatch
	 * @return
	 */
	<T1 extends ModelElementI, T2 extends ModelElementI> List<T1> getAllExtensionElementDeeply(Class<T1> type, Class<T2> subsequentType, ModelElementI potentialResultingElement, boolean exactMatch);


	/**
	 * @param element
	 * @return
	 */
	boolean hasExtensionElement(ModelElementI element);
}
