/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.basic;

/**
 * @author hrr
 *
 */
public enum ParameterType {

	/** Normal input parameter */
	IN,
	/** Output parameter only (e.g. return type in java) */
	OUT,
	/** Input and output parameter */
	INOUT
}
