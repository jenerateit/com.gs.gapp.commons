package com.gs.gapp.metamodel.basic;

import java.util.Set;

import com.gs.gapp.metamodel.basic.options.OptionDefinition;

public interface OptionsI {

	/**
	 * @return
	 */
	Set<OptionDefinition<?>.OptionValue> getOptions();

	/**
	 * @param options
	 */
	void addOptions(OptionDefinition<?>.OptionValue ... options);
}
