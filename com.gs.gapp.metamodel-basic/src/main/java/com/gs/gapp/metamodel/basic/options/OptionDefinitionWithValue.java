/**
 * 
 */
package com.gs.gapp.metamodel.basic.options;

import java.io.Serializable;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * @author mmt
 *
 */
public class OptionDefinitionWithValue extends ModelElement {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2995453712400953079L;
	
	private final OptionDefinition<? extends Serializable> optionDefinition;
	private final Serializable optionValue;
	
	/**
	 * @param optionDefinition
	 * @param optionValue
	 */
	public OptionDefinitionWithValue(OptionDefinition<? extends Serializable> optionDefinition, Serializable optionValue) {
		super(optionDefinition.getKey());
		this.optionDefinition = optionDefinition;
		this.optionValue = optionValue;
	}

	public OptionDefinition<? extends Serializable> getOptionDefinition() {
		return optionDefinition;
	}

	public Serializable getOptionValue() {
		return optionValue;
	}

}
