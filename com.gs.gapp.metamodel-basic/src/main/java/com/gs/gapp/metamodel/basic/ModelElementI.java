/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.basic;

import java.io.Serializable;

import com.gs.gapp.metamodel.AcceptableI;

/**
 * @author hrr
 *
 */
public interface ModelElementI
	extends Serializable, AcceptableI<ModelElementVisitorI>, DocumentationI, Comparable<ModelElementI>, NameableI {

	/**
	 * Getter for the name of this model element.
	 *
	 * @return the name of this element
	 */
	String getName();

	/**
	 * A getter that constructs a string-id, that uniquely identifies a model element within
	 * the whole world of model elements.
	 *
	 * @return
	 */
	String getId();
	
	/**
	 * @return
	 */
	Model getModel();
	
	/**
	 * @return
	 */
	Module getModule();
	
	/**
	 * @param model
	 */
	void setModel(Model model);

	/**
	 * Getter for the generated flag.
	 * This flag indicates if this element needs to be included in the generation process.
	 *
	 * @return true if this element needs to be generated otherwise false
	 */
	boolean isGenerated();
	
	/**
	 * @return true if any of the chain of originating elements has isGenerated() returning false
	 */
	boolean isAnOriginatingElementNotGenerated();
	
	/**
	 * If this flag is set to false, then there
	 * is never any file being generated
	 * for this model element.
	 * It is not allowed to set 'generated' to true after having set it to 'false'.
	 * An IllegalArgumentException is being thrown when such an attempt is being made.
	 * 
	 * @param generated
	 */
	void setGenerated(boolean generated);

	/**
	 * @param conversionDetails
	 */
	void setConversionDetails(ConversionDetails conversionDetails);

	/**
	 * @return
	 */
	ConversionDetails getConversionDetails();
	
	/**
	 * This method is being used in compareTo() method to be able to
	 * explicitly control the ordering of model elements.
	 * 
	 * @return
	 */
	int getSortValue();
	
	/**
	 * @return
	 */
	boolean isInGlobalCache();
	
	default String getTechnicalName() {
		return getTechnicalName("");
	}
	
	default String getTechnicalName(String space) {
		return getName().replaceAll("[^a-zA-Z0-9_\\-]", "").replace(" ", space);
	}
}
