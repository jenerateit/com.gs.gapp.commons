package com.gs.gapp.metamodel.basic;

import java.net.URI;


/**
 * TODO use TargetStatus information in analytics (mmt 05-Jun-2013)
 * @author mmt
 *
 */
public class TargetElement extends ModelElement {

	private static final long serialVersionUID = 2533038738772174946L;

	private final URI uri;

	private TargetStatus targetStatus;

	public TargetElement(String name, URI uri, TargetStatus targetStatus) {
		super(name);
		this.uri = uri;
		this.setTargetStatus(targetStatus);
	}

	/**
	 * @return the uri
	 */
	public URI getUri() {
		return uri;
	}

	/**
	 * @return the targetStatus
	 */
	public TargetStatus getTargetStatus() {
		return targetStatus;
	}

	/**
	 * @param targetStatus the targetStatus to set
	 */
	public void setTargetStatus(TargetStatus targetStatus) {
		this.targetStatus = targetStatus;
	}

	public enum TargetStatus {
		TRANSFORMED,
		LOADED,
		FOUND;
	}
}
