/**
 *
 */
package com.gs.gapp.metamodel.basic.options;

import java.io.Serializable;
import java.util.List;


/**
 * @author mmt
 *
 */
public class OptionDefinitionSerializable extends OptionDefinition<Serializable> {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6424952350985443150L;

	
	public OptionDefinitionSerializable(String key) {
		super(key);
	}

	public OptionDefinitionSerializable(String key, String description,
			boolean mandatory, Serializable defaultValue,
			List<Serializable> listOfValues, boolean multivalued) {
		super(key, description, mandatory, defaultValue, listOfValues, multivalued);
	}

	public OptionDefinitionSerializable(String key, String description,
			boolean mandatory, Serializable defaultValue, List<Serializable> listOfValues) {
		super(key, description, mandatory, defaultValue, listOfValues);
	}

	public OptionDefinitionSerializable(String key, String description,
			boolean mandatory, boolean multivalued) {
		super(key, description, mandatory, multivalued);
	}

	public OptionDefinitionSerializable(String key, String description,
			boolean mandatory, Serializable defaultValue) {
		super(key, description, mandatory, defaultValue);
	}

	public OptionDefinitionSerializable(String key, String description,
			boolean mandatory) {
		super(key, description, mandatory);
	}

	public OptionDefinitionSerializable(String key, String description,
			Serializable defaultValue) {
		super(key, description, defaultValue);
	}

	public OptionDefinitionSerializable(String key, String description) {
		super(key, description);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.OptionDefinition#convertToTypedOptionValue(java.io.Serializable)
	 */
	@Override
	public String convertToTypedOptionValue(Serializable rawOptionValue) {
		if (rawOptionValue != null) {
		    return rawOptionValue.toString();
		} else {
			return getDefaultValue() == null ? null : getDefaultValue().toString();
		}
	}
}
