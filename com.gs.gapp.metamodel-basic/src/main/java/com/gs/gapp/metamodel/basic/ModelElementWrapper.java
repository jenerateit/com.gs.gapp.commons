/**
 *
 */
package com.gs.gapp.metamodel.basic;



/**
 * Instances of this class' subclasses wrap model elements that are not serializable.
 * The wrapper is meant to be used to be able to track back to the initial model
 * elements that are used for a transformation-chain.
 *
 * @author mmt
 *
 */
public class ModelElementWrapper extends ModelElement {

	/**
	 *
	 */
	private static final long serialVersionUID = -6877572167930445087L;

	final private transient Object wrappedElement;

	/**
	 * @param wrappedElement
	 */
	public ModelElementWrapper(Object wrappedElement) {
		super(wrappedElement == null ? "" :
			wrappedElement.toString() == null || wrappedElement.toString().length() == 0 ?
					"schema" : wrappedElement.toString());
		if (wrappedElement == null) throw new NullPointerException("parameter 'wrappedElement' must not be null");
		this.wrappedElement = wrappedElement;
	}

	/**
	 * @return the wrappedElement
	 */
	public Object getWrappedElement() {
		return wrappedElement;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElement#getId()
	 */
	@Override
	public String getId() {
		return new StringBuilder(getWrappedElement().getClass().getName().replace(".", "_").replace("$", "_")).append("_").append(getName().replace(".", "_").replace("$", "_").replace("-", "_")).toString();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementI#isGenerated()
	 */
	@Override
	public boolean isGenerated() {
		return true;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelElementI#getName()
	 */
	@Override
	public String getName() {
		if (wrappedElement == null) {
			return null;
		} else {
			return Integer.toString(wrappedElement.hashCode());
		}
	}

	/**
	 * The URL that is returned by this method can be use in generated files, e.g. in
	 * JavaDoc for Java source code, to make it easy for developers to quickly go to
	 * the model element's "home page" for the model element that triggered the
	 * generation of a file for the section of a file.
	 * 
	 * @return the URL that leads to the model element's "home page" in the modeling tool (might be null)
	 */
	public String getLink() {
		return null;
	}

	
	
	/**
	 * @return
	 */
	public String getModuleName() {
		return null;
	}
	
	/**
	 * Get the URL to the Module of this element
	 * 
	 * @return the URL that leads to the module
	 */
	public String getModuleLink() {
		return null;
	}

	/**
	 * @return
	 */
	public String getType() {
		return this.getClass().getSimpleName();
	}
	
	/**
	 * The URL that is returned by this method can be use in generated files, e.g. in
	 * JavaDoc for Java source code, to make it easy for developers to quickly go to
	 * the model element's "home page" for the model element that triggered the
	 * generation of a file for the section of a file.
	 * 
	 * @return the URL that leads to the model element's "home page" in the modeling tool (might be null
	 * @deprecated used {@link ModelElementWrapper#getLink()} insted
	 */
	@Deprecated
	public String getLinkToModelingTool() {
		return getLink();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getName().hashCode();
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ModelElementWrapper other = (ModelElementWrapper) obj;
		if (wrappedElement == null) {
			if (other.wrappedElement != null) {
				return false;
			}
		} else if (!wrappedElement.equals(other.wrappedElement)) {
			return false;
		}
		return true;
	}
}
