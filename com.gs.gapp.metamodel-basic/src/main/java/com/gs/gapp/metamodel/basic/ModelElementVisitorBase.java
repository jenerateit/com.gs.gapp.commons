/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.basic;

import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.VisitorState;

/**
 * @author hrr
 *
 */
public abstract class ModelElementVisitorBase implements ModelElementVisitorI {

	private final Set<ModelElement> visitedElements = new LinkedHashSet<>();
	
	/**
	 * Visit method for model elements. All elements will be checked for duplicated visits.
	 * 
	 * @param e the model element to visit
	 * @return the visitor state
	 * @see com.gs.gapp.metamodel.VisitorI#visit(java.lang.Object)
	 */
	@Override
	public final VisitorState visit(ModelElement e) {
		if (visitedElements.contains(e)) {
			return VisitorState.STOP_PATH;
		}
		visitedElements.add(e);
		return visitElement(e);
	}

	/**
	 * Client implementations for the visitor.
	 * 
	 * @param e the model element to visit
	 * @return the visitor state
	 */
	protected abstract VisitorState visitElement(ModelElement e);
}
