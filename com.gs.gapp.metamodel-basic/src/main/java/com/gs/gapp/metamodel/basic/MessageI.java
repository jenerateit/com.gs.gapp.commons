package com.gs.gapp.metamodel.basic;

import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.ModelValidatorI.MessageStatus;

public interface MessageI {
	
	public static final class MessageBuilder {
		private final MessageI message;
		private ModelElementI modelElement;
		private Object[] parameters;
		private Throwable throwable;
		
		private MessageBuilder(MessageI message) {
			this.message = message;
		}
		
		public MessageBuilder modelElement(ModelElementI modelElement) {
			this.modelElement = modelElement;
			return this;
		}
		
		public MessageBuilder parameters(Object...parameters) {
			this.parameters = parameters;
			return this;
		}
		
		public MessageBuilder throwable(Throwable throwable) {
			this.throwable = throwable;
			return this;
		}
		
		public Message build() {
			String messageText = null;
			if (parameters != null) {
			    messageText = String.format(message.getMessageText(), parameters);
			} else {
				messageText = message.getMessageText();
			}
			if (modelElement instanceof ModelElement) {
				ModelElement element = (ModelElement) modelElement;
				Object rootOriginatingElement = element.getRootOriginatingElement();
				if (rootOriginatingElement instanceof ModelElementWrapper) {
					ModelElementWrapper wrapper = (ModelElementWrapper) rootOriginatingElement;
					StringBuilder sb = new StringBuilder(messageText);
					sb.append(System.lineSeparator());
					sb.append("Check element '").append(wrapper.getName());
					
					if (wrapper.getModuleName() != null && !wrapper.getModuleName().isEmpty()) {
					    sb.append("' in module '").append(wrapper.getModuleName()).append("'");
					} else {
						sb.append("'");
					}
					if (wrapper.getLink() != null && !wrapper.getLink().isEmpty()) {
						sb.append(System.lineSeparator());
						sb.append(wrapper.getLink());
					}
					messageText = sb.toString();
				}
			}
			return new Message(message.getStatus(), messageText, throwable);
		}
	}
	
	
	/**
	 * ERROR, WARNING or INFO
	 * 
	 * @return
	 */
	MessageStatus getStatus();
	
	/**
	 * A short code, indicating which organization
	 * a message belongs to, e.g. 'GS' for Generative Software.
	 * An organization has to organize its messages to have
	 * unique qualified ids, e.g. 'GS-JAVA-0001'. 
	 * 
	 * @return
	 */
	String getOrganization();
	
	/**
	 * A section is used by organizations to categorize their
	 * messages. Section names have to be unique within the context
	 * of an organization.
	 * 
	 * @return
	 */
	String getSection();
	
	/**
	 * Alphanumeric id that is unique within the context of an organization
	 * and section.
	 * 
	 * @return
	 */
	String getId();
	
	/**
	 * Creates a combined id by using organization, section and id.
	 * 
	 * @return
	 */
	default String getQualifiedId() {
		return getOrganization() + "-" + getSection() + "-" + getId();
	}
	
	/**
	 * The message text describing what went wrong or might become a problem.
	 * 
	 * @return
	 */
	String getProblemDescription();
	
	/**
	 * An optional information that tells a user what she can do to solve the reported issue.
	 * 
	 * @return
	 */
	String getInstruction();
	
	/**
	 * Creates a combined message text that contains all information that
	 * is available. Use this method to get the text that you are going to
	 * display in a log output or a user interface.
	 * 
	 * @return
	 */
	default String getMessageText() {
		return getQualifiedId() + ": " + getProblemDescription() +
				(getInstruction() != null && getInstruction().length() > 0 ?
						System.lineSeparator() + getInstruction() : "");
	}
	
	/**
	 * @param args
	 * @return
	 * @deprecated use the Method {@link MessageI#getMessageBuilder()} instead and also add the model element
	 */
	@Deprecated
	default Message getMessage(Object...args) {
		return getMessageBuilder()
				.parameters(args)
				.build();
	}
	
	/**
	 * @param th
	 * @param args
	 * @return
	 * @deprecated use the Method {@link MessageI#getMessageBuilder()} instead and also add the model element
	 */
	@Deprecated
	default Message getMessage(Throwable th, Object...args) {
		return getMessageBuilder()
				.parameters(args)
				.throwable(th)
				.build();
	}
	
	default MessageBuilder getMessageBuilder() {
		return new MessageBuilder(this);
	}
}
