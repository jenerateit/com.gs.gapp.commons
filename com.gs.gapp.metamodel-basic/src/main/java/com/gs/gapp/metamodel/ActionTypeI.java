package com.gs.gapp.metamodel;

import com.gs.gapp.metamodel.basic.NameableI;

/**
 * A class implementing this interface nominates an action type that can be used for testing
 * purposes, e.g. for ui testing or service testing. Examples for action types are: CLICK, ENTER, CALL. 
 * 
 * @author mmt
 *
 */
public interface ActionTypeI extends NameableI {
	
	/**
	 * If an enum implements the interfaces, getName() normally returns the enum entry's name.
	 * The name that is retured for an action type should not be subject to change in the future
	 * since modeling tools/languages depend on that name. 
	 * 
	 * @return
	 */
	String getName();

}
