/**
 *
 */
package com.gs.gapp.metamodel.basic.options;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.BasicMessage;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;


/**
 * @author mmt
 *
 */
public class OptionDefinition<T extends Serializable> extends ModelElement {

	private static final long serialVersionUID = 6622878831954234343L;

	private final String key;
	private final String description;
	private final Boolean mandatory;
	private final Boolean multivalued;
	private final T defaultValue;
	private final List<T> listOfValues = new ArrayList<>();

	private String namespace;

	/**
	 * @param key
	 */
	public OptionDefinition(String key) {
		this(key, "no description available");
	}
	
	/**
	 * @param key
	 * @param description
	 */
	public OptionDefinition(String key, String description) {
		this(key, description, false, false);
	}

    /**
     * @param key
     * @param description
     * @param mandatory
     */
    public OptionDefinition(String key, String description, boolean mandatory) {
		this(key, description, mandatory, false);
	}

    /**
     * @param key
     * @param description
     * @param mandatory
     * @param multivalued
     */
    public OptionDefinition(String key, String description, boolean mandatory, boolean multivalued) {
		this(key, description, mandatory, null, null, multivalued);
	}

    /**
     * @param key
     * @param description
     * @param mandatory
     * @param defaultValue
     */
    public OptionDefinition(String key, String description, boolean mandatory, T defaultValue) {
		this(key, description, mandatory, defaultValue, null, false);
	}

    /**
     * @param key
     * @param description
     * @param defaultValue
     */
    public OptionDefinition(String key, String description, T defaultValue) {
		this(key, description, false, defaultValue);
	}
    
	/**
	 * @param key
	 * @param description
	 * @param mandatory
	 * @param defaultValue
	 * @param listOfValues
	 */
	public OptionDefinition(String key, String description, boolean mandatory, T defaultValue, List<T> listOfValues) {
		this(key, description, mandatory, defaultValue, listOfValues, false);
	}


	/**
	 * @param key
	 * @param description
	 * @param mandatory
	 * @param defaultValue
	 * @param listOfValues
	 * @param multivalued
	 */
	public OptionDefinition(String key, String description, boolean mandatory, T defaultValue, List<T> listOfValues, boolean multivalued) {
		super(key);
		this.key = key;
		this.description = description;
		this.mandatory = mandatory;
		this.multivalued = multivalued;
		this.defaultValue = defaultValue;
		if (listOfValues != null) {
		    this.listOfValues.addAll(listOfValues);
		}
	}

	/**
	 * @return the key
	 */
	public final String getKey() {
		return key;
	}

	/**
	 * @return the mandatory
	 */
	public final Boolean getMandatory() {
		return mandatory;
	}

	/**
	 * @return the defaultValue
	 */
	public final T getDefaultValue() {
		return defaultValue;
	}

	/**
	 * @return the listOfValues
	 */
	public final List<T> getListOfValues() {
		return Collections.unmodifiableList(listOfValues);
	}
	
	/**
	 * @param modelElement
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public OptionDefinition<T>.OptionValue extractOptionValue(ModelElement modelElement) {
		OptionDefinition<T>.OptionValue result = null;
		
		for (OptionDefinition<?>.OptionValue value : modelElement.getOptions()) {
			if (value.getOptionDefinition().equals(this)) {
//				result = (OptionDefinition<T>.OptionValue) value;
				result = (OptionValue) value;
				break;
			}
		}
		
		return result;
	}
	
	/**
	 * @param rawOptionValue
	 * @return the option value, if rawOptionValue is null _and_ the definition has a default value, that default value is returned
	 */
	public T convertToTypedOptionValue(Serializable rawOptionValue) {
		return null;
	}
	
//	public T convertToTypedOptionValue(Serializable rawOptionValue) {
//		T result = null;
//		
//		if (rawOptionValue != null) {
//			
//			Class<?> type = getType();
//			
//			
//			if (Integer.class == type) {
//				@SuppressWarnings("unchecked")
//				T integerResult = (T) new Integer( Integer.parseInt(rawOptionValue.toString()) );
//				result = integerResult;
//			} else if (Long.class == type) {
//				@SuppressWarnings("unchecked")
//				T longResult = (T) new Long( Long.parseLong(rawOptionValue.toString()) );
//				result = longResult;
//			} else if (Float.class == type) {
//				@SuppressWarnings("unchecked")
//				T floatResult = (T) new Float( Float.parseFloat(rawOptionValue.toString()) );
//				result = floatResult;
//			} else if (Double.class == type) {
//				@SuppressWarnings("unchecked")
//				T doubleResult = (T) new Double( Double.parseDouble(rawOptionValue.toString()) );
//				result = doubleResult;
//			} else if (Boolean.class == type) {
//				@SuppressWarnings("unchecked")
//				T booleanResult = (T) new Boolean( Boolean.parseBoolean(rawOptionValue.toString()) );
//				result = booleanResult;
//			} else if (String.class == type) {
//				@SuppressWarnings("unchecked")
//				T stringResult = (T) rawOptionValue.toString();
//				result = stringResult;
//			} else {
//				throw new RuntimeException("convertToTypedOptionValue() called for a type '" + type + "' where there is no conversion implemented for");
//			}
//		} else if (this.getDefaultValue() != null) {
//			result = this.getDefaultValue();
//		}
//		
//		return result;
//	}

	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Class<T> getType() {
		Class<T> result = null;

		Type genericClass = this.getClass().getGenericSuperclass();
		
		if (genericClass instanceof ParameterizedType) {
			ParameterizedType parameterizedType = (ParameterizedType) genericClass;
			Type actualTypeArgument = parameterizedType.getActualTypeArguments()[0];
			if (actualTypeArgument instanceof TypeVariable) {
				result = (Class<T>) ((TypeVariable<?>) actualTypeArgument).getBounds()[0];
			} else if (actualTypeArgument instanceof Class<?>) {
				result = (Class<T>) actualTypeArgument;
			}
		}

		return result;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result
				+ ((namespace == null) ? 0 : namespace.hashCode());
		result = prime * result
				+ ((getType() == null) ? 0 : getType().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OptionDefinition<?> other = (OptionDefinition<?>) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (namespace == null) {
			if (other.namespace != null)
				return false;
		} else if (!namespace.equals(other.namespace))
			return false;
		
		if (this.getType() != other.getType()) {
			return false;
		}
			
		return true;
	}

	/**
	 * @return the namespace
	 */
	public String getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}



	/**
	 * @return the multivalued
	 */
	public Boolean getMultivalued() {
		return multivalued;
	}



	public String getDescription() {
		return description;
	}

	


	/**
	 * An instance of this class can hold 0, 1 or several option values for a given option definition.
	 * 
	 * <p>Note that once there is an instance of this class already existing, you can still
	 * add more values to the instance, by calling {@link #addOptionValue(Serializable)}.
	 *
	 */
	public class OptionValue extends ModelElement {

		private static final long serialVersionUID = 7309857571158199383L;

		private final List<T> optionValues = new ArrayList<>();

		public OptionValue(List<T> optionValues) {
			super(OptionDefinition.this.getKey());
			if (optionValues != null) {
			    this.optionValues.addAll(optionValues);
			}
		}

		public OptionValue(T optionValue) {
			super(OptionDefinition.this.getKey());
			if (optionValue != null) {
			    this.optionValues.add(optionValue);
			}
		}

		/**
		 * @param value
		 */
		public void addOptionValue(T value) {
			if (this.getOptionDefinition().multivalued != null && this.getOptionDefinition().multivalued.booleanValue()) {
				this.optionValues.add(value);
			} else {
				if (this.optionValues.isEmpty()) {
			        this.optionValues.add(value);
				} else {
					// This is an attempt to add another option value when there already is one set
					// and there is at most one value allowed.
					Message errorMessage = BasicMessage.TOO_MANY_VALUES_FOR_OPTION.getMessageBuilder()
						.modelElement(getOptionDefinition())
						.parameters(getOptionDefinition().getName())
						.build();
					throw new ModelConverterException(errorMessage.getMessage());
				}
			}
		}
		
		/**
		 * 
		 */
		public void clearOptionValues() {
			this.optionValues.clear();
		}
		
		/**
		 * @param value
		 */
		public void setOptionValue(T value) {
			if (this.getOptionDefinition().multivalued != null && this.getOptionDefinition().multivalued.booleanValue()) {
				// the option can have multiple vales set => keep the previously existing values
				addOptionValue(value);
			} else {
				this.clearOptionValues();
				addOptionValue(value);
			}
		}

		/**
		 * @return the optionDefinition
		 */
		public OptionDefinition<T> getOptionDefinition() {
			return OptionDefinition.this;
		}

		/**
		 * @return the optionValue
		 */
		public List<T> getOptionValues() {
			return Collections.unmodifiableList(optionValues);
		}

		/**
		 * If the option definition has more than one option value set, it returns the very first of the values.
		 * 
		 * @return a single option value
		 */
		public T getOptionValue() {
			if (optionValues != null && optionValues.size() > 0) {
				return optionValues.get(0);
			}
			return null;
		}
	}
}
