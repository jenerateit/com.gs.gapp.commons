/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Marcus Munzert
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.basic;

/**
 * @author mmt
 *
 */
public interface DocumentationI {

	/**
	 * Setter for the documentation body
	 * 
	 * @param body the documentation
	 */
	void setBody(String body);
	
	/**
	 * Getter for the documentation
	 * 
	 * @return the documentation
	 */
	String getBody();
}
