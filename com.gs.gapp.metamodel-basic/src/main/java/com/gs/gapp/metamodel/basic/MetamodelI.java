package com.gs.gapp.metamodel.basic;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * Implementations of this interface provide information about all metatypes
 * of a metamodel.
 * 
 * @author marcu
 *
 */
public interface MetamodelI {
	
	/**
	 * @return
	 */
	Collection<Class<? extends ModelElementI>> getMetatypes();
	
	/**
	 * @return
	 */
	Collection<Class<? extends ModelElementI>> getMetatypesForConversionCheck();
	
	/**
	 * @param metatype
	 * @return
	 */
	boolean isIncluded(Class<? extends ModelElementI> metatype);

	/**
	 * @param metatype
	 * @return
	 */
	boolean isExtendingOneOfTheMetatypes(Class<? extends ModelElementI> metatype);
	
	/**
	 * @param metatype
	 * @return
	 */
	default boolean isConversionChecked(Class<? extends ModelElementI> metatype) {
		return isIncluded(metatype) && getMetatypesForConversionCheck().contains(metatype);
	}
	
	/**
	 * @param metatype
	 * @return
	 */
	default Class<? extends Module> getModuleType(Class<? extends ModelElementI> metatype) {
		return null;
	}
	
	/**
	 * This method reuturns those metatypes, that should not be passed on
	 * to model converters or generation groups.
	 * 
	 * <p>The purpose of this method is not to let a generator create a different output
	 * but to prevent the generator from doing superfluous things. This saves time, memory
	 * and energy.
	 * 
	 * @return
	 */
	default Set<Class<?>> getMetaTypesForIndirectProcessing() {
		return Collections.emptySet();
	}
}
