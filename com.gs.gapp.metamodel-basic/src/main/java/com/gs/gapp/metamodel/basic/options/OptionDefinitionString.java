/**
 *
 */
package com.gs.gapp.metamodel.basic.options;

import java.io.Serializable;
import java.util.List;


/**
 * @author mmt
 *
 */
public class OptionDefinitionString extends OptionDefinition<String> {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6424952350985443150L;

	
	public OptionDefinitionString(String key) {
		super(key);
	}

	public OptionDefinitionString(String key, String description,
			boolean mandatory, String defaultValue,
			List<String> listOfValues, boolean multivalued) {
		super(key, description, mandatory, defaultValue, listOfValues, multivalued);
	}

	public OptionDefinitionString(String key, String description,
			boolean mandatory, String defaultValue, List<String> listOfValues) {
		super(key, description, mandatory, defaultValue, listOfValues);
	}

	public OptionDefinitionString(String key, String description,
			boolean mandatory, boolean multivalued) {
		super(key, description, mandatory, multivalued);
	}

	public OptionDefinitionString(String key, String description,
			boolean mandatory, String defaultValue) {
		super(key, description, mandatory, defaultValue);
	}

	public OptionDefinitionString(String key, String description,
			boolean mandatory) {
		super(key, description, mandatory);
	}

	public OptionDefinitionString(String key, String description,
			String defaultValue) {
		super(key, description, defaultValue);
	}

	public OptionDefinitionString(String key, String description) {
		super(key, description);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.OptionDefinition#convertToTypedOptionValue(java.io.Serializable)
	 */
	@Override
	public String convertToTypedOptionValue(Serializable rawOptionValue) {
		if (rawOptionValue != null) {
		    return rawOptionValue.toString();
		} else {
			return getDefaultValue();
		}
	}
}
