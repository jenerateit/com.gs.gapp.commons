/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.basic;

import com.gs.gapp.metamodel.VisitorI;


/**
 * @author mmt
 *
 */
public interface ModelElementTreeVisitorI extends VisitorI<ModelElement> {

}
