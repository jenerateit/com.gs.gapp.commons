/**
 *
 */
package com.gs.gapp.metamodel.basic.options;

import java.io.Serializable;
import java.util.List;


/**
 * @author mmt
 *
 */
public class OptionDefinitionDouble extends OptionDefinition<Double> {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6424952350985443150L;

	
	public OptionDefinitionDouble(String key) {
		super(key);
	}

	public OptionDefinitionDouble(String key, String description,
			boolean mandatory, Double defaultValue,
			List<Double> listOfValues, boolean multivalued) {
		super(key, description, mandatory, defaultValue, listOfValues, multivalued);
	}

	public OptionDefinitionDouble(String key, String description,
			boolean mandatory, Double defaultValue, List<Double> listOfValues) {
		super(key, description, mandatory, defaultValue, listOfValues);
	}

	public OptionDefinitionDouble(String key, String description,
			boolean mandatory, boolean multivalued) {
		super(key, description, mandatory, multivalued);
	}

	public OptionDefinitionDouble(String key, String description,
			boolean mandatory, Double defaultValue) {
		super(key, description, mandatory, defaultValue);
	}

	public OptionDefinitionDouble(String key, String description,
			boolean mandatory) {
		super(key, description, mandatory);
	}

	public OptionDefinitionDouble(String key, String description,
			Double defaultValue) {
		super(key, description, defaultValue);
	}

	public OptionDefinitionDouble(String key, String description) {
		super(key, description);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.OptionDefinition#convertToTypedOptionValue(java.io.Serializable)
	 */
	@Override
	public Double convertToTypedOptionValue(Serializable rawOptionValue) {
		if (rawOptionValue != null) {
		    Double result = new Double( Double.parseDouble(rawOptionValue.toString()) );
		    return result;
		} else {
			return getDefaultValue();
		}
	}
}
