/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel.basic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.AcceptState;

/**
 * @author hrr
 *
 */
public class Module extends ModelElement {

	/*
	 * serial version UID
	 */
	private static final long serialVersionUID = -164486315360531277L;

	private final Set<ModelElement> elements = new LinkedHashSet<>();

	private Namespace namespace;

	/**
	 * Constructor.
	 *
	 * @param name the name of this element
	 */
	public Module(String name) {
		super(name);
	}

	/**
	 * Add an element to this module.
	 *
	 * @param e the element to add
	 * @return true if this set did not already contain the specified element
	 */
	public boolean addElement(ModelElement e) {
		if (e instanceof ModelElement) {
			ModelElement modelElement = e;
			if (modelElement.isInGlobalCache()) {
				return true;  // a model element that is meant to be in a global cache cannot be a member of a model at the same time since we do not have the concept of global models (mmt 27-Aug-2018)
			}
		}

		
		boolean add = this.elements.add(e);
		if (e.getModule() == null &&
				(this.getClass().getPackage() == e.getClass().getPackage() || e.getClass().getPackage().getName().startsWith(this.getClass().getPackage().getName())) ) {
		    e.setModule(this);
		}
		return add;
	}

	/**
	 * Getter for the elements in this module.
	 *
	 * @return the elements
	 */
	public Set<ModelElement> getElements() {
		return elements;
	}

	/**
	 * A convenience method to get all elements that are instances of the given type
	 * and are in the module's set of model elements.
	 *
	 * @param resultType
	 * @return
	 */
	public final <T extends ModelElement> Set<T> getElements(Class<T> resultType) {
		Set<T> elements = new LinkedHashSet<>();

		for (ModelElement element : getElements()) {
			if (resultType.isAssignableFrom(element.getClass())) {
				elements.add(resultType.cast(element));
			}
		}

		return elements;
	}
	
	/**
	 * A convenience method to get all elements that are instances of the given type
	 * and are in the model's set of model elements. The collection of model elements
	 * are returned in their natural ordering.
	 *
	 * @param resultType
	 * @return
	 */
	public final <T extends ModelElementI> Set<T> getElementsSorted(Class<T> resultType) {
		ArrayList<T> elementsForSorting = new ArrayList<>();

		for (ModelElementI element : getElements()) {
			if (resultType.isAssignableFrom(element.getClass())) {
				elementsForSorting.add(resultType.cast(element));
			}
		}

		Collections.sort(elementsForSorting);
		Set<T> elements = new LinkedHashSet<>(elementsForSorting);
		
		return elements;
	}

	/**
	 * @param e
	 */
	public void removeElement(ModelElement e) {
		if (this.elements.remove(e)) {
		    e.setModule(null);
		}
	}


	/**
	 * Visit method to accept all elements.
	 *
	 * @param visitor the visitor
	 * @return the visitor result
	 * @see com.gs.gapp.metamodel.basic.ModelElement#visitChilds(com.gs.gapp.metamodel.basic.ModelElementVisitorI)
	 */
	@Override
	protected AcceptState visitChilds(ModelElementVisitorI visitor) {
		if (super.visitChilds(visitor) == AcceptState.CONTINUE) {
			for (ModelElement e : this.elements) {
				if (e.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}
			return AcceptState.CONTINUE;
		}
		return AcceptState.INTERRUPT;
	}

	/**
	 * Compares the super class and the elements.
	 *
	 * @param element the element to compare to
	 * @return a negative integer, zero, or a positive integer as this object is less than, equal to,
	 * or greater than the specified object.
	 * @see ModelElement#compareTo(ModelElementI)
	 */
	@Override
	public int compareTo(ModelElementI element) {
		int comp = super.compareTo(element);
		if (comp == 0) {
			if (Module.class.isInstance(element)) {
				Module m = Module.class.cast(element);
				return this.elements.size() - m.elements.size();
			}
		}
		return comp;
	}

	/**
	 * @return the namespace
	 */
	public Namespace getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(Namespace namespace) {
		this.namespace = namespace;
	}
	
	/**
	 * Combines the namespace with the module name, separated by a '.'.
	 * @return
	 */
	public String getQualifiedName() {
		return new StringBuilder(namespace != null ? namespace.getName() : "").append(".").append(getName()).toString();
	}
}
