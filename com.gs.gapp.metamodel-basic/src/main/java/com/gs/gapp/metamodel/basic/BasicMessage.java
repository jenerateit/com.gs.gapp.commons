package com.gs.gapp.metamodel.basic;

import com.gs.gapp.metamodel.basic.ModelValidatorI.MessageStatus;

public enum BasicMessage implements MessageI {

	// ---------------------------------------------------
	// ------ ERRORS -------------------------------------
	UNHANDLED_ENUM_ENTRY (MessageStatus.ERROR, "0001",
			"The generator has a problem since it doesn't use that most actual version of a generation component. This most likely is a bug in the generator.",
			"Please contact the support of the generator's producer and provide it with the following information: enum entry '%s' of enum '%s' is not handled in class '%s'."),
	UNKNOWN_OPTION_VALUE (MessageStatus.ERROR, "0002",
			"The provieded value '%s' for the generator option '%s' is invalid. These are the valid values for the option:%s",
			"Please change the option value in your configuration by choosing a valid one."),
	OPTION_VALUE_IS_NOT_A_NUMBER (MessageStatus.ERROR, "0003",
			"The provieded value '%s' for the generator option '%s' is not a number.",
			"Please provide a number for the option."),
	MISSING_CONVERTER_OPTIONS (MessageStatus.ERROR, "0004",
			"The generator can't process some generator options. This most likely is a bug in the generator.",
			"Please contact the support of the generator's producer and provide it with the following information: the options object of type '%s' could not be found in '%s'."),
	MISSING_GENERATION_GROUP_OPTIONS (MessageStatus.ERROR, "0005",
			"The generator can't process some generator options. This most likely is a bug in the generator.",
			"Please contact the support of the generator's producer and provide it with the following information: the options object of type '%s' could not be found in '%s'."),
	INCORRECT_MODEL_VALIDATION_EXPRESSION (MessageStatus.ERROR, "0006",
			"Found an incorrect validation rule expression '%s' being used.",
			"Please contact the support of the generator's producer and provide it with the following information: The validation expression '%s' that has been applied in a generator is incorrrect. Exception: %s"),
	INCORRECT_MODEL_FILTER_EXPRESSION (MessageStatus.ERROR, "0007",
			"Found an incorrect filter expression '%s' being used.",
			"Please contact the support of the generator's producer and provide it with the following information: The filter expression '%s' that has been applied in a generator is incorrrect. Exception: %s"),
	TARGET_FILE_PATH_CONTAINS_INVALID_CHARACTERS (MessageStatus.ERROR, "0008",
			"The path or name of the to-be-generated file '%s' contains invalid characters (not allowed in a URI). At index %s an invalid character has been found. A frequent reason for this is when the path or name contains blanks.",
			"Please check the path mappings for virtual projects and/or the names of model elements that contain the invalid character and correct them."),
	TOO_MANY_VALUES_FOR_OPTION (MessageStatus.ERROR, "0009",
			"Detected an attempt to add another option value for the option '%s' when there already is one set. That option only allows one single value to be set.",
			"Please contact the support of the generator's producer and provide it with that information."),
	NO_NAME_PROVIDED_FOR_A_MODEL_ELEMENT (MessageStatus.ERROR, "0010",
			"Detected an attempt to create a model elment with an empty name during the generator's internal processing (element type: %s).",
			"Please contact the support of the generator's producer and provide it with that information."),
	ATTEMPT_TO_ENABLE_GENERATION_FOR_A_NON_GENERATED_ELEMENT (MessageStatus.ERROR, "0011",
			"Detected an attempt to enable the generation for a model element where the generation was disabled before. This happened during the generator's internal processing (element type: %s).",
			"Please contact the support of the generator's producer and provide it with that information."),
	ATTEMPT_TO_RESET_MODULE_TO_NULL (MessageStatus.ERROR, "0012",
			"Detected an attempt to reset the module of element '%s' where that element already had a module set before (module was: %s).",
			"Please contact the support of the generator's producer and provide it with that information."),
	ATTEMPT_TO_OVERWRITE_MODULE (MessageStatus.ERROR, "0013",
			"Detected an attempt to overwrite the module '%s' of element '%s' to module '%s'.",
			"Please contact the support of the generator's producer and provide it with that information."),
	AMBIGUOUS_ELEMENT_CONVERSION_DETECTED (MessageStatus.ERROR, "0014",
			"More than one conversion result found for element '%s': '%s' and '%s' (desired type: %s). Converter details: %s",
			"Please contact the support of the generator's producer and provide it with that information."),
	AMBIGUOUS_DEEP_ELEMENT_CONVERSION_DETECTED (MessageStatus.ERROR, "0015",
			"More than one conversion result found for element '%s' (searching deeply): '%s' and '%s' (desired type: %s). Converter detail 1: %s, converter detail 2: %s",
			"Please contact the support of the generator's producer and provide it with that information."),
	ERROR_VALUE_TYPE_OF_OPTION_DEFINITION_COULD_NOT_BE_DETERMINED (MessageStatus.ERROR, "0016",
			"The value type of the option '%s' could not be determined.",
			"This indicates a bug in the generator. Please contact the producer of the generator and report this issue."),
	ERROR_VALUE_TYPE_OF_OPTION_DEFINITION_NOT_MATCHING_THE_REQUESTED_VALUE_TYPE (MessageStatus.ERROR, "0017",
			"The value type '%s' of the option '%s' is not compatible with the requested value type '%s'.",
			"This indicates a bug in the generator. Please contact the producer of the generator and report this issue."),
	
	// -----------------------------------------------------
    // ------ WARNINGS -------------------------------------
	/**
	 * Note that the third %s in the message text lets you add some additional information (or nothing).
	 */
	WARNING_MODEL_ELEMENT_NAME_TRUNCATED (MessageStatus.WARNING, "1001",
			"The original name of a model element has been truncated to a shorter name: '%s' ==> '%s'. %s",
			"The resulting name might cause trouble during the further processing of the generator."),
	
	// ---------------------------------------------------
    // ------ INFOS -------------------------------------
	
	;

	private final MessageStatus status;
	private final String organization = "GS";
	private final String section = "BASIC";
	private final String id;
	private final String problemDescription;
	private final String instruction;
	
	private BasicMessage(MessageStatus status, String id, String problemDescription, String instruction) {
		this.status = status;
		this.id = id;
		this.problemDescription = problemDescription;
		this.instruction = instruction;
	}
	
	@Override
	public MessageStatus getStatus() {
		return status;
	}

	@Override
	public String getOrganization() {
		return organization;
	}

	@Override
	public String getSection() {
		return section;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getProblemDescription() {
		return problemDescription;
	}
	
	@Override
	public String getInstruction() {
		return instruction;
	}
}
 