/**
 *
 */
package com.gs.gapp.metamodel.basic.options;

import java.io.Serializable;
import java.util.List;


/**
 * @author mmt
 *
 */
public class OptionDefinitionBoolean extends OptionDefinition<Boolean> {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6424952350985443150L;


	

	public OptionDefinitionBoolean(String key) {
		super(key);
	}

	public OptionDefinitionBoolean(String key, String description,
			boolean mandatory, Boolean defaultValue,
			List<Boolean> listOfValues, boolean multivalued) {
		super(key, description, mandatory, defaultValue, listOfValues, multivalued);
	}

	public OptionDefinitionBoolean(String key, String description,
			boolean mandatory, Boolean defaultValue, List<Boolean> listOfValues) {
		super(key, description, mandatory, defaultValue, listOfValues);
	}

	public OptionDefinitionBoolean(String key, String description,
			boolean mandatory, boolean multivalued) {
		super(key, description, mandatory, multivalued);
	}

	public OptionDefinitionBoolean(String key, String description,
			boolean mandatory, Boolean defaultValue) {
		super(key, description, mandatory, defaultValue);
	}

	public OptionDefinitionBoolean(String key, String description,
			boolean mandatory) {
		super(key, description, mandatory);
	}

	public OptionDefinitionBoolean(String key, String description,
			Boolean defaultValue) {
		super(key, description, defaultValue);
	}

	public OptionDefinitionBoolean(String key, String description) {
		super(key, description);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.OptionDefinition#convertToTypedOptionValue(java.io.Serializable)
	 */
	@Override
	public Boolean convertToTypedOptionValue(Serializable rawOptionValue) {
		if (rawOptionValue != null) {
		    Boolean result = new Boolean( Boolean.parseBoolean(rawOptionValue.toString()) );
		    return result;
		} else {
			return getDefaultValue();
		}
	}
}
