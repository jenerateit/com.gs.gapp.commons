package com.gs.gapp.metamodel.basic;

public class Info extends ModelElement {

	private static final long serialVersionUID = 3449407781048924628L;
	
	public Info(String name) {
		super(name);
	}
}
