/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel;

/**
 * Results a {@link VisitorI}'s {@link VisitorI#visit(Object)} method has.
 * 
 * @author hrr
 *
 */
public enum VisitorState {

	/**
	 * Go on with the next item.
	 */
	CONTINUE,
	
	/**
	 * Stop with this item (do not go deeper in the path) and go on with the next item.
	 */
	STOP_PATH,
	
	/**
	 * Stop visiting and return.
	 */
	INTERRUPT
}
