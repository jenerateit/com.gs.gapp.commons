package com.gs.gapp.metamodel.basic;

import java.util.Optional;

import org.jenerateit.modelconverter.ModelConverterException;

/**
 * A GeneratorInfo holds information about the generator
 * that is currently being executed. This information
 * can be written to any kind of generated file to simplify
 * the task of finding out, which generator version has been
 * used to generate something.
 * 
 * Note that this class is independent from the way the information
 * about a generator is being provided (generator option, Virtual Developer API, ...).
 * 
 * 
 * @author marcu
 *
 */
public class GeneratorInfo extends ModelElement {

	private static final long serialVersionUID = 1L;
	
	
	/**
	 * @param modelElement
	 * @return
	 */
	public static Optional<StringBuilder> getGeneratorInfo(ModelElement modelElement) {
		StringBuilder generatorInfoStringBuilder = null;
		if (modelElement.getModel() != null) {
			GeneratorInfo generatorInfo = modelElement.getModel().getLeadingGeneratorInfo();
			if (generatorInfo != null) {
				generatorInfoStringBuilder = new StringBuilder(System.lineSeparator())
						.append("Generator: ")
						.append(generatorInfo.getName())
						.append(":")
						.append(generatorInfo.getSemanticVersion().getMajor())
						.append(".")
						.append(generatorInfo.getSemanticVersion().getMinor());
			}
		}
		
		return Optional.ofNullable(generatorInfoStringBuilder);
	}
	
	/**
	 * @param modelElement
	 * @return
	 */
	public static StringBuilder formatGeneratorInfo(ModelElement modelElement) {
		StringBuilder result = new StringBuilder();
		
		Optional<StringBuilder> generatorInfoStringBuilder = getGeneratorInfo(modelElement);
		Object rootOriginatingElement = modelElement.getRootOriginatingElement();
		StringBuilder originStringBuilder = null;
		if (rootOriginatingElement instanceof ModelElementI) {
			ModelElementI rootModelElement = (ModelElementI) rootOriginatingElement;
			originStringBuilder = new StringBuilder(System.lineSeparator())
					.append(result.length() > 0 && generatorInfoStringBuilder == null ? System.lineSeparator() : "")
					.append("Model: ");
			
			if (rootModelElement instanceof ModelElementWrapper) {
				ModelElementWrapper modelElementWrapper = (ModelElementWrapper) rootModelElement;
				originStringBuilder.append("name=")
				    .append(modelElementWrapper.getName())
					.append(", type=")
					.append(modelElementWrapper.getType())
					.append(", module=")
				    .append(modelElementWrapper.getModuleName() == null ? "<none>" : modelElementWrapper.getModuleName());
			} else {
				originStringBuilder.append("name=")
					.append(rootModelElement.getName())
					.append(", type=")
					.append(rootModelElement.getClass().getSimpleName())
					.append(", module=")
					.append(rootModelElement.getModule() != null ? rootModelElement.getModule().getName() : "<none>");
			}
			
		}
		
		if (generatorInfoStringBuilder.isPresent()  || originStringBuilder != null) {
		    result.append(generatorInfoStringBuilder.isPresent() ? generatorInfoStringBuilder.get() : "")
		          .append(originStringBuilder == null ? "" : originStringBuilder);
		}
		
		return result;
	}
	
	/**
	 * @param modelElement
	 * @return
	 */
	public static Optional<String> getModelElementLink(ModelElement modelElement) {
		String result = null;
		Object rootOriginatingElement = modelElement.getRootOriginatingElement();
		if (rootOriginatingElement instanceof ModelElementI) {
			ModelElementI rootModelElement = (ModelElementI) rootOriginatingElement;
			if (rootModelElement instanceof ModelElementWrapper) {
				ModelElementWrapper modelElementWrapper = (ModelElementWrapper) rootModelElement;
				if (modelElementWrapper.getLink() != null) {
					result = modelElementWrapper.getLink();
				}
			}
		}
		
		return Optional.ofNullable(result);
	}
	
    /**
     * @param modelElement
     * @return
     */
    public static Optional<String> getModuleLink(ModelElement modelElement) {
    	String result = null;
		Object rootOriginatingElement = modelElement.getRootOriginatingElement();
		if (rootOriginatingElement instanceof ModelElementI) {
			ModelElementI rootModelElement = (ModelElementI) rootOriginatingElement;
			if (rootModelElement instanceof ModelElementWrapper) {
				ModelElementWrapper modelElementWrapper = (ModelElementWrapper) rootModelElement;
				if (modelElementWrapper.getModuleLink() != null) {
					result = modelElementWrapper.getModuleLink();
				}
			}
		}
		
		return Optional.ofNullable(result);
	}
	
	/**
	 * @param modelElement
	 * @return
	 */
	public static StringBuilder formatGeneratorInfoHtml(ModelElement modelElement) {
		StringBuilder result = new StringBuilder();

		StringBuilder generatorInfoStringBuilder = null;
		if (modelElement.getModel() != null) {
			GeneratorInfo generatorInfo = modelElement.getModel().getLeadingGeneratorInfo();
			if (generatorInfo != null) {
				generatorInfoStringBuilder = new StringBuilder()
						.append("<tr><td>Generator</td><td>")
						.append(generatorInfo.getName())
						.append(":")
						.append(generatorInfo.getSemanticVersion().getMajor())
						.append(".")
						.append(generatorInfo.getSemanticVersion().getMinor())
						.append("</td></tr>");
			}
		}
		
		Object rootOriginatingElement = modelElement.getRootOriginatingElement();
		StringBuilder originStringBuilder = null;
		if (rootOriginatingElement instanceof ModelElementI) {
			ModelElementI rootModelElement = (ModelElementI) rootOriginatingElement;
//			originStringBuilder = new StringBuilder(System.lineSeparator())
//					.append(result.length() > 0 && generatorInfoStringBuilder == null ? System.lineSeparator() : "")
//					.append("Model: ");
			String element;
			String type;
			String module;
			if (rootModelElement instanceof ModelElementWrapper) {
				ModelElementWrapper modelElementWrapper = (ModelElementWrapper) rootModelElement;
				if (modelElementWrapper.getLink() != null) {
					element = "<a href=\"" + modelElementWrapper.getLink() + "\">" + modelElementWrapper.getName() + "</a>";
				} else {
					element = modelElementWrapper.getName();
				}
				type = modelElementWrapper.getType();
				String moduleName = modelElementWrapper.getModuleName() == null ? "none" : modelElementWrapper.getModuleName();
				if (modelElementWrapper.getModuleLink() != null) {
					module = "<a href=\"" + modelElementWrapper.getModuleLink() + "\">" + moduleName + "</a>";
				} else {
					module = moduleName;
				}
			} else {
				element = rootModelElement.getName();
				type = rootModelElement.getClass().getSimpleName();
				module = rootModelElement.getModule() != null ? rootModelElement.getModule().getName() : "none";
			}
			
			

			originStringBuilder = new StringBuilder()
					.append("<tr><td>Element</td><td>")
					.append(element)
					.append("</td></tr>")
					.append(System.lineSeparator())
					.append("<tr><td>Type</td><td>")
					.append(type)
					.append("</td></tr>")
					.append(System.lineSeparator())
					.append("<tr><td>Module</td><td>")
					.append(module)
					.append("</td></tr>");
		}

		if (generatorInfoStringBuilder != null || originStringBuilder != null) {
			result.append("<table>")
					.append(System.lineSeparator());
			if (generatorInfoStringBuilder != null) {
				result.append(generatorInfoStringBuilder)
						.append(System.lineSeparator());
			}
			if (originStringBuilder != null) {
				result.append(originStringBuilder)
						.append(System.lineSeparator());
			}
			result.append("</table>");
		}
		
		
		return result;
	}
	
	/**
	 * @deprecated Use one of the other methods of {@link GeneratorInfo} to the the information about the modeling tool.
	 * @param modelElement
	 * @return
	 */
	@Deprecated
	public static String getLinkToModelingTool(ModelElement modelElement) {
		Object rootOriginatingElement = modelElement.getRootOriginatingElement();
		if (rootOriginatingElement instanceof ModelElementWrapper) {
			ModelElementWrapper modelElementWrapper = (ModelElementWrapper) rootOriginatingElement;
			if (modelElementWrapper.getLinkToModelingTool() != null && modelElementWrapper.getLinkToModelingTool().length() > 0) {
				return modelElementWrapper.getLinkToModelingTool();
			}
		}
        return null;
	}
	
	private final String versionString;
	private final org.osgi.framework.Version semanticVersion;

	public GeneratorInfo(String name, String versionString) {
		super(name);
		this.versionString = versionString;
		
		org.osgi.framework.Version parsedSemanticVersion = null;
		try {
			parsedSemanticVersion = org.osgi.framework.Version.parseVersion(this.versionString);
		} catch (Throwable th) {
			throw new ModelConverterException("given version string '" + versionString + "' is not a valid semantic version", th);
		}
		
		semanticVersion = parsedSemanticVersion;
	}

	public String getVersionString() {
		return versionString;
	}

	public org.osgi.framework.Version getSemanticVersion() {
		return semanticVersion;
	}
}
