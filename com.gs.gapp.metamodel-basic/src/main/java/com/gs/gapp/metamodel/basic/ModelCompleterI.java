package com.gs.gapp.metamodel.basic;

import java.util.Collection;

import javax.validation.constraints.NotNull;

import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;

/**
 * Classes that implement this interface have the purpose of
 * adding missing parts in the model that can be deduced from
 * other parts of the model.
 * 
 * @author marcu
 *
 */
public interface ModelCompleterI {

	/**
	 * Complete some model elements.
	 * 
	 * @param modelElements
	 * @return a collection of completion messages, normally these are WARN or INFO messages informing about completions
	 */
	@NotNull
	Collection<Message> complete(Collection<Object> modelElements);
}
