package com.gs.gapp.metamodel.basic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;

/**
 * @author marcu
 *
 */
public abstract class AbstractModelFilter implements ModelFilterI {

	/**
	 * <p>Calling this utility method from your filter implementation in {@link AbstractModelFilter#filter(Collection)}
	 * to make sure that there are not too many elements removed from the collection of elements that you process in your filter logic.
	 * At the time of writing this documentation only Namespace types are meant to be filtered.
	 * 
	 * @param modelElements
	 * @return
	 */
	public static Collection<?> extractMandatoryElements(Collection<?> modelElements) {
		Set<?> namespaces = modelElements.stream()
			.filter(modelElement -> modelElement != null)
		    .filter(modelElement -> Namespace.class.isAssignableFrom(modelElement.getClass()))
		    .collect(Collectors.toCollection(LinkedHashSet::new));
		
		return namespaces;
	}
	
	/**
	 * Convenience method to throw a {@link ModelConverterException} in case an enum entry
	 * should have been handled in a switch but in fact wasn't. Such a situation indicates
	 * a bug, e.g. generator components with semantically incompatible versions.
	 * 
	 * @param enumEntry
	 */
	protected void throwUnhandledEnumEntryException(Enum<?> enumEntry) {
	    Message errorMessage = BasicMessage.UNHANDLED_ENUM_ENTRY
			.getMessageBuilder()
			.parameters(enumEntry.name(), enumEntry.getClass().getSimpleName(), this.getClass().getSimpleName())
			.build();
        throw new ModelConverterException(errorMessage.getMessage());
	}

	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.ModelFilterI#filter(java.util.Collection)
	 */
	@Override
	public Collection<?> filter(Collection<?> modelElements) {
		return modelElements;
	}
	
	/**
	 * @param clazz
	 * @param modelElements
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected <T> Collection<T> getElementsForMetatype(Class<T> clazz, Collection<?> modelElements) {
		Collection<T> result = new ArrayList<>();
		
		List<?> collectedElements = modelElements.stream().filter(element -> element.getClass() == clazz).collect(Collectors.toList());
		for (Object collectedElement : collectedElements) result.add((T) collectedElement);
		
		return result;
	}
}
