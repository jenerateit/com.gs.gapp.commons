/**
 *
 */
package com.gs.gapp.metamodel.basic.options;

import java.io.Serializable;
import java.util.List;


/**
 * @author mmt
 *
 */
public class OptionDefinitionLong extends OptionDefinition<Long> {


	/**
	 * 
	 */
	private static final long serialVersionUID = -6424952350985443150L;

	
	public OptionDefinitionLong(String key) {
		super(key);
	}

	public OptionDefinitionLong(String key, String description,
			boolean mandatory, Long defaultValue,
			List<Long> listOfValues, boolean multivalued) {
		super(key, description, mandatory, defaultValue, listOfValues, multivalued);
	}

	public OptionDefinitionLong(String key, String description,
			boolean mandatory, Long defaultValue, List<Long> listOfValues) {
		super(key, description, mandatory, defaultValue, listOfValues);
	}

	public OptionDefinitionLong(String key, String description,
			boolean mandatory, boolean multivalued) {
		super(key, description, mandatory, multivalued);
	}

	public OptionDefinitionLong(String key, String description,
			boolean mandatory, Long defaultValue) {
		super(key, description, mandatory, defaultValue);
	}

	public OptionDefinitionLong(String key, String description,
			boolean mandatory) {
		super(key, description, mandatory);
	}

	public OptionDefinitionLong(String key, String description,
			Long defaultValue) {
		super(key, description, defaultValue);
	}

	public OptionDefinitionLong(String key, String description) {
		super(key, description);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.options.OptionDefinition#convertToTypedOptionValue(java.io.Serializable)
	 */
	@Override
	public Long convertToTypedOptionValue(Serializable rawOptionValue) {
		if (rawOptionValue != null) {
		    Long result = new Long( Long.parseLong(rawOptionValue.toString()) );
		    return result;
		} else {
			return getDefaultValue();
		}
	}
}
