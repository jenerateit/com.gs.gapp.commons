package com.gs.gapp.metamodel.basic;

import java.util.Collection;
import java.util.LinkedHashSet;

import javax.validation.constraints.NotNull;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;

/**
 * Classes that implement this interface have the purpose of
 * removing some elements from a given collection of model elements.
 * Such functionality typically is used in model converters.
 * 
 * @author marcu
 *
 */
public interface ModelFilterI {

	/**
	 * Remove some elements from the passed collection of model elements.
	 * 
	 * @param modelElements
	 * @return a collection that has all or a subset of the passed collection of elements
	 */
	@NotNull
	Collection<?> filter(Collection<?> modelElements);
	
	/**
	 * @param modelElements
	 * @param rules
	 * @return
	 */
	@NotNull
	default Collection<?> filter(Collection<?> modelElements, Collection<RuleI> rules) {
        Collection<Object> result = new LinkedHashSet<>();
		
		if (modelElements != null && rules != null) {
			for (RuleI rule : rules) {
				result.addAll(rule.filter(modelElements));
			}
		}
			
		return result;
	}
	
	/**
	 * @author marcu
	 *
	 */
	public static interface RuleI {
		Collection<?> filter(Collection<?> modelElements);
	}
	
	/**
	 * @author marcu
	 *
	 */
	public static class RuleElementsByNameAndType implements RuleI {
		
		/**
		 * Creates a collection of rules by parsing an expression of this type:
		 * [simple type name]=[min occurrence],[max occurrence];[simple type name]=[min occurrence],[max occurrence]; ...
		 * 
		 * @param expression
		 * @return
		 */
		public static Collection<RuleI> createRulesFromExpression(String expression) {
			Collection<RuleI> result = new LinkedHashSet<>();
			
			if (expression != null && expression.length() > 0) {
				try {
					String[] rulesAsStrings = expression.split(";");
					for (String ruleAsString : rulesAsStrings) {
						String[] simpleTypeNameAndElementName = ruleAsString.split("=");
						String simpleTypeName = simpleTypeNameAndElementName[0];
						
						if (simpleTypeNameAndElementName.length > 1 && simpleTypeNameAndElementName[1] != null && simpleTypeNameAndElementName[1].length() > 0) { 
							String elementName = simpleTypeNameAndElementName[1];
							RuleElementsByNameAndType rule = new RuleElementsByNameAndType(simpleTypeName, elementName);
							result.add(rule);
						}
					}
				} catch (Throwable th) {
					Message errorMessage = BasicMessage.INCORRECT_MODEL_FILTER_EXPRESSION.getMessageBuilder()
						    .parameters(expression, th.getMessage())
						    .build();
					throw new ModelConverterException(errorMessage.getMessage());
				}
			}
			
			return result;
		}
		
		private final String simpleTypeName;
		private final String elementName;
		
		/**
		 * @param simpleTypeName
		 * @param elementName
		 */
		RuleElementsByNameAndType(String simpleTypeName, String elementName) {
			this.simpleTypeName = simpleTypeName;
			this.elementName = elementName.toLowerCase();
		}
		
		public String getSimpleTypeName() {
			return simpleTypeName;
		}

		public String getElementName() {
			return elementName;
		}

		/* (non-Javadoc)
		 * @see com.gs.gapp.metamodel.basic.ModelFilterI.RuleI#filter(java.util.Collection)
		 */
		@Override
		public Collection<?> filter(Collection<?> modelElements) {
			Collection<Object> elementsToBeRemoved = new LinkedHashSet<>();
			for (Object obj : modelElements) {
				if (obj instanceof ModelElementI) {
					ModelElementI modelElement = (ModelElementI) obj;
					if (modelElement.getClass().getSimpleName().equalsIgnoreCase(simpleTypeName)) {
						if (elementName.equalsIgnoreCase(modelElement.getName()) || modelElement.getName().matches(elementName)) {
							// element found that should be used, do nothing
						} else {
							elementsToBeRemoved.add(modelElement);
						}
					}
				}
			}

			Collection<Object> result = new LinkedHashSet<>(modelElements);
			if (elementsToBeRemoved.size() == 0) {
				// leave the result as is
			} else {
				result.removeAll(elementsToBeRemoved);
			}
			
			return result;
		}
	}
	
	/**
	 * @author marcu
	 *
	 */
	public static final class DefaultFilter implements ModelFilterI {

		@Override
		public Collection<?> filter(Collection<?> modelElements) {
			return filter(modelElements, null);
		}
	}
}
