package com.gs.gapp.metamodel.basic;

import java.util.LinkedHashSet;
import java.util.Set;

public class Version extends ModelElement {

	private static final long serialVersionUID = -7227139284719470055L;
	
	private final String versionNumber;
	
	private final Set<ModelElement> modelElements = new LinkedHashSet<>();
	
	private final Set<ModelElement> modelElementsSince = new LinkedHashSet<>();
	
	private final Set<ModelElement> modelElementsDeprecatedSince = new LinkedHashSet<>();
	
	private final org.osgi.framework.Version semanticVersion;
	
	private final Long numericVersion;

	/**
	 * @param name
	 * @param versionNumber
	 */
	public Version(String name, String versionNumber) {
		super(name);
		this.versionNumber = versionNumber;
		
		org.osgi.framework.Version parsedSemanticVersion = null;
		try {
			parsedSemanticVersion = org.osgi.framework.Version.parseVersion(this.versionNumber);
		} catch (Throwable th) {/*intentionally ignore this*/}
		semanticVersion = parsedSemanticVersion;
		
		Long parsedNumericVersion = null;
		if (getSemanticVersion() == null) {
			// try to use a numeric version
			try {
			    parsedNumericVersion = Long.parseLong(this.versionNumber);
			} catch (Throwable th) {/*intentionally ignore this*/}
		}
		numericVersion = parsedNumericVersion;
	}
	
	public String getVersionNumber() {
		return versionNumber;
	}

	/**
	 * @return
	 */
	public Set<ModelElement> getModelElements() {
		return modelElements;
	}
	
	/**
	 * @param modelElement
	 * @return
	 */
	public boolean addModelElement(ModelElement modelElement) {
		return this.modelElements.add(modelElement);
	}

	public Set<ModelElement> getModelElementsSince() {
		return modelElementsSince;
	}
	
	/**
	 * @param modelElement
	 * @return
	 */
	public boolean addModelElementSince(ModelElement modelElement) {
		return this.modelElementsSince.add(modelElement);
	}

	public Set<ModelElement> getModelElementsDeprecatedSince() {
		return modelElementsDeprecatedSince;
	}
	
	/**
	 * @param modelElement
	 * @return
	 */
	public boolean addModelElementDeprecatedSince(ModelElement modelElement) {
		return this.modelElementsDeprecatedSince.add(modelElement);
	}
	
	/**
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> Set<T> getModelElements(Class<T> clazz) {
		Set<T> result = new LinkedHashSet<>();
		for (ModelElement modelElement : modelElements) {
			if (clazz.isAssignableFrom(modelElement.getClass())) {
				result.add((T) modelElement);
			}
		}
		
		return result;
	}
	
	/**
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> Set<T> getModelElementsSince(Class<T> clazz) {
		Set<T> result = new LinkedHashSet<>();
		for (ModelElement modelElement : modelElementsSince) {
			if (clazz.isAssignableFrom(modelElement.getClass())) {
				result.add((T) modelElement);
			}
		}
		
		return result;
	}
	
	/**
	 * @param clazz
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public <T> Set<T> getModelElementsDeprecatedSince(Class<T> clazz) {
		Set<T> result = new LinkedHashSet<>();
		for (ModelElement modelElement : modelElementsDeprecatedSince) {
			if (clazz.isAssignableFrom(modelElement.getClass())) {
				result.add((T) modelElement);
			}
		}
		
		return result;
	}
	
	/**
	 * @return true when the versionNumber can be parsed into a Long value, false otherwise
	 */
	public boolean isNumber() {
		return this.numericVersion != null;
	}

	public org.osgi.framework.Version getSemanticVersion() {
		return semanticVersion;
	}

	public Long getNumericVersion() {
		return numericVersion;
	}

	@Override
	public int compareTo(ModelElementI element) {
		if (element instanceof Version) {
			Version otherVersion = (Version) element;
			if (otherVersion.getSemanticVersion() != null && this.semanticVersion != null) {
				// compare semantic versions
				return this.semanticVersion.compareTo(otherVersion.getSemanticVersion());
			} else if (otherVersion.getNumericVersion() != null && this.getNumericVersion() != null) {
				return this.numericVersion.compareTo(otherVersion.getNumericVersion());
			} else {
				return this.versionNumber.compareTo(otherVersion.getVersionNumber());
			}
		} else {
		    return super.compareTo(element);
		}
	}
}
