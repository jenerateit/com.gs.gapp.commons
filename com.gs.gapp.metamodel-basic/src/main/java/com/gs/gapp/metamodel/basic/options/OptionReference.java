package com.gs.gapp.metamodel.basic.options;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;

public class OptionReference extends ModelElement {

	private static final long serialVersionUID = 3243305371732845027L;

	private final ModelElement referencedElement;

	private final Set<String> referencedElementSettings = new LinkedHashSet<>();

	public OptionReference(String name, ModelElement referencedElement) {
		super(name);
		this.referencedElement = referencedElement;
	}

	public OptionReference(String name, ModelElement referencedElement, Collection<String> referencedElementSettings) {
		super(name);
		this.referencedElement = referencedElement;
		this.referencedElementSettings.addAll(referencedElementSettings);
	}

	/**
	 * @return the referencedElement
	 */
	public ModelElement getReferencedElement() {
		return referencedElement;
	}

	/**
	 * @return the referencedElementSettings
	 */
	public Set<String> getReferencedElementSettings() {
		return Collections.unmodifiableSet(referencedElementSettings);
	}
}
