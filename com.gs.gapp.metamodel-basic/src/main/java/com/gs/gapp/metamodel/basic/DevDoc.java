package com.gs.gapp.metamodel.basic;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * An instance of this type holds documentation for developers that use the output of
 * a generator (in form of HTML).
 * 
 * @author mmt
 *
 */
public class DevDoc extends ModelElement {

	private static final long serialVersionUID = 4958332663714266989L;
	
	private final String title;
	private final Set<Entry> entries = new LinkedHashSet<>();
	
	public DevDoc(String name, String title) {
		super(name);
		this.title = title;
	}

	public String getTitle() {
		return title;
	}
	
	public Set<Entry> getEntries() {
		return entries;
	}
	
	public boolean addEntry(Entry entry) {
		return this.entries.add(entry);
	}

	/**
	 * @author mmt
	 *
	 */
	public static class Entry extends ModelElement {
		
		private static final long serialVersionUID = -6966698858431101071L;
		
		private final String topic;
		private final String answer;
		
		/**
		 * @param topic
		 * @param answer
		 */
		public Entry(String topic, String answer) {
			super(topic);
			this.topic = topic;
			this.answer = answer;
		}

		public String getTopic() {
			return topic;
		}

		public String getAnswer() {
			return answer;
		}
	}
}
