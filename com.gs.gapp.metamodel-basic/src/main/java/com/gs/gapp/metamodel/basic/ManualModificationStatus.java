package com.gs.gapp.metamodel.basic;

/**
 * A ManualModificationStatus is used to be able to distinguish whether a piece
 * of code had been generated for a manually modified or added model element (manually
 * added or modified code that results in new or modified model element). With this
 * information the generated code that gets sent back to the cloud-connector on the
 * client-side can be marked there to indicate the difference to the developer.
 *
 * @author mmt
 *
 */
public enum ManualModificationStatus {

	MODIFIED,
	ADDED,
	UNCHANGED;
}
