/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.metamodel;

/**
 * @author hrr
 *
 */
public interface AcceptableI<T extends VisitorI<?>> {

	/**
	 * Accepts the given visitor. 
	 * The visitor's {@link VisitorI#visit(Object)} method is called with this model element. 
	 * If the visitor returns true, this method visits this model element's members.
	 * 
	 * @param visitor the visitor
	 */
	AcceptState accept(T visitor);
}
