/**
 * 
 */
package com.gs.gapp.metamodel.basic;

/**
 * @author mmt
 *
 */
public enum Multiplicity {
	SINGLE_VALUED,
	MULTI_VALUED,
	;

}
