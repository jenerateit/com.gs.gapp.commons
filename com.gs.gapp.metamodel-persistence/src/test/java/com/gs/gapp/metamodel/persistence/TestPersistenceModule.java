package com.gs.gapp.metamodel.persistence;

import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.gs.gapp.metamodel.basic.Model;

/**
 * @author marcu
 *
 */
public class TestPersistenceModule {
	
	/**
	 * 
	 */
	@Test
	public void testSortByName() {
		Model model = new Model("MyModel");
		
		
		final PersistenceModule persistenceModule1 = new PersistenceModule("PM3");
		model.addElement(persistenceModule1);
		
		final Entity entity1 = new Entity("e4");
		persistenceModule1.addElement(entity1);
		final Entity entity2 = new Entity("e99");
		persistenceModule1.addElement(entity2);
		final Entity entity3 = new Entity("e11");
		persistenceModule1.addElement(entity3);
		
		final PersistenceModule persistenceModule2 = new PersistenceModule("PM2");
		model.addElement(persistenceModule2);
		
		final PersistenceModule persistenceModule3 = new PersistenceModule("PM1");
		model.addElement(persistenceModule3);
		
		Set<PersistenceModule> sortedPersistenceModules = model.getElementsSorted(PersistenceModule.class);
		
		Assert.assertTrue("collection of persistence modules is not of size 3", sortedPersistenceModules.size() == 3);
		Assert.assertTrue("first persistence modules in sorted collection is not the one with name 'PM1'", "PM1".equals(sortedPersistenceModules.iterator().next().getName()));
		
		Set<Entity> sortedEntities = persistenceModule1.getEntitiesSorted();
		Assert.assertTrue("collection of persistence modules is not of size 3", sortedEntities.size() == 3);
		Assert.assertTrue("first entity in sorted collection is not the one with name 'e11'", "e11".equals(sortedEntities.iterator().next().getName()));
		
	}
	
	/**
	 * 
	 */
	@Test
	public void testSortBySortValue() {
        Model model = new Model("MyModel");
		
		final PersistenceModule persistenceModule1 = new PersistenceModule("PM3");
		model.addElement(persistenceModule1);
		
		final Entity entity1 = new Entity("e4");
		entity1.setSortValue(3);
		persistenceModule1.addElement(entity1);
		final Entity entity2 = new Entity("e99");
		entity2.setSortValue(2);
		persistenceModule1.addElement(entity2);
		final Entity entity3 = new Entity("e55");
		entity3.setSortValue(1);
		persistenceModule1.addElement(entity3);
		
		final PersistenceModule persistenceModule2 = new PersistenceModule("PM2");
		model.addElement(persistenceModule2);
		
		final PersistenceModule persistenceModule3 = new PersistenceModule("PM1");
		model.addElement(persistenceModule3);
		
		Set<PersistenceModule> sortedPersistenceModules = model.getElementsSorted(PersistenceModule.class);
		
		Assert.assertTrue("collection of persistence modules is not of size 3", sortedPersistenceModules.size() == 3);
		Assert.assertTrue("first persistence modules in sorted collection is not the one with name 'PM1'", "PM1".equals(sortedPersistenceModules.iterator().next().getName()));
		
		Set<Entity> sortedEntities = persistenceModule1.getEntitiesSorted();
		Assert.assertTrue("collection of persistence modules is not of size 3", sortedEntities.size() == 3);
		Assert.assertTrue("first entity in sorted collection is not the one with name '" + entity3.getName() + "'", entity3 == sortedEntities.iterator().next());
		
	}

	/**
	 * 
	 */
	@Test
	public void testSortBySortValueAndName() {
		 Model model = new Model("MyModel");
			
			final PersistenceModule persistenceModule1 = new PersistenceModule("PM3");
			model.addElement(persistenceModule1);
			
			final Entity entity1 = new Entity("e4");
			entity1.setSortValue(3);
			persistenceModule1.addElement(entity1);
			final Entity entity2 = new Entity("e99");
			entity2.setSortValue(2);
			persistenceModule1.addElement(entity2);
			final Entity entity3 = new Entity("e55");
			entity3.setSortValue(1);
			persistenceModule1.addElement(entity3);
			final Entity entity4 = new Entity("a");
			persistenceModule1.addElement(entity4);
			final Entity entity5 = new Entity("b");
			persistenceModule1.addElement(entity5);
			
			final PersistenceModule persistenceModule2 = new PersistenceModule("PM2");
			model.addElement(persistenceModule2);
			
			final PersistenceModule persistenceModule3 = new PersistenceModule("PM1");
			model.addElement(persistenceModule3);
			persistenceModule3.setSortValue(3);
			
			Set<PersistenceModule> sortedPersistenceModules = model.getElementsSorted(PersistenceModule.class);
			
			Assert.assertTrue("collection of persistence modules is not of size 3", sortedPersistenceModules.size() == 3);
			Assert.assertTrue("first persistence module in sorted collection is not the one with name 'PM2'", "PM2".equals(sortedPersistenceModules.iterator().next().getName()));
			
			Set<Entity> sortedEntities = persistenceModule1.getEntitiesSorted();
			Assert.assertTrue("collection of persistence modules is not of size 5", sortedEntities.size() == 5);
			Assert.assertTrue("first entity in sorted collection is not the one with name '" + entity4.getName() + "'", entity4 == sortedEntities.iterator().next());
	}
}
