package com.gs.gapp.metamodel.persistence;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;

/**
 * @author marcu
 *
 */
public class TestValidatorDuplicateEntities {
	
	/**
	 * 
	 */
	@Test
	public void test() {
		Model model = new Model("MyModel");
		
		final PersistenceModule persistenceModule0 = new PersistenceModule("PM0");
		model.addElement(persistenceModule0);
		final PersistenceModule persistenceModule1 = new PersistenceModule("PM1");
		model.addElement(persistenceModule1);
		
		Entity entity0 = new Entity("entity");
		persistenceModule0.addElement(entity0);
		new EntityField("Name0", entity0);
		new EntityField("Name0", entity0);
		new EntityField("Name0", entity0);
		
		Entity entity1 = new Entity("Entity");
		persistenceModule0.addElement(entity1);
		new EntityField("Name1", entity1);
		new EntityField("Name1", entity1);
		new EntityField("Name1", entity1);
		
		Entity entity2 = new Entity("ENTITY");
		persistenceModule0.addElement(entity2);
		new EntityField("Name2", entity2);
		new EntityField("Name2", entity2);
		new EntityField("Name2", entity2);
		
		Collection<Object> modelElements = new ArrayList<>();
		modelElements.add(entity0);
		modelElements.add(entity1);
		modelElements.add(entity2);
		
		entity0 = new Entity("entity");
		persistenceModule1.addElement(entity0);
		new EntityField("Name0", entity0);
		new EntityField("Name0", entity0);
		new EntityField("Name0", entity0);
		
		entity1 = new Entity("Entity");
		persistenceModule1.addElement(entity1);
		new EntityField("Name1", entity1);
		new EntityField("Name1", entity1);
		new EntityField("Name1", entity1);
		
		entity2 = new Entity("ENTITY");
		persistenceModule1.addElement(entity2);
		new EntityField("Name2", entity2);
		new EntityField("Name2", entity2);
		new EntityField("Name2", entity2);
		
		modelElements.add(entity0);
		modelElements.add(entity1);
		modelElements.add(entity2);
		
		Collection<Message> errorMessages = new ValidatorDuplicateEntities().validate(modelElements);
		for (Message errorMessage : errorMessages) {
			System.out.println("Error:" + errorMessage.getMessage());
		}
		assertTrue("not enought or too many error messages found", errorMessages.size() == 2);
	}
}
