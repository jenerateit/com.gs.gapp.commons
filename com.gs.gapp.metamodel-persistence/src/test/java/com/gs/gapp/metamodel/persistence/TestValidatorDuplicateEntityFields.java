package com.gs.gapp.metamodel.persistence;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;

/**
 * @author marcu
 *
 */
public class TestValidatorDuplicateEntityFields {
	
	/**
	 * 
	 */
	@Test
	public void test() {
		Model model = new Model("MyModel");
		
		final PersistenceModule persistenceModule1 = new PersistenceModule("PM");
		model.addElement(persistenceModule1);
		
		final Entity entity0 = new Entity("e0");
		persistenceModule1.addElement(entity0);
		new EntityField("Name0", entity0);
		new EntityField("NamE0", entity0);
		new EntityField("NaMe0", entity0);
		
		final Entity entity1 = new Entity("e1");
		persistenceModule1.addElement(entity1);
		new EntityField("NAme1", entity1);
		new EntityField("Name1", entity1);
		new EntityField("name1", entity1);
		
		final Entity entity2 = new Entity("e2");
		persistenceModule1.addElement(entity2);
		new EntityField("Name2", entity2);
		new EntityField("Name2", entity2);
		new EntityField("Name2", entity2);
		
		Collection<Object> modelElements = new ArrayList<>();
		modelElements.add(entity0);
		modelElements.add(entity1);
		modelElements.add(entity2);
		
		Collection<Message> errorMessages = new ValidatorDuplicateEntityFields().validate(modelElements);
		for (Message errorMessage : errorMessages) {
			System.out.println("Error:" + errorMessage.getMessage());
		}
		assertTrue("not enought or too many error messages found", errorMessages.size() == 4);
	}
}
