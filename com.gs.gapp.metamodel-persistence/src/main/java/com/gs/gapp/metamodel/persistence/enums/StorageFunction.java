/*
 * CascadeType.java
 *
 * Created on 24. Oktober 2006, 21:12
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.gs.gapp.metamodel.persistence.enums;

import java.util.LinkedHashMap;
import java.util.Map;

import com.gs.gapp.dsl.rest.PurposeEnum;

/**
 *
 * @author mmt
 */
public enum StorageFunction {
	
	
    CREATE ("Create"),
    CREATE_MANY ("CreateMany"),
    READ ("Read"),
    READ_MANY ("ReadMany"),
    UPDATE ("Update"),
    UPDATE_MANY ("UpdateMany"),
    DELETE ("Delete"),
    DELETE_MANY ("DeleteMany"),
    DELETE_ALL ("DeleteAll"),
    MULTI_PURPOSE ("MultiPurpose"),
    ;
	
    private static final Map<String, StorageFunction> stringToEnum =
		new LinkedHashMap<>();
	
	static {
		for (StorageFunction enumEntry : values()) {
			stringToEnum.put(enumEntry.name.toLowerCase(), enumEntry);
		}
	}
	
	private final String name;

	/**
	 * @param name
	 */
	private StorageFunction(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public static StorageFunction fromString(String name) {
		if (name != null) {
		    return stringToEnum.get(name.toLowerCase());
		}
		return null;
	}
	
	public static StorageFunction fromPurposeEnum(PurposeEnum purposeEnumEntry) {
		if (purposeEnumEntry != null) {
		    return stringToEnum.get(purposeEnumEntry.getName().toLowerCase());
		}
		return null;
	}
}
