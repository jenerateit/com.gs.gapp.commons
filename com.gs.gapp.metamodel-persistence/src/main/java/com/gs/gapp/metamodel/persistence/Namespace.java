/**
 *
 */
package com.gs.gapp.metamodel.persistence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author mmt
 *
 */
public class Namespace extends com.gs.gapp.metamodel.basic.Namespace {

	/**
	 *
	 */
	private static final long serialVersionUID = 7609130794144048820L;

	private Set<Entity> entities = new LinkedHashSet<>();

	/**
	 * @param name
	 */
	public Namespace(String name) {
		super(name);
	}

	/**
	 * @return the entities
	 */
	public Set<Entity> getEntities() {
		return Collections.unmodifiableSet(entities);
	}
	
	/**
	 * A convenience method to get all entities, sorted according to their natural ordering.
	 *
	 * @return
	 */
	public Set<Entity> getEntitiesSorted() {
		ArrayList<Entity> elementsForSorting = new ArrayList<>(entities);
		Collections.sort(elementsForSorting);
		Set<Entity> elements = new LinkedHashSet<>(elementsForSorting);
		
		return elements;
	}

	/**
	 * @param entity
	 */
	public void addEntity(Entity entity) {
		this.entities.add(entity);
	}

}
