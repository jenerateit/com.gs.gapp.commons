package com.gs.gapp.metamodel.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

import com.gs.gapp.metamodel.basic.ModelValidatorI;

/**
 * This validator checks whether there are entities that
 * have multiple entity fields with the same name.
 * 
 * @author marcu
 *
 */
public class ValidatorDuplicateEntities implements ModelValidatorI {

	public ValidatorDuplicateEntities() {}

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(assertNoDuplicateEntities(modelElements));
		return result;
	}

	/**
	 * @param rawElements
	 * @return
	 */
	private Collection<Message> assertNoDuplicateEntities(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();
		
		Message summaryMessage = null;
		
		Map<String,ArrayList<Entity>> entitiesMap = new LinkedHashMap<>();
		for (Object element : rawElements) {
			if (element instanceof Entity) {
				Entity entity = (Entity) element;
				
				ArrayList<Entity> entities = entitiesMap.get(entity.getName().toLowerCase());
				if (entities == null) {
					entities = new ArrayList<>();
					entitiesMap.put(entity.getName().toLowerCase(), entities);
				}
				entities.add(entity);
			}
		}
		
		for (String entityName : entitiesMap.keySet()) {
			ArrayList<Entity> entitiesPerName = entitiesMap.get(entityName);
			if (entitiesPerName.size() > 1) {
				StringBuilder entityAndModuleNames = new StringBuilder();
				String comma = "";
				for (Entity anEntity : entitiesPerName) {
					entityAndModuleNames.append(comma).append("'").append(anEntity.getName()).append("'").
					    append(" in module ").append("'").append(anEntity.getModule().getName()).append("'");
					comma = ", ";
				}
				
				if (summaryMessage == null) {
					summaryMessage = PersistenceMessage.DUPLICATE_ENTITY_NAMES_SUMMARY.getMessage();
					result.add(summaryMessage);
				}
				
				Message message = PersistenceMessage.DUPLICATE_ENTITY_NAMES.getMessage(entitiesPerName.size(), entityName, entityAndModuleNames.toString());
				result.add(message);
			}
		}

		return result;
	}
}
