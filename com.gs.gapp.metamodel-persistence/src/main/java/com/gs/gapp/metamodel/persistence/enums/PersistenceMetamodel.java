package com.gs.gapp.metamodel.persistence.enums;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs.gapp.metamodel.basic.MetamodelI;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.persistence.EntityRelationEnd;
import com.gs.gapp.metamodel.persistence.Namespace;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.gs.gapp.metamodel.persistence.Relation;

/**
 * @author marcu
 *
 */
public enum PersistenceMetamodel implements MetamodelI {

	INSTANCE,
	;
	
	private static final LinkedHashSet<Class<? extends ModelElementI>> metatypes = new LinkedHashSet<>();
	private static final Collection<Class<? extends ModelElementI>> collectionOfCheckedMetatypes = new LinkedHashSet<>();
	
	static {
		metatypes.add(Entity.class);
		metatypes.add(EntityField.class);
		metatypes.add(EntityRelationEnd.class);
		metatypes.add(Namespace.class);
		metatypes.add(PersistenceModule.class);
		metatypes.add(Relation.class);
		
		collectionOfCheckedMetatypes.add(Entity.class);
		collectionOfCheckedMetatypes.add(PersistenceModule.class);
		collectionOfCheckedMetatypes.add(Namespace.class);
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypes() {
		return Collections.unmodifiableCollection(metatypes);
	}

	@Override
	public boolean isIncluded(Class<? extends ModelElementI> metatype) {
		return metatypes.contains(metatype);
	}

	@Override
	public boolean isExtendingOneOfTheMetatypes(Class<? extends ModelElementI> metatype) {
		for (Class<? extends ModelElementI> metatypeOfMetamodel : metatypes) {
			if (metatypeOfMetamodel.isAssignableFrom(metatype)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Collection<Class<? extends ModelElementI>> getMetatypesForConversionCheck() {
		return collectionOfCheckedMetatypes;
	}
	
	/**
	 * @param metatype
	 * @return
	 */
	public Class<? extends Module> getModuleType(Class<? extends ModelElementI> metatype) {
		if (isIncluded(metatype)) {
		    return PersistenceModule.class;
		}
		return null;
	}
	
	/**
	 * @param entities
	 * @return
	 */
	public Set<Entity> getTenantEntities(Collection<Entity> entities) {
		if (entities != null) {
			return entities.stream()
					.map(entity -> entity.findFirstMultiTenancyAwareParent())
					.filter(entity -> entity != null)
				    .filter(entity -> entity.getTenantEntity() != null)
				    .map(entity -> entity.getTenantEntity())
				    .collect(Collectors.toCollection(LinkedHashSet::new));
		}
		
		return Collections.emptySet();
	}
	
	/**
	 * @param entities
	 * @return
	 */
	public Set<Entity> getUserAccountEntities(Collection<Entity> entities) {
		if (entities != null) {
			return entities.stream()
				.map(entity -> entity.findFirstUserAccountAwareParent())
				.filter(entity -> entity != null)
			    .filter(entity -> entity.getUserAccountEntity() != null)
			    .map(entity -> entity.getUserAccountEntity())
			    .collect(Collectors.toCollection(LinkedHashSet::new));
		}
		
		return Collections.emptySet();
	}
}
