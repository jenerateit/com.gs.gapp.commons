package com.gs.gapp.metamodel.persistence;

import com.gs.gapp.metamodel.basic.MessageI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.MessageStatus;

public enum PersistenceMessage implements MessageI {

	DUPLICATE_ENTITY_NAMES_SUMMARY (MessageStatus.ERROR, "0001a",
			"Multiple entities with identical names (names may differ in upper and lower case) are not allowed and cannot be used for code generation.",
			"Modify the model by changing the name of one or more entities."),
	DUPLICATE_ENTITY_NAMES (MessageStatus.ERROR, "0001b",
			"Found %d entities with the name '%s': '%s'.",
			"Change the names to make them unique and re-run the generation."),
	
	DUPLICATE_ENTITY_FIELD_NAMES_SUMMARY (MessageStatus.ERROR, "0002a",
			"Multiple entity fields with identical names (names may differ in upper and lower case), in one and the same entity, are not allowed and cannot be used for code generation.",
			"Modify the model by changing the name of one or more entity fields."),
	DUPLICATE_ENTITY_FIELD_NAMES (MessageStatus.ERROR, "0002b",
			"Found %d entity fields with the name '%s': '%s'.",
			"Modify the model by changing the name of one or more entity fields."),
	
	MORE_THAN_ONE_ID_FIELD (MessageStatus.ERROR, "0003",
			"The entity '%s' has %d ID fields (%s), but at most one ID field is supported by the generator.",
			"Reduce the number of ID fields in the model."),
	
	MORE_THAN_ONE_BUSINESS_ID_FIELD (MessageStatus.ERROR, "0004",
			"The entity '%s' has %d business ID fields (%s), but at most one business ID field is supported by the generator.",
			"Reduce the number of business ID fields in the model."),
	
	NO_ID_FIELD (MessageStatus.ERROR,
			"0005",
			"The entity '%s' doesn't have any ID field. The generator needs an ID field in order to be able to generate additional functionality for an entity.",
			"Add an ID field to your entity."),
	
	// ---------------- WARNINGS from here on -----------------------------------
	NO_ID_FIELD_WARNING (MessageStatus.WARNING,
			"1001",
			"The entity '%s' doesn't have any ID field. The generator needs an ID field in order to be able to generate additional functionality for an entity.",
			"Add an ID field to your entity in case you want to generate additional functionality for it, e.g. in a REST service."),
	;

	private final MessageStatus status;
	private final String organization = "GS";
	private final String section = "DATA";
	private final String id;
	private final String problemDescription;
	private final String instruction;
	
	private PersistenceMessage(MessageStatus status, String id, String problemDescription, String instruction) {
		this.status = status;
		this.id = id;
		this.problemDescription = problemDescription;
		this.instruction = instruction;
	}
	
	@Override
	public MessageStatus getStatus() {
		return status;
	}

	@Override
	public String getOrganization() {
		return organization;
	}

	@Override
	public String getSection() {
		return section;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getProblemDescription() {
		return problemDescription;
	}
	
	@Override
	public String getInstruction() {
		return instruction;
	}
}
 