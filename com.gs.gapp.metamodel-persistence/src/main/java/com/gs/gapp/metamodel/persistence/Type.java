/**
 * 
 */
package com.gs.gapp.metamodel.persistence;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author mmt
 *
 */
public enum Type implements TypeI {
	STRING ("string"),
	INTEGER ("integer"),
	LONG ("long"),
	FLOAT ("float"),
	DOUBLE ("double"),
	BOOLEAN ("boolean"),
	TIME ("time"),
	DATE ("date"),
	TIMESTAMP ("timestamp"),
	ENUMERATION ("enumeration"),
	BIG_DECIMAL ("bigdecimal"),
	BYTE ("byte"),
	;
	
	private static final Map<String, Type> stringToEnum =
		new LinkedHashMap<>();
	
	static {
		for (Type t : values()) {
			stringToEnum.put(t.name.toLowerCase(), t);
		}
	}
	
	private final String name;

	/**
	 * @param name
	 */
	private Type(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	@Override
	public String getName() {
		return name;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.persistence.TypeI#isEntity()
	 */
	@Override
	public boolean isEntity() { return false; }
	
	/**
	 * @param name
	 * @return
	 */
	public static Type fromString(String name) {
		return stringToEnum.get(name.toLowerCase());
	}
}
