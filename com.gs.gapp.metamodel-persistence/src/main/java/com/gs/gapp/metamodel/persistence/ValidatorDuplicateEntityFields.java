package com.gs.gapp.metamodel.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

import com.gs.gapp.metamodel.basic.ModelValidatorI;

/**
 * This validator checks whether there are entities that
 * have multiple entity fields with the same name.
 * 
 * @author marcu
 *
 */
public class ValidatorDuplicateEntityFields implements ModelValidatorI {

	public ValidatorDuplicateEntityFields() {}

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(assertNoDuplicateEntityFields(modelElements));
		return result;
	}

	/**
	 * @param rawElements
	 * @return
	 */
	private Collection<Message> assertNoDuplicateEntityFields(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();
		
		Message summaryMessage = null;
		
		for (Object element : rawElements) {
			if (element instanceof Entity) {
				Entity entity = (Entity) element;
				Map<String,ArrayList<EntityField>> entityFieldsMap = new LinkedHashMap<>();
				for (EntityField entityField : entity.getEntityFields()) {
					ArrayList<EntityField> entityFields = entityFieldsMap.get(entityField.getName().toLowerCase());
					if (entityFields == null) {
						entityFields = new ArrayList<>();
						entityFieldsMap.put(entityField.getName().toLowerCase(), entityFields);
					}
					entityFields.add(entityField);
				}
				
				for (String fieldName : entityFieldsMap.keySet()) {
					ArrayList<EntityField> entityFields = entityFieldsMap.get(fieldName);
					if (entityFields.size() > 1) {
						StringBuilder entityFieldAndEntityNames = new StringBuilder();
						String comma = "";
						for (EntityField anEntityField : entityFields) {
							entityFieldAndEntityNames.append(comma).append("'").append(anEntityField.getName()).append("'").
							    append(" in entity ").append("'").append(entity.getName()).append("'");
							comma = ", ";
						}
						
						if (summaryMessage == null) {
							summaryMessage = PersistenceMessage.DUPLICATE_ENTITY_FIELD_NAMES_SUMMARY.getMessage();
							result.add(summaryMessage);
						}
						
						Message message = PersistenceMessage.DUPLICATE_ENTITY_FIELD_NAMES.getMessage(entityFields.size(), fieldName, entityFieldAndEntityNames.toString());
						result.add(message);
					}
				}
			}
		}

		return result;
	}
}
