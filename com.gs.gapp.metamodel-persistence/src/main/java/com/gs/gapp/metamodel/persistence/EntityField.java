package com.gs.gapp.metamodel.persistence;

import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.persistence.enums.EnumType;

/**
 * @author mmt
 *
 */
public class EntityField extends Field {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * A transient entity attribute is not being
	 * read from the database and not be written
	 * to the database.
	 */
	private boolean isTransient = false;

	/**
	 * An entity attribute marked as "version" is used
	 * to identify the version of a data record in the
	 * database. The version attribute will be
	 * updated automatically whenever an entity
	 * got updated in the database.
	 */
	private boolean version = false;

	/**
	 *
	 */
	private Entity owner;

	/**
	 * If the attribute's type is an enumeration, the enum type tells the generation
	 * whether the ordinal number or the enum item's string should be stored in the
	 * database table. If this field is left null, then the generation decides
	 * itself what to do by default.
	 *
	 * @see <a href="http://www.oracle.com/technology/products/ias/toplink/jpa/resources/toplink-jpa-annotations.html#Enumerated">@Enumerated</a>
	 */
	private EnumType enumType;

	/**
	 * @see <a href="http://www.oracle.com/technology/products/ias/toplink/jpa/resources/toplink-jpa-annotations.html#Lob">@Lob</a>
	 */
	private boolean lob = false;


	/**
	 *
	 */
	private boolean partOfId = false;

	/**
	 *
	 */
	private boolean unique = false;
	
	/**
	 *
	 */
	private boolean searchable = false;
	
	/**
	 * 
	 */
	private boolean cacheable = false;
	
	/**
	 * 
	 */
	private boolean partOfBusinessId = false;

	/**
	 * @param name
	 */
	public EntityField(String name, Entity owner) {
		super(name, owner);

		if (owner == null) throw new NullPointerException("param 'owner' must not be null");

		this.owner = owner;
		owner.addField(this);
	}

	/**
	 * @return the isTransient
	 */
	public boolean isTransient() {
		return isTransient;
	}

	/**
	 * @param isTransient the isTransient to set
	 */
	public void setTransient(boolean isTransient) {
		this.isTransient = isTransient;
	}

	/**
	 * @return the version
	 */
	public boolean isVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(boolean version) {
		this.version = version;
	}

	/**
	 * @return
	 */
	public boolean isEnumeration() {
		return getType() instanceof Enumeration;
	}

	/**
	 * @return the enumType
	 */
	public EnumType getEnumType() {
		return enumType;
	}

	/**
	 * @param enumType the enumType to set
	 */
	public void setEnumType(EnumType enumType) {
		this.enumType = enumType;
	}

	/**
	 * @return the lob
	 */
	public boolean isLob() {
		return lob;
	}

	/**
	 * @param lob the lob to set
	 */
	public void setLob(boolean lob) {
		this.lob = lob;
	}

	/**
	 * @return the partOfId
	 */
	public boolean isPartOfId() {
		return partOfId;
	}

	/**
	 * @param partOfId the partOfId to set
	 */
	public void setPartOfId(boolean partOfId) {
		this.partOfId = partOfId;
	}

	/**
	 * @return the owner
	 */
	@Override
	public Entity getOwner() {
		return owner;
	}

	/**
	 * @return the unique
	 */
	public boolean isUnique() {
		return unique;
	}

	/**
	 * @param unique the unique to set
	 */
	public void setUnique(boolean unique) {
		this.unique = unique;
	}

	/**
	 * @return searchable field
	 */
	public boolean isSearchable() {
		return searchable;
	}

	/**
	 * @param searchable field set
	 */
	public void setSearchable(boolean searchable) {
		this.searchable = searchable;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		EntityField other = (EntityField) obj;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		return true;
	}

	public boolean isPartOfBusinessId() {
		return partOfBusinessId;
	}

	public void setPartOfBusinessId(boolean partOfBusinessId) {
		this.partOfBusinessId = partOfBusinessId;
	}

	public boolean isCacheable() {
		return cacheable;
	}

	public void setCacheable(boolean cacheable) {
		this.cacheable = cacheable;
	}
}
