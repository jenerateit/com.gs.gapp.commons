/*
 * CascadeType.java
 *
 * Created on 24. Oktober 2006, 21:12
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.gs.gapp.metamodel.persistence.enums;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author mmt
 */
public enum FetchType {
    LAZY ("lazy"),
    EAGER ("eager"),
    ;
    
    private static final Map<String, FetchType> stringToEnum =
		new LinkedHashMap<>();
	
	static {
		for (FetchType fetchTypes : values()) {
			stringToEnum.put(fetchTypes.name, fetchTypes);
		}
	}
	
	private final String name;

	/**
	 * @param name
	 */
	private FetchType(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public static FetchType fromString(String name) {
		return stringToEnum.get(name.toLowerCase());
	}
}
