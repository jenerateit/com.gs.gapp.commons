/**
 *
 */
package com.gs.gapp.metamodel.persistence;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.persistence.enums.CascadeType;
import com.gs.gapp.metamodel.persistence.enums.FetchType;

/**
 * @author mmt
 *
 */
public class EntityRelationEnd extends EntityField {


	/**
	 *
	 */
	private static final long serialVersionUID = 3513196163243809206L;

	/**
	 * In a bidirectional relationship that is navigable in both
	 * directions, only one side of the relationship can own
	 * the relationship. This flag indicates, which side is the owner.
	 * This flag is being ignored if the relationship is not bidirectional.
	 */
	private boolean ownerOfBidirectionalRelationship = false;


	/**
	 * In case this entity attribute represents a OneToMany or a
	 * ManyToMany association, these settings determine the order of
	 * the entries in the association's many-side.
	 *
	 * @see <a href="http://www.oracle.com/technology/products/ias/toplink/jpa/resources/toplink-jpa-annotations.html#OrderBy">@OrderBy</a>
	 */
	final private Set<EntityField> orderByAttributes = new LinkedHashSet<>();

	/**
	 *
	 */
	final private Set<String> orderByDirections = new LinkedHashSet<>();

	/**
	 * This is alternative to using orderByAttributes and orderByDirections
	 */
	private String orderByClause;


	/**
	 * Can be EAGER or LAZY. EAGER fetch type means that this entity attribute's
	 * values are going to be fetch when the entity itself is loaded. LAZY means
	 * that this entity attribute's values are going to be fetched when
	 * the entity attribute is going to be accessed for the very first time.
	 */
	private FetchType fetchType;

	/**
	 * One or more of ALL, MERGE, PERSIST, REFRESH, REMOVE, SETNULL.
	 * Determines, which of the peristence operations are going to be
	 * cascaded for this entity attribute. If the set is null or empty,
	 * then none of the persistence operations are going to be cascaded.
	 * These options are only applicable for entity attributes that
	 * represent an association.
	 *
	 * @see <a href="http://www.oracle.com/technology/products/ias/toplink/jpa/resources/toplink-jpa-annotations.html#OneToMany">@OneToMany</a>
	 * @see <a href="http://www.oracle.com/technology/products/ias/toplink/jpa/resources/toplink-jpa-annotations.html#OneToOne">@OneToOne</a>
	 * @see <a href="http://www.oracle.com/technology/products/ias/toplink/jpa/resources/toplink-jpa-annotations.html#ManyToOne">@ManyToOne</a>
	 * @see <a href="http://www.oracle.com/technology/products/ias/toplink/jpa/resources/toplink-jpa-annotations.html#ManyToMany">@ManyToMany</a>
	 */
	private Set<CascadeType> cascadeOptions = new LinkedHashSet<>();

	/**
	 * joins to relation ends together
	 */
	private Relation relation;

	private String referencedBy;
	
	private boolean privatelyOwned;
	
	private boolean keepOrdering = false;

	/**
	 * @param name
	 */
	public EntityRelationEnd(String name, Entity owner) {
		super(name, owner);
	}

	/**
	 * @return the ownerOfBidirectionalRelationship
	 */
	public boolean isOwnerOfBidirectionalRelationship() {
		return ownerOfBidirectionalRelationship;
	}

	/**
	 * @param ownerOfBidirectionalRelationship the ownerOfBidirectionalRelationship to set
	 */
	public void setOwnerOfBidirectionalRelationship(
			boolean ownerOfBidirectionalRelationship) {
		this.ownerOfBidirectionalRelationship = ownerOfBidirectionalRelationship;
	}

	/**
	 * @return the orderByAttributes
	 */
	public Set<EntityField> getOrderByAttributes() {
		return orderByAttributes;
	}

	/**
	 * @return the orderByDirections
	 */
	public Set<String> getOrderByDirections() {
		return orderByDirections;
	}

	/**
	 * @param orderByAttribute
	 * @param direction
	 */
	public void addOrderBy(EntityField orderByAttribute, String direction) {
		this.orderByAttributes.add(orderByAttribute);
		this.orderByDirections.add(direction);
	}

	/**
	 * @return the orderByClause
	 */
	public String getOrderByClause() {
		if (this.orderByClause == null || this.orderByClause.length() == 0) {
			Set<String> orderDirections = this.getOrderByDirections();
        	String direction = null;
        	StringBuffer orderByClauseBuffer = new StringBuffer();
        	int ii = 0;
        	Iterator<String> orderDirectionsIter = orderDirections.iterator();
        	for (EntityField entityAttributeForOrdering : this.getOrderByAttributes()) {
        		direction = "ASC";
        		if (ii < orderDirections.size()) {
        			direction = orderDirectionsIter.next();
        		}
        		if (orderByClauseBuffer.length() > 0) {
        			orderByClauseBuffer.append(", ");
        		}
        		orderByClauseBuffer.append(entityAttributeForOrdering.getName()).append(" ").append(direction);
        	}
        	return orderByClauseBuffer.toString();
		} else {
		    return orderByClause;
		}
	}

	/**
	 * @param orderByClause the orderByClause to set
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * @return the fetchType
	 */
	public FetchType getFetchType() {
		return fetchType;
	}

	/**
	 * @param fetchType the fetchType to set
	 */
	public void setFetchType(FetchType fetchType) {
		this.fetchType = fetchType;
	}

	/**
	 * @return the cascadeOptions
	 */
	public Set<CascadeType> getCascadeOptions() {
		return cascadeOptions;
	}

	/**
	 * @param cascadeType
	 */
	public void addCascadeOption(CascadeType cascadeType) {
		this.cascadeOptions.add(cascadeType);
	}

	/**
	 * @return
	 */
	public boolean isSetNullCascadeOptionSet() {
    	return (getCascadeOptions().contains(CascadeType.SETNULL) && !getCascadeOptions().contains(CascadeType.REMOVE) && !getCascadeOptions().contains(CascadeType.ALL));
    }

	/**
	 * @return the relation
	 */
	public Relation getRelation() {
		return relation;
	}

	/**
	 * @param relation the relation to set
	 */
	public void setRelation(Relation relation) {
		this.relation = relation;
	}

	/**
	 * @return
	 */
	public boolean isBidirectional() {
		return this.getOtherRelationEnd() != null;
	}

	/**
	 * @return
	 */
	public EntityRelationEnd getOtherRelationEnd() {
		if (getRelation() != null) {
			if (getRelation().getRoleA() != null && getRelation().getRoleA() != this) {
				return getRelation().getRoleA();
			} else if (getRelation().getRoleB() != null && getRelation().getRoleB() != this) {
				return getRelation().getRoleB();
			}
		}

		return null;
	}

	/**
	 * @return the referencedBy
	 */
	public String getReferencedBy() {
		return referencedBy;
	}

	/**
	 * @param referencedBy the referencedBy to set
	 */
	public void setReferencedBy(String referencedBy) {
		this.referencedBy = referencedBy;
	}

	public boolean isPrivatelyOwned() {
		return privatelyOwned;
	}

	public void setPrivatelyOwned(boolean privatelyOwned) {
		this.privatelyOwned = privatelyOwned;
	}

	public boolean isKeepOrdering() {
		return keepOrdering;
	}

	public void setKeepOrdering(boolean keepOrdering) {
		this.keepOrdering = keepOrdering;
	}

}
