/*
 * CascadeType.java
 *
 * Created on 24. Oktober 2006, 21:12
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.gs.gapp.metamodel.persistence.enums;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author mmt
 */
public enum EnumType {
    ORDINAL ("ordinal"),
    STRING ("string"),
    ;
    
    private static final Map<String, EnumType> stringToEnum =
		new LinkedHashMap<>();
	
	static {
		for (EnumType enumTypes : values()) {
			stringToEnum.put(enumTypes.name, enumTypes);
		}
	}
	
	private final String name;

	/**
	 * @param name
	 */
	private EnumType(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public static EnumType fromString(String name) {
		return stringToEnum.get(name.toLowerCase());
	}
}
