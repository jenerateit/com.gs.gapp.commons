/*
 * CascadeType.java
 *
 * Created on 24. Oktober 2006, 21:12
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.gs.gapp.metamodel.persistence.enums;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author mmt
 */
public enum Nature {
	
	
    STATIC ("Static"),
    TRANSACTIONAL ("Transactional"),
    ;
	
    private static final Map<String, Nature> stringToEnum =
		new LinkedHashMap<>();
	
	static {
		for (Nature enumEntry : values()) {
			stringToEnum.put(enumEntry.name, enumEntry);
		}
	}
	
	private final String name;

	/**
	 * @param name
	 */
	private Nature(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public static Nature fromString(String name) {
		if (name != null) {
		    return stringToEnum.get(name.toLowerCase());
		}
		return null;
	}
}
