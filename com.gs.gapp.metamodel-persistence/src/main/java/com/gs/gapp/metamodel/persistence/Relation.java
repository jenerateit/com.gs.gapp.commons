/**
 *
 */
package com.gs.gapp.metamodel.persistence;

import com.gs.gapp.metamodel.basic.ModelElement;

/**
 * @author mmt
 *
 */
public class Relation extends ModelElement {

	/**
	 *
	 */
	private static final long serialVersionUID = 3357206015935064670L;

	private EntityRelationEnd roleA;

	private EntityRelationEnd roleB;

	/**
	 * @param name
	 */
	public Relation(String name) {
		super(name);
	}

	/**
	 * @return the roleA
	 */
	public EntityRelationEnd getRoleA() {
		return roleA;
	}

	/**
	 * @param roleA the roleA to set
	 */
	public void setRoleA(EntityRelationEnd roleA) {
		this.roleA = roleA;
	}

	/**
	 * @return the roleB
	 */
	public EntityRelationEnd getRoleB() {
		return roleB;
	}

	/**
	 * @param roleB the roleB to set
	 */
	public void setRoleB(EntityRelationEnd roleB) {
		this.roleB = roleB;
	}

}
