package com.gs.gapp.metamodel.persistence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;

import com.gs.gapp.metamodel.AcceptState;
import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.ModelElementVisitorI;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.persistence.enums.Nature;
import com.gs.gapp.metamodel.persistence.enums.StorageFunction;

/**
 * 
 */
public class Entity extends ComplexType implements TypeI {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *
	 */
	private final Set<EntityField> entityFields = new LinkedHashSet<>();

	/**
	 *
	 */
	private Entity parent;

	/**
	 * If this is set to true, this entity itself is not going to mapped to a
	 * database table, but its properties are going to be mapped to the database
	 * table that belongs to an entity that inherits from this entity.
	 *
	 * @see <a href="http://www.oracle.com/technology/products/ias/toplink/jpa/resources/toplink-jpa-annotations.html#MappedSuperclass">@MappedSuperclass</a>
	 */
	private boolean mappedSuperclass = false;

	/**
	 *
	 */
	private boolean utilityFields = false;
	
	private Nature nature;

	private Namespace namespace;
	
	private final Map<StorageFunction, Set<ExceptionType>> exceptions = new TreeMap<>();
	
	private final Set<StorageFunction> storageFunctions = new LinkedHashSet<>();
	
	private final Set<Entity> consumedEntities = new LinkedHashSet<>();
	
	private final Set<ModelElement> consumedModelElements = new LinkedHashSet<>();
	
	private Integer numberOfTestEntries;
	
	private boolean cacheable = false;
	
	private Entity tenantEntity;
	
	private EntityField tenantEntityField;
	
	private Entity userAccountEntity;
	
	private EntityField userAccountEntityField;
	
	private EntityField userAccountEntityFieldForModifiedBy;
	
	/**
	 * @param name
	 */
	public Entity(String name) {
		super(name);
	}

	/**
	 * @return the entityFields
	 */
	public Set<EntityField> getEntityFields() {
		return Collections.unmodifiableSet(entityFields);
	}

	/**
	 * @return
	 */
	public Set<EntityField> getAllEntityFields() {
		Set<EntityField> result = new LinkedHashSet<>();

		Entity parent = this;
		while (parent != null) {
			result.addAll(parent.getEntityFields());
			parent = parent.getParent();
		}

		return result;
	}
	
	
	/**
	 * @return the list of all ancestors, including this Entity (which is at the end of the list)
	 */
	@Override
	public List<Entity> getAncestors() {
		List<Entity> result = new ArrayList<>();
		
		Entity parent = this;
		while (parent != null) {
			result.add(0, parent);
			parent = parent.getParent();
		}
		
		return result;
	}
	
	/**
	 * @return all entity fields, including fields of all ancestors, ordering: 1. id fields, 2. non id fields (fields of ancestors before fields of descendants)
	 */
	public Set<EntityField> getAllEntityFieldsAncestorsFirst() {
		Set<EntityField> result = new LinkedHashSet<>();

		Set<EntityField> idFields = new LinkedHashSet<>();
		Set<EntityField> nonIdFields = new LinkedHashSet<>();
		
		for (Entity entity : getAncestors()) {
			for (EntityField entityField : entity.getEntityFields()) {
				if (entityField.isPartOfId()) {
					if (!idFields.add(entityField)) {
						// field could not be added since the set already contained a different object that is equal to the object to be added
						if (idFields.remove(entityField)) {
							idFields.add(entityField);
						}
					}
				} else {
					if (!nonIdFields.add(entityField)) {
						// field could not be added since the set already contained a different object that is equal to the object to be added
						if (nonIdFields.remove(entityField)) {
							nonIdFields.add(entityField);
						}
					}
				}
			}
		}
		
		// finally, combine the two sets to have one single result in the desired order
		result.addAll(idFields);
		result.addAll(nonIdFields);

		return result;
	}

	/**
	 * @return the parent
	 */
	@Override
	public Entity getParent() {
		return parent;
	}

	/**
	 * @param parent the parent to set
	 */
	public void setParent(Entity parent) {
		super.setParent(parent);
		this.parent = parent;
	}
	

	/**
	 * @return all entity id entityFields
	 */
	public List<EntityField> getIdFields() {
		List<EntityField> result = new ArrayList<>();
		for (EntityField field : this.entityFields) {
			if (field.isPartOfId()) {
				result.add(field);
			}
		}
		return result;
	}

	/**
	 * @return
	 */
	public Set<EntityField> getAllIdFields() {
		Set<EntityField> result = new LinkedHashSet<>();

		Entity parent = this;
		while (parent != null) {
			result.addAll(parent.getIdFields());
			parent = parent.getParent();
		}

		return result;
	}
	
	public boolean hasIdFields() {
		return getAllIdFields().size() > 0;
	}
	
	/**
	 * @return all entity id entityFields
	 */
	public List<EntityField> getBusinessIdFields() {
		List<EntityField> result = new ArrayList<>();
		for (EntityField field : this.entityFields) {
			if (field.isPartOfBusinessId()) {
				result.add(field);
			}
		}
		return result;
	}

	/**
	 * @return
	 */
	public Set<EntityField> getAllBusinessIdFields() {
		Set<EntityField> result = new LinkedHashSet<>();

		Entity parent = this;
		while (parent != null) {
			result.addAll(parent.getBusinessIdFields());
			parent = parent.getParent();
		}

		return result;
	}
	
	public boolean hasBusinessIdFields() {
		return getAllBusinessIdFields().size() > 0;
	}
	
	/**
	 * @return
	 */
	public boolean hasEntityTypedFields() {
		for (EntityField entityField : this.getAllEntityFields()) {
			if (entityField.getType() instanceof Entity) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * @return
	 */
	public Collection<EntityField> getEntityTypedFields() {
		LinkedHashSet<EntityField> entityTypedFields = new LinkedHashSet<>();
		for (EntityField entityField : this.getAllEntityFields()) {
			if (entityField.getType() instanceof Entity) {
				entityTypedFields.add(entityField);
			}
		}
		
		return entityTypedFields;
	}
	
	/**
	 * @return
	 */
	public Collection<EntityField> getSingleValuedEntityTypedFields() {
		LinkedHashSet<EntityField> singleValuedEntityTypedFields = new LinkedHashSet<>();
		for (EntityField entityField : getEntityTypedFields()) {
			if (entityField.getCollectionType() == null) {
				singleValuedEntityTypedFields.add(entityField);
			}
		}
		
		return singleValuedEntityTypedFields;
	}
	
	/**
	 * @return
	 */
	public Collection<EntityField> getMandatorySingleValuedEntityTypedFields() {
		LinkedHashSet<EntityField> result = new LinkedHashSet<>();
		for (EntityField entityField : getSingleValuedEntityTypedFields()) {
			if (!entityField.isNullable()) {
				result.add(entityField);
			}
		}
		
		return result;
	}
	
	/**
	 * @return
	 */
	public Collection<EntityField> getMultiValuedEntityTypedFields() {
		LinkedHashSet<EntityField> singleValuedEntityTypedFields = new LinkedHashSet<>();
		for (EntityField entityField : getEntityTypedFields()) {
			if (entityField.getCollectionType() != null) {
				singleValuedEntityTypedFields.add(entityField);
			}
		}
		
		return singleValuedEntityTypedFields;
	}

	/**
	 * @return the mappedSuperclass
	 */
	public boolean isMappedSuperclass() {
		return mappedSuperclass;
	}

	/**
	 * @param mappedSuperclass the mappedSuperclass to set
	 */
	public void setMappedSuperclass(boolean mappedSuperclass) {
		this.mappedSuperclass = mappedSuperclass;
	}


    /**
     * @return derived value
     */
    public boolean canHaveIdGeneration() {
        boolean result = false;

        if (getParent() == null) {
            result = true;
        } else {
            if (isParentMappedSuperclass()) {
                result = true;
            }
        }

        return result;
    }

	/**
	 * @return
	 */
	public PersistenceModule getPersistenceModule() {
		return (PersistenceModule) this.getModule();
	}


	/**
	 * @return
	 */
	public boolean isParentMappedSuperclass() {
		return (getParent() != null && getParent().isMappedSuperclass());
	}

	/**
	 * Visit the annotations.
	 *
	 * @param visitor the visitor
	 * @return the visitor result
	 */
	@Override
	protected AcceptState visitChilds(ModelElementVisitorI visitor) {
		if (super.visitChilds(visitor) != AcceptState.INTERRUPT) {
			if (getParent() != null && getParent().accept(visitor) == AcceptState.INTERRUPT) {
				return AcceptState.INTERRUPT;
			}

			for (EntityField field : this.entityFields) {
				if (field.accept(visitor) == AcceptState.INTERRUPT) {
					return AcceptState.INTERRUPT;
				}
			}

			return AcceptState.CONTINUE;
		}
		return AcceptState.INTERRUPT;
	}

	/**
	 * @return the namespace
	 */
	public Namespace getNamespace() {
		return namespace;
	}

	/**
	 * @param namespace the namespace to set
	 */
	public void setNamespace(Namespace namespace) {
		this.namespace = namespace;
	}

	/**
	 * @param fieldName
	 * @return
	 */
	public EntityField getField(String fieldName) {
		for (EntityField field : this.entityFields) {
			if (field.getName().equalsIgnoreCase(fieldName)) {
				return field;
			}
		}
		return null;
	}

	/**
	 * @param fieldName
	 * @return
	 */
	public EntityField getFieldInParents(String fieldName) {
		for (EntityField field : this.getAllEntityFields()) {
			if (field.getName().equalsIgnoreCase(fieldName)) {
				return field;
			}
		}
		return null;
	}
	
	/**
	 * Utility entityFields are createdBy, modifiedBy, creationTimestamp, modificationTimestamp.
	 * If this flag is set to true, then all those entityFields are getting automatically generated.
	 *
	 * @return the utilityFields
	 */
	public boolean isUtilityFields() {
		return utilityFields;
	}

	/**
	 * @param utilityFields the utilityFields to set
	 */
	public void setUtilityFields(boolean utilityFields) {
		this.utilityFields = utilityFields;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.persistence.TypeI#isEntity()
	 */
	@Override
	public boolean isEntity() { return true; }

	/**
	 * @return
	 */
	public Set<EntityField> getAParentsIdFields() {
		Set<EntityField> result = new LinkedHashSet<>(getIdFields());

		if (this.getParent() != null) {
		    Entity parentEntity = this.getParent();

			while (parentEntity != null) {
				List<EntityField> idFields = parentEntity.getIdFields();
				if (idFields != null && idFields.size() > 0) {
					result.addAll(idFields);
					break;
				}
				parentEntity = parentEntity.getParent();
			}
		}

		return result;
	}

	/**
	 * @param entityField
	 */
	protected void addField(EntityField entityField) {
		if (entityField == null) throw new NullPointerException("parameter 'entityField' must not be null");
		this.entityFields.add(entityField);
	}

	public Map<StorageFunction, Set<ExceptionType>> getExceptions() {
		return exceptions;
	}
	
	public boolean addException(StorageFunction storageFunction, ExceptionType exceptionType) {
		Set<ExceptionType> exceptionTypes = exceptions.get(storageFunction);
		if (exceptionTypes == null) {
			exceptionTypes = new LinkedHashSet<>();
			exceptions.put(storageFunction, exceptionTypes);
		}
		
		return exceptionTypes.add(exceptionType);
	}

	public Set<StorageFunction> getStorageFunctions() {
		return storageFunctions;
	}
	
	public boolean addStorageFunction(StorageFunction storageFunction) {
		return this.storageFunctions.add(storageFunction);
	}
	
	/**
	 * @param storageFunction
	 * @return true if no storage function is configured or if the configured storage functions contain the given storage function
	 */
	public boolean hasStorageFunction(StorageFunction storageFunction) {
		return this.storageFunctions.size() == 0 ? true : this.storageFunctions.contains(storageFunction);
	}

	public Nature getNature() {
		return nature;
	}

	public void setNature(Nature nature) {
		this.nature = nature;
	}

	public Set<Entity> getConsumedEntities() {
		return consumedEntities;
	}
	
	public boolean addConsumedEntity(Entity entity) {
		return this.consumedEntities.add(entity);
	}
	
	/**
	 * Consumed model elements are model elements that this entity is related to and that
	 * get purpose by model element converters that inherit from the EntityConverter and thus
	 * get access to metatypes that are _not_ part of the persistence meta model (e.g. function).
	 * 
	 * At the time being, the types that are supposed to be added here are:
	 * - FunctionModule
	 * - ServiceInterface
	 * - Storage
	 * 
	 * Note that the concept of general "consumed model elements" has been invented since the persistence
	 * metamodel cannot have a dependency to the function metamodel. Else we would end with a bidirectional
	 * dependency. If we in the future merge all agnostic metamodels into one single agnostic metamodel,
	 * this concept should be replaced with "consumed function modules", "consumed service interfaces" and
	 * "consumed storages".  
	 * 
	 * @return
	 */
	public Set<ModelElement> getConsumedModelElements() {
		return consumedModelElements;
	}
	
	public boolean addConsumedModelElement(ModelElement modelElement) {
		return this.consumedModelElements.add(modelElement);
	}

	public Integer getNumberOfTestEntries() {
		return numberOfTestEntries;
	}

	public void setNumberOfTestEntries(Integer numberOfTestEntries) {
		this.numberOfTestEntries = numberOfTestEntries;
	}
	
	public Integer getNumberOfTestEntriesFromParent() {
		Entity parent = this.getParent();
		while (parent != null) {
			if (parent.getNumberOfTestEntries() != null) {
				return parent.getNumberOfTestEntries();
			}
			parent = parent.getParent();
		}
		
		return null;
	}
	
	/**
	 * @return
	 */
	public Set<EntityField> getFieldWithThisEntityAsType() {
		LinkedHashSet<EntityField> result = new LinkedHashSet<>();
		
		for (Entity entity : getModel().getElements(Entity.class)) {
			for (EntityField entityField : entity.getEntityFields()) {
				if (entityField.getType() == this) {
					result.add(entityField);
				}
			}
		}
		
		return result;
	}

	public boolean isCacheable() {
		return cacheable;
	}

	public void setCacheable(boolean cacheable) {
		this.cacheable = cacheable;
	}

	/**
	 * @return the tenantEntity
	 */
	public Entity getTenantEntity() {
		return tenantEntity;
	}

	/**
	 * @param tenantEntity the tenantEntity to set
	 */
	public void setTenantEntity(Entity tenantEntity) {
		this.tenantEntity = tenantEntity;
	}

	/**
	 * @return the userAccountEntity
	 */
	public Entity getUserAccountEntity() {
		return userAccountEntity;
	}

	/**
	 * @param userAccountEntity the userAccountEntity to set
	 */
	public void setUserAccountEntity(Entity userAccountEntity) {
		this.userAccountEntity = userAccountEntity;
	}

	/**
	 * @return
	 */
	public boolean isMultiTenancyAware() {
		return findFirstMultiTenancyAwareParent() != null;
	}
	
	/**
	 * @return
	 */
	public Entity findFirstMultiTenancyAwareParent() {
		if (this.getTenantEntity() != null) {
			return this;
		}
		
		Optional<ComplexType> firstParentWithTenantEntity = this.getAllParents().stream().filter(parentType -> {
			if (parentType instanceof Entity) {
				Entity parentEntityType = (Entity) parentType;
				if (parentEntityType.getTenantEntity() != null) {
					return true;
				}
			}
			return false;
			}).findFirst();
		
		return firstParentWithTenantEntity.isPresent() ?
				(Entity)firstParentWithTenantEntity.get() : null;
	}
	
	/**
	 * @return
	 */
	public Entity findFirstUserAccountAwareParent() {
		if (this.getUserAccountEntity() != null) {
			return this;
		}
		
		Optional<ComplexType> firstParentWithUserAccountEntity = this.getAllParents().stream().filter(parentType -> {
			if (parentType instanceof Entity) {
				Entity parentEntityType = (Entity) parentType;
				if (parentEntityType.getUserAccountEntity() != null) {
					return true;
				}
			}
			return false;
			}).findFirst();
		
		return firstParentWithUserAccountEntity.isPresent() ?
				(Entity)firstParentWithUserAccountEntity.get() : null;
	}

	/**
	 * @return the tenantEntityField
	 */
	public Field getTenantEntityField() {
		return tenantEntityField;
	}

	/**
	 * @param tenantEntityField the tenantEntityField to set
	 */
	public void setTenantEntityField(EntityField tenantEntityField) {
		this.tenantEntityField = tenantEntityField;
	}

	/**
	 * @return the userAccountEntityField
	 */
	public Field getUserAccountEntityField() {
		return userAccountEntityField;
	}

	/**
	 * @param userAccountEntityField the userAccountEntityField to set
	 */
	public void setUserAccountEntityField(EntityField userAccountEntityField) {
		this.userAccountEntityField = userAccountEntityField;
	}

	/**
	 * @return the userAccountEntityFieldForModifiedBy
	 */
	public EntityField getUserAccountEntityFieldForModifiedBy() {
		return userAccountEntityFieldForModifiedBy;
	}

	/**
	 * @param userAccountEntityFieldForModifiedBy the userAccountEntityFieldForModifiedBy to set
	 */
	public void setUserAccountEntityFieldForModifiedBy(EntityField userAccountEntityFieldForModifiedBy) {
		this.userAccountEntityFieldForModifiedBy = userAccountEntityFieldForModifiedBy;
	}

	@Override
	public void setModule(Module module) {
		if (module instanceof PersistenceModule) {
		    super.setModule(module);
		}
	}
}
