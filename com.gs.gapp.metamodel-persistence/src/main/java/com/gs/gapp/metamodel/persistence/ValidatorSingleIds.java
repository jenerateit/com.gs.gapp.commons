package com.gs.gapp.metamodel.persistence;

import java.util.Collection;
import java.util.LinkedHashSet;

import com.gs.gapp.metamodel.basic.ModelValidatorI;

/**
 * @author marcu
 *
 */
public class ValidatorSingleIds implements ModelValidatorI {

	public ValidatorSingleIds() {}

	@Override
	public Collection<Message> validate(Collection<Object> modelElements) {
		Collection<Message> result = new LinkedHashSet<>();
		result.addAll(assertExactlyOneIdField(modelElements));
		result.addAll(assertExactlyOneBusinessIdField(modelElements));
		return result;
	}

	/**
	 * @param rawElements
	 */
	private Collection<Message> assertExactlyOneIdField(Collection<Object> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();
		
		for (Object element : rawElements) {
			if (element instanceof Entity) {
				Entity entity = (Entity) element;
				if (entity.getAllIdFields().size() > 1) {
					StringBuilder sb = new StringBuilder("id fields:");
					String comma = "";
					for (EntityField field : entity.getAllIdFields()) {
						sb.append(comma).append(field.getOwner().getName()).append(".").append(field.getName());
						comma = ", ";
					}
					
					Message message = PersistenceMessage.MORE_THAN_ONE_ID_FIELD.getMessageBuilder()
							.parameters(entity.getName(), entity.getAllIdFields().size(), sb.toString())
							.build();
					result.add(message);
				}
			}
		}

		return result;
	}
	
	
	/**
	 * @param rawElements
	 */
	private Collection<Message> assertExactlyOneBusinessIdField(Collection<?> rawElements) {
		Collection<Message> result = new LinkedHashSet<>();
		
		for (Object element : rawElements) {
			if (element instanceof Entity) {
				Entity entity = (Entity) element;
				if (entity.getAllBusinessIdFields().size() > 1) {
					StringBuilder sb = new StringBuilder("id fields:");
					String comma = "";
					for (EntityField field : entity.getAllBusinessIdFields()) {
						sb.append(comma).append(field.getOwner().getName()).append(".").append(field.getName());
						comma = ", ";
					}
					
					Message message = PersistenceMessage.MORE_THAN_ONE_BUSINESS_ID_FIELD.getMessageBuilder()
							.parameters(entity.getName(), entity.getAllBusinessIdFields().size(), sb.toString())
							.build();
					result.add(message);
				}
			}
		}
		
		return result;
	}
}
