/*
 * CascadeType.java
 *
 * Created on 24. Oktober 2006, 21:12
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.gs.gapp.metamodel.persistence.enums;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author mmt
 */
public enum CascadeType {
    ALL ("all"),
    MERGE ("merge"),
    PERSIST ("persist"),
    REFRESH ("refresh"),
    REMOVE ("remove"),
    SETNULL ("setnull"),
    NONE ("none"),  // TODO we should be able to rename this to "none" since the parser reference resolvers are now improved (mmt 02-Aug-2013)
    ;
    
    private static final Map<String, CascadeType> stringToEnum =
		new LinkedHashMap<>();
	
	static {
		for (CascadeType cascadeTypes : values()) {
			stringToEnum.put(cascadeTypes.name, cascadeTypes);
		}
	}
	
	private final String name;

	/**
	 * @param name
	 */
	private CascadeType(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public static CascadeType fromString(String name) {
		return stringToEnum.get(name.toLowerCase());
	}
}
