package com.gs.gapp.metamodel.persistence;

/**
 * @author mmt
 *
 */
public interface TypeI {

	/**
	 * @return
	 */
	String getName();
	
	/**
	 * @return
	 */
	boolean isEntity();
}
