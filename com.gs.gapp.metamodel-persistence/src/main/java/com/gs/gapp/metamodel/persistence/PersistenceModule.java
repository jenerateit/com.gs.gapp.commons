/**
 *
 */
package com.gs.gapp.metamodel.persistence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import com.gs.gapp.metamodel.basic.ModelElement;
import com.gs.gapp.metamodel.basic.Module;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;

/**
 * @author mmt
 *
 */
public class PersistenceModule extends Module {

	/**
	 *
	 */
	private static final long serialVersionUID = 2898208776049989583L;

	/**
	 * @param name
	 */
	public PersistenceModule(String name) {
		super(name);
	}

	/**
	 * @return
	 */
	public Set<Entity> getEntities() {
		Set<Entity> result = new LinkedHashSet<>();

		for (ModelElement modelElement : getElements()) {
			if (modelElement instanceof Entity) {
				result.add((Entity) modelElement);
			} else if (modelElement instanceof Namespace) {
				result.addAll(((Namespace)modelElement).getEntities());
			}
		}

		return result;
	}
	
	/**
	 * A convenience method to get all entities sorted in their natural ordering.
	 *
	 * @return
	 */
	public Set<Entity> getEntitiesSorted() {
		ArrayList<Entity> elementsForSorting = new ArrayList<>();

		for (ModelElement modelElement : getElements()) {
			if (modelElement instanceof Entity) {
				elementsForSorting.add((Entity) modelElement);
			} else if (modelElement instanceof Namespace) {
				elementsForSorting.addAll(((Namespace)modelElement).getEntities());
			}
		}

		Collections.sort(elementsForSorting);
		Set<Entity> elements = new LinkedHashSet<>(elementsForSorting);
		
		
		return elements;
	}
	
	public Entity getEntity(String name) {
		for (Entity entity : getEntities()) {
			if (entity.getName().equalsIgnoreCase(name)) return entity;
		}
		
		return null;
	}
	

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#getNamespace()
	 */
	@Override
	public Namespace getNamespace() {
		return (Namespace) super.getNamespace();
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.basic.Module#setNamespace(com.gs.gapp.metamodel.basic.Namespace)
	 */
	@Override
	public void setNamespace(com.gs.gapp.metamodel.basic.Namespace namespace) {
		if (!(namespace instanceof Namespace)) {
			throw new IllegalArgumentException("the namespace of a PersistenceModule must be an instance of com.gs.gapp.metamodel.persistence.Namespace");
		}
		super.setNamespace(namespace);
	}
	
	/**
	 * @return
	 */
	public Set<ComplexType> getAllUsedComplexTypes() {
		final LinkedHashSet<ComplexType> result = new LinkedHashSet<>();
		this.getEntities().forEach(entity -> {
			result.add(entity);
			result.addAll(entity.getAllUsedComplexTypes());
		});
		return result;
	}
}
