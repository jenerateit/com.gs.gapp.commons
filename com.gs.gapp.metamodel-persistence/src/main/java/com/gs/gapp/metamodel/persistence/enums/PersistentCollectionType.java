/*
 * CascadeType.java
 *
 * Created on 24. Oktober 2006, 21:12
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.gs.gapp.metamodel.persistence.enums;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author mmt
 */
public enum PersistentCollectionType {
    ONE_TO_MANY ("onetomany"),
    ONE_TO_ONE ("onetoone"),
    MANY_TO_ONE ("manytoone"),
    MANY_TO_MANY ("manytomany"),
    ;
    
    private static final Map<String, PersistentCollectionType> stringToEnum =
		new LinkedHashMap<>();
	
	static {
		for (PersistentCollectionType collectionType : values()) {
			stringToEnum.put(collectionType.name, collectionType);
		}
	}
	
	private final String name;

	/**
	 * @param name
	 */
	private PersistentCollectionType(String name) {
		this.name = name;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name
	 * @return
	 */
	public static PersistentCollectionType fromString(String name) {
		return stringToEnum.get(name.toLowerCase());
	}
}
