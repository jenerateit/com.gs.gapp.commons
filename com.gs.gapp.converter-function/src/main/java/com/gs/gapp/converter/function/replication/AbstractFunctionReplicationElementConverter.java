package com.gs.gapp.converter.function.replication;

import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;

public abstract class AbstractFunctionReplicationElementConverter<S extends ModelElementI, T extends ModelElementI> extends
		AbstractM2MModelElementConverter<S, T> {

	
	/**
	 * @param modelConverter
	 * @param previousResultingElementRequired
	 * @param indirectConversionOnly
	 * @param createAndConvertInOneGo
	 */
	public AbstractFunctionReplicationElementConverter(
			AbstractConverter modelConverter,
			boolean previousResultingElementRequired,
			boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly,
				createAndConvertInOneGo);
	}

	/**
	 * @param modelConverter
	 */
	public AbstractFunctionReplicationElementConverter(
			AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/**
	 * @return
	 */
	protected ModelElementCache getModelElementCache() {
		return getModelConverter().getModelElementCache();
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#getModelConverter()
	 */
	@Override
	protected FunctionTypificationConverter getModelConverter() {
		return (FunctionTypificationConverter) super.getModelConverter();
	}
	

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#getConverterOptions()
	 */
	@Override
	protected FunctionTypificationConverterOptions getConverterOptions() {
		return getModelConverter().getConverterOptions();
	}

}
