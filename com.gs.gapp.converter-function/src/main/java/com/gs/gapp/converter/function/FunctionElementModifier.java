package com.gs.gapp.converter.function;

import javax.lang.model.type.PrimitiveType;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.dsl.jaxrs.JaxrsOptionEnum;
import com.gs.gapp.dsl.jaxrs.ParamTypeEnum;
import com.gs.gapp.dsl.rest.PurposeEnum;
import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionParameter;

public class FunctionElementModifier<S extends Function, T extends Function> extends
		AbstractM2MModelElementConverter<S, T> {

	public FunctionElementModifier(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		// --- add JAX-RS option values to be able to generate a useful JAX-RS implementation for the given model
		@SuppressWarnings("deprecation")
		OptionDefinitionString optionDefinitionParamType =
				new OptionDefinitionString(JaxrsOptionEnum.PARAM_TYPE.getName());
		OptionDefinition<String>.OptionValue optionValueParamType = null;


		OptionDefinition<String>.OptionValue optionValuePurpose =
				originalModelElement.getOption(RestOptionEnum.PURPOSE.getName(), String.class);
		if (optionValuePurpose != null) {
			PurposeEnum purposeEnum = PurposeEnum.getByName(optionValuePurpose.getOptionValue());
			switch (purposeEnum) {
			case CREATE:
				// nothing to be done
				break;
			case DELETE:
			case READ:
				if (originalModelElement.getFunctionOutParameters().size() == 0) {
					FunctionParameter outParameter = originalModelElement.getFunctionOutParameters().iterator().next();
					if (outParameter.getCollectionType() == null) {
						// in this case we read a single resource => key parameters could be sufficient as in-parameters
						for (FunctionParameter inParameter : originalModelElement.getFunctionInParameters()) {
							if (inParameter.getType() instanceof PrimitiveType) {
                                optionValueParamType = optionDefinitionParamType. new OptionValue(ParamTypeEnum.PATH_PARAM.getName());
                                inParameter.addOptions(optionValueParamType);
							}
						}
					}
				}
				break;
			case UPDATE:
				// nothing to be done
				break;
			default:
				throw new ModelConverterException("unhandled purpose enum entry found for function '" + originalModelElement + "':" + purposeEnum);
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		return (T) originalModelElement;
	}
}
