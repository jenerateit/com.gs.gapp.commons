/**
 *
 */
package com.gs.gapp.converter.function;

import java.util.List;

import com.gs.gapp.converter.analytics.AbstractAnalyticsConverter;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;

/**
 * @author mmt
 *
 */
public class FunctionJaxrsOptionsConverter extends AbstractAnalyticsConverter {

	/**
	 *
	 */
	public FunctionJaxrsOptionsConverter() {
		super(new ModelElementCache());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.BasicConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		result.add(new FunctionElementModifier<>(this));
		result.add(new FunctionModuleElementModifier<>(this));

		return result;
	}
}
