package com.gs.gapp.converter.function.replication;

import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;

public class FunctionToOutTypeConverter<S extends Function, T extends ComplexType> extends
		FunctionToTypeConverter<S, T> {

	public FunctionToOutTypeConverter(AbstractConverter modelConverter,
			boolean previousResultingElementRequired, boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly, createAndConvertInOneGo);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S function, T complexType) {
		super.onConvert(function, complexType);
		
		// --- function out parameters
		this.createFields(complexType, function.getFunctionOutParameters());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement,
			ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);
		if (result) {
			Function function = (Function) originalModelElement;
			if (function.getFunctionOutParameters().size() == 0) {
				result = false;
			} else {
//				for (FunctionParameter functionParameter : function.getFunctionOutParameters()) {
//					if (functionParameter.getType() instanceof Entity) {
//						result = false;
//						break;
//					}
//				}
			}
		}
		
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S function, ModelElementI previousResultingModelElement) {
		
		FunctionModule owner = null;
		
		if (previousResultingModelElement instanceof FunctionModule) {
			owner = (FunctionModule) previousResultingModelElement;
		} else {
			throw new ModelConverterException("no previous resulting model element of type 'FunctionModule' found", previousResultingModelElement);
		}
		
		T result = (T) new ComplexType(StringTools.firstUpperCase(function.getName()) + "OutParams");
		result.setModule(owner);
		owner.addElement(result);
		
		return result;
	}
}
