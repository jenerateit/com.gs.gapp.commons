package com.gs.gapp.converter.function.replication;

import java.io.Serializable;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.converter.persistence.basic.PersistenceToBasicConverterOptions;
import com.gs.gapp.converter.persistence.function.FunctionParamTypeEnum;

public class FunctionTypificationConverterOptions extends
		PersistenceToBasicConverterOptions {
	
	public static final String OPTION_DEFAULT_FUNCTION_PARAM_TYPE = "default-function-param-type";
	
	public static final String OPTION_GENERATE_BEANS_FOR_ENTITIES = "generate-beans-for-entities";

	public FunctionTypificationConverterOptions(ModelConverterOptions options) {
		super(options);
	}
	
	/**
	 * @return
	 */
	public FunctionParamTypeEnum getFunctionParamType() {
		String functionParamTypeString = (String) getOptions().get(OPTION_DEFAULT_FUNCTION_PARAM_TYPE);
		
		FunctionParamTypeEnum result = null;
		if (functionParamTypeString != null) {
			try {
		        result = FunctionParamTypeEnum.forName(functionParamTypeString);
			} catch (IllegalArgumentException ex) {
				throwIllegalEnumEntryException(functionParamTypeString, OPTION_DEFAULT_FUNCTION_PARAM_TYPE, FunctionParamTypeEnum.values());
			}
		}

		return result == null ? FunctionParamTypeEnum.COMPLEX_TYPE : result;
	}

	/**
	 * @return
	 */
	public boolean generateBeansForEntities() {
		Serializable rawOption = getOptions().get(OPTION_GENERATE_BEANS_FOR_ENTITIES);
		validateBooleanOption(rawOption, OPTION_GENERATE_BEANS_FOR_ENTITIES);	
		return rawOption == null || Boolean.parseBoolean((String) rawOption);
	}
}
