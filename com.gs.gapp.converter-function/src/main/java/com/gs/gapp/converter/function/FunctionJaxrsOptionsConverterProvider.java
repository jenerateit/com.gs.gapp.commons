/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.converter.function;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;

/**
 * @author hrr
 *
 */
public class FunctionJaxrsOptionsConverterProvider implements ModelConverterProviderI {

	/**
	 * 
	 */
	public FunctionJaxrsOptionsConverterProvider() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.modelconverter.ModelConverterProviderI#getModelConverter()
	 */
	@Override
	public ModelConverterI getModelConverter() {
		return new FunctionJaxrsOptionsConverter();
	}
}
