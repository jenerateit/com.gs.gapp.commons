package com.gs.gapp.converter.function.replication;

import java.util.Set;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.converter.persistence.function.FunctionParamTypeEnum;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.Type;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionParameter;
import com.gs.gapp.metamodel.persistence.Entity;

public abstract class FunctionToTypeConverter<S extends Function, T extends ComplexType> extends
    AbstractFunctionReplicationElementConverter<S, T> {

	public FunctionToTypeConverter(AbstractConverter modelConverter,
			boolean previousResultingElementRequired, boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly, createAndConvertInOneGo);
	}

	/**
	 * @param complexType
	 * @param functionParameters
	 */
	protected void createFields(T complexType, Set<FunctionParameter> functionParameters) {
		for (FunctionParameter functionParameter : functionParameters) {
			Type paramType = null;
			
			// TODO add additional logic to check whether function's owning function module is a boundary or a controller component (EBC pattern)
			if (functionParameter.getType() instanceof Entity) {
				FunctionParamTypeEnum functionParamType = getConverterOptions().getFunctionParamType();
				switch (functionParamType) {
				case COMPLEX_TYPE:
					Entity entity = (Entity) functionParameter.getType();
					ComplexType existingComplexType = entity.getSingleExtensionElement(ComplexType.class, true, complexType.getConversionDetails());
					if (existingComplexType == null) {
						// the existing complex type may have been converted within a different branch of the converter tree
						existingComplexType = entity.getSingleExtensionElement(ComplexType.class);
					}
					
					if (existingComplexType != null) {
					    paramType = existingComplexType;
					} else {
						paramType = this.convertWithOtherConverter(ComplexType.class, entity);
					}
					break;
				case ENTITY:
					paramType = functionParameter.getType();
					break;
				default:
					throw new ModelConverterException("unhandled function param type enum entry '" + functionParamType + "' (comes from converter option)");
				}
				
			} else {
				paramType = functionParameter.getType();
			}
			
			Field parameterField = new Field(functionParameter.getName(), complexType);
			parameterField.setType(paramType);
			parameterField.setOriginatingElement(functionParameter);
			parameterField.setCollectionType(functionParameter.getCollectionType());
		}
	}
}
