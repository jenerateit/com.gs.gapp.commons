/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */
package com.gs.gapp.converter.function.replication;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;

/**
 * @author hrr
 *
 */
public class FunctionTypificationConverterProvider implements ModelConverterProviderI {

	/**
	 * 
	 */
	public FunctionTypificationConverterProvider() {
		super();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.modelconverter.ModelConverterProviderI#getModelConverter()
	 */
	@Override
	public ModelConverterI getModelConverter() {
		return new FunctionTypificationConverter();
	}
}
