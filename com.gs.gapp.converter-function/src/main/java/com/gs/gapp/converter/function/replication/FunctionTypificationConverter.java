/**
 *
 */
package com.gs.gapp.converter.function.replication;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.gs.gapp.converter.persistence.basic.AbstractPersistenceToBasicElementConverter;
import com.gs.gapp.converter.persistence.basic.PersistenceToBasicConverter;
import com.gs.gapp.converter.persistence.function.FunctionParamTypeEnum;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

/**
 * @author mmt
 *
 */
public class FunctionTypificationConverter extends /*AbstractAnalyticsConverter*/ PersistenceToBasicConverter {

	
	private FunctionTypificationConverterOptions converterOptions;
	
	/**
	 *
	 */
	public FunctionTypificationConverter() {
		super(new ModelElementCache());
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.BasicConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = super.onGetAllModelElementConverters();

		// --- master element converters
		result.add(new FunctionModuleReplicationConverter<>(this));
		

		// --- slave element converters
		result.add(new FunctionReplicationConverter<>(this, true, true, true));
		result.add(new FunctionToInTypeConverter<>(this, true, true, true));
		result.add(new FunctionToOutTypeConverter<>(this, true, true, true));
		
		// --- in case we use pure entity classes  instead of Java beans with JAXB annotations, we simply remove model element converters that have the capability to provide the JAXB beans (mmt 16-Mar-2015)
		if (converterOptions.getFunctionParamType() == FunctionParamTypeEnum.ENTITY) {
			for (AbstractModelElementConverter<? extends Object, ? extends ModelElementI> elementConverter : new ArrayList<>(result)) {
				if (elementConverter instanceof AbstractPersistenceToBasicElementConverter) {
					result.remove(elementConverter);
				}
			}
		}
			
			
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onPerformModelConsolidation(java.util.Set)
	 */
	@Override
	protected Set<Object> onPerformModelConsolidation(Set<?> normalizedElements) {

		Set<Object> result =  super.onPerformModelConsolidation(normalizedElements);
		
		// --- re-add all elements that serve as input for this model converter (pass-through functionality), exception: model element of type 'Model'
		for (Object normalizedElement : normalizedElements) {
			if (normalizedElement instanceof Model) continue;
			
			if (normalizedElement instanceof PersistenceModule) {
			    PersistenceModule persistenceModule = (PersistenceModule) normalizedElement;
				result.add(persistenceModule);
				
				if (getConverterOptions().generateBeansForEntities() == false) {
					for (Entity entity : persistenceModule.getEntities()) {
						if (entity.getSingleExtensionElement(ComplexType.class) != null) {
							ComplexType complexType = entity.getSingleExtensionElement(ComplexType.class);
							complexType.setGenerated(false);
							if (complexType.getSingleExtensionElement(ComplexType.class) != null) {
								ComplexType complexType2 = complexType.getSingleExtensionElement(ComplexType.class);
								complexType2.setGenerated(false);
							}
						}
					}
				}
			}
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.persistence.basic.PersistenceToBasicConverter#getConverterOptions()
	 */
	@Override
	public FunctionTypificationConverterOptions getConverterOptions() {
		if (this.converterOptions == null) {
			this.converterOptions = new FunctionTypificationConverterOptions(getOptions());
		}
		return this.converterOptions;
	}
	
	
}
