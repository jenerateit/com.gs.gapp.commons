package com.gs.gapp.converter.function.replication;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.Namespace;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.PersistenceModule;

public class FunctionModuleReplicationConverter<S extends FunctionModule, T extends FunctionModule> extends
		AbstractFunctionReplicationElementConverter<S, T> {

	public FunctionModuleReplicationConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);
		
		// --- namespace
		Namespace functionNamespace = new Namespace(originalModelElement.getNamespace().getName());
		resultingModelElement.setNamespace(functionNamespace);

		// --- functions
		for (Function function : originalModelElement.getFunctions()) {
			@SuppressWarnings("unused")
			Function newFunction = this.convertWithOtherConverter(Function.class, function, resultingModelElement);
		}
		
		// --- persistence modules
		for (PersistenceModule persistenceModule : originalModelElement.getPersistenceModules()) {
		    resultingModelElement.addPersistenceModule(persistenceModule);
		}
		
		// --- consumed entities
		for (Entity consumedEntity : originalModelElement.getConsumedEntities()) {
			resultingModelElement.addConsumedEntity(consumedEntity);
		}
		
		// --- consumed function modules
        for (FunctionModule consumedFunctionModule : originalModelElement.getConsumedFunctionModules()) {
        	FunctionModule replicatedFunctionModule = this.convertWithOtherConverter(FunctionModule.class, consumedFunctionModule);
			resultingModelElement.addConsumedFunctionModule(replicatedFunctionModule);
		}

	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		T result = (T) new FunctionModule(originalModelElement.getName());
		
		return result;
	}
}
