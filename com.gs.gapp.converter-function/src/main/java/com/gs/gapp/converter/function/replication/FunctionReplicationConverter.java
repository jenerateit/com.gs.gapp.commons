package com.gs.gapp.converter.function.replication;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ParameterType;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.BusinessException;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.FunctionParameter;
import com.gs.gapp.metamodel.function.TechnicalException;
import com.gs.gapp.metamodel.persistence.Entity;

public class FunctionReplicationConverter<S extends Function, T extends Function> extends
    AbstractFunctionReplicationElementConverter<S, T> {

	public FunctionReplicationConverter(AbstractConverter modelConverter,
			boolean previousResultingElementRequired, boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly, createAndConvertInOneGo);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);
		
		// --- function in parameters
		ComplexType complexTypeForInParameters =
				this.convertWithOtherConverter(ComplexType.class, originalModelElement, resultingModelElement.getModule(), new Class<?>[] {FunctionToInTypeConverter.class});
		if (complexTypeForInParameters != null) {
			FunctionParameter singleInParameter = new FunctionParameter("parameters");
			singleInParameter.setType(complexTypeForInParameters);
			resultingModelElement.addFunctionInParameter(singleInParameter);
			
			for (FunctionParameter functionParameter : originalModelElement.getFunctionInParameters()) {
				if (functionParameter.getType() instanceof Entity) {
					FunctionParameter inParameterWithEntityType = new FunctionParameter(functionParameter.getName());
					inParameterWithEntityType.setParameterType(ParameterType.IN);
					inParameterWithEntityType.setType(functionParameter.getType());
					inParameterWithEntityType.setCollectionType(functionParameter.getCollectionType());
					resultingModelElement.addFunctionInParameter(inParameterWithEntityType);
				}
			}
		}
		
		// --- function out parameters
		ComplexType complexTypeForOutParameters =
				this.convertWithOtherConverter(ComplexType.class, originalModelElement, resultingModelElement.getModule(), new Class<?>[] {FunctionToOutTypeConverter.class});
		if (complexTypeForOutParameters != null) {
			FunctionParameter singleOutParameter = new FunctionParameter("result");
			singleOutParameter.setParameterType(ParameterType.OUT);
			singleOutParameter.setType(complexTypeForOutParameters);
			resultingModelElement.addFunctionOutParameter(singleOutParameter);
		}
		
		// --- exceptions
		for (BusinessException businessException : originalModelElement.getBusinessExceptions()) {
			resultingModelElement.addBusinessException(businessException);  // TODO check whether we have to create new instances of the business exceptions instead of reusing the original ones (mmt 25-Oct-2013)
		}
		
		for (TechnicalException technicalException : originalModelElement.getTechnicalExceptions()) {
			resultingModelElement.addTechnicalException(technicalException);  // TODO check whether we have to create new instances of the business exceptions instead of reusing the original ones (mmt 07-Mar-2017))
		}
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		
		FunctionModule owner = null;
		
		if (previousResultingModelElement instanceof FunctionModule) {
			owner = (FunctionModule) previousResultingModelElement;
		} else {
			throw new ModelConverterException("no previous resulting model element of type 'FunctionModule' found", previousResultingModelElement);
		}
		
		T result = (T) new Function(originalModelElement.getName(), owner);
		
		return result;
	}
}
