package com.gs.gapp.converter.function;

import com.gs.gapp.dsl.jaxrs.JaxrsOptionEnum;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionBoolean;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.function.FunctionModule;

public class FunctionModuleElementModifier<S extends FunctionModule, T extends FunctionModule> extends
		AbstractM2MModelElementConverter<S, T> {

	public FunctionModuleElementModifier(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);

		// --- add JAX-RS option values to be able to generate a useful JAX-RS implementation for the given model
		OptionDefinitionBoolean optionDefinitionAnnotationsInInterface =
				new OptionDefinitionBoolean(JaxrsOptionEnum.ANNOTATIONS_IN_INTERFACE.getName());
		OptionDefinitionBoolean.OptionValue optionValueAnnotationsInInterface =
				optionDefinitionAnnotationsInInterface. new OptionValue(true);

		resultingModelElement.addOptions(optionValueAnnotationsInInterface);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		return (T) originalModelElement;
	}
}
