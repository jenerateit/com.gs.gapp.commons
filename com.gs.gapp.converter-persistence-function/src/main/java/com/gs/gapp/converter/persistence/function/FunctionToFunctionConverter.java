package com.gs.gapp.converter.persistence.function;

import java.util.stream.Stream;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.persistence.Entity;

/**
 * This converter checks whether a function has entity-typed parameters where the parameter type
 * has to be replaced to get a complex type.
 *
 * @param <S>
 */
public class FunctionToFunctionConverter<S extends Function> extends AbstractPersistenceToFunctionElementConverter<S, S> {

	public FunctionToFunctionConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY___CREATE_AND_CONVERT_IN_ONE_GO);
	}

	@Override
	protected S onCreateModelElement(S function, ModelElementI previousResultingModelElement) {
		return function;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElementI, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected void onConvert(S sourceFunction, S targetFunction) {
		// Find out, for which functions we have to replace the function parameter type.
		// In some cases we have to replace an Entity-type with a ComplexType.
		
		Stream.of(sourceFunction.getFunctionInParameters(), sourceFunction.getFunctionOutParameters())
		    .flatMap(aCollection -> aCollection.stream())
		    .filter(parameter -> parameter.getType() instanceof Entity)
		    .forEach(parameter -> {
		    	Entity entity = (Entity) parameter.getType();
		    	ComplexType complexType = convertWithOtherConverter(ComplexType.class, entity);
		    	if (complexType != null) {
		    		// Only in this case, we have to replace the entity type with the complex type.
		    		// The converter logic in EntityToComplexTypeConverter servers us well here.
		    		// There is no duplication of this logic needed.
		    		// Note that EntityToComplexTypeConverter also takes care of converting the PeristenceModule to a basic Module.
		    		parameter.setType(complexType);
		    	}
		    });
	}
}
