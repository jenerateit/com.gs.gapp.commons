package com.gs.gapp.converter.persistence.function;

import java.io.Serializable;

import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.converter.persistence.basic.PersistenceToBasicConverterOptions;

public class PersistenceToFunctionConverterOptions extends PersistenceToBasicConverterOptions {
	
	public static final String PARAMETER_FUNCTION_PARAM_TYPE = "function-param-type";
	public static final String PARAMETER_NAMESPACE_POSTFIX = "namespace-postfix";
	public static final String PARAMETER_USE_INTERFACES = "use-interfaces";
	public static final String PARAMETER_PASSTHROUGH_INCOMING_ELEMENTS = "passthrough.incoming.elements";
	public static final String PARAMETER_MAKE_ENTITY_TYPED_PARAMS_COMPLEX_TYPED = "make-entity-typed-params-complex-typed";
	
	public PersistenceToFunctionConverterOptions(ModelConverterOptions options) {
		super(options);
	}

	/**
	 * @return
	 */
	public FunctionParamTypeEnum getFunctionParamType() {
		String functionParamTypeString = (String) getOptions().get(PARAMETER_FUNCTION_PARAM_TYPE);
		FunctionParamTypeEnum result = null;
		
		if (functionParamTypeString != null) {
			try {
			    result = FunctionParamTypeEnum.forName(functionParamTypeString);
			} catch (IllegalArgumentException ex) {
				throwIllegalEnumEntryException((String) functionParamTypeString, PARAMETER_FUNCTION_PARAM_TYPE, FunctionParamTypeEnum.values());
			}
		}

		return result == null ? FunctionParamTypeEnum.COMPLEX_TYPE : result;
	}
	
	/**
	 * @return
	 */
	@Override
	public String getNamespacePostfix() {
		Serializable namespacePostfix = getOptions().get(PARAMETER_NAMESPACE_POSTFIX);

		String result = namespacePostfix == null || namespacePostfix.toString().length() == 0 ? null : namespacePostfix.toString();
		if (result != null && result.startsWith(".") == false) result = "." + result;
		return result;
	}
	
	/**
	 * @return
	 */
	public boolean areInterfacesUsed() {
		String useInterfaces = (String) getOptions().get(PARAMETER_USE_INTERFACES);
		validateBooleanOption(useInterfaces, PARAMETER_USE_INTERFACES);
		return useInterfaces != null && useInterfaces.length() > 0 ?
			Boolean.parseBoolean(useInterfaces) : true;
	}
	
	/**
	 * @return
	 */
	public boolean areIncomingElementsPassedThrough() {
		String passthrough = (String) getOptions().get(PARAMETER_PASSTHROUGH_INCOMING_ELEMENTS);
		validateBooleanOption(passthrough, PARAMETER_PASSTHROUGH_INCOMING_ELEMENTS);
		return passthrough != null && passthrough.length() > 0 ?
			Boolean.parseBoolean(passthrough) : false;
	}
	
	/**
	 * @return
	 */
	public boolean areEntityTypedParamsMadeComplexTyped() {
		String replaceParamType = (String) getOptions().get(PARAMETER_MAKE_ENTITY_TYPED_PARAMS_COMPLEX_TYPED);
		validateBooleanOption(replaceParamType, PARAMETER_MAKE_ENTITY_TYPED_PARAMS_COMPLEX_TYPED);
		return replaceParamType != null && replaceParamType.length() > 0 ?
			Boolean.parseBoolean(replaceParamType) : false;
	}
}
