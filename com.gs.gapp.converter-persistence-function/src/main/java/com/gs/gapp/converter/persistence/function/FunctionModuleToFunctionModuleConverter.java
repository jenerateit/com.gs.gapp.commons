package com.gs.gapp.converter.persistence.function;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionModule;

/**
 * This converter systematically checks all functions of a function module
 * to find out whether there are parameters that are entity-typed where the
 * parameter typ has to be replaced with a complex type. 
 * 
 * @param <S>
 */
public class FunctionModuleToFunctionModuleConverter<S extends FunctionModule> extends AbstractPersistenceToFunctionElementConverter<S, S> {

	public FunctionModuleToFunctionModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.CREATE_AND_CONVERT_IN_ONE_GO);
	}

	@Override
	protected S onCreateModelElement(S functionModule, ModelElementI previousResultingModelElement) {
		return functionModule;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElementI, com.gs.gapp.metamodel.basic.ModelElementI)
	 */
	@Override
	protected void onConvert(S sourceFunctionModule, S targetFunctionModule) {
		sourceFunctionModule.getFunctions().stream()
		    .forEach(function -> convertWithOtherConverter(Function.class, function));
	}
}
