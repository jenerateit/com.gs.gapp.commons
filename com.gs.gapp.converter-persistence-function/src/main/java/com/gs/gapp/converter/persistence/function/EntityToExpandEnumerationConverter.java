package com.gs.gapp.converter.persistence.function;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.EnumerationEntry;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.persistence.Entity;

public class EntityToExpandEnumerationConverter<S extends Entity, T extends Enumeration> extends AbstractPersistenceToFunctionElementConverter<S, T> {

	public EntityToExpandEnumerationConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
	}

	@Override
	protected T onCreateModelElement(S entity, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T enumeration = (T) new Enumeration(entity.getName() + "ExpandableEnum");
		enumeration.setModule(entity.getModule());
		fillExpandableEnum(enumeration, entity, null, 0);
		return enumeration;
	}

	
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement, Class<? extends ModelElementI> resultClass) {
		boolean responsible = super.isResponsibleFor(originalModelElement, previousResultingModelElement, resultClass);
		if (responsible && resultClass == Enumeration.class) {
			@SuppressWarnings("unchecked")
			T expandableEnum = onCreateModelElement((S) originalModelElement, null);
			return !expandableEnum.getEnumerationEntries().isEmpty();
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	private boolean fillExpandableEnum(T expandableEnum, S type, String qualifier, int depth) {
		boolean expandable = false;
		for (Field field : type.getFields()) {
			if (field.getExpandable() != null && field.getExpandable()) {
				String entryName = qualifier == null || qualifier.isEmpty() ? field.getName() : qualifier + "_" + field.getName();
				EnumerationEntry enumerationEntry = new EnumerationEntry(entryName);
				expandableEnum.addEnumerationEntry(enumerationEntry);
				expandable = true;
			}
			if (depth < 3 && field.getType() instanceof Entity) {
				Entity fieldType = (Entity) field.getType();
				if (fillExpandableEnum(expandableEnum, (S) fieldType, field.getName(), depth + 1)) {
					expandable = true;
				}
			}
		}
		return expandable;
	}
}
