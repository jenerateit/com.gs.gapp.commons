package com.gs.gapp.converter.persistence.function;

import com.gs.gapp.converter.persistence.basic.NamespaceToBasicNamespaceConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.Namespace;

public class NamespaceToFunctionNamespaceConverter<S extends com.gs.gapp.metamodel.persistence.Namespace, T extends Namespace> extends
     NamespaceToBasicNamespaceConverter<S, T> {
//    AbstractPersistenceToFunctionElementConverter<S, T> {

	public NamespaceToFunctionNamespaceConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S originalModelElement, T resultingModelElement) {
		super.onConvert(originalModelElement, resultingModelElement);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S originalModelElement, ModelElementI previousResultingModelElement) {
		
		String postfix = getConverterOptions().getNamespacePostfix() == null ? "" : getConverterOptions().getNamespacePostfix();
		
		@SuppressWarnings("unchecked")
		T result = (T) new Namespace(originalModelElement.getName() + postfix);
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#getModelConverter()
	 */
	@Override
	protected PersistenceToFunctionConverter getModelConverter() {
		return (PersistenceToFunctionConverter) super.getModelConverter();
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#getConverterOptions()
	 */
	@Override
	protected PersistenceToFunctionConverterOptions getConverterOptions() {
		return getModelConverter().getConverterOptions();
	}
}
