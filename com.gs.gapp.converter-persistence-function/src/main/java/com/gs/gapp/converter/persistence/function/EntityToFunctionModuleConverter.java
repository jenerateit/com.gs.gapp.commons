package com.gs.gapp.converter.persistence.function;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.jenerateit.modelconverter.ModelConverterException;

import com.gs.gapp.dsl.ejb.EjbImplementationEnum;
import com.gs.gapp.dsl.ejb.EjbInterfacesEnum;
import com.gs.gapp.dsl.ejb.EjbOptionEnum;
import com.gs.gapp.dsl.rest.PurposeEnum;
import com.gs.gapp.dsl.rest.RestOptionEnum;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.ModelValidatorI.Message;
import com.gs.gapp.metamodel.basic.ParameterType;
import com.gs.gapp.metamodel.basic.options.OptionDefinition;
import com.gs.gapp.metamodel.basic.options.OptionDefinition.OptionValue;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionBoolean;
import com.gs.gapp.metamodel.basic.options.OptionDefinitionString;
import com.gs.gapp.metamodel.basic.typesystem.CollectionType;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.basic.typesystem.Enumeration;
import com.gs.gapp.metamodel.basic.typesystem.ExceptionType;
import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.BusinessException;
import com.gs.gapp.metamodel.function.Function;
import com.gs.gapp.metamodel.function.FunctionEnum;
import com.gs.gapp.metamodel.function.FunctionModule;
import com.gs.gapp.metamodel.function.FunctionParameter;
import com.gs.gapp.metamodel.function.Namespace;
import com.gs.gapp.metamodel.function.ServiceModelProcessing;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.EntityField;
import com.gs.gapp.metamodel.persistence.PersistenceMessage;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.gs.gapp.metamodel.persistence.enums.StorageFunction;
import com.gs.gapp.metamodel.product.Capability;

public class EntityToFunctionModuleConverter<S extends Entity, T extends FunctionModule> extends
    AbstractPersistenceToFunctionElementConverter<S, T> {


	public EntityToFunctionModuleConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S entity, T functionModule) {
		super.onConvert(entity, functionModule);
		
		Capability capability = getModelConverter().getCapabilityContext();
		
		// --- determine the EJB implementation pattern by checking whether JPA entity classes have been created for the modeled entities
		boolean entityIsUsedForStorage = true;
		if (capability != null) {
			entityIsUsedForStorage = false;
			
			for (PersistenceModule persistenceModule : capability.getAllPersistenceModulesInStorages()) {
				if (persistenceModule.getEntities().contains(entity)) {
					entityIsUsedForStorage = true;
					break;
				}
			}
		}
		
		// --- control which implementation is going to be generated inside methods of EJBs (mmt 02-Apr-2019)
		// --- TODO Persistence-to-Function converter should not include EJB specific stuff => find a better solution, e.g. moving this logic to FunctionToEJBConverter
		OptionDefinition<String> definition = EjbOptionEnum.EJB_IMPLEMENTATION_FOR_ENTITY.getDefinition(String.class);
		if (entityIsUsedForStorage) {
			@SuppressWarnings("rawtypes")
			OptionValue optionValue = definition.new OptionValue(EjbImplementationEnum.PERSISTENCE_JPA.getName());
			functionModule.addOptions(optionValue);
		} else {
			@SuppressWarnings("rawtypes")
			OptionValue optionValue = definition.new OptionValue(EjbImplementationEnum.SKELETON.getName());
			functionModule.addOptions(optionValue);
		}
		
		// --- predefine media types that are consumed and produced by the function module
		ServiceModelProcessing.addProducedStandardMediaTypesIfNotPresent(functionModule);
		ServiceModelProcessing.addConsumedStandardMediaTypesIfNotPresent(functionModule);
		
		if (getConverterOptions().areInterfacesUsed()) {
			// --- ensure that there is a local interface generated for the EJB
			OptionDefinitionString optionDefinitionEjbInterfaces = new OptionDefinitionString(EjbOptionEnum.EJB_INTERFACES.getName());
			OptionDefinition<String>.OptionValue useLocalInterface = optionDefinitionEjbInterfaces.new OptionValue(EjbInterfacesEnum.LOCAL.getName());
			functionModule.addOptions(useLocalInterface);
		}

		FunctionParamTypeEnum functionParamType = getConverterOptions().getFunctionParamType();
		
		// --- set namespace for function module
		Namespace namespace = this.convertWithOtherConverter(Namespace.class, entity.getModule().getNamespace());
		functionModule.setNamespace(namespace);
		namespace.addElement(functionModule);
		
		// --- add persistence modules
		addPersistenceModule(entity, functionModule);

		// --- check the entity's id fields
		// TODO entities with composite ids are not handled properly so far
		List<EntityField> idFields = new ArrayList<>();
		idFields.addAll(entity.getAllIdFields());
		if (idFields.isEmpty()) {
			Message error = PersistenceMessage.NO_ID_FIELD.getMessageBuilder()
					.modelElement(entity)
					.parameters(entity)
					.build();
			throw new ModelConverterException(error.getMessage());
		}

        // ---------------------------------------------------------------
		// --- add a set of functions for every entity
		OptionDefinitionBoolean optionDefinitionIdempotent = new OptionDefinitionBoolean(RestOptionEnum.IDEMPOTENT.getName());
		OptionDefinitionString optionDefinitionPurpose = new OptionDefinitionString(RestOptionEnum.PURPOSE.getName());
		OptionDefinitionBoolean.OptionValue optionValueIdempotent = null;
		OptionDefinitionString.OptionValue optionValuePurpose = null;

		if (entity.hasStorageFunction(StorageFunction.DELETE)) {
			// --- delete
			optionValuePurpose = optionDefinitionPurpose. new OptionValue(PurposeEnum.DELETE.getName());
	
			Function deleteFunction = new Function(FunctionEnum.DELETE.getName(), functionModule);
			setConversionDetails(deleteFunction);
			deleteFunction.addOptions(optionValuePurpose);
			// TODO should we rather return a status code 204 (NO CONTENT)? (mmt 18-Jul-2022)
			ServiceModelProcessing.setStatusCodeIfNotPresent(deleteFunction, 200L);
			deleteFunction.setStorageFunction(StorageFunction.DELETE);
			addBusinessExceptions(entity.getExceptions().get(StorageFunction.DELETE), deleteFunction);
			deleteFunction.setOriginatingElement(entity);
			FunctionParameter functionParameter = null;
			if (!idFields.isEmpty()) {
	            for (EntityField idField : idFields) {
	            	String paramName = idFields.size() == 1 ? FunctionEnum.PARAM_NAME_ID : idField.getName();
	            	functionParameter = new FunctionParameter(paramName);
	            	functionParameter.setParameterType(ParameterType.IN);
	            	functionParameter.setType(idField.getType());
	            	functionParameter.setOriginatingElement(idField);
	            	functionParameter.addOptions( RestOptionEnum.PARAM_TYPE.getDefinition(String.class). new OptionValue(RestOptionEnum.ParamTypeEnum.PATH.getName()));
	            	ServiceModelProcessing.addConsumedStandardMediaTypesIfNotPresent(functionParameter);
	            	deleteFunction.addFunctionInParameter(functionParameter);
	            }
	            FunctionParameter outParameter = new FunctionParameter("result");
	            outParameter.setParameterType(ParameterType.OUT);
	            outParameter.setType(PrimitiveTypeEnum.UINT1.getPrimitiveType());
	            outParameter.setOriginatingElement(entity);
			}
		}

		if (entity.hasStorageFunction(StorageFunction.READ)) {
			// --- read
			optionValuePurpose = optionDefinitionPurpose. new OptionValue(PurposeEnum.READ.getName());
	
			Function readFunction = new Function(FunctionEnum.READ.getName(), functionModule);
			setConversionDetails(readFunction);
			ServiceModelProcessing.addProducedStandardMediaTypesIfNotPresent(readFunction);
			readFunction.addOptions(optionValuePurpose);
			ServiceModelProcessing.setStatusCodeIfNotPresent(readFunction, 200L);
			readFunction.setStorageFunction(StorageFunction.READ);
			readFunction.setOriginatingElement(entity);
			addBusinessExceptions(entity.getExceptions().get(StorageFunction.READ), readFunction);
			if (!idFields.isEmpty()) {
				FunctionParameter functionParameter = null;
	            for (EntityField idField : idFields) {
	            	String paramName = idFields.size() == 1 ? FunctionEnum.PARAM_NAME_ID : idField.getName();
	            	functionParameter = new FunctionParameter(paramName);
	            	functionParameter.setParameterType(ParameterType.IN);
	            	functionParameter.setType(idField.getType());
	            	functionParameter.setOriginatingElement(idField);
	            	functionParameter.addOptions( RestOptionEnum.PARAM_TYPE.getDefinition(String.class). new OptionValue(RestOptionEnum.ParamTypeEnum.PATH.getName()));
	            	readFunction.addFunctionInParameter(functionParameter);
	            }
	            Enumeration expandEnumeration = convertWithOtherConverter(Enumeration.class, entity);
	            if (expandEnumeration != null) {
	            	functionParameter = new FunctionParameter("expand");
	            	functionParameter.setParameterType(ParameterType.IN);
	            	functionParameter.setType(expandEnumeration);
	            	functionParameter.addOptions( RestOptionEnum.PARAM_TYPE.getDefinition(String.class). new OptionValue(RestOptionEnum.ParamTypeEnum.QUERY.getName()));
	            	readFunction.addFunctionInParameter(functionParameter);
	            	
	            }
	            FunctionParameter outParameter = new FunctionParameter("result");
	            outParameter.setParameterType(ParameterType.OUT);
	
	            switch (functionParamType) {
				case COMPLEX_TYPE:
					ComplexType resultType = this.convertWithOtherConverter(ComplexType.class, entity);
		            if (resultType != null) {
		                outParameter.setType(resultType);
		            } else {
		            	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
		            }
					break;
				case ENTITY:
					outParameter.setType(entity);
					break;
				default:
					throw new ModelConverterException("unhandled function param type '" + functionParamType + "'");
	            }
	
	            outParameter.setOriginatingElement(entity);
	            readFunction.addFunctionOutParameter(outParameter);
			}
		}
		
		
		/* Temporarily disabled due to AJAX-156 (mmt 17-Feb-2020)
		if (entity.hasStorageFunction(StorageFunction.DELETE_ALL)) {
			// --- Delete All
			// AJAX-145 (the following line was commented before)
			optionValuePurpose = optionDefinitionPurpose. new OptionValue(PurposeEnum.DELETE.getName());
		
			Function deleteAllFunction = new Function(FunctionEnum.DELETE_ALL.getName(), functionModule);
			setConversionDetails(deleteAllFunction);
			deleteAllFunction.addOptions(optionValuePurpose);
			deleteAllFunction.setStorageFunction(StorageFunction.DELETE_ALL);
			deleteAllFunction.setOriginatingElement(entity);
			addBusinessExceptions(entity.getExceptions().get(StorageFunction.DELETE_ALL), deleteAllFunction);
		
			FunctionParameter functionParameter = null;
			
			functionParameter = new FunctionParameter(FunctionEnum.PARAM_NAME_BEANS);
			ComplexType resultType = this.convertWithOtherConverter(ComplexType.class, entity);
	        if (resultType != null) {
	            functionParameter.setType(resultType);
	        } else {
	        	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
	        }
		    functionParameter.setCollectionType(CollectionType.LIST);
			functionParameter.setOriginatingElement(entity);
			deleteAllFunction.addFunctionInParameter(functionParameter);
		}
		*/

		if (entity.hasStorageFunction(StorageFunction.READ_MANY)) {
			// --- readList
			optionValuePurpose = optionDefinitionPurpose. new OptionValue(PurposeEnum.READ.getName());
		
			Function readListFunction = new Function(FunctionEnum.READ_LIST.getName(), functionModule);
			setConversionDetails(readListFunction);
			ServiceModelProcessing.addProducedStandardMediaTypesIfNotPresent(readListFunction);
			readListFunction.addOptions(optionValuePurpose);
			ServiceModelProcessing.setStatusCodeIfNotPresent(readListFunction, 200L);
			readListFunction.setStorageFunction(StorageFunction.READ_MANY);
			readListFunction.setOriginatingElement(entity);
			addBusinessExceptions(entity.getExceptions().get(StorageFunction.READ_MANY), readListFunction);
		
			FunctionParameter functionParameter = null;
			
			// - in-parameters
			functionParameter = new FunctionParameter(FunctionEnum.PARAM_NAME_OFFSET);
		    functionParameter.setType(PrimitiveTypeEnum.SINT32.getPrimitiveType());
			functionParameter.setOriginatingElement(entity);
			functionParameter.addOptions( RestOptionEnum.PARAM_TYPE.getDefinition(String.class). new OptionValue(RestOptionEnum.ParamTypeEnum.QUERY.getName()));
			readListFunction.addFunctionInParameter(functionParameter);
		
			functionParameter = new FunctionParameter(FunctionEnum.PARAM_NAME_LIMIT);
		    functionParameter.setType(PrimitiveTypeEnum.SINT32.getPrimitiveType());
			functionParameter.setOriginatingElement(entity);
			functionParameter.addOptions( RestOptionEnum.PARAM_TYPE.getDefinition(String.class). new OptionValue(RestOptionEnum.ParamTypeEnum.QUERY.getName()));
			readListFunction.addFunctionInParameter(functionParameter);
		
			functionParameter = new FunctionParameter(FunctionEnum.PARAM_NAME_IDS);
		    functionParameter.setType(PrimitiveTypeEnum.STRING.getPrimitiveType());
		    functionParameter.setCollectionType(CollectionType.LIST);
			functionParameter.setOriginatingElement(entity);
			functionParameter.addOptions( RestOptionEnum.PARAM_TYPE.getDefinition(String.class). new OptionValue(RestOptionEnum.ParamTypeEnum.QUERY.getName()));
			readListFunction.addFunctionInParameter(functionParameter);
		
		    FunctionParameter outParameter = new FunctionParameter("result");
		    outParameter.setParameterType(ParameterType.OUT);
		    switch (functionParamType) {
			case COMPLEX_TYPE:
				ComplexType resultType = this.convertWithOtherConverter(ComplexType.class, entity);
		        if (resultType == null) {
		        	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
		        }
				ComplexType resultListType = this.convertWithOtherConverter(ComplexType.class, resultType);  // this conversion returns the list type for the given complex type
		        if (resultListType != null) {
		            outParameter.setType(resultListType);
		        } else {
		        	outParameter.setType(resultType);
		        	outParameter.setCollectionType(CollectionType.LIST);
		        }
				break;
			case ENTITY:
				outParameter.setType(entity);
				// TODO invent a list-type here, too? same as with complex type?
				outParameter.setCollectionType(CollectionType.LIST);
				break;
			default:
				throw new ModelConverterException("unhandled function param type '" + functionParamType + "'");
		    }
		
		    outParameter.setOriginatingElement(entity);
		    readListFunction.addFunctionOutParameter(outParameter);
		}

		if (entity.hasStorageFunction(StorageFunction.UPDATE) && !entity.isAbstractType()) {
			// --- update
			optionValuePurpose = optionDefinitionPurpose. new OptionValue(PurposeEnum.UPDATE.getName());
			optionValueIdempotent = optionDefinitionIdempotent. new OptionValue(true);
	
			Function updateFunction = new Function(FunctionEnum.UPDATE.getName(), functionModule);
			setConversionDetails(updateFunction);
			updateFunction.addOptions(optionValuePurpose, optionValueIdempotent);
			ServiceModelProcessing.setStatusCodeIfNotPresent(updateFunction, 200L);
			updateFunction.setStorageFunction(StorageFunction.UPDATE);
			updateFunction.setOriginatingElement(entity);
			addBusinessExceptions(entity.getExceptions().get(StorageFunction.UPDATE), updateFunction);
			
			FunctionParameter functionParameter = null;
			
			if (idFields.size() > 0) {
	            for (EntityField idField : idFields) {
	            	String paramName = idFields.size() == 1 ? FunctionEnum.PARAM_NAME_ID : idField.getName();
	            	functionParameter = new FunctionParameter(paramName);
	            	functionParameter.setParameterType(ParameterType.IN);
	            	functionParameter.setType(idField.getType());
	            	functionParameter.setOriginatingElement(idField);
	            	functionParameter.addOptions( RestOptionEnum.PARAM_TYPE.getDefinition(String.class). new OptionValue(RestOptionEnum.ParamTypeEnum.PATH.getName()));
	            	updateFunction.addFunctionInParameter(functionParameter);
	            }
			}
	
	    	switch (functionParamType) {
			case COMPLEX_TYPE:
				functionParameter = new FunctionParameter(FunctionEnum.PARAM_NAME_BEAN);
		    	functionParameter.setParameterType(ParameterType.IN);
				ComplexType resultType = this.convertWithOtherConverter(ComplexType.class, entity);
		        if (resultType != null) {
		            functionParameter.setType(resultType);
		        } else {
		        	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
		        }
				break;
			case ENTITY:
				functionParameter = new FunctionParameter(FunctionEnum.PARAM_NAME_ENTITY);
		    	functionParameter.setParameterType(ParameterType.IN);
				functionParameter.setType(entity);
				break;
			default:
				throw new ModelConverterException("unhandled function param type '" + functionParamType + "'");
	        }
	
	    	ServiceModelProcessing.addConsumedStandardMediaTypesIfNotPresent(functionParameter);
	    	functionParameter.setOriginatingElement(entity);
	    	updateFunction.addFunctionInParameter(functionParameter);
	
	    	FunctionParameter outParameter = new FunctionParameter("result");
	        outParameter.setParameterType(ParameterType.OUT);
	
	        switch (functionParamType) {
			case COMPLEX_TYPE:
				ComplexType resultType = this.convertWithOtherConverter(ComplexType.class, entity);
		        if (resultType != null) {
		            outParameter.setType(resultType);
		        } else {
		        	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
		        }
				break;
			case ENTITY:
				outParameter.setType(entity);
				break;
			default:
				throw new ModelConverterException("unhandled function param type '" + functionParamType + "'");
	        }
	
	        outParameter.setOriginatingElement(entity);
	        updateFunction.addFunctionOutParameter(outParameter);
		}

		if (entity.hasStorageFunction(StorageFunction.CREATE) && !entity.isAbstractType()) {
			// --- create
			optionValuePurpose = optionDefinitionPurpose. new OptionValue(PurposeEnum.CREATE.getName());
			optionValueIdempotent = optionDefinitionIdempotent. new OptionValue(false);
	
			Function createFunction = new Function(FunctionEnum.CREATE.getName(), functionModule);
			setConversionDetails(createFunction);
			createFunction.addOptions(optionValuePurpose, optionValueIdempotent);
			ServiceModelProcessing.setStatusCodeIfNotPresent(createFunction, 201L);
			createFunction.setStorageFunction(StorageFunction.CREATE);
			createFunction.setOriginatingElement(entity);
			addBusinessExceptions(entity.getExceptions().get(StorageFunction.CREATE), createFunction);
			
			FunctionParameter functionParameter = null;
			
	    	switch (functionParamType) {
			case COMPLEX_TYPE:
				functionParameter = new FunctionParameter(FunctionEnum.PARAM_NAME_BEAN);
		    	functionParameter.setParameterType(ParameterType.IN);
				ComplexType resultType = this.convertWithOtherConverter(ComplexType.class, entity);
		        if (resultType != null) {
		            functionParameter.setType(resultType);
		        } else {
		        	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
		        }
				break;
			case ENTITY:
				functionParameter = new FunctionParameter(FunctionEnum.PARAM_NAME_ENTITY);
		    	functionParameter.setParameterType(ParameterType.IN);
				functionParameter.setType(entity);
				break;
			default:
				throw new ModelConverterException("unhandled function param type '" + functionParamType + "'");
	        }
	
	    	ServiceModelProcessing.addConsumedStandardMediaTypesIfNotPresent(functionParameter);
	    	functionParameter.setOriginatingElement(entity);
	    	createFunction.addFunctionInParameter(functionParameter);
	
	    	FunctionParameter outParameter = new FunctionParameter("result");
	        outParameter.setParameterType(ParameterType.OUT);
	        switch (functionParamType) {
			case COMPLEX_TYPE:
				ComplexType resultType = this.convertWithOtherConverter(ComplexType.class, entity);
		        if (resultType != null) {
		            outParameter.setType(resultType);
		        } else {
		        	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
		        }
				break;
			case ENTITY:
				outParameter.setType(entity);
				break;
			default:
				throw new ModelConverterException("unhandled function param type '" + functionParamType + "'");
	        }
	
	
	        outParameter.setOriginatingElement(entity);
	        createFunction.addFunctionOutParameter(outParameter);
		}
		
		if (entity.hasStorageFunction(StorageFunction.MULTI_PURPOSE)) {
			// --- multi purpose ('process' function)
//			Function processFunction = new Function(FunctionEnum.MULTI_PURPOSE.getName(), functionModule);
//			setConversionDetails(processFunction);
			// TODO add functionality for this kind of storage function
		}
		
		// --- create and add function modules that are created for consumed entities and add their related services as consumed services
		for (Entity consumedEntity : entity.getConsumedEntities()) {
			functionModule.addConsumedEntity(consumedEntity);
			FunctionModule consumedFunctionModule = convertWithOtherConverter(FunctionModule.class, consumedEntity);
			functionModule.addConsumedFunctionModule(consumedFunctionModule);
		}
        
		
		// --- create function to search for fields
		List<EntityField> searchableFields = entity.getAllEntityFields().stream()
				.filter(EntityField::isSearchable)
				.collect(Collectors.toList());
		if (!searchableFields.isEmpty()) {
			OptionDefinition<String>.OptionValue optionOperation = RestOptionEnum.OPTION_DEFINITION_OPERATION. new OptionValue("GET");
			OptionDefinition<String>.OptionValue optionPath = RestOptionEnum.OPTION_DEFINITION_PATH. new OptionValue("/search");
			OptionDefinition<String>.OptionValue optionQuery = RestOptionEnum.OPTION_DEFINITION_PARAM_TYPE. new OptionValue(RestOptionEnum.ParamTypeEnum.QUERY.getName());

			Function searchFunction = new Function(FunctionEnum.SEARCH.getName(), functionModule);
			setConversionDetails(searchFunction);
			ServiceModelProcessing.addProducedStandardMediaTypesIfNotPresent(searchFunction);
			searchFunction.setOriginatingElement(entity);
			for (EntityField searchableField : searchableFields) {
				FunctionParameter searchParameter = new FunctionParameter(searchableField.getName());
				searchParameter.setType(searchableField.getType());
				searchParameter.setCollectionType(searchableField.getCollectionType());
				searchParameter.setParameterType(ParameterType.IN);
				searchParameter.addOptions(optionQuery);
				searchFunction.addFunctionInParameter(searchParameter);
			}
			FunctionParameter searchResult = new FunctionParameter("result");
			switch (getConverterOptions().getFunctionParamType()) {
			case COMPLEX_TYPE:
				ComplexType resultType = this.convertWithOtherConverter(ComplexType.class, entity);
	            if (resultType != null) {
	            	searchResult.setType(resultType);
	            } else {
	            	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
	            }
				break;
			case ENTITY:
				searchResult.setType(entity);
				break;
			default:
				throw new ModelConverterException("unhandled function param type '" + functionParamType + "'");
			}
			searchResult.setCollectionType(CollectionType.LIST);
			searchResult.setParameterType(ParameterType.OUT);
			searchFunction.addFunctionOutParameter(searchResult);
			searchFunction.addOptions(optionOperation);
			searchFunction.addOptions(optionPath);
			ServiceModelProcessing.setStatusCodeIfNotPresent(searchFunction, 200L);
		}
		
        // --- the following calls allows manually modeled functions to be assigned to the resulting function module
        addAdditionalFunctions(entity, functionModule);

	}

	/**
	 * When there is a function module that has the same name and the same namespace as the entity's persistence module,
	 * all functions that have an out parameter with the given entity type are going to be added to the function module
	 * that has been created for the given entity. In other words: This method creates additional functions that
	 * serve the purpose to retrieve one or more entities.
	 * Note that all other functions, that do not have an entity-typed out parameter or where the type is _not_ the same as the
	 * entity paramter for this method, are simply ignored.
	 * 
	 * @param entity
	 * @param functionModule
	 */
	private void addAdditionalFunctions(S entity, T functionModule) {
		Model previousModel = getModel().getOriginatingElement(Model.class);
		FunctionParamTypeEnum functionParamType = getConverterOptions().getFunctionParamType();
		OptionDefinitionString optionDefinitionPurpose = new OptionDefinitionString(RestOptionEnum.PURPOSE.getName());
		
		if (previousModel != null) {
			PersistenceModule persistenceModule = entity.getPersistenceModule();
			Set<FunctionModule> functionModules = previousModel.getElements(FunctionModule.class);
			for (FunctionModule aFunctionModule : functionModules) {
				if (aFunctionModule.getName().equals(persistenceModule.getName()) && aFunctionModule.getNamespace().getName().equals(persistenceModule.getNamespace().getName())) {
					// identified a function module that has functions that need to be added to the resulting function module
					for (Function existingFunction : aFunctionModule.getFunctions()) {

						if (existingFunction.getFunctionOutParameters().size() == 1 && existingFunction.getFunctionOutParameters().iterator().next().getType() == entity) {
							// --- this is the positive case: only take those functions into account that return the entity type 
						} else {
							continue;  // try it with the next function
						}
						
						// --- create a new function by using the information of the original function
						OptionDefinition<String>.OptionValue purpose = existingFunction.getOption(RestOptionEnum.PURPOSE.getName(), String.class);
				    	OptionDefinition<Boolean>.OptionValue idempotent = existingFunction.getOption(RestOptionEnum.IDEMPOTENT.getName(), Boolean.class);
				    	
						if (purpose == null) purpose = optionDefinitionPurpose. new OptionValue(PurposeEnum.READ.getName());  // by default we assume a READ purpose
						PurposeEnum purposeEnumEntry = PurposeEnum.getByName(purpose.getOptionValue());

						Function newFunction = new Function(existingFunction.getName(), functionModule);
						newFunction.setStorageFunction(StorageFunction.fromPurposeEnum(purposeEnumEntry));
						setConversionDetails(newFunction);
						newFunction.addOptions(purpose);
						if (idempotent != null) newFunction.addOptions(idempotent);
						ServiceModelProcessing.setStatusCodeIfNotPresent(newFunction, 200L);
						newFunction.setOriginatingElement(existingFunction);
						
						// --- in parameters
						for (FunctionParameter existingInParam : existingFunction.getFunctionInParameters()) {
							FunctionParameter functionParameter = new FunctionParameter(existingInParam.getName());
			            	functionParameter.setParameterType(ParameterType.IN);
			            	if (existingInParam.getType() instanceof Entity) {
				            	Entity entityType = (Entity) existingInParam.getType();
								switch (functionParamType) {
								case COMPLEX_TYPE:
									ComplexType resultType = this.convertWithOtherConverter(ComplexType.class, entityType);
						            if (resultType != null) {
						                functionParameter.setType(resultType);
						            } else {
						            	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
						            }
									break;
								case ENTITY:
									functionParameter.setType(entityType);
									break;
								default:
									throw new ModelConverterException("unhandled function param type '" + functionParamType + "'");
					            }
								
								ServiceModelProcessing.addConsumedStandardMediaTypesIfNotPresent(functionParameter);
			            	} else {
			            		functionParameter.setType(existingInParam.getType());
			            	}
			            	
			            	functionParameter.setOriginatingElement(existingInParam);
			            	OptionDefinition<String>.OptionValue pathParam = existingInParam.getOption(RestOptionEnum.PARAM_TYPE.getName(), String.class);
			            	if (pathParam != null) functionParameter.addOptions(pathParam);
			            	newFunction.addFunctionInParameter(functionParameter);
						}

						// --- out parameters
						for (FunctionParameter existingOutParam : existingFunction.getFunctionOutParameters()) {
							FunctionParameter functionParameter = new FunctionParameter(existingOutParam.getName());
			            	functionParameter.setParameterType(ParameterType.OUT);
			            	if (existingOutParam.getType() instanceof Entity) {
				            	Entity entityType = (Entity) existingOutParam.getType();
								switch (functionParamType) {
								case COMPLEX_TYPE:
									ComplexType resultType = this.convertWithOtherConverter(ComplexType.class, entityType);
						            if (resultType != null) {
						                functionParameter.setType(resultType);
						            } else {
						            	throw new ModelConverterException("was not able to convert entity type '" + entity + "' to a complex type");
						            }
									break;
								case ENTITY:
									functionParameter.setType(entityType);
									break;
								default:
									throw new ModelConverterException("unhandled function param type '" + functionParamType + "'");
					            }
			            	} else {
			            		functionParameter.setType(existingOutParam.getType());
			            	}
			            	
			            	functionParameter.setOriginatingElement(existingOutParam);
			            	newFunction.addFunctionOutParameter(functionParameter);
						}
						
						// --- business exceptions
						for (BusinessException businessException : existingFunction.getBusinessExceptions()) {
                            newFunction.addBusinessException(businessException);
						}
					}
				}
			}
		}
	}

	/**
	 * @param entity
	 * @param functionModule
	 */
	private void addPersistenceModule(S entity, T functionModule) {
		PersistenceModule persistenceModule = entity.getPersistenceModule();
		functionModule.addPersistenceModule(persistenceModule);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#isResponsibleFor(java.lang.Object)
	 */
	@Override
	public boolean isResponsibleFor(Object originalModelElement, ModelElementI previousResultingModelElement) {
		boolean result = super.isResponsibleFor(originalModelElement, previousResultingModelElement);

		if (result) {
			Entity entity = (Entity) originalModelElement;
			if (entity.isMappedSuperclass() == true || /*AJAX-146*/ entity.isAbstractType() == true) {
				result = false;
			} else {
				Set<EntityField> idFields = entity.getAllIdFields();
				if (idFields.isEmpty()) {
					// An entity always needs an ID field, otherwise we won't convert it to a function module.
					Message warning = PersistenceMessage.NO_ID_FIELD_WARNING.getMessageBuilder()
							.modelElement(entity)
							.parameters(entity)
							.build();
					addWarning(warning.getMessage());
					result = false;
				}
			}
			
			if (result) {
				result = (entity.getExternalizable() == null || entity.getExternalizable().equals(Boolean.TRUE));
			}
			
			if (result) {
				result = isEntityExternalized(entity);
			}
		}
		
		return result;
	}
	

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S entity, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T functionModule = (T) new FunctionModule(entity.getName());
		
        // Set the PATH option value as early as possible in order to have it consistently set, no matter for which prog. language we are going to generate code. (mmt 11-May-2020)
		OptionDefinition<String>.OptionValue pathOptionValue = RestOptionEnum.OPTION_DEFINITION_PATH. new OptionValue(functionModule.getName().toLowerCase());
		functionModule.addOptions(pathOptionValue);
		
		// --- @Consumes annotation
		OptionDefinition<String>.OptionValue consumesMediaTypes = entity.getOption(RestOptionEnum.CONSUMES.getName(), String.class);
		if (consumesMediaTypes != null && consumesMediaTypes.getOptionValues() != null && !consumesMediaTypes.getOptionValues().isEmpty()) {
			functionModule.addOptions(consumesMediaTypes);
		}
		
		// --- @Produces annotation
		OptionDefinition<String>.OptionValue producesMediaTypes = entity.getOption(RestOptionEnum.PRODUCES.getName(), String.class);
		if (producesMediaTypes != null && producesMediaTypes.getOptionValues() != null && producesMediaTypes.getOptionValues().size() > 0) {
			functionModule.addOptions(producesMediaTypes);
		}

		return functionModule;
	}
	
	/**
	 * TODO how to continue this? invent ExceptionTypeToBusinessExceptionConverter? move BusinessException to Persistence?
	 * 
	 * @param exceptionTypes
	 * @param function
	 */
	private void addBusinessExceptions(Collection<ExceptionType> exceptionTypes, Function function) {
		if (exceptionTypes != null) {
			BusinessException parentBusinessException = null;
			Capability capability = getModelConverter().getCapabilityContext();
			if (capability != null) {
				parentBusinessException = convertWithOtherConverter(BusinessException.class, capability);
			}
			for (ExceptionType exceptionType : exceptionTypes) {
				final BusinessException businessException;
				if (exceptionType.getSingleExtensionElement(BusinessException.class) != null) {
					businessException = exceptionType.getSingleExtensionElement(BusinessException.class);
				} else {
					businessException = new BusinessException(exceptionType.getName());
					businessException.setOriginatingElement(exceptionType);
					businessException.setParent(parentBusinessException);
					businessException.setModule(function.getModule());
					addModelElement(businessException);
					// AJAX-260
					exceptionType.getFields().stream()
					.forEach(field -> {
						Field newField = new Field(field.getName(), businessException);
						newField.setOriginatingElement(field);
						newField.setType(field.getType());
						for (@SuppressWarnings("rawtypes") OptionValue optionValue : field.getOptions()) {
							newField.addOptions(optionValue);
						}
					});
					
				}
				
				function.addBusinessException(businessException);
			}	
		}
	}
}
