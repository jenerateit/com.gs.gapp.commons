package com.gs.gapp.converter.persistence.function;

import java.util.stream.Stream;

import com.gs.gapp.metamodel.basic.typesystem.Field;
import com.gs.gapp.metamodel.basic.typesystem.PrimitiveTypeEnum;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.function.AbstractCapabilityException;
import com.gs.gapp.metamodel.product.Capability;

public abstract class AbstractCapabilityToAbstractCapabilityExceptionConverter<S extends Capability, T extends AbstractCapabilityException> extends
    AbstractM2MModelElementConverter<S, T> {


	public AbstractCapabilityToAbstractCapabilityExceptionConverter(AbstractConverter modelConverter) {
		super(modelConverter, ModelElementConverterBehavior.CREATE_AND_CONVERT_IN_ONE_GO);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElement, com.gs.gapp.metamodel.basic.ModelElement)
	 */
	@Override
	protected void onConvert(S capability, T abstractCapabilityException) {
		super.onConvert(capability, abstractCapabilityException);
		
		// --- Here we add standard fields for a textual status and a numeric status.
		// --- Note that although these fields look like being invented for exceptions
		// --- that are going to be used for generated webservices, they actually are not.
		// --- It is common practice to use an error number along with a short text to tell
		// --- the user something about the root cause in a brief manner. A Java exception's
		// --- already existing text parameter 'message' is going to be used for a more elaborate
		// --- error message that should always be interpreted by a human being.
		Stream.of(AbstractCapabilityException.FieldEnum.values()).forEach(fieldEnum -> {
			Field statusField = new Field(fieldEnum.getName(), abstractCapabilityException);
			statusField.setReadOnly(true);
			
			switch (fieldEnum) {
			case STATUS_CODE:
				statusField.setType(PrimitiveTypeEnum.STRING.getPrimitiveType());
				break;
			case STATUS_NUMBER:
				statusField.setType(PrimitiveTypeEnum.SINT64.getPrimitiveType());
				break;
			default:
				break;
			}
		});
	}
	
	protected PersistenceToFunctionConverter getModelConverter() {
		return (PersistenceToFunctionConverter) super.getModelConverter();
	}
}
