package com.gs.gapp.converter.persistence.function;

import java.util.HashMap;

public enum FunctionParamTypeEnum {

	ENTITY ("entity"),
	COMPLEX_TYPE ("complexType"),
	;

	private static HashMap<String, FunctionParamTypeEnum> entryMap =
			new HashMap<>();

	static {
		for (FunctionParamTypeEnum enumEntry : values()) {
            entryMap.put(enumEntry.getName().toLowerCase(), enumEntry);
		}
	}

	private final String name;

	private FunctionParamTypeEnum(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 * @return
	 */
	public static FunctionParamTypeEnum forName(String name) throws IllegalArgumentException {
		if (name != null) {
            return entryMap.get(name.toLowerCase());
		}
		throw new IllegalArgumentException("no FunctionParamTypeEnum enum entry found for '" + name + "'");
	}
}
