package com.gs.gapp.converter.persistence.function;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.AbstractBusinessException;
import com.gs.gapp.metamodel.product.Capability;

/**
 * This element converter ensures that a modeled capability always has a related instance of BusinessException that serves
 * as an abstract parent class for all business exceptions that are going to be generated further down the line.
 * As of August 2016 the class 'Capability' is not given a field 'abstractBusinessException' since we do not want to
 * introduce a dependency from function metamodel to product metamodel for this single reason. This might change over time, though.
 * 
 * @author mmt
 * 
 * @param <S>
 * @param <T>
 */
public class CapabilityToAbstractBusinessExceptionConverter<S extends Capability, T extends AbstractBusinessException>
    extends AbstractCapabilityToAbstractCapabilityExceptionConverter<S, T> {


	public CapabilityToAbstractBusinessExceptionConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S capability, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T abstractBusinessException = (T) new AbstractBusinessException(StringTools.firstUpperCase(capability.getName()) + "ServiceException");
		abstractBusinessException.setModule(capability.getModule());
		
		return abstractBusinessException;
	}
}