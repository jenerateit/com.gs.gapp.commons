package com.gs.gapp.converter.persistence.function;

import org.jenerateit.util.StringTools;

import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.function.AbstractTechnicalException;
import com.gs.gapp.metamodel.product.Capability;

/**
 * This element converter ensures that a modeled capability always has a related instance of TechnicalException that serves
 * as an abstract parent class for all technical exceptions that are going to be generated further down the line.
 * 
 * @author mmt
 * 
 * @param <S>
 * @param <T>
 */
public class CapabilityToAbstractTechnicalExceptionConverter<S extends Capability, T extends AbstractTechnicalException>
    extends AbstractCapabilityToAbstractCapabilityExceptionConverter<S, T> {


	public CapabilityToAbstractTechnicalExceptionConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object)
	 */
	@Override
	protected T onCreateModelElement(S capability, ModelElementI previousResultingModelElement) {
		@SuppressWarnings("unchecked")
		T abstractTechnicalException = (T) new AbstractTechnicalException(StringTools.firstUpperCase(capability.getName()) + "TechnicalServiceException");
		abstractTechnicalException.setModule(capability.getModule());
		
		return abstractTechnicalException;
	}
}
