/**
 *
 */
package com.gs.gapp.converter.persistence.function;

import com.gs.gapp.converter.persistence.basic.AbstractPersistenceToBasicElementConverter;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;

/**
 * This abstract model element converter is meant to be used when you want to convert
 * from an object that is of type ModelElement to another object that
 * also is of type ModelElement.
 *
 * @author mmt
 *
 */
public abstract class AbstractPersistenceToFunctionElementConverter<S extends ModelElementI, T extends ModelElementI> extends
		AbstractPersistenceToBasicElementConverter<S, T> {

	/**
	 * @param modelConverter
	 */
	public AbstractPersistenceToFunctionElementConverter(AbstractConverter modelConverter) {
		super(modelConverter);
	}

	public AbstractPersistenceToFunctionElementConverter(AbstractConverter modelConverter, ModelElementConverterBehavior modelElementConverterBehavior) {
		super(modelConverter, modelElementConverterBehavior);
	}

	/**
	 * @param modelConverter
	 * @param previousResultingElementRequired
	 */
	public AbstractPersistenceToFunctionElementConverter(AbstractConverter modelConverter,
			boolean previousResultingElementRequired, boolean indirectConversionOnly, boolean createAndConvertInOneGo) {
		super(modelConverter, previousResultingElementRequired, indirectConversionOnly, createAndConvertInOneGo);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#getModelConverter()
	 */
	@Override
	protected PersistenceToFunctionConverter getModelConverter() {
		return (PersistenceToFunctionConverter) super.getModelConverter();
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#getConverterOptions()
	 */
	@Override
	protected PersistenceToFunctionConverterOptions getConverterOptions() {
		return getModelConverter().getConverterOptions();
	}
}
