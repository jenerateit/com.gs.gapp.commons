package com.gs.gapp.converter.persistence.function;

import java.util.List;
import java.util.Set;

import com.gs.gapp.converter.persistence.basic.PersistenceToBasicConverter;
import com.gs.gapp.metamodel.analytics.ElementConverterConfigurationTreeNode;
import com.gs.gapp.metamodel.analytics.TransformationStepConfiguration;
import com.gs.gapp.metamodel.basic.Model;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.persistence.Entity;
import com.gs.gapp.metamodel.persistence.PersistenceModule;
import com.gs.gapp.metamodel.product.Organization;
import com.gs.gapp.metamodel.product.ProductModule;

/**
 * @author mmt
 *
 */
public class PersistenceToFunctionConverter extends PersistenceToBasicConverter {

	private PersistenceToFunctionConverterOptions converterOptions;
	/**
	 *
	 */
	public PersistenceToFunctionConverter() {
		super();
	}

	
	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.BasicConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result =
				super.onGetAllModelElementConverters();

		result.add(new EntityToFunctionModuleConverter<>(this));
		result.add(new NamespaceToFunctionNamespaceConverter<>(this));
//		result.add(new CapabilityToAbstractBusinessExceptionConverter<>(this));
//		result.add(new CapabilityToAbstractTechnicalExceptionConverter<>(this));
		result.add(new EntityToExpandEnumerationConverter<>(this));
		
		if (converterOptions.areEntityTypedParamsMadeComplexTyped()) {
			result.add(new FunctionModuleToFunctionModuleConverter<>(this));
			result.add(new FunctionToFunctionConverter<>(this));
		}

		return result;
	}
	
	

    @Override
	protected Set<Object> onPerformModelConsolidation(Set<?> normalizedElements) {
		final Set<Object> result = super.onPerformModelConsolidation(normalizedElements);
            
		// --- re-add all elements that serve as input for the next model converter (pass-through functionality), exception: model element of type 'Model'
		for (Object normalizedElement : normalizedElements) {
			if (normalizedElement instanceof Model ||
					normalizedElement instanceof TransformationStepConfiguration ||
					normalizedElement instanceof ElementConverterConfigurationTreeNode) {
					continue;
			}
			
			if (getConverterOptions().areIncomingElementsPassedThrough()) {
				if (normalizedElement instanceof ModelElementI) {
					ModelElementI normalizedModelElement = (ModelElementI) normalizedElement;
					result.add(normalizedModelElement);
					getModel().addElement(normalizedModelElement);
					
//					if (normalizedElement instanceof Module) {
//						Module module = (Module) normalizedElement;
//						module.getElements().stream()
//							.filter(ComplexType.class::isInstance)
//							.map(ComplexType.class::cast)
//							.forEach(complexType -> result.add(complexType));
//					}
				}
			} else {
				// We are still passing through a few of them, since they are needed in subsequent steps.
				if (normalizedElement instanceof PersistenceModule) {
				    PersistenceModule persistenceModule = (PersistenceModule) normalizedElement;
					result.add(persistenceModule);
				    getModel().addElement(persistenceModule);
				} else if (normalizedElement instanceof Entity) {
					result.add(normalizedElement);
				} else if (normalizedElement instanceof ProductModule) {
		            ProductModule productModule = (ProductModule) normalizedElement;
		            result.add(productModule);
		            getModel().addElement(productModule);
				} else if (normalizedElement instanceof Organization) {
		            result.add(normalizedElement);
		            getModel().addElement((Organization) normalizedElement);
				}
			}
		}
		
//		ServiceModelProcessing.completeServiceOptions(result)
//			.stream()
//			.forEach(message -> addMessage(message));
		
		return result;
	}
    

	/* (non-Javadoc)
     * @see com.gs.gapp.converter.analytics.AbstractAnalyticsConverter#getConverterOptions()
     */
    @Override
	public PersistenceToFunctionConverterOptions getConverterOptions() {
    	if (this.converterOptions == null) {
    		this.converterOptions = new PersistenceToFunctionConverterOptions(getOptions());
    	}
		return converterOptions;
	}
}
